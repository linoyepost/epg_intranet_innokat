﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;

/// <summary>
/// Summary description for ErrorLogs
/// </summary>
public class ErrorLogs
{
    public void LogError(string ErrorMessage)
    {
        try
        {
            string strWebAppPath = HttpContext.Current.Server.MapPath(".") + "\\logs";
            if (!Directory.Exists(strWebAppPath))
            {
                Directory.CreateDirectory(strWebAppPath);
            }
            strWebAppPath += "\\ErrorLogs-" + DateTime.Now.ToString("ddMMyyyy") + ".log";
            if (!File.Exists(strWebAppPath))
            {
                // Create a new file to write to.
                using (StreamWriter objStreamWriter = File.CreateText(strWebAppPath))
                {
                    objStreamWriter.WriteLine("Message,   " + DateTime.Now.ToString() + ",   " + ErrorMessage);
                    objStreamWriter.WriteLine("---------------------------------------------------------------------------------------");
                    objStreamWriter.Close();
                }
            }
            else
            {
                // Append in exisiting file.
                using (StreamWriter objStreamWriter = File.AppendText(strWebAppPath))
                {
                    objStreamWriter.WriteLine("Message,   " + DateTime.Now.ToString() + ",   " + ErrorMessage);
                    objStreamWriter.WriteLine("---------------------------------------------------------------------------------------");
                    objStreamWriter.Close();
                }
            }
        }
        catch (Exception Msgexception)
        {
        }
    }

    public void LogError(Exception ex)
    {
        try
        {
            string strWebAppPath = HttpContext.Current.Server.MapPath(".") + "\\logs";
            if (!Directory.Exists(strWebAppPath))
            {
                Directory.CreateDirectory(strWebAppPath);
            }
            strWebAppPath += "\\ErrorLogs-" + DateTime.Now.ToString("ddMMyyyy") + ".log";
            if (!File.Exists(strWebAppPath))
            {
                // Create a new file to write to.
                using (StreamWriter objStreamWriter = File.CreateText(strWebAppPath))
                {
                    objStreamWriter.WriteLine("Exception,   " + DateTime.Now.ToString() + ",   " + ex.Message.ToString());
                    objStreamWriter.WriteLine("Exception Trace,   " + DateTime.Now.ToString() + ",   " + ex.StackTrace.ToString());
                    objStreamWriter.WriteLine("---------------------------------------------------------------------------------------");
                    objStreamWriter.Close();
                }
            }
            else
            {
                // Append in exisiting file.
                using (StreamWriter objStreamWriter = File.AppendText(strWebAppPath))
                {
                    objStreamWriter.WriteLine("Exception,   " + DateTime.Now.ToString() + ",   " + ex.Message.ToString());
                    objStreamWriter.WriteLine("Exception Trace,   " + DateTime.Now.ToString() + ",   " + ex.StackTrace.ToString());
                    objStreamWriter.WriteLine("---------------------------------------------------------------------------------------");
                    objStreamWriter.Close();
                }
            }
        }
        catch (Exception Exexception)
        {
        }
    }

    public void LogError(System.Data.SqlClient.SqlException ex)
    {
        try
        {
            string strWebAppPath = HttpContext.Current.Server.MapPath(".") + "\\logs";
            if (!Directory.Exists(strWebAppPath))
            {
                Directory.CreateDirectory(strWebAppPath);
            }
            strWebAppPath += "\\ErrorLogs-" + DateTime.Now.ToString("ddMMyyyy") + ".log";
            if (!File.Exists(strWebAppPath))
            {
                // Create a new file to write to.
                using (StreamWriter objStreamWriter = File.CreateText(strWebAppPath))
                {
                    foreach (System.Data.SqlClient.SqlError myError in ex.Errors)
                    {
                        objStreamWriter.WriteLine("SqlException,   " + DateTime.Now.ToString() + ",   " + myError.Message.ToString());

                    }
                    objStreamWriter.WriteLine("---------------------------------------------------------------------------------------");
                    objStreamWriter.Close();
                }
            }
            else
            {
                // Append in exisiting file.
                using (StreamWriter objStreamWriter = File.AppendText(strWebAppPath))
                {
                    foreach (System.Data.SqlClient.SqlError myError in ex.Errors)
                    {
                        objStreamWriter.WriteLine("SqlException,   " + DateTime.Now.ToString() + ",   " + myError.Message.ToString());
                    }
                    objStreamWriter.WriteLine("---------------------------------------------------------------------------------------");
                    objStreamWriter.Close();
                }
            }
        }
        catch (Exception SQLExexception)
        {
        }
    }
}