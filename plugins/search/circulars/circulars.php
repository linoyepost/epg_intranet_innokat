<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Weblinks Search plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	Search.weblinks
 * @since		1.6
 */
class plgSearchCirculars extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	/**
	 * @return array An array of search areas
	 */
	function onContentSearchAreas() {
		static $areas = array(
			'circulars' => 'PLG_SEARCH_CONTENT_CIRCULARS'
			);
			return $areas;
	}

	/**
	 * Weblink Search method
	 *
	 * The sql must return the following fields that are used in a common display
	 * routine: href, title, section, created, text, browsernav
	 * @param string Target search string
	 * @param string mathcing option, exact|any|all
	 * @param string ordering option, newest|oldest|popular|alpha|category
	 * @param mixed An array if the search it to be restricted to areas, null if search all
	 */
	function onContentSearch($text, $phrase='', $ordering='', $areas=null)
	{
		$db		= JFactory::getDbo();
		$app	= JFactory::getApplication();
		$user	= JFactory::getUser();
		$groups	= implode(',', $user->getAuthorisedViewLevels());


		$searchText = $text;

		if (is_array($areas)) {
			if (!array_intersect($areas, array_keys($this->onContentSearchAreas()))) {
				return array();
			}
		}

		$sContent		= $this->params->get('search_content',		1);
		$sArchived		= $this->params->get('search_archived',		1);
		$limit			= $this->params->def('search_limit',		50);
		$state = array();
		if ($sContent) {
			$state[]=1;
		}
		if ($sArchived) {
			$state[]=2;
		}

		$text = trim($text);
		if ($text == '') {
			return array();
		}
		$section	= JText::_('PLG_SEARCH_CIRCULARS');

		$wheres	= array();
		$tag = JFactory::getLanguage()->getTag();
		switch ($phrase)
		{
			
		case 'exact':
				$text		= $db->Quote('%'.$db->escape($text, true).'%', false);
				$wheres2	= array();
				$wheres2[]	= 'a.id LIKE '.$text;
					if ($tag == 'ar-AA') :
					$wheres2[]	= 'a.circulars_description_ar LIKE '.$text;
					$wheres2[]	= 'a.circulars_name_ar LIKE '.$text;
					else:
					$wheres2[]	= 'a.circulars_description LIKE '.$text;
					$wheres2[]	= 'a.circulars_name LIKE '.$text;
					endif;
				$where		= '(' . implode(') OR (', $wheres2) . ')';
				break;

			case 'all':
			case 'any':
			default:
				$words	= explode(' ', $text);
				$wheres = array();
				foreach ($words as $word)
				{
					$word		= $db->Quote('%'.$db->escape($word, true).'%', false);
					$wheres2	= array();
					$wheres2[]	= 'a.id LIKE '.$word;
					  if ($tag == 'ar-AA') :
						$wheres2[]	= 'a.circulars_description_ar LIKE '.$word;
						$wheres2[]	= 'a.circulars_name_ar LIKE '.$word;
					  else:
						$wheres2[]	= 'a.circulars_description LIKE '.$word;
						$wheres2[]	= 'a.circulars_name LIKE '.$word;
					  endif;
					$wheres[]	= implode(' OR ', $wheres2);
				}
				$where	= '(' . implode(($phrase == 'all' ? ') AND (' : ') OR ('), $wheres) . ')';
				break;
		}

		switch ($ordering)
		{
			case 'oldest':
				$order = 'a.publishedon ASC';
				break;

			case 'alpha':
				$order = 'a.circulars_name ASC, a.circulars_name_ar ASC';
				break;

			case 'newest':
			default:
				$order = 'a.publishedon DESC';
		}
		
	$deptid  = (isset($_REQUEST['deptid']))? $_REQUEST['deptid'] : 0 ;
	$deptid = isset($_SESSION['deptid']) ? $_SESSION['deptid'] : $deptid;

   if($user->id && $deptid){  
		$deptquery .= " (circulars_access = 1 || circulars_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && circulars_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " circulars_access = 1 && circulars_department = '".$deptid."' ";
   }else{
	    $deptquery .= " circulars_access = 1 ";	 
   }

		$return = array();
		if (!empty($state)) {
			$query	= $db->getQuery(true);
		if ($tag == 'ar-AA') :
				$query->select('CONCAT(a.circulars_name_ar ,"(", c.departments_name_ar ,")") AS title, a.circulars_description_ar AS text, a.publishedon AS created, a.id as url');
				else:
				$query->select('CONCAT(a.circulars_name ,"(", c.departments_name ,")") AS title, a.circulars_description AS text, a.publishedon AS created, a.id as url');
				endif;
			
			$query->from('#__eposts_circulars AS a');
			$query->innerJoin('#__eposts_departments AS c ON c.id = a.circulars_department');
			$query->where('('.$where.')' . '  AND a.publish = 1 AND ('.$deptquery.')');
			$query->order($order);
		/*
		$query->where('('.$where.')' . ' AND a.publish = 1 AND ('.$deptquery.')');
		*/

			$db->setQuery($query, 0, $limit);
			$rows = $db->loadObjectList();

			$return = array();
			if ($rows) {
				foreach($rows as $key => $row) {
					$rows[$key]->href = JRoute::_("index.php?option=com_eposts&view=mainshowcirculars&deptid=".$deptid."&cirid=".$row->url."&Itemid=394");
				}
			}
			
			foreach($rows as $key => $weblink) {
					if (searchHelper::checkNoHTML($weblink, $searchText, array('url', 'text', 'title'))) {
						$return[] = $weblink;
					}
			}
		}
		return $return;
	}
}
