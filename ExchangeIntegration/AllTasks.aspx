﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AllTasks.aspx.cs" Inherits="AllTasks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>All Tasks</title>
    <style type="text/css">
        th > a
        {
            color: Black;
        }
        
        .errorMessage
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: Red;
        }
        
        .GridTitle
        {
            background-color: #d6d6d6;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
        }
        
        .GridTitle a
        {
            background-color: #d6d6d6;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
        }
        
        .AltLine
        {
            background-color: #F9F9F9;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
        }
        .Line
        {
            background-color: #E7E7E7;
            font-size: 11px;
            font-family: Arial, Helvetica, sans-serif;
        }
        .AltLine:Hover
        {
            background-color: #E7E3D8;
        }
        .Line:Hover
        {
            background-color: #E7E3D8;
        }
    </style>
</head>
<body>
    <form id="frmTasks" runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:Label ID="lblError" runat="server" CssClass="errorMessage" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvTasks" runat="server" AllowSorting="true" AllowPaging="true"
                    AutoGenerateColumns="false" Width="100%" PageSize="50" GridLines="None" ShowHeader="true"
                    DataKeyNames="Id" CellPadding="4" OnPageIndexChanging="gvTasks_PageIndexChanging"
                    OnSorting="gvTasks_Sorting" OnRowCreated="gvTasks_RowCreated">
                    <EmptyDataRowStyle Font-Names="Arial, Helvetica, sans-serif" Font-Size="11px" />
                    <EmptyDataTemplate>
                        No tasks present in the system.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField HeaderText="Title" DataField="Subject" SortExpression="Subject" ItemStyle-Width="27%"
                            ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="Left" />
                        <asp:BoundField HeaderText="Assigned By" DataField="AssignedBy" SortExpression="AssignedBy" />
                        <asp:BoundField HeaderText="Start Date" DataField="StartDate" SortExpression="StartDate" />
                        <asp:BoundField HeaderText="Due Date" DataField="DueDate" SortExpression="DueDate" />
                        <asp:BoundField HeaderText="Status" DataField="Status" SortExpression="Status" />
                        <asp:BoundField HeaderText="Priority" DataField="Priority" SortExpression="Priority" />
                        <asp:BoundField HeaderText="% Complete" DataField="PercentComplete" SortExpression="PercentComplete" />
                    </Columns>
                    <AlternatingRowStyle HorizontalAlign="Center" Width="10%" CssClass="AltLine" />
                    <RowStyle HorizontalAlign="Center" Width="10%" CssClass="Line" />
                    <HeaderStyle CssClass="GridTitle" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
