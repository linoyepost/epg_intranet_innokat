﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Security.Principal;
using System.Threading;

public partial class Login : System.Web.UI.Page
{
    DAL objDAL = new DAL();
    ErrorLogs objErrorLogs = new ErrorLogs();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

              //  string userID = WindowsIdentity.GetCurrent().Name;
				//string userName = System.Net.CredentialCache.DefaultCredentials.ToString();
				WindowsPrincipal userID = Thread.CurrentPrincipal as WindowsPrincipal; //MOIZ
				//Response.Write("<br>identity : " + userID.Identity.Name.ToString().Split('\\')[1]);
				//Response.Write("Hello " + userName);
			
                txtUserName.Text = userID.Identity.Name.TrimEnd();
                imgError.Visible = false;
                if (Request.QueryString["E"] != null && Request.QueryString["T"] != null)
                {
                    if (Request.QueryString["E"].ToString().Trim() != String.Empty && Request.QueryString["T"].ToString().Trim() != String.Empty)
                    {
                        ShowError(objDAL.ErrorMessages(Request.QueryString["E"].ToString().TrimEnd()), Request.QueryString["T"].ToString().Trim());
                    }
                }
            }
        }
        catch (Exception ex)
        {
            objErrorLogs.LogError(ex);
            ShowError(ex.Message, "E");
        }
    }

    private void ShowError(string message, string ErrorType)
    {
        imgError.Visible = true;
        switch (ErrorType)
        {
            case "E":
                imgError.Src = "/ExchangeIntegration/images/error_icon.png";
                break;
            case "W":
                imgError.Src = "/ExchangeIntegration/images/warning.jpg";
                break;
        }
        lblError.Text = message;
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            int res = -1;
            if (txtUserName.Text.Trim() != "" && txtPassword.Text.Trim() != "")
            {
                string Password = objDAL.Encrypt(txtPassword.Text.TrimEnd());
				
                if (objDAL.IsUserNameExistinDB("SELECT * FROM loginifo Where UserName='" + txtUserName.Text.TrimEnd().Split('\\')[1] + "';") == "")
                {
                    res = objDAL.ExecuteSQL("Insert INto loginifo (UserName, Password) Values ('" + txtUserName.Text.TrimEnd().Split('\\')[1] + "','" + Password + "');");
                }
                else
                {
                    res = objDAL.ExecuteSQL("Update loginifo Set Password='" + Password + "' Where UserName='" + txtUserName.Text.TrimEnd().Split('\\')[1] + "';");
                }

                if (res > 0)
                    Response.Redirect("AllTasks.aspx", false);
                else
                    ShowError("Unable to save username or password, please try again later.", "E");
            }
            else
                ShowError("Enter username or password.", "W");
        }
        catch (Exception ex)
        {
            objErrorLogs.LogError(ex);
            ShowError(ex.Message, "E");
        }
    }
}