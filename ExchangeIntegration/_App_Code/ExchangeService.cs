﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

/// <summary>
/// Summary description for ExchangeService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ExchangeService : System.Web.Services.WebService {

    public ExchangeService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public DataTable TasksList()
    {
        //ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
        //service.UseDefaultCredentials = true;
        //service.Url = new Uri(ConfigurationManager.AppSettings["ExchangeServerUrl"]);
        //TasksFolder tasksfolder = TasksFolder.Bind(service, WellKnownFolderName.Tasks, new PropertySet(BasePropertySet.IdOnly, FolderSchema.TotalCount));
        //ItemView view = new ItemView(tasksfolder.TotalCount);
        //view.PropertySet = new PropertySet(BasePropertySet.IdOnly, TaskSchema.Subject, TaskSchema.Owner, TaskSchema.StartDate, TaskSchema.DueDate, TaskSchema.Status, TaskSchema.Importance, TaskSchema.PercentComplete);
        ////view.OrderBy = OrderByCollection.
        //FindItemsResults<Item> taskItems = service.FindItems(WellKnownFolderName.Tasks, view);
        //if (taskItems.TotalCount == 0)
        //    return null;
        //else
        //{
        DataTable tasksTable = new DataTable();
        tasksTable.TableName = "Tasks";
        tasksTable.Columns.Add("Id");
        tasksTable.Columns.Add("Subject");
        tasksTable.Columns.Add("AssignedBy");
        tasksTable.Columns.Add("StartDate", typeof(DateTime));
        tasksTable.Columns.Add("DueDate", typeof(DateTime));
        tasksTable.Columns.Add("Status");
        tasksTable.Columns.Add("Priority");
        tasksTable.Columns.Add("PercentComplete", typeof(int));
        tasksTable.Rows.Add(1, "Test 1", "Owner 1", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
        tasksTable.Rows.Add(1, "Test 2", "Owner 2", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
        tasksTable.Rows.Add(1, "Test 3", "Owner 3", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
        tasksTable.Rows.Add(1, "Test 4", "Owner 4", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
        tasksTable.Rows.Add(1, "Test 5", "Owner 5", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
        tasksTable.Rows.Add(1, "Test 6", "Owner 6", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
        tasksTable.Rows.Add(1, "Test 7", "Owner 7", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
        tasksTable.Rows.Add(1, "Test 8", "Owner 8", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
        //foreach (Item item in taskItems)
        //{
        //Task task = item as Task;
        //DataRow taskRow = tasksTable.NewRow();
        //taskRow["Id"] = task.Id;
        //taskRow["Subject"] = task.Subject;
        //taskRow["AssignedBy"] = task.Owner;
        //taskRow["StartDate"] = task.StartDate;
        //taskRow["DueDate"] = task.DueDate;
        //taskRow["Status"] = task.Status;
        //taskRow["Priority"] = Enum.GetName(typeof(Importance), task.Importance);
        //taskRow["PercentComplete"] = task.PercentComplete;
        //tasksTable.Rows.Add(taskRow);
        //}
        return tasksTable;
        //}
    }
}
