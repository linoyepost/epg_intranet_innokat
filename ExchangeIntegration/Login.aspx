﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Form</title>
    <style type="text/css">
        th > a
        {
            color: Black;
        }
        
        .errorMessage
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: Red;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table width="60%" cellpadding="0" cellspacing="0" align="left" style=" border: 0px;">
        <tr>
            <td style=" background-color: #F9F9F9; padding-left: 10px; padding-top: 10px; font-family: Arial, Helvetica, sans-serif;font-size: 12px;">
                User Name :<span style="font-weight:bold; color: Red;">*</span> 
            </td>
            <td style=" background-color: #F9F9F9; padding-left: 10px; padding-top: 10px;">
                <asp:TextBox ID="txtUserName" runat="server" Columns="25" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style=" background-color: #F9F9F9; padding-left: 10px; padding-bottom: 10px; padding-top: 10px; font-family: Arial, Helvetica, sans-serif;font-size: 12px;">
                Password :<span style="font-weight:bold; color: Red;">*</span> 
            </td>
            <td style=" background-color: #F9F9F9; padding-left: 10px; padding-bottom: 10px; padding-top: 10px;">
                <asp:TextBox ID="txtPassword" runat="server" Columns="25" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="*" 
                    Font-Bold="true" ControlToValidate="txtPassword" Display="Dynamic" 
                    ValidationGroup="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
        <td style="background: #ffffff;" colspan="2" align="center">
            <asp:Button ID="btnLogin" runat="server" Text="Login" 
                onclick="btnLogin_Click" ValidationGroup="Required" />
        </td>
        </tr>
        <tr>
            <td colspan="2">
                <img id="imgError" src="" runat="server" />&nbsp;<asp:Label ID="lblError" runat="server" CssClass="errorMessage"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
