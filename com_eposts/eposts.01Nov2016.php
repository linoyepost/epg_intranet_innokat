﻿<?php
/**
* @version 1.5
* @package JDownloads
* @copyright (C) 2009 www.eposts.com
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* 
*
*/

defined( '_JEXEC' ) or die( 'Restricted access-php' );

// error_reporting(E_ALL ^ (E_NOTICE | E_DEPRECATED | E_STRICT) );
error_reporting(E_ERROR | E_PARSE | E_COMPILE_ERROR);

clearstatcache();
session_start();

    jimport( 'joomla.application.component.view');
    $database = &JFactory::getDBO();
    $jconfig = new JConfig();
    $option = 'com_eposts';
    
	global $Itemid, $mainframe;
    global $id, $limit, $limitstart, $site_aktuell, $catid, $cid, $task, $view, $pop, $jlistConfig, $jlistTemplates, $page_title;
    global $cat_link_itemids, $upload_link_itemid, $search_link_itemid, $root_itemid, $deptid;
    
	$mainframe = JFactory::getApplication();
    $Itemid = JRequest::getInt('Itemid');
    $deptid = $_SESSION['deptid'];
    
    jimport( 'joomla.html.parameter' );

    $document =& JFactory::getDocument();
    $params    = &$mainframe->getParams();
    
	$GLOBALS['_VERSION']	= new JVersion();
	$version				= $GLOBALS['_VERSION']->getShortVersion();
		
    $params2   = JComponentHelper::getParams('com_languages');
    $frontend_lang = $params2->get('site', 'en-GB');
    $language = JLanguage::getInstance($frontend_lang);
    
    require_once( JPATH_COMPONENT_SITE.DS.'eposts.html.php' ); 
    require_once( JPATH_COMPONENT_SITE.DS.'eposts.class.php' );
	//require_once(ELPATH.'/../../includes/pageNavigation.php');
	//require_once('/../../plugins/content/pagenavigation/pagenavigation.php');

	$id = (int)JArrayHelper::getValue( $_REQUEST, 'cid', array(0));
	if (!is_array( $id)) {
         $id = array(0);
    }
    $cid = $id;
    
    $GLOBALS['jlistConfig'] = buildjlistConfig();
    $GLOBALS['jlistTemplates'] = getTemplates();
    
    // search for root menu item to use
    $sql = 'SELECT id FROM #__menu WHERE link = ' . $database->Quote("index.php?option=com_eposts&view=viewcategories"). ' AND published = 1' ;
    $database->setQuery($sql);
    $root_itemid = $database->loadResult();
    if (!$root_itemid) $root_itemid = $Itemid;

    // Page Title
    $menus    = &JSite::getMenu();
    $menu    = $menus->getActive();
	
    $page_title =  $document->getTitle( 'title');
    
    // get all published single category menu links
    $database->setQuery("SELECT id, link from #__menu WHERE link LIKE 'index.php?option=com_eposts&view=viewcategory&catid%' AND published = 1");
    $cat_link_itemids = $database->loadAssocList();
    if ($cat_link_itemids){
        for ($i=0; $i < count($cat_link_itemids); $i++){
             $cat_link_itemids[$i][catid] = substr( strrchr ( $cat_link_itemids[$i][link], '=' ), 1);
        }    
    }
    // get upload link itemid when exists
    $database->setQuery("SELECT id from #__menu WHERE link = 'index.php?option=com_eposts&view=upload' AND published = 1");    
    $upload_link_itemid = $database->loadResult();

    // get search link itemid when exists
    $database->setQuery("SELECT id from #__menu WHERE link = 'index.php?option=com_eposts&view=search' AND published = 1");    
    $search_link_itemid = $database->loadResult();

    $pop 			= intval( JArrayHelper::getValue( $_REQUEST, 'pop', 0 ) );
	$task 			= JRequest::getCmd( 'task' );
    $view           = JRequest::getCmd(  'view' );
	$cid 			= (int)JArrayHelper::getValue($_REQUEST, 'cid', array());
    $catid          = (int)JArrayHelper::getValue($_REQUEST, 'catid', 0);
    
	$limit        = intval($jlistConfig['files.per.side']);
	$limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    $site_aktuell = intval( JRequest::getInt( 'site', 1 ) );
	
    
    // AUP integration
    if ($jlistConfig['use.alphauserpoints']){
        $api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
        if (file_exists($api_AUP)){
            require_once ($api_AUP);
        }
    }    
     

    if ($task && !$view) $view = $task;

switch ($view) {
	/////////////////////////Linke and Dislike Module///////////////////
		case'IsLike':
		IsLike();
		break;
		case'LikeCir':
		LikeCir();
		break;	
		case'LikeImages':
		LikeImages();
		break;
		case'LikeForms':
		LikeForms();
		break;	
		case'LikeDoc':
		LikeDoc();
		break;
	/////////////////////////Development Phase VIII/////////////////////
	   case 'loadiframe':
	     showiframe($option,$view);
	   break;
	   case 'iframeTarger':
	     iframeTarger($option,$view);
	   break;
	 /////////////////////////Development Phase VII/////////////////////
	   case 'loadimage':
	     showloadimage($option,$view);
	   break;
	///////////////////////////////////Depevloment Phase VI ////////////////////////////
	  	  case 'mainapplyjobs':
	      if($task == "edit.mainapplyjobs"){
		    saveApplyJobs($option, $cid, $apply=0);
		  }else{
	        mainApplyJobs($option,$view);
		  }
	   break;	   
	   case 'mainshowjobs':
	       mainShowJobs($option,$view);
	   break;
	   case 'mainlistjobs':
	       mainListJobs($option,$view);
	   break;
	   case 'mainlistjobsmessage':
	   		mainlistjobsmessage($option,$view);
	   break;
	   case 'mainshowtasks':
	       mainShowTasks($option,$view);
	   break;
	///////////////////////////////////Depevloment Phase V ////////////////////////////
		case 'empdownload' :
			empDownload();
		break;
	///////////////////////////////////Depevloment Phase IV ////////////////////////////
	   case 'newannouncements':
	      if($task == "edit.announcements"){
		    saveNewAnnouncements($option, $cid, $apply=0);
		  }elseif($task == "delannouncements"){ 
		    deleteNewAnnouncements($option, $cid);
		  }else{
	        newAnnouncements($option,$view);
		  }
	   break;	   
	   case 'listannouncements':
	       listAnnouncements($option,$view);
	   break;	   
	   case 'mainshowannouncements':
	       mainShowAnnouncements($option,$view);
	   break;	   
	   case 'mainlistannouncements':
	       mainListAnnouncements($option,$view);
	   break;
	   
	   case 'newnews':
	      if($task == "edit.news"){
		    saveNewNews($option, $cid, $apply=0);
		  }elseif($task == "delnews"){ 
		    deleteNewNews($option, $cid);
		  }else{
	        newNews($option,$view);
		  }
	   break;	   
	   case 'listnews':
	       listNews($option,$view);
	   break;	   
	   case 'mainshownews':
	       mainShowNews($option,$view);
	   break;	   
	   case 'mainlistnews':
	       mainListNews($option,$view);
	   break;
	   
	   case 'newobjectives':
	      if($task == "edit.objectives"){
		    saveNewObjectives($option, $cid, $apply=0);
		  }elseif($task == "delobjectives"){ 
		    deleteNewObjectives($option, $cid);
		  }else{
	        newObjectives($option,$view);
		  }
	   break;	   
	   case 'listobjectives':
	       listObjectives($option,$view);
	   break;	   
	   
	   case 'newvisions':
	      if($task == "edit.visions"){
		    saveNewVisions($option, $cid, $apply=0);
		  }elseif($task == "delvisions"){ 
		    deleteNewVisions($option, $cid);
		  }else{
	        newVisions($option,$view);
		  }
	   break;	   
	   case 'listvisions':
	       listVisions($option,$view);
	   break;
	   case 'mainshowvisions':
	       mainShowVisions($option,$view);
	   break;	   
	   case 'mainlistvisions':
	       mainListVisions($option,$view);
	   break;
	   
	   case 'newalerts':
	      if($task == "edit.alerts"){
		    saveNewAlerts($option, $cid, $apply=0);
		  }elseif($task == "delalerts"){ 
		    deleteNewAlerts($option, $cid);
		  }else{
	        newAlerts($option,$view);
		  }
	   break;	   
	   case 'listalerts':
	       listAlerts($option,$view);
	   break;	   
	   case 'mainshowalerts':
	       mainShowAlerts($option,$view);
	   break;	   
	   case 'mainlistalerts':
	       mainListAlerts($option,$view);
	   break;
	   
	   case 'newpolicies':
	      if($task == "edit.policies"){
		    saveNewPolicies($option, $cid, $apply=0);
		  }elseif($task == "delpolicies"){ 
		    deleteNewPolicies($option, $cid);
		  }else{
	        newPolicies($option,$view);
		  }
	   break;	   
	   case 'listpolicies':
	       listPolicies($option,$view);
	   break;
	   case 'mainshowpolicies':
	       mainShowPolicies($option,$view);
	   break;	   
	   case 'mainlistpolicies':
	       mainListPolicies($option,$view);
	   break;
	   	   
	   case 'newquotes':
	      if($task == "edit.quotes"){
		    saveNewQuotes($option, $cid, $apply=0);
		  }elseif($task == "delquotes"){ 
		    deleteNewQuotes($option, $cid);
		  }else{
	        newQuotes($option,$view);
		  }
	   break;	   
	   case 'listquotes':
	       listQuotes($option,$view);
	   break;
	   case 'mainshowquotes':
	       mainShowQuotes($option,$view);
	   break;	   
	   case 'mainlistquotes':
	       mainListQuotes($option,$view);
	   break;
	   
	   case 'newmanuals':
	      if($task == "edit.manuals"){
		    saveNewManuals($option, $cid, $apply=0);
		  }elseif($task == "delmanuals"){ 
		    deleteNewManuals($option, $cid);
		  }else{
	        newManuals($option,$view);
		  }
	   break;	   
	   case 'listmanuals':
	       listManuals($option,$view);
	   break;	   
	   case 'mainshowmanuals':
	       mainShowManuals($option,$view);
	   break;	   
	   case 'mainlistmanuals':
	       mainListManuals($option,$view);
	   break;
	   
	   case 'newfaqs':
	      if($task == "edit.faqs"){
		    saveNewFaqs($option, $cid, $apply=0);
		  }elseif($task == "delfaqs"){ 
		    deleteNewFaqs($option, $cid);
		  }else{
	        newFaqs($option,$view);
		  }
	   break;	   
	   case 'listfaqs':
	       listFaqs($option,$view);
	   break;	   	   
	   case 'mainshowfaqs':
	       mainShowFaqs($option,$view);
	   break;	   
	   case 'mainlistfaqs':
	       mainListFaqs($option,$view);
	   break;
	   
	   case 'newbenchmarks':
	      if($task == "edit.benchmarks"){
		    saveNewBenchmarks($option, $cid, $apply=0);
		  }elseif($task == "delbenchmarks"){ 
		    deleteNewBenchmarks($option, $cid);
		  }else{
	        newBenchmarks($option,$view);
		  }
	   break;	   
	   case 'listbenchmarks':
	       listBenchmarks($option,$view);
	   break;	   	   
	   case 'mainshowbenchmarks':
	       mainShowBenchmarks($option,$view);
	   break;	   
	   case 'mainlistbenchmarks':
	       mainListBenchmarks($option,$view);
	   break;
	   
	   case 'newlinks':
	      if($task == "edit.links"){
		    saveNewLinks($option, $cid, $apply=0);
		  }elseif($task == "dellinks"){ 
		    deleteNewLinks($option, $cid);
		  }else{
	        newLinks($option,$view);
		  }
	   break;	   
	   case 'listlinks':
	       listLinks($option,$view);
	   break;
   //////////////////////////////END Depevloment Phase IV////////////////////////////////
	   case 'showsllider':
		   mainShowSlider($option,$view);
	   break;
	   case 'mainaddempinfo':
	       mainAddEmpInfo($option,$view);
	   break;
	   
	   case 'mainshowempinfo':
	       mainShowEmpInfo($option,$view);
	   break;
	   
	   case 'mainlistlinks':
	       mainListLinks($option,$view);
	   break;
	   
	   case 'mainshoworgcharts':
	       mainShowOrgCharts($option,$view);
	   break;
	   
	   case 'mainlistorgcharts':
	       mainListOrgCharts($option,$view);
	   break;
	   
	   case 'mainshowstaffdirectories':
	       mainShowStaffDirectories($option,$view);
	   break;
	   
	   case 'mainliststaffdirectories':
	       mainListStaffDirectories($option,$view);
	   break;
	   ////////////////////////////
	
	   case 'mainshownewhires':
	       mainShowNewHires($option,$view);
	   break;
	   
	   case 'mainlistnewhires':
	       mainListNewHires($option,$view);
	   break;
	   
	   case 'mainshowdocuments':
	       mainShowDocuments($option,$view);
	   break;
	   
	   case 'mainlistdocuments':
	       mainListDocuments($option,$view);
	   break;
	   
	   case 'mainlistmedias':
	       mainListMedias($option,$view);
	   break;
	   
	   case 'mainshowevents':
	       mainShowEvents($option,$view);
	   break;
	   
	   case 'mainlistevents':
	       mainListEvents($option,$view);
	   break;

	   case 'mainshowtasawaqs':
	       mainShowTasawaqs($option,$view);
	   break;
	   
	   case 'mainlisttasawaqs':
	       mainListTasawaqs($option,$view);
	   break;
	   
	   case 'mainshowcirculars':
	       mainShowCirculars($option,$view);
	   break;
	   
	   case 'mainlistcirculars':
	       mainListCirculars($option,$view);
	   break;
	   
	   case 'newdocument':
	      if($task == "edit.newdocument"){
		    saveNewDocument($option, $cid, $apply=0);
		  }elseif($task == "delnewdocument"){ 
		    deleteNewDocument($option, $cid);
		  }else{
	        newDocument($option,$view);
		  }
	   break;
	   
	   case 'listdocuments':
	       listDocuments($option,$view);
	   break;
	   
	   case 'newevent':
	      if($task == "edit.newevent"){
		    saveNewEvent($option, $cid, $apply=0);
		  }elseif($task == "delnewevent"){ 
		    deleteNewEvent($option, $cid);
		  }else{
	        newEvent($option,$view);
		  }
	   break;
	   
	   case 'listevents':
	       listEvents($option,$view);
	   break;
	   
	   case 'complaints':
	      if($task == "edit.complaints"){
		    saveComplaints($option, $cid, $apply=0);
		  }else{
	        newComplaints($option,$view);
		  }
	   break;
	   
	   case 'listcomplaints':
	       listComplaints($option,$view);
	   break;
	   
	   case 'detailcomplaints':
	       detailComplaints($option,$view);
	   break;
	   
	   case 'suggestions':
	      if($task == "edit.suggestions"){
		    saveSuggestions($option, $cid, $apply=0);
		  }else{
	        newSuggestions($option,$view);
		  }
	   break;
	   
	   case 'listsuggestions':
	       listSuggestions($option,$view);
	   break;
	   
	   case 'detailsuggestions':
	       detailSuggestions($option,$view);
	   break;
	   
	   case 'grievances':
	      if($task == "edit.grievances"){
		    saveGrievances($option, $cid, $apply=0);
		  }else{
	        newGrievances($option,$view);
		  }
	   break;
	   
	   case 'listgrievances':
	       listGrievances($option,$view);
	   break;
	   
	   case 'detailgrievances':
	       detailGrievances($option,$view);
	   break;
	   
	   case 'newtasawaq':
	      if($task == "edit.newtasawaq"){
		    saveNewTasawaq($option, $cid, $apply=0);
		  }elseif($task == "delnewtasawaq"){ 
		    deleteNewTasawaq($option, $cid);
		  }else{
	        newTasawaq($option,$view);
		  }
	   break;
	   
	   case 'listtasawaqs':
	       listTasawaqs($option,$view);
	   break;
	   
	   case 'newhires':
	      if($task == "edit.newhires"){
		    saveNewHires($option, $cid, $apply=0);
		  }elseif($task == "delnewhires"){ 
		    deleteNewHires($option, $cid);
		  }else{
	        newHires($option,$view);
		  }
	   break;
	   
	   case 'listnewhires':
	       listNewHires($option,$view);
	   break;
	   
	   case 'newmedia':
	      if($task == "edit.media"){
		    saveMedia($option, $cid, $apply=0);
		  }elseif($task == "delmedia"){ 
		    deleteMedia($option, $cid);
		  }else{
	        newMedia($option,$view);
		  }
	   break;
	   
	   case 'media':
	       myMedia($option,$view);
	   break;
	   
	   case 'listmedia':
	       listMedia($option,$view);
	   break;
	   
	   case 'newcircular':
	      if($task == "edit.circulars"){
		    saveCirculars($option, $cid, $apply=0);
		  }elseif($task == "delcircular"){ 
		    deleteCirculars($option, $cid);
		  }else{
	        newCircular($option,$view);
		  }
	   break;
	   
	   case 'listcirculars':
	       listCirculars($option,$view);
	   break;
	   
	   case 'myhomepage':
	       myHomePage($option,$view);
	   break;
	   
	   case 'mainmyhomepage':
	       mainMyHomePage($option,$view);
	   break;
	   
	   case 'mydepartment':
	       myDepartment($option,$view);
	   break;
	   
	   case 'listdepartment':
	       departmentList($option,$view);
	   break;
	   
	    case 'approvebookingrequest':
	       if($task == "approve.approvebookingrequest"){
		    approveBookingRequest($option, $cid, $apply=0);
		  }elseif($task == "reject.approvebookingrequest"){ 
		    rejectBookingRequest($option, $cid, $apply=0);
		  }else{
	        viewApproveBookingRequest($option,$view);
		  }	       
	   break;
	   
	   case 'viewbookingrequest':
	     if($task == "delbookingrequest"){ 
		    deleteBookingRequest($option, $cid);
		  }else{
	        viewBookingRequest($option,$view);
		  }	       
	   break;
	   
	   case 'bookingrequest':
	       if($task == "edit.bookingrequest"){
		    saveBookingRequest($option, $cid, $apply=0);
		  }else{
	        bookingRequest($option,$view);
		  }	       
	   break;
	   
	   case 'addUserBookingRequest':
		    addUserBookingRequest($option,$view);
	   break;		  
	   
	   case 'ajaxbookingrequest':
	        ajaxListbookingRequest($option,$view);     
	   break;
	   
       case 'listresources':
		    listResources($option,$view);
	   break;
	   
	   case 'calendar':
		    resourceCalendar($option,$view);
	   break;			

	   case 'upload':
		    viewUpload($option,$view);
	        break;
	   
	   case 'summary':
		    Summary($option);
		    break;

	   case 'finish':
		    finish($option);
		    break;

       case 'viewcategory':
       case 'category':
    		showOneCategory($option,$cid);
	       	break;

       case 'viewdownload':
       case 'view.download':            
       case 'download':                        
            showDownload($option,$cid);               
            break;

    /* case 'download':
            download($option,$cid);               
            break;
    */                   
       case 'search':
            showSearchForm($option,$cid);
            break;            
            
       case 'searchresult':
            showSearchResult($option,$cid);
            break;
            
       case 'report':
            reportDownload($option,$cid);
            break;
            
       case 'viewcategories':
	   case 'categories':       
     		showCats($option,$cid);
            break;

       case 'editor.insert.file':
            editorInsertFile('com_eposts');
            break;
            
       case 'edit':
            require_once(JPATH_COMPONENT.'/eposts.edit.php');
            editFile($option, $cid);
            break; 
            
       case 'save':
            require_once(JPATH_COMPONENT.'/eposts.edit.php');
            saveFile($option, $cid);
            break; 
               
	   default: showCats($option,$cid);
            break;

       case 'weathernprayer':
            weathernprayer($option,$view);
            break;
			case 'weathernprayertest':
				weathernprayertest($option,$view);
				break;
		case 'updateuserstate':
            updateuserstate($option,$view);
			break;
}

function showiframe($option,$view){
global $jlistConfig, $Itemid, $mainframe, $page_title;
	
	$database		= &JFactory::getDBO();
	$user			= &JFactory::getUser();
    $document		= &JFactory::getDocument();
	$language		= &JFactory::getLanguage();
	$breadcrumbs	= &$mainframe->getPathWay();
	jlist_HTML::showiframe($option, $user, $document->language);
	
}//showiframe

function iframeTarger($option,$view){
global $jlistConfig, $Itemid, $mainframe, $page_title;

	$database		= &JFactory::getDBO();
	$user			= &JFactory::getUser();
    $document		= &JFactory::getDocument();
	$language		= &JFactory::getLanguage();
	$breadcrumbs	= &$mainframe->getPathWay();
	
	jlist_HTML::iframeTarger($option, $user, $document->language);
	
}//iframeTarger

function showloadimage($option,$view){
	global $jlistConfig, $Itemid, $mainframe;
	//file://hqdintdev2/media/albumSSSSS1.png
				$config =& JFactory::getConfig();
                $filepath = $config->getValue( 'config.file_path' );
	
	$filename = JRequest::getString('myfile', '');
	
	//echo $download_path = strstr($filename, ':\\') ? $filename : $filepath . "\\" . $filename;
	//$download_path = "\\HQDINTDB1\PortalStorage\eposts\4.jpg"; //strstr($filename, ':\\') ? $filename : $filename;
	$download_path = strstr($filename, ':\\') ? $filename : $filename;
	$filename = explode('\\', $filename);
	$count = count($filename);
    $filename = $outputfile == '' ? $filename[$count-1] : $outputfile;
	
    // Detect missing filename
    if(!$filename) die("I'm sorry, you must specify a file name to download.");
   
    // Make sure we can't download files above the current directory location.
    if(eregi("\.\.", $filename)) die("I'm sorry, you may not download that file.");
    $file = str_replace("..", "", $filename);
   
    // Make sure we can't download .ht control files.
    if(eregi("\.ht.+", $filename)) die("I'm sorry, you may not download that file.");
   
    // Combine the download path and the filename to create the full path to the file.
   $file = "$download_path";
   
   
    // Test to ensure that the file exists.
    if(!file_exists($file)) die("I'm sorry, the file doesn't seem to exist.");
   
    // Extract the type of file which will be sent to the browser as a header
    $type = filetype($file);
   
    // Get a date and timestamp
    $today = date("F j, Y, g:i a");
    $time = time();

    // Send file headers
    header("Content-type: $type");
    header("Content-Disposition: attachment;filename=$filename");
    header('Pragma: no-cache');
    header('Expires: 0');
	//header('Content-type: image/jpeg', true);

    // Send the file contents.
    readfile($file);
	exit;
	
	$myfile = JRequest::getString('myfile', '');
	//$myfile = '//hqdintdb1/PortalStorage/eposts/27-03-2013_1.jpg';
	$myfile = str_replace("\\", "/", $myfile);
	$myfile2 = str_replace("/", "\\", $myfile);;
if(empty($myfile)) exit;
$contents = '';
$extar = explode('.',$myfile);
$totalext = (count($extar) - 1);
$ext = strtolower($extar[$totalext]);
		
/*switch ($ext) {
  case 'jpg':
    $mime = 'image/jpeg';
    break;
  case 'gif':
    $mime = 'image/gif';
    break;
  case 'png':
    $mime = 'image/png';
    break;
  default: 
    $mime = 'image/jpeg';
}
if ($mime) {
  header('Content-type: '.$mime);
header('Content-length: '.filesize($myfile));
}
$file = @fopen($myfile, 'rb');
if ($file) {
  fpassthru($file);
  exit; 
} */
    /*if (file_exists($myfile))
    {*/
		//return 'file:'.$myfile; //failed
		//echo 'file:'.$myfile; //failed
		//exit;
	    // Note: You should probably do some more checks 
        // on the filetype, size, etc.
		
        //$contents = file_get_contents('file:'.$myfile);
		
		$contents = file_get_contents('file:'.$myfile);

    /*} else {
	   //$contents = file_get_contents('C:\\Clients\\C4311\\goshooble.com\\public_html\\images\\thumb_video.gif');
	    //header('Content-type: image/gif');
		//exit;
	}*/
	
        // Note: You should probably implement some kind 
        // of check on filetype
		if(empty($contents)){
		  $contents = file_get_contents($myfile2);	
		}
		
		//$extar = explode('.',$myfile);
		//$totalext = (count($extar) - 1);
        //$ext = strtolower($extar[$totalext]);
		/*if($ext == 'gif')
		   header('Content-type: image/gif', true);
		elseif($ext == 'png')
		   header('Content-type: image/png', true);
		else*/
          header('Content-type: image/jpeg', true);
	    echo $contents;
	
	exit;
}//end showloadimage

function updateuserstate($option,$view) {
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	
	$database		= &JFactory::getDBO();
	$user			= &JFactory::getUser();
    $document		= &JFactory::getDocument();
	$language		= &JFactory::getLanguage();
	$breadcrumbs	= &$mainframe->getPathWay();
	
	$database->setQuery("SELECT * FROM #__users");
    $rows = $database->loadObjectList();
	
	$crrDate = date("Y-m-d H:i:s", strtotime("-60 days"));
	$UserStateQuery = "";
	
	foreach ($rows as $row){
		$lastvsit = $row->lastvisitDate;
		$UserState = 0;
		if ($crrDate < $lastvsit) {
			$UserState = 0;
		} else {
			$UserState = 1;
		}
		$UserStateQuery = "UPDATE `#__users` SET `block` = '".$UserState."' WHERE `id` = '".$row->id."'";
		$database->setQuery($UserStateQuery);
		if($database->query()) {
			print 'p <hr>';
		}
	}
	print "hello";
}

function weathernprayertest($option,$view){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	
	$database		= &JFactory::getDBO();
	$user			= &JFactory::getUser();
    $document		= &JFactory::getDocument();
	$language		= &JFactory::getLanguage();
	$breadcrumbs	= &$mainframe->getPathWay();


$weather_url['dubai'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in%20%28select

%20woeid%20from%20geo.places%281%29%20where%20text=%27dubai-1940345%27%29&format=json";
	$weather_url['abudhabi'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27abu-dhabi-1940330%27%29&format=json";
	$weather_url['sharjah'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27sharjah-1940119%27%29&format=json";
	$weather_url['ajman'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27ajman-1940332%27%29&format=json";
	$weather_url['rakhaim'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27ras-al-khaimah-1940263%27%29&format=json";
	$weather_url['uaquwain'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27umm-al-quwain-55922647%27%29&format=json";
	$weather_url['fujairah'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27fujairah-12517735%27%29&format=json";
	
	foreach($weather_url as $key => $urlxml) {
		$aContext = array(
			'http' => array(
				'proxy' => 'tcp://192.168.111.110:8080',
				'request_fulluri' => true,
			),
		);
		$cxContext = stream_context_create($aContext);
		
		//$result = file_get_contents($urlxml);
		 $result = file_get_contents($urlxml, False, $cxContext);
		
		$obj = json_decode($result);
		
		$forecast = $obj->query->results->channel->item->forecast;


	

		 $wdata["location"] = $obj->query->results->channel->location->city; 
		 $wdata["code"] = $obj->query->results->channel->item->condition->code;
		 $wdata["temp"] = $obj->query->results->channel->item->condition->temp;
		 $wdata["text"] = $obj->query->results->channel->item->condition->text;
		 $wdata["high"] = $forecast[0]->high;
		 $wdata["low"] = $forecast[0]->low;
		
	$query = $database->setQuery("UPDATE `#__eposts_wnpdata_test` SET `code` = '".$wdata["code"]."', `temp` = '".$wdata["temp"]."', `high` = '".$wdata

["high"]."', `low` = '".$wdata["low"]."', `text` = '".$wdata["text"]."' WHERE `type` = '1' AND `state` = '".$key."'");
		

		if($database->query()) {
			echo "success";
			//print 'w <hr>';
		}else {

		echo "error";
		}
	}


	
	// Prayers Code
	
	$prayer_url['dubai'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=dubai&state=03&zipcode=&latitude=25.2522&longitude=55.2800&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwilight2

=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['abudhabi'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=abu_dhabi&state=01&zipcode=&latitude=24.4667&longitude=54.3667&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwili

ght2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['sharjah'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=sharjah&state=06&zipcode=&latitude=25.3622&longitude=55.3911&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwiligh

t2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['ajman'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=ajman&state=00&zipcode=&latitude=25.4061&longitude=55.4428&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwilight2

=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['rakhaim'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=ras_al_khaimah&state=05&zipcode=&latitude=25.7911&longitude=55.9428&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajr

Twilight2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['uaquwain'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=umm_al_quwain&state=07&zipcode=&latitude=25.5000&longitude=55.7500&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrT

wilight2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['fujairah'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=fujairah&state=04&zipcode=&latitude=25.1333&longitude=56.2500&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwilig

ht2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	
	foreach($prayer_url as $key => $urlxml) {
		/*$opts = array(
					'http' => array(
						'proxy' => 'tcp://10.1.1.111:8080',
						'request_fulluri' => true,
						'header' => "Proxy-Authorization: Basic $auth"
					)
				);
		$context = stream_context_create($opts);
		
		$result = file_get_contents($params->get('dubai_pr'), false, $context);*/
		
		$aContext = array(
				'http' => array(
					'proxy' => 'tcp://192.168.111.110:8080',
					'request_fulluri' => true
				
			),
		);

		$cxContext = stream_context_create($aContext);
		 
		
		//$result = file_get_contents($urlxml);
	 		
		$result = file_get_contents($urlxml, False,$cxContext);
		//print_r($result); exit();
		

		$DOM = new DOMDocument();
		$DOM->loadHTML($result);
	
		$Header = $DOM->getElementsByTagName('tr');
		
		$Detail = $DOM->getElementsByTagName('td');
	
	 //#Get header name of the table

	foreach($Header as $NodeHeader) 
	{
		$aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
		
	}

	
	
	$prayerdata = array_values($aDataTableHeaderHTML);
	$hijri = $prayerdata[0];
	$hijri =  substr($hijri, 0,-1);
	//$hijri = preg_replace('/\s+/', '', $hijri);
	
	 $fajar = $prayerdata[3]; //bin
	 $fajar = substr($fajar , 5);
	 $fajar = substr($fajar , 0,-3);
	 $fajar = preg_replace('/\s+/', '', $fajar);
	
	$sunrise = $prayerdata[4]; 
	$sunrise = substr($sunrise , 7);
	$sunrise = substr($sunrise , 0,-3);
	$sunrise = preg_replace('/\s+/', '', $sunrise);
	
	$Dhuhr = $prayerdata[5]; 
	$Dhuhr = substr($Dhuhr , 5);
	$Dhuhr = substr($Dhuhr , 0,-3);
	$Dhuhr = preg_replace('/\s+/', '', $Dhuhr);
	
	$Asar = $prayerdata[6]; 
	$Asar = substr($Asar , 5);
	$Asar = substr($Asar , 0,-3);
	$Asar = preg_replace('/\s+/', '', $Asar);
	
	$Maghrib = $prayerdata[7];
	$Maghrib = substr($Maghrib, 7);
	$Maghrib = substr($Maghrib, 0,-3);
	$Maghrib = preg_replace('/\s+/', '', $Maghrib);
	
	$Esha = $prayerdata[8];
	$Esha =    substr($Esha , 5);
	$Esha =    substr($Esha , 0,-3);
	$Esha = preg_replace('/\s+/', '', $Esha);
	
	$query = $database->setQuery("UPDATE `#__eposts_wnpdata_test` SET `fajr` = '".$fajar."', `sunrise` = '".$sunrise."', `dhuhr` = '".$Dhuhr."', `asr` = '".

$Asar."', `maghrib` = '".$Maghrib."', `isha` = '".$Esha."', `hijri` = '".$hijri."' WHERE `type` = '1' AND `state` = '".$key."'");
		if($database->query()) {
			echo "succes";
			print 'p <hr>';
		}

	$aDataTableHeaderHTML = "";
 

 			
		//print_r( $result); 
		//print "<hr/>";
		

		//$xml = simplexml_load_string($result);
		
		//$data = $xml->children();

	 
	/*$query = $database->setQuery("UPDATE `#__eposts_wnpdata` SET `fajr` = '".$data->fajr."', `sunrise` = '".$data->sunrise."', `dhuhr` = '".$data-

>dhuhr."', `asr` = '".$data->asr."', `maghrib` = '".$data->maghrib."', `isha` = '".$data->isha."', `hijri` = '".$data->hijri."' WHERE `type` = '1' AND 

`state` = '".$key."'");
		
	print_r($query); 
		if($database->query()) {
			echo "Success";
			print 'p <hr>';
		}else {
			echo "failur";
		}*/
	}

}

function weathernprayer($option,$view) {
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

	global $jlistConfig, $Itemid, $mainframe, $page_title;
	
	$database		= &JFactory::getDBO();
	$user			= &JFactory::getUser();
    $document		= &JFactory::getDocument();
	$language		= &JFactory::getLanguage();
	$breadcrumbs	= &$mainframe->getPathWay();


$weather_url['dubai'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in%20%28select

%20woeid%20from%20geo.places%281%29%20where%20text=%27dubai-1940345%27%29&format=json";
	$weather_url['abudhabi'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27abu-dhabi-1940330%27%29&format=json";
	$weather_url['sharjah'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27sharjah-1940119%27%29&format=json";
	$weather_url['ajman'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27ajman-1940332%27%29&format=json";
	$weather_url['rakhaim'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27ras-al-khaimah-1940263%27%29&format=json";
	$weather_url['uaquwain'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27umm-al-quwain-55922647%27%29&format=json";
	$weather_url['fujairah'] = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20u=%27c%27%20and%20woeid%20in

%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%27fujairah-12517735%27%29&format=json";
	
	foreach($weather_url as $key => $urlxml) {
		$aContext = array(
			'http' => array(
				'proxy' => 'tcp://192.168.111.110:8080',
				'request_fulluri' => true,
			),
		);
		$cxContext = stream_context_create($aContext);
		
		//$result = file_get_contents($urlxml);
		 $result = file_get_contents($urlxml, False, $cxContext);
		
		$obj = json_decode($result);
		
		$forecast = $obj->query->results->channel->item->forecast;


	

		 $wdata["location"] = $obj->query->results->channel->location->city; 
		 $wdata["code"] = $obj->query->results->channel->item->condition->code;
		 $wdata["temp"] = $obj->query->results->channel->item->condition->temp;
		 $wdata["text"] = $obj->query->results->channel->item->condition->text;
		 $wdata["high"] = $forecast[0]->high;
		 $wdata["low"] = $forecast[0]->low;
		
	$query = $database->setQuery("UPDATE `#__eposts_wnpdata` SET `code` = '".$wdata["code"]."', `temp` = '".$wdata["temp"]."', `high` = '".$wdata

["high"]."', `low` = '".$wdata["low"]."', `text` = '".$wdata["text"]."' WHERE `type` = '1' AND `state` = '".$key."'");
		

		if($database->query()) {
			echo "success";
			//print 'w <hr>';
		}else {

		echo "error";
		}
	}


	
	// Prayers Code
	
	$prayer_url['dubai'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=dubai&state=03&zipcode=&latitude=25.2522&longitude=55.2800&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwilight2

=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['abudhabi'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=abu_dhabi&state=01&zipcode=&latitude=24.4667&longitude=54.3667&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwili

ght2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['sharjah'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=sharjah&state=06&zipcode=&latitude=25.3622&longitude=55.3911&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwiligh

t2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['ajman'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=ajman&state=00&zipcode=&latitude=25.4061&longitude=55.4428&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwilight2

=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['rakhaim'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=ras_al_khaimah&state=05&zipcode=&latitude=25.7911&longitude=55.9428&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajr

Twilight2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['uaquwain'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=umm_al_quwain&state=07&zipcode=&latitude=25.5000&longitude=55.7500&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrT

wilight2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	$prayer_url['fujairah'] = "http://www.islamicfinder.org/prayer_service.php?

country=united_arab_emirates&city=fujairah&state=04&zipcode=&latitude=25.1333&longitude=56.2500&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwilig

ht2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml";
	
	foreach($prayer_url as $key => $urlxml) {
		/*$opts = array(
					'http' => array(
						'proxy' => 'tcp://10.1.1.111:8080',
						'request_fulluri' => true,
						'header' => "Proxy-Authorization: Basic $auth"
					)
				);
		$context = stream_context_create($opts);
		
		$result = file_get_contents($params->get('dubai_pr'), false, $context);*/
		
		$aContext = array(
				'http' => array(
					'proxy' => 'tcp://192.168.111.110:8080',
					'request_fulluri' => true
				
			),
		);

		$cxContext = stream_context_create($aContext);
		 
		
		//$result = file_get_contents($urlxml);
	 		
		$result = file_get_contents($urlxml, False,$cxContext);
		//print_r($result); exit();
		

		$DOM = new DOMDocument();
		$DOM->loadHTML($result);
	
		$Header = $DOM->getElementsByTagName('tr');
		
		$Detail = $DOM->getElementsByTagName('td');
	
	 //#Get header name of the table

	foreach($Header as $NodeHeader) 
	{
		$aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
		
	}

	
	
	$prayerdata = array_values($aDataTableHeaderHTML);
	$hijri = $prayerdata[0];
	$hijri =  substr($hijri, 0,-1);
	//$hijri = preg_replace('/\s+/', '', $hijri);
	
	 $fajar = $prayerdata[3]; //bin
	 $fajar = substr($fajar , 5);
	 $fajar = substr($fajar , 0,-3);
	 $fajar = preg_replace('/\s+/', '', $fajar);
	
	$sunrise = $prayerdata[4]; 
	$sunrise = substr($sunrise , 7);
	$sunrise = substr($sunrise , 0,-3);
	$sunrise = preg_replace('/\s+/', '', $sunrise);
	
	$Dhuhr = $prayerdata[5]; 
	$Dhuhr = substr($Dhuhr , 5);
	$Dhuhr = substr($Dhuhr , 0,-3);
	$Dhuhr = preg_replace('/\s+/', '', $Dhuhr);
	
	$Asar = $prayerdata[6]; 
	$Asar = substr($Asar , 5);
	$Asar = substr($Asar , 0,-3);
	$Asar = preg_replace('/\s+/', '', $Asar);
	
	$Maghrib = $prayerdata[7];
	$Maghrib = substr($Maghrib, 7);
	$Maghrib = substr($Maghrib, 0,-3);
	$Maghrib = preg_replace('/\s+/', '', $Maghrib);
	
	$Esha = $prayerdata[8];
	$Esha =    substr($Esha , 5);
	$Esha =    substr($Esha , 0,-3);
	$Esha = preg_replace('/\s+/', '', $Esha);
	
	$query = $database->setQuery("UPDATE `#__eposts_wnpdata` SET `fajr` = '".$fajar."', `sunrise` = '".$sunrise."', `dhuhr` = '".$Dhuhr."', `asr` = '".

$Asar."', `maghrib` = '".$Maghrib."', `isha` = '".$Esha."', `hijri` = '".$hijri."' WHERE `type` = '1' AND `state` = '".$key."'");
		if($database->query()) {
			echo "succes";
			print 'p <hr>';
		}

	$aDataTableHeaderHTML = "";
 

 			
		//print_r( $result); 
		//print "<hr/>";
		

		//$xml = simplexml_load_string($result);
		
		//$data = $xml->children();

	 
	/*$query = $database->setQuery("UPDATE `#__eposts_wnpdata` SET `fajr` = '".$data->fajr."', `sunrise` = '".$data->sunrise."', `dhuhr` = '".$data-

>dhuhr."', `asr` = '".$data->asr."', `maghrib` = '".$data->maghrib."', `isha` = '".$data->isha."', `hijri` = '".$data->hijri."' WHERE `type` = '1' AND 

`state` = '".$key."'");
		
	print_r($query); 
		if($database->query()) {
			echo "Success";
			print 'p <hr>';
		}else {
			echo "failur";
		}*/
	}
}

// show summary
function Summary($option){
	global $jlistConfig, $Itemid, $mainframe;
    //$session = JFactory::getSession();
    //$session->set('jd_sec_check', 1);
    
    $app = &JFactory::getApplication();
    $user = &JFactory::getUser();
    $user_access = checkAccess_JD();
    $users_access =  (int)substr($user_access, 0, 1);
    
    $database = &JFactory::getDBO();
    // AUP support
    $sum_aup_points = 0;
    $extern_site = false;
    $open_in_blank_page = false;
    $marked_files_id = array();
    $has_licenses = false;
    $must_confirm = false;
    $license_text = '';
    // get file-id from the marked files - when used
    $marked_files_id = JArrayHelper::getValue( $_POST, 'cb_arr', array(0));
    for($i=0,$n=count($marked_files_id);$i<$n;$i++){
        $marked_files_id[$i] = intval($marked_files_id[$i]);
    }
    // get file id
    $fileid = intval(JArrayHelper::getValue( $_REQUEST, 'cid', 0 ));
    // get cat id
    $catid = intval(JArrayHelper::getValue( $_REQUEST, 'catid', 0 ));
    
    // get departments access
    $user_is_in_departments = getUserDepartmentsX();
    $user_is_in_departments_arr = explode(',', $user_is_in_departments);
    
    // check access for manual url manipulation - fix
    $database->setQuery('SELECT * FROM #__eposts_cats WHERE published = 1 AND cat_id = '.$catid);
    $cat = $database->loadObject();
    $access[0] = (int)substr($cat->cat_access, 0, 1);
    $access[1] = (int)substr($cat->cat_access, 1, 1); 
    
    if ($users_access < $access[1] || !$cat){
        if (!in_array($cat->cat_department_access, $user_is_in_departments_arr)){
            // jump to login
            if (!$user->id) {
                $app->redirect('index.php?option=com_users&view=login');
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            }    
        }    
    }
    
    $breadcrumbs =& $mainframe->getPathWay();
    if ($catid){
        $breadcrumbs = createPathway($catid, $breadcrumbs, $option);
        $breadcrumbs->addItem($cat->cat_title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=viewcategory&catid='.$catid));     
    }
    if ($fileid){
        $database->setQuery("SELECT * FROM #__eposts_files WHERE published = 1 AND file_id = '$fileid' AND cat_id = '$catid'");
        if (!$file = $database->loadObject()){
            // jump to login
            if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            } 
        }    
        $breadcrumbs->addItem($file->file_title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=viewdownload&catid='.$catid.'&cid='.$fileid));
        
    }
    $breadcrumbs->addItem(JText::_('COM_EPOSTS_FRONTEND_HEADER_SUMMARY_TITLE'), '');    
    
    // is mirror file ?
    $is_mirror =  intval(JArrayHelper::getValue( $_REQUEST, 'm', 0 ));
 
    // when exists - no checkbox was used
    if ($fileid){
        $direktlink = true;
        $id_text = $fileid;        
        $filename = JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=finish&cid='.$fileid.'&catid='.$catid.'&m='.$is_mirror);
        if ($file->license && $file->license_agree) $must_confirm = true;
        $download_link = $filename;
        $file_title = ' - '.$file->file_title;       
    }    
        
    // move in text for view the files list
    $anz = 0;
    if (!$id_text){
        $anz = count($marked_files_id);
        if ( $anz > 1 ){
           $id_text = implode(',', $marked_files_id);
        } else {
           $id_text = $marked_files_id[0];
        }
    }

    // get filetitle and release for mail and summary
    $mail_files_arr = array();
    $mail_files = "<div><ul>";
    $database->setQuery("SELECT * FROM #__eposts_files WHERE published = 1 AND file_id IN ($id_text) ");
    if (!$mail_files_arr = $database->loadObjectList()){
         // jump to login
         if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
         } 
    }    
    
    if ($jlistConfig['use.alphauserpoints']){
        // get standard points value from AUP
        $database->setQuery("SELECT points FROM #__alpha_userpoints_rules WHERE published = 1 AND plugin_function = 'plgaup_eposts_user_download'");
        $aup_default_points = (int)$database->loadResult(); 
    }    
     
    for ($i=0; $i<count($mail_files_arr); $i++) {

       // build sum of aup points
       if ($jlistConfig['use.alphauserpoints']){
          if ($jlistConfig['use.alphauserpoints.with.price.field']){
              $sum_aup_points = $sum_aup_points + (int)$mail_files_arr[$i]->price;
          } else {
              $sum_aup_points += $aup_default_points;
          }    
       }

       // get license name
       if ($mail_files_arr[$i]->license > 0){  
           if ($mail_files_arr[$i]->license && $mail_files_arr[$i]->license_agree) $must_confirm = true;
           $lic = $mail_files_arr[$i]->license;
           $has_licenses = true;
           $database->setQuery("SELECT * FROM #__eposts_license WHERE id = '$lic'");
           $license = $database->loadObject(); 
           if ($must_confirm) $license_text = stripslashes($license->license_text);
           if ($license->license_url){
               // add license link
               // a little pic for extern links
               $extern_url_pic = '<img src="'.JURI::base().'components/com_eposts/assets/images/link_extern.gif" alt="" />';
               $mail_files .= "<div><li><b>".$mail_files_arr[$i]->file_title.' '.$mail_files_arr[$i]->release.'&nbsp;&nbsp;&nbsp;</b>'.JText::_('COM_EPOSTS_FE_DETAILS_LICENSE_TITLE').': <b><a href="'.$license->license_url.'" target="_blank">'.$license->license_title.'</a> '.$extern_url_pic.' &nbsp;&nbsp;&nbsp;</b>'.JText::_('COM_EPOSTS_FE_DETAILS_FILESIZE_TITLE').': <b>'.$mail_files_arr[$i]->size.'</b></li></div>';
           } else {
               $mail_files .= "<div><li><b>".$mail_files_arr[$i]->file_title.' '.$mail_files_arr[$i]->release.'&nbsp;&nbsp;&nbsp;</b>'.JText::_('COM_EPOSTS_FE_DETAILS_LICENSE_TITLE').': <b>'.$license->license_title.'&nbsp;&nbsp;&nbsp;</b>'.JText::_('COM_EPOSTS_FE_DETAILS_FILESIZE_TITLE').': <b>'.$mail_files_arr[$i]->size.'</b></li></div>';
           }   
       } else {
           $mail_files .= "<div><li><b>".$mail_files_arr[$i]->file_title.' '.$mail_files_arr[$i]->release.'&nbsp;&nbsp;&nbsp;</b>'.JText::_('COM_EPOSTS_FE_DETAILS_FILESIZE_TITLE').': <b>'.$mail_files_arr[$i]->size.'</b></li></div>';
       }     
    }
    $mail_files .= "</ul></div>";
    
    // set flag when link must opened in a new browser window 
    if (!$is_mirror && $i == 1 && $mail_files_arr[0]->extern_site){
        $extern_site = true;    
    }
    if ($is_mirror == 1 && $i == 1 && $mail_files_arr[0]->extern_site_mirror_1){
        $extern_site = true;    
    }
    if ($is_mirror == 2 && $i == 1 && $mail_files_arr[0]->extern_site_mirror_2){
        $extern_site = true;    
    }
    // get file extension  when only one file selected - set flag when link must opened in a new browser window 
    if (count($marked_files_id) == 1 && $mail_files_arr[0]->url_download) {
        $view_types = array();
        $view_types = explode(',', $jlistConfig['file.types.view']);
        $fileextension = strtolower(substr(strrchr($mail_files_arr[0]->url_download,"."),1));
        if (in_array($fileextension, $view_types)){
            $open_in_blank_page = true;
        }
    }
        
    // when mass download with checkboxes
    if (!$direktlink){ 
        // more as one file is selected - zip it in a temp file
        $download_verz = JURI::base().$jlistConfig['files.uploaddir'].'/';
        $zip_verz = JPATH_SITE.'/'.$jlistConfig['files.uploaddir'].'/';
        if (count($marked_files_id) > 1) {
            // build random value for zip filename
            if (empty($user_random_id)){
                $user_random_id = buildRandomID();
            }
            $zip=new ss_zip();
            for ($i=0; $i<count($marked_files_id); $i++) {
                // get file url
                $database->setQuery("SELECT url_download, cat_id, file_title FROM #__eposts_files WHERE file_id = '".(int)$marked_files_id[$i]."'");
                $file_data = $database->loadObject();
                $filename = $file_data->url_download;
                $file_title = $file_title.' - '.$file_data->file_title;
                $cat_id = $file_data->cat_id; 
                $database->setQuery("SELECT cat_dir FROM #__eposts_cats WHERE cat_id = '$cat_id'");
                $cat_dir = $database->loadResult();
                $cat_dir = $cat_dir.'/'; 
                $zip->add_file($zip_verz.$cat_dir.$filename, $filename);
            }
            $zip->archive(); // return the ZIP
            $zip->save($zip_verz."tempzipfiles/".$jlistConfig['zipfile.prefix'].$user_random_id.".zip");
            $zip_size = fsize($zip_verz."tempzipfiles/".$jlistConfig['zipfile.prefix'].$user_random_id.".zip");
            $zip_file_info = JText::_('COM_EPOSTS_FRONTEND_SUMMARY_ZIP_FILESIZE').': <b>'.$zip_size.'</b>';
            
            // delete older zip files
            $del_ok = deleteOldFile($zip_verz."tempzipfiles/");
            $filename = $download_verz."tempzipfiles/".$jlistConfig['zipfile.prefix'].$user_random_id.".zip";
            $download_link = JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=finish&catid='.$cat_id.'&list='.$id_text.'&user='.$user_random_id); 
        } else {
            // only one file selected
            $database->setQuery("SELECT cat_id, file_title FROM #__eposts_files WHERE file_id = '".(int)$marked_files_id[0]."'");
            $cat_id = $database->loadObject();
            $filename = JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=finish&cid='.(int)$marked_files_id[0].'&catid='.$cat_id->cat_id);
            $download_link = $filename;
            $file_title = ' - '.$cat_id->file_title;
        }
    }
    $sum_aup_points = ABS($sum_aup_points);            
    jlist_HTML::Summary($option, $marked_files_id, $mail_files, $filename, $download_link, $del_ok, $extern_site, $sum_aup_points, $has_licenses, $open_in_blank_page, $must_confirm, $license_text, $zip_file_info, $file_title);
}

// finish and start the download
function finish($option){
	global $mainframe, $jlistConfig, $mail_files, $Itemid;
   
   $app = &JFactory::getApplication();
   $user = &JFactory::getUser();
   $coreUserDepartments = $user->getAuthorisedDepartments();
   $user_access = checkAccess_JD();
   $users_access =  (int)substr($user_access, 0, 1);
   
   
   $database = &JFactory::getDBO();
   $extern = false;
   $extern_site = false;
   $can_download = false;
   $price = '';
    
   // anti lecching
   $url = JURI::base( false );
   list($remove,$stuff2)=split('//',$url,2);
   list($domain,$stuff2)=split('/',$stuff2,2); 
   $domain = str_replace('www.', '', $domain); 
   
   $refr=getenv("HTTP_REFERER");
   list($remove,$stuff)=split('//',$refr,2);
   list($home,$stuff)=split('/',$stuff,2);
   $home = str_replace('www.', '', $home); 
   
   // check leeching
   $blocking = false; 
   if ($home != $domain) {
       $allowed_urls = explode(',' , $jlistConfig['allowed.leeching.sites']);
       if ($jlistConfig['check.leeching']) {
           if ($jlistConfig['block.referer.is.empty']) {
               if (!$refr) {
                   $blocking = true;
               }
           } else {
               if  (!$refr){
                   $blocking = false;
               }    
           }    
           
           if (in_array($home,$allowed_urls)) {
              $blocking = false;
           } else {
             $blocking = true;        
           }  
       } 
   }

   // check blacklist
   if ($jlistConfig['use.blocking.list'] && $jlistConfig['blocking.list'] != '') {
       $user_ip = getRealIp();
       if (stristr($jlistConfig['blocking.list'], $user_ip)){
           $blocking = true;
       }    
   }
  
    if ($blocking) {
        // leeching message
        echo '<div align ="center"><br /><b><font color="red">'.JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE').'</font></b><br /></div>';
        echo '<div align ="center"><br />'.JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE2').'<br /></div>';
    
    } else {
    
    // get file id
    $fileid = intval(JArrayHelper::getValue( $_REQUEST, 'cid', 0 ));
    // get cat id
    $catid = intval(JArrayHelper::getValue( $_REQUEST, 'catid', 0 ));
    
    $is_mirror = intval(JArrayHelper::getValue( $_REQUEST, 'm', 0 ));
    
    // get departments access
    $user_is_in_departments = getUserDepartmentsX();
    $user_is_in_departments_arr = explode(',', $user_is_in_departments);    
    
    // check access for manually url manipulation - fix
    $database->setQuery('SELECT * FROM #__eposts_cats WHERE published = 1 AND cat_id = '.$catid);
    $cat = $database->loadObject();
    
    $access[0] = (int)substr($cat->cat_access, 0, 1);
    $access[1] = (int)substr($cat->cat_access, 1, 1);
    
    if ($users_access < $access[1] || !$cat){
        if (!in_array($cat->cat_department_access, $user_is_in_departments_arr)){
            // jump to login
            if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            }    
        }    
    }

    // set page for the redirect after download button click
    if (!$Itemid){
        $database->setQuery("SELECT id from #__menu WHERE link = 'index.php?option=com_eposts&view=viewcategories' and published = 1");
        $Itemid = $database->loadResult();
    }   
    $redirect_to = JRoute::_( "index.php?option=com_eposts&Itemid=".$Itemid."&view=viewcategory&catid=".$catid); 
    
    // get AUP user points
    $api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
    if (file_exists($api_AUP)){
        require_once ($api_AUP);
        $profil = AlphaUserPointsHelper:: getUserInfo('', $user->id);
    }
    if ($jlistConfig['use.alphauserpoints']){
        // get standard points value from AUP
        $database->setQuery("SELECT points FROM #__alpha_userpoints_rules WHERE published = 1 AND plugin_function = 'plgaup_eposts_user_download'");
        $aup_fix_points = (int)$database->loadResult();
        $aup_fix_points = abs($aup_fix_points);
    }    
    
    // files liste holen wenn mhr als ein download markiert
    $files = $database->getEscaped (JRequest::getString('list', '' ));
    $files_arr = explode(',', $files);
    if ($files){
        // sammeldownload
        $user_random_id = intval(JRequest::getString('user', 0 ));
        $download_verz = JPATH_SITE.'/'.$jlistConfig['files.uploaddir'].'/'; 
        $filename = $download_verz.'tempzipfiles/'.$jlistConfig['zipfile.prefix'].$user_random_id.'.zip'; 
        $filename_direct = JURI::base().$jlistConfig['files.uploaddir'].'/tempzipfiles/'.$jlistConfig['zipfile.prefix'].$user_random_id.'.zip';
        // check whether direct access
        $database->setQuery('SELECT file_id, file_title FROM #__eposts_files WHERE published = 1 AND file_id IN ('.$files.')');
        if (!$rows = $database->loadObjectList()){
            // jump to login
            if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            } 
        }    

        // add AUP points
        if ($jlistConfig['use.alphauserpoints']){
            if ($jlistConfig['use.alphauserpoints.with.price.field']){
                $database->setQuery("SELECT SUM(price) FROM #__eposts_files WHERE file_id IN ($files)");
                $sum_points = (int)$database->loadResult();
                if ($profil->points >= $sum_points){
                    foreach($rows as $aup_data){
                        $database->setQuery("SELECT price FROM #__eposts_files WHERE file_id = '$aup_data->file_id'");
                        if ($price = (int)$database->loadResult()){
                            $can_download = setAUPPointsDownloads($user->id, $aup_data->file_title, $aup_data->file_id, $price);
                        }
                    }
                }
            
            } else {
                // use fix points
                $sum_points = $aup_fix_points * count($files_arr);
                if ($profil->points >= $sum_points){
                    foreach($rows as $aup_data){
                        $can_download = setAUPPointsDownloads($user->id, $aup_data->file_title, $aup_data->file_id, 0);
                    }
                } else {
                    $can_download = false;
                }    
            }
       
        } else {
            // no AUP active
            $can_download = true;
        }
        if ($jlistConfig['user.can.download.file.when.zero.points'] && $user->id){
            $can_download = true;
        }    
        if (!$can_download){
            $aup_no_points = '<div style="text-align:center" class="jd_div_aup_message">'.stripslashes($jlistConfig['user.message.when.zero.points']).'</div>'. 
            '<div style="text-align:center" class="jd_div_aup_message">'.JText::_('COM_EPOSTS_BACKEND_SET_AUP_FE_MESSAGE_NO_DOWNLOAD_POINTS').' '.(int)$profil->points.'<br />'.JText::_('COM_EPOSTS_BACKEND_SET_AUP_FE_MESSAGE_NO_DOWNLOAD_NEEDED').' '.JText::_($sum_points).'</div>'.
            '<div style="text-align:left" class="back_button"><a href="javascript:history.go(-1)">'.JText::_('COM_EPOSTS_FRONTEND_BACK_BUTTON').'</a></div>';
            echo $aup_no_points;
        }
        // download limits
        // check the log - can user download the file?
        $may_download = false;
        foreach ($files_arr as $file){
            $may_download = checkLog($file, $user);
        }    
        if (!$may_download){
            // download not possible
            $datenow =& JFactory::getDate(); 
            $date = $datenow->toFormat("%Y-%m-%d %H:%m");
            $back .= '<div style="text-align:left" class="back_button"><a href="javascript:history.go(-1)">'.JText::_('COM_EPOSTS_FRONTEND_BACK_BUTTON').'</a></div>'; 
            echo '<div style="text-align:center" class="jd_limit_reached_message">'.stripslashes($jlistConfig['limited.download.reached.message']).' '.$date.'</div>'.$back;         
        }
        
    } else {
        // einzelner download
        // check whether direct access
        $database->setQuery("SELECT file_title FROM #__eposts_files WHERE published = 1 AND file_id = '$fileid' AND cat_id = '$catid'");
        if (!$ok = $database->loadResult()){
            // jump to login
            if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            } 
        }    
        
        // download limits
        // check the log - can user download the file?
        $may_download = false;
        $may_download = checkLog($fileid, $user);
        if (!$may_download){
            // download not possible
            $datenow =& JFactory::getDate(); 
            $date = $datenow->toFormat("%Y-%m-%d %H:%m");
            $back .= '<div style="text-align:left" class="back_button"><a href="javascript:history.go(-1)">'.JText::_('COM_EPOSTS_FRONTEND_BACK_BUTTON').'</a></div>'; 
            echo '<div style="text-align:center" class="jd_limit_reached_message">'.stripslashes($jlistConfig['limited.download.reached.message']).' '.$date.'</div>'.$back;         
        } else {        

           // get filename and build path
           if (!$is_mirror){
               $database->setQuery("SELECT url_download FROM #__eposts_files WHERE file_id = '".(int)$fileid."'");
               $file_url = $database->loadResult();
               if ($file_url){
                   $database->setQuery("SELECT cat_dir FROM #__eposts_cats WHERE cat_id = '".(int)$catid."'");
                   $cat_dir = $database->loadResult();
                   $filename = JPATH_SITE.'/'.$jlistConfig['files.uploaddir'].'/'.$cat_dir.'/'.$file_url;
                   $filename_direct = JURI::base().$jlistConfig['files.uploaddir'].'/'.$cat_dir.'/'.$file_url;        
               } else {
                   $database->setQuery("SELECT * FROM #__eposts_files WHERE file_id = '".(int)$fileid."'");
                   $result = $database->loadObjectlist();
                   $filename = $result[0]->extern_file; 
                   if ($result[0]->extern_site){
                       $extern_site = true;
                   }
                   $extern = true;
               }
           } else {
             // is mirror 
             $database->setQuery("SELECT * FROM #__eposts_files WHERE file_id = '".(int)$fileid."'");
             $result = $database->loadObjectlist();
             if ($is_mirror == 1){
                 $filename = $result[0]->mirror_1; 
                 if ($result[0]->extern_site_mirror_1){
                     $extern_site = true;
                 }
             } else {
                 $filename = $result[0]->mirror_2; 
                 if ($result[0]->extern_site_mirror_2){
                     $extern_site = true;
                 }
             }
             $extern = true;    
           }      
           // AUP integration
           if ($jlistConfig['use.alphauserpoints.with.price.field'] && $jlistConfig['use.alphauserpoints']){
               $database->setQuery("SELECT price FROM #__eposts_files WHERE file_id = '".(int)$fileid."'");
               $price = (int)$database->loadResult();
           } else {
               if ($jlistConfig['use.alphauserpoints']){
                   $price = $aup_fix_points;
               }        
           }    
            
           $can_download = setAUPPointsDownload($user->id, $ok, $fileid, $price);
           if ($jlistConfig['user.can.download.file.when.zero.points'] && $user->id){
               $can_download = true;
           }    
           if (!$can_download){
               // get AUP user data
               $profil = AlphaUserPointsHelper:: getUserInfo ( '', $user->id );
               $aup_no_points = '<div style="text-align:center" class="jd_div_aup_message">'.stripslashes($jlistConfig['user.message.when.zero.points']).'</div>'.
               '<div style="text-align:center" class="jd_div_aup_message">'.JText::_('COM_EPOSTS_BACKEND_SET_AUP_FE_MESSAGE_NO_DOWNLOAD_POINTS').' '.(int)$profil->points.'<br />'.JText::_('COM_EPOSTS_BACKEND_SET_AUP_FE_MESSAGE_NO_DOWNLOAD_NEEDED').' '.JText::_($price).'</div>'. 
               '<div style="text-align:left" class="back_button"><a href="javascript:history.go(-1)">'.JText::_('COM_EPOSTS_FRONTEND_BACK_BUTTON').'</a></div>';
               echo $aup_no_points;
           } 
        }   
    }    
    // run download
    if ($can_download && $may_download){
        // send mail
        if ($jlistConfig['send.mailto.option'] == '1') {
            if ($fileid){
                sendMail($fileid);  
            } else {
                sendMail($files);               
            }    
        }
        // give uploader AUP points when is set on
        if ($jlistConfig['use.alphauserpoints']){
            setAUPPointsDownloaderToUploader($fileid, $files);  
        }
        
        // update downloads hits
        if ($files){
            $database->setQuery('UPDATE #__eposts_files SET downloads=downloads+1 WHERE file_id IN ('.$files.')'); 
            $database->query();    
        } else {
            if ($fileid){
                $database->setQuery("UPDATE #__eposts_files SET downloads=downloads+1 WHERE file_id = '".(int)$fileid."'");
                $database->query();
            }    
        }
            
	    // start download
        $x = download($filename, $filename_direct, $extern, $extern_site, $redirect_to);
    }    
    if ($x == 2) {
        // files not exists
        echo '<div align ="center"><br /><b><font color="#990000">'.JText::_('COM_EPOSTS_FRONTEND_FILE_NOT_FOUND_MESSAGE').'</font></b><br /><br /></div>';         
    }
  }    
}

// download starten
function download($file, $filename_direct, $extern, $extern_site, $redirect_to){
     global $jlistConfig, $mainframe;

    $app = &JFactory::getApplication(); 
    
    $view_types = array();
    $view_types = explode(',', $jlistConfig['file.types.view']); 
    clearstatcache(); 
    // existiert file - wenn nicht error
    if (!$extern){
        if (!file_exists($file)) { 
            return 2;
        } else {
            $len = filesize($file);
        }    
    } else {   
         $len = urlfilesize($file); 
    }
    
    // if url go to other website - open it in a new browser window
    if ($extern_site){
        echo "<script>document.location.href='$file';</script>\n";  
        exit;   
    }    
    
    // if set the option for direct link to the file
    if (!$jlistConfig['use.php.script.for.download']){
        if (empty($filename_direct)) {
            $app->redirect($file);
        } else {
            $app->redirect($filename_direct);
        }
    } else {    
        $filename = basename($file);
        $file_extension = strtolower(substr(strrchr($filename,"."),1));
        $ctype = datei_mime($file_extension);
        ob_end_clean();
        // needed for MS IE - otherwise content disposition is not used?
        if (ini_get('zlib.output_compression')){
            ini_set('zlib.output_compression', 'Off');
        }
        
        header("Cache-Control: public, must-revalidate");
        header('Cache-Control: pre-check=0, post-check=0, max-age=0');
        // header("Pragma: no-cache");  // Problems with MS IE
        header("Expires: 0"); 
        header("Content-Description: File Transfer");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-Type: " . $ctype);
        header("Content-Length: ".(string)$len);
        if (!in_array($file_extension, $view_types)){
            header('Content-Disposition: attachment; filename="'.$filename.'"');
        } else {
          // view file in browser
          header('Content-Disposition: inline; filename="'.$filename.'"');
        }   
        header("Content-Transfer-Encoding: binary\n");
        // redirect to category when it is set the time
        if (intval($jlistConfig['redirect.after.download']) > 0){ 
            header( "refresh:".$jlistConfig['redirect.after.download']."; url=".$redirect_to );
        }    
        
        // set_time_limit doesn't work in safe mode
        if (!ini_get('safe_mode')){ 
            @set_time_limit(0);
        }
        @readfile($file);
    }
    exit;
}

function datei_mime($filetype) {
    
    switch ($filetype) {
        case "ez":  $mime="application/andrew-inset"; break;
        case "hqx": $mime="application/mac-binhex40"; break;
        case "cpt": $mime="application/mac-compactpro"; break;
        case "doc": $mime="application/msword"; break;
        case "bin": $mime="application/octet-stream"; break;
        case "dms": $mime="application/octet-stream"; break;
        case "lha": $mime="application/octet-stream"; break;
        case "lzh": $mime="application/octet-stream"; break;
        case "exe": $mime="application/octet-stream"; break;
        case "class": $mime="application/octet-stream"; break;
        case "dll": $mime="application/octet-stream"; break;
        case "oda": $mime="application/oda"; break;
        case "pdf": $mime="application/pdf"; break;
        case "ai":  $mime="application/postscript"; break;
        case "eps": $mime="application/postscript"; break;
        case "ps":  $mime="application/postscript"; break;
        case "xls": $mime="application/vnd.ms-excel"; break;
        case "ppt": $mime="application/vnd.ms-powerpoint"; break;
        case "wbxml": $mime="application/vnd.wap.wbxml"; break;
        case "wmlc": $mime="application/vnd.wap.wmlc"; break;
        case "wmlsc": $mime="application/vnd.wap.wmlscriptc"; break;
        case "vcd": $mime="application/x-cdlink"; break;
        case "pgn": $mime="application/x-chess-pgn"; break;
        case "csh": $mime="application/x-csh"; break;
        case "dvi": $mime="application/x-dvi"; break;
        case "spl": $mime="application/x-futuresplash"; break;
        case "gtar": $mime="application/x-gtar"; break;
        case "hdf": $mime="application/x-hdf"; break;
        case "js":  $mime="application/x-javascript"; break;
        case "nc":  $mime="application/x-netcdf"; break;
        case "cdf": $mime="application/x-netcdf"; break;
        case "swf": $mime="application/x-shockwave-flash"; break;
        case "tar": $mime="application/x-tar"; break;
        case "tcl": $mime="application/x-tcl"; break;
        case "tex": $mime="application/x-tex"; break;
        case "texinfo": $mime="application/x-texinfo"; break;
        case "texi": $mime="application/x-texinfo"; break;
        case "t":   $mime="application/x-troff"; break;
        case "tr":  $mime="application/x-troff"; break;
        case "roff": $mime="application/x-troff"; break;
        case "man": $mime="application/x-troff-man"; break;
        case "me":  $mime="application/x-troff-me"; break;
        case "ms":  $mime="application/x-troff-ms"; break;
        case "ustar": $mime="application/x-ustar"; break;
        case "src": $mime="application/x-wais-source"; break;
        case "zip": $mime="application/x-zip"; break;
        case "au":  $mime="audio/basic"; break;
        case "snd": $mime="audio/basic"; break;
        case "mid": $mime="audio/midi"; break;
        case "midi": $mime="audio/midi"; break;
        case "kar": $mime="audio/midi"; break;
        case "mpga": $mime="audio/mpeg"; break;
        case "mp2": $mime="audio/mpeg"; break;
        case "mp3": $mime="audio/mpeg"; break;
        case "aif": $mime="audio/x-aiff"; break;
        case "aiff": $mime="audio/x-aiff"; break;
        case "aifc": $mime="audio/x-aiff"; break;
        case "m3u": $mime="audio/x-mpegurl"; break;
        case "ram": $mime="audio/x-pn-realaudio"; break;
        case "rm":  $mime="audio/x-pn-realaudio"; break;
        case "rpm": $mime="audio/x-pn-realaudio-plugin"; break;
        case "ra":  $mime="audio/x-realaudio"; break;
        case "wav": $mime="audio/x-wav"; break;
        case "pdb": $mime="chemical/x-pdb"; break;
        case "xyz": $mime="chemical/x-xyz"; break;
        case "bmp": $mime="image/bmp"; break;
        case "gif": $mime="image/gif"; break;
        case "ief": $mime="image/ief"; break;
        case "jpeg": $mime="image/jpeg"; break;
        case "jpg": $mime="image/jpeg"; break;
        case "jpe": $mime="image/jpeg"; break;
        case "png": $mime="image/png"; break;
        case "tiff": $mime="image/tiff"; break;
        case "tif": $mime="image/tiff"; break;
        case "wbmp": $mime="image/vnd.wap.wbmp"; break;
        case "ras": $mime="image/x-cmu-raster"; break;
        case "pnm": $mime="image/x-portable-anymap"; break;
        case "pbm": $mime="image/x-portable-bitmap"; break;
        case "pgm": $mime="image/x-portable-graymap"; break;
        case "ppm": $mime="image/x-portable-pixmap"; break;
        case "rgb": $mime="image/x-rgb"; break;
        case "xbm": $mime="image/x-xbitmap"; break;
        case "xpm": $mime="image/x-xpixmap"; break;
        case "xwd": $mime="image/x-xwindowdump"; break;
        case "msh": $mime="model/mesh"; break;
        case "mesh": $mime="model/mesh"; break;
        case "silo": $mime="model/mesh"; break;
        case "wrl": $mime="model/vrml"; break;
        case "vrml": $mime="model/vrml"; break;
        case "css": $mime="text/css"; break;
        case "asc": $mime="text/plain"; break;
        case "txt": $mime="text/plain"; break;
        case "gpg": $mime="text/plain"; break;
        case "rtx": $mime="text/richtext"; break;
        case "rtf": $mime="text/rtf"; break;
        case "wml": $mime="text/vnd.wap.wml"; break;
        case "wmls": $mime="text/vnd.wap.wmlscript"; break;
        case "etx": $mime="text/x-setext"; break;
        case "xsl": $mime="text/xml"; break;
        case "flv": $mime="video/x-flv"; break;
        case "mpeg": $mime="video/mpeg"; break;
        case "mpg": $mime="video/mpeg"; break;
        case "mpe": $mime="video/mpeg"; break;
        case "qt":  $mime="video/quicktime"; break;
        case "mov": $mime="video/quicktime"; break;
        case "mxu": $mime="video/vnd.mpegurl"; break;
        case "avi": $mime="video/x-msvideo"; break;
        case "movie": $mime="video/x-sgi-movie"; break;
        case "asf": $mime="video/x-ms-asf"; break;
        case "asx": $mime="video/x-ms-asf"; break;
        case "wm":  $mime="video/x-ms-wm"; break;
        case "wmv": $mime="video/x-ms-wmv"; break;
        case "wvx": $mime="video/x-ms-wvx"; break;
        case "ice": $mime="x-conference/x-cooltalk"; break;
        case "rar": $mime="application/x-rar"; break;
        default:    $mime="application/octet-stream"; break; 
    }
    return $mime;
}


// frontend upload form anzeigen
function viewUpload($option, $view){
    global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
  
    // view only when category exist
    $database->SetQuery('SELECT COUNT(*) FROM #__eposts_cats WHERE published = 1');
    $cat_sum = $database->loadResult();
    if (!$cat_sum) {
       echo JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_CATS_EXIST');             
       
    } else { 
        $breadcrumbs =& $mainframe->getPathWay();
        $breadcrumbs->addItem(JText::_('COM_EPOSTS_FRONTEND_UPLOAD_PAGE_TITLE'), JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=upload'));
               
		jlist_HTML::viewUpload($option, $view);
    }
}

// show only one category
function showOneCategory($option, $cid) {
    global $mainframe, $limit, $limitstart, $site_aktuell, $jlistConfig, $Itemid, $jlistTemplates;

    JHTML::_('behavior.modal');
    $breadcrumbs =& $mainframe->getPathWay();
    $database = &JFactory::getDBO();
    $app = &JFactory::getApplication();   
    $catid = (int)JRequest::getString('catid', 0);
    $user = &JFactory::getUser();
   
    $access = checkAccess_JD();
    if ($user->id > 0){
        $user_is_in_departments = getUserDepartmentsX();
    } else {
        $user_is_in_departments = 0;
    }    
    $user_departments = '';   
    if ($user_is_in_departments) $user_departments = "AND cat_department_access IN ($user_is_in_departments)";
    
    // cat laden
	if ($user_is_in_departments){
        $database->setQuery("SELECT * FROM #__eposts_cats WHERE published = 1 AND cat_id = '$catid' AND ( cat_access <= '$access' OR cat_department_access IN ($user_is_in_departments))");
    } else {
        $database->setQuery("SELECT * FROM #__eposts_cats WHERE published = 1 AND cat_id = '$catid' AND cat_access <= '$access'");
    }    
    if (!$cat = $database->loadObjectList()){
            // jump to login
            if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            } 
    }
	if ($cat[0]->cat_access == '99'){
        // access only for departments and admins
        if (!$user_is_in_departments && !$access == '99'){
            if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            }      
        }    
        if (!$access == '99'){
            $database->setQuery("SELECT * FROM #__eposts_cats WHERE published = 1 AND cat_id = '$catid' $user_departments "); 
            if (!$cat = $database->loadObjectList()){
                // jump to login
                if (!$user->id) {
                    $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
                } else {
                    echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                    exit();  
                } 
            }
        }    
    }    

    // actualise pathway 
    $breadcrumbs = createPathway($catid, $breadcrumbs, $option);
    $breadcrumbs->addItem($cat[0]->cat_title, '');
    
    if(empty($cat)){
		$cat[0] = new jlist_cats($database);
		$cat[0]->cat_id = 0;
		$cat[0]->cat_title = JText::_('COM_EPOSTS_FRONTEND_NOCAT');
    } else {
            // load subcats 
            if ($user_is_in_departments) $user_departments = "OR cat_department_access IN ($user_is_in_departments)"; 
            
            // sort order as set in config
            $cat_sort_field = 'ordering';
            $cat_sort = '';
            if ($jlistConfig['cats.order'] == 1) {
                $cat_sort_field = 'cat_title';
            }
            if ($jlistConfig['cats.order'] == 2) {
                $cat_sort_field = 'cat_title';
                $cat_sort = 'DESC';
            }    
            
            $database->setQuery("SELECT * FROM #__eposts_cats WHERE parent_id = '$catid' AND published = 1 AND (cat_access <= '$access' $user_departments) ORDER BY $cat_sort_field $cat_sort");
            $subs = $database->loadObjectList();
        
            if ($subs) {
                $sum_subcats = array();
                $sum_subfiles = array();
                
                foreach($subs as $sub){
                    // summe für subcats und files der einzel cat holen
                    $files = 0;
                    $subcats = 0;
                    $database->setQuery("SELECT COUNT(*) FROM #__eposts_files WHERE cat_id = '$sub->cat_id' AND published = 1");
                    $sum = $database->loadResult();
                    $files = $files + $sum;
                    infos($sub->cat_id, $subcats, $files, $access, $user_departments);
                    $sum_subfiles[] = $files;
                    $sum_subcats[] = $subcats;
                }         
            }    

        // anzahl files ermitteln
        $database->setQuery("SELECT COUNT(*) FROM #__eposts_files WHERE cat_id = '$catid' AND published = 1");
        $total = $database->loadResult();

        if ( $total <= $limit ) {
            $limitstart = 0;
        }
        $sum_pages = ceil($total / $limit);
    
        // manipulation ungültiger werte abblocken
        if ($site_aktuell > $sum_pages || $limitstart > $total || $limitstart < 0){
            $limitstart = 0;
            $site_aktuell = 1; 
        }         

        // create page navigation
        jimport('joomla.html.pagination'); 
        $pageNav = new JPagination( $total, $limitstart, $limit ); 

        // load files in order by config 
        $order = $database->getEscaped(JRequest::getString('order',''));
        $dir   = $database->getEscaped(JRequest::getString('dir', ''));            
        $files = array();
        $files = getSortedFiles($catid, $limitstart, $limit, $order, $dir);
        $dir = $files[dir];
        unset($files['dir']);        
    }

    $columns = (int)$jlistTemplates[1][0]->cols;
     
    jlist_HTML::showOneCategory($option, $cat, $subs, $files, $catid, $total, $sum_pages, $limit, $limitstart, $sum_subcats, $sum_subfiles, $site_aktuell,
                                $access, $columns, $pageNav, $order, $dir);
}                                                                                              

// einzelnen download mit detaillierten infos anzeigen
function showDownload($option,$cid){
   global $mainframe, $jlistConfig, $Itemid;

    //$session = JFactory::getSession();
    //$session->set('jd_sec_check', 1); 

   JHTML::_('behavior.modal'); 
   $database = &JFactory::getDBO();
   $app = &JFactory::getApplication(); 
   $user = &JFactory::getUser();
   $coreUserDepartments = $user->getAuthorisedDepartments();
   
   $database->setQuery('SELECT * FROM #__eposts_files WHERE published = 1 AND file_id = '.(int)$cid);
   if (!$file = $database->loadObject()){
            // jump to login
            if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            } 
   } 
   
    $access = checkAccess_JD();
    if ($user->id > 0){
        $user_is_in_departments = getUserDepartmentsX();
    } else {
        $user_is_in_departments = 0;
    } 
    $user_departments = '';   
    $user_can_edit = false;
    
    if ($user_is_in_departments) $user_departments = "AND cat_department_access IN ($user_is_in_departments)";
    
    // cat laden
    if ($user_is_in_departments){
        $database->setQuery("SELECT * FROM #__eposts_cats WHERE published = 1 AND cat_id = '$file->cat_id' AND ( cat_access <= '$access' OR cat_department_access IN ($user_is_in_departments))"); 
    } else {   
        $database->setQuery("SELECT * FROM #__eposts_cats WHERE published = 1 AND cat_id = '$file->cat_id' AND cat_access <= '$access'"); 
    }
    if (!$cat = $database->loadObject()){
            // jump to login
            if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            } 
    }
    if ($cat->cat_access == '99' && !$access == '99'){
        // access only for departments
        if (!$user_is_in_departments){
            // jump to login
            if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            }      
        }    
        $database->setQuery("SELECT * FROM #__eposts_cats WHERE published = 1 AND cat_id = '$file->cat_id' $user_departments "); 
        if (!$cat = $database->loadObject()){
            // jump to login
            if (!$user->id) {
                $app->redirect(JRoute::_('index.php?option=com_users&view=login'));
            } else {
                echo "<script> alert('".JText::_('COM_EPOSTS_FRONTEND_ANTILEECH_MESSAGE')."'); window.history.go(-1); </script>\n";
                exit();  
            } 
        }
    }     
   

  // $access = checkAccess_JD();
   $access = array();
   $access[0] = (int)substr($cat->cat_access, 0, 1);
   $access[1] = (int)substr($cat->cat_access, 1, 1);

   // check edit link access                                                                                 
   if ($user->id > 0){
      if (($user->id == $file->submitted_by && $jlistConfig['uploader.can.edit.fe'] == '1') || (in_array(8,$coreUserDepartments)) || (in_array(7,$coreUserDepartments))){
          $user_can_edit = true;
      } else {   
          $user_can_edit = getUserEditDepartment();
      }
   }
         
   if ($user_can_edit){
       $session_data = array('id' => $user->id, 'file_id' => $file->file_id);
       $session = JFactory::getSession();
       $session->set('jd_edit_user', $session_data);
       
       $edit_pic = '<img src="'.JURI::base().'components/com_eposts/assets/images/edit.png" title="'.JText::_('COM_EPOSTS_BACKEND_TOOLBAR_EDIT').'" alt="'.JText::_('COM_EPOSTS_BACKEND_TOOLBAR_EDIT').'" />';   
       $edit_link = ' <a href="'.JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=edit&cid='.$file->file_id, false).'" rel="nofollow">'.$edit_pic.'</a>';
   } else {
       $edit_link = '';
   }    
   
   $breadcrumbs =& $mainframe->getPathWay();
   $breadcrumbs = createPathway($file->cat_id, $breadcrumbs, $option);
   $breadcrumbs->addItem($cat->cat_title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=viewcategory&catid='.$cat->cat_id, false));
   $breadcrumbs->addItem($file->file_title, '' ); 
   
   jlist_HTML::showDownload($option, $file, $cat, $access, $edit_link, $user_can_edit);   
}  
// show only categories
function showCats($option,$cid){
	global $jlistConfig, $limit, $limitstart, $site_aktuell, $mainframe, $jlistTemplates;
    
    $user = &JFactory::getUser();
    JHTML::_('behavior.modal');
    $limit = intval($jlistConfig['categories.per.side']);
    $database = &JFactory::getDBO(); 
	$breadcrumbs = $mainframe->getPathWay();
    // access
    $access = checkAccess_JD();
    // get departments access
    if ($user->id > 0){
        $user_is_in_departments = getUserDepartmentsX();
    } else {
        $user_is_in_departments = 0;
    } 
    
    if(is_array($cid)) $cid = 0;
	$parent_id = (int)JArrayHelper::getValue($_REQUEST,'parent_id',0);
	$where = '';
    $user_departments = '';
	
    if($cid) $where = ' AND cat_id='.$cid;
    if ($user_is_in_departments) $user_departments = "OR cat_department_access IN ($user_is_in_departments)"; 
	$database->SetQuery( "SELECT count(*)"
						. "\nFROM #__eposts_cats "
						. "\nWHERE published = 1 AND parent_id = 0 AND (cat_access <= '$access' $user_departments)"
						);
  	$total = $database->loadResult();
    if ( $total <= $limit ) {
        $limitstart = 0;
    }
  	$sum_pages = ceil($total / $limit);
    
    // manipulation ungültiger werte abblocken
    if ($site_aktuell > $sum_pages || $limitstart > $total || $limitstart < 0){
       $limitstart = 0;
       $site_aktuell = 1; 
    } 
    
    // reihenfolge wie in optionen gesetzt
    $cat_sort_field = 'ordering';
    $cat_sort = '';
    if ($jlistConfig['cats.order'] == 1) {
        $cat_sort_field = 'cat_title';
    }
    if ($jlistConfig['cats.order'] == 2) {
        $cat_sort_field = 'cat_title';
        $cat_sort = 'DESC';
    }    

    $database->setQuery("SELECT * FROM #__eposts_cats WHERE published = 1".$where." AND parent_id = 0 AND (cat_access <= '$access' $user_departments) ORDER BY $cat_sort_field $cat_sort LIMIT $limitstart, $limit");
    $cats = $database->loadObjectList();

    // create pgae navigation
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit ); 

	if(empty($cats)){
		$cats[0] = new jlist_cats($database);
		$cats[0]->cat_id = 0;
		$cats[0]->cat_title = JText::_('COM_EPOSTS_FRONTEND_NOCAT');
        $no_cats = true;
	} else {
        // gesamt download infos holen...
        $no_cats = false;
        $catlist = array();
        $query = "SELECT cat_id AS id, parent_id AS parent, cat_title AS name FROM #__eposts_cats WHERE published = 1 AND (cat_access <= '$access' $user_departments)";
        $database->setQuery( $query );
        $catlist = $database->loadObjectList();
        
        // gesamtanzahl cats inkl. subcats 
        $sum_all_cats = count($catlist);
         
        $sub_cats = array();
        $sub_files = array();  
        
        // summe für subcats und files der einzel cat holen
        foreach($cats as $cat){
            $files = 0;
            $subcats = 0;
            $database->setQuery("SELECT COUNT(*) FROM #__eposts_files WHERE published = 1 AND cat_id = '$cat->cat_id'");
            $sum = $database->loadResult();
            $files = $files + $sum;
            infos($cat->cat_id, $subcats, $files, $access, $user_departments);
            $sub_files[] = $files;
            $sub_cats[] = $subcats;
        }    
    } 
    $columns = (int)$jlistTemplates[1][0]->cols;
    if ($columns > 1 && strpos($jlistTemplates[1][0]->template_text, '{cat_title1}')){   
 	    jlist_HTML::showCatswithColumns($option, $cats, $total, $sum_pages, $limit, $limitstart, $site_aktuell, $sub_cats, $sub_files, $sum_all_cats, $columns, $no_cats, $pageNav);
    } else {   
        jlist_HTML::showCats($option, $cats, $total, $sum_pages, $limit, $limitstart, $site_aktuell, $sub_cats, $sub_files, $sum_all_cats, $no_cats, $pageNav);
    }    
}

function showSearchForm($option,$cid){
    global $mainframe;
    
   $breadcrumbs =& $mainframe->getPathWay();
   $breadcrumbs->addItem(JText::_('COM_EPOSTS_FRONTEND_SEARCH_TITLE'), JRoute::_('index.php?option='.$option));
    
    jlist_HTML::showSearchForm($option);
}    
            
function showSearchResult($option,$cid){
   global $mainframe, $Itemid;
                                                                                                      
   $breadcrumbs =& $mainframe->getPathWay();
   $breadcrumbs->addItem(JText::_('COM_EPOSTS_FRONTEND_SEARCH_TITLE'), JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=search'));
   $breadcrumbs->addItem(JText::_('COM_EPOSTS_FRONTEND_SEARCH_RESULT_TITLE'),'');
    
    jlist_HTML::showSearchResult($option);
}

/**
 * send mail to admin if config set
 */
function sendMail($files){
    global $jlistConfig;
    
    $user = &JFactory::getUser();
    $database = &JFactory::getDBO();
    $config =& JFactory::getConfig();
    $mailfrom = $config->getValue( 'mailfrom' );
    $mailfromname = $config->getValue( 'fromname' );
    
    // get filetitle and release for mail and summary
    $mail_files_arr = array();
    $mail_files = "<div><ul>";
    $database->setQuery("SELECT * FROM #__eposts_files WHERE file_id IN ($files) ");
    $mail_files_arr = $database->loadObjectList();
    
    for ($i=0; $i<count($mail_files_arr); $i++) {
       if ($mail_files_arr[$i]->license > 0){
           // get license name
           $lic = $mail_files_arr[$i]->license;
           $database->setQuery("SELECT license_title FROM #__eposts_license WHERE id = '$lic'");
           $lic_title = $database->loadResult(); 
           $mail_files .= "<div><li>".$mail_files_arr[$i]->file_title.' '.$mail_files_arr[$i]->release.'&nbsp;&nbsp;&nbsp;'.JText::_('COM_EPOSTS_FE_DETAILS_LICENSE_TITLE').': '.$lic_title.'&nbsp;&nbsp;&nbsp;'.JText::_('COM_EPOSTS_FE_DETAILS_FILESIZE_TITLE').': '.$mail_files_arr[$i]->size.'</li></div>';
       } else {
           $mail_files .= "<div><li>".$mail_files_arr[$i]->file_title.' '.$mail_files_arr[$i]->release.'&nbsp;&nbsp;&nbsp;'.JText::_('COM_EPOSTS_FE_DETAILS_FILESIZE_TITLE').': '.$mail_files_arr[$i]->size.'</li></div>';
       }  
    }
    $mail_files .= "</ul></div>";
 
    // get IP
    $ip = getRealIp();

    // date and time
    $timestamp = time();
    $date_time = date($jlistConfig['global.datetime'], $timestamp);

    $user_downloads = '<br />';

    // get user
    if ($user->get('id') == 0) {
       $user_name = JText::_('COM_EPOSTS_MAIL_DOWNLOADER_NAME_VISITOR');
       $user_department = JText::_('COM_EPOSTS_MAIL_DOWNLOADER_DEPARTMENT');
    } else {
       $user_name = $user->get('username');
       //$user_department = $user->get('usertype');
       $user_email = $user->get('email');
    }

    $jlistConfig['send.mailto'] = str_replace(' ', '', $jlistConfig['send.mailto']);
    $empfaenger = explode(';', $jlistConfig['send.mailto']);
    $betreff = $jlistConfig['send.mailto.betreff'];
    $html_format = true;

    $text = "";
    $text = stripslashes($jlistConfig['send.mailto.template.download']);
    $text = str_replace('{file_list}', $mail_files, $text);
    $text = str_replace('{ip_address}', $ip, $text);
    $text = str_replace('{user_name}', $user_name, $text);
    $text = str_replace('{user_department}', $user_department, $text);
    $text = str_replace('{date_time}', $date_time, $text);
    $text = str_replace('{user_email}', $user_email, $text);
    if (!$jlistConfig['send.mailto.html']){
        $html_format = false;
        $text = strip_tags($text);
    }
    $first_adress = array_shift($empfaenger);
    $success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
}    

function sendMailUploads($name, $mail, $url_download, $filetitle, $description){
    global $jlistConfig;

    $config =& JFactory::getConfig();
    $mailfrom = $config->getValue( 'mailfrom' );
    $mailfromname = $config->getValue( 'fromname' );

    $database = &JFactory::getDBO(); 
    // get IP
    $ip = getRealIp();
    // date and time
    $timestamp = time();
    $date_time = date($jlistConfig['global.datetime'], $timestamp);

    $jlistConfig['send.mailto.upload'] = str_replace(' ', '', $jlistConfig['send.mailto.upload']);
    $empfaenger = explode(';', $jlistConfig['send.mailto.upload']);
    $betreff = $jlistConfig['send.mailto.betreff.upload'];
    $html_format = true;

    $text = "";
    $text = stripslashes($jlistConfig['send.mailto.template.upload']);
    $text = str_replace('{name}', $name, $text);
    $text = str_replace('{ip}', $ip, $text);
    $text = str_replace('{mail}', $mail, $text);
    $text = str_replace('{file_title}', $filetitle, $text);
    $text = str_replace('{file_name}', $url_download, $text);
    $text = str_replace('{date}', $date_time, $text);
    $text = str_replace('{description}', $description, $text);
    if (!$jlistConfig['send.mailto.html.upload']){
        $html_format = false;
        $text = strip_tags($text);
    }
    $first_adress = array_shift($empfaenger);
    $success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
}      

/**
 * Builds configuration variable
 * @return jlistConfig
 */
function buildjlistConfig(){
	$database = &JFactory::getDBO();

	$jlistConfig = array();
	$database->setQuery("SELECT id, setting_name, setting_value FROM #__eposts_config");
	$jlistConfigObj = $database->loadObjectList();
	if(!empty($jlistConfigObj)){
		foreach ($jlistConfigObj as $jlistConfigRow){
			$jlistConfig[$jlistConfigRow->setting_name] = $jlistConfigRow->setting_value;
		}
	}
	return $jlistConfig;
}

/**
 * Build random downloader User-ID
 */
function buildRandomID(){
   mt_srand((double)microtime()*1000000);
   mt_getrandmax();
   $random_id = mt_rand();
   return $random_id;
}

/* Alle Dateien in "tempzipfiles" löschen
/  die älter als der in config angegebenen zeit sind
*/

function deleteOldFile($dir){
	global $jlistConfig;
    jimport('joomla.filesystem.file');
	
   $del_ok = false;
   $time = gettimeofday();
   foreach (glob($dir."*.*") as $datei) {
      if ( $time[sec] - date(filemtime($datei)) >= ($jlistConfig['tempfile.delete.time'] * 60) )
           $del_ok = JFile::delete($datei);
      }
    return $del_ok;
}

// Get active templates text
// @return jlistTemplates

function getTemplates(){
	$database = &JFactory::getDBO();

    $templates_values = array();

    for ($i=1;$i<6;$i++) {
	   $database->setQuery("SELECT * FROM #__eposts_templates WHERE template_typ = '$i' AND template_active = 1");
       $templates_values[$i] = $database->loadObjectList();
       // ist leer, kein layout aktiviert. versuchen standard zu aktivieren, sonst meldung
       if (empty($templates_values[$i])){
           $database->setQuery("SELECT id FROM #__eposts_templates WHERE template_typ = '$i' AND locked = 1");
           $id = $database->loadResultArray(0);
           if ($id){
                $database->setQuery("UPDATE #__eposts_templates SET template_active = 1 WHERE id = $id[0]");
                $result = $database->query();
                $database->setQuery("SELECT * FROM #__eposts_templates WHERE template_typ = '$i' AND template_active = 1");
                $templates_values[$i] = $database->loadObjectList();
           }
       }    
    }
    return $templates_values;
}

// get files in sortorder (see config)
function getSortedFiles($catid, $limitstart, $limit, $order, $dir) {
    global $jlistConfig;
    $database = &JFactory::getDBO();

  if (!$order || $order == 'default'){   
    switch ($jlistConfig['files.order']) {
    case '0':
        if (!$dir) $dir = 'asc';
        $database->setQuery("SELECT * FROM #__eposts_files WHERE cat_id = '$catid' AND published = 1 ORDER BY ordering ".$dir." LIMIT $limitstart, $limit");
        break;

    case '1':
        if (!$dir) $dir = 'desc';
        $database->setQuery("SELECT a.* FROM #__eposts_files AS a WHERE a.cat_id = '$catid' AND a.published = 1 ORDER BY a.date_added ".$dir." LIMIT $limitstart, $limit");
        break;

    case '2':
        if (!$dir) $dir = 'asc';
        $database->setQuery("SELECT a.* FROM #__eposts_files AS a WHERE a.cat_id = '$catid' AND a.published = 1 ORDER BY a.date_added ".$dir." LIMIT $limitstart, $limit");
        break;

    case '3':
        if (!$dir) $dir = 'asc';
        $database->setQuery("SELECT a.* FROM #__eposts_files AS a WHERE a.cat_id = '$catid' AND a.published = 1 ORDER BY a.file_title ".$dir." LIMIT $limitstart, $limit");
        break;

    case '4':
        if (!$dir) $dir = 'desc';
        $database->setQuery("SELECT a.* FROM #__eposts_files AS a WHERE a.cat_id = '$catid' AND a.published = 1 ORDER BY a.file_title ".$dir." LIMIT $limitstart, $limit");
        break;
    
    case '5':
        if (!$dir) $dir = 'desc';
        $database->setQuery("SELECT a.* FROM #__eposts_files AS a WHERE a.cat_id = '$catid' AND a.published = 1 ORDER BY a.update_active ".$dir.", a.modified_date ".$dir.", a.date_added ".$dir." LIMIT $limitstart, $limit");
        break;
    }
  } else {
      if ($order == 'name'){ 
          $database->setQuery("SELECT a.* FROM #__eposts_files AS a WHERE a.cat_id = '$catid' AND a.published = 1 ORDER BY a.file_title ".$dir." LIMIT $limitstart, $limit");
      } 
      if ($order == 'date'){ 
          $database->setQuery("SELECT a.* FROM #__eposts_files AS a WHERE a.cat_id = '$catid' AND a.published = 1 ORDER BY a.date_added ".$dir." LIMIT $limitstart, $limit");
      }
      if ($order == 'hits'){ 
          $database->setQuery("SELECT a.* FROM #__eposts_files AS a WHERE a.cat_id = '$catid' AND a.published = 1 ORDER BY a.downloads ".$dir." LIMIT $limitstart, $limit");
      }       
  }  
    $files = $database->loadObjectList();
    $files[dir] = $dir;
    return $files;
}

function fsize($file) {
        $a = array("B", "KB", "MB", "GB", "TB", "PB");

        $pos = 0;
        $size = filesize($file);
        while ($size >= 1024) {
                $size /= 1024;
                $pos++;
        }

        return round($size,2)." ".$a[$pos];
}

// build comp header
function makeHeader($header, $compo_text, $is_showcats, $is_one_cat, $sum_subs, $is_detail, $is_search, $is_upload, $is_summary,  $is_finish, $sum_pages, $limit, $total, $limitstart, $site_aktuell, $pageNav, $order, $dir) {
	global $jlistConfig, $jlistTemplates, $Itemid, $page_title, $cat_link_itemids, $upload_link_itemid, $search_link_itemid, $root_itemid;
    $header = '<table class="jd_cat_subheader" width="100%"><tr><td><b> '.$header.' </b></td><td width="30%" align="right"> </td></tr></table>'; 
    return $header;           
}

// build comp footer
function makeFooter($make_back_button, $is_showcats, $is_one_cat, $sum_pages, $limit, $limitstart, $site_aktuell, $pageNav, $is_summary, $is_detail) {
    global $Itemid, $jlistConfig, $jlistTemplates;
    $database = &JFactory::getDBO();
    $config =& JFactory::getConfig();
    $secret = $config->getValue( 'secret' );
    
	
    // get templates for footer
    if ($is_summary){
        $footer = $jlistTemplates[3][0]->template_footer_text;
    } elseif ($is_detail){
        $footer = $jlistTemplates[5][0]->template_footer_text;
    } elseif ($is_one_cat){
        $footer = $jlistTemplates[2][0]->template_footer_text;
    } else {
        // show all cats / overview
        $footer = $jlistTemplates[1][0]->template_footer_text;
    }
    
    // view page navigation bottom
    if ($jlistConfig['option.navigate.bottom']){ 
       if ($is_showcats || $is_one_cat) {
           $page_navi_links   = $pageNav->getPagesLinks(); 
           if ($page_navi_links){
               $page_navi_pages   = $pageNav->getPagesCounter();
               $page_navi_counter = $pageNav->getResultsCounter(); 
               $page_limit_box    = $pageNav->getLimitBox();  
           } 
           $footer = str_replace('{page_navigation}', $page_navi_links, $footer);
           $footer = str_replace('{page_navigation_results_counter}', $page_navi_counter, $footer);
           $footer = str_replace('{page_navigation_pages_counter}', $page_navi_pages, $footer);                

       } else {
           $footer = str_replace('{page_navigation}', '', $footer);
           $footer = str_replace('{page_navigation_results_counter}', '', $footer);
           $footer = str_replace('{page_navigation_pages_counter}', '', $footer);                
       }
    } else {
           $footer = str_replace('{page_navigation}', '', $footer);
           $footer = str_replace('{page_navigation_results_counter}', '', $footer);
           $footer = str_replace('{page_navigation_pages_counter}', '', $footer);                
    }
    
    // replace google adsense placeholder with script when active (also for footer tab)
    if ($jlistConfig['google.adsense.active'] && $jlistConfig['google.adsense.code'] != ''){
            $footer = str_replace( '{google_adsense}', stripslashes($jlistConfig['google.adsense.code']), $footer);
    } else {
            $footer = str_replace( '{google_adsense}', '', $footer);
    } 
      
    // footer text
    if ($jlistConfig['downloads.footer.text'] != '') {
        $footer_text = stripslashes($jlistConfig['downloads.footer.text']);
        if ($jlistConfig['google.adsense.active'] && $jlistConfig['google.adsense.code'] != ''){
            $footer_text = str_replace( '{google_adsense}', stripslashes($jlistConfig['google.adsense.code']), $footer_text);
        } else {    
            $footer_text = str_replace( '{google_adsense}', '', $footer_text);
        }    
        $footer .= $footer_text;
    }
    
    // back button
	if ($make_back_button && $jlistConfig['view.back.button']){
        $footer = str_replace('{back_link}', '<a href="javascript:history.go(-1)">'.JText::_('COM_EPOSTS_FRONTEND_BACK_BUTTON').'</a>', $footer); 
    } else {
        $footer = str_replace('{back_link}', '', $footer);
    }    
    
    if (strrev($jlistConfig['com']) != $secret){
        $power = 'Powered&nbsp;by&nbsp;Acrologix';
        $footer .= '';
	}     
	//$footer = '';
	return $footer;
}

function reportDownload($option,$cid){
    global $Itemid, $jlistConfig;
    
    $database = &JFactory::getDBO();
    $user = &JFactory::getUser();
    
    $config =& JFactory::getConfig();
    $mailfrom = $config->getValue( 'mailfrom' );
    $mailfromname = $config->getValue( 'fromname' );
    
    if ($jlistConfig['report.link.only.regged'] && !$user->guest || !$jlistConfig['report.link.only.regged']) { 
        $database->setQuery('SELECT file_title FROM #__eposts_files WHERE file_id = '.$cid.' AND published = 1');
        $title = $database->loadResult();
        if ($title){
            // send report
            $mailto_report = str_replace(' ', '', $jlistConfig['send.mailto.report']);
            $empfaenger = explode(';', $mailto_report);
            $betreff = JText::_('COM_EPOSTS_REPORT_FILE_MESSAGE_TITLE');                                   
            $html_format = true;
            $text = sprintf(JText::_('COM_EPOSTS_REPORT_FILE_MESSAGE_TEXT'), $title, $cid);
            $first_adress = array_shift($empfaenger);
            $success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, '',$empfaenger);
            if ($success){
                $message = '<div style="text-align:center" class="jd_cat_title"><br /><img src="'.JURI::base().'components/com_eposts/assets/images/summary.png" width="48" height="48" border="0" alt="" />'.JText::_('COM_EPOSTS_REPORT_FILE_MESSAGE_OK').'<br /><br /></div>';
            } else {
                $message = '<div style="text-align:center" class="jd_cat_title"><br /><img src="'.JURI::base().'components/com_eposts/assets/images/warning.png" width="48" height="48" border="0" alt="" />'.JText::_('COM_EPOSTS_REPORT_FILE_MESSAGE_ERROR').'<br /><br /></div>';
            }    
        } else {
                $message = '<div style="text-align:center" class="jd_cat_title"><br /><img src="'.JURI::base().'components/com_eposts/assets/images/warning.png" width="48" height="48" border="0" alt="" />'.JText::_('COM_EPOSTS_REPORT_FILE_MESSAGE_ERROR').'<br /><br /></div>';
        }    
        $message .= '<div style="text-align:left" class="back_button"><a href="javascript:history.go(-1)">'.JText::_('COM_EPOSTS_FRONTEND_BACK_BUTTON').'</a></div>'; 
        echo $message;
    }     
} 

function checkAccess_JD(){
    
    // special user department:
    // 3 = author
    // 4 = editor
    // 5 = publisher
    // 6 = manager
    // 7 = admin
    // 8 = super admin - super user
    
    $user = &JFactory::getUser();
    //$coreUserDepartments = $user->getAuthorisedDepartments(); //chheena
    // $coreViewLevels = $user->getAuthorisedViewLevels();
    $aid = max ($user->getAuthorisedViewLevels());
    
    $access = '';
    if ($aid == 1) $access = '02'; // public
    if ($aid == 2 || $aid > 3) $access = '11'; // regged or member from custom joomla department
    if ($aid == 3 || in_array(3,$coreUserDepartments) || in_array(4,$coreUserDepartments) || in_array(5,$coreUserDepartments) || in_array(6,$coreUserDepartments)) $access = '22'; // special user
    if (in_array(8,$coreUserDepartments) || in_array(7,$coreUserDepartments)){
        // is admin or super user
        $access = '99';
    }
    return $access;
}

function DatumsDifferenz_JD($Start,$Ende) {
    $Tag1=(int) substr($Start, 8, 2);
    $Monat1=(int) substr($Start, 5, 2);
    $Jahr1=(int) substr($Start, 0, 4);
    
    $Tag2=(int) substr($Ende, 8, 2);
    $Monat2=(int) substr($Ende, 5, 2);
    $Jahr2=(int) substr($Ende, 0, 4);

    if (checkdate($Monat1, $Tag1, $Jahr1)and checkdate($Monat2, $Tag2, $Jahr2)){
        $Datum1=mktime(0,0,0,$Monat1, $Tag1, $Jahr1);
        $Datum2=mktime(0,0,0,$Monat2, $Tag2, $Jahr2);

        $Diff=(Integer) (($Datum1-$Datum2)/3600/24);
        return $Diff;
    } else {
        return -1;
    }
} 

function infos($parent, &$subcats, &$files, $access, $user_departments) {
 $database = &JFactory::getDBO();
    // subcats holen
    $database->setQuery("SELECT * FROM #__eposts_cats WHERE parent_id = '$parent' AND published = 1 AND (cat_access <= '$access' $user_departments)");
    $rows = $database->loadObjectList();
    if ($database->getErrorNum()) {
        echo $database->stderr();
        return false;
    }
    if ($rows){
        foreach ($rows as $v) {
            $database->setQuery("SELECT COUNT(*) FROM #__eposts_files WHERE cat_id = '$v->cat_id' AND published = 1");
            $sum = $database->loadResult();
            $files = $files + $sum;
            $subcats++;
            // nach nächster ebene suchen
            infos($v->cat_id, $subcats, $files, $access, $user_departments);
        }
    }
}

// Dateigröße einer externen Datei ermitteln
function urlfilesize($url) {
    if (substr($url,0,4)=='http' || substr($url,0,3)=='ftp') {
        // for php 4 users
        if (!function_exists('get_headers')) {
            function get_headers($url, $format=0) {
                $headers = array();
                $url = parse_url($url);
                $host = isset($url['host']) ? $url['host'] : '';
                $port = isset($url['port']) ? $url['port'] : 80;
                $path = (isset($url['path']) ? $url['path'] : '/') . (isset($url['query']) ? '?' . $url['query'] : '');
                $fp = fsockopen($host, $port, $errno, $errstr, 3);
                if ($fp) {
                    $hdr = "GET $path HTTP/1.1\r\n";
                    $hdr .= "Host: $host \r\n";
                    $hdr .= "Connection: Close\r\n\r\n";
                    fwrite($fp, $hdr);
                    while (!feof($fp) && $line = trim(fgets($fp, 1024))) {
                        if ($line == "\r\n") break;
                        list($key, $val) = explode(': ', $line, 2);
                        if ($format)
                            if ($val) $headers[$key] = $val;
                            else $headers[] = $key;
                        else $headers[] = $line;
                    }
                    fclose($fp);
                    return $headers;
                }
                return false;
            }
        }
        $size = array_change_key_case(get_headers($url, 1),CASE_LOWER);
        $size = $size['content-length'];
        if (is_array($size)) { $size = $size[1]; }
    } else {
        $size = @filesize($url); 
    }
    return $size;    
} 

function create_new_thumb($picturepath) {
    global $jlistConfig;
    jimport('joomla.filesystem.folder');     
    $thumbpath = JPATH_SITE.'/images/eposts/screenshots/thumbnails/';
    if (!is_dir($thumbpath)){
        JFolder::create("$thumbpath", 0755);
    }    
    $newsize = $jlistConfig['thumbnail.size.width'];
    $thumbfilename = $thumbpath.basename($picturepath);
    if (file_exists($thumbfilename)){
       return true;
    }   
    
    /* Prüfen ob Datei existiert */
    if(!file_exists($picturepath)) {
        return false;
    }
    
    /* MIME-Typ auslesen */
    $size=getimagesize($picturepath);
    switch($size[2]) {
        case "1":
        $oldpic = imagecreatefromgif($picturepath);
        break;
        case "2":
        $oldpic = imagecreatefromjpeg($picturepath);
        break;
        case "3":
        $oldpic = imagecreatefrompng($picturepath);
        break;
        default:
        return false;
    }
    /* Alte Maße auslesen */
    $width = $size[0];
    $height = $size[1]; 

    $maxwidth = $jlistConfig['thumbnail.size.width'];
    $maxheight = $jlistConfig['thumbnail.size.height'];
    if ($width/$maxwidth > $height/$maxheight) {
        $newwidth = $maxwidth;
        $newheight = $maxwidth*$height/$width;
    } else {
        $newheight = $maxheight;
        $newwidth = $maxheight*$width/$height;
    }
     
    /* Neues Bild erstellen mit den neuen Maßen */
    $newpic = imagecreatetruecolor($newwidth,$newheight);
    /* Jetzt wird das Bild nur noch verkleinert */
    imagecopyresampled($newpic,$oldpic,0,0,0,0,$newwidth,$newheight,$width,$height); 
    // Bild speichern
    switch($size[2]) {
        case "1":    return imagegif($newpic, $thumbfilename);
        break;
        case "2":    return imagejpeg($newpic, $thumbfilename);
        break;
        case "3":    return imagepng($newpic, $thumbfilename);
        break;
    }
    //Bilderspeicher freigeben
    imagedestroy($oldpic);
    imagedestroy($newpic);
}

function create_new_image($picturepath) {
    global $jlistConfig;
    jimport('joomla.filesystem.folder'); 
    $thumbpath = JPATH_SITE.'/images/eposts/screenshots/';
    if (!is_dir($thumbpath)){
        JFolder::create("$thumbpath", 0755);
    }    
    $newsize = $jlistConfig['create.auto.thumbs.from.pics.image.width'];
    $thumbfilename = $thumbpath.basename($picturepath);
    if (file_exists($thumbfilename)){
       return true;
    }   
    
    /* Pruefen ob Datei existiert */
    if(!file_exists($picturepath)) {
        return false;
    }
    
    /* MIME-Typ auslesen */
    $size=getimagesize($picturepath);
    switch($size[2]) {
        case "1":
        $oldpic = imagecreatefromgif($picturepath);
        break;
        case "2":
        $oldpic = imagecreatefromjpeg($picturepath);
        break;
        case "3":
        $oldpic = imagecreatefrompng($picturepath);
        break;
        default:
        return false;
    }
    /* Alte Groesse auslesen */
    $width = $size[0];
    $height = $size[1]; 
    /* Neue Groesse errechnen */

    $maxwidth = $jlistConfig['create.auto.thumbs.from.pics.image.width'];
    $maxheight = $jlistConfig['create.auto.thumbs.from.pics.image.height'];
    if ($width/$maxwidth > $height/$maxheight) {
        $newwidth = $maxwidth;
        $newheight = $maxwidth*$height/$width;
    } else {
        $newheight = $maxheight;
        $newwidth = $maxheight*$width/$height;
    }

    $newpic = imagecreatetruecolor($newwidth,$newheight);
    imagealphablending($newpic,false);
    imagesavealpha($newpic,true);
    
    /* Jetzt wird das Bild nur noch verkleinert */
    imagecopyresampled($newpic,$oldpic,0,0,0,0,$newwidth,$newheight,$width,$height); 
    // Bild speichern
    switch($size[2]) {
        case "1":    return imagegif($newpic, $thumbfilename);
        break;
        case "2":    return imagejpeg($newpic, $thumbfilename);
        break;
        case "3":    return imagepng($newpic, $thumbfilename);
        break;
    }
    //Bilderspeicher freigeben
    imagedestroy($oldpic);
    imagedestroy($newpic);
}

// create thumnail from pdf file
function create_new_pdf_thumb($target_path, $only_name, $thumb_path, $screenshot_path){
    global $jlistConfig;    
    
    $pdf_thumb_file_name = '';
    
    if (extension_loaded('imagick')){ 
        // create small thumb
        $image = new Imagick($target_path.'[0]');
        $image -> setImageIndex(0);
        $image -> setImageFormat($jlistConfig['pdf.thumb.image.type']);
        $image -> scaleImage($jlistConfig['pdf.thumb.height'],$jlistConfig['pdf.thumb.width'],1);
        $pdf_thumb_file_name = $only_name.'.'.strtolower($jlistConfig['pdf.thumb.image.type']);
        $image->writeImage($thumb_path.$only_name.'.'.strtolower($jlistConfig['pdf.thumb.image.type']));
        $image->clear();
        $image->destroy();
        // create big thumb
        $image = new Imagick($target_path.'[0]');
        $image -> setImageIndex(0);
        $image -> setImageFormat($jlistConfig['pdf.thumb.image.type']);
        $image -> scaleImage($jlistConfig['pdf.thumb.pic.height'],$jlistConfig['pdf.thumb.pic.width'],1);
        $image->writeImage($screenshot_path.$only_name.'.'.strtolower($jlistConfig['pdf.thumb.image.type']));
        $image->clear();
        $image->destroy();    
    }
    return $pdf_thumb_file_name; 
}

function checkFileName($name){
    global $jlistConfig;
    if ($name) {
        // change to uppercase
        if ($jlistConfig['fix.upload.filename.uppercase']){
            $name = strtolower($name); 
        }            
        // change blanks
        if ($jlistConfig['fix.upload.filename.blanks']){
            $name = str_replace(' ', '_', $name);
        }
        if ($jlistConfig['fix.upload.filename.specials']){
            // change special chars
            $search  = array( 'ä', 'ü', 'ö', 'Ä', 'Ü', 'Ö', 'ß');
            $replace = array( 'ae', 'ue', 'oe', 'Ae', 'Ue', 'Oe', 'ss');
            for ($i=0; $i < count($search); $i++) { 
                $name = str_replace($search[$i], $replace[$i], $name);
            }    
            
            // remove invalid chars
            $file_extension = strrchr($name,".");
            $name_cleared = preg_replace('#[^A-Za-z0-9 _.-]#', '', $name);
            if ($name_cleared != $file_extension){
                $name = $name_cleared;
            }    
        }
        // anti hack .php.rar
        $name = str_replace('.php.', '.', $name);
        $name = str_replace('.php4.', '.', $name); 
        $name = str_replace('.php5.', '.', $name);
    }               
    return $name;    
}

Function createPathway($catid, $breadcrumbs, $option){
    global $mainframe, $Itemid;
    
    $database = &JFactory::getDBO();
    // cat laden
    $database->setQuery('SELECT * FROM #__eposts_cats WHERE published = 1 AND cat_id = '.$catid);
    $cat = $database->loadObjectList();

    $path = array();
    $values = array();
    while ($cat[0]->parent_id){
        $database->setQuery('SELECT cat_id, cat_title, cat_access, parent_id FROM #__eposts_cats WHERE published = 1 AND cat_id = '.$cat[0]->parent_id);
        $parent = $database->loadObject();
        if ($parent){
            array_unshift($path, $parent->cat_title.'|'.JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=viewcategory&catid='.$parent->cat_id)); 
        } else {
          $cat[0]->parent_id = 0;  
        } 
        $cat[0]->parent_id = $parent->parent_id;    
    }
    foreach($path as $pat){
       $values = explode ( '|', $pat );
       $breadcrumbs->addItem($values[0], $values[1]);    
    }
    return $breadcrumbs;
}
    
function getID3v2Tags($file,$blnAllFrames=0){
    if (file_exists($file)){
        $arrTag[_file]=$file;
        $fp=fopen($file,"rb");
        if($fp){
            $id3v2=fread($fp,3);
            if($id3v2=="ID3"){// a ID3v2 tag always starts with 'ID3'
                $arrTag[_ID3v2]=1;
                $arrTag[_version]=ord(fread($fp,1)).".".ord(fread($fp,1));// = version.revision
                fseek($fp,6);// skip 1 'flag' byte, because i don't need it :)
                unset($tagSize);
                for($i=0;$i<4;$i++){
                    $tagSize=$tagSize.base_convert(ord(fread($fp,1)),10,16);
                }
                $tagSize=hexdec($tagSize);
                if($tagSize>filesize($file)){
                    $arrTag[_error]=4;// = tag is bigger than file
                }
                fseek($fp,10);
                while(ereg("^[A-Z][A-Z0-9]{3}$",$frameName=fread($fp,4))){
                    unset($frameSize);
                    for($i=0;$i<4;$i++){
                        $frameSize=$frameSize.base_convert(ord(fread($fp,1)),10,16);
                    }
                    $frameSize=hexdec($frameSize);
                    if($frameSize>$tagSize){
                        $arrTag[_error]=5;// = frame is bigger than tag
                        break;
                    }
                    fseek($fp,ftell($fp)+2);// skip 2 'flag' bytes, because i don't need them :)
                    if($frameSize<1){
                        $arrTag[_error]=6;// = frame size is smaller then 1
                        break;
                    }
                    if($blnAllFrames==0){
                        if(!ereg("^T",$frameName)){// = not a text frame, they always starts with 'T'
                            unset($arrTag[$frameName]);
                            fseek($fp,ftell($fp)+$frameSize);// go to next frame
                            continue;// read next frame
                        }
                    }
                    $frameContent=fread($fp,$frameSize);
                    if(!$arrTag[$frameName]){
                        $arrTag[$frameName]=trim(utf8_encode($frameContent));// the frame content (always?) starts with 0, so it's better to remove it
                    }
                    else{// if there is more than one frame with the same name
                        $arrTag[$frameName]=$arrTag[$frameName]."~".trim($frameContent);
                    }
                }// while(ereg("^[A-Z0-9]{4}$",fread($fp,4)))
            }// if($id3v2=="ID3")
            else{
                $arrTag[_ID3v2]=0;// = no ID3v2 tag found
                $arrTag[_error]=3;// = no ID3v2 tag found
            }
        }// if($fp)
        else{
            $arrTag[_error]=2;// can't open file
        }
        fclose($fp);
    }// if(is_file($file) and eregi(".mp3$",$file)){
    else{
        $arrTag[_error]=1;// = file doesn't exists or isn't a mp3
    }
    // convert lenght
    if ($arrTag[TLEN] > 0){
        $arrTag[TLEN] = round(($arrTag[TLEN] / 1000)/60,2);
    }    
   
    return $arrTag;
}     

function getRatings($id){    
    global $mainframe, $jlistConfig;
    $app = &JFactory::getApplication();
    $user = &JFactory::getUser();
    $aid = max ($user->getAuthorisedViewLevels());
    $database = &JFactory::getDBO();
    $document=& JFactory::getDocument(); 
    $vote = array();
    $database->setQuery('SELECT * FROM #__eposts_rating WHERE file_id='. (int) $id);
    $vote = $database->loadObject();
    if ($vote->rating_count!=0){
            $result = number_format(intval($vote->rating_sum) / intval( $vote->rating_count ),2)*20;
    }    
    $rating_sum = intval($vote->rating_sum);
    $rating_count = intval($vote->rating_count);
    // rating only for registered?
    if (($jlistConfig['rating.only.for.regged'] && $aid > 1) || !$jlistConfig['rating.only.for.regged']) {
        $script='
        <!-- JW AJAX Vote Plugin v1.1 starts here -->
        <script type="text/javascript">
        var live_site = \''.JURI::base().'\';
        var jwajaxvote_lang = new Array();
        jwajaxvote_lang[\'UPDATING\'] = \''.JText::_('JDVOTE_UPDATING').'\';
        jwajaxvote_lang[\'THANKS\'] = \''.JText::_('JDVOTE_THANKS').'\';
        jwajaxvote_lang[\'ALREADY_VOTE\'] = \''.JText::_('JDVOTE_ALREADY_VOTE').'\';
        jwajaxvote_lang[\'VOTES\'] = \''.JText::_('JDVOTE_VOTES').'\';
        jwajaxvote_lang[\'VOTE\'] = \''.JText::_('JDVOTE_VOTE').'\';
        </script>
        <script type="text/javascript" src="'.JURI::base().'components/com_eposts/assets/rating/js/ajaxvote.php"></script>
        <!-- JW AJAX Vote Plugin v1.1 ends here -->
        ';    
        if(!$addScriptJWAjaxVote){ 
            $addScriptJWAjaxVote = 1;
            if($app->getCfg(caching)) {
                $html = $script;
            } else {
                $document->addCustomTag($script);
            }
        }        

        $html .='
        <!-- JW AJAX Vote Plugin v1.1 starts here -->
        <div class="jwajaxvote-inline-rating">
        <ul class="jwajaxvote-star-rating">
        <li id="rating'.$id.'" class="current-rating" style="width:'.$result.'%;"></li>
        <li><a href="javascript:void(null)" onclick="javascript:jwAjaxVote('.$id.',1,'.$rating_sum.','.$rating_count.');" title="1 '.JText::_('JDVOTE_STAR').' 5" class="one-star"></a></li>
        <li><a href="javascript:void(null)" onclick="javascript:jwAjaxVote('.$id.',2,'.$rating_sum.','.$rating_count.');" title="2 '.JText::_('JDVOTE_STARS').' 5" class="two-stars"></a></li>
        <li><a href="javascript:void(null)" onclick="javascript:jwAjaxVote('.$id.',3,'.$rating_sum.','.$rating_count.');" title="3 '.JText::_('JDVOTE_STARS').' 5" class="three-stars"></a></li>
        <li><a href="javascript:void(null)" onclick="javascript:jwAjaxVote('.$id.',4,'.$rating_sum.','.$rating_count.');" title="4 '.JText::_('JDVOTE_STARS').' 5" class="four-stars"></a></li>
        <li><a href="javascript:void(null)" onclick="javascript:jwAjaxVote('.$id.',5,'.$rating_sum.','.$rating_count.');" title="5 '.JText::_('JDVOTE_STARS').' 5" class="five-stars"></a></li>
        </ul>
        <div id="jwajaxvote'.$id.'" class="jwajaxvote-box">
        ';
    } else {
        // view only the results
        $html .='
        <!-- JW AJAX Vote Plugin v1.1 starts here -->
        <div class="jwajaxvote-inline-rating">
        <ul class="jwajaxvote-star-rating">
        <li id="rating'.$id.'" class="current-rating" style="width:'.$result.'%;"></li>
        <li><a href="javascript:void(null)" onclick="" title="1 '.JText::_('JDVOTE_STAR').' 5" class="one-star"></a></li>
        <li><a href="javascript:void(null)" onclick="" title="2 '.JText::_('JDVOTE_STARS').' 5" class="two-stars"></a></li>
        <li><a href="javascript:void(null)" onclick="" title="3 '.JText::_('JDVOTE_STARS').' 5" class="three-stars"></a></li>
        <li><a href="javascript:void(null)" onclick="" title="4 '.JText::_('JDVOTE_STARS').' 5" class="four-stars"></a></li>
        <li><a href="javascript:void(null)" onclick="" title="5 '.JText::_('JDVOTE_STARS').' 5" class="five-stars"></a></li>
        </ul>
        <div id="jwajaxvote'.$id.'" class="jwajaxvote-box">
        ';
    }
    if($rating_count!=1) {
       $html .= "(".$rating_count." ".JText::_('JDVOTE_VOTES').")";
    } else { 
       $html .= "(".$rating_count." ".JText::_('JDVOTE_VOTE').")";
    }
    $html .= '
        </div>
        </div>
        <div class="jwajaxvote-clr"></div>
        <!-- JW AJAX Vote Plugin v1.1 ends here -->    
        '; 
    return $html;       
}

function setAUPPointsUploads($submitted_by, $file_title){
    // added (or reduce) points to the alphauserpoints when is activated in the jD config
    // $submitted_by = user ID after upload a file
    global $jlistConfig;
    if ($jlistConfig['use.alphauserpoints'] && $submitted_by){
        $api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
        if (file_exists($api_AUP)){
            require_once ($api_AUP);
            $aupid = AlphaUserPointsHelper::getAnyUserReferreID( $submitted_by );
            if ($aupid){
                $text = JText::_('COM_EPOSTS_BACKEND_SET_AUP_UPLOAD_TEXT');
                $text = sprintf($text, $file_title);
                AlphaUserPointsHelper::newpoints( 'plgaup_eposts_user_upload_published', $aupid, $file_title, $text);
            }     
        }    
    }
}    

function setAUPPointsDownload($user_id, $file_title, $file_id, $price){
    // added (or reduce) points to the alphauserpoints when is activated in the jD config
    // $user_id = user ID from the file download
    global $jlistConfig;
    if ($jlistConfig['use.alphauserpoints'] && $user_id){
        $session = JFactory::getSession();
        $session_data = $session->get('jd_aup_session');
        if (isset($session_data) && $session_data[id] == $user_id && $session_data[file_id] == $file_id){ 
            return true;
        }
        $api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
        if (file_exists($api_AUP)){
            require_once ($api_AUP);
            $aupid = AlphaUserPointsHelper::getAnyUserReferreID( $user_id );
            if ($aupid){
                $text = JText::_('COM_EPOSTS_BACKEND_SET_AUP_DOWNLOAD_TEXT');
                $text = sprintf($text, $file_title);
                // get AUP user data
                $profil = AlphaUserPointsHelper:: getUserInfo ( '', $user_id );
                if ($jlistConfig['user.can.download.file.when.zero.points'] || $profil->points > 0 || $price == 0){
                    if ($price){
                        // price as points activated
                        if ($profil->points >= $price){
                            if ($jlistConfig['use.alphauserpoints.with.price.field']){
                            AlphaUserPointsHelper::newpoints( 'plgaup_eposts_user_download_use_price', $aupid, '', $text, '-'.$price, $text);
                            $session_data = array('id' => $user_id, 'file_id' => $file_id);
                            $session->set('jd_aup_session', $session_data);
                            return true;
                            } else {
                                AlphaUserPointsHelper::newpoints( 'plgaup_eposts_user_download', $aupid, '', $text);
                                $session_data = array('id' => $user_id, 'file_id' => $file_id);
                                $session->set('jd_aup_session', $session_data);
                                return true;
                            }    
                        } else {
                            // not enough points . no download
                            return false;
                        }    
                    } else {
                        // use points set in AUP plugin
                        //AlphaUserPointsHelper::newpoints( 'plgaup_eposts_user_download', $aupid, '', $text);
                        return true;
                    }    
                } else {
                    // not enough points . no download
                    return false;
                }   
            }     
        } else {
           return true;
        }    
    } else {
      if ($price){
          // not registered user
          return false;
      } else {     
          // guest but no price
          return true;  
      }    
    } 
}

function setAUPPointsDownloads($user_id, $file_title, $file_id, $price){
    // added (or reduce) points to the alphauserpoints when is activated in the jD config
    // $user_id = user ID from the file download
    global $jlistConfig;
    if ($jlistConfig['use.alphauserpoints'] && $user_id){
        $session = JFactory::getSession();
        $session_data = $session->get('jd_aup_session');
        if (isset($session_data) && $session_data[id] == $user_id && $session_data[file_id] == $file_id){ 
            return true;
        }
        $api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
        if (file_exists($api_AUP)){
            require_once ($api_AUP);
            $aupid = AlphaUserPointsHelper::getAnyUserReferreID( $user_id );
            if ($aupid){
                $text = JText::_('COM_EPOSTS_BACKEND_SET_AUP_DOWNLOAD_TEXT');
                $text = sprintf($text, $file_title);
                // get AUP user data
                $profil = AlphaUserPointsHelper:: getUserInfo ( '', $user_id );
                if ($jlistConfig['user.can.download.file.when.zero.points'] || $profil->points > 0 || $price == 0){
                    if ($price){
                        // price as points activated
                            AlphaUserPointsHelper::newpoints( 'plgaup_eposts_user_download_use_price', $aupid, '', $text, '-'.$price, $text);
                            $session_data = array('id' => $user_id, 'file_id' => $file_id);
                            $session->set('jd_aup_session', $session_data);
                            return true;
                    } else {
                        AlphaUserPointsHelper::newpoints( 'plgaup_eposts_user_download', $aupid, '', $text);
                        $session_data = array('id' => $user_id, 'file_id' => $file_id);
                        $session->set('jd_aup_session', $session_data);
                        return true;
                    }    
                } else {
                    return false;
                }   
            }     
        } else {
           return true;
        }   
    } else {
      return true;
    }
}

function setAUPPointsDownloaderToUploader($fileid, $files){
    // Assign points to the file uploader when a user download this file from ePosts
    $database = &JFactory::getDBO(); 
    $files_arr = explode(',', $files);
    $files_arr[] = $fileid;  
    foreach ($files_arr as $file){  
      if ($file){
        $database->setQuery("SELECT submitted_by FROM #__eposts_files WHERE file_id = '$file'");
        $uploader_id = (int)$database->loadResult();
        if ($uploader_id){
            $database->setQuery("SELECT file_title FROM #__eposts_files WHERE file_id = '$file'");
            $file_title = $database->loadResult();
            $api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
            if (file_exists($api_AUP)){
                require_once ($api_AUP);
                $aupid = AlphaUserPointsHelper::getAnyUserReferreID( $uploader_id );
                if ($aupid){
                    $text = JText::_('COM_EPOSTS_BACKEND_SET_AUP_DOWNLOADER_TO_UPLOADER_TEXT');
                    $text = sprintf($text, $file_title);
                    AlphaUserPointsHelper::newpoints( 'plgaup_eposts_downloader_to_uploader', $aupid);
                }     
            }
        }
      }      
    }        
}    

function checkLog($fileid, $user){
    global $jlistConfig;
    $database = &JFactory::getDBO();
    $app = JFactory::getApplication();
    $offset = $app->getCfg('offset');
    $datenow =& JFactory::getDate(); 
    $datenow->setOffset($offset);
    
    $date = $datenow->toFormat("%Y-%m-%d %H:%M:%S");
    $max_files_day = $jlistConfig['limited.download.number.per.day'];

    $ip = getRealIp();
    
    // download logs
    if ($jlistConfig['activate.download.log'] == '1' && $max_files_day == 0){
        $database->setQuery("INSERT INTO #__eposts_log (log_file_id, log_ip, log_datetime, log_user, log_browser) VALUES ('".$fileid."', '".$ip."', '".$date."', '".$user->get('id')."', '')");
        $database->query();
        return true;
    }
        
    if ($max_files_day != 0){
        // check limit
        $logged_user_files = array();
        $search_date = $datenow->toFormat("%Y-%m-%d");
        $database->setQuery("SELECT * FROM #__eposts_log WHERE DATE(log_datetime) = '$search_date' AND log_user = '". (int) $user->get('id')."'");
        $logged_user_files = $database->loadObjectList();
        if (!$logged_user_files || count($logged_user_files) < $max_files_day){
            // add file in log 
            $database->setQuery("INSERT INTO #__eposts_log (log_file_id, log_ip, log_datetime, log_user, log_browser) VALUES ('".$fileid."', '".$ip."', '".$date."', '".$user->get('id')."', '')");
            $database->query();
            return true;
        } else {
            // download not allowed
            return false;
        }   
    } else {
        return true;
    }       
} 

// added for search function
function _ctrSort($a, $b) {
     if (!is_array($a) || !is_array($b) || !array_key_exists("ctr", $a) || !array_key_exists("ctr", $b) || $a['ctr'] == $b['ctr'])
         return 0;
    return ($a['ctr'] < $b['ctr']) ? 1 : -1;

}   

function placeThumbs($html_file, $thumb1, $thumb2, $thumb3){
     global $jlistConfig;
 
            
        if ($thumb1 != ''){
            $thumbnail =  JURI::base().'images/eposts/screenshots/thumbnails/'.$thumb1; 
            $screenshot = JURI::base().'images/eposts/screenshots/'.$thumb1; 
            $html_file = str_replace('{thumbnail}', $thumbnail, $html_file);
            $html_file = str_replace('{screenshot}', $screenshot, $html_file);
            $html_file = str_replace('{screenshot_end}', '', $html_file);
            $html_file = str_replace('{screenshot_begin}', '', $html_file); 
            
         } else { 
            if ($jlistConfig["thumbnail.view.placeholder.in.lists"]) {
                $thumbnail = JURI::base().'images/eposts/screenshots/thumbnails/no_pic.gif';
                $screenshot = JURI::base().'images/eposts/screenshots/no_pic.gif';
                $html_file = str_replace('{thumbnail}', $thumbnail, $html_file);
                $html_file = str_replace('{screenshot}', $screenshot, $html_file);    
                $html_file = str_replace('{screenshot_end}', '', $html_file);
                $html_file = str_replace('{screenshot_begin}', '', $html_file);
            } else {    
                $pos_end = strpos($html_file, '{screenshot_end}');
                $pos_beg = strpos($html_file, '{screenshot_begin}');
                if ($pos_beg && $pos_end){     
                     $html_file = substr_replace($html_file, '', $pos_beg, ($pos_end - $pos_beg) + 16);
                } 
            }    
         }  

     
        if ($thumb2 != ''){
            $thumbnail =  JURI::base().'images/eposts/screenshots/thumbnails/'.$thumb2; 
            $screenshot = JURI::base().'images/eposts/screenshots/'.$thumb2; 
            $html_file = str_replace('{thumbnail2}', $thumbnail, $html_file);
            $html_file = str_replace('{screenshot2}', $screenshot, $html_file);
            $html_file = str_replace('{screenshot_end2}', '', $html_file);
            $html_file = str_replace('{screenshot_begin2}', '', $html_file); 
         } else { 
            if ($jlistConfig["thumbnail.view.placeholder.in.lists"]) {
                $thumbnail = JURI::base().'images/eposts/screenshots/thumbnails/no_pic.gif';
                $screenshot = JURI::base().'images/eposts/screenshots/no_pic.gif';
                $html_file = str_replace('{thumbnail2}', $thumbnail, $html_file);
                $html_file = str_replace('{screenshot2}', $screenshot, $html_file);    
                $html_file = str_replace('{screenshot_end2}', '', $html_file);
                $html_file = str_replace('{screenshot_begin2}', '', $html_file);
            } else {    
                if ($pos_end = strpos($html_file, '{screenshot_end2}')){
                     $pos_beg = strpos($html_file, '{screenshot_begin2}');
                     $html_file = substr_replace($html_file, '', $pos_beg, ($pos_end - $pos_beg) + 17);
                } 
            }    
         }  
     
        if ($thumb3 != ''){
            $thumbnail =  JURI::base().'images/eposts/screenshots/thumbnails/'.$thumb3; 
            $screenshot = JURI::base().'images/eposts/screenshots/'.$thumb3; 
            $html_file = str_replace('{thumbnail3}', $thumbnail, $html_file);
            $html_file = str_replace('{screenshot3}', $screenshot, $html_file);
            $html_file = str_replace('{screenshot_end3}', '', $html_file);
            $html_file = str_replace('{screenshot_begin3}', '', $html_file); 
         } else { 
            if ($jlistConfig["thumbnail.view.placeholder.in.lists"]) {
                $thumbnail = JURI::base().'images/eposts/screenshots/thumbnails/no_pic.gif';
                $screenshot = JURI::base().'images/eposts/screenshots/no_pic.gif';
                $html_file = str_replace('{thumbnail3}', $thumbnail, $html_file);
                $html_file = str_replace('{screenshot3}', $screenshot, $html_file);    
                $html_file = str_replace('{screenshot_end3}', '', $html_file);
                $html_file = str_replace('{screenshot_begin3}', '', $html_file);
            } else {    
                if ($pos_end = strpos($html_file, '{screenshot_end3}')){
                     $pos_beg = strpos($html_file, '{screenshot_begin3}');
                     $html_file = substr_replace($html_file, '', $pos_beg, ($pos_end - $pos_beg) + 17);
                } 
            }    
         }  
    return $html_file;
}

function getUserDepartmentsX(){
    $database = &JFactory::getDBO();
    $user = &JFactory::getUser();
    $department_list = array();
    $user_in_departments = array();
    $database->setQuery("SELECT id, departments_members FROM #__eposts_departments");
    $all_departments = $database->loadObjectList();
    if (count($all_departments > 0)){
        foreach ($all_departments as $department){
                 $department_list = explode(',', $department->departments_members);
                 if (in_array($user->id, $department_list)){
                     $user_in_departments[] = $department->id;
                 }    
        }    
    }    
    if (count($user_in_departments) > 1){
       $user_in_departments = implode(',', $user_in_departments);
    } else {
       $user_in_departments = $user_in_departments[0];
    }     
    return $user_in_departments;
}

function getUserEditDepartment(){
    global $jlistConfig;
    
    $database = &JFactory::getDBO();
    $user = &JFactory::getUser();
    $members_arr = array();
    $edit_department = (int)$jlistConfig['department.can.edit.fe'];
    if ($edit_department > 0){
        $database->setQuery("SELECT departments_members FROM #__eposts_departments WHERE id = '$edit_department'");
        $members = $database->loadResult();
        if ($members){
            $members_arr = explode(',', $members);
            if (in_array($user->id, $members_arr)){
                return true;
            }    
        }    
    }
    return false;
}

function existsCustomFieldsTitlesX(){
    global $jlistConfig;
    // check that any field is activated (has title)
    $custom_arr = array();
    $custom_array = array();
    $custom_titles = array();
    $custom_values = array();
    for ($i=1; $i<15; $i++){
        if ($jlistConfig["custom.field.$i.title"] != ''){
           $custom_array[] = $i;
           $custom_titles[] = $jlistConfig["custom.field.$i.title"];
           $custom_values[] = explode(',', $jlistConfig["custom.field.$i.values"]);
           array_unshift($custom_values[$i-1],"select");
        } else {
           $custom_array[] = 0;
           $custom_titles[] = '';
           $custom_values[] = '';
        }   
    }    
    $custom_arr[]=$custom_array;
    $custom_arr[]=$custom_titles;
    $custom_arr[]=$custom_values;
    return $custom_arr;
}

function buildFieldTitles($html_file, $file){
    global $jlistConfig;
    
    if ($jlistConfig['remove.field.title.when.empty']){
        $html_file = ($file->license) ? str_replace('{license_title}', JText::_('COM_EPOSTS_FE_DETAILS_LICENSE_TITLE'), $html_file) : str_replace('{license_title}', '', $html_file);
        $html_file = ($file->price) ? str_replace('{price_title}', JText::_('COM_EPOSTS_FE_DETAILS_PRICE_TITLE'), $html_file) : str_replace('{price_title}', '', $html_file);                                          
        $html_file = ($file->language) ? str_replace('{language_title}', JText::_('COM_EPOSTS_FE_DETAILS_LANGUAGE_TITLE'), $html_file) : str_replace('{language_title}', '', $html_file);
        $html_file = ($file->size) ? str_replace('{filesize_title}', JText::_('COM_EPOSTS_FE_DETAILS_FILESIZE_TITLE'), $html_file) : str_replace('{filesize_title}', '', $html_file);
        $html_file = ($file->system) ? str_replace('{system_title}', JText::_('COM_EPOSTS_FE_DETAILS_SYSTEM_TITLE'), $html_file) : str_replace('{system_title}', '', $html_file);
        $html_file = ($file->author) ? str_replace('{author_title}', JText::_('COM_EPOSTS_FE_DETAILS_AUTHOR_TITLE'), $html_file) : str_replace('{author_title}', '', $html_file);
        $html_file = ($file->url_home) ? str_replace('{author_url_title}', JText::_('COM_EPOSTS_FE_DETAILS_AUTHOR_URL_TITLE'), $html_file) : str_replace('{author_url_title}', '', $html_file);
        $html_file = ($file->date_added != '0000-00-00 00:00:00') ? str_replace('{created_date_title}', JText::_('COM_EPOSTS_FE_DETAILS_CREATED_DATE_TITLE'), $html_file) : str_replace('{created_date_title}', '', $html_file);
        $html_file = ($file->downloads != '') ? str_replace('{hits_title}', JText::_('COM_EPOSTS_FE_DETAILS_HITS_TITLE'), $html_file) : str_replace('{hits_title}', '', $html_file);
        $html_file = ($file->created_by) ? str_replace('{created_by_title}', JText::_('COM_EPOSTS_FE_DETAILS_CREATED_BY_TITLE'), $html_file) : str_replace('{created_by_title}', '', $html_file);
        $html_file = ($file->modified_by) ? str_replace('{modified_by_title}', JText::_('COM_EPOSTS_FE_DETAILS_MODIFIED_BY_TITLE'), $html_file) : str_replace('{modified_by_title}', '', $html_file);
        $html_file = ($file->modified_date != '0000-00-00 00:00:00') ? str_replace('{modified_date_title}', JText::_('COM_EPOSTS_FE_DETAILS_MODIFIED_DATE_TITLE'), $html_file) : str_replace('{modified_date_title}', '', $html_file);
        $html_file = ($file->file_date != '0000-00-00 00:00:00') ? str_replace('{file_date_title}', JText::_('COM_EPOSTS_FE_DETAILS_FILE_DATE_TITLE'), $html_file) : str_replace('{file_date_title}', '', $html_file);
        $html_file = ($file->url_download) ? str_replace('{file_name_title}', JText::_('COM_EPOSTS_FE_DEATAILS_FILE_NAME_TITLE'), $html_file) : str_replace('{file_name_title}', '', $html_file);          
    } else {    
        $html_file = str_replace('{license_title}', JText::_('COM_EPOSTS_FE_DETAILS_LICENSE_TITLE'), $html_file);
        $html_file = str_replace('{price_title}', JText::_('COM_EPOSTS_FE_DETAILS_PRICE_TITLE'), $html_file);
        $html_file = str_replace('{language_title}', JText::_('COM_EPOSTS_FE_DETAILS_LANGUAGE_TITLE'), $html_file);
        $html_file = str_replace('{filesize_title}', JText::_('COM_EPOSTS_FE_DETAILS_FILESIZE_TITLE'), $html_file);
        $html_file = str_replace('{system_title}', JText::_('COM_EPOSTS_FE_DETAILS_SYSTEM_TITLE'), $html_file);
        $html_file = str_replace('{author_title}', JText::_('COM_EPOSTS_FE_DETAILS_AUTHOR_TITLE'), $html_file);
        $html_file = str_replace('{author_url_title}', JText::_('COM_EPOSTS_FE_DETAILS_AUTHOR_URL_TITLE'), $html_file);
        $html_file = str_replace('{created_date_title}', JText::_('COM_EPOSTS_FE_DETAILS_CREATED_DATE_TITLE'), $html_file);
        $html_file = str_replace('{hits_title}', JText::_('COM_EPOSTS_FE_DETAILS_HITS_TITLE'), $html_file);
        $html_file = str_replace('{created_by_title}', JText::_('COM_EPOSTS_FE_DETAILS_CREATED_BY_TITLE'), $html_file);
        $html_file = str_replace('{modified_by_title}', JText::_('COM_EPOSTS_FE_DETAILS_MODIFIED_BY_TITLE'), $html_file);
        $html_file = str_replace('{modified_date_title}', JText::_('COM_EPOSTS_FE_DETAILS_MODIFIED_DATE_TITLE'), $html_file);
        $html_file = str_replace('{file_date_title}', JText::_('COM_EPOSTS_FE_DETAILS_FILE_DATE_TITLE'), $html_file);
        $html_file = str_replace('{file_name_title}', JText::_('COM_EPOSTS_FE_DEATAILS_FILE_NAME_TITLE'), $html_file);   
    }
    return $html_file;
} 

// support for editor button plugin
// insert for the content plugin a file id or a category id 
function editorInsertFile($option){
    global $mainframe, $jlistConfig;
    
    $database = &JFactory::getDBO();
    $document = & JFactory::getDocument();
    $lang = & JFactory::getLanguage();
    $lang->load('plg_editors-xtd_eposts', JPATH_ADMINISTRATOR);
    
    // build cat tree listbox
    $query = "SELECT cat_id AS id, parent_id AS parent, cat_title AS title FROM #__eposts_cats WHERE published = '1' ORDER BY ordering";
    $database->setQuery( $query );
    $cats2 = $database->loadObjectList();
    $preload = array();
    $catlist= treeSelectList( $cats2, 0, $preload, 'cat_id', 'class="inputbox" size="9"', 'value', 'text', '');

    // build files listbox
    $files_list = array();
    
    $query = "SELECT a.cat_id, a.published, a.file_id AS id,"
    . " CONCAT(b.cat_dir, '/', a.file_title, ' ', a.release) AS name"
    . ' FROM #__eposts_files AS a'
    . ' LEFT JOIN #__eposts_cats AS b ON b.cat_id = a.cat_id'
    . " WHERE a.published = '1'"
    . ' ORDER BY a.cat_id, a.file_title';
    
    $database->setQuery( $query );
    $files = $database->loadObjectList();
    foreach ($files as $file) {
        $files_list[] = JHTML::_('select.option', $file->id, $file->name);
    }
    $files_listbox =  JHTML::_('select.genericlist', $files_list, 'file_id', 'class="inputbox" size="9"', 'value', 'text', '' );
    
    $eName    = JRequest::getVar('e_name');
    $eName    = preg_replace( '#[^A-Z0-9\-\_\[\]]#i', '', $eName );
   
    $js = "
              function insertDownload()
      {
        var file_id = document.getElementById(\"file_id\").value;
        var cat_id = document.getElementById(\"cat_id\").value;
        var count = document.getElementById(\"count\").value;
        var tag;
         
        if (file_id >0){
            tag = \"\{jd_file file==\"+file_id+\"\}\";
        } else {
           if (cat_id > 0){
               tag = \"\{jd_file category==\"+cat_id+\" count==\"+count+\"\}\";
           }    
        }    
        if (file_id || cat_id){                                                                                 
           window.parent.jInsertEditorText(tag, '".$eName."');
           window.parent.SqueezeBox.close();
           return true;     
        }    
        window.parent.SqueezeBox.close();  
        return false;
       }";

        $doc = JFactory::getDocument();
        $doc->addScriptDeclaration($js);
   
    ?>
   
    <body class="jd_editor_body"> 
    <fieldset class="adminform">
    <form name="adminFormLink" id="adminFormLink">
    <table class="jd_editor_body" width="100%" cellpadding="0" cellspacing="2" border="0" style="padding: 0px;">
       <tr> 
         <td colspan="2">
            <img src="<?php echo JURI::root(); ?>administrator/components/com_eposts/images/jd_logo_48.png" width="32px" height="32px" align="middle" border="0"/>
            <b><?php echo JText::_('PLG_EDITORS-XTD_JDOWNLOADS_TITLE').'</b><br />'; ?>
            <?php echo JText::_('PLG_EDITORS-XTD_JDOWNLOADS_DESC'); ?>
         </td>
       </tr>
       <tr>
          <td class="key" align="right" width="25%" valign="top">
              <label for="file_id">
                  <?php echo JText::_('PLG_EDITORS-XTD_JDOWNLOADS_FILE_ID_TITLE'); ?>
              </label>
          </td>
          <td width="75%">
              <?php echo $files_listbox; ?>
          </td>
       </tr>
              <tr><td></td><td><small><?php echo JText::_('PLG_EDITORS-XTD_JDOWNLOADS_FILE_ID_NOTE'); ?></small></td></tr>
       <tr><td colspan="2"><hr></td></tr>
            <tr>
                <td class="key" align="right" valign="top">
                    <label for="cat_id">
                        <?php echo JText::_('PLG_EDITORS-XTD_JDOWNLOADS_CAT_ID_TITLE'); ?>
                    </label>
                </td>
                <td>
                   <?php echo $catlist; ?>
                </td>
            </tr>
            <tr>
                <td class="key" align="right" valign="top">
                    <label for="count">
                        <?php echo JText::_('PLG_EDITORS-XTD_JDOWNLOADS_COUNT_TITLE'); ?>
                    </label>
                </td>
                <td>
                   <input type="text" id="count" name="count" value="0" />
                </td>
            </tr>
            <tr><td></td><td><small><?php echo JText::_('PLG_EDITORS-XTD_JDOWNLOADS_COUNT_DESC'); ?></small></td></tr>
            <tr>
                <td class="key" align="right"></td>
                <td>
                    <button onClick="insertDownload();return false;"><?php echo JText::_('PLG_EDITORS-XTD_JDOWNLOADS_CAT_BUTTON_TEXT'); ?></button>
                    <button type="button" onClick="window.parent.SqueezeBox.close();"><?php echo JText::_('JCANCEL') ?></button>                     
                </td>
            </tr>
            <tr><td colspan="2"><small><?php echo JText::_('PLG_EDITORS-XTD_JDOWNLOADS_INFO'); ?></small></td></tr> 
        </table>

        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="e_name" value="<?php echo $eName; ?>" />
        <?php echo  JHTML::_( 'form.token' ); ?>
        </form>
        </fieldset> 
        </body>  
        <?php
}

function removeEmptyTags($html){
    $pattern = "/<[^\/>]*>([\s]?)*<\/[^>]*>/";
    return preg_replace($pattern, '', $html);
} 

function getRealIp() {
      if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP']; // share internet
      } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR']; // pass from proxy
      } else {
        $ip = $_SERVER['REMOTE_ADDR'];
      }
      return $ip;
}
  
// need it only, to make it compatible with joomla 1.6 !
function jdgenericlist( $arr, $name, $attribs = null, $key = 'value', $text = 'text', $selected = NULL, $idtag = false, $translate = false )
    {
        if ( is_array( $arr ) ) {
            reset( $arr );
        }

        if (is_array($attribs)) {
            $attribs = JArrayHelper::toString($attribs);
         }

        $id = $name;

        /*if ( $idtag ) {
            $id = $idtag;
        }

        $id        = str_replace('[','',$id);
        $id        = str_replace(']','',$id);
        */
        $html    = '<select name="'. $name .'" id="'. $id .'" '. $attribs .'>';
        $html    .= jdoptions( $arr, $key, $text, $selected, $translate );
        $html    .= '</select>';

        return $html;
    }
    
// need it only, to make it compatible with joomla 1.6 !   
function jdoptions( $arr, $key = 'value', $text = 'text', $selected = null, $translate = false )
    {
        $html = '';

        foreach ($arr as $i => $option)
        {
            $element =& $arr[$i]; // since current doesn't return a reference, need to do this

            $isArray = is_array( $element );
            $extra     = '';
            if ($isArray)
            {
                $k         = $element[$key];
                $t         = $element[$text];
                $id     = ( isset( $element['id'] ) ? $element['id'] : null );
                if(isset($element['disable']) && $element['disable']) {
                    $extra .= ' disabled="disabled"';
                }
            }
            else
            {
                $k         = $element->$key;
                $t         = $element->$text;
                $id     = ( isset( $element->id ) ? $element->id : null );
                if(isset( $element->disable ) && $element->disable) {
                    $extra .= ' disabled="disabled"';
                }
            }

            // This is real dirty, open to suggestions,
            // barring doing a propper object to handle it
            if ($k === '<OPTDEPARTMENT>') {
                $html .= '<optdepartment label="' . $t . '">';
            } else if ($k === '</OPTDEPARTMENT>') {
                $html .= '</optdepartment>';
            }
            else
            {
                //if no string after hypen - take hypen out
                $splitText = explode( ' - ', $t, 2 );
                $t = $splitText[0];
                if(isset($splitText[1])){ $t .= ' - '. $splitText[1]; }

                //$extra = '';
                //$extra .= $id ? ' id="' . $arr[$i]->id . '"' : '';
                if (is_array( $selected ))
                {
                    foreach ($selected as $val)
                    {
                        $k2 = is_object( $val ) ? $val->$key : $val;
                        if ($k == $k2)
                        {
                            $extra .= ' selected="selected"';
                            break;
                        }
                    }
                } else {
                    $extra .= ( (string)$k == (string)$selected  ? ' selected="selected"' : '' );
                }

                //if flag translate text
                if ($translate) {
                    $t = JText::_( $t );
                }

                // ensure ampersands are encoded
                $k = JFilterOutput::ampReplace($k);
                $t = JFilterOutput::ampReplace($t);

                $html .= '<option value="'. $k .'" '. $extra .'>' . $t . '</option>';
            }
        }

        return $html;
    }  
    

    
    
    
    
function booleanlist( $name, $attribs = null, $selected = null, $yes='yes', $no='no', $id=false )
    {
        $arr = array(
            JHTML::_('select.option',  '0', JText::_( $no ) ),
            JHTML::_('select.option',  '1', JText::_( $yes ) )
        );
        return jdradiolist($arr, $name, $attribs, 'value', 'text', (int) $selected, $id );
    }  
    
function jdradiolist( $arr, $name, $attribs = null, $key = 'value', $text = 'text', $selected = null, $idtag = false, $translate = false )
    {
        reset( $arr );
        $html = '';
        $html = '<fieldset class="radio">';

        if (is_array($attribs)) {
            $attribs = JArrayHelper::toString($attribs);
         }

        $id_text = $name;
        if ( $idtag ) {
            $id_text = $idtag;
        }

        for ($i=0, $n=count( $arr ); $i < $n; $i++ )
        {
            $k    = $arr[$i]->$key;
            $t    = $translate ? JText::_( $arr[$i]->$text ) : $arr[$i]->$text;
            $id    = ( isset($arr[$i]->id) ? @$arr[$i]->id : null);

            $extra    = '';
            $extra    .= $id ? " id=\"" . $arr[$i]->id . "\"" : '';
            if (is_array( $selected ))
            {
                foreach ($selected as $val)
                {
                    $k2 = is_object( $val ) ? $val->$key : $val;
                    if ($k == $k2)
                    {
                        $extra .= " selected=\"selected\"";
                        break;
                    }
                }
            } else {
                $extra .= ((string)$k == (string)$selected ? " checked=\"checked\"" : '');
            }
            $html .= "\n\t<input type=\"radio\" name=\"$name\" id=\"$id_text$k\" value=\"".$k."\"$extra $attribs />";
            $html .= "\n\t<label for=\"$id_text$k\">$t</label>";
        }
        $html .= '</fieldset>';
        $html .= "\n";
        return $html;
    }   

function getParentsCatsTitles($parent_id){
    $database = &JFactory::getDBO(); 
    $title = '';
    while ($parent_id){
        $database->setQuery("SELECT cat_title, parent_id FROM #__eposts_cats WHERE cat_id = '$parent_id'");
        $result = $database->loadObject();
        if ($title){
            $title = $result->cat_title.' / '.$title;
        } else {
            $title = $result->cat_title;
        }    
        $parent_id = $result->parent_id;
    }
    return $title;  
}

function escape_string($str) {
    return strtr($str, array(
        "\0" => "",
        "'"  => "&#39;",
        "\"" => "&#34;",
        "\\" => "&#92;",
        "<"  => "&lt;",
        ">"  => "&gt;",
    ));
}

function listResources($option, $view){
    global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
  
    // view only when category exist
    $database->SetQuery('SELECT COUNT(*) FROM #__eposts_offices');
    $office_sum = $database->loadResult();
    if (!$office_sum) {
       echo JText::_('COM_EPOSTS_ERROR_NO_OFFICE_EXIST');             
       
    } else {
               
		jlist_HTML::listResources($option, $view);
    }
}

function resourceCalendar($option, $view){
    global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::resourcesCalendar($option, $view);
}

function bookingRequest($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::bookingRequest($option, $view);
}

function ajaxListbookingRequest($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::ajaxListbookingRequest($option, $view);
}

function addUserBookingRequest($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::addUserBookingRequest($option, $view);
}

function saveBookingRequest ($option, $cid, $apply=0){
 
  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $row = new jlist_bookingrequest($database);
                $crruser = &JFactory::getUser();
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);  $resourceid1 = (int)JRequest::getString('resourceid', 0);
    if($cirid) $row->id = $cirid;
                $users_selected = JRequest::getVar('users_selected', array(), 'post', 'array');
 
    $row->booking_description = trim($row->booking_description);
               
                $database->setQuery("SELECT count(*) as total FROM #__eposts_bookingrequest WHERE  id != '".$row->id."' and resourceid='".$resourceid1."' and booking_status != '2' AND bookingdate = '". $row->bookingdate."' AND ('".($row->from_h*60+$row->from_m+1)."' BETWEEN (from_h*60+from_m) AND (to_h*60+to_m) OR '".($row->to_h*60+$row->to_m-1)."' BETWEEN (from_h*60+from_m) AND (to_h*60+to_m)  )");
               
                echo ("SELECT count(*) as total FROM #__eposts_bookingrequest WHERE  id != '".$row->id."' and resourceid='".$resourceid1."' and booking_status != '2' AND bookingdate = '". $row->bookingdate."' AND ('".($row->from_h*60+$row->from_m+1)."' BETWEEN (from_h*60+from_m) AND (to_h*60+to_m) OR '".($row->to_h*60+$row->to_m-1)."' BETWEEN (from_h*60+from_m) AND (to_h*60+to_m)  )");
              
                $confilct = $database->loadObject();
               
////echo '<br />';
				//echo $confilct->total;
				//exit();
				if ($confilct->total > 0) {
                                $_SESSION['bookingForm']['booking_title'] = JRequest::getVar('booking_title');
                                $_SESSION['bookingForm']['booking_description'] = JRequest::getVar('booking_description', '', 'post', 'string', JREQUEST_ALLOWHTML);
                                $_SESSION['bookingForm']['booking_details_description'] = JRequest::getVar('booking_details_description', '', 'post', 'string', JREQUEST_ALLOWHTML);
                               
                                $link = "index.php?option=com_eposts&view=bookingrequest&page=error&resourceid=".$row->resourceid."&resources_offices=".$row->resources_offices."&bookingdate=".$row->bookingdate."&Itemid=194&cirid=".$row->id;
                                $mainframe->redirect($link, JText::_('Sorry slot(s) are already booked.'), 'error');
                } else {
                                $_SESSION['bookingForm']['booking_title'] = '';
                                $_SESSION['bookingForm']['booking_description'] = '';
                                $_SESSION['bookingForm']['booking_details_description'] = '';
                }
/*$database->setQuery("SELECT * "
                                                . "\n FROM #__eposts_bookingslots "
                                                . "\n WHERE id IN (SELECT slots_id FROM #__eposts_bookingrequest_slots WHERE bookingrequest_id IN (SELECT id FROM #__eposts_bookingrequest WHERE resourceid = '".$row->resourceid."' && bookingdate = '".$row->bookingdate."' && booking_status IN (0,1) && id != '".$row->id."'))"
                                                . "\n ORDER BY id ASC"
                                );
$pardingResorce = $database->loadObjectList();
$link = "index.php?option=com_eposts&view=bookingrequest&resourceid=".$row->resourceid."&resources_offices=".$row->resources_offices."&bookingdate=".$row->bookingdate."&cirid=".$row->id;
                foreach($pardingResorce as $xslot) {
                                if(in_array($xslot->id,$slots_selected)){
                                   $mainframe->redirect($link, JText::_('Sorry slot(s) are already booked.')." ");
                                }
                }*/
               
                $row2 = new jlist_resources( $database );
    $row2->load( $row->resourceid );
               
                $row->submittedto = $row2->resources_controller;
                               
                $query = "SELECT count(*) as totalrec FROM #__eposts_resources_members
              WHERE user_id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id='".$row->publishedby."')
              && resource_id = '".$row->resourceid."'";
                                                               
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rowstatus = $database->loadObject();
               
                if($rowstatus->totalrec > 0 || $row->publishedby == $row->submittedto){
                   $row->booking_status = 1;
                }else{
                   $row->booking_status = 1; //previously by 0 farhaj
                $row->approvedby=425;////here we add it our self
                }
 
///////////////////////////////////////
                                    // store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
////////////// coding by farhaj
 
$cirid = $row->id;
                $user = &JFactory::getUser();
               
                $row = new jlist_bookingrequest( $database );
    $row->load( $cirid );
               
                $row->approvedby = 425;//$user->id;
                $row->approvedon = date($jlistConfig['global.datetime']);
                $row->booking_status = '1';
               
                $query = "SELECT user_id FROM #__eposts_bookingrequest_members
                             WHERE bookingrequest_id = '".$cirid."'
                              ";
                $database = &JFactory::getDBO();
                $database->SetQuery($query);
                $rowsUsers = $database->loadObjectList();
               
                                                                                                                /////////////////////
                                                                                  //$html_format = true;
$startDate = $row->bookingdate;
$startDate = str_replace("-", "", $startDate);
$endDate = $startDate;
 
$database->setQuery("SELECT bookingslots_description FROM #__eposts_bookingslots WHERE id IN (SELECT MIN(slots_id) FROM  #__eposts_bookingrequest_slots WHERE bookingrequest_id='".$cirid."')");
$startTime = $database->loadResult();
 
$database->setQuery("SELECT bookingslots_description_ar FROM #__eposts_bookingslots WHERE id IN (SELECT MAX(slots_id) FROM #__eposts_bookingrequest_slots WHERE bookingrequest_id='".$cirid."')");
$endTime = $database->loadResult();
 
$startDate .= 'T'.$startTime;
$endDate .= 'T'.$endTime;
                                                                                                  
                foreach($rowsUsers as $rowsUser){
                                   $where = ' WHERE 1 = 1  && id = "'.$rowsUser->user_id.'" ';
                                   $database->SetQuery( "SELECT email FROM #__users".$where);
                                                  $memdtl = $database->loadResult();
 
                  if($row->booking_status && !empty($memdtl)){
                                 
                                                /////////////////////
                                               
$html_format  = 'MIME-Version: 1.0' . "\r\n";
$html_format .= 'Content-type: text/Calendar; charset=iso-8859-1' . "\r\n";
//$headers .= "Content-Disposition: inline; filename=calendar.ics";
 
// Additional headers
$html_format .= 'From: '.$user->email.' <'.$user->email.'>' . "\r\n";
 
$message  = "BEGIN:VCALENDAR\n";
//$message .= "VERSION:2.0\n";
//$message .= "PRODID:-//Foobar Corporation//NONSGML Foobar//EN\n";
//$message .= "METHOD:REQUEST\n"; // requied by Outlook
$message .= "BEGIN:VEVENT\n";
//$message .= "UID:".date('Ymd').'T'.date('His')."-".rand()."-example.com\n"; // required by Outlok
//$message .= "DTSTAMP:".date('Ymd').'T'.date('His')."\n"; // required by Outlook
//$message .= "DTSTART:20130125T000000\n";
//$message .= "DTEND:20130127T000000\n";
$message .= "DTSTART:".$startDate."\n";
$message .= "DTEND:".$endDate."\n";
$message .= "SUMMARY:".$row->booking_title."\n";
$message .= "DESCRIPTION:".$row->booking_description."\n";
$message .= "END:VEVENT\n";
$message .= "END:VCALENDAR\n";
                                                                                                   $receiverEmail = $memdtl;
                                                                                                   $mailSubject = $row->booking_title;                                                                                     
                                                                                                   $text = $message. $row->booking_description;
                                                  //$success = JUtility::sendMail($user->email, $mailSubject, NULL, NULL, $text, $html_format, NULL, $receiverEmail);
                                                  $success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);
                                                  //$success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
                                /////////////////////
 
                  }
                }
               
                $query = "SELECT publishedby FROM #__eposts_bookingrequest
                             WHERE id = '".$cirid."'
                              ";
                $database = &JFactory::getDBO();
                $database->SetQuery($query);
                $rowsUsers = $database->loadObjectList();
                foreach($rowsUsers as $rowsUser){
                                   $where = ' WHERE 1 = 1  && id = "'.$rowsUser->publishedby.'" ';
                                   $database->SetQuery( "SELECT email, name FROM #__users".$where);
                                                  $memdtl = $database->loadObject();
 
                                $database->SetQuery( "SELECT resources_title FROM #__eposts_resources WHERE id = '".$row->resourceid."'");
                                $resource = $database->loadResult();
                  if($row->booking_status && !empty($memdtl)){
                                 
                                                /////////////////////
                                                   $receiverEmail = $memdtl->email;
                                                    $mailSubject = 'Your resource booking request has been approved';                                                                                     
                                                                //$text = '<p>Booking Title:\n '.$row->booking_title.'</p>\n<p>'.$row->booking_description.'</p>';                                                                    
                                                                                                                $text = "Dear ".$memdtl->name.",";
                                                                                                                $text .= "<br><br><p>Your request for booking the &quot;".$resource."&quot; has been approved.</p>";
                                                                                                                $text .= "<br><br><p>Thanks</p>";
                                                                                                                $text .= "<br><br><p><b>Note:</b> This is a system generated email. Please do not reply to this email address.</p>"; 
                                                  $html_format = true;
                                                  //$success = JUtility::sendMail($user->email, $mailSubject, NULL, NULL, $text, $html_format, NULL, $receiverEmail);
                                                  $success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);
                                                  //$success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
                                /////////////////////
 
                  }
                 
                }
 
                                ///////////////////////////////////////
                                    // store it in the db
  
                if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
                }
                               
                               
                ////////////////
                $database->SetQuery("DELETE FROM #__eposts_bookingrequest_slots WHERE bookingrequest_id ='".$row->id."'");
                                $database->query();
                                if(!empty($slots_selected)){
                                                foreach($slots_selected as $slot_selected){
                                                                $database->setQuery("INSERT INTO #__eposts_bookingrequest_slots (bookingrequest_id,slots_id) VALUES ('".$row->id."','".$slot_selected."')");
                $database->query();
                                                                }
                                }
                ////////////////////////////
                                $database->SetQuery("DELETE FROM #__eposts_bookingrequest_members WHERE bookingrequest_id ='".$row->id."'");
                                $database->query();
                               
                                if(!empty($users_selected)){
                                                foreach($users_selected as $memID){
                                                                $database->setQuery("INSERT INTO #__eposts_bookingrequest_members (bookingrequest_id,user_id) VALUES ('".$row->id."','".$memID."')");
                $database->query();
                                                                                   $where = ' WHERE 1 = 1  && id = "'.$memID.'" ';
                                                                                   $database->SetQuery( "SELECT email FROM #__users".$where);
                                                                                                  $memdtl = $database->loadResult();
                               
                                                                  if($row->booking_status && !empty($memdtl)){
                                                                                 
                                                                                                /////////////////////
                                                                                  //$html_format = true;
$startDate = $row->bookingdate;
$startDate = str_replace("-", "", $startDate);
$endDate = $startDate;
 
/*$database->setQuery("SELECT bookingslots_description FROM #__eposts_bookingslots WHERE id IN (SELECT MIN(slots_id) FROM  #__eposts_bookingrequest_slots WHERE bookingrequest_id='".$row->id."')");
$startTime = $database->loadResult();
 
$database->setQuery("SELECT bookingslots_description_ar FROM #__eposts_bookingslots WHERE id IN (SELECT MAX(slots_id) FROM #__eposts_bookingrequest_slots WHERE bookingrequest_id='".$row->id."')");
$endTime = $database->loadResult();*/
 
$startDate .= 'T'.$row->from_h . $row->from_m . '00';
$endDate .= 'T'.$row->to_h . $row->to_m . '00';
 
$html_format  = 'MIME-Version: 1.0' . "\r\n";
$html_format .= 'Content-type: text/Calendar; charset=iso-8859-1' . "\r\n";
//$headers .= "Content-Disposition: inline; filename=calendar.ics";
 
// Additional headers
$html_format .= 'From: '.$user->email.' <'.$user->email.'>' . "\r\n";
 
$message  = "BEGIN:VCALENDAR\n";
//$message .= "VERSION:2.0\n";
//$message .= "PRODID:-//Foobar Corporation//NONSGML Foobar//EN\n";
//$message .= "METHOD:REQUEST\n"; // requied by Outlook
$message .= "BEGIN:VEVENT\n";
//$message .= "UID:".date('Ymd').'T'.date('His')."-".rand()."-example.com\n"; // required by Outlok
//$message .= "DTSTAMP:".date('Ymd').'T'.date('His')."\n"; // required by Outlook
//$message .= "DTSTART:20130125T000000\n";
//$message .= "DTEND:20130127T000000\n";
$message .= "DTSTART:".$startDate."\n";
$message .= "DTEND:".$endDate."\n";
$message .= "SUMMARY:".$row->booking_title."\n";
$message .= "DESCRIPTION:".$row->booking_description."\n";
$message .= "END:VEVENT\n";
$message .= "END:VCALENDAR\n";
                                                                                                   $receiverEmail = $memdtl;
                                                                                                   $mailSubject = $row->booking_title;                                                                                     
                                                                                                   $text = $message. $row->booking_description;
                                                                                                  //$success = JUtility::sendMail($user->email, $mailSubject, NULL, NULL, $text, $html_format, NULL, $receiverEmail);
                                                  $success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);
                                                                                                  //$success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
                                                                                /////////////////////
 
                                                                  }
                                                                }
                                }
                                $database->SetQuery( "SELECT resources_title FROM #__eposts_resources WHERE id = '".$row->resourceid."'");
                                $resource = $database->loadResult();
                                $where = ' WHERE 1 = 1  && id = "'.$row->submittedto.'" ';
                                                                                      $database->SetQuery( "SELECT email, name FROM #__users".$where);
                                                                                                  $memdtl = $database->loadObject();
                                             if(!$row->booking_status && !empty($memdtl)){
                                                   $receiverEmail = $memdtl->email;
                                                                                                   $mailSubject = 'A resource booking request is pending for your approval';                                                                                                  
                                                                                                   //$text = '<p>Booking Title:\n '.$row->booking_title.'</p>\n<p>'.$row->booking_description.'</p>';                                                                    
                                                                                                                $text = "Dear ".$memdtl->name.",";
                                                                                                                $text .= "<br><br><p>A request has been submitted by &quot;".$crruser->name."&quot; for booking the &quot;".$resource."&quot;. Kindly <a href='".JURI::base()."index.php?option=com_eposts&view=approvebookingrequest&Itemid=193'>click here</a> to see the further details of this request and to approve or reject this request.</p>";
                                                                                                                $text .= "<br><br><p>Thanks</p>";
                                                                                                                $text .= "<br><br><p><b>Note:</b> This is a system generated email. Please do not reply to this email address.</p>";
                                                                                                  $html_format = true;
                                                                                                  //$success = JUtility::sendMail($user->email, $mailSubject, NULL, NULL, $text, $html_format, NULL, $receiverEmail);
                                                  $success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);
                                                                                                  //$success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
                                              }
                ///////////////////////
    }
               
    $linkSuccess = "index.php?option=com_eposts&view=calendar&resourceid=".$row->resourceid."&resources_offices=".$row->resources_offices."&Itemid=105";
                $mainframe->redirect($linkSuccess, JText::_('Resource has been reserved.')." ");
               
///////////////////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////
}

function deleteBookingRequest($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
    //$categories = join(",", $cirid);
    $resourceid = JRequest::getString('resourceid');
    $resources_offices = JRequest::getString('resources_offices');
    $startmonth = JRequest::getString('startmonth');
    $startyear = JRequest::getString('startyear');
    $lang = JRequest::getString('lang');
    $returnview = JRequest::getString('returnview');
    $returnItemid = JRequest::getString('returnItemid');
	
$linkSuccess = "index.php?option=com_eposts&view=viewbookingrequest&Itemid=194";

	if (!empty($returnview)) {
		$linkSuccess = "index.php?option=com_eposts";
		$linkSuccess .= "&view=".$returnview;
		$linkSuccess .= "&resourceid=".$resourceid;
		$linkSuccess .= "&resources_offices=".$resources_offices;
		$linkSuccess .= !empty($startmonth) ? "&startmonth=".$startmonth : "";
		$linkSuccess .= !empty($startyear) ? "&startyear=".$startyear : "";
		$linkSuccess .= "&Itemid=".$returnItemid;
		$linkSuccess .= "&lang=".$lang;
	}
    //Delete Categories
	
    $database->SetQuery("DELETE FROM #__eposts_bookingrequest WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

	$database->SetQuery("DELETE FROM #__eposts_bookingrequest_slots WHERE bookingrequest_id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

	$database->SetQuery("DELETE FROM #__eposts_bookingrequest_members WHERE bookingrequest_id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

    //$msg = $total .JText::_('Booking has been canceled.' . $linkSuccess)." ";
    $msg = $total .JText::_('Booking has been canceled.')." ";
	$mainframe->redirect($linkSuccess, $msg );
}


function myHomePage($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::myHomePage($option, $view);
}

function mainMyHomePage($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$breadcrumbs =& $mainframe->getPathWay();
        $breadcrumbs->addItem(JText::_('COM_EPOSTS_MY_HOME_PAGE_TITLE'), JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainmyhomepage'));
	jlist_HTML::mainMyHomePage($option, $view);
}

function myDepartment($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$breadcrumbs =& $mainframe->getPathWay();
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	
	$cirid = (int)JRequest::getString('cirid', 0);
	$where = ' WHERE id IN (' . $cirid . ')';
    
	$query = "SELECT * FROM #__eposts_departments
              $where
              ORDER BY departments_name ASC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $row = $database->loadObject();
	
	$title = $row->departments_name;
	$objective = $row->departments_description;
	if ($tag == 'ar-AA') :
		$title = $row->departments_name_ar;
		$objective = $row->departments_description_ar;
	endif;
	
	//$title = JText::_('COM_EPOSTS_DEPART_OBJECTIVE');
	
	jlist_HTML::myDepartment($option, $view, $title, $objective, $tag);
}

function departmentList($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::departmentList($option, $view);
}

function listCirculars($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listCirculars($option, $view);
}

function newCircular($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newCircular($option, $view);
}

function saveCirculars($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $row = new jlist_circulars($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	
	if($cirid){
		$row2 = new jlist_circulars($database);
        $row2->load( $cirid );
	}

    $row->circulars_description = trim($row->circulars_description);
	$link = "index.php?option=com_eposts&view=newcircular&Itemid=156&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listcirculars&Itemid=156";
/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.$jlistConfig['files.uploaddir'])){
		   mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   }
////////////////////////////////////////////////////////////////////
	   
/////////////////////////////////file upload///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
 //global $mainframe, $jlistConfig;
   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
 
   $fieldName = 'circulars_file';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
	
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
		
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    

  // echo JPATH_SITE.DS.$jlistConfig['files.uploaddir'];
		//exit;
 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
  $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
	//$uploadPath = '\\\\sqlsrv\\Chheena'.DS.$fileName;

   //if(!JFile::upload($fileTemp, $uploadPath)){
   if(!copy($fileTemp, $uploadPath)){	   
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->circulars_document = $jlistConfig['files.uploaddir'].DS.$fileName;
	}
	endif;	

		///////////////////////////////////////
		   $fieldName = 'circulars_file_ar';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
	
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->circulars_document_ar = $jlistConfig['files.uploaddir'].DS.$fileName;
	}//end else second file
	endif;
		///////////////////////////////////////
		    // store it in the db
	$row->circulars_date = date('Y-m-d', strtotime($row->circulars_date));
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
		    if($cirid){
		     if(isset($_FILES['circulars_file']['name']) && $_FILES['circulars_file']['name'] != ''){
				$file1 = $filepath.DS.$row2->circulars_document;
				unlink($file1);	
			 }
			 
			 if(isset($_FILES['circulars_file_ar']['name']) && $_FILES['circulars_file_ar']['name'] != ''){
				$file2 = $filepath.DS.$row2->circulars_document_ar;
				unlink($file2);
			 } 
			}
    }
    
	$mainframe->redirect($linkSuccess, JText::_('Circular has been saved.')." ");
	//////////////////
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteCirculars($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
	
	$row2 = new jlist_circulars($database);
    $row2->load( $cirid );
	
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
	
	
	
    //$categories = join(",", $cirid);
     $linkSuccess = "index.php?option=com_eposts&view=listcirculars&Itemid=156";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_circulars WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

    $file1 = $filepath.DS.$row2->circulars_document;
	$file2 = $filepath.DS.$row2->circulars_document_ar;
	unlink($file1);
	unlink($file2);
	
    $msg = $total .JText::_('Circular has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

function return_bytes ($size_str)
{
    switch (substr ($size_str, -1))
    {
        case 'M': case 'm': return (int)$size_str * 1048576;
        case 'K': case 'k': return (int)$size_str * 1024;
        case 'G': case 'g': return (int)$size_str * 1073741824;
        default: return $size_str;
    }
}

function listNewHires($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listNewHires($option, $view);
}

function newHires($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newHires($option, $view);
}

function saveNewHires($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $row = new jlist_newhires($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
	
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	 if($cirid){
		$row2 = new jlist_newhires($database);
        $row2->load( $cirid );
	}
	
	/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.$jlistConfig['files.uploaddir'])){
		   mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   }
    ////////////////////////////////////////////////////////////////////

    $row->newhires_description = trim($row->newhires_description);
	$link = "index.php?option=com_eposts&view=newhires&Itemid=155&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listnewhires&Itemid=155";
	$padfile = JArrayHelper::getValue($_FILES,'newhires_file',array('tmp_name'=>''));


	/////////////////////////////////file upload////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////

   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
 
   $fieldName = 'newhires_file';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->newhires_picture = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	
	endif;
	
	$row->expirydate = date('Y-m-d', strtotime($row->expirydate));
	$row->expirydate .= ' 23:59:00';
	// store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
		  if($cirid){
			   if(isset($_FILES['newhires_file']['name']) && $_FILES['newhires_file']['name'] != ''){
				  $file1 = $filepath.DS.$row2->newhires_picture;
				  unlink($file1);	
			   }
		  } 
    }

	    
	$mainframe->redirect($linkSuccess, JText::_('Newhire has been saved.')." ");
	//////////////////

///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewHires($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
	
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
	
	$row = new jlist_newhires( $database );
    $row->load( $cirid );

$linkSuccess = "index.php?option=com_eposts&view=listnewhires&Itemid=155";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_newhires WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }
	$file = $filepath.DS.$row->newhires_picture;
	unlink($file);

    $msg = $total .JText::_('Newhire has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}


//Tasawaqs
function listTasawaqs($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listTasawaqs($option, $view);
}

function newTasawaq($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newTasawaq($option, $view);
}

function saveNewTasawaq($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	$row = new jlist_tasawaqs($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	 if($cirid){
		$row2 = new jlist_tasawaqs($database);
        $row2->load( $cirid );
	}
	/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.$jlistConfig['files.uploaddir'])){
		   mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   }
    ////////////////////////////////////////////////////////////////////

    $row->tasawaqs_description = trim($row->tasawaqs_description);
    $link = "index.php?option=com_eposts&view=newtasawaq&Itemid=153&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listtasawaqs&Itemid=153";
	
	/////////////////////////////////file upload////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////

   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
 
   $fieldName = 'tasawaqs_p_logo';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->tasawaqs_p_logo = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	
	endif;
	
	
	
    /*$filename2 = $_FILES['tasawaqs_image']['name'];
    $fileTemp2 = $_FILES['tasawaqs_image']['tmp_name'];
    $uploadPath2 = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename2;
	if ($filename2 != '') {
    	//JFile::upload($fileTemp2, $uploadPath2);
		copy($fileTemp2, $uploadPath2);
		$row->tasawaqs_image = $filename2;
	}*/
	
	$fieldName = 'tasawaqs_image';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->tasawaqs_image = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	
	endif;
	
	
    /*$filename3 = $_FILES['tasawaqs_attachment']['name'];
    $fileTemp3 = $_FILES['tasawaqs_attachment']['tmp_name'];
    $uploadPath3 = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename3;
	if ($filename3 != '') {
    	//JFile::upload($fileTemp3, $uploadPath3);
		copy($fileTemp3, $uploadPath3);
		$row->tasawaqs_attachment = $filename3;
	}*/
		    // store it in the db
	$fieldName = 'tasawaqs_attachment';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
   
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		/*echo $uploadPath;
        echo 'chheena';
		exit;*/
		///////////////
		
		$row->tasawaqs_attachment = $jlistConfig['files.uploaddir'].DS.$fileName;
	}
			
	endif;
	
		
	$row->tasawaqs_expiry = date('Y-m-d', strtotime($row->tasawaqs_expiry));
	$row->tasawaqs_expiry .= ' 23:59:00';
	
	           if(isset($_REQUEST['del_tasawaqs_p_logo']) && $_REQUEST['del_tasawaqs_p_logo'] == '1' && $_FILES['tasawaqs_p_logo']['name'] == ''){
				  $row->tasawaqs_p_logo = '';	
			   }
			   
			   if(isset($_REQUEST['del_tasawaqs_image']) && $_REQUEST['del_tasawaqs_image'] == '1' && $_FILES['tasawaqs_image']['name'] == ''){
				  $row->tasawaqs_image = '';
			   } 
			   
			   if(isset($_REQUEST['del_tasawaqs_attachment']) && $_REQUEST['del_tasawaqs_attachment'] == '1' && $_FILES['tasawaqs_attachment']['name'] == ''){
				  $row->tasawaqs_attachment='';
			   } 
	
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
		  if($cirid){
			   if(isset($_REQUEST['del_tasawaqs_p_logo']) && $_REQUEST['del_tasawaqs_p_logo'] == '1' && file_exists($filepath.DS.$row2->tasawaqs_p_logo)){
				  $file1 = $filepath.DS.$row2->tasawaqs_p_logo;
				  unlink($file1);	
			   }
			   
			   if(isset($_REQUEST['del_tasawaqs_image']) && $_REQUEST['del_tasawaqs_image'] == '1' && file_exists($filepath.DS.$row2->tasawaqs_image)){
				  $file2 = $filepath.DS.$row2->tasawaqs_image;
				  unlink($file2);
			   } 
			   
			   if(isset($_REQUEST['del_tasawaqs_attachment']) && $_REQUEST['del_tasawaqs_attachment'] == '1' && file_exists($filepath.DS.$row2->tasawaqs_attachment)){
				  $file3 = $filepath.DS.$row2->tasawaqs_attachment;
				  unlink($file3);
			   } 
		  } 
    }
	
	$mainframe->redirect($linkSuccess, JText::_('Tasawaq has been saved.')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewTasawaq($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
	$row2 = new jlist_tasawaqs($database);
    $row2->load( $cirid );
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
    //$categories = join(",", $cirid);
    $linkSuccess = "index.php?option=com_eposts&view=listtasawaqs&Itemid=153";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_tasawaqs WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }
	
	           if(file_exists($filepath.DS.$row2->tasawaqs_p_logo)){
				  $file1 = $filepath.DS.$row2->tasawaqs_p_logo;
				  unlink($file1);	
			   }
			   
			   if(file_exists($filepath.DS.$row2->tasawaqs_image)){
				  $file2 = $filepath.DS.$row2->tasawaqs_image;
				  unlink($file2);
			   } 
			   
			   if(file_exists($filepath.DS.$row2->tasawaqs_attachment)){
				  $file3 = $filepath.DS.$row2->tasawaqs_attachment;
				  unlink($file3);
			   } 

    $msg = $total .JText::_('Tasawaq has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

//envents
function listEvents($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listEvents($option, $view);
}

function newEvent($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newEvent($option, $view);
}

function saveNewEvent($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	$row = new jlist_events($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	 
    if($cirid){
		$row2 = new jlist_events($database);
        $row2->load( $cirid );
	}
	/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.$jlistConfig['files.uploaddir'])){
		   mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   }
////////////////////////////////////////////////////////////////////

    $row->events_description = trim($row->events_description);
	$link = "index.php?option=com_eposts&view=newevent&Itemid=157&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listevents&Itemid=157";
	
    /*$filename1 = $_FILES['event_logo']['name'];
    $fileTemp1 = $_FILES['event_logo']['tmp_name'];
    $uploadPath1 = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename1;
	if ($filename1 != '') {
    	//JFile::upload($fileTemp1, $uploadPath1);
		copy($fileTemp1, $uploadPath1);
		$row->event_logo = $jlistConfig['files.uploaddir'].DS.$filename1;
		if(is_file($row2->event_logo)){
		 $file1 = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$row2->event_logo;
	     unlink($file1);
		}
	}
	
    $filename2 = $_FILES['event_image']['name'];
    $fileTemp2 = $_FILES['event_image']['tmp_name'];
    $uploadPath2 = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename2;
	if ($filename2 != '') {
    	//JFile::upload($fileTemp2, $uploadPath2);
		copy($fileTemp2, $uploadPath2);
		$row->event_image = $jlistConfig['files.uploaddir'].DS.$filename2;;
		if(is_file($row2->event_image)){
		 $file1 = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$row2->event_image;
	     unlink($file1);
		}
	}*/

	/////////////////////////////////file upload////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////

   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
 
   $fieldName = 'event_logo';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }


    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->event_logo = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	
	endif;
	
   $fieldName = 'event_image';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->event_image = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	
	endif;
	
	
	// store it in the db
	//$row->events_enddate .= ' 23:59:00';
	$row->events_startdate = date('Y-m-d H:m:s', strtotime($row->events_startdate));
	
	$row->events_enddate = date('Y-m-d H:m:s', strtotime($row->events_enddate));
	
	if(isset($_REQUEST['del_event_logo']) && $_REQUEST['del_event_logo'] == '1'  && $_FILES['event_logo']['name'] == ''){
	  $row->event_logo = '';	
	}
	
	if(isset($_REQUEST['del_event_image']) && $_REQUEST['del_event_image'] == '1'  && $_FILES['event_image']['name'] == ''){
	  $row->event_image = '';	
	}
	
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
		if($cirid){
			//if(isset($_FILES['event_logo']['name']) && $_FILES['event_logo']['name'] != '' && file_exists($filepath.DS.$row2->event_logo)){
		     if(isset($_REQUEST['del_event_logo']) && $_REQUEST['del_event_logo'] == '1' && file_exists($filepath.DS.$row2->event_logo)){
				$file1 = $filepath.DS.$row2->event_logo;
				unlink($file1);	
			 }
			 
			 //if(isset($_FILES['event_image']['name']) && $_FILES['event_image']['name'] != '' && file_exists($filepath.DS.$row2->event_image)){
			 if(isset($_REQUEST['del_event_image']) && $_REQUEST['del_event_image'] == '1' && file_exists($filepath.DS.$row2->event_image)){	 
				$file2 = $filepath.DS.$row2->event_image;
				unlink($file2);
			 } 
        } 
    }
    
$mainframe->redirect($linkSuccess, JText::_('Event has been saved.')." ");

///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewEvent($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
	
	$row2 = new jlist_events( $database );
    $row2->load( $cirid );
	
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
	
    //$categories = join(",", $cirid);
    $linkSuccess = "index.php?option=com_eposts&view=listevents&Itemid=157";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_events WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }
	if(file_exists($row2->event_image)){
		 $file1 = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$row2->event_image;
	     unlink($file1);
	}
	if(file_exists($row2->event_logo)){
		 $file1 = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$row2->event_logo;
	     unlink($file1);
	}

    $msg = $total .JText::_('Events has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

//DMS Documents Management System
function listDocuments($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listDocuments($option, $view);
}

function newDocument($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$breadcrumbs =& $mainframe->getPathWay();
    $breadcrumbs->addItem(JText::_('COM_EPOSTS_DOCUMENTS_NEW_PAGE_TITLE'), JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newdocument'));
	jlist_HTML::newDocument($option, $view);
}

function saveNewDocument($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $row = new jlist_dms($database);
	
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	if($cirid){
		$row2 = new jlist_dms($database);
        $row2->load( $cirid );
	}
	
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
	 
	/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.'edms')){
		   mkdir($filepath.DS.'edms');
	   }
    ////////////////////////////////////////////////////////////////////

    $row->dms_description = trim($row->dms_description);
	
	$link = "index.php?option=com_eposts&view=newdocument&Itemid=340&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listdocuments&Itemid=340";
	/////////////////////////////////file upload////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
 //global $mainframe, $jlistConfig;
   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
   
   $fieldName = 'dms_file';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
 	/////////////////////////////
/*	if (!is_dir(JPATH_SITE.DS.'edms')){
           if (!JFolder::create(JPATH_SITE.DS.'edms', 0755)){
               $mainframe->redirect($link, JText::_( 'File permissions denied.' )." ");    
           }    
        }*/
	$newDir = mb_substr($_FILES[$fieldName]['name'], 0, 1);
	$myChars = array("a", "b", "c", "d", "e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");

//if (in_array("23",$people, TRUE))//TRUE = Case sensitive
	if(!in_array($newDir,$myChars)){
	  $newDir = rand(0,9).rand(0,9).rand(0,9);
	}
		
	if (!is_dir($filepath.DS.'edms'.DS.$newDir)){
           //if (!JFolder::create(JPATH_SITE.DS.'edms'.DS.$newDir, 0755)){
		       mkdir($filepath.DS.'edms'.DS.$newDir);
               //$mainframe->redirect($link, JText::_( 'File permissions denied.' )." ");    
           //}    
        }
	$newPath = $filepath.DS.'edms'.DS.$newDir;
		//exit;
/////////////////////////////
    // replace special chars in filename?
    $filename_new = checkFileName($fileName,$newPath);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($newPath.DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
 
   $fileTemp = $_FILES[$fieldName]['tmp_name'];
   

//$uploadPath = JPATH_SITE.DS.'edms'.DS.$newDir.DS.$_FILES[$fieldName]['name'];
    //$newDir = getMyDirectoryName();//diectory name should be in english alphabets
    $uploadPath = $newPath.DS.$fileName;
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {

		///////////////
		
		//$row->dms_path = $uploadPath;
		$row->dms_path = 'edms'.DS.$newDir.DS.$fileName;
		$row->dms_title = $_FILES[$fieldName]['name'];
		/*print_r($row);
		exit;*/
		/*$fileName = $_FILES[$fieldName]['name'];
        $uploadedFileNameParts = explode('.',$fileName);
        $uploadedFileExtension = array_pop($uploadedFileNameParts);*/
		//////docs///////////////
		$invalidFileExts = explode(',', 'docs');
		$valid_ext = false;
		foreach($invalidFileExts as $key => $value){
			if( preg_match("/$value/i", $uploadedFileExtension )){
				$valid_ext = true;
			}
		}
		if ($valid_ext == true){
			//Grabe data
			$row->dms_file_data = read_file_docx($uploadPath);
		}
		
		/////////////doc///////////////////////
		$invalidFileExts = explode(',', 'doc');
		$valid_ext = false;
		foreach($invalidFileExts as $key => $value){
			if( preg_match("/$value/i", $uploadedFileExtension )){
				$valid_ext = true;
			}
		}
		if ($valid_ext == true){
			//Grabe data
			$row->dms_file_data = parseWord($uploadPath);
		}
		
		$invalidFileExts = explode(',', 'txt');
		$valid_ext = false;
		foreach($invalidFileExts as $key => $value){
			if( preg_match("/$value/i", $uploadedFileExtension )){
				$valid_ext = true;
			}
		}
		if ($valid_ext == true){
			//Grabe data
			$row->dms_file_data = file_get_contents($uploadPath, true);
		}

    }
	endif;
		    // store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
			if($cirid){
				 if(isset($_FILES['dms_file']['name']) && $_FILES['dms_file']['name'] != ''){
					$file1 = $filepath.DS.$row2->dms_path;
					unlink($file1);	
				 }
			}
    }
    
$mainframe->redirect($linkSuccess, JText::_('Document has been saved.')." ");
	//////////////////
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function parseWord($userDoc) 
{
    $fileHandle = fopen($userDoc, "r");
    $line = @fread($fileHandle, filesize($userDoc));   
    $lines = explode(chr(0x0D),$line);
    $outtext = "";
    foreach($lines as $thisline)
      {
        $pos = strpos($thisline, chr(0x00));
        if (($pos !== FALSE)||(strlen($thisline)==0))
          {
          } else {
            $outtext .= $thisline." ";
          }
      }
     $outtext = preg_replace("/[^a-zA-Z0-9\s\,\.\-\n\r\t@\/\_\(\)]/","",$outtext);
    return $outtext;
} 

function read_file_docx($filename){

    $striped_content = '';
    $content = '';

    if(!$filename || !file_exists($filename)) return false;

    $zip = zip_open($filename);

    if (!$zip || is_numeric($zip)) return false;

    while ($zip_entry = zip_read($zip)) {

        if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

        if (zip_entry_name($zip_entry) != "word/document.xml") continue;

        $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

        zip_entry_close($zip_entry);
    }// end while

    zip_close($zip);

    //echo $content;
    //echo "<hr>";
    //file_put_contents('1.xml', $content);

    $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
    $content = str_replace('</w:r></w:p>', "\r\n", $content);
    $striped_content = strip_tags($content);

    return $striped_content;
}
	   
function deleteNewDocument($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
	 
    if($cirid){
		$row2 = new jlist_dms($database);
        $row2->load( $cirid );
	}
    //$categories = join(",", $cirid);
	$linkSuccess = "index.php?option=com_eposts&view=listdocuments&Itemid=340";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_dms WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }
	$file1 = $filepath.DS.$row2->dms_path;
	unlink($file1);

    $msg = $total .JText::_('Document has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

//Medialibrary
function myMedia($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$breadcrumbs =& $mainframe->getPathWay();
        $breadcrumbs->addItem(JText::_('COM_EPOSTS_DWNMEDIAS_LIST_PAGE_TITLE_FRONT'), JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=media'));
	jlist_HTML::myMedia($option, $view);
}	   
	   
function listMedia($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listMedia($option, $view);
}

function newMedia($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newMedia($option, $view);
}

function saveMedia($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	$row = new jlist_dwnmedias($database);
    // bind it to the table
   
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	  if($cirid){
		  $row2 = new jlist_dwnmedias($database);
		  $row2->load( $cirid );
	  }
	  
	   // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
	/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.$jlistConfig['files.uploaddir'])){
		   mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   }
   /////////////////////////////////////////////////////////////////

    $row->media_description = trim($row->media_description);
    $link = "index.php?option=com_eposts&view=newmedia&Itemid=154&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listmedia&Itemid=154";
	
	$row->media_description = trim($row->media_description);
	/*$circular_file = $padfile = JArrayHelper::getValue($_FILES,'circulars_file',array('tmp_name'=>''));
	print_r($circular_file);
	exit;*/
	
	/////////////////////////////////file upload////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
 //global $mainframe, $jlistConfig;
   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
 
   $fieldName = 'media_file';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
   
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		/*echo $uploadPath;
        echo 'chheena';
		exit;*/
		///////////////
		
		$row->media_document = $jlistConfig['files.uploaddir'].DS.$fileName;
	}
endif;	
	// store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
		if($cirid){
		     if(isset($_FILES['media_file']['name']) && $_FILES['media_file']['name'] != ''){
				$file1 = $filepath.DS.$row2->media_document;
				unlink($file1);	
		     }
		}
    }
	
	
	
	$mainframe->redirect($linkSuccess, JText::_('Media has been saved.')." ");
		//////////////////
    
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteMedia($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
	
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
	
	$row = new jlist_dwnmedias( $database );
    $row->load( $cirid );
	
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listmedia&Itemid=154";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_dwnmedias WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }
	
    $file = $filepath.DS.$row->media_document;
	unlink($file);
	
    $msg = $total .JText::_('Media has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

/*Complaints*/
function newComplaints($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newComplaints($option, $view);
}

function listComplaints($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listComplaints($option, $view);
}

function detailComplaints($option,$view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::detailComplaints($option, $view);
}

function saveComplaints($option, $cid, $apply=0){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	$user = &JFactory::getUser();

    $row->employeeid = JRequest::getString('employeeid');
    $row->name = JRequest::getString('name');
	$row->designation = JRequest::getString('designation');
	$row->section = JRequest::getString('section');
	$row->details = JRequest::getString('details');
	$row->department = JRequest::getString('department');
	$row->for_department = JRequest::getString('for_department');
	
	$query = 'INSERT INTO #__eposts_scgdata (`id`, `employeeid`, `name`, `designation`, `user_deptid`,  `section`, `details`, `for_deptid`, `created`, `type`) VALUES (NULL, "'.htmlentities($row->employeeid).'", "'.htmlentities($row->name).'", "'.htmlentities($row->designation).'", "'.htmlentities($row->department).'", "'.htmlentities($row->section).'", "'.htmlentities($row->details).'", "'.htmlentities($row->for_department).'", NOW(), "3")';
	
	$database->setQuery($query);
	$database->query();
	$lastid = $database->insertid();
	
	$database->setQuery("SELECT * "
		  . "\n FROM #__eposts_departments "
		  . "\n WHERE id = " . $row->department
	  );
   
	 $objmydepartment = $database->loadObject();
	 $myDepartmentName = $objmydepartment->departments_name;
	
	 $database->setQuery("SELECT * "
		  . "\n FROM #__eposts_departments "
		  . "\n WHERE id = " . $row->for_department
	  );
   
	 $objdepartment = $database->loadObject();
	 $receiverEmail = $objdepartment->departments_email;
	 $receiverDepartmentName = $objdepartment->departments_name;
	 
	 /////////////////////
	 $mailSubject = "Complaint Against ".$receiverDepartmentName;
	 
	 $text = "\n\n<p><b>Hi,</b></p>";
	 $text .= "<p>A new Complaint has been submitted by ".$row->name.". Kindly <a href='".JRoute::_(JURI::base().'index.php?option=com_eposts&view=detailcomplaints&Itemid=364&cirid='.$lastid.'&lang=en')."'>click here</a> to view the detail of this Complaint.</p>";
	 $text .= "<br><br><br><p>Thank you</p>";
	 $text .= "<br><br><p><b>Note:</b> This is a system generated mail. Please do not reply to this email address.</p>";
	 
  
	$html_format = true;
    $success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);
	//$success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
	 /////////////////////

	$linkSuccess = "index.php?option=com_eposts&view=complaints&Itemid=250";
		    // store it in the db
    if (empty($receiverEmail) && $success) {
        echo "<script> alert('"."unable to send email. Please contact system administrator to fix the issue!"."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        $mainframe->redirect($linkSuccess, JText::_('COM_EPOSTS_COMPLAINTS_SENT')." ");
    }
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

/*Grievances*/
function newGrievances($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newGrievances($option, $view);
}

function listGrievances($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listGrievances($option, $view);
}

function detailGrievances($option,$view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::detailGrievances($option, $view);
}

function saveGrievances($option, $cid, $apply=0){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	$user = &JFactory::getUser();

    $row->employeeid = JRequest::getString('employeeid');
    $row->name = JRequest::getString('name');
	$row->designation = JRequest::getString('designation');
	$row->section = JRequest::getString('section');
	$row->details = JRequest::getString('details');
	$row->department = JRequest::getString('department');
	$row->for_department = JRequest::getString('for_department');
	
	$query = 'INSERT INTO #__eposts_scgdata (`id`, `employeeid`, `name`, `designation`, `user_deptid`,  `section`, `details`, `for_deptid`, `created`, `type`) VALUES (NULL, "'.htmlentities($row->employeeid).'", "'.htmlentities($row->name).'", "'.htmlentities($row->designation).'", "'.htmlentities($row->department).'", "'.htmlentities($row->section).'", "'.htmlentities($row->details).'", "'.htmlentities($row->for_department).'", NOW(), "2")';
	
	$database->setQuery($query);
	$database->query();
	$lastid = $database->insertid();
	
	$database->setQuery("SELECT * "
		  . "\n FROM #__eposts_departments "
		  . "\n WHERE id = " . $row->department
	  );
   
	 $objmydepartment = $database->loadObject();
	 $myDepartmentName = $objmydepartment->departments_name;
	
	 $database->setQuery("SELECT * "
		  . "\n FROM #__eposts_departments "
		  . "\n WHERE id = " . $row->for_department
	  );
   
	 $objdepartment = $database->loadObject();
	 $receiverEmail = $objdepartment->departments_email;
	 $receiverDepartmentName = $objdepartment->departments_name;
	 
	 /////////////////////
	 $mailSubject = "Grievances for ".$receiverDepartmentName;
	 
	 $text = "\n\n<p><b>Hi,</b></p>";
	 $text .= "<p>A new Grievance has been submitted by ".$row->name.". Kindly <a href='".JRoute::_(JURI::base().'index.php?option=com_eposts&view=detailgrievances&Itemid=363&cirid='.$lastid.'&lang=en')."'>click here</a> to view the detail of this Grievance.</p>";
	 $text .= "<br><br><br><p>Thank you</p>";
	 $text .= "<br><br><p><b>Note:</b> This is a system generated mail. Please do not reply to this email address.</p>";
	 
  
	$html_format = true;
    $success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);
	//$success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
	 /////////////////////

	$linkSuccess = "index.php?option=com_eposts&view=grievances&Itemid=251";
		    // store it in the db
    if (empty($receiverEmail) && $success) {
        echo "<script> alert('"."unable to send email. Please contact system administrator to fix the issue!"."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        $mainframe->redirect($linkSuccess, JText::_('COM_EPOSTS_GRIEVANCES_SENT')." ");
    }
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

/*Suggestions*/
function newSuggestions($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newSuggestions($option, $view);
}

function listSuggestions($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listSuggestions($option, $view);
}

function detailSuggestions($option,$view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::detailSuggestions($option, $view);
}

function saveSuggestions($option, $cid, $apply=0){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	$user = &JFactory::getUser();

    $row->employeeid = JRequest::getString('employeeid');
    $row->name = JRequest::getString('name');
	$row->designation = JRequest::getString('designation');
	$row->section = JRequest::getString('section');
	$row->details = JRequest::getString('details');
	$row->department = JRequest::getString('department');
	$row->for_department = JRequest::getString('for_department');
	
	$query = 'INSERT INTO #__eposts_scgdata (`id`, `employeeid`, `name`, `designation`, `user_deptid`,  `section`, `details`, `for_deptid`, `created`, `type`) VALUES (NULL, "'.htmlentities($row->employeeid).'", "'.htmlentities($row->name).'", "'.htmlentities($row->designation).'", "'.htmlentities($row->department).'", "'.htmlentities($row->section).'", "'.htmlentities($row->details).'", "'.htmlentities($row->for_department).'", NOW(), "1")';
	
	$database->setQuery($query);
	$database->query();
	$lastid = $database->insertid();
	
	$database->setQuery("SELECT * "
		  . "\n FROM #__eposts_departments "
		  . "\n WHERE id = " . $row->department
	  );
   
	 $objmydepartment = $database->loadObject();
	 $myDepartmentName = $objmydepartment->departments_name;
	
	 $database->setQuery("SELECT * "
		  . "\n FROM #__eposts_departments "
		  . "\n WHERE id = " . $row->for_department
	  );
   
	 $objdepartment = $database->loadObject();
	 $receiverEmail = $objdepartment->departments_email;
	 $receiverDepartmentName = $objdepartment->departments_name;
	 
	 /////////////////////
	 $mailSubject = "Suggestions for ".$receiverDepartmentName;
	 
	 $text = "\n\n<p><b>Hi,</b></p>";
	 $text .= "<p>A new Suggestion has been submitted by ".$row->name.". Kindly <a href='".JRoute::_(JURI::base().'index.php?option=com_eposts&view=detailsuggestions&Itemid=362&cirid='.$lastid.'&lang=en')."'>click here</a> to view the detail of this Suggestion.</p>";
	 $text .= "<br><br><br><p>Thank you</p>";
	 $text .= "<br><br><p><b>Note:</b> This is a system generated mail. Please do not reply to this email address.</p>";
	 
	$html_format = true;
    $success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);
	//$success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
	 /////////////////////
		
	$linkSuccess = "index.php?option=com_eposts&view=suggestions&Itemid=249";
		    // store it in the db
    if (empty($receiverEmail) && $success) {
        echo "<script> alert('"."unable to send email. Please contact system administrator to fix the issue!"."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        $mainframe->redirect($linkSuccess, JText::_('COM_EPOSTS_SUGGESTIONS_SENT')." ");
    }
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}


//////////////////////////////
function approveBookingRequest($option, $view){
	global $jlistConfig, $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$cirid = (int)JRequest::getString('cirid', 0);
	$user = &JFactory::getUser();
	
	$row = new jlist_bookingrequest( $database );
    $row->load( $cirid );
	
	$row->approvedby = 425;//$user->id;
	$row->approvedon = date($jlistConfig['global.datetime']);
	$row->booking_status = '1';
	
	$query = "SELECT user_id FROM #__eposts_bookingrequest_members
                             WHERE bookingrequest_id = '".$cirid."'
                              ";
	$database = &JFactory::getDBO();
	$database->SetQuery($query);
	$rowsUsers = $database->loadObjectList();
	
						  	/////////////////////
					  //$html_format = true;
$startDate = $row->bookingdate;
$startDate = str_replace("-", "", $startDate);
$endDate = $startDate;

$database->setQuery("SELECT bookingslots_description FROM #__eposts_bookingslots WHERE id IN (SELECT MIN(slots_id) FROM  #__eposts_bookingrequest_slots WHERE bookingrequest_id='".$cirid."')");
$startTime = $database->loadResult();

$database->setQuery("SELECT bookingslots_description_ar FROM #__eposts_bookingslots WHERE id IN (SELECT MAX(slots_id) FROM #__eposts_bookingrequest_slots WHERE bookingrequest_id='".$cirid."')");
$endTime = $database->loadResult();

$startDate .= 'T'.$startTime;
$endDate .= 'T'.$endTime;
						   
	foreach($rowsUsers as $rowsUser){
		   $where = ' WHERE 1 = 1  && id = "'.$rowsUser->user_id.'" ';
		   $database->SetQuery( "SELECT email FROM #__users".$where);
			  $memdtl = $database->loadResult();

	  if($row->booking_status && !empty($memdtl)){
		  
			/////////////////////
			
$html_format  = 'MIME-Version: 1.0' . "\r\n";
$html_format .= 'Content-type: text/Calendar; charset=iso-8859-1' . "\r\n";
//$headers .= "Content-Disposition: inline; filename=calendar.ics";

// Additional headers
$html_format .= 'From: '.$user->email.' <'.$user->email.'>' . "\r\n";

$message  = "BEGIN:VCALENDAR\n";
//$message .= "VERSION:2.0\n";
//$message .= "PRODID:-//Foobar Corporation//NONSGML Foobar//EN\n";
//$message .= "METHOD:REQUEST\n"; // requied by Outlook
$message .= "BEGIN:VEVENT\n";
//$message .= "UID:".date('Ymd').'T'.date('His')."-".rand()."-example.com\n"; // required by Outlok
//$message .= "DTSTAMP:".date('Ymd').'T'.date('His')."\n"; // required by Outlook
//$message .= "DTSTART:20130125T000000\n"; 
//$message .= "DTEND:20130127T000000\n"; 
$message .= "DTSTART:".$startDate."\n"; 
$message .= "DTEND:".$endDate."\n"; 
$message .= "SUMMARY:".$row->booking_title."\n";
$message .= "DESCRIPTION:".$row->booking_description."\n";
$message .= "END:VEVENT\n";
$message .= "END:VCALENDAR\n";
						   $receiverEmail = $memdtl;
						   $mailSubject = $row->booking_title;						   
						   $text = $message. $row->booking_description;
			  //$success = JUtility::sendMail($user->email, $mailSubject, NULL, NULL, $text, $html_format, NULL, $receiverEmail);
			  $success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);
			  //$success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
		/////////////////////

	  }
	}
	
	$query = "SELECT publishedby FROM #__eposts_bookingrequest
                             WHERE id = '".$cirid."'
                              ";
	$database = &JFactory::getDBO();
	$database->SetQuery($query);
	$rowsUsers = $database->loadObjectList();
	foreach($rowsUsers as $rowsUser){
		   $where = ' WHERE 1 = 1  && id = "'.$rowsUser->publishedby.'" ';
		   $database->SetQuery( "SELECT email, name FROM #__users".$where);
			  $memdtl = $database->loadObject();

		$database->SetQuery( "SELECT resources_title FROM #__eposts_resources WHERE id = '".$row->resourceid."'");
		$resource = $database->loadResult();
	  if($row->booking_status && !empty($memdtl)){
		  
			/////////////////////
			   $receiverEmail = $memdtl->email;
			    $mailSubject = 'Your resource booking request has been approved';						   
				//$text = '<p>Booking Title:\n '.$row->booking_title.'</p>\n<p>'.$row->booking_description.'</p>';					
						  	$text = "Dear ".$memdtl->name.",";
							$text .= "<br><br><p>Your request for booking the &quot;".$resource."&quot; has been approved.</p>";
							$text .= "<br><br><p>Thanks</p>";
							$text .= "<br><br><p><b>Note:</b> This is a system generated email. Please do not reply to this email address.</p>";	
			  $html_format = true;
			  //$success = JUtility::sendMail($user->email, $mailSubject, NULL, NULL, $text, $html_format, NULL, $receiverEmail);
			  $success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);
			  //$success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
		/////////////////////

	  }
	  
	}

		///////////////////////////////////////
		    // store it in the db
    
	if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
	}
	
	$linkSuccess = "index.php?option=com_eposts&view=approvebookingrequest&Itemid=193";
	$mainframe->redirect($linkSuccess, JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_APPROVED_ACTION')." ");
}

function rejectBookingRequest($option, $view){
	global $jlistConfig, $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$user = &JFactory::getUser();
	$cirid = (int)JRequest::getString('cirid', 0);
	
	$row = new jlist_bookingrequest( $database );
    $row->load( $cirid );
	
	$row->approvedby = $user->id;
	$row->approvedon = date($jlistConfig['global.datetime']);
	$row->booking_status = '2';
	
	$query = "SELECT publishedby FROM #__eposts_bookingrequest
                             WHERE id = '".$cirid."'
                              ";
	$database = &JFactory::getDBO();
	$database->SetQuery($query);
	$rowsUsers = $database->loadObjectList();
	foreach($rowsUsers as $rowsUser){
		   $where = ' WHERE 1 = 1  && id = "'.$rowsUser->publishedby.'" ';
		   $database->SetQuery( "SELECT email, name FROM #__users".$where);
			  $memdtl = $database->loadObject();

		$database->SetQuery( "SELECT resources_title FROM #__eposts_resources WHERE id = '".$row->resourceid."'");
		$resource = $database->loadResult();
	  if($row->booking_status && !empty($memdtl)){
		  
			/////////////////////
			   $receiverEmail = $memdtl->email;
			    $mailSubject = 'Your resource booking request has been canceled';						   
				//$text = '<p>Booking Title:\n '.$row->booking_title.'</p>\n<p>'.$row->booking_description.'</p>';		
						  	$text = "Dear ".$memdtl->name.",";
							$text .= "<br><br><p>Your request for booking the &quot;".$resource."&quot; has been rejected.</p>";
							$text .= "<br><br><p>Thanks</p>";
							$text .= "<br><br><p><b>Note:</b> This is a system generated email. Please do not reply to this email address.</p>";					
			  $html_format = true;
			  //$success = JUtility::sendMail($user->email, $mailSubject, NULL, NULL, $text, $html_format, NULL, $receiverEmail);
			  $success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);
			  //$success = JUtility::sendMail($mailfrom, $mailfromname, $first_adress, $betreff, $text, $html_format, null, $empfaenger);
		/////////////////////

	  }
	}
	///////////////////////////////////////
		    // store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
	}	
	$linkSuccess = "index.php?option=com_eposts&view=approvebookingrequest&Itemid=193";
	$mainframe->redirect($linkSuccess, JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_REJECTED_ACTION')." ");
}

function viewApproveBookingRequest($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::viewApproveBookingRequest($option, $view);
}

function viewBookingRequest($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::viewBookingRequest($option, $view);
}

////////////////////////////New Pages/////////////////////////////
function mainListCirculars($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListCirculars($option, $view);
}

function mainShowCirculars($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowCirculars($option, $view);
}

function mainShowTasawaqs($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowTasawaqs($option, $view);
}

function mainListTasawaqs($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListTasawaqs($option, $view);
}

function mainShowEvents($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowEvents($option, $view);
}

function mainListEvents($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListEvents($option, $view);
}

function mainListMedias($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListMedias($option, $view);
}

function mainListDocuments($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListDocuments($option, $view);
}

function mainShowDocuments($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowDocuments($option, $view);
}


function mainListNewHires($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListNewHires($option, $view);
}

function mainShowNewHires($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowNewHires($option, $view);
}



////////////////////////////////////////////////////

function mainListStaffDirectories($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListStaffDirectories($option, $view);
}

function mainShowStaffDirectories($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowStaffDirectories($option, $view);
}

function mainListOrgCharts($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListOrgCharts($option, $view);
}

function mainShowOrgCharts($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowOrgCharts($option, $view);
}

function mainListLinks($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListLinks($option, $view);
}

function mainShowEmpInfo($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$breadcrumbs =& $mainframe->getPathWay();
    $breadcrumbs->addItem(JText::_('COM_EPOSTS_MAIN_SHOW_EMPINFO_LIST_PAGE_TITLE'), JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowempinfo'));
	jlist_HTML::mainShowEmpInfo($option, $view);
}

function mainAddEmpInfo($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$breadcrumbs =& $mainframe->getPathWay();
    $breadcrumbs->addItem(JText::_('COM_EPOSTS_MAIN_SHOW_ADDEMPINFO_LIST_PAGE_TITLE'), JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainaddempinfo'));
	jlist_HTML::mainAddEmpInfo($option, $view);
}


function mainShowSlider() {
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="<?php print 'templates/emiratespost/css/wheel.css'; ?>">
<script src="<?php print 'templates/emiratespost/javascript/jquery.js'; ?>"></script>
<script src="<?php print 'templates/emiratespost/javascript/jqwheel.js'; ?>"></script>
                <script>
                        jQuery.noConflict();
                        jQuery(document).ready(function ($) {
							$('#tabsbtm_cn #pic_tab').click(function(){
									$('#tabsbtm_cn #pictureGallery').load('<?php print JRoute::_('index.php?option=com_eposts&view=showsllider'); ?> .pictures_area').bind(function(){
									});
								});
								$('.pictures_area .contents_area').waterwheelCarousel({
									startingWaveSeparation: 0,
									centerOffset: 30,
									startingItemSeparation: 30,
									itemSeparationFactor: .7,
									itemDecreaseFactor: .75,
									opacityDecreaseFactor: 1
								});
                                $('.pictures_area img').click(function(){
									alert('d');
								});
							$("body").delegate("p", "click", function(){
								$(this).after("<span>Another paragraph!</span>");
							});
                        });
                </script>
</head>

<body><div class="abc" style="display:none;"><div class="pictures_area">
            <div class="contents_area">
                <div class="carousel-images">
                <?php                    
                    $where = ' WHERE parent = "1" AND published = "1"';
                    
                    $query = "SELECT * FROM #__igallery
                              $where
                              ORDER BY date ASC
                              LIMIT 10";
                    $database = &JFactory::getDBO();
                    $database->SetQuery($query);
                    $rows = $database->loadObjectList();
                    $k = 0;
                    for($i=0, $n=count( $rows ); $i < $n; $i++) {
                        $row = &$rows[$i];
                        $link = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->id);
                        
                        $title = $row->name;
                        
                    
                            $where = 'WHERE gallery_id = "'.$row->id.'" AND published = "1"';
                        
                            $query = "SELECT * FROM #__igallery_img
                                  $where
                                  ORDER BY date ASC
                                  LIMIT 1";
                        
                            $database->SetQuery($query);
                            $row = $database->loadObject();
                        
                        $image = $row->filename;
                        $name = substr($image, 0, strrpos($image, '.'));
                        $extnt = substr($image, strrpos($image, '.'));
                    ?>
                    <img height="120" src="<?php print JURI::base(); ?>images/igallery/resized/1-100/<?php print $name . '-240-150-80-c' . $extnt; ?>" />
                    <?php //print $extnt; ?>
                <?php $k = 1 - $k;  } ?>
                </div>
            </div>
        </div>
        </div>
</body>
</html>
    <?php
	exit;
}//end funtion mainShowSlider

	///////////////////////////////////Depevloment Phase IV ////////////////////////////

//Announcements
function listAnnouncements($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listAnnouncements($option, $view);
}

function newAnnouncements($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newAnnouncements($option, $view);
}

function saveNewAnnouncements($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	$row = new jlist_announcements($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	
	/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.$jlistConfig['files.uploaddir'])){
		   mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   }
	   /*mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   echo $filepath.DS.$jlistConfig['files.uploaddir'];
	   exit;*/
   ////////////////////////////////////////////////////////////////////
    
    $link = "index.php?option=com_eposts&view=newannouncements&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listannouncements&Itemid=152";

	jimport('joomla.filesystem.file');
	jimport('joomla.filesystem.folder');
 
	$fieldName = 'announcements_file';
	
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE =.75;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName ='far-'. $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
	
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_('INVALID image type! Only *.gif, *.jpg and *.png are allowed.')." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
   
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->announcements_file = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	endif;
	
	$fieldName = 'announcements_file_ar';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = .75;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = 'far-'.$_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
	
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
   
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->announcements_file_ar = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	endif;
	
	
	$fieldName = 'announcements2_file';
	
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE =.75;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName ='far-'. $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
	
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_('INVALID image type! Only *.gif, *.jpg and *.png are allowed.')." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
   
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->announcements2_file = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	endif;
	
	$fieldName = 'announcements2_file_ar';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = .75;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = 'far-'.$_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
	
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
   
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->announcements2_file_ar = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	endif;
	
	$fieldName = 'announcements3_file';
	
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE =.75;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName ='far-'. $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
	
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_('INVALID image type! Only *.gif, *.jpg and *.png are allowed.')." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
   
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->announcements3_file = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	endif;
	
	$fieldName = 'announcements3_file_ar';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = .75;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = 'far-'.$_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
	
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
   
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->announcements3_file_ar = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	endif;

		    // store it in the db
	$row->announcements_expirydate = date('Y-m-d', strtotime($row->announcements_expirydate));
	$row->announcements_expirydate .= ' 23:59:00';
	//print_r($row->announcements_expirydate);
	//exit;
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
    }
	
	$mainframe->redirect($linkSuccess, JText::_('Announcement has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewAnnouncements($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listannouncements&Itemid=152";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_announcements WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

    $msg = $total .JText::_('Announcement has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

//News
function listNews($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listNews($option, $view);
}

function newNews($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newNews($option, $view);
}

function saveNewNews($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	    $row = new jlist_news($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	if($cirid){
		$row2 = new jlist_news($database);
        $row2->load( $cirid );
	}
	/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.$jlistConfig['files.uploaddir'])){
		   mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   }
	   /*mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   echo $filepath.DS.$jlistConfig['files.uploaddir'];
	   exit;*/
   ////////////////////////////////////////////////////////////////////

    $row->news_description = trim($row->news_description);
    
    $link = "index.php?option=com_eposts&view=newnews&Itemid=150&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listnews&Itemid=150";
	
	/////////////////////////////////file upload////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////

   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
 
	$fieldName = 'news_file';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE =.75;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName ='far-'. $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
	
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_('INVALID image type! Only *.gif, *.jpg and *.png are allowed.')." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
   
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->news_image = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	endif;


   /*$fileName = $_FILES['news_file_ar']['name'];
   $fileTemp = $_FILES['news_file_ar']['tmp_name'];
   $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
   if ($fileName != '') :
	   JFile::upload($fileTemp, $uploadPath);
	   $row->news_file_ar = $fileName;
   endif;*/
   	$fieldName = 'news_file_ar';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = .75;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = 'far-'.$_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
	
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
   
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->news_file_ar = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	endif;

		    // store it in the db
	$row->news_newsdate = date('Y-m-d', strtotime($row->news_newsdate));
	$row->news_newsdate .= ' 23:59:00';
	
	           if(isset($_REQUEST['del_news_image']) && $_REQUEST['del_news_image'] == '1'  && $_FILES['news_image']['name'] == ''){
				  $row->news_image = '';
			   }
			   
			   if(isset($_REQUEST['del_news_file_ar']) && $_REQUEST['del_news_file_ar'] == '1'  && $_FILES['news_file_ar']['name'] == ''){
				  $row->news_file_ar='';
			   } 
			   
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
		  if($cirid){
			  
			   if(isset($_REQUEST['del_news_image']) && $_REQUEST['del_news_image'] == '1' && file_exists($filepath.DS.$row2->news_image)){
				  $file1 = $filepath.DS.$row2->news_image;
				  unlink($file1);	
			   }
			   
			   if(isset($_REQUEST['del_news_file_ar']) && $_REQUEST['del_news_file_ar'] == '1' && file_exists($filepath.DS.$row2->news_file_ar)){
				  $file2 = $filepath.DS.$row2->news_file_ar;
				  unlink($file2);
			   } 
		 }
    }

		define( '_JEXEC', 1 );

define( '_VALID_MOS', 1 );

define( 'JPATH_BASE', realpath(dirname(__FILE__)));

define( 'DS', DIRECTORY_SEPARATOR );

require_once ( JPATH_BASE .DS.'ir.php' );
	$mainframe->redirect($linkSuccess, JText::_('News has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewNews($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
	
	$row = new jlist_news( $database );
    $row->load( $cirid );
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
	
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listnews&Itemid=150";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_news WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }
	
	if(file_exists($filepath.DS.$row2->news_image)){
		$file1 = $filepath.DS.$row->news_image;
		unlink($file1);
		}
	if(file_exists($filepath.DS.$row2->news_file_ar)){
	    $file2 = $filepath.DS.$row->news_file_ar;	
	    unlink($file2);
	    }

    $msg = $total .JText::_('News has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}


//Visions
function listVisions($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listVisions($option, $view);
}

function newVisions($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newVisions($option, $view);
}

function saveNewVisions($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	 $row = new jlist_visions($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;

    $row->visions_description = trim($row->visions_description);
    
    $link = "index.php?option=com_eposts&view=newvisions&Itemid=242&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listvisions&Itemid=242";
	
	// store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
    }

	$mainframe->redirect($linkSuccess, JText::_('Vision has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewVisions($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listvisions&Itemid=242";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_visions WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

    $msg = $total .JText::_('Vision has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

//Objectives
function listObjectives($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listObjectives($option, $view);
}

function newObjectives($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newObjectives($option, $view);
}

function saveNewObjectives($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	 $row = new jlist_departments($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;

    $row->departments_description = trim($row->departments_description);
    
    $link = "index.php?option=com_eposts&view=newobjectives&Itemid=300&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listobjectives&Itemid=300";
	
	// store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
    }

	$mainframe->redirect($linkSuccess, JText::_('Objective has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewObjectives($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listobjectives&Itemid=300";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_departments WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

    $msg = $total .JText::_('Objective has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}


//Alerts
function listAlerts($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listAlerts($option, $view);
}

function newAlerts($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newAlerts($option, $view);
}

function saveNewAlerts($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
	$row = new jlist_alerts($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;

    $row->alerts_description = trim($row->alerts_description);
	
	$link = "index.php?option=com_eposts&view=newalerts&Itemid=243&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listalerts&Itemid=243";
	
	// store it in the db
	$row->alerts_startdate = date('Y-m-d', strtotime($row->alerts_startdate));
	
	$row->alerts_enddate = date('Y-m-d', strtotime($row->alerts_enddate));
	$row->alerts_enddate .= ' 23:59:00';
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
    }

	$mainframe->redirect($linkSuccess, JText::_('Alert has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewAlerts($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listalerts&Itemid=243";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_alerts WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

    $msg = $total .JText::_('Alert has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

//Policies
function listPolicies($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listPolicies($option, $view);
}

function newPolicies($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newPolicies($option, $view);
}

function saveNewPolicies($option, $cid, $apply=0){

  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $row = new jlist_policies($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	
	if($cirid){
		$row2 = new jlist_policies($database);
        $row2->load( $cirid );
	}
	
	/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.$jlistConfig['files.uploaddir'])){
		   mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   }
   ////////////////////////////////////////////////////////////////////
	
	$link = "index.php?option=com_eposts&view=newpolicies&Itemid=244&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listpolicies&Itemid=244";
	
	/////////////////////////////////file upload////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////

   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
 
   $fieldName = 'policies_file';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
	
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	/*$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        //HandleUploadError(JText::_( 'INVALID extension type!' ));
        //exit(0);
		$mainframe->redirect("index.php?option=".$option."&task=edit.policies&cid=".$row->id, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }*/
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
	
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->policies_doc = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	endif;
	/////////////////////////////////////
	$fieldName = 'policies_file_ar';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
	
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
	
	/*$invalidFileExts = explode(',', 'gif,jpg,jpeg,png');
	$valid_ext = false;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = true;
        }
    }
    if ($valid_ext == false){
        //HandleUploadError(JText::_( 'INVALID extension type!' ));
        //exit(0);
		$mainframe->redirect("index.php?option=".$option."&task=edit.policies&cid=".$row->id, JText::_( 'INVALID image type! Only *.gif, *.jpg and *.png are allowed.' )." ");
    }*/
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    

 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$row->policies_doc_ar = $jlistConfig['files.uploaddir'].DS.$fileName; //$uploadPath;
    }
	endif;

		    // store it in the db
	$row->policies_policydate = date('Y-m-d', strtotime($row->policies_policydate));
	$row->policies_policydate .= ' 23:59:00';
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
		  if($cirid){
			   if(isset($_FILES['policies_file']['name']) && $_FILES['policies_file']['name'] != ''){
				  $file1 = $filepath.DS.$row2->policies_doc;
				  unlink($file1);	
			   }
			   
			   if(isset($_FILES['policies_file_ar']['name']) && $_FILES['policies_file_ar']['name'] != ''){
				  $file2 = $filepath.DS.$row2->policies_doc_ar;
				  unlink($file2);
			   } 
		  }
    }

	$mainframe->redirect($linkSuccess, JText::_('Policy has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewPolicies($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
	 $config =& JFactory::getConfig();
     $filepath = $config->getValue( 'config.file_path' );
	   
	$row = new jlist_policies( $database );
    $row->load( $cirid );
	
	//print_r($file);
	//exit;
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listpolicies&Itemid=244";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_policies WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }
	$file = $filepath.DS.$row->policies_doc;
	$file2 = $filepath.DS.$row->policies_doc_ar;
	unlink($file);
	unlink($file2);

    $msg = $total .JText::_('Policy has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

//Quotes
function listQuotes($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listQuotes($option, $view);
}

function newQuotes($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newQuotes($option, $view);
}

function saveNewQuotes($option, $cid, $apply=0){
  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
   $row = new jlist_quotes($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	 if($cirid){
		$row2 = new jlist_quotes($database);
        $row2->load( $cirid );
	}
	/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.$jlistConfig['files.uploaddir'])){
		   mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   }
////////////////////////////////////////////////////////////////////
	
	$link = "index.php?option=com_eposts&view=newquotes&Itemid=245&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listquotes&Itemid=245";
///////////////////////////////////////////////////////
/////////////////////////////////file upload////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
 //global $mainframe, $jlistConfig;
   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
 
   $fieldName = 'quotes_file';
  if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''): 
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;

	if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		/*echo $uploadPath;
        echo 'chheena';
		exit;*/
		///////////////
		
		$row->quotes_file = $jlistConfig['files.uploaddir'].DS.$fileName;
	}
	endif;
////////////////////////////////////////////////////////

// store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
		if($cirid){
		     if(isset($_FILES['quotes_file']['name']) && $_FILES['quotes_file']['name'] != ''){
				 $file1 = $filepath.DS.$row2->quotes_file;
				  if(file_exists($file1)){	
				  unlink($file1);
				  }
			 }
		}
    }

	$mainframe->redirect($linkSuccess, JText::_('Quote has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewQuotes($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
	$row2 = new jlist_quotes($database);
    $row2->load( $cirid );
	
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
    //$categories = join(",", $cirid);
    $linkSuccess = "index.php?option=com_eposts&view=listquotes&Itemid=245";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_quotes WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }
	$file1 = $filepath.DS.$row2->quotes_file;
	if(file_exists($file1)){	
	   unlink($file1);
	}

    $msg = $total .JText::_('Quote has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

//Manuals
function listManuals($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listManuals($option, $view);
}

function newManuals($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newManuals($option, $view);
}

function saveNewManuals($option, $cid, $apply=0){
  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
   $row = new jlist_manuals($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	 if($cirid){
		$row2 = new jlist_manuals($database);
        $row2->load( $cirid );
	}
	
	/////////////////////////////New location Save///////////////////////
	   $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	   if(!is_dir($filepath.DS.$jlistConfig['files.uploaddir'])){
		   mkdir($filepath.DS.$jlistConfig['files.uploaddir']);
	   }
////////////////////////////////////////////////////////////////////
	
	$link = "index.php?option=com_eposts&view=newmanuals&Itemid=246&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listmanuals&Itemid=246";
/////////////////////////////////file upload////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
 //global $mainframe, $jlistConfig;
   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
 
   $fieldName = 'manuals_file';
  if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''): 
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;

	if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'INVALID extension type!' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		/*echo $uploadPath;
        echo 'chheena';
		exit;*/
		///////////////
		
		$row->manuals_doc = $jlistConfig['files.uploaddir'].DS.$fileName;
	}
	endif;
	////////////////////////////////////////////////////
	$fieldName = 'manuals_file_ar';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

	if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'INVALID extension type!' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists($filepath.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    //if(!JFile::upload($fileTemp, $uploadPath)){
	if(!copy($fileTemp, $uploadPath)){	
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect("index.php?option=".$option."&task=edit.manuals&cid=".$row->id, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		/*echo $uploadPath;
        echo 'chheena';
		exit;*/
		///////////////
		
		$row->manuals_doc_ar = $jlistConfig['files.uploaddir'].DS.$fileName;
	}
endif;
		    // store it in the db
	$row->manuals_manualdate = date('Y-m-d', strtotime($row->manuals_manualdate));
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
		  if($cirid){
			   if(isset($_FILES['manuals_file']['name']) && $_FILES['manuals_file']['name'] != ''){
				  $file1 = $filepath.DS.$row2->manuals_doc;
				  unlink($file1);	
			   }
			   
			   if(isset($_FILES['manuals_file_ar']['name']) && $_FILES['manuals_file_ar']['name'] != ''){
				  $file2 = $filepath.DS.$row2->manuals_doc_ar;
				  unlink($file2);
			   } 
		} 
    }
	$mainframe->redirect($linkSuccess, JText::_('Manual has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewManuals($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
	
	$row = new jlist_manuals( $database );
    $row->load( $cirid );
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
	
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listmanuals&Itemid=246";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_manuals WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }
    $file = $filepath.DS.$row->manuals_doc;
	$file2 = $filepath.DS.$row->manuals_doc_ar;
	unlink($file);
	unlink($file2);
    $msg = $total .JText::_('Manual has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

//Benchmarks
function listBenchmarks($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listBenchmarks($option, $view);
}

function newBenchmarks($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newBenchmarks($option, $view);
}

function saveNewBenchmarks($option, $cid, $apply=0){
  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
   $row = new jlist_benchmarks($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	
	$link = "index.php?option=com_eposts&view=newbenchmarks&Itemid=247&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listbenchmarks&Itemid=247";
	    // store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
    }

	$mainframe->redirect($linkSuccess, JText::_('Benchmark has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewBenchmarks($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listbenchmarks&Itemid=247";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_Benchmarks WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

    $msg = $total .JText::_('Benchmark has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

//FAQs
function listFaqs($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listFaqs($option, $view);
}

function newFaqs($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newFaqs($option, $view);
}

function saveNewFaqs($option, $cid, $apply=0){
  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
   $row = new jlist_faqs($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	
	$link = "index.php?option=com_eposts&view=newfaqs&Itemid=290&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listfaqs&Itemid=290";
	    // store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
    }

	$mainframe->redirect($linkSuccess, JText::_('FAQ has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewFaqs($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listfaqs&Itemid=290";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_faqs WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

    $msg = $total .JText::_('FAQ has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}


function mainShowFaqs($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowFaqs($option, $view);
}

function mainListFaqs($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListFaqs($option, $view);
}

//Links
function listLinks($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::listLinks($option, $view);
}

function newLinks($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::newLinks($option, $view);
}

function saveNewLinks($option, $cid, $apply=0){
  global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
  $row = new jlist_links($database);
    // bind it to the table
    if (!$row -> bind($_POST)) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    }
    $cirid = (int)JRequest::getString('cirid', 0);
    if($cirid) $row->id = $cirid;
	
	$link = "index.php?option=com_eposts&view=newlinks&Itemid=248&cirid=".$row->id;
	$linkSuccess = "index.php?option=com_eposts&view=listlinks&Itemid=248";
	    // store it in the db
    if (!$row -> store()) {
        echo "<script> alert('"
            .$row -> getError()
            ."'); window.history.go(-1); </script>\n";
        exit();
    } else {
        if(!$row->id) $row->id = mysql_insert_id();
    }

	$mainframe->redirect($linkSuccess, JText::_('Link has been saved')." ");
///////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}

function deleteNewLinks($option, $cid){
    global $mainframe, $jlistConfig;
    $database = &JFactory::getDBO();
    $cirid = (int)JRequest::getString('cirid', 0);
    $total = count( $cirid );
    //$categories = join(",", $cirid);
$linkSuccess = "index.php?option=com_eposts&view=listlinks&Itemid=248";
    //Delete Categories
    $database->SetQuery("DELETE FROM #__eposts_links WHERE id IN ($cirid)");
    if ( !$database->query() ) {
        echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
        exit();
    }

    $msg = $total .JText::_('Link has been deleted.')." ";
    $mainframe->redirect($linkSuccess, $msg );
}

/////////////////////////Main Pages/////////////////////////////
function mainShowAnnouncements($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	
	jlist_HTML::mainShowAnnouncements($option, $view);
}

function mainListAnnouncements($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListAnnouncements($option, $view);
}

function mainShowNews($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowNews($option, $view);
}

function mainListNews($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListNews($option, $view);
}

function mainShowVisions($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$breadcrumbs =& $mainframe->getPathWay();
       $breadcrumbs->addItem(JText::_('Visions'), JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowvisions'));
	jlist_HTML::mainShowVisions($option, $view);
}

function mainListVisions($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListVisions($option, $view);
}


function mainShowAlerts($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowAlerts($option, $view);
}

function mainListAlerts($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListAlerts($option, $view);
}

function mainShowPolicies($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowPolicies($option, $view);
}

function mainListPolicies($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListPolicies($option, $view);
}

function mainShowQuotes($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowQuotes($option, $view);
}

function mainListQuotes($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListQuotes($option, $view);
}

function mainShowManuals($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowManuals($option, $view);
}

function mainListManuals($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListManuals($option, $view);
}

function mainShowBenchmarks($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowBenchmarks($option, $view);
}

function mainListBenchmarks($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListBenchmarks($option, $view);
}

function empDownload() {
	global $jlistConfig, $Itemid, $mainframe;
	//file://hqdintdev2/media/albumSSSSS1.png
				$config =& JFactory::getConfig();
                $filepath = $config->getValue( 'config.file_path' );
	
	$filename = JRequest::getString('filename');
	
	$download_path = strstr($filename, '\\\\') ? $filename : $filepath . "\\" . $filename;
	
	$filename = explode('\\', $filename);
	$count = count($filename);
    $filename = $outputfile == '' ? $filename[$count-1] : $outputfile;
	
    // Detect missing filename
    if(!$filename) die("I'm sorry, you must specify a file name to download.");
   
    // Make sure we can't download files above the current directory location.
    if(eregi("\.\.", $filename)) die("I'm sorry, you may not download that file.");
    $file = str_replace("..", "", $filename);
   
    // Make sure we can't download .ht control files.
    if(eregi("\.ht.+", $filename)) die("I'm sorry, you may not download that file.");
   
    // Combine the download path and the filename to create the full path to the file.
   $file = "$download_path";
   
   
    // Test to ensure that the file exists.
    if(!file_exists($file)) die("I'm sorry, the file doesn't seem to exist.");
   
    // Extract the type of file which will be sent to the browser as a header
    $type = filetype($file);
  
    // Get a date and timestamp
    $today = date("F j, Y, g:i a");
    $time = time();

   

    
   // Send file headers
   // header("Content-type: $type");
	
    //header("Content-Disposition: attachment;filename=$filename");
   // header('Pragma: no-cache');
    //header('Expires: 0');
	
	header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
	

    // Send the file contents.
   // readfile($file);
	//exit;
	ob_clean();
	 flush();
    readfile($file);
    exit;
}

function empDownload1() {
	global $jlistConfig, $Itemid, $mainframe;
	//file://hqdintdev2/media/albumSSSSS1.png
				$config =& JFactory::getConfig();
                $filepath = $config->getValue( 'config.file_path' );
	
	$filename = JRequest::getString('filename');
	
	$download_path = strstr($filename, '\\\\') ? $filename : $filepath . "\\" . $filename;
	
	$filename = explode('\\', $filename);
	$count = count($filename);
    $filename = $outputfile == '' ? $filename[$count-1] : $outputfile;
	
    // Detect missing filename
    if(!$filename) die("I'm sorry, you must specify a file name to download.");
   
    // Make sure we can't download files above the current directory location.
    if(eregi("\.\.", $filename)) die("I'm sorry, you may not download that file.");
    $file = str_replace("..", "", $filename);
   
    // Make sure we can't download .ht control files.
    if(eregi("\.ht.+", $filename)) die("I'm sorry, you may not download that file.");
   
    // Combine the download path and the filename to create the full path to the file.
   $file = "$download_path";
   
   
    // Test to ensure that the file exists.
    if(!file_exists($file)) die("I'm sorry, the file doesn't seem to exist.");
   
    // Extract the type of file which will be sent to the browser as a header
    $type = filetype($file);
  
    // Get a date and timestamp
    $today = date("F j, Y, g:i a");
    $time = time();

   

    
   // Send file headers
   // header("Content-type: $type");
	
    //header("Content-Disposition: attachment;filename=$filename");
   // header('Pragma: no-cache');
    //header('Expires: 0');
	
	header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: inline; filename='.basename($file));
   
	header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
	

    // Send the file contents.
   // readfile($file);
	//exit;
	ob_clean();
	 flush();
    readfile($file);
    exit;
}

	//////////////////////////////END Depevloment Phase IV////////////////////////////////

///////////////////////////////////Depevloment Phase VI ////////////////////////////

function mainApplyJobs($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$breadcrumbs =& $mainframe->getPathWay();
       $breadcrumbs->addItem(JText::_('Job Application'), JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainapplyjobs'));
	jlist_HTML::mainApplyJobs($option, $view);
}

function saveApplyJobs($option, $view){
	global $mainframe, $jlistConfig;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
	
	$breadcrumbs =& $mainframe->getPathWay();
	$JobId = JRequest::getVar('JobId');
	$JobTitle = JRequest::getString('jobtitle');
	$staffName = JRequest::getString('staffName');
	$staffEmail = JRequest::getString('staffEmail');
	$staffMobile = JRequest::getString('staffMobile');
	$staffId = JRequest::getString('staffId');
	$initiatedById = JRequest::getString('initiatedById');
	$currentGrade = JRequest::getString('currentGrade');
	$currentSalary = JRequest::getString('currentSalary');
	$companyName = JRequest::getString('companyName');
	$department = JRequest::getString('department');
	$location = JRequest::getString('location');
	$nationality = JRequest::getString('nationality');
	$totalExperienceInYears = JRequest::getString('totalExperienceInYears', 0);
	
	$link = "index.php?option=com_eposts&view=mainapplyjobs&cirid=".$JobId;
	$linkSuccess = "index.php?option=com_eposts&view=mainlistjobsmessage&page=home&Itemid=303&jobtitle=".$JobTitle;

	


	
	/////////////////////////////////file upload////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
 //global $mainframe, $jlistConfig;
   jimport('joomla.filesystem.file');
   jimport('joomla.filesystem.folder');
 
   $fieldName = 'cv_file';
   if(isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != ''):
   $filesize = ($_FILES[$fieldName]['size']/1024)/1024;
   $POST_MAX_SIZE = 5;
    $unit = strtoupper(substr($POST_MAX_SIZE, -1));
    $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

    if ((int)$filesize > (int)$POST_MAX_SIZE) {
        $mainframe->redirect($link, JText::_( 'POST exceeded maximum allowed size.' )." ");
        exit;
    }
    
   $fileError = $_FILES[$fieldName]['error'];
   if ($fileError > 0) {
        switch ($fileError){
            case 1:
            //echo JText::_( 'FILE TO LARGE THAN PHP INI ALLOWS' );
			$mainframe->redirect($link, JText::_( 'File size is exceeded to limit' )." ");
            return;
            case 2:
            //echo  JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' );
			$mainframe->redirect($link, JText::_( 'FILE TO LARGE THAN HTML FORM ALLOWS' )." ");
            return;
            case 3:
            //echo  JText::_( 'ERROR PARTIAL UPLOAD' );
			$mainframe->redirect($link, JText::_( 'ERROR PARTIAL UPLOAD' )." ");
            return;
            case 4:
            //echo  JText::_( 'ERROR NO FILE' );
			$mainframe->redirect($link, JText::_( 'ERROR NO FILE' )." ");
            return;
        }
   }
 
   //check for filesize
   $fileSize = $_FILES[$fieldName]['size'];
   $limit = return_bytes(ini_get('upload_max_filesize'));
   if ($fileSize >  $limit){
        //echo JText::_( 'FILE BIGGER THAN ALLOWED' );
		$mainframe->redirect($link, JText::_( 'FILE BIGGER THAN ALLOWED' )." ");
   } 
 
    //check the file extension is ok
    $fileName = $_FILES[$fieldName]['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);
    $invalidFileExts = explode(',', 'php,php4,php5,html,htm');
    $valid_ext = true;
    foreach($invalidFileExts as $key => $value){
        if( preg_match("/$value/i", $uploadedFileExtension )){
            $valid_ext = false;
        }
    }
    if ($valid_ext == false){
        /*HandleUploadError(JText::_( 'INVALID extension type!' ));
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'INVALID extension type!' )." ");
    }
 
    // replace special chars in filename?
    $filename_new = checkFileName($fileName);
    // rename new file when it exists in this folder
    $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
    if ($only_name != ''){
        // filename is valid
        $file_extension = strrchr($filename_new,".");
        $num = 0;
        while (file_exists(JPATH_SITE.DS.$jlistConfig['files.uploaddir'].DS.$filename_new)){
              $filename_new = $only_name.$num++.$file_extension;
              if ($num > 5000) exit(0); 
        }
        $fileName = $filename_new; 
    } else {
        /*echo JText::_('COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME');
        exit(0);*/
		$mainframe->redirect($link, JText::_( 'COM_EPOSTS_BACKEND_FILESEDIT_INVALID_FILENAME' )." ");
    }    
	
   $config =& JFactory::getConfig();
   $filepath = $config->getValue( 'config.file_path' );
 
    $fileTemp = $_FILES[$fieldName]['tmp_name'];
    $uploadPath = $filepath.DS.$jlistConfig['files.uploaddir'].DS.$fileName;
 
    if(!copy($fileTemp, $uploadPath)){
        //echo JText::_( 'ERROR MOVING FILE' );
       // return;
		$mainframe->redirect($link, JText::_( 'ERROR MOVING FILE' )." ");
    } else {
		$cvFile = $uploadPath;
	    $cvFileName = $fileTemp;
	}
	endif;	
$handle = fopen($cvFile, "rb");
$fsize = filesize($cvFile);
$cvFile = fread($handle, $fsize);
fclose($handle);

///////////////////////////		

///////////////////////////		
 require_once dirname(__FILE__).'/nusoap/lib/nusoap.php';
			//$client = new nusoap_client("http://intranet/irecruitmentservice/WS_ApplicantManager.svc?wsdl", true);
			//$client = new nusoap_client("http://intranet/irecwstest/WS_ApplicantManager.svc?wsdl", true);
			$client = new nusoap_client("http://intranet/irecruitmentservice/WS_ApplicantManager.svc?wsdl", true);  // Production Webservice
			$error = $client->getError();
			if ($error) {
				echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
			}
			$client->soap_defencoding = 'UTF-8';
			/*$postParams = array(
			            "JobId" => $JobId,
						"staffName" => $staffName, 
						"staffId" => $staffId, 
						"initiatedById" => $initiatedById, 
						"currentGrade" => $currentGrade, 
						"currentSalary" => $currentSalary, 
						"companyName" => $companyName, 
						"department" => $department,  
						"location" => $location,
						"totalExperienceInYears" => $totalExperienceInYears,
						"cvFile" => $cvFile,
						"cvFileName" => htmlentities($cvFileName)
						);*/
						
			/*var response = client.ApplyForInternalJob("VC8250712123633", "Jojy", "C8240412000000", "C8240412000000", "12", "123123", "EDPD", "IT", "Karama", 2, new byte[0], "mycv");
they are passing values like this\ "staffId" => $staffId,
for there application
this is the insert query*/						
			$postParams = array(
			            "jobId" => $JobId,
						"staffName" => $staffName,
						"emailAddress" => $staffEmail,
						"mobileNumber" => $staffMobile,
						"staffId" => $staffId, 
						"initiatedById" => $initiatedById,
						"currentGrade" => $currentGrade, 
						"currentSalary" => $currentSalary, 
						"companyName" => $companyName, 
						"department" => $department,  
						"location" => $location,
						"nationality" => $nationality,
						"totalExperienceInYears" => $totalExperienceInYears,
						"cvFile" => base64_encode($cvFile),
						"cvFileName" => $_FILES[$fieldName]['name']
						);
			$result = $client->call("ApplyForInternalJob", $postParams);
			
/* echo '<p><b>Fault: ';
          print_r($result);
          echo '</b></p>';
          echo '<p><b>Request: <br>';
          echo htmlspecialchars($client->request, ENT_QUOTES) . '</b></p>';
          echo '<p><b>Response: <br>';
          echo htmlspecialchars($client->response, ENT_QUOTES) . '</b></p>';
          echo '<p><b>Debug: <br>';
          echo htmlspecialchars($client->debug_str, ENT_QUOTES) . '</b></p>';



if ($client->fault) {
    echo "<h2>Fault</h2><pre>";
    print_r($result);
    echo "</pre>";
}
else {
    $error = $client->getError();
    if ($error) {
        echo "<h2>Error</h2><pre>" . $error . "</pre>";
    }
    else {
        echo "<h2>Books</h2><pre>";
        print( addslashes($result) );
        echo "</pre>";
    }
}*/
			
/*print_r($result);			
$output = simplexml_load_string($result, NULL, LIBXML_NOCDATA);
print_r($output);*/	

//$output = simplexml_load_string($result['ApplyForInternalJobResult'], NULL, LIBXML_NOCDATA);
/*$output = simplexml_load_string($result['ApplyForInternalJobResult']);
print_r($output);
echo "--------------end-------------<br />";
			$limit = 5;	
			$i = 0;	
			foreach ($output->irecruitmentfunctionresponse as $item) {
				echo 'LOCATION'.((string)$item->responsestatus);
				echo 'CLOSEDATE'.((string)$item->responsemessage);
				$i++;
			}	
echo "--------------end-------------<br />";	
print_r($result);
echo "--------------end-------------<br />";*/

$output = '<?xml version="1.0" encoding="ISO-8859-1"?>'.$result['ApplyForInternalJobResult'];
//print_r($output);
//echo "--------------end-------------<br />";
$myresult = array();
$xmlparser = xml_parser_create();
$xmldata = $output;
xml_parse_into_struct($xmlparser,$xmldata,$values);
xml_parser_free($xmlparser);
//print_r($values);
foreach($values as $value){
	if($value['tag'] == 'RESPONSESTATUS' ){
		  $myresult['statsu'] = $value['value'];
		}
	if($value['tag'] == 'RESPONSEMESSAGE' ){
		  $myresult['msg'] = $value['value'];
		}	
	
}

//print_r($myresult);

if(isset($myresult['statsu']) && $myresult['statsu'] == 'True'){
  //$mainframe->redirect($linkSuccess, "Thank you for Applying for the Vacancy &quot;".$JobTitle."&quot;. To follow up on your application, please contact <a href='mailto:careers@emiratespost.ae'>careers@emiratespost.ae</a>.");
 
$where = ' WHERE 1 = 1  && id = "'.$user->id.'" ';
$database->SetQuery( "SELECT email FROM #__users".$where);
$memdtl = $database->loadResult();

$receiverEmail = $memdtl;

 
$html_format  = 'MIME-Version: 1.0' . "\r\n";
$html_format .= 'From: Careers@epg.gov.ae < Careers@epg.gov.ae>' . "\r\n"; //  FROM EMAIL ADDRESS - Moiz
$mailSubject = JText::_('EPOSTS_JOB_MSG011');                                                 
$receiverEmail = "epgintranet@epg.gov.ae"; // TO EMAIL ADDRESS - Moiz
                                                                                  
                   
$text = "Dear ".$user->name.",";
$text .= "<br><br><p>".JText::_('EPOSTS_JOB_MSG05')."".$JobTitle." ". JText::_('EPOSTS_JOB_MSG06')."  ".$JobId.".</p>";
$text .= "<br><p>".JText::_('EPOSTS_JOB_MSG07')."</p>";
$text .= "<br><p>".JText::_('EPOSTS_JOB_MSG08').".</p>";
$text .= "<br><br><p><b>Note:</b> ".JText::_('EPOSTS_JOB_MSG010')."</p>"; 
$html_format = true;
                                            //$success = JUtility::sendMail($user->email, $mailSubject, NULL, NULL, $text, $html_format, NULL, $receiverEmail);
$success = JUtility::sendMail($user->email, $user->name, $receiverEmail, $mailSubject, $text, $html_format, NULL, NULL, NULL, $user->email, $user->name);

/////////////////////
$mainframe->redirect($linkSuccess,"Thank you for applying for the  vacancy ".$JobTitle."");      
}else{
  $mainframe->redirect($link,"Fail to apply job. Please try again.");           
}


}

function mainlistjobsmessage($option,$view) {
	global $mainframe, $jlistConfig;
	
    $database = &JFactory::getDBO();
	$breadcrumbs =& $mainframe->getPathWay();
	$JobTitle = JRequest::getString('jobtitle');
	?>
	<div style="ine-height:30px; text-align:center;"><?php print JText::_('EPOSTS_JOB_MSG01'); ?> "<?php print $JobTitle; ?>". <?php print JText::_('EPOSTS_JOB_MSG02'); ?> <a href='mailto:Careers@epg.gov.aee'>Careers@epg.gov.ae </a> / 04-2303999 .</div>
	<?php
}

function xml2tree2($string)
{
  $parser = xml_parser_create();
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
  xml_parse_into_struct($parser, $string, $vals, $index);
  xml_parser_free($parser);
 
  $stack = array( array() );
  $stacktop = 0;
  $parent = array();
  $result_array = array();
  foreach($vals as $val)
  {
    $type = $val['type'];
    if($type=='open' || $type=='complete')
    {
      // open tag
      $stack[$stacktop++] = $tagi;
      $tagi = array('tag' => $val['tag']);

      if(isset($val['attributes']))  $tagi['attrs'] = $val['attributes'];     
      if(isset($val['value']))
        $tagi['values'][] = htmlentities($val['value']);
    }
    if($type=='complete' || $type=='close')
    {
      // finish tag
      $tags[] = $oldtagi = $tagi;
      $tagi = $stack[--$stacktop];
      $oldtag = $oldtagi['tag']; 
      unset($oldtagi['tag']);
      $tagi['children'][$oldtag][] = $oldtagi;
	  
	  //$result_array[];
      $parent = $tagi;
    }
    if($type=='cdata')
    {
      $tagi['values'][] = htmlentities($val['value']);
    }
  }
  return $parent['children'];
}

function mainShowJobs($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainShowJobs($option, $view);
}

function mainListJobs($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	jlist_HTML::mainListJobs($option, $view);
}

function mainShowTasks($option, $view){
	global $Itemid, $mainframe;
    $database = &JFactory::getDBO();
	$breadcrumbs =& $mainframe->getPathWay();
        $breadcrumbs->addItem(JText::_('Exchange Tasks'), JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowtasks'));
	jlist_HTML::mainShowTasks($option, $view);
}

///////////////////////////////////END Depevloment Phase VI ////////////////////////////
/*Start Like and Dislike Component*/
function IsLike(){
	
	try	{
			$likeview = JRequest::getCmd( 'like_view' );
			$action = JRequest::getCmd( 'do' );
			$status = JRequest::getCmd( 'status' );
			$is_like = $action == 1 ? 1 : 2;
			$Item_Id 	= $_POST['Item_Id']; 
			$User_ID 	= $_POST['User_ID']; 
			$database = &JFactory::getDBO();

			if($likeview=="mainshownews"){
				$table_like = "like_news"; 
			}else if($likeview=="mainshowannouncements"){
				$table_like = "like_announcement";
			}else if($likeview=="mainshowpolicies"){
				$table_like = "like_policies";
			}else if($likeview=="article"){
				$table_like = "like_pages";
			}

						
			if($status == "Ins"){
			$query = 	$database->SetQuery("INSERT INTO #__".$table_like . "  (Item_Id, Is_Like, User_Id) values (" .$Item_Id . "," 
			. $is_like . ",". $User_ID .") ");						
				
					if($database->query()){
						$success_msg = "Success";
						return $success_msg;
					}
					
			}else if($status == "Update") {
					$UserStateQuery  = "Update  #__".$table_like." SET Is_Like = ". $is_like . " Where Item_Id = ". $Item_Id ." AND 

User_ID =" .$User_ID;
							$database->setQuery($UserStateQuery);
								if($database->query()) {
									$success_msg = "Success";
									return $success_msg;
								}
			} else  {
				$UserStateQuery  = "Delete From #__".$table_like."  Where Item_Id = ". $Item_Id ." AND User_ID = " . $User_ID;
				$database->setQuery($UserStateQuery);
					if($database->query()) {
						$success_msg = "Success";
						return $success_msg;
					}
			}	
		}catch (RuntimeException $e){	
			$exception_msg = 	"Exception";
			return  $exception_msg;
		}
}

function LikeCir(){
	try	{
			$action = JRequest::getCmd( 'do' );
			$status = JRequest::getCmd( 'status' );
			$is_like = $action == 1 ? 1 : 2;
			$Item_Id 	= $_POST['Item_Id'];
			$User_ID 	= $_POST['User_ID']; 
		
			$database = &JFactory::getDBO();


			InsertSiteStat($Item_Id, "Circular");				
			
			if($status == "Ins"){
			
			$query = 	$database->SetQuery("INSERT INTO #__like_circulars  (Item_Id, Is_Like, User_Id) values (" .$Item_Id . "," 
			. $is_like . ",". $User_ID .") ");	
			
					if($database->query()){
						$success_msg = "Success";
						return $success_msg;
					}
					
			}else if($status == "Update") {
				
				$UserStateQuery  = "Update  #__like_circulars SET Is_Like =" .$is_like  ."Where Item_Id = ". $Item_Id ." AND User_ID =" .

$User_ID;
							$database->setQuery($UserStateQuery);
								if($database->query()) {
									$success_msg = "Success";
									return $success_msg;
								}
			} else  {
				$UserStateQuery  = "Delete From #__like_circulars  Where Item_Id = ". $Item_Id ." AND User_ID = " . $User_ID;
				$database->setQuery($UserStateQuery);
					if($database->query()) {
						$success_msg = "Success";
						return $success_msg;
					}
			}	
		}catch (RuntimeException $e){	
			$exception_msg = 	"Exception";
			return  $exception_msg;
		}
}

function LikeForms(){
	try	{
			$action = JRequest::getCmd( 'do' );
			$status = JRequest::getCmd( 'status' );
			$is_like = $action == 1 ? 1 : 2;
		
			$Item_Id 	= $_POST['Item_Id'];
			$User_ID 	= $_POST['User_ID']; 
		
			$database = &JFactory::getDBO();

			InsertSiteStat($Item_Id, "Form");				
			
			if($status == "Ins"){
			
			$query = 	$database->SetQuery("INSERT INTO #__like_forms  (Item_Id, Is_Like, User_Id) values (" .$Item_Id . "," 
			. $is_like . ",". $User_ID .") ");	
			
					if($database->query()){
						$success_msg = "Success";
						return $success_msg;
					}
					
			}else if($status == "Update") {
				
				$UserStateQuery  = "Update  #__like_forms SET Is_Like = ".$is_like." Where Item_Id = ". $Item_Id ." AND User_ID =" .$User_ID;
							$database->setQuery($UserStateQuery);
								if($database->query()) {
									$success_msg = "Success";
									return $success_msg;
								}
			} else  {
				$UserStateQuery  = "Delete From #__like_forms  Where Item_Id = ". $Item_Id ." AND User_ID = " . $User_ID;
				$database->setQuery($UserStateQuery);
					if($database->query()) {
						$success_msg = "Success";
						return $success_msg;
					}
			}	
		}catch (RuntimeException $e){	
			$exception_msg = 	"Exception";
			return  $exception_msg;
			//echo $e->getMessage();
		
}
}

function LikeImages(){
	try	{
			$action = JRequest::getCmd( 'do' );
			$status = JRequest::getCmd( 'status' );
			$is_like = $action == 1 ? 1 : 2;
		
			$Item_Id 	= $_POST['Item_Id'];
			//$Page_Title = $_POST['Page_Title']; 
			//$Url 		= $_POST['Page_Url']; 
			$Gallery_ID = $_POST['Gallery_ID']; 
			$User_ID 	= $_POST['User_ID']; 
		
			$database = &JFactory::getDBO();				
			
			if($status == "Ins"){
			
			$query = 	$database->SetQuery("INSERT INTO #__like_images  (Gallery_ID, Item_Id, Is_Like, User_Id) values (".$Gallery_ID."," .

$Item_Id . "," 
			. $is_like . ",". $User_ID .") ");	
			
					if($database->query()){
						$success_msg = "Success";
						return $success_msg;
					}
					
			}else if($status == "Update") {
				
				$UserStateQuery  = "Update  #__like_images SET Is_Like = ".$is_like." Where Item_Id = ". $Item_Id ." AND Gallery_ID = " .

$Gallery_ID . " AND User_ID =" .$User_ID;
							$database->setQuery($UserStateQuery);
								if($database->query()) {
									$success_msg = "Success";
									return $success_msg;
								}
			} else  {
				$UserStateQuery  = "Delete From #__like_images  Where Item_Id = ". $Item_Id ." AND Gallery_ID = ".$Gallery_ID." AND User_ID = 

" . $User_ID;
				$database->setQuery($UserStateQuery);
					if($database->query()) {
						$success_msg = "Success";
						return $success_msg;
					}
			}	
		}catch (RuntimeException $e){	
			$exception_msg = 	"Exception";
			return  $exception_msg;
			//echo $e->getMessage();
		
}
}

function LikeDoc(){
	try	{
			
			$action = JRequest::getCmd( 'do' );
			$status = JRequest::getCmd( 'status' );
			$is_like = $action == 1 ? 1 : 2;
			$Item_Id 	= $_POST['Item_Id'];
			$User_ID 	= $_POST['User_ID']; 
		
			$database = &JFactory::getDBO();

	
			InsertSiteStat($Item_Id, "Documents");				
			
			if($status == "Ins"){
			
			$query = 	$database->SetQuery("INSERT INTO #__like_documents  (Item_Id, Is_Like, User_Id) values (" .$Item_Id . "," 
			. $is_like . ",". $User_ID .") "); 	
			
					if($database->query()){
						$success_msg = "Success";
						return $success_msg;
					}
					
			}else if($status == "Update") {
				
		$UserStateQuery  = "Update  #__like_documents SET Is_Like =" .$is_like  ." Where Item_Id = ". $Item_Id ." AND User_ID =" .$User_ID; 
							$database->setQuery($UserStateQuery);
								if($database->query()) {
									$success_msg = "Success";
									return $success_msg;
								}
			} else  {
				$UserStateQuery  = "Delete From #__like_documents  Where Item_Id = ". $Item_Id ." AND User_ID = " . $User_ID;
				$database->setQuery($UserStateQuery);
					if($database->query()) {
						$success_msg = "Success";
						return $success_msg;
					}
			}	
		}catch (RuntimeException $e){	
			$exception_msg = 	"Exception";
			return  $exception_msg;
		}
}
/*End Like and Dislike Component*/	
function InsertSiteStat($item_id, $view){
             
			 try{
				
				$user = &JFactory::getUser();

				$database = &JFactory::getDBO();				
				$uri = JFactory::getURI();
		
			
			$query = "SELECT Id, Item_Id, Views_Count  FROM #__site_analytics where Item_Id = " . $item_id . " AND View_Name= '" .$view."' AND User_ID = ".$user->id;												
			
			$database->SetQuery( $query );
			$site_data = $database->loadObjectList();
			
			if($site_data[0]->Id == ""){
				$query = 	$database->SetQuery("INSERT INTO #__site_analytics  (Item_Id, View_Name, Url, User_Id, Views_Count) values (" . $item_id . ",'" . $view . "','". $uri->toString() ."',".$user->id.",1) ");
				/*$Views_Count = $site_data[0]->Views_Count;
				$Views_Count++;
$query = 	$database->SetQuery("UPDATE #__site_analytics SET Views_Count = ". $Views_Count ." Where Id = ".$site_data[0]->Id ." AND View_Name= '" .$view ."'");
			
			}else {*/
		
			}
			$database->query();	
				 
		 }catch(RuntimeException $e) {
			 return true;
		}
}

	
?>