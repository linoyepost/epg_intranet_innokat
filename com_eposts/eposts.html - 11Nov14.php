﻿<?php
/**
* @version 1.5
* @package JDownloads
* @copyright (C) 2009 Arno Betz - www.eposts.com
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* 
*
*/
defined( '_JEXEC' ) or die( 'Restricted access-html' ); 




function treeSelectList( &$src_list, $src_id, $tgt_list, $tag_name, $tag_attribs, $key, $text, $selected )
       {
   
           // establish the hierarchy of the menu
           $children = array();
           // first pass - collect children
           foreach ($src_list as $v ) {
               $pt = $v->parent;
               $list = @$children[$pt] ? $children[$pt] : array();
               array_push( $list, $v );
               $children[$pt] = $list;
           }
           // second pass - get an indent list of the items
           jimport( 'joomla.html.html.menu' );

           // JHTML::_('menu.treerecurse', $id, $indent, $list, $children, $maxlevel, $level, $type)

           $ilist = JHTML::_('menu.treerecurse', 0, '', array(), $children );
   
           // assemble menu items to the array
           $this_treename = '';
           foreach ($ilist as $item) {
               if ($this_treename) {
                   if ($item->id != $src_id && strpos( $item->treename, $this_treename ) === false) {
                       $tgt_list[] = JHTML::_('select.option', $item->id, $item->treename );
                   }
               } else {
                   if ($item->id != $src_id) {
                       $tgt_list[] = JHTML::_('select.option', $item->id, $item->treename );
                  } else {
                      $this_treename = "$item->treename/";
                  }
              }
          }
          // build the html select list 
           return jdgenericlist($tgt_list, $tag_name, $tag_attribs, $key, $text, $selected, true, false );  
      }


$jlistConfig = buildjlistConfig();

$document->addScript(JURI::base().'components/com_eposts/eposts.js');


$marked_files_id = array();
global $addScriptJWAjaxVote;
   
class jlist_HTML{
	
/////////////Development Phase V//////////////
	function iframeTarger($option, $user,$polanguage){
		global $jlistConfig, $jlistTemplates, $Itemid, $mainframe, $page_title, $cat_link_itemids;
		$user = &JFactory::getUser();
        $user_access = checkAccess_JD();
        $users_access =  (int)substr($user_access, 0, 1);        
		$database = &JFactory::getDBO();
        $app = JFactory::getApplication();
        $document = & JFactory::getDocument();
        $document->setTitle($page_title);
    $html_cat = '';
	$html_cat = makeHeader($html_cat, true, true, false, 0, false, false, false, false, false, $sum_pages, $limit, $total, $limitstart, $site_aktuell, $pageNav, '', '');
    echo $html_cat;
if(isset($user->username) && !empty($user->username)){	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <script>
        function postData() {
			//alert("chheena");
            document.forms["ctForm"].submit();
        }
    </script>
</head>
<body onload="postData();">
    <form id="ctForm"  action="http://intranet/creativethinking/home/login" method="post">
        <input type="hidden" name="Username" value="<?php echo $user->username;?>" />
        <input type="hidden" name="FirstName" value="" />
        <input type="hidden" name="FatherName" value="" />
        <input type="hidden" name="LastName" value="" />
        <input type="hidden" name="MobileNumber" value="" />
        <input type="hidden" name="OfficeNumber" value="" />
        <input type="hidden" name="Email" value="<?php echo $user->email;?>" />
        <input type="hidden" name="Job" value="" />
        <input type="hidden" name="JobTitle" value="" />
        <input type="hidden" name="Id" value="1" />
        <input type="hidden" name="FullName" value="<?php echo $user->name;?>" />
 <?php if($polanguage == "ar-aa"){$putLanguage = "ar";}else{$putLanguage = "en";}?>       
        <input type="hidden" name="Language" value="<?php echo $putLanguage;?>" />
    </form>
</body>
</html>
<?php
}
exit;
}	
	function showiframe($option, $user,$polanguage){
		global $jlistConfig, $jlistTemplates, $Itemid, $mainframe, $page_title, $cat_link_itemids;
		$user = &JFactory::getUser();
        $user_access = checkAccess_JD();
        $users_access =  (int)substr($user_access, 0, 1);        
		$database = &JFactory::getDBO();
        $app = JFactory::getApplication();
        $document = & JFactory::getDocument();
        $document->setTitle($page_title);
    $html_cat = '';
	$html_cat = makeHeader($html_cat, true, true, false, 0, false, false, false, false, false, $sum_pages, $limit, $total, $limitstart, $site_aktuell, $pageNav, '', '');
    echo $html_cat;
if(isset($user->username) && !empty($user->username)){	
?>
  <?php if($polanguage == "ar-aa"){$putLanguage = "ar";}else{$putLanguage = "en";}?>
 <!-- <iframe width="100%" frameBorder="0" style="margin-top:-15px; border:none; overflow:hidden;" scrolling="no" height="2700px" src="index.php?option=com_eposts&view=iframeTarger&lang=<?php echo $putLanguage;?>"></iframe>
-->
<iframe width="100%" frameBorder="0" style="margin-top:-15px; border:none;" height="600px" src="index.php?option=com_eposts&view=iframeTarger&lang=<?php echo $putLanguage;?>"></iframe>
<?php	
}else{
               $html_off = '<br /><br />Access denied.<br /><br />';
                echo $html_off;
	}
}	

/* ###########################################################
/  Nur Kategorien-Übersicht anzeigen
############################################################## */
	function showCats($option, $cats, $total, $sum_pages, $limit, $limitstart, $site_aktuell, $sub_cats, $sub_files, $sum_all_cats, $no_cats, $pageNav){
		global $jlistConfig, $jlistTemplates, $Itemid, $mainframe, $page_title, $cat_link_itemids;
		$user = &JFactory::getUser();
        $user_access = checkAccess_JD();
        $users_access =  (int)substr($user_access, 0, 1);        
		$database = &JFactory::getDBO();
        $app = JFactory::getApplication();
        $document = & JFactory::getDocument();
        $document->setTitle($page_title);
	     
    $html_cat = '';
	$html_cat = makeHeader($html_cat, true, true, false, 0, false, false, false, false, false, $sum_pages, $limit, $total, $limitstart, $site_aktuell, $pageNav, '', '');
    echo $html_cat;
	$html_cat = '';
    $metakey = '';		
	
	if(!empty($cats)){
		for ($i=0; $i < $limit; $i++) { 	
           if ($cats[$i]->cat_title){
               // get access control
               $access = array();
               $access[0] = (int)substr($cats[$i]->cat_access, 0, 1);
               $access[1] = (int)substr($cats[$i]->cat_access, 1, 1);
               // only view when user has corect access level
        
               if ($users_access >= $access[0] || $cats[$i]->cat_access == '99') {
                   $database->setQuery("SELECT COUNT(*) FROM #__eposts_files WHERE cat_id = '{$cats[$i]->cat_id}' AND published = 1");
                   $anzahl_files = $database->loadResult();

                   //display cat info
                   if (!$no_cats){
                       // exists a single category menu link for it? 
                       if ($cat_link_itemids){  
                           $cat_itemid = '';
                           for ($i2=0; $i2 < count($cat_link_itemids); $i2++) {
                               if ($cat_link_itemids[$i2][catid] == $cats[$i]->cat_id){
                                   $cat_itemid = $cat_link_itemids[$i2][id];
                               }     
                           }
                       }    
                       if (!$cat_itemid){
                           // use global itemid when no single link exists
                           $cat_itemid = $Itemid;
                       }    
                       $catlink = "<a href='".JRoute::_("index.php?option=com_eposts&Itemid=".$cat_itemid."&view=viewcategory&catid=".$cats[$i]->cat_id)."'>";
                   } else {
                       $catlink = ''; 
                   }    
                   
                   // symbol anzeigen - auch als url
                   if ($cats[$i]->cat_pic != '' ) {
                       $size = $jlistConfig['cat.pic.size'];
                       $catpic = $catlink.'<img src="'.JURI::base().'images/eposts/catimages/'.$cats[$i]->cat_pic.'" align="top" width="'.$size.'" height="'.$size.'" border="0" alt="" /> </a>';
                   } else {
                       $catpic = '';
                   }                   
                   $html_cat .= str_replace('{cat_title}', $catlink.$cats[$i]->cat_title.'</a>', $jlistTemplates[1][0]->template_text);
			       $html_cat = str_replace('{cat_description}', $cats[$i]->cat_description, $html_cat);
			       $html_cat = str_replace('{cat_pic}', $catpic, $html_cat);
                   if ($sub_cats[$i] == 0){
                       $html_cat = str_replace('{sum_subcats}','', $html_cat);
                   } else {
                       $html_cat = str_replace('{sum_subcats}', JText::_('COM_EPOSTS_FRONTEND_COUNT_SUBCATS').' '.$sub_cats[$i], $html_cat);
                   }
                   if ($no_cats){
                       $html_cat = str_replace('{sum_files_cat}', '', $html_cat);
                   } else {
                       $html_cat = str_replace('{sum_files_cat}', JText::_('COM_EPOSTS_FRONTEND_COUNT_FILES').' '.$sub_files[$i], $html_cat); 
                   }    
			       $html_cat = str_replace('{files}', "", $html_cat);
    	           $html_cat = str_replace('{checkbox_top}', "", $html_cat);
			       $html_cat = str_replace('{form_button}', "", $html_cat);
                   $html_cat = str_replace('{form_hidden}', "", $html_cat);
                   $html_cat = str_replace('{cat_info_end}', "", $html_cat);
                   $html_cat = str_replace('{cat_info_begin}', "", $html_cat);
                   // google adsense
                   if ($jlistConfig['google.adsense.active']){
                      $html_cat = str_replace('{google_adsense}', stripslashes($jlistConfig['google.adsense.code']), $html_cat);
                   } else {
                      $html_cat = str_replace('{google_adsense}', '', $html_cat);
                   } 
                   
                   // cat title row info only view in one category
                   // remove all title html tags in output
                   if ($pos_end = strpos($html_cat, '{cat_title_end}')){
                       $pos_beg = strpos($html_cat, '{cat_title_begin}');
                       $html_cat = substr_replace($html_cat, '', $pos_beg, ($pos_end - $pos_beg)+ 15);
                   }
                   // add metakey infos
                   if ($cats[$i]->metakey){
                        $metakey = $metakey.' '.$cats[$i]->metakey; 
                   }
               } // access control
           }
        }
        $jmeta = $document->getMetaData( 'keywords' );
        if (!$metakey){
            $document->setMetaData( 'keywords' , $jmeta);
        } else {
            $document->setMetaData( 'keywords' , strip_tags($metakey));
        }    
        
    	$footer = makeFooter(false, true, false, $sum_pages, $limit, $limitstart, $site_aktuell, $pageNav, false, false);  
        $html_cat .= $footer;

        if ( !$jlistConfig['offline'] ) {
            echo $html_cat;
        } else {
            if ($aid == 3) {
                echo JText::_('COM_EPOSTS_BACKEND_OFFLINE_ADMIN_MESSAGE_TEXT');
                echo $html_cat;
            } else {
                $html_off = '<br /><br />'.stripslashes($jlistConfig['offline.text']).'<br /><br />';
                $html_off .= $footer;
                echo $html_off;
            }
        }
    }  
}

/* ###########################################################
/  Nur Kategorien-Übersicht anzeigen
############################################################## */
    function showCatswithColumns($option, $cats, $total, $sum_pages, $limit, $limitstart, $site_aktuell, $sub_cats, $sub_files, $sum_all_cats, $columns, $no_cats, $pageNav){
        global $jlistConfig, $jlistTemplates, $Itemid, $mainframe, $page_title, $cat_link_itemids, $root_itemid;
        $user = &JFactory::getUser();
        $user_access = checkAccess_JD();
        $users_access =  (int)substr($user_access, 0, 1);
        $database = &JFactory::getDBO();
        $app = JFactory::getApplication();
        $document=& JFactory::getDocument();
        $document->setTitle($page_title);
         
    $html_cat = makeHeader($html_cat, true, true, false, 0, false, false, false, false, false, $sum_pages, $limit, $total, $limitstart, $site_aktuell, $pageNav, '', '');
    echo $html_cat;
    $html_cat = '';
    $metakey = '';        
    $amount = 0;
    $rows = (count($cats) / $columns);
     
    if (count($cats) < $limit){
        $amount = count($cats);
    } else {
        $amount = $limit;
    }    
    if(!empty($cats)){
        for ($i=0; $i < $amount; $i++) {
        $a = 0;     
            for ($a=0; $a < $columns; $a++){
            if ($cats[$i]->cat_title){
               // get access control
               $access = array();
               $access[0] = (int)substr($cats[$i]->cat_access, 0, 1);
               $access[1] = (int)substr($cats[$i]->cat_access, 1, 1);
               // only view when user has corect access level
               if ($users_access >= $access[0] || $cats[$i]->cat_access == '99') {
                   $database->setQuery("SELECT COUNT(*) FROM #__eposts_files WHERE cat_id = '{$cats[$i]->cat_id}' AND published = 1");
                   $anzahl_files = $database->loadResult();

                   //display cat info
                   if (!$no_cats){
                       // exists a single category menu link for it? 
                       if ($cat_link_itemids){  
                           $cat_itemid = '';
                           for ($i2=0; $i2 < count($cat_link_itemids); $i2++) {
                               if ($cat_link_itemids[$i2][catid] == $cats[$i]->cat_id){
                                   $cat_itemid = $cat_link_itemids[$i2][id];
                               }     
                           }
                       }    
                       if (!$cat_itemid){
                           // use global itemid when no single link exists
                           $cat_itemid = $root_itemid;
                       } 
                       $catlink = "<a href='".JRoute::_("index.php?option=com_eposts&Itemid=".$cat_itemid."&view=viewcategory&catid=".$cats[$i]->cat_id)."'>";
                   } else {
                        $catlink = $cats[$i]->cat_title;
                   } 

                   // symbol anzeigen - auch als url
                   if ($cats[$i]->cat_pic != '' ) {
                       $size = $jlistConfig['cat.pic.size'];
                       $catpic = $catlink.'<img src="'.JURI::base().'images/eposts/catimages/'.$cats[$i]->cat_pic.'" align="top" width="'.$size.'" height="'.$size.'" border="0" alt="" /> </a>';
                   } else {
                       $catpic = '';
                   }                   
                   $x = $a+1;
                   $x = (string)$x;
                   if ($i < $amount){
                        if ($a == 0){
                            if ($no_cats){
                                $html_cat .= str_replace("{cat_title$x}", $catlink, $jlistTemplates[1][0]->template_text);
                            } else {    
                                $html_cat .= str_replace("{cat_title$x}", $catlink.$cats[$i]->cat_title.'</a>', $jlistTemplates[1][0]->template_text);
                            }
                        } else {
                            $html_cat = str_replace("{cat_title$x}", $catlink.$cats[$i]->cat_title.'</a>', $html_cat);
                        }
                        $html_cat = str_replace("{cat_pic$x}", $catpic, $html_cat);
                        $html_cat = str_replace("{cat_description$x}", $cats[$i]->cat_description, $html_cat);
                        if ($sub_cats[$i] == 0){
                             if ($no_cats){
                                 $html_cat = str_replace("{sum_subcats$x}", '', $html_cat); 
                            } else {
                                $html_cat = str_replace("{sum_subcats$x}", JText::_('COM_EPOSTS_FRONTEND_COUNT_SUBCATS').' 0', $html_cat); 
                            }    
                        } else {
                            $html_cat = str_replace("{sum_subcats$x}", JText::_('COM_EPOSTS_FRONTEND_COUNT_SUBCATS').' '.$sub_cats[$i], $html_cat);
                        }
                        if ($no_cats){
                            $html_cat = str_replace("{sum_files_cat$x}", '', $html_cat);
                        } else {    
                            $html_cat = str_replace("{sum_files_cat$x}", JText::_('COM_EPOSTS_FRONTEND_COUNT_FILES').' '.$sub_files[$i], $html_cat);
                        }    
                   } else {
                        $html_cat = str_replace("{cat_title$x}", '', $html_cat);
                        $html_cat = str_replace("{cat_pic$x}", '', $html_cat);
                        $html_cat = str_replace("{cat_description$x}", '', $html_cat);
                   }         
                   $html_cat = str_replace('{cat_description}', '', $html_cat);
                   $html_cat = str_replace('{files}', "", $html_cat);
                   $html_cat = str_replace('{checkbox_top}', "", $html_cat);
                   $html_cat = str_replace('{form_button}', "", $html_cat);
                   $html_cat = str_replace('{form_hidden}', "", $html_cat);
                   $html_cat = str_replace('{cat_info_end}', "", $html_cat);
                   $html_cat = str_replace('{cat_info_begin}', "", $html_cat);
                   // google adsense
                   if ($jlistConfig['google.adsense.active']){
                      $html_cat = str_replace('{google_adsense}', stripslashes($jlistConfig['google.adsense.code']), $html_cat);
                   } else {
                      $html_cat = str_replace('{google_adsense}', '', $html_cat);
                   }                    
                   // cat title row info only view in one category
                   // remove all title html tags in output
                   if ($pos_end = strpos($html_cat, '{cat_title_end}')){
                       $pos_beg = strpos($html_cat, '{cat_title_begin}');
                       $html_cat = substr_replace($html_cat, '', $pos_beg, ($pos_end - $pos_beg)+ 15);
                   }
                   // add metakey infos
                   if ($cats[$i]->metakey){
                        $metakey = $metakey.' '.$cats[$i]->metakey; 
                   }
               } // access control
            }
            if (($a+1) < $columns){
                $i++;
            }    
          } 
        }
        
        for ($b=1; $b < ($columns+1); $b++){
            $x = (string)$b;
            $html_cat = str_replace("{cat_title$x}", '', $html_cat);
            $html_cat = str_replace("{cat_pic$x}", '', $html_cat);
            $html_cat = str_replace("{sum_files_cat$x}", '', $html_cat); 
            $html_cat = str_replace("{sum_subcats$x}", '', $html_cat);
            $html_cat = str_replace("{cat_description$x}", '', $html_cat); 
        }
        $jmeta = $document->getMetaData( 'keywords' ); 
        if (!$metakey){
            $document->setMetaData( 'keywords' , $jmeta);
        } else {
            $document->setMetaData( 'keywords' , strip_tags($metakey));
        }            
        
        $footer = makeFooter(false, true, false, $sum_pages, $limit, $limitstart, $site_aktuell, $pageNav, false, false);  
        $html_cat .= $footer;

        if ( !$jlistConfig['offline'] ) {
            echo $html_cat;
        } else {
            if ($aid == 3) {
                echo JText::_('COM_EPOSTS_BACKEND_OFFLINE_ADMIN_MESSAGE_TEXT');
                echo $html_cat;
            } else {
                $html_off = '<br /><br />'.stripslashes($jlistConfig['offline.text']).'<br /><br />';
                $html_off .= $footer;
                echo $html_off;
            }
        }
    }
}

/* ###########################################################
/  Einzelne Kategorie mit Liste der download files anzeigen
############################################################## */
	function showOneCategory($option, $cat, $subcats, $files, $catid, $total, $sum_pages, $limit, $limitstart, $sum_subcats, $sum_subfiles, $site_aktuell, $access, $columns, $pageNav, $order, $dir){
		global $jlistConfig, $jlistTemplates, $Itemid, $mainframe, $mosConfig_MetaKeys, $page_title, $cat_link_itemids, $root_itemid;

		$user = &JFactory::getUser();
        $user_access = checkAccess_JD();
        $users_access =  (int)substr($user_access, 0, 1);
		
        $database = &JFactory::getDBO();
        $app = &JFactory::getApplication();
        $document=& JFactory::getDocument();
        $document->setTitle($page_title.' - '.$cat[0]->cat_title);
        
    $jmeta = $document->getMetaData( 'keywords' ); 
    $jmetadesc = $document->getMetaData( 'description' ); 
    if (!$cat[0]->metakey){
        $document->setMetaData( 'keywords' , $jmeta);
    } else {
        $document->setMetaData( 'keywords' , strip_tags($cat[0]->metakey));
    }    
    if (!$cat[0]->metadesc){
        $document->setMetaData( 'description' , $jmetadesc);
    } else {
        $document->setMetaData( 'description' , strip_tags($cat[0]->metadesc));
    }    
      
    $sum_subs = count($subcats);
    $html_cat = makeHeader($html_cat, true, false, true, $sum_subs, false, false, false, false, false, $sum_pages, $limit, $total, $limitstart, $site_aktuell, $pageNav, $order, $dir);
    echo $html_cat; 
 
    // get rights to download from category
    $cat_access = (int)substr($cat[0]->cat_access, 1, 1);
 
    // url manipulation?
    if ($site_aktuell > 1) $subcats = '';
        // get pic
        if ($cat[0]->cat_pic != '' ) {
            $size = $jlistConfig['cat.pic.size'];
            $catpic = '<img src="'.JURI::base().'images/eposts/catimages/'.$cat[0]->cat_pic.'" align="top" width="'.$size.'" height="'.$size.'" border="0" alt="" /> ';
        } else {
            $catpic = '';
        }
  		//display cat info
		// make sure that this option only works with 1 column layouts
        if ($jlistConfig['view.category.info'] && $columns < 2){
            $viewcatinfo = true;
        } else {
            $viewcatinfo = false;
        }        
        if ($viewcatinfo) {
            $html_cat = str_replace('{cat_title}', $cat[0]->cat_title, $jlistTemplates[1][0]->template_text);
		    // support for content plugins
            if ($jlistConfig['activate.general.plugin.support'] && $jlistConfig['use.general.plugin.support.only.for.descriptions']) {  
               $cat[0]->cat_description = JHTML::_('content.prepare', $cat[0]->cat_description);
              /* $myDesc = new stdClass;
               $myDesc->text = $cat[0]->cat_description;
               JPluginHelper::importPlugin('content');
               $dispatcher = JDispatcher::getInstance();
               $results = $dispatcher->trigger(
                     'onContentPrepare', array ('text', &$myDesc, &$params, 0)
               );
               $cat[0]->cat_description = $results[1]; */
            }
            $html_cat = str_replace('{cat_description}', $cat[0]->cat_description, $html_cat);
		    $html_cat = str_replace('{cat_pic}', $catpic, $html_cat);
            $html_cat = str_replace('{sum_subcats}', '', $html_cat);
            $html_cat = str_replace('{sum_files_cat}', JText::_('COM_EPOSTS_FRONTEND_COUNT_FILES').' '.$total, $html_cat);
            $html_cat = str_replace('{cat_info_begin}', '', $html_cat); 
            $html_cat = str_replace('{cat_info_end}', '', $html_cat);
            // remove all title html tags in top cat output
            if ($pos_end = strpos($html_cat, '{cat_title_end}')){
                $pos_beg = strpos($html_cat, '{cat_title_begin}');
                $html_cat = substr_replace($html_cat, '', $pos_beg, ($pos_end - $pos_beg) + 15);
            }
        } else {
            if ($columns > 1 && strpos($jlistTemplates[1][0]->template_text, '{cat_title1}')){ 
                $html_cat = str_replace('{cat_title1}', '', $jlistTemplates[1][0]->template_text);
                for ($b=1; $b < 10; $b++){
                    $x = (string)$b;
                    $html_cat = str_replace("{cat_title$x}", '', $html_cat);
                    $html_cat = str_replace("{cat_pic$x}", '', $html_cat);
                    $html_cat = str_replace("{sum_files_cat$x}", '', $html_cat); 
                    $html_cat = str_replace("{sum_subcats$x}", '', $html_cat);
                    $html_cat = str_replace("{cat_description$x}", '', $html_cat);  
                } 
            } else {
                $html_cat = str_replace('{cat_title}', '', $jlistTemplates[1][0]->template_text);
                
            }    
            // remove all title html tags in top cat output
            if ($pos_end = strpos($html_cat, '{cat_title_end}')){
                $pos_beg = strpos($html_cat, '{cat_title_begin}');
                $html_cat = substr_replace($html_cat, '', $pos_beg, ($pos_end - $pos_beg) + 15);
            } 
            // remove all html tags in top cat output
            if ($pos_end = strpos($html_cat, '{cat_info_end}')){
                $pos_beg = strpos($html_cat, '{cat_info_begin}');
                $html_cat = substr_replace($html_cat, '', $pos_beg, ($pos_end - $pos_beg) + 14);
            } else {
                $html_cat = str_replace('{cat_description}', '', $html_cat);
                $html_cat = str_replace('{cat_pic}', '', $html_cat);
                $html_cat = str_replace('{sum_subcats}', '', $html_cat);
                $html_cat = str_replace('{sum_files_cat}', '', $html_cat);
            }
        }
        // google adsense
        if ($jlistConfig['google.adsense.active']){
            $html_cat = str_replace('{google_adsense}', stripslashes($jlistConfig['google.adsense.code']), $html_cat);
        } else {
            $html_cat = str_replace('{google_adsense}', '', $html_cat);
        }         
  		$html_files = '';
        $i = 0;
        $formid = $cat[0]->cat_id;
                
        // subcats anzeigen
        if(!empty($subcats)){
            $html_cat = str_replace('{files}', "", $html_cat);
            $html_cat = str_replace('{checkbox_top}', "", $html_cat);
            $html_cat = str_replace('{form_hidden}', "", $html_cat);
            $html_cat = str_replace('{form_button}', "", $html_cat);
            
            for ($i=0; $i < count($subcats); $i++){        
                // exists a single category menu link for it? 
                if ($cat_link_itemids){  
                    $cat_itemid = '';
                    for ($i2=0; $i2 < count($cat_link_itemids); $i2++) {
                         if ($cat_link_itemids[$i2][catid] == $subcats[$i]->cat_id){
                             $cat_itemid = $cat_link_itemids[$i2][id];
                         }     
                    }
                }    
                if (!$cat_itemid){
                    // use global itemid when no single link exists
                    $cat_itemid = $root_itemid;
                } 
                //display cat info
                 $catlink = "<a href='".JRoute::_("index.php?option=com_eposts&Itemid=".$cat_itemid."&view=viewcategory&catid=".$subcats[$i]->cat_id)."'>";
                 // Symbol anzeigen - auch als url
                 if ($subcats[$i]->cat_pic != '' ) {
                     $size = $jlistConfig['cat.pic.size'];
                     $catpic = $catlink.'<img src="'.JURI::base().'images/eposts/catimages/'.$subcats[$i]->cat_pic.'" align="top" width="'.$size.'" height="'.$size.'" border="0" alt="" /> </a>';
                 } else {
                     $catpic = '';
                 }                         

                 // more as one column   ********************************************************
                 if ($columns > 1 && strpos($jlistTemplates[1][0]->template_text, '{cat_title1}')){
                    $a = 0;     
                    for ($a=0; $a < $columns; $a++){

                        // exists a single category menu link for it? 
                        if ($cat_link_itemids){  
                            $cat_itemid = '';
                            for ($i2=0; $i2 < count($cat_link_itemids); $i2++) {
                                if ($cat_link_itemids[$i2][catid] == $subcats[$i]->cat_id){
                                    $cat_itemid = $cat_link_itemids[$i2][id];
                                }        
                            }
                        }    
                        if (!$cat_itemid){
                            // use global itemid when no single link exists
                            $cat_itemid = $root_itemid;
                        } 
                        //display cat info
                        $catlink = "<a href='".JRoute::_("index.php?option=com_eposts&Itemid=".$cat_itemid."&view=viewcategory&catid=".$subcats[$i]->cat_id)."'>";
                        // Symbol anzeigen - auch als url
                        if ($subcats[$i]->cat_pic != '' ) {
                            $size = $jlistConfig['cat.pic.size'];
                            $catpic = $catlink.'<img src="'.JURI::base().'images/eposts/catimages/'.$subcats[$i]->cat_pic.'" align="top" width="'.$size.'" height="'.$size.'" border="0" alt="" /> </a>';
                        } else {
                            $catpic = '';
                        }                     
                    
                       
                         $x = $a+1;
                         $x = (string)$x;
                         if ($i < count($subcats)){
                            if ($a == 0){
                                $html_cat .= str_replace("{cat_title$x}", $catlink.$subcats[$i]->cat_title.'</a>', $jlistTemplates[1][0]->template_text);
                            } else {
                                $html_cat = str_replace("{cat_title$x}", $catlink.$subcats[$i]->cat_title.'</a>', $html_cat);
                            } 
                            $html_cat = str_replace("{cat_pic$x}", $catpic, $html_cat);
                            $html_cat = str_replace("{cat_description$x}", $subcats[$i]->cat_description, $html_cat);
                            if ($sum_subcats[$i] == 0){
                                $html_cat = str_replace("{sum_subcats$x}", JText::_('COM_EPOSTS_FRONTEND_COUNT_SUBCATS').' 0', $html_cat); 
                            } else {
                                $html_cat = str_replace("{sum_subcats$x}", JText::_('COM_EPOSTS_FRONTEND_COUNT_SUBCATS').' '.$sum_subcats[$i], $html_cat);
                            }
                            $html_cat = str_replace("{sum_files_cat$x}", JText::_('COM_EPOSTS_FRONTEND_COUNT_FILES').' '.$sum_subfiles[$i], $html_cat);
                         } else {
                            $html_cat = str_replace("{cat_title$x}", '', $html_cat);
                            $html_cat = str_replace("{cat_pic$x}", '', $html_cat);
                            $html_cat = str_replace("{cat_description$x}", '', $html_cat);
                         }
                         if (($a+1) < $columns){
                            $i++;
                            $catlink = "<a href='".JRoute::_("index.php?option=com_eposts&Itemid=".$cat_itemid."&view=viewcategory&catid=".$subcats[$i]->cat_id)."'>";
                            // Symbol anzeigen - auch als url
                            if ($subcats[$i]->cat_pic != '' ) {
                                $size = $jlistConfig['cat.pic.size'];
                                $catpic = $catlink.'<img src="'.JURI::base().'images/eposts/catimages/'.$subcats[$i]->cat_pic.'" align="top" width="'.$size.'" height="'.$size.'" border="0" alt="" /> </a>';
                            } else {
                                $catpic = '';
                            }
                         }  
                    }
                    for ($b=1; $b < 10; $b++){
                        $x = (string)$b;
                        $html_cat = str_replace("{cat_title$x}", '', $html_cat);
                        $html_cat = str_replace("{cat_pic$x}", '', $html_cat);
                        $html_cat = str_replace("{sum_files_cat$x}", '', $html_cat); 
                        $html_cat = str_replace("{sum_subcats$x}", '', $html_cat); 
                    }
                 } else {
                    $html_cat .= str_replace('{cat_title}', $catlink.$subcats[$i]->cat_title.'</a>', $jlistTemplates[1][0]->template_text);
                    if ($sum_subcats[$i] == 0){
                        $html_cat = str_replace('{sum_subcats}','', $html_cat);
                     } else {
                        $html_cat = str_replace('{sum_subcats}', JText::_('COM_EPOSTS_FRONTEND_COUNT_SUBCATS').' '.$sum_subcats[$i], $html_cat);
                     }
                     $html_cat = str_replace('{sum_files_cat}', JText::_('COM_EPOSTS_FRONTEND_COUNT_FILES').' '.$sum_subfiles[$i], $html_cat);
                 }
                   
                    $html_cat = str_replace('{cat_description}', $subcats[$i]->cat_description, $html_cat);
                    $html_cat = str_replace('{cat_pic}', $catpic, $html_cat);
                    $html_cat = str_replace('{cat_info_begin}', '', $html_cat); 
                    $html_cat = str_replace('{cat_info_end}', '', $html_cat);
                    if ($i > 0){
                         // remove all title html tags in top cat output
                         if ($pos_end = strpos($html_cat, '{cat_title_end}')){
                            $pos_beg = strpos($html_cat, '{cat_title_begin}');
                            $html_cat = substr_replace($html_cat, '', $pos_beg, ($pos_end - $pos_beg) + 15);
                         } 
                     } else {
                         $html_cat = str_replace('{subcats_title_text}', JText::_('COM_EPOSTS_FE_FILELIST_TITLE_OVER_SUBCATS_LIST'), $html_cat);             
                         $html_cat = str_replace('{cat_title_begin}', '', $html_cat); 
                         $html_cat = str_replace('{cat_title_end}', '', $html_cat);
                     }    
                     // mehrfache file liste anzeige verhindern
                     if ($i < (count($subcats) -1)) {
                        $html_cat = str_replace('{files}', "", $html_cat);
                        $html_cat = str_replace('{checkbox_top}', "", $html_cat);
                        $html_cat = str_replace('{form_hidden}', "", $html_cat);
                        $html_cat = str_replace('{form_button}', "", $html_cat);
                     }
           }
            // google adsense
            if ($jlistConfig['google.adsense.active']){
                $html_cat = str_replace('{google_adsense}', stripslashes($jlistConfig['google.adsense.code']), $html_cat);
            } else {
                $html_cat = str_replace('{google_adsense}', '', $html_cat);
            }
        }                        
        // support for content plugins 
        if ($jlistConfig['activate.general.plugin.support'] && !$jlistConfig['use.general.plugin.support.only.for.descriptions']) {
            $html_cat = JHTML::_('content.prepare', $html_cat);
        }
        
        // build info pics
        $pic_date    = '';
        $pic_license = '';
        $pic_author  = '';
        $pic_website = '';
        $pic_system = '';
        $pic_language  = '';
        $pic_download = '';
        $pic_price = '';
        $pic_size = '';
        // anzeigen wenn im Layout aktiviert (0 = aktiv !!)
        if ($jlistTemplates[2][0]->symbol_off == 0 ) {
            $msize =  $jlistConfig['info.icons.size'];
            $pic_date = '<img src="'.JURI::base().'images/eposts/miniimages/date.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_DATE').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_DATE').'" />&nbsp;';
            $pic_license = '<img src="'.JURI::base().'images/eposts/miniimages/license.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_LICENCE').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_LICENCE').'" />&nbsp;';
            $pic_author = '<img src="'.JURI::base().'images/eposts/miniimages/contact.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_AUTHOR').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_AUTHOR').'" />&nbsp;';
            $pic_website = '<img src="'.JURI::base().'images/eposts/miniimages/weblink.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_WEBSITE').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_WEBSITE').'" />&nbsp;';
            $pic_system = '<img src="'.JURI::base().'images/eposts/miniimages/system.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_SYSTEM').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_SYSTEM').'" />&nbsp;';
            $pic_language = '<img src="'.JURI::base().'images/eposts/miniimages/language.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_LANGUAGE').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_LANGUAGE').'" />&nbsp;';
            $pic_downloads = '<img src="'.JURI::base().'images/eposts/miniimages/download.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_DOWNLOAD').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_DOWNLOAD_HITS').'" />&nbsp;';
            //$pic_download = '<img src="'.JURI::base().'images/eposts/miniimages/download.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_DOWNLOAD').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_DOWNLOAD').'" />&nbsp;';
            $pic_price = '<img src="'.JURI::base().'images/eposts/miniimages/currency.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_PRICE').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_PRICE').'" />&nbsp;';
            $pic_size = '<img src="'.JURI::base().'images/eposts/miniimages/stuff.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_FILESIZE').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_FILESIZE').'" />&nbsp;';
        }
        // a little pic for extern links
        $extern_url_pic = '<img src="'.JURI::base().'components/com_eposts/assets/images/link_extern.gif" alt="" />';

        // files der cat anzeigen
        for ($i=0; $i<count($files); $i++) {
            $value = $files[$i]->file_id;

            // nur checkbox wenn kein externer link
            if (!$files[$i]->extern_file){
                $checkbox_list = '<input type="checkbox" id="cb'.$i.'" name="cb_arr[]" value="'.$value.'" onclick="istChecked(this.checked,'.$formid.');"/>';
            } else {
                $userinfo = JText::_('COM_EPOSTS_FRONTEND_EXTERN_FILE_USER_INFO');
                $checkbox_list = JHTML::_('tooltip', $userinfo);
            }    
        	$html_file = str_replace('{file_id}',$files[$i]->file_id, $jlistTemplates[2][0]->template_text);
            // files title row info only view when it is the first file
            if ($i > 0){
                // remove all html tags in top cat output
                if ($pos_end = strpos($html_file, '{files_title_end}')){
                    $pos_beg = strpos($html_file, '{files_title_begin}');
                    $html_file = substr_replace($html_file, '', $pos_beg, ($pos_end - $pos_beg) + 17);
                }
            } else {
                $html_file = str_replace('{files_title_text}', JText::_('COM_EPOSTS_FE_FILELIST_TITLE_OVER_FILES_LIST'), $html_file);
                $html_file = str_replace('{files_title_end}', '', $html_file);
                $html_file = str_replace('{files_title_begin}', '', $html_file);
            } 
     
             // create file titles
             $html_file = buildFieldTitles($html_file, $files[$i]);
            
             $html_file = str_replace('{file_name}', $files[$i]->url_download, $html_file);
             
             // google adsense
             if ($jlistConfig['google.adsense.active']){
                 $html_file = str_replace('{google_adsense}', stripslashes($jlistConfig['google.adsense.code']), $html_file);
             } else {
                 $html_file = str_replace('{google_adsense}', '', $html_file);
             } 
             // report download link
             if ($jlistConfig['use.report.download.link']){
                $report_link = '<a href="'.JRoute::_("index.php?option=com_eposts&Itemid=".$root_itemid."&view=report&cid=".$files[$i]->file_id).'">'.JText::_('COM_EPOSTS_CONFIG_REPORT_FILE_LINK_TEXT').'</a>';
                if ($jlistConfig['report.link.only.regged'] && !$user->guest || !$jlistConfig['report.link.only.regged']) {
                   $html_file = str_replace('{report_link}', $report_link, $html_file);
                } else {
                   $html_file = str_replace('{report_link}', '', $html_file);
                }   
             } else {
                $html_file = str_replace('{report_link}', '', $html_file);
             }
            
             // view sum comments 
             if ($jlistConfig['view.sum.jcomments'] && $jlistConfig['jcomments.active']){
                 $database->setQuery('SELECT COUNT(*) from #__jcomments WHERE object_department = \'com_eposts\' AND object_id = '.$files[$i]->file_id);
                 $sum_comments = $database->loadResult();
                 if ($sum_comments >= 0){
                     $comments = sprintf(JText::_('COM_EPOSTS_FRONTEND_JCOMMENTS_VIEW_SUM_TEXT'), $sum_comments); 
                     $html_file = str_replace('{sum_jcomments}', $comments, $html_file);
                 } else {
                    $html_file = str_replace('{sum_jcomments}', '', $html_file);
                 }
             } else {   
                 $html_file = str_replace('{sum_jcomments}', '', $html_file);
             }    

            if ($files[$i]->release == '' ) {
                $html_file = str_replace('{release}', '', $html_file);
            } else {
                $html_file = str_replace('{release}',JText::_('COM_EPOSTS_FRONTEND_VERSION_TITLE').$files[$i]->release, $html_file);
            }

            // thumbnails
            $html_file = placeThumbs($html_file, $files[$i]->thumbnail, $files[$i]->thumbnail2, $files[$i]->thumbnail3);                                                    

            // support for content plugins
            if ($jlistConfig['activate.general.plugin.support'] && $jlistConfig['use.general.plugin.support.only.for.descriptions']) {  
                $files[$i]->description = JHTML::_('content.prepare', $files[$i]->description);
            }                

            if ($jlistConfig['auto.file.short.description'] && $jlistConfig['auto.file.short.description.value'] > 0){
                 if (strlen($files[$i]->description) > $jlistConfig['auto.file.short.description.value']){ 
                     $shorted_text=preg_replace("/[^ ]*$/", '..', substr($files[$i]->description, 0, $jlistConfig['auto.file.short.description.value']));
                     $html_file = str_replace('{description}', $shorted_text, $html_file);
                 } else {
                     $html_file = str_replace('{description}', $files[$i]->description, $html_file);
                 }    
            } else {
                 $html_file = str_replace('{description}', $files[$i]->description, $html_file);
            }   

            // pics for: new file / hot file / updated
            $hotpic = '<img src="'.JURI::base().'images/eposts/hotimages/'.$jlistConfig['picname.is.file.hot'].'" alt="" />';
            $newpic = '<img src="'.JURI::base().'images/eposts/newimages/'.$jlistConfig['picname.is.file.new'].'" alt="" />';
            $updatepic = '<img src="'.JURI::base().'images/eposts/updimages/'.$jlistConfig['picname.is.file.updated'].'" alt="" />';

            if ($jlistConfig['loads.is.file.hot'] > 0 && $files[$i]->downloads >= $jlistConfig['loads.is.file.hot'] ){
                $html_file = str_replace('{pic_is_hot}', $hotpic, $html_file);
            } else {    
                $html_file = str_replace('{pic_is_hot}', '', $html_file);
            }
    
            // berechnung für NEW
            $tage_diff = DatumsDifferenz_JD(date('Y-m-d H:i:s'), $files[$i]->date_added);
            if ($jlistConfig['days.is.file.new'] > 0 && $tage_diff <= $jlistConfig['days.is.file.new']){
                $html_file = str_replace('{pic_is_new}', $newpic, $html_file);
            } else {    
                $html_file = str_replace('{pic_is_new}', '', $html_file);
            }
            
            // berechnung für UPDATED
            // view only when in download is set it to updated active
            if ($files[$i]->update_active) {
                $tage_diff = DatumsDifferenz_JD(date('Y-m-d H:i:s'), $files[$i]->modified_date);
                if ($jlistConfig['days.is.file.updated'] > 0 && $tage_diff >= 0 && $tage_diff <= $jlistConfig['days.is.file.updated']){
                    $html_file = str_replace('{pic_is_updated}', $updatepic, $html_file);
                } else {    
                    $html_file = str_replace('{pic_is_updated}', '', $html_file);
                }
            } else {
                $html_file = str_replace('{pic_is_updated}', '', $html_file);
            }    

            // mp3 player
            $filetype = strtolower(substr(strrchr($files[$i]->url_download, '.'), 1));
            if ($filetype == 'mp3'){
                $mp3_path =  JURI::base().$jlistConfig['files.uploaddir'].'/'.$cat[0]->cat_dir.'/'.$files[$i]->url_download;
                $mp3_config = trim($jlistConfig['mp3.player.config']);
                $mp3_config = str_replace('', '', $mp3_config);
                $mp3_config = str_replace(';', '&', $mp3_config);
                $mp3_player =  
                '<object type="application/x-shockwave-flash" data="components/com_eposts/mp3_player_maxi.swf" width="200" height="20">
                <param name="movie" value="components/com_eposts/mp3_player_maxi.swf" />
                <param name="wmode" value="transparent"/>
                <param name="FlashVars" value="mp3='.$mp3_path.'&'.$mp3_config.'" />
                </object>';   
                if ($jlistConfig['mp3.view.id3.info']){
                    // read mp3 infos
                    $mp3_path_abs = JPATH_BASE.DS.$jlistConfig['files.uploaddir'].DS.$cat[0]->cat_dir.DS.$files[$i]->url_download;
                    $info = getID3v2Tags($mp3_path_abs);
                    if ($info){
                        // add it
                        $mp3_info = stripslashes($jlistConfig['mp3.info.layout']);
                        $mp3_info = str_replace('{name_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_TITLE'), $mp3_info);
                        $mp3_info = str_replace('{name}', $files[$i]->url_download, $mp3_info);
                        $mp3_info = str_replace('{album_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_ALBUM'), $mp3_info);
                        $mp3_info = str_replace('{album}', $info[TALB], $mp3_info);
                        $mp3_info = str_replace('{artist_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_ARTIST'), $mp3_info);
                        $mp3_info = str_replace('{artist}', $info[TPE1], $mp3_info);
                        $mp3_info = str_replace('{genre_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_GENRE'), $mp3_info);
                        $mp3_info = str_replace('{genre}', $info[TCON], $mp3_info);
                        $mp3_info = str_replace('{year_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_YEAR'), $mp3_info);
                        $mp3_info = str_replace('{year}', $info[TYER], $mp3_info);
                        $mp3_info = str_replace('{length_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_LENGTH'), $mp3_info);
                        $mp3_info = str_replace('{length}', $info[TLEN].' '.JText::_('COM_EPOSTS_FE_VIEW_ID3_MINS'), $mp3_info);
                        $html_file = str_replace('{mp3_id3_tag}', $mp3_info, $html_file); 
                    } else {
                        $html_file = str_replace('{mp3_id3_tag}', '', $html_file); 
                    }    
                } else {
                    $html_file = str_replace('{mp3_id3_tag}', '', $html_file);
                }       
                $html_file = str_replace('{mp3_player}', $mp3_player, $html_file);
            } else {
                $html_file = str_replace('{mp3_player}', '', $html_file);
                $html_file = str_replace('{mp3_id3_tag}', '', $html_file);             
            }
            // get license data and build link
            $lic = array();
            if ($files[$i]->license == '') $files[$i]->license = 0;
            $database->setQuery('SELECT * from #__eposts_license WHERE id = '.$files[$i]->license);
            $lic = $database->loadObject();
            $lic_data = '';
            if (!$lic->license_url == '') {
                $lic_data = $pic_license.'<a href="'.$lic->license_url.'" target="_blank" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_LICENCE').'">'.$lic->license_title.'</a> '.$extern_url_pic;
            } else {
                if (!$lic->license_title == '') {
                     if (!$lic->license_text == '') {
                          $lic_data = $pic_license.$lic->license_title;
                          $lic_data .= JHTML::_('tooltip', stripslashes($lic->license_text), $lic->license_title);
                     } else {
                          $lic_data = $pic_license.$lic->license_title;
                     }
                } else {
                     $lic_data = '';
                }
            }
            $html_file = str_replace('{license}',$lic_data, $html_file);
            $html_file = str_replace('{license_text}',$lic_data, $html_file);
            
            // checkboxen nur anzeigen wenn user hat zugang und checkbox in layout aktiviert ( = 0 !! )
            if ( ($users_access >= $cat_access || $cat_access == '9') && $jlistTemplates[2][0]->checkbox_off == 0 ) {
                 $html_file = str_replace('{checkbox_list}',$checkbox_list, $html_file);
            } else {
                 $html_file = str_replace('{checkbox_list}','', $html_file);
            }

			$html_file = str_replace('{cat_id}', $files[$i]->cat_id, $html_file);
			
            // file size
            if (!$files[$i]->size == '') {
                $html_file = str_replace('{size}', $pic_size.$files[$i]->size, $html_file);
                $html_file = str_replace('{filesize_value}', $pic_size.$files[$i]->size, $html_file);
            } else {
                $html_file = str_replace('{size}', '', $html_file);
                $html_file = str_replace('{filesize_value}', '', $html_file);
            }
            
            // price
            if ($files[$i]->price != '') {
                $html_file = str_replace('{price_value}', $pic_price.$files[$i]->price, $html_file);
            } else {
                $html_file = str_replace('{price_value}', '', $html_file);
            }

            // file_date
            if ($files[$i]->file_date != '0000-00-00 00:00:00') {
                 $filedate_data = $pic_date.JHTML::_('date',$files[$i]->file_date, $jlistConfig['global.datetime']);
            } else {
                 $filedate_data = '';
            }
            $html_file = str_replace('{file_date}',$filedate_data, $html_file);
            
            // date_added
            if ($files[$i]->date_added != '0000-00-00 00:00:00') {
                 $date_data = $pic_date.JHTML::_('date',$files[$i]->date_added, $jlistConfig['global.datetime']);
            } else {
                 $date_data = '';
            }
			$html_file = str_replace('{date_added}',$date_data, $html_file);
            $html_file = str_replace('{created_date_value}',$date_data, $html_file);
            
            if ($files[$i]->created_id) { 
                $database->setQuery('SELECT username FROM #__users WHERE id = '.$files[$i]->created_id);
                $createdbyname = $database->loadResult();
            }
            if ($files[$i]->modified_id) { 
                $database->setQuery('SELECT username FROM #__users WHERE id = '.$files[$i]->modified_id); 
                $modifiedbyname = $database->loadResult();
            }             

            if ($createdbyname){
                $html_file = str_replace('{created_by_value}', $createdbyname, $html_file);
            } else {
                $html_file = str_replace('{created_by_value}', $files[$i]->created_by, $html_file);
            }                
            if ($modifiedbyname){
                $html_file = str_replace('{modified_by_value}', $modifiedbyname, $html_file);
            } else {                              
                $html_file = str_replace('{modified_by_value}', $files[$i]->modified_by, $html_file);
            }
            
            // modified_date
            if ($files[$i]->modified_date != '0000-00-00 00:00:00') {
                $modified_data = $pic_date.JHTML::_('date',$files[$i]->modified_date, $jlistConfig['global.datetime']);
            } else {
                $modified_data = '';
            }
            $html_file = str_replace('{modified_date_value}',$modified_data, $html_file);

            $user_can_seen_download_url = false;
            // only view download-url when user has corect access level
            if ($users_access >= $cat_access || $cat_access == '9') {
                $user_can_seen_download_url = true;
                $blank_window = '';
                $blank_window1 = '';
                $blank_window2 = '';
                // get file extension
                $view_types = array();
                $view_types = explode(',', $jlistConfig['file.types.view']);
                $only_file_name = basename($files[$i]->url_download);
                $fileextension = strtolower(substr(strrchr($only_file_name,"."),1));
                if (in_array($fileextension, $view_types)){
                    $blank_window = 'target="_blank"';
                }    
                // check is set link to a new window?
                if ($files[$i]->extern_file && $files[$i]->extern_site   ){
                    $blank_window = 'target="_blank"';
                }

                 // direct download ohne zusammenfassung?
                 if ($jlistConfig['direct.download'] == '0'){
                     $url_task = 'summary';
                 } else {
                     if ($files[$i]->license_agree){
                         // user must agree the license - so it must viewed the summary page!
                         $url_task = 'summary';
                     } else {     
                         $url_task = 'finish';
                     }    
                 }                    
                
                  if ($cat_link_itemids){  
                     $cat_itemid = '';
                     for ($i2=0; $i2 < count($cat_link_itemids); $i2++) {
                          if ($cat_link_itemids[$i2][catid] == $files[$i]->cat_id){
                              $cat_itemid = $cat_link_itemids[$i2][id];
                          }     
                     }
                 }  
                 if (!$cat_itemid){
                     // use global itemid when no single link exists
                     $cat_itemid = $Itemid;
                 } 
                
                 $download_link = JRoute::_('index.php?option='.$option.'&Itemid='.$cat_itemid.'&view='.$url_task.'&cid='.$files[$i]->file_id.'&catid='.$files[$i]->cat_id); 
                  if ($url_task == 'finish'){ 
                      $download_link_text = '<a '.$blank_window.' href="'.$download_link.'" title="'.JText::_('COM_EPOSTS_LINKTEXT_DOWNLOAD_URL').'" class="jd_download_url">';
                  } else {
                      $download_link_text = '<a href="'.$download_link.'" title="'.JText::_('COM_EPOSTS_LINKTEXT_DOWNLOAD_URL').'">';                  
                  }    
				 if (!$pic_download){
                     $pic_download = '<img src="'.JURI::base().'images/eposts/downloadimages/'.$jlistConfig['download.pic.files'].'" align="middle" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_DOWNLOAD').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_DOWNLOAD').'" />';
                 }    
                 if ($jlistConfig['view.also.download.link.text']){
                    $html_file = str_replace('{url_download}',$download_link_text.$pic_download.'<br />'.JText::_('COM_EPOSTS_LINKTEXT_DOWNLOAD_URL').'</a>', $html_file);
			     } else {
                    $html_file = str_replace('{url_download}',$download_link_text.$pic_download.'</a>', $html_file);  
                 }    
                // mirrors
                if ($files[$i]->mirror_1) {
                    if ($files[$i]->extern_site_mirror_1 && $url_task == 'finish'){
                        $blank_window1 = 'target="_blank"';
                    }
                    $mirror1_link_dum = JRoute::_('index.php?option='.$option.'&Itemid='.$cat_itemid.'&view='.$url_task.'&cid='.$files[$i]->file_id.'&catid='.$files[$i]->cat_id.'&m=1');
                    $mirror1_link = JRoute::_('<a '.$blank_window1.' href="'.$mirror1_link_dum.'" class="jd_download_url" title="'.JText::_('COM_EPOSTS_FRONTEND_MIRROR_URL_TITLE_1').'">');
                    $mir1_down_pic = '<img src="'.JURI::base().'images/eposts/downloadimages/'.$jlistConfig['download.pic.mirror_1'].'" align="middle" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MIRROR_URL_TITLE_1').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MIRROR_URL_TITLE_1').'" />';
                    $html_file = str_replace('{mirror_1}', $mirror1_link.$mir1_down_pic.'</a>', $html_file);
                } else {
                    $html_file = str_replace('{mirror_1}', '', $html_file);
                }
                if ($files[$i]->mirror_2) {
                    if ($files[$i]->extern_site_mirror_2 && $url_task == 'finish'){
                        $blank_window2 = 'target="_blank"';
                    }
                    $mirror2_link_dum = JRoute::_('index.php?option='.$option.'&Itemid='.$cat_itemid.'&view='.$url_task.'&cid='.$files[$i]->file_id.'&catid='.$files[$i]->cat_id.'&m=2');
                    $mirror2_link = '<a '.$blank_window2.' href="'.$mirror2_link_dum.'" class="jd_download_url" title="'.JText::_('COM_EPOSTS_FRONTEND_MIRROR_URL_TITLE_2').'">';
                    $mir2_down_pic = '<img src="'.JURI::base().'images/eposts/downloadimages/'.$jlistConfig['download.pic.mirror_2'].'" align="middle" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MIRROR_URL_TITLE_2').'" title="'.JText::_('COM_EPOSTS_FRONTEND_MIRROR_URL_TITLE_2').'" />';                
                    $html_file = str_replace('{mirror_2}', $mirror2_link.$mir2_down_pic.'</a>', $html_file);
                } else {
                    $html_file = str_replace('{mirror_2}', '', $html_file);
                }            
            } else {
			     if (!$cat_itemid){
                    // use global itemid when no single link exists
                    $cat_itemid = $root_itemid;
                 }  
                 $html_file = str_replace('{url_download}', '', $html_file);
                 $html_file = str_replace('{mirror_1}', '', $html_file); 
                 $html_file = str_replace('{mirror_2}', '', $html_file); 
            }
            
            if ($jlistConfig['view.detailsite']){
                // titel als link zur detailseite
                if (!$cat_itemid){
                    // use global itemid when no single link exists
                    $cat_itemid = $root_itemid;
                } 
                $titel_link = JRoute::_('index.php?option='.$option.'&Itemid='.$cat_itemid.'&view=viewdownload&catid='.$cat[0]->cat_id.'&cid='.$files[$i]->file_id);
                $titel_link_text = '<a href="'.$titel_link.'">'.stripslashes($files[$i]->file_title).'</a>';
                $detail_link_text = '<a href="'.$titel_link.'">'.JText::_('COM_EPOSTS_FE_DETAILS_LINK_TEXT_TO_DETAILS').'</a>';
                // Symbol anzeigen - auch als url
                if ($files[$i]->file_pic != '' ) {
                    $fpicsize = $jlistConfig['file.pic.size'];
                    $filepic = '<a href="'.$titel_link.'">'.'<img src="'.JURI::base().'images/eposts/fileimages/'.$files[$i]->file_pic.'" align="top" width="'.$fpicsize.'" height="'.$fpicsize.'" border="0" alt="" /> </a>';
                } else {
                    $filepic = '';
                }
                $html_file = str_replace('{file_pic}',$filepic, $html_file);
                // link zu details am ende
                $html_file = str_replace('{link_to_details}', $detail_link_text, $html_file);
                $html_file = str_replace('{file_title}', $titel_link_text, $html_file);
            } elseif ($jlistConfig['use.download.title.as.download.link']){
                if ($user_can_seen_download_url){
                    // build title link as download link
                   if ($url_task == 'finish'){ 
                      $download_link_text = '<a '.$blank_window.' href="'.$download_link.'" title="'.JText::_('COM_EPOSTS_LINKTEXT_DOWNLOAD_URL').'" class="jd_download_url">'.$files[$i]->file_title.'</a>';
                   } else {
                      $download_link_text = '<a href="'.$download_link.'" title="'.JText::_('COM_EPOSTS_LINKTEXT_DOWNLOAD_URL').'">'.$files[$i]->file_title.'</a>';                  
                   }
                   // Symbol anzeigen - auch als url
                   if ($files[$i]->file_pic != '' ) {
                        $fpicsize = $jlistConfig['file.pic.size'];
                        $filepic = '<a href="'.$download_link.'"><img src="'.JURI::base().'images/eposts/fileimages/'.$files[$i]->file_pic.'" align="top" width="'.$fpicsize.'" height="'.$fpicsize.'" border="0" alt="" /></a>';
                   } else {
                        $filepic = '';
                   }
                   $html_file = str_replace('{file_pic}',$filepic, $html_file);
                   $html_file = str_replace('{link_to_details}', '', $html_file);
                   $html_file = str_replace('{file_title}', $download_link_text, $html_file);
                } else {
                    // can not seen url
                    $html_file = str_replace('{file_title}', $files[$i]->file_title, $html_file);
                    if ($files[$i]->file_pic != '' ) {
                        $fpicsize = $jlistConfig['file.pic.size'];
                        $filepic = '<img src="'.JURI::base().'images/eposts/fileimages/'.$files[$i]->file_pic.'" align="top" width="'.$fpicsize.'" height="'.$fpicsize.'" border="0" alt="" />';
                    } else {
                        $filepic = '';
                    }
                    $html_file = str_replace('{file_pic}',$filepic, $html_file);
                }    
            } else {
                // no links
                if ($files[$i]->file_pic != '' ) {
                    $fpicsize = $jlistConfig['file.pic.size'];
                    $filepic = '<img src="'.JURI::base().'images/eposts/fileimages/'.$files[$i]->file_pic.'" align="top" width="'.$fpicsize.'" height="'.$fpicsize.'" border="0" alt="" />';
                } else {
                    $filepic = '';
                }
                $html_file = str_replace('{file_pic}',$filepic, $html_file);
                // link zu details am ende entfernen
                $html_file = str_replace('{link_to_details}', '', $html_file);
                $html_file = str_replace('{file_title}', $files[$i]->file_title, $html_file);
            }             
            
            
            // build website url
            if (!$files[$i]->url_home == '') {
                 if (strpos($files[$i]->url_home, 'http://') !== false) {    
                     $html_file = str_replace('{url_home}',$pic_website.'<a href="'.$files[$i]->url_home.'" target="_blank" title="'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'">'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'</a> '.$extern_url_pic, $html_file);
                     $html_file = str_replace('{author_url_text} ',$pic_website.'<a href="'.$files[$i]->url_home.'" target="_blank" title="'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'">'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'</a> '.$extern_url_pic, $html_file);
                 } else {
                     $html_file = str_replace('{url_home}',$pic_website.'<a href="http://'.$files[$i]->url_home.'" target="_blank" title="'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'">'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'</a> '.$extern_url_pic, $html_file);
                     $html_file = str_replace('{author_url_text}',$pic_website.'<a href="http://'.$files[$i]->url_home.'" target="_blank" title="'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'">'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'</a> '.$extern_url_pic, $html_file);
                 }    
            } else {
                $html_file = str_replace('{url_home}', '', $html_file);
                $html_file = str_replace('{author_url_text}', '', $html_file);
            }

            // encode is link a mail
            if (strpos($files[$i]->url_author, '@') && $jlistConfig['mail.cloaking']){
                if (!$files[$i]->author) { 
                    $mail_encode = JHTML::_('email.cloak', $files[$i]->url_author);
                } else {
                    $mail_encode = JHTML::_('email.cloak',$files[$i]->url_author, true, $files[$i]->author, false);
                }        
            }
                    
            // build author link
            if ($files[$i]->author <> ''){
                if ($files[$i]->url_author <> '') {
                    if ($mail_encode) {
                        $link_author = $pic_author.' '.$mail_encode;
                    } else {
                        if (strpos($files[$i]->url_author, 'http://') !== false) {    
                            $link_author = $pic_author.'<a href="'.$files[$i]->url_author.'" target="_blank">'.$files[$i]->author.'</a> '.$extern_url_pic;
                        } else {
                            $link_author = $pic_author.'<a href="http://'.$files[$i]->url_author.'" target="_blank">'.$files[$i]->author.'</a> '.$extern_url_pic;
                        }        
                    }
                    $html_file = str_replace('{author}',$link_author, $html_file);
                    $html_file = str_replace('{author_text}',$link_author, $html_file);
                    $html_file = str_replace('{url_author}', '', $html_file);
                } else {
                    $link_author = $pic_author.$files[$i]->author;
                    $html_file = str_replace('{author}',$link_author, $html_file);
                    $html_file = str_replace('{author_text}',$link_author, $html_file);
                    $html_file = str_replace('{url_author}', '', $html_file);
                }
            } else {
                    $html_file = str_replace('{url_author}', $pic_author.$files[$i]->url_author, $html_file);
        	        $html_file = str_replace('{author}','', $html_file);
                    $html_file = str_replace('{author_text}','', $html_file); 
            }

            // set system value
            $file_sys_values = explode(',' , $jlistConfig['system.list']);
			if ($files[$i]->system == 0 ) {
                $html_file = str_replace('{system}', '', $html_file);
                 $html_file = str_replace('{system_text}', '', $html_file); 
            } else {
                $html_file = str_replace('{system}', $pic_system.$file_sys_values[$files[$i]->system], $html_file);
                $html_file = str_replace('{system_text}', $pic_system.$file_sys_values[$files[$i]->system], $html_file);
            }

            // set language value
            $file_lang_values = explode(',' , $jlistConfig['language.list']);
			if ($files[$i]->language == 0 ) {
                $html_file = str_replace('{language}', '', $html_file);
                $html_file = str_replace('{language_text}', '', $html_file);
            } else {
                $html_file = str_replace('{language}', $pic_language.$file_lang_values[$files[$i]->language], $html_file);
                $html_file = str_replace('{language_text}', $pic_language.$file_lang_values[$files[$i]->language], $html_file);
            }

            // insert rating system
            if ($jlistConfig['view.ratings']){
                $rating_system = getRatings($files[$i]->file_id);
                $html_file = str_replace('{rating}', $rating_system, $html_file);
            } else {
                $html_file = str_replace('{rating}', '', $html_file);
            }  			
            
            // custom fields
            $custom_fields_arr = existsCustomFieldsTitlesX();
            $row_custom_values = array('dummy',$files[$i]->custom_field_1, $files[$i]->custom_field_2, $files[$i]->custom_field_3, $files[$i]->custom_field_4, $files[$i]->custom_field_5,
                               $files[$i]->custom_field_6, $files[$i]->custom_field_7, $files[$i]->custom_field_8, $files[$i]->custom_field_9, $files[$i]->custom_field_10, $files[$i]->custom_field_11, $files[$i]->custom_field_12, $files[$i]->custom_field_13, $files[$i]->custom_field_14);
            for ($x=1; $x<15; $x++){
                // replace placeholder with title and value
                if (in_array($x,$custom_fields_arr[0]) && $row_custom_values[$x] && $row_custom_values[$x] != '0000-00-00'){
                    $html_file = str_replace("{custom_title_$x}", $custom_fields_arr[1][$x-1], $html_file);
                    if ($x > 5){
                        $html_file = str_replace("{custom_value_$x}", stripslashes($row_custom_values[$x]), $html_file);
                    } else {
                        $html_file = str_replace("{custom_value_$x}", $custom_fields_arr[2][$x-1][$row_custom_values[$x]], $html_file);
                    }    
                } else {
                    // remove placeholder
                    if ($jlistConfig['remove.field.title.when.empty']){
                        $html_file = str_replace("{custom_title_$x}", '', $html_file);
                    } else {
                        $html_file = str_replace("{custom_title_$x}", $custom_fields_arr[1][$x-1], $html_file);
                    }    
                    $html_file = str_replace("{custom_value_$x}", '', $html_file);
                }    
            }
            
            $html_file = str_replace('{downloads}',$pic_downloads.$files[$i]->downloads, $html_file);
            $html_file = str_replace('{hits_value}',$pic_downloads.$files[$i]->downloads, $html_file);
			$html_file = str_replace('{ordering}',$files[$i]->ordering, $html_file);
			$html_file = str_replace('{published}',$files[$i]->published, $html_file);
            
            // support for content plugins 
            if ($jlistConfig['activate.general.plugin.support'] && !$jlistConfig['use.general.plugin.support.only.for.descriptions']) {  
                $html_file = JHTML::_('content.prepare', $html_file);

            }

            $html_files .= $html_file;
    	}

		// nur anzeigen wenn files vorhanden
        if (!empty($files)) {
            $html_cat = str_replace('{files}',$html_files,$html_cat);
        } else {
            $no_files_msg = '<br /><b> '.JText::_('COM_EPOSTS_FRONTEND_NOFILES').'<br /><br /></b>';            
            $html_cat = str_replace('{files}', $no_files_msg, $html_cat);
        }    

        // top checkbox nur anzeigen wenn user hat zugang
        if ($users_access >= $cat_access || $cat_access == '9') {
            $checkbox_top = '<tr><form name="down'.$formid.'" action="'.JRoute::_('index.php?option=com_eposts&Itemid='.$cat_itemid.'&view=summary').'"
                    onsubmit="return pruefen('.$formid.',\''.JText::_('COM_EPOSTS_JAVASCRIPT_TEXT_1').' '.JText::_('COM_EPOSTS_JAVASCRIPT_TEXT_2').'\');" method="post">
                    <td width="89%" align="right">'.$jlistConfig['checkbox.top.text'].'</td>
                    <td width="11%" align="center"><input type="checkbox" name="toggle"
                    value="" onclick="checkAlle('.$i.','.$formid.');" /></td></tr>';
            
            // top checkboxen nur anzeigen wenn im layout aktiviert
            if ($jlistTemplates[2][0]->checkbox_off == 0 && !empty($files)) {
               $html_cat = str_replace('{checkbox_top}', $checkbox_top, $html_cat);
            } else {
               $html_cat = str_replace('{checkbox_top}', '', $html_cat);
            }   
        } else {
            //$load_access = (int)substr($cat->cat_access, 1, 1);
            if ($cat_access == 2){
                 // additional msg: download only for special members
                 $regg = str_replace('<br />', '', '<b>'.JText::_('COM_EPOSTS_FRONTEND_CAT_ACCESS_REGGED').' '.JText::_('COM_EPOSTS_FRONTEND_CAT_ACCESS_REGGED2').'</b>');
            } else {
                 $regg = str_replace('<br />', '', '<b>'.JText::_('COM_EPOSTS_FRONTEND_CAT_ACCESS_REGGED').'</b>');
            }
            if ($files){
                $html_cat = str_replace('{checkbox_top}', '<div style="text-align:center; padding:8px;"><img src="'.JURI::base().'components/com_eposts/assets/images/info32.png" align="middle" width="32" height="32" border="0" alt="" /> '.$regg.'</div>', $html_cat);                    
            } else {
                $html_cat = str_replace('{checkbox_top}', '', $html_cat);                    
            }    
        }
                
        $form_hidden = '<input type="hidden" name="boxchecked" value=""/> ';
        $html_cat = str_replace('{form_hidden}', $form_hidden, $html_cat);
        $html_cat .= '<input type="hidden" name="catid" value="'.$catid.'"/>';
        $html_cat .= JHTML::_( 'form.token' ).'</form>';

        // button nur anzeigen wenn checkboxen aktiviert
        $button = '<input class="button" type="submit" name="weiter" value="'.JText::_('COM_EPOSTS_FORM_BUTTON_TEXT').'"/>';
        
        // only view submit button when user has corect access level
        if (($users_access >= $cat_access || $cat_access == '9') && $jlistTemplates[2][0]->checkbox_off == 0 && !empty($files)) {
            $html_cat = str_replace('{form_button}', $button, $html_cat);
        } else {
            $html_cat = str_replace('{form_button}', '', $html_cat);
        } 
    	$footer =  makeFooter(true, false, true, $sum_pages, $limit, $limitstart, $site_aktuell, $pageNav, false, false); 
        $html_cat .= $footer;
        
        // remove empty html tags
        if ($jlistConfig['remove.empty.tags']){
            $html_cat = removeEmptyTags($html_cat);
        }    

        if ( !$jlistConfig['offline'] ) {
            echo $html_cat;
        } else {
            if ($aid == 3) {
                echo JText::_('COM_EPOSTS_BACKEND_OFFLINE_ADMIN_MESSAGE_TEXT');
                echo $html_cat;
            } else {
                $html_off = '<br /><br />'.stripslashes($jlistConfig['offline.text']).'<br /><br />';
                $html_off .= $footer;
                echo $html_off;
            }
        }    
}


/* #################################################################################
/  Einzelnen Download anzeigen mit Detail Infos
/  ################################################################################# */
function showDownload($option, $file, $cat, $access, $edit_link, $user_can_edit){
    global $jlistConfig, $jlistTemplates, $Itemid, $mainframe, $addScriptJWAjaxVote, $page_title;
    $user = &JFactory::getUser();
    $user_access = checkAccess_JD();
    $users_access =  (int)substr($user_access, 0, 1);
    
    $database = &JFactory::getDBO();
    $mainframe = JFactory::getApplication();
    $app = JFactory::getApplication();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.$cat->cat_title.' - '.$file->file_title );
    // for tabs
    jimport('joomla.html.pane');
    // für JHMTL Tooltip
    JHTML::_('behavior.tooltip');
    
    $jmeta = $document->getMetaData( 'keywords' ); 
    $jmetadesc = $document->getMetaData( 'description' ); 
    if (!$file->metakey){
        $document->setMetaData( 'keywords' , $jmeta);
    } else {
        $document->setMetaData( 'keywords' , strip_tags($file->metakey));
    }    
    if (!$file->metadesc){
        $document->setMetaData( 'description' , $jmetadesc);
    } else {
        $document->setMetaData( 'description' , strip_tags($file->metadesc));
    }     

    $html_file = makeHeader($html_file, true, false, false, 0, true, false, false, false, false, 0, 0, 0, 0, 0, 0, '', '');
    echo $html_file;        
    
    $html_file = str_replace('{price_value}',$file->price, $jlistTemplates[5][0]->template_text);
    
    // translate data title
    $html_file = str_replace('{pathway_text}', JText::_('COM_EPOSTS_FE_DETAILS_PATHWAY_TEXT'), $html_file);
    $html_file = str_replace('{details_block_title}', JText::_('COM_EPOSTS_FE_DETAILS_DATA_BLOCK_TITLE'), $html_file);
    $html_file = str_replace('{file_name}', $file->url_download, $html_file);
    
    // create titles
    $html_file = buildFieldTitles($html_file, $file);
    
    // tabs or sliders when the placeholders are used
    if ((int)$jlistConfig['use.tabs.type'] > 0){
       if ((int)$jlistConfig['use.tabs.type'] == 1){
            $jd_pane =& JPane::getInstance('sliders');
       } else {
           $jd_pane =& JPane::getInstance('tabs');
       }    
       $html_file = str_replace('{tabs begin}', $jd_pane->startPane('jdpane'), $html_file);
       $html_file = str_replace('{tab description}', $jd_pane->startPanel(JText::_('COM_EPOSTS_FE_TAB_DESCRIPTION_TITLE'), 'panel1'), $html_file);
       $html_file = str_replace('{tab description end}', $jd_pane->endPanel(JText::_('COM_EPOSTS_FE_TAB_DESCRIPTION_TITLE'), 'panel1'), $html_file);
       $html_file = str_replace('{tab pics}', $jd_pane->startPanel(JText::_('COM_EPOSTS_FE_TAB_PICS_TITLE'), 'panel2'), $html_file);
       $html_file = str_replace('{tab pics end}', $jd_pane->endPanel(JText::_('COM_EPOSTS_FE_TAB_PICS_TITLE'), 'panel2'), $html_file);
       $html_file = str_replace('{tab mp3}', $jd_pane->startPanel(JText::_('COM_EPOSTS_FE_TAB_AUDIO_TITLE'), 'panel3'), $html_file);
       $html_file = str_replace('{tab mp3 end}', $jd_pane->endPanel(JText::_('COM_EPOSTS_FE_TAB_AUDIO_TITLE'), 'panel3'), $html_file);
       $html_file = str_replace('{tab data}', $jd_pane->startPanel(JText::_('COM_EPOSTS_FE_TAB_DATA_TITLE'), 'panel4'), $html_file);
       $html_file = str_replace('{tab data end}', $jd_pane->endPanel(JText::_('COM_EPOSTS_FE_TAB_DATA_TITLE'), 'panel4'), $html_file);
       $html_file = str_replace('{tab download}', $jd_pane->startPanel(JText::_('COM_EPOSTS_FE_TAB_DOWNLOAD_TITLE'), 'panel5'), $html_file);
       $html_file = str_replace('{tab download end}', $jd_pane->endPanel(JText::_('COM_EPOSTS_FE_TAB_DOWNLOAD_TITLE'), 'panel5'), $html_file);
       $html_file = str_replace('{tab custom1}', $jd_pane->startPanel($jlistConfig['additional.tab.title.1'], 'panel6'), $html_file);
       $html_file = str_replace('{tab custom1 end}', $jd_pane->endPanel($jlistConfig['additional.tab.title.1'], 'panel6'), $html_file);      
       $html_file = str_replace('{tab custom2}', $jd_pane->startPanel($jlistConfig['additional.tab.title.2'], 'panel7'), $html_file);
       $html_file = str_replace('{tab custom2 end}', $jd_pane->endPanel($jlistConfig['additional.tab.title.2'], 'panel7'), $html_file);
       $html_file = str_replace('{tab custom3}', $jd_pane->startPanel($jlistConfig['additional.tab.title.3'], 'panel8'), $html_file);
       $html_file = str_replace('{tab custom3 end}', $jd_pane->endPanel($jlistConfig['additional.tab.title.3'], 'panel8'), $html_file);
       $html_file = str_replace('{tabs end}', $jd_pane->endPane('jdpane'), $html_file);      
    } else {
       // delete the placeholder 
       $html_file = str_replace('{tabs begin}', '', $html_file);
       $html_file = str_replace('{tab description}', '', $html_file);
       $html_file = str_replace('{tab description end}', '', $html_file);
       $html_file = str_replace('{tab pics}', '', $html_file);
       $html_file = str_replace('{tab pics end}', '', $html_file);
       $html_file = str_replace('{tab mp3}', '', $html_file);
       $html_file = str_replace('{tab mp3 end}', '', $html_file);
       $html_file = str_replace('{tab data}', '', $html_file);
       $html_file = str_replace('{tab data end}', '', $html_file);
       $html_file = str_replace('{tab download}', '', $html_file);
       $html_file = str_replace('{tab download end}', '', $html_file);
       $html_file = str_replace('{tab custom1}', '', $html_file);
       $html_file = str_replace('{tab custom1 end}', '', $html_file);      
       $html_file = str_replace('{tab custom2}', '', $html_file);
       $html_file = str_replace('{tab custom2 end}', '', $html_file);
       $html_file = str_replace('{tab custom3}', '', $html_file);
       $html_file = str_replace('{tab custom3 end}', '', $html_file);
       $html_file = str_replace('{tabs end}', '', $html_file);      
    }    

    // custom fields
    $custom_fields_arr = existsCustomFieldsTitlesX();
    $row_custom_values = array('dummy',$file->custom_field_1, $file->custom_field_2, $file->custom_field_3, $file->custom_field_4, $file->custom_field_5,
                               $file->custom_field_6, $file->custom_field_7, $file->custom_field_8, $file->custom_field_9, $file->custom_field_10, $file->custom_field_11, $file->custom_field_12, $file->custom_field_13, $file->custom_field_14);
    for ($x=1; $x<15; $x++){
        // replace placeholder with title and value
        if (in_array($x,$custom_fields_arr[0]) && $row_custom_values[$x] && $row_custom_values[$x] != '0000-00-00'){
            $html_file = str_replace("{custom_title_$x}", $custom_fields_arr[1][$x-1], $html_file);
            if ($x > 5){
                $html_file = str_replace("{custom_value_$x}", stripslashes($row_custom_values[$x]), $html_file);
            } else {
                $html_file = str_replace("{custom_value_$x}", $custom_fields_arr[2][$x-1][$row_custom_values[$x]], $html_file);
            }    
        } else {
            // remove placeholder
            if ($jlistConfig['remove.field.title.when.empty']){
                $html_file = str_replace("{custom_title_$x}", '', $html_file);
            } else {
                $html_file = str_replace("{custom_title_$x}", $custom_fields_arr[1][$x-1], $html_file);
            }    
            $html_file = str_replace("{custom_value_$x}", '', $html_file);
        }    
    }
    
    // google adsense
    if ($jlistConfig['google.adsense.active']){
       $html_file = str_replace('{google_adsense}', stripslashes($jlistConfig['google.adsense.code']), $html_file);
    } else {
       $html_file = str_replace('{google_adsense}', '', $html_file);
    }
    // report download link
    if ($jlistConfig['use.report.download.link']){
       $report_link = '<a href="'.JRoute::_("index.php?option=com_eposts&Itemid=".$Itemid."&view=report&cid=".$file->file_id).'" rel="nofollow">'.JText::_('COM_EPOSTS_CONFIG_REPORT_FILE_LINK_TEXT').'</a>';
       if ($jlistConfig['report.link.only.regged'] && !$user->guest || !$jlistConfig['report.link.only.regged']) {
           $html_file = str_replace('{report_link}', $report_link, $html_file);
       } else {
           $html_file = str_replace('{report_link}', '', $html_file);
       } 
    } else {
       $html_file = str_replace('{report_link}', '', $html_file);
    }
    
    $database->setQuery('SELECT cat_title from #__eposts_cats WHERE cat_id = '.$file->cat_id);
    $cattitle = $database->loadResult();
    $html_file = str_replace('{cat_title}', $cattitle, $html_file);      
    
    // a little pic for extern links
    $extern_url_pic = '<img src="'.JURI::base().'components/com_eposts/assets/images/link_extern.gif" alt="" />';
     
    // get pic
    if ($file->file_pic != '' ) {
        $fpicsize = $jlistConfig['file.pic.size'];
        $filepic = '<img src="'.JURI::base().'images/eposts/fileimages/'.$file->file_pic.'" align="top" width="'.$fpicsize.'" height="'.$fpicsize.'" border="0" alt="" /> ';
    } else {
        $filepic = '';
    }
    $html_file = str_replace('{file_pic}',$filepic, $html_file);
    $html_file = str_replace('{file_title}', stripslashes($file->file_title), $html_file);    
    
    if ($file->release) {
        $html_file = str_replace('{release}',JText::_('COM_EPOSTS_FRONTEND_VERSION_TITLE').$file->release.$edit_link, $html_file);        
    } else {
        $html_file = str_replace('{release}', $edit_link, $html_file);        
    }

    // thumbnails
    $html_file = placeThumbs($html_file, $file->thumbnail, $file->thumbnail2, $file->thumbnail3);                                                    

    // description
    if (!$file->description_long){
        // support for content plugins
        if ($jlistConfig['activate.general.plugin.support'] && $jlistConfig['use.general.plugin.support.only.for.descriptions']) {  
           $file->description = JHTML::_('content.prepare', $file->description);    /*old 1.5 way */        
        }        
        $html_file = str_replace('{description_long}',$file->description, $html_file); 
    } else {
        // support for content plugins
        if ($jlistConfig['activate.general.plugin.support'] && $jlistConfig['use.general.plugin.support.only.for.descriptions']) {  
            $file->description_long = JHTML::_('content.prepare', $file->description_long);   /*old 1.5 way */             
        }
        $html_file = str_replace('{description_long}',$file->description_long, $html_file);
    }
    
    // pics for: new file / hot file /updated
    $hotpic = '<img src="'.JURI::base().'images/eposts/hotimages/'.$jlistConfig['picname.is.file.hot'].'" alt="" />';
    $newpic = '<img src="'.JURI::base().'images/eposts/newimages/'.$jlistConfig['picname.is.file.new'].'" alt="" />';
    $updatepic = '<img src="'.JURI::base().'images/eposts/updimages/'.$jlistConfig['picname.is.file.updated'].'" alt="" />';

    // berechnung für HOT
    if ($jlistConfig['loads.is.file.hot'] > 0 && $file->downloads >= $jlistConfig['loads.is.file.hot'] ){
        $html_file = str_replace('{pic_is_hot}', $hotpic, $html_file);
    } else {    
        $html_file = str_replace('{pic_is_hot}', '', $html_file);
    }
    
    // berechnung für NEW
    $tage_diff = DatumsDifferenz_JD(date('Y-m-d H:i:s'), $file->date_added);
    if ($jlistConfig['days.is.file.new'] > 0 && $tage_diff <= $jlistConfig['days.is.file.new']){
        $html_file = str_replace('{pic_is_new}', $newpic, $html_file);
    } else {    
        $html_file = str_replace('{pic_is_new}', '', $html_file);
    }
    
    // berechnung für UPDATED
    // view only when in download is set it to updated active
    if ($file->update_active) {
        $tage_diff = DatumsDifferenz_JD(date('Y-m-d H:i:s'), $file->modified_date);
        if ($jlistConfig['days.is.file.updated'] > 0 && $tage_diff >= 0 && $tage_diff <= $jlistConfig['days.is.file.updated']){
            $html_file = str_replace('{pic_is_updated}', $updatepic, $html_file);
        } else {    
            $html_file = str_replace('{pic_is_updated}', '', $html_file);
        }
    } else {
       $html_file = str_replace('{pic_is_updated}', '', $html_file);
    }    

    // mp3 player
    if ($file->extern_file){
      $extern_mp3 = true;
      $filetype = strtolower(substr(strrchr($file->extern_file, '.'), 1));
    } else {    
      $filetype = strtolower(substr(strrchr($file->url_download, '.'), 1));
      $extern_mp3 = false;
    }  
    if ($filetype == 'mp3'){
        if ($extern_mp3){
            $mp3_path = $file->extern_file;
        } else {        
            $mp3_path =  JURI::base().$jlistConfig['files.uploaddir'].'/'.$cat->cat_dir.'/'.$file->url_download;
        }    
        $mp3_config = trim($jlistConfig['mp3.player.config']);
        $mp3_config = str_replace('', '', $mp3_config);
        $mp3_config = str_replace(';', '&', $mp3_config);
        $mp3_player =  
        '<object type="application/x-shockwave-flash" data="components/com_eposts/mp3_player_maxi.swf" width="200" height="20">
        <param name="movie" value="components/com_eposts/mp3_player_maxi.swf" />
        <param name="wmode" value="transparent"/>
        <param name="FlashVars" value="mp3='.$mp3_path.'&'.$mp3_config.'" />
        </object>';   
        if ($jlistConfig['mp3.view.id3.info'] && !$extern_mp3){
           // read mp3 infos
            $mp3_path_abs = JPATH_BASE.DS.$jlistConfig['files.uploaddir'].DS.$cat->cat_dir.DS.$file->url_download;
            $info = getID3v2Tags($mp3_path_abs);
            if ($info){
                // add it
                $mp3_info = stripslashes($jlistConfig['mp3.info.layout']);
                $mp3_info = str_replace('{name_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_TITLE'), $mp3_info);
                $mp3_info = str_replace('{name}', $file->url_download, $mp3_info);
                $mp3_info = str_replace('{album_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_ALBUM'), $mp3_info);
                $mp3_info = str_replace('{album}', $info[TALB], $mp3_info);
                $mp3_info = str_replace('{artist_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_ARTIST'), $mp3_info);
                $mp3_info = str_replace('{artist}', $info[TPE1], $mp3_info);
                $mp3_info = str_replace('{genre_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_GENRE'), $mp3_info);
                $mp3_info = str_replace('{genre}', $info[TCON], $mp3_info);
                $mp3_info = str_replace('{year_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_YEAR'), $mp3_info);
                $mp3_info = str_replace('{year}', $info[TYER], $mp3_info);
                $mp3_info = str_replace('{length_title}', JText::_('COM_EPOSTS_FE_VIEW_ID3_LENGTH'), $mp3_info);
                $mp3_info = str_replace('{length}', $info[TLEN].' '.JText::_('COM_EPOSTS_FE_VIEW_ID3_MINS'), $mp3_info);
                $html_file = str_replace('{mp3_id3_tag}', $mp3_info, $html_file); 
            } else {
                $html_file = str_replace('{mp3_id3_tag}', '', $html_file); 
            }    
        } else {
            $html_file = str_replace('{mp3_id3_tag}', '', $html_file);
        }        
        $html_file = str_replace('{mp3_player}', $mp3_player, $html_file);
    } else {
        $html_file = str_replace('{mp3_player}', '', $html_file);
        $html_file = str_replace('{mp3_id3_tag}', '', $html_file);             
    }
    
    // get license data and build link
    $lic = array();
    if ($file->license == '') $file->license = 0;
    $database->setQuery('SELECT * from #__eposts_license WHERE id = '.$file->license);
    $lic = $database->loadObject();
    $lic_data = '';
    if (!$lic->license_url == '') {
         $lic_data = $pic_license.'<a href="'.$lic->license_url.'" target="_blank" title="'.JText::_('COM_EPOSTS_FRONTEND_MINI_ICON_ALT_LICENCE').'">'.$lic->license_title.'</a> '.$extern_url_pic;
    } else {
        if (!$lic->license_title == '') {
             if (!$lic->license_text == '') {
                  $lic_data = $pic_license.$lic->license_title;
                  $lic_data .= JHTML::_('tooltip', stripslashes($lic->license_text), $lic->license_title);
             } else {
                  $lic_data = $pic_license.$lic->license_title;
             }
        } else {
            $lic_data = '';
        }
    }
    if ($file->created_id) { 
            $database->setQuery("SELECT username FROM #__users WHERE id = '$file->created_id'");
            $createdbyname = $database->loadResult();
    }
    if ($file->modified_id) { 
            $database->setQuery("SELECT username FROM #__users WHERE id = '$file->modified_id'");
            $modifiedbyname = $database->loadResult();
    }
    $html_file = str_replace('{license_text}',$lic_data, $html_file);
    $html_file = str_replace('{filesize_value}',$file->size, $html_file);
    if ($createdbyname){
        $html_file = str_replace('{created_by_value}',$createdbyname, $html_file);    
    } else {
        $html_file = str_replace('{created_by_value}',$file->created_by, $html_file);
    }    
    if ($modifiedbyname){
        $html_file = str_replace('{modified_by_value}',$modifiedbyname, $html_file);
    } else {
        $html_file = str_replace('{modified_by_value}',$file->modified_by, $html_file);
    }    
    
    if ($file->modified_date != '0000-00-00 00:00:00') {
        $modified_data = $pic_date.JHTML::_('date',$file->modified_date, $jlistConfig['global.datetime']);
    } else {
        $modified_data = '';
    }
    $html_file = str_replace('{modified_date_value}',$modified_data, $html_file);
    
    // funktion zur berechnung entfernt - hier nur falls vorhanden platzhalter entfernen
    $html_file = str_replace('{download_time}','', $html_file);    

    // file_date
    if ($file->file_date != '0000-00-00 00:00:00') {
         $filedate_data = $pic_date.JHTML::_('date',$file->file_date, $jlistConfig['global.datetime']);
    } else {
         $filedate_data = '';
    }
    $html_file = str_replace('{file_date}',$filedate_data, $html_file);

    // date_added    
    if ($file->date_added != '0000-00-00 00:00:00') {
        $date_data = $pic_date.JHTML::_('date',$file->date_added, $jlistConfig['global.datetime']);
    } else {
        $date_data = '';
    }
    $html_file = str_replace('{created_date_value}',$date_data, $html_file);

    // only view download link when user has corect access level
    if ($users_access >= $access[1] || $access[1] == '9') {
        $blank_window = '';
        $blank_window1 = '';
        $blank_window2 = '';
        // get file extension
        $view_types = array();
        $view_types = explode(',', $jlistConfig['file.types.view']);
        $only_file_name = basename($file->url_download);
        $fileextension = strtolower(substr(strrchr($only_file_name,"."),1));
        if (in_array($fileextension, $view_types)){
            $blank_window = 'target="_blank"';
        }    
        // check is set link to a new window?
        if ($file->extern_file && $file->extern_site   ){
            $blank_window = 'target="_blank"';
        }
        // direct download ohne zusammenfassung?
        if ($jlistConfig['direct.download'] == '0'){ 
            $url_task = 'summary';
        } else {
            if ($file->license_agree){
                // user must agree the license - so it must viewed the summary page!
                $url_task = 'summary';
            } else {     
                $url_task = 'finish';
            }
        }
        $download_link = JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view='.$url_task.'&cid='.$file->file_id.'&catid='.$file->cat_id);
        if ($url_task == 'finish'){
            $download_link_text = '<a '.$blank_window.' href="'.$download_link.'" class="jd_download_url"><img src="'.JURI::base().'images/eposts/downloadimages/'.$jlistConfig['download.pic.details'].'" border="0" alt="'.JText::_('COM_EPOSTS_LINKTEXT_DOWNLOAD_URL').'" /></a>';
        } else {
            $download_link_text = '<a href="'.$download_link.'" class="jd_download_url"><img src="'.JURI::base().'images/eposts/downloadimages/'.$jlistConfig['download.pic.details'].'" border="0" alt="'.JText::_('COM_EPOSTS_LINKTEXT_DOWNLOAD_URL').'" /></a>';
        }
        $html_file = str_replace('{url_download}',$pic_download.$download_link_text, $html_file);
        
        // mirrors
        if ($file->mirror_1) {
            if ($file->extern_site_mirror_1 && $url_task == 'finish'){
                $blank_window1 = 'target="_blank"';
            }
            $mirror1_link_dum = JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view='.$url_task.'&cid='.$file->file_id.'&catid='.$file->cat_id.'&m=1');
            $mirror1_link = '<a '.$blank_window1.' href="'.$mirror1_link_dum.'" class="jd_download_url"><img src="'.JURI::base().'images/eposts/downloadimages/'.$jlistConfig['download.pic.mirror_1'].'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MIRROR_URL_TITLE_1').'" /></a>';
            $html_file = str_replace('{mirror_1}', $mirror1_link, $html_file);
        } else {
            $html_file = str_replace('{mirror_1}', '', $html_file);
        }
        if ($file->mirror_2) {
            if ($file->extern_site_mirror_2 && $url_task == 'finish'){
                $blank_window2 = 'target="_blank"';
            }            
            $mirror2_link_dum = JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view='.$url_task.'&cid='.$file->file_id.'&catid='.$file->cat_id.'&m=2');
            $mirror2_link = '<a '.$blank_window2.' href="'.$mirror2_link_dum.'" class="jd_download_url"><img src="'.JURI::base().'images/eposts/downloadimages/'.$jlistConfig['download.pic.mirror_2'].'" border="0" alt="'.JText::_('COM_EPOSTS_FRONTEND_MIRROR_URL_TITLE_2').'" /></a>';
            $html_file = str_replace('{mirror_2}', $mirror2_link, $html_file);
        } else {
            $html_file = str_replace('{mirror_2}', '', $html_file);
        }            
    } else {
        //Infotext kein zugriff 
        if ($access[1] == 2){
            // additional msg: download only for special members
            $regg = str_replace('<br />', '', JText::_('COM_EPOSTS_FRONTEND_CAT_ACCESS_REGGED').' '.JText::_('COM_EPOSTS_FRONTEND_CAT_ACCESS_REGGED2'));
        } else {
            $regg = str_replace('<br />', '', JText::_('COM_EPOSTS_FRONTEND_CAT_ACCESS_REGGED'));
        }    
        $html_file = str_replace('{url_download}', $regg, $html_file);
        $html_file = str_replace('{mirror_1}', '', $html_file); 
        $html_file = str_replace('{mirror_2}', '', $html_file); 
    }    
    

    // build website url
    if (!$file->url_home == '') {
         if (strpos($file->url_home, 'http://') !== false) {    
             $html_file = str_replace('{author_url_text}',$pic_website.'<a href="'.$file->url_home.'" target="_blank" title="'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'">'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'</a> '.$extern_url_pic, $html_file);
         } else {
             $html_file = str_replace('{author_url_text}',$pic_website.'<a href="http://'.$file->url_home.'" target="_blank" title="'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'">'.JText::_('COM_EPOSTS_FRONTEND_HOMEPAGE').'</a> '.$extern_url_pic, $html_file);
         }    
    } else {
        $html_file = str_replace('{author_url_text}', '', $html_file);
    }

    // encode is link a mail
    $link_author = '';
    if (strpos($file->url_author, '@') && $jlistConfig['mail.cloaking']){
        if (!$file->author) { 
            $mail_encode = JHTML::_('email.cloak',$file->url_author);
        } else {
            $mail_encode = JHTML::_('email.cloak',$file->url_author, true, $file->author, false);
        }        
    }
                    
    // build author link
    if ($file->author <> ''){
         if ($file->url_author <> '') {
              if ($mail_encode) {
                  $link_author = $mail_encode;
              } else {
                  if (strpos($file->url_author, 'http://') !== false) {
                     $link_author = $pic_author.'<a href="'.$file->url_author.'" target="_blank">'.$file->author.'</a> '.$extern_url_pic;
                  } else {
                     $link_author = $pic_author.'<a href="http://'.$file->url_author.'" target="_blank">'.$file->author.'</a>  '.$extern_url_pic;
                  }        
              }
              $html_file = str_replace('{author_text}',$link_author, $html_file);
              $html_file = str_replace('{url_author}', '', $html_file);
         } else {
              $link_author = $pic_author.$file->author;
              $html_file = str_replace('{author_text}',$link_author, $html_file);
              $html_file = str_replace('{url_author}', '', $html_file);
         }
    } else {
        $html_file = str_replace('{url_author}', $pic_author.$file->url_author, $html_file);
        $html_file = str_replace('{author_text}','', $html_file);
    }

    // set system value
    $file_sys_values = explode(',' , $jlistConfig['system.list']);
    if ($file->system == 0 ) {
         $html_file = str_replace('{system_text}', '', $html_file);
    } else {
         $html_file = str_replace('{system_text}', $pic_system.$file_sys_values[$file->system], $html_file);
    }

    // set language value
    $file_lang_values = explode(',' , $jlistConfig['language.list']);
    if ($file->language == 0 ) {
        $html_file = str_replace('{language_text}', '', $html_file);
    } else {
        $html_file = str_replace('{language_text}', $pic_language.$file_lang_values[$file->language], $html_file);
    }
    $html_file = str_replace('{hits_value}',$file->downloads, $html_file);

    // remove empty html tags
    if ($jlistConfig['remove.empty.tags']){
        $html_file = removeEmptyTags($html_file);
    }
         
    // Option for JComments integration
    if ($jlistConfig['jcomments.active']){
        $jcomments = $mainframe->getCfg('absolute_path') . '/components/com_jcomments/jcomments.php';
        if (file_exists($jcomments)) {
            require_once($jcomments);
            $obj_id = $file->file_id;
            $obj_title = $file->file_title;
            $html_file .= JComments::showComments($obj_id, 'com_eposts', $obj_title);
        }    
    }

    // Option for JomComment integration
    if ($jlistConfig['view.jom.comment']){
       if (file_exists(JPATH_PLUGINS . DS . 'content' . DS . 'jom_comment_bot.php')){
           include_once( JPATH_PLUGINS . DS . 'content' . DS . 'jom_comment_bot.php' );
           $html_file .= jomcomment($file->file_id, "com_eposts");
       }    
    }    
    
    // insert rating system
    if ($jlistConfig['view.ratings']){
        $rating_system = getRatings($file->file_id);
        $html_file = str_replace('{rating}', $rating_system, $html_file);
    } else {
        $html_file = str_replace('{rating}', '', $html_file);
    }    
    
    $footer = makeFooter(true, false, false, $sum_pages, $limit, $limitstart, $site_aktuell, false, false, true);  
    $html_file .= $footer;
    
    // support for content plugins
    if ($jlistConfig['activate.general.plugin.support'] && !$jlistConfig['use.general.plugin.support.only.for.descriptions']) {  
        $html_file = JHTML::_('content.prepare', $html_file);
    }
    
    if ( !$jlistConfig['offline'] ) {
            echo $html_file;
        } else {
            if ($aid == 3) {
                echo JText::_('COM_EPOSTS_BACKEND_OFFLINE_ADMIN_MESSAGE_TEXT');
                echo $html_file;
            } else {
                $html_off = '<br /><br />'.stripslashes($jlistConfig['offline.text']).'<br /><br />';
                $html_off .= $footer;
                echo $html_off;
            }
        }
}    

/* #################################################################################
/  View Summary page with link to Download
/  ################################################################################# */
function Summary($option, $marked_files_id, $mail_files, $filename, $download_link, $del_ok, $extern_site, $sum_aup_points, $has_licenses, $open_in_blank_page, $must_confirm, $license_text, $zip_file_info, $file_title){
    global $jlistConfig, $jlistTemplates, $Itemid, $mainframe, $page_title;
    $user = &JFactory::getUser();
    $database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_BACKEND_TEMP_TYP3').htmlspecialchars($file_title));
    
    $html_sum = makeHeader($html_sum, true, false, false, 0, false, false, false, true, false, 0, $sum_pages, $list_per_page, $total, $list_start, $pageNav, '', '');
    echo $html_sum;
   
    // build output from template
    $html_sum = $jlistTemplates[3][0]->template_text;
    $html_sum = str_replace('{download_liste}', $mail_files, $html_sum);
    $html_sum = str_replace('{title_text}', JText::_('COM_EPOSTS_FE_SUMMARY_PAGE_TITLE_TEXT'), $html_sum);
    $html_sum = str_replace('{info_zip_file_size}', $zip_file_info, $html_sum);
    // remove placeholder from prior version
    $html_sum = str_replace('{license_note}', '', $html_sum);
        
    // summary pic
    $sum_size = $jlistConfig['cat.pic.size'];
    $sumpic = '<img src="'.JURI::base().'components/com_eposts/assets/images/summary.png" width="'.$sum_size.'" height="'.$sum_size.'" border="0" alt="" /> ';
    $html_sum = str_replace('{summary_pic}', $sumpic, $html_sum);
        
    // google adsense
    if ($jlistConfig['google.adsense.active']){
        $html_sum = str_replace('{google_adsense}', stripslashes($jlistConfig['google.adsense.code']), $html_sum);
    } else {
        $html_sum = str_replace('{google_adsense}', '', $html_sum);
    }    
    if ($user->guest && $jlistConfig['countdown.active']){
        $countdown = '<script type="text/javascript"> counter='.$jlistConfig['countdown.start.value'].'; active=setInterval("countdown2()",1000);
                       function countdown2(){
                          if (counter >0){
                              counter-=1;
                              document.getElementById("countdown").innerHTML=sprintf(\''.addslashes($jlistConfig['countdown.text']).'\',counter);
                          } else {
                              document.getElementById("countdown").innerHTML=\''.'{link}'.'\'
                              window.clearInterval(active);
                          }    
                        } </script>';
        }
    // support for AUP
    if ($jlistConfig['use.alphauserpoints']){
        $api_AUP = JPATH_SITE.DS.'components'.DS.'com_alphauserpoints'.DS.'helper.php';
        if (file_exists($api_AUP)){
            require_once ($api_AUP);
            $profil = AlphaUserPointsHelper:: getUserInfo('', $user->id);
        }
        if ($profil){
            $points_info = sprintf( JText::_('COM_EPOSTS_FE_VIEW_AUP_SUM_POINTS'), $sum_aup_points, $profil->points);
            $html_sum = str_replace('{aup_points_info}', $points_info, $html_sum); 
        } else {
            $points_info = sprintf( JText::_('COM_EPOSTS_FE_VIEW_AUP_SUM_POINTS'), $sum_aup_points, 0);
            $html_sum = str_replace('{aup_points_info}', $points_info, $html_sum); 
        }    
    } else {
        $html_sum = str_replace('{aup_points_info}', '', $html_sum); 
    }    
            
    if (count($marked_files_id) > 1) {
        // mass download
         if ($must_confirm){
            $html_sum = str_replace('{license_title}','', $html_sum);
            $html_sum = str_replace('{license_text}', '', $html_sum);
            $agree_form = '<form action="'.$download_link.'" method="post" name="jd_agreeForm" id="jd_agreeForm" >';
            $agree_form .= '<input type="checkbox" name="license_agree" onclick="enableDownloadButton(this)" /> '.JText::_('COM_EPOSTS_FE_SUMMARY_LICENSE_VIEW_AGREE_TEXT').'<br /><br />';
            $agree_form .= '<input type="submit" name="submit" id="jd_license_submit" class="button" value="'.JText::_('COM_EPOSTS_LINKTEXT_DOWNLOAD_URL').'" disabled="disabled" />';
            $agree_form .= JHTML::_( 'form.token' )."</form>";
        } else {
            $html_sum = str_replace('{license_text}', '', $html_sum);
            $html_sum = str_replace('{license_title}', '', $html_sum);
            $html_sum = str_replace('{license_checkbox}', '', $html_sum);
        }
        
        $link = '<div id="countdown" style="text-align:center"><a href="'.$download_link.'" target="_self"  title="'.JText::_('COM_EPOSTS_LINKTEXT_ZIP').'"><img src="'.JURI::base().'images/eposts/downloadimages/'.$jlistConfig['download.pic.details'].'" border="0" alt="'.JText::_('COM_EPOSTS_LINKTEXT_ZIP').'" /></a></div>';
        if ($countdown){
           if ($must_confirm){
               $countdown = str_replace('{link}', $agree_form, $countdown);
               $html_sum = str_replace('{license_checkbox}', '<div id="countdown">'.$countdown.'</div>', $html_sum);
               $html_sum = str_replace('{download_link}', '', $html_sum);
           } else {
                 $countdown = str_replace('{link}', $link, $countdown);
                 $html_sum = str_replace('{download_link}', '<div id="countdown">'.$countdown.'</div>', $html_sum);
           }    
        } else {    
           if ($must_confirm){
               $html_sum = str_replace('{license_checkbox}', $agree_form, $html_sum);
               $html_sum = str_replace('{download_link}', '', $html_sum);
           } else {   
               $html_sum = str_replace('{download_link}', $link, $html_sum);
           }
        }    
        $html_sum = str_replace('{external_download_info}', '', $html_sum);
    } else {
        // single download          
        if ($must_confirm){
            if ($license_text != ''){
                $html_sum = str_replace('{license_title}', JText::_('COM_EPOSTS_FE_SUMMARY_LICENSE_VIEW_TITLE'), $html_sum);
                $html_sum = str_replace('{license_text}', '<div id="jd_license_text">'.$license_text.'</div>', $html_sum);
            } else {
                $html_sum = str_replace('{license_title}', '', $html_sum);
                $html_sum = str_replace('{license_text}', '', $html_sum);
            }    
            $agree_form = '<form action="'.$download_link.'" method="post" name="jd_agreeForm" id="jd_agreeForm" >';
            $agree_form .= '<input type="checkbox" name="license_agree" onclick="enableDownloadButton(this)" /> '.JText::_('COM_EPOSTS_FE_SUMMARY_LICENSE_VIEW_AGREE_TEXT').'<br /><br />';
            $agree_form .= '<input type="submit" name="submit" id="jd_license_submit" class="button" value="'.JText::_('COM_EPOSTS_LINKTEXT_DOWNLOAD_URL').'" disabled="disabled" />';
            $agree_form .= JHTML::_( 'form.token' )."</form>";
        } else {
            $html_sum = str_replace('{license_text}', '', $html_sum);
            $html_sum = str_replace('{license_title}', '', $html_sum);
            $html_sum = str_replace('{license_checkbox}', '', $html_sum);
        }            
         
        if ($open_in_blank_page || $extern_site){
            $targed = '_blank';
            if ($extern_site){
                $html_sum = str_replace('{external_download_info}', JText::_('COM_EPOSTS_FRONTEND_DOWNLOAD_GO_TO_OTHER_SITE_INFO'), $html_sum);
            } else {
                $html_sum = str_replace('{external_download_info}', '', $html_sum);
            }    
        } else {
            $targed = '_self';
            $html_sum = str_replace('{external_download_info}', '', $html_sum);
        }                    
    
        $link = '<div id="countdown" style="text-align:center"><a href="'.$download_link.'" target="'.$targed.'" title="'.JText::_('COM_EPOSTS_LINKTEXT_ZIP').'" ><img src="'.JURI::base().'images/eposts/downloadimages/'.$jlistConfig['download.pic.details'].'" border="0" alt="'.JText::_('COM_EPOSTS_LINKTEXT_ZIP').'" /></a></div>'; 
        if ($countdown){
             if ($must_confirm){
                 $countdown = str_replace('{link}', $agree_form, $countdown);
                 $html_sum = str_replace('{license_checkbox}', '<div id="countdown">'.$countdown.'</div>', $html_sum);
                 $html_sum = str_replace('{download_link}', '', $html_sum);
             } else {
                 $countdown = str_replace('{link}', $link, $countdown);
                 $html_sum = str_replace('{download_link}', '<div id="countdown">'.$countdown.'</div>', $html_sum); 
             }
        } else {    
             if ($must_confirm){
                 $html_sum = str_replace('{license_checkbox}', $agree_form, $html_sum);
                 $html_sum = str_replace('{download_link}', '', $html_sum);
             } else {   
                 $html_sum = str_replace('{download_link}', $link, $html_sum);
             }    
                
        }
    }
        
	$footer = makeFooter(true, false, false, 0, 0, 0, 0, false, true, false);  
    $html_sum .= $footer;
    echo $html_sum;
}

// view frontend upload form
function viewUpload($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
    $aid = max ($user->getAuthorisedViewLevels());
    $coreUserDepartments = $user->getAuthorisedDepartments();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_PAGE_TITLE'));
                            
    $can_upload = false;
    
    // get access right for upload form
    if (intval($jlistConfig['upload.access.department']) > 0){ 
        $user_is_in_departments = getUserDepartmentsX();
        $user_departments = explode(',', $user_is_in_departments);
        if (in_array($jlistConfig['upload.access.department'], $user_departments) || (in_array(8,$coreUserDepartments) || in_array(7,$coreUserDepartments))){
            $can_upload = true;
        } else {
            $msg = '<div class="jd_div_content"><br />'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ACCESS_ONLY_SPECIALS').'<br /><br /></div>';
        }    
    } else {
        // Joomla departments selected
        if ($aid < (int)($jlistConfig['upload.access']+1)){
            if ((int)$jlistConfig['upload.access']+1 == 3 ) {
                $msg = '<div class="jd_div_content"><br />'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ACCESS_ONLY_SPECIALS').'<br /><br /></div>';
            } else {
                $msg = '<div class="jd_div_content"><br />'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ACCESS_ONLY_REGGED').'<br /><br /></div>';
            }
        } else {
            $can_upload = true;
        }    
    }        
    
    $editor =& JFactory::getEditor();
    $editor2 =& JFactory::getEditor();
    $params = array( 'smilies'=> '0' ,
                 'style'  => '1' ,  
                 'layer'  => '0' , 
                 'table'  => '0' ,
                 'clear_entities'=>'0'
                 );
    
    // variablen vorbelegen
	$image1 = '<img src="'.JURI::base().'components/com_eposts/assets/images/';
	$image2 = '" width="18" height="18" border="0" alt="" align="top" />';
	$upload_stop_pic = '<img src="'.JURI::base().'components/com_eposts/assets/images/upload_stop.png" width="24" height="24" border="0" alt="" />';
	$upload_ok_pic = '<img src="'.JURI::base().'components/com_eposts/assets/images/upload_ok.png" width="24" height="24" border="0" alt="" />';
    $allowed_file_types_view = strtolower(str_replace(',', ', ', $jlistConfig['allowed.upload.file.types']));
    $allowed_file_types = strtolower($jlistConfig['allowed.upload.file.types']); 
	
	$max_file_size = $jlistConfig['allowed.upload.file.size'] * 1024 ;
	$name_pic 		 =	'form_no_value.png';
	$mail_pic 		 =	'form_no_value.png';
	$filetitle_pic 	 =	'form_no_value.png';
	$catlist_pic	 =	'form_no_value.png';
	$file_upload_pic =	'form_no_value.png';
    $extern_file_pic =  'form_no_value.png';
	$description_pic =	'form_no_value.png';
	
    $html_form = makeHeader($html_form, true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

	// Zugriffskontrolle
    if (!$jlistConfig['frontend.upload.active']){     
              $msg = '<div class="jd_div_content"><br />'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ACCESS_ONLY_SPECIALS').'<br /><br /></div>';
              $access = false;
              $html_form .= '{msg}';     
	} elseif (!$can_upload){
        $html_form .= '{msg}';
        $access = false;
    } else {
		$access= true;
		// nur einfügen wenn access = true
		$html_form .= '<div class="jd_div_content"><br />'.stripslashes($jlistConfig['upload.form.text']).'<br /></div>';
		// support for content plugins
		if ($jlistConfig['activate.general.plugin.support']  == 1){
			$html_form = JHTML::_('content.prepare', $html_form);
		}
	}
		// Inhalte holen, falls vorhanden	
	if ($user->get('id') > 0) {
       $name = $user->get('username');
	   $mail = $user->get('email');
       $submitted_by = $user->get('id');
	   $created_id = $user->get('id');
       $disabled = 'disabled="disabled"';
	   $name_pic =	'';
	   $mail_pic =	'';
    } else {
	   $name =	$database->getEscaped (JRequest::getString('name', '' ));
	   $mail =  $database->getEscaped (JRequest::getString('mail', '' ));
	   $disabled = '';	   	   
    }

	$author = 		$database->getEscaped (JRequest::getString('author', '' ));
	$author_url = 	$database->getEscaped (JRequest::getString('author_url', '' ));
    $author_url =   htmlspecialchars($author_url);
	$filetitle =	$database->getEscaped (JRequest::getString('filetitle', '' ));
	$version =		$database->getEscaped (JRequest::getString('version', '' ));
    $price =        $database->getEscaped (JRequest::getString('price', '' ));
	$catlist_sel =	$database->getEscaped (JRequest::getInt('catlist', 0 ));
    $license_sel =	$database->getEscaped (JRequest::getInt('license', 0 ));
	$system_sel  =	$database->getEscaped (JRequest::getInt('system', 0 ));
	$language_sel =	$database->getEscaped (JRequest::getInt('language', 0 ));
	$description =	$database->getEscaped (JRequest::getVar('description', '', 'post', 'string', JREQUEST_ALLOWHTML ));
    $description_long = $database->getEscaped (JRequest::getVar('description_long', '', 'post', 'string', JREQUEST_ALLOWHTML )); 	
    $extern_file =  $database->getEscaped (JRequest::getString('extern_file', '' ));    
    $file_upload  = JArrayHelper::getValue($_FILES,'file_upload',array('tmp_name'=>''));
    $pic_upload  =  JArrayHelper::getValue($_FILES,'pic_upload',array('tmp_name'=>''));
    $pic_upload2  = JArrayHelper::getValue($_FILES,'pic_upload2',array('tmp_name'=>''));
    $pic_upload3  = JArrayHelper::getValue($_FILES,'pic_upload3',array('tmp_name'=>''));		
	
    // is upload send?
    $sended = $database->getEscaped(JRequest::getInt('send', 0 ));
	if ($sended == 1) { 
        // JRequest::checkToken( 'request' ) or jexit( 'Invalid Token' ); 
		$no_valid = 0; 
		if ($name != '') {
			$name_pic =	'';
		} else {
			$name_pic =	'form_no_value.png';
			$no_valid++; }		

		// simple mail check		
		if ($mail != '' && eregi("^[a-z0-9\._-]+@+[a-z0-9\._-]+\.+[a-z]{2,4}$", $mail)){
    		$mail_pic =	'';
		} else {
			$mail_pic =	'form_no_value.png';
			$mail = '';
			$no_valid++; }		

		if ($filetitle != '') {
			$filetitle_pic = '';
		} else {
			$filetitle_pic = 'form_no_value.png';
			$no_valid++; }		

		if ($catlist_sel != '0' ) {
			$catlist_pic = '';
		} else {
			$catlist_pic = 'form_no_value.png';
			$no_valid++; }		

		if ($file_upload['tmp_name'] != '') {
			$file_upload_pic = '';
		} else {
			if ($extern_file == ''){
                $file_upload_pic = 'form_no_value.png';
			    $no_valid++;
            } else {
              // extern file url exist
              $file_upload_pic = ''; 
            }    
        }
        
        if ($extern_file != '') {
            $extern_file_pic = '';
        } else {
            if ($file_upload['tmp_name'] == ''){
                $extern_file_pic = 'form_no_value.png';
                $no_valid++;
            } else {
              // file_upload exist
              $extern_file_pic = '';  
            }    
        }     		

		if ($description != '') {
			$description_pic =	'';
		} else {
			if ($jlistConfig['fe.upload.view.desc.short'] == '1') {
                $description_pic = 'form_no_value.png';
			    $no_valid++; 
            }
        }    		
				
		// when all is ready - save the data
		if ($no_valid == 0) {
			$msg = '';
			// get upload category
			$database->SetQuery("SELECT cat_dir FROM #__eposts_cats WHERE cat_id = '$catlist_sel'");
			$mark_catdir = $database->loadResult();
			$description = trim($description);
            $description_long = trim($description_long);
            
            // build file alias
            $file_alias = $filetitle;
            $file_alias = JFilterOutput::stringURLSafe($file_alias);
            if (trim(str_replace('-','',$file_alias)) == '') {
               $datenow =& JFactory::getDate();
               $file_alias = $datenow->toFormat("%Y-%m-%d-%H-%M-%S");
            }
    		
			// check file extensions
	       if ($file_upload['tmp_name'] != '') { 
            $filetype = strtolower(substr(strrchr($file_upload['name'], '.'), 1));
           	$file_types = trim($allowed_file_types);
           	$file_types = str_replace(',', '|', $file_types);
           	if (!eregi( $file_types, $filetype ) || stristr($file_upload['name'], '.php.')){
           		$file_upload['tmp_name'] = '';
				$msg = '<div>'.$upload_stop_pic.'<font color="red"> '
				   		.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_FILETYPE').
				   		'</font><br />&nbsp;</div>';
				$html_form = str_replace('{form}', '{msg}{form}', $html_form);	
           	}

			// check filesize
           	if ($file_upload['size'] > $max_file_size) {
           		$file_upload['tmp_name'] = '';
				$msg = '<div>'.$upload_stop_pic.'<font color="red"> '
				   		.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_FILESIZE').
				   		'</font><br />&nbsp;</div>';
				$html_form = str_replace('{form}', '{msg}{form}', $html_form);	
           	}           	
           }
           
           // check file type when extern file
           if ($extern_file != '') { 
               $only_extern_link = false;
               $only_file_name = basename($extern_file);
               //$filetype = strtolower(substr(strrchr($only_file_name, '.'), 1));
               if ($filetype){
                    //$file_types = trim($jlistConfig['allowed.upload.file.types']);
                    //$file_types = str_replace(',', '|', $file_types);
                    if (stristr($only_file_name, '.php.')){
                        $extern_file = '';
                        $msg = '<div>'.$upload_stop_pic.'<font color="red"> '
                           .JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_FILETYPE').
                           '</font><br />&nbsp;</div>';
                        $html_form = str_replace('{form}', '{msg}{form}', $html_form);    
                        
                     }  
               } else {
                 $only_extern_link = true;
               }   
           }
            
            //pic upload bearbeiten
            $thumbnail = '';
            $thumbnail2 = '';
            $thumbnail3 = '';
            $upload_dir = '/images/eposts/screenshots/'; 
            $pic_types = 'gif|jpg|png';
            
            if($pic_upload['tmp_name']!=''){
              $pictype = strtolower(substr(strrchr($pic_upload['name'],"."),1)); 
              if (eregi( $pictype, $pic_types )) {
                 // replace special chars in filename
                $pic_filename = checkFileName($pic_upload['name']);
                $only_name = substr($pic_filename, 0, strrpos($pic_filename, '.'));
                $file_extension = strrchr($pic_filename,".");
                $num = 0;
                while (is_file(JPATH_SITE.$upload_dir.$pic_filename)){
                    $pic_filename = $only_name.$num++.$file_extension;
                    if ($num > 5000) break; 
                }
                $target_path =  JPATH_SITE.$upload_dir.$pic_filename;
                if(@move_uploaded_file($pic_upload['tmp_name'], $target_path)) {
                     // set chmod
                     @chmod($target_path, 0655);
                     // create thumb
                     create_new_thumb($target_path);
                     $thumbnail = basename($target_path);
                }      
              }             
            }

            //pic upload bearbeiten
            if($pic_upload2['tmp_name']!=''){
              $pictype = strtolower(substr(strrchr($pic_upload2['name'],"."),1)); 
              if (eregi( $pictype, $pic_types )) {
                 // replace special chars in filename
                $pic_filename = checkFileName($pic_upload2['name']);
                $only_name = substr($pic_filename, 0, strrpos($pic_filename, '.'));
                $file_extension = strrchr($pic_filename,".");
                $num = 0;
                while (is_file(JPATH_SITE.$upload_dir.$pic_filename)){
                    $pic_filename = $only_name.$num++.$file_extension;
                    if ($num > 5000) break; 
                }
                $target_path =  JPATH_SITE.$upload_dir.$pic_filename;
                if(@move_uploaded_file($pic_upload2['tmp_name'], $target_path)) {
                     // set chmod
                     @chmod($target_path, 0655);
                     // create thumb
                     create_new_thumb($target_path);
                     $thumbnail2 = basename($target_path);
                }      
              }             
            }
             
            //pic upload bearbeiten
            if($pic_upload3['tmp_name']!=''){
              $pictype = strtolower(substr(strrchr($pic_upload3['name'],"."),1)); 
              if (eregi( $pictype, $pic_types )) {
                 // replace special chars in filename
                $pic_filename = checkFileName($pic_upload3['name']);
                $only_name = substr($pic_filename, 0, strrpos($pic_filename, '.'));
                $file_extension = strrchr($pic_filename,".");
                $num = 0;
                while (is_file(JPATH_SITE.$upload_dir.$pic_filename)){
                    $pic_filename = $only_name.$num++.$file_extension;
                    if ($num > 5000) break; 
                }
                $target_path =  JPATH_SITE.$upload_dir.$pic_filename;
                if(@move_uploaded_file($pic_upload3['tmp_name'], $target_path)) {
                     // set chmod
                     @chmod($target_path, 0655);
                     // create thumb
                     create_new_thumb($target_path);
                     $thumbnail3 = basename($target_path);
                }      
              }             
            } 
               		
			//file upload 
			if($file_upload['tmp_name']!=''){
          		// replace special chars in filename
                $filename_new = checkFileName($file_upload['name']);
                $upload_dir = '/'.$jlistConfig['files.uploaddir'].'/'.$mark_catdir.'/';
                $only_name = substr($filename_new, 0, strrpos($filename_new, '.'));
                $file_extension = strtolower(strrchr($filename_new,"."));
                $num = 0;
                while (is_file(JPATH_SITE.$upload_dir.$filename_new)){
                    $filename_new = $only_name.'_'.$num++.$file_extension;
                    if ($num > 5000) break; 
                }
                $dir_and_filename = str_replace('/'.$jlistConfig['files.uploaddir'].'/', '', $upload_dir.$filename_new);
				$target_path = JPATH_SITE.$upload_dir.$filename_new;
                // upload only when the file not exist in the directory
              if (!is_file($target_path)){
		    	if(@move_uploaded_file($file_upload['tmp_name'], $target_path)) {
              		// get filesize
               		$size = fsize($target_path);
               		// get filedate
            		$date_added = JHTML::_('date', 'now', 'Y-m-d H:i:s' );
					$url_download = basename($target_path);
					// create thumbs from pdf
                    if ($jlistConfig['create.pdf.thumbs'] && $file_extension == '.pdf'){
                       $thumb_path = JPATH_SITE.'/images/eposts/screenshots/thumbnails/';
                       $screenshot_path = JPATH_SITE.'/images/eposts/screenshots/';
                       $pdf_tumb_name = create_new_pdf_thumb($target_path, $only_name, $thumb_path, $screenshot_path);
                       if ($pdf_tumb_name){
                           // add thumb file name to thumbnail data field
                           if ($thumbnail == ''){
                                $thumbnail = $pdf_tumb_name;
                           } elseif ($thumbnail2 == '') {
                                $thumbnail2 = $pdf_tumb_name;  
                           } else {
                                 $thumbnail3 = $pdf_tumb_name;  
                           }   
                       }    
                    } 
                    
                    // create auto thumb when extension is a pic
                    if ($jlistConfig['create.auto.thumbs.from.pics'] && ($file_extension == '.gif' || $file_extension == '.png' || $file_extension == '.jpg')){
                          $thumb_created = create_new_thumb($target_path);       
                          if ($thumb_created){
                              // add thumb file name to thumbnail data field
                               if ($thumbnail == ''){
                                    $thumbnail = $filename_new;
                               } elseif ($thumbnail2 == '') {
                                    $thumbnail2 = $filename_new;  
                               } else {
                                     $thumbnail3 = $filename_new;  
                               }
                          }
                          // create new big image for full view
                          $image_created = create_new_image($target_path);
                    }                       
                    
                    // auto publish ?
                    if ($jlistConfig['upload.auto.publish']){
                        $publish = 1;
                        setAUPPointsUploads($submitted_by, $filetitle);
                        $set_aup_points = 0;
                    } else {
                        $set_aup_points = 1;
                        $publish = 0;
                    }
                    $file_extension = strtolower(substr(strrchr($url_download,"."),1));
                    $filepfad = JPATH_SITE.'/images/eposts/fileimages/'.$file_extension.'.png';
                    if(file_exists(JPATH_SITE.'/images/eposts/fileimages/'.$file_extension.'.png')){
                    $filepic       = $file_extension.'.png';
                    } else {
                    $filepic       = $jlistConfig['file.pic.default.filename'];
                    }                    
                    $database->setQuery("INSERT INTO #__eposts_files (`file_id`, `file_title`, `file_alias`,`description`, `description_long`, `file_pic`, `thumbnail`, `thumbnail2`, `thumbnail3`, `price`, `release`, `language`, `system`, `license`, `url_license`, `size`, `date_added`, `file_date`, `url_download`, `url_home`, `author`, `url_author`, `created_by`, `created_id`, `created_mail`, `modified_by`, `modified_date`, `submitted_by`, `set_aup_points`, `downloads`, `cat_id`, `ordering`, `published`, `checked_out`, `checked_out_time`)
                                                                     VALUES (NULL, '$filetitle', '$file_alias', '$description', '$description_long', '$filepic', '$thumbnail', '$thumbnail2', '$thumbnail3', '$price', '$version', '$language_sel', '$system_sel', '$license_sel', '', '$size', '$date_added', '', '$url_download', '$author_url', '$author', '', '$name', '$created_id', '$mail', '', '0000-00-00 00:00:00', '$submitted_by', '$set_aup_points', '0', '$catlist_sel', '0', '$publish', '0', '0000-00-00 00:00:00')");
		   	   		if (!$database->query()) {
						// DB error
						echo $database->stderr();
						exit;
					}					 
                    // alles OK!
                    if (!$msg) {
                        $msg = '<div>'.$upload_ok_pic.'<font color="green"> '
                               .JText::_('COM_EPOSTS_FRONTEND_UPLOAD_OK').
                               '</font><br />&nbsp;</div>';
                               $html_form = str_replace('{form}', '{msg}{form}', $html_form);
                               // send email wenn aktiviert
                               if ($jlistConfig['send.mailto.option.upload']){
                                   sendMailUploads($name, $mail, $url_download, $filetitle, $description);   
                               }    
                    }
                 } else {
					// error when file is moved
					$msg = '<div>'.$upload_stop_pic.'<font color="red"> '
				   		.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_MOVE_FILE').
				   		'</font><br />&nbsp;</div>';
					$html_form = str_replace('{form}', '{msg}{form}', $html_form);	
				 }
              } else {
                  // file exist with the same name
                    $msg = '<div>'.$upload_stop_pic.'<font color="red"> '
                           .JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_FILE_EXISTS').
                           '</font><br />&nbsp;</div>';
                    $html_form = str_replace('{form}', '{msg}{form}', $html_form);    
              } 
		   	}
            
            // save the data with URL to extern file
            if($extern_file !=''){
                if (!$only_extern_link){
                    // get filesize
                    $size = urlfilesize($extern_file);
                    $a = array("B", "KB", "MB", "GB", "TB", "PB");
                    $pos = 0;
                    while ($size >= 1024) {
                        $size /= 1024;
                        $pos++;
                    }
                    $size = round($size,2)." ".$a[$pos];
                    
                    $file_extension = strtolower(substr(strrchr($extern_file,"."),1));
                    $filepfad = JPATH_SITE.'/images/eposts/fileimages/'.$file_extension.'.png';
                    if(file_exists(JPATH_SITE.'/images/eposts/fileimages/'.$file_extension.'.png')){
                        $filepic       = $file_extension.'.png';
                    } else {
                        $filepic       = $jlistConfig['file.pic.default.filename'];
                    }
                    $linked_to_extern_site = 0;
                } else {
                        $filepic = $jlistConfig['file.pic.default.filename'];
                        $linked_to_extern_site = 1;
                }    
                // get filedate
                $date_added = JHTML::_('date', 'now', 'Y-m-d H:i:s' );
                // auto publish ?
                if ($jlistConfig['upload.auto.publish']){
                    $publish = 1;
                    setAUPPointsUploads($submitted_by, $filetitle);
                } else {
                    $publish = 0;
                }
                        
                $database->setQuery("INSERT INTO #__eposts_files (`file_id`, `file_title`, `file_alias`, `description`, `description_long`, `file_pic`, `thumbnail`, `price`, `release`, `language`, `system`, `license`, `url_license`, `size`, `date_added`, `file_date`, `url_download`, `extern_file`, `extern_site`, `url_home`, `author`, `url_author`, `created_by`, `created_mail`, `modified_by`, `modified_date`, `submitted_by`, `downloads`, `cat_id`, `ordering`, `published`, `checked_out`, `checked_out_time`) VALUES 
                                                                      (NULL, '$filetitle', '$file_alias', '$description', '$description_long', '$filepic', '$thumbnail', '$price', '$version', '$language_sel', '$system_sel', '$license_sel', '', '$size', '$date_added', '', '', '$extern_file', '$linked_to_extern_site', '$author_url', '$author', '', '$name', '$mail', '', '0000-00-00 00:00:00', '$submitted_by', '0', '$catlist_sel', '0', '$publish', '0', '0000-00-00 00:00:00')");
                if (!$database->query()) {
                    echo $database->stderr();
                    exit;
                }                     
                // all is OK!
                    if (!$msg) {
                        $msg = '<div>'.$upload_ok_pic.'<font color="green"> '
                               .JText::_('COM_EPOSTS_FRONTEND_UPLOAD_URL_OK').
                               '</font><br />&nbsp;</div>';
                               $html_form = str_replace('{form}', '{msg}{form}', $html_form);
                               // send email when activated
                               if ($jlistConfig['send.mailto.option.upload']){
                                   sendMailUploads($name, $mail, $extern_file, $filetitle, $description);   
                               }    
                    }
          }             

		// file saved upload end ------------------------------------------------------		
			
		} else {
			// add error msg	
			$msg = '<div>'.$upload_stop_pic.'<font color="red"> '.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE').'</font><br />&nbsp;</div>';
			$html_form = str_replace('{form}', '{msg}{form}', $html_form);
		}
	}

	// only view form when user has access
	if ($access) {
        
	// view form 
    // vars for javascript form validation
    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    if ($name_pic){
        $name_pic = $image1.$name_pic.$image2;
    } else {
        $name_pic = '';
    }     

    if ($mail_pic){
        $mail_pic = $image1.$mail_pic.$image2;
    } else {
        $mail_pic = '';
    }     
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
    $form = '<form name="uploadForm" id="uploadForm" action="'.$uri.'" onsubmit="return CheckForm(\''.$error_msg.'\', \''.$allowed_file_types.'\', \''.$error_msg_ext.'\');" method="post" enctype="multipart/form-data">
	<table class="jd_upload_form" border="0" cellpadding="0" cellspacing="5" width="99%">
        <tr>
            <td width="140" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_NAME').'
            </td><td width="20" valign="top">'.$name_pic.'</td>
            <td width="267" valign="middle">
                <input type="text" name="name" id="name" maxlength="255" size="40" '.$disabled.' value="'.$name.'"/>
            </td>
        </tr>
        <tr>
            <td width="140" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_MAIL').'
            </td><td width="20" valign="top">'.$mail_pic.'</td>
            <td width="267">
                <input type="text" name="mail" maxlength="255" size="40" '.$disabled.' value="'.$mail.'"/>
            </td>
            </tr>';
	  if ($jlistConfig['fe.upload.view.author'] == '1') {	 
         $form .= '<tr>
            <td width="140" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_AUTHOR').'
            </td><td width="20" valign="top">&nbsp;</td>
            <td width="267">
                <input type="text" name="author" id="author" maxlength="255" size="40" value="'.$author.'"/>
            </td>
            </tr>';
      } 
      if ($jlistConfig['fe.upload.view.author.url'] == '1') {     
            $form .= '<tr>
            <td width="140" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_AUTHOR_URL').'
            </td><td width="20" valign="top">&nbsp;</td>
            <td width="267">
                <input type="text" name="author_url" maxlength="255" size="40"  value="'.$author_url.'"/>
            </td>
            </tr>';
      }
      
      $form .= '<tr>
            <td width="140" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_TITLE_FILE').'
            </td><td width="20" valign="top">'.$image1.$filetitle_pic.$image2.'</td>
            <td width="267">
                <input type="text" name="filetitle" maxlength="255" size="40" value="'.stripslashes($filetitle).'"/>
            </td>
            </tr>';
	  if ($jlistConfig['fe.upload.view.release'] == '1') {     
            $form .= '<tr>
            <td width="140" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_VERSION').'
            </td><td width="20" valign="top">&nbsp;</td>
            <td width="267">
                <input type="text" name="version" maxlength="255" size="40" value="'.$version.'"/>
            </td>
            </tr>';
      }  
      if ($jlistConfig['fe.upload.view.price'] == '1') {     
            $form .= '<tr>
            <td width="140" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_PRICE').'
            </td><td width="20" valign="top">&nbsp;</td>  
            <td width="267">
                <input type="text" name="price" maxlength="20" size="40" value="'.$price.'"/>
            </td>
            </tr>';		
	  }	
		$form .= '<tr>
            <td width="140" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_CATEGORY').'
            </td><td width="20" valign="top">'.$image1.$catlist_pic.$image2.'</td>
            <td width="200">';
            
       	// build cat tree listbox
        $access = checkAccess_JD();

        // get cat departments access
        if ($user->id > 0){
            $user_is_in_departments = getUserDepartmentsX();
        } else {
            $user_is_in_departments = 0;
        }
        $user_departments = '';
        if ($user_is_in_departments) $user_departments = "OR cat_department_access IN ($user_is_in_departments)";
        
        $src_list = array();
        // reihenfolge wie in optionen gesetzt
        $cat_sort_field = 'ordering';
        $cat_sort = '';
        if ($jlistConfig['cats.order'] == 1) {
            $cat_sort_field = 'cat_title';
        }
        if ($jlistConfig['cats.order'] == 2) {
            $cat_sort_field = 'cat_title';
            $cat_sort = 'DESC';
        }   

		$query = "SELECT cat_id AS id, parent_id AS parent, cat_title AS title FROM #__eposts_cats WHERE published = 1 AND (cat_access <= '$access' $user_departments) ORDER BY $cat_sort_field $cat_sort";
		$database->setQuery( $query );
		$src_list = $database->loadObjectList();
		$preload = array();
		$preload[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_LISTBOXES') );
		$selected = array();
		$selected[] = JHTML::_('select.option', '0' );
    	//1.5 Native treeselect List ersetzen durch makeoption und selectlist
        $cat_listbox = treeSelectList( $src_list, 0, $preload, 'catlist','class="inputbox" size="1"', 'value', 'text', $catlist_sel );
 		$form .= $cat_listbox;
        $form .= '</td>
        </tr>';      
		
        if ($jlistConfig['fe.upload.view.license'] == '1') {     
         $form .= '<tr>
            <td width="140" valign="middle">
                '.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_LIZENZ').'
            </td><td width="20" valign="top">&nbsp;</td>
            <td width="267">';
		   // build listbox with licenses
    	   $licenses = array();
    	   $licenses[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_LISTBOXES') );
    	   $database->setQuery( "SELECT id AS value, license_title AS text FROM #__eposts_license" );
    	   $licenses = array_merge( $licenses, $database->loadObjectList() );
    	   $lic_listbox = JHTML::_('select.genericlist', $licenses, 'license', 'size="1" class="inputbox"', 'value', 'text', $license_sel );
   		   $form .= $lic_listbox.'
            </td>
            </tr>';
        }        
		
        if ($jlistConfig['fe.upload.view.language'] == '1') {     
            $form .= '<tr>
            <td width="140" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_LANGUAGE').'
            </td><td width="20" valign="top">&nbsp;</td>
            <td width="200">';
	        // build language listbox
    	    $file_language = array();
    	    $file_lang_values = explode(',' , $jlistConfig['language.list']);
    	    for ($i=0; $i < count($file_lang_values); $i++) {
        	    $file_language[] = JHTML::_('select.option', $i, $file_lang_values[$i] );
    	    }
    	    $listbox_language = JHTML::_('select.genericlist', $file_language, 'language', 'class="inputbox" size="1"', 'value', 'text', $language_sel );                 
		    $form .= $listbox_language.'
            </td>
            </tr>';
        }      
		
        if ($jlistConfig['fe.upload.view.system'] == '1') {     
            $form .= '<tr>
                <td width="140" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_SYSTEM').'
                </td><td width="20" valign="top">&nbsp;</td>
                <td width="200">';
			
		    // build system listbox
    	    $file_system = array();
    	    $file_sys_values = explode(',' , $jlistConfig['system.list']);
    	    for ($i=0; $i < count($file_sys_values); $i++) {
        	    $file_system[] = JHTML::_('select.option', $i, $file_sys_values[$i] );
    	    }
    	    $listbox_system = JHTML::_('select.genericlist', $file_system, 'system', 'class="inputbox" size="1"', 'value', 'text', $system_sel );				
            $form .= $listbox_system.'</td></tr>';              
        }
        
        if ($jlistConfig['fe.upload.view.select.file'] == '1') {
            if ($jlistConfig['fe.upload.view.extern.file'] == '1') {
                $checkUpload = 'checkUploadFieldFile(file_upload)';
            } else {
                $checkUpload = '';
            }     
            $form .= '<tr>
            <td width="140" valign="top">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_FILENAME').'
            </td><td width="20" valign="top">'.$image1.$file_upload_pic.$image2.'</td>
            <td width="200">
                <input name="file_upload" id="file_upload" size="30" type="file" onchange="'.$checkUpload.'" value="'.$file_upload.'"/><br />'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ALLOWED_FILETYPE').': '.$allowed_file_types_view.'<br />'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ALLOWED_MAX_SIZE').': '.$jlistConfig['allowed.upload.file.size'].' KB
            </td>
            </tr>';
        }
        if ($jlistConfig['fe.upload.view.extern.file'] == '1') {
            if ($jlistConfig['fe.upload.view.select.file'] == '1') {
               $checkUpload2 = 'checkUploadFieldExtern(extern_file)'; 
               $form .= '<tr><td width="140" valign="top"><b>'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_EXTERN_FILE_OR').'</b></td></tr>'; 
            } else {
               $checkUpload2 = '';
            }    
            $form .= '<tr>
            <td width="140" valign="top">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_EXTERN_FILE_TITEL').'
            </td><td width="20" valign="top">'.$image1.$extern_file_pic.$image2.'</td>
            <td width="200">
                <input type="text" name="extern_file" id="extern_file" maxlength="255" size="45" onchange="'.$checkUpload2.'" value="'.$extern_file.'"/>
            </td>
            </tr>';
        }

        if ($jlistConfig['fe.upload.view.pic.upload'] == '1') { 
            $form .= '<tr>
            <td width="140" valign="top">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_PIC_FILETITLE').'
            </td><td width="20" valign="top">&nbsp;</td>
            <td width="200">
                <input name="pic_upload" size="30" type="file" value="'.$pic_upload.'"/><br />'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_PIC_ALLOWED_FILES').' gif, jpg, png
            </td>
            </tr>';
            $form .= '<tr>
            <td width="140" valign="top">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_PIC_FILETITLE2').'
            </td><td width="20" valign="top">&nbsp;</td>
            <td width="200">
                <input name="pic_upload2" size="30" type="file" value="'.$pic_upload2.'"/><br />'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_PIC_ALLOWED_FILES').' gif, jpg, png
            </td>
            </tr>';
            $form .= '<tr>
            <td width="140" valign="top">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_PIC_FILETITLE2').'
            </td><td width="20" valign="top">&nbsp;</td>
            <td width="200">
                <input name="pic_upload3" size="30" type="file" value="'.$pic_upload3.'"/><br />'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_PIC_ALLOWED_FILES').' gif, jpg, png
            </td>
            </tr>';
		}  
        if ($jlistConfig['fe.upload.view.desc.short'] == '1') {
            $form .= '<tr>
            <td width="140" valign="top">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_DESC_FILE').'
            </td><td width="20" valign="top">'.$image1.$description_pic.$image2.'</td></tr> 
            <tr><td colspan="3"> 
                '.$editor->display( 'description', '', '500', '300', '60', '10', false, '' ).'
            </td>
            </tr>';
        }
        if ($jlistConfig['fe.upload.view.desc.long'] == '1') {     
            $form .= '<tr>
            <td width="140" valign="top">'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_TITEL_DESC_FILE_LONG').'
            </td><td width="20" valign="top">&nbsp;</td></tr>
            <tr><td colspan="3">
                '.$editor2->display( 'description_long', '', '500', '400', '60', '20', false, '' ).'
               
            </td>
            </tr>';
        }
        $form .= '<tr>
            <td width="140" valign="middle">&nbsp;
            </td><td width="20" valign="top">&nbsp;</td>
            <td width="267">
                <input class="button" type="submit" name="senden" value="'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_FILENAME_BUTTON_TEXT_SEND').'"/> <input class="button" type="reset" name="cancel" 
                       value="'.JText::_('COM_EPOSTS_FRONTEND_UPLOAD_FILENAME_BUTTON_TEXT_CLEAR').'"/>
            </td>
            </tr>
            </table>	
	<input type="hidden" name="option" value="'.$option.'" />
	<input type="hidden" name="view" value="'.$view.'" />
	<input type="hidden" name="send" value="1" />
	<input type="hidden" name="MAX_FILE_SIZE" value="'.$max_file_size.'"/>'.JHTML::_( 'form.token' ).'</form>';	

	
	} else {
		$form = '';  
	}	// end access if()...
	
	$html_form = str_replace('{form}', $form, $html_form);
	
	if (isset($msg)) {
		$html_form = str_replace('{msg}', $msg, $html_form);	
	}
	//echo $html_form; 
	//echo $footer;
}

function showSearchForm($option){
     global $Itemid, $jlistConfig, $mainframe, $page_title;
     $user = &JFactory::getUser();
     $aid = max ($user->getAuthorisedViewLevels());
     $document=& JFactory::getDocument();
     $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_FRONTEND_SEARCH_LINKTEXT'));     
     
    $html_form = makeHeader($html_form, true, false, false, 0, false, true, false, false, false, 0, 0, 0, 0, 0, 0, '', '');
    //echo $html_form;
    $html_form = '';
    
    $html_form = '<form name="jdsearch" action="'.JRoute::_('index.php?option=com_eposts&Itemid='.$Itemid.'&view=searchresult').'" onsubmit="return CheckSearch(\''.JText::_('COM_EPOSTS_FRONTEND_SEARCH_RESULT_TEXT_TO_SHORT').'\',\''.JText::_('COM_EPOSTS_FRONTEND_SEARCH_RESULT_NO_OPTION').'\');" method="post">';
    $html_form .= '<table class="jd_div_content" border="0" cellpadding="0" cellspacing="5" width="99%">
        <tr><td><br /></td></tr>
        <tr>
            <td colspan="2" width="100" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_SEARCH_DESCRIPTION').'
            </td>
        </tr>
        <tr>
            <td width="100" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_SEARCH_TEXT_TITLE').'
            </td>
            <td width="200" valign="middle">
                <input class="jd_inputbox" type="text" name="jdsearchtext" id="jdsearchtext" maxlength="80" size="30"  value=""/> <input class="button" type="submit" name="searchsubmit" value="'.JText::_('COM_EPOSTS_FRONTEND_SEARCH_BUTTON_TEXT').'"/> 
            </td>
        </tr>
        <tr>
            <td width="100" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_SEARCH_IN_TITLE').'
            </td>
            <td width="200">
                <input class="jd_inputbox" type="checkbox" name="jdsearchintitle" id="jdsearchintitle" value="1" checked="checked"/> 
            </td>
        </tr>
        <tr>
           <td width="100" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_SEARCH_IN_DESC').'
            </td>
            <td width="200">
                <input class="jd_inputbox" type="checkbox" name="jdsearchindesc" id="jdsearchindesc" value="1" checked="checked"/> 
            </td>
        </tr>
        <tr>
           <td width="100" valign="middle">'.JText::_('COM_EPOSTS_FRONTEND_SEARCH_NUMBERS').'
            </td>
            <td width="200">
                <input class="jd_inputbox" type="text" name="jdsearchnumber" id="jdsearchnumber" maxlength="3" size="3" value="30"/>
            </td>
        </tr>
        <tr><td><br /><br /></td></tr>
        </table>'.JHTML::_( 'form.token' ).'</form>';
        
    $html_form .= makeFooter(true, false, false, 0, 0, 0, 0, false, false, false); 
    
    if ( !$jlistConfig['offline'] ) {
            echo $html_cat;
    } else {
            if ($aid == 3) {
                echo JText::_('COM_EPOSTS_BACKEND_OFFLINE_ADMIN_MESSAGE_TEXT');
                echo $html_cat;
            } else {
                $html_off = '<br /><br />'.stripslashes($jlistConfig['offline.text']).'<br /><br />';
                echo $html_off;
            }
    }
    
    //echo $html_form;
    ?>
    <script type="text/Javascript" language="JavaScript">
    <!--
        document.getElementById("jdsearchtext").focus();
    -->
    </script>
    <?php
}    

function showSearchResult($option){
    global $Itemid, $jlistConfig, $mainframe, $page_title;
    $database = &JFactory::getDBO();
    $user = &JFactory::getUser();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_FRONTEND_SEARCH_RESULT_TITLE') ); 
    JRequest::checkToken( 'request' ) or jexit( 'Invalid Token' ); 
   
    $html_form = makeHeader($html_form, true, false, false, 0, false, true, false, false, false, 0, 0, 0, 0, 0, 0, '', '');

    $searchtext =    $database->getEscaped(JRequest::getString('jdsearchtext', ''));
    $searchintitle = $database->getEscaped(JRequest::getString('jdsearchintitle', ''));
    $searchintext =  $database->getEscaped(JRequest::getString('jdsearchindesc', ''));
    $searchnumber =  $database->getEscaped(JRequest::getInt('jdsearchnumber', 30));
    if (!$searchnumber) $searchnumber = 30;

    $search_array = explode(' ', $searchtext);
    $files2 = array();
    
    foreach ($search_array as $word) {
        if ($searchintitle && $searchintext){
            $database->setQuery("SELECT * FROM #__eposts_files WHERE file_title LIKE '%$word%' OR description LIKE '%$word%' OR description_long LIKE '%$word%' AND published = 1 ORDER BY date_added LIMIT $searchnumber");
        }
        if ($searchintitle && !$searchintext){
            $database->setQuery("SELECT * FROM #__eposts_files WHERE file_title LIKE '%$word%' AND published = 1 ORDER BY date_added LIMIT $searchnumber");
        }
        if (!$searchintitle && $searchintext){
            $database->setQuery("SELECT * FROM #__eposts_files WHERE description LIKE '%$word%' OR description_long LIKE '%$word%' AND published = 1 ORDER BY date_added LIMIT $searchnumber");
        }
        $files = $database->loadObjectList();

        foreach ($files as $file) {
            if (!array_key_exists($file->file_id, $files2)) {
                $files2[$file->file_id] = array('file'=>$file, 'ctr'=> 0);
            }
            $files2[$file->file_id]['ctr']++;
        }

    }
    
    usort($files2, '_ctrSort');

  if ($files2) {
    // files gefunden   
    // cat der files holen und auf access beschränken
    $access = checkAccess_JD();        
      
    // get departments access
    if ($user->id > 0){
        // make sure that admins can upload in all categories
        $user_is_in_departments = getUserDepartmentsX();
    } else {
        $user_is_in_departments = 0;
    } 
    
    $where = '';
    $user_departments = '';
    
    if ($user_is_in_departments) $user_departments = "OR cat_department_access IN ($user_is_in_departments)"; 
               
    $output = array();
    foreach($files2 as $file2) {
          $file = $file2['file'];
          if ($file->published) {  
              $database->setQuery("SELECT * FROM #__eposts_cats WHERE cat_id = '$file->cat_id' AND  published = 1 AND (cat_access <= '$access' $user_departments)");
              $cat = $database->loadObject();
              if ($cat){
                  $output[] = $file;
              }
          }      
    } 
    
    if ($output) {
            $files_found = true;
            // result header
            $html_form .= '<table class="jd_search_form" border="0" cellpadding="0" cellspacing="5" width="99%">
                  <tr>
                  <td class="jd_search_result_title" width="100%"><b>'.JText::_('COM_EPOSTS_FRONTEND_SEARCH_RESULT_TITLE').'</b><br />'
                  .JText::_('COM_EPOSTS_FRONTEND_SEARCH_RESULT_SEARCH_TEXT').': <b>'.$searchtext.'</b><br />'
                  .JText::_('COM_EPOSTS_FRONTEND_SEARCH_RESULT_SUM_FILES').': <b>'.count($output).'</b></td>
                  </tr>
                  <tr>
                  </tr>';
                            
            foreach ($output as $out){
                
                // suchtext farblich hervorheben
                foreach ($search_array as $word) {
                    $regexp = "/($word)(?![^<]+>)/i";
                    if ($searchintitle && $searchintext){
                        $out->description = preg_replace($regexp, '<font color="#CC3300">'.$word.'</font>', $out->description);
                        $out->file_title = preg_replace($regexp, '<font color="#CC3300">'.$word.'</font>', $out->file_title);
                    }
                    if ($searchintitle && !$searchintext){
                        $out->file_title = preg_replace($regexp, '<font color="#CC3300">'.$word.'</font>', $out->file_title);
                    }
                    if (!$searchintitle && $searchintext){
                        $out->description = preg_replace($regexp, '<font color="#CC3300">'.$word.'</font>', $out->description);
                    }
                }
                $titel_link = JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=viewdownload&catid='.$out->cat_id.'&cid='.$out->file_id);
                $titel_link_text = '<a href="'.$titel_link.'">'.$out->file_title.'</a>';
                $detail_link_text = '<a href="'.$titel_link.'">'.JText::_('COM_EPOSTS_FE_DETAILS_LINK_TEXT_TO_DETAILS').'</a>';
                
                $html_form .= '<tr width="100%"><td class="jd_search_results"><b>'.$titel_link_text.' '.$out->release.'</b><br />'.substr($out->description, 0, 400).'...<br />'.$detail_link_text.'</td></tr>';    
            }    
            $html_form .= '</table>'; 
        } else {
            $files_found = false;  
        }    
    } else {
      $files_found = false;  
    }
    if (!$files_found) {
        // keine files gefunden - oder falsche berechtigung
        // result header
        $html_form .= '<table class="jd_search_form" border="0" cellpadding="0" cellspacing="5" width="99%">
                  <tr>
                  <td class="jd_search_result_title" width="100%"><b>'.JText::_('COM_EPOSTS_FRONTEND_SEARCH_RESULT_TITLE').'</b><br />'
                  .JText::_('COM_EPOSTS_FRONTEND_SEARCH_RESULT_SEARCH_TEXT').': <b>'.$searchtext.'</b><br />'
                  .JText::_('COM_EPOSTS_FRONTEND_SEARCH_RESULT_NO_SUM_FILES').'</td>
                  </tr>
                  <tr>
                  </tr></table>';
    }    
    $html_form .= makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);
    echo $html_form;
}

//listResources
function listResources($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	
	define( '_JEXEC', 1 );

define( '_VALID_MOS', 1 );

define( 'JPATH_BASE', realpath(dirname(__FILE__)));

define( 'DS', DIRECTORY_SEPARATOR );


//require_once ( JPATH_BASE .DS.'resir.php' ); //moved down by chheena
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$resources_offices = JRequest::getString('resources_offices');

	if ($resources_offices=="Search by Name"){
	$resources_offices ="";
	}

	if ($resources_offices=="البحث بالإسم"){
	$resources_offices ="";
	}
$send = JRequest::getString('submit_go');

$eid = (int)JRequest::getString('eid', 0);
$resources_title =(int)JRequest::getString('resources_title', 0);
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_LIST_RESOURCES_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_LIST_RESOURCES_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	//echo $uri;
	$exp=(explode("&drp=",$uri ));
	$database->setQuery("SELECT * FROM #__emirate ORDER BY emirate_name ASC" );
 

$listDepartments = $database->loadObjectList(); 

$jAp = JFactory::getApplication();
$q = $jAp->input->get('drp');
//echo $q;
$val= JRequest::getVar('drp');
	     
$database->setQuery("SELECT  id,offices_title,offices_title_ar "
 . "\n FROM #__eposts_offices where emirate_id= $val ORDER BY offices_title ASC  "
				
            );

			$listDepartments2 = $database->loadObjectList();
			

	//JHTML::_("behavior.mootools"); //commented by chheena temporary //moved down by chheena
	//$document->addStyleSheet(JURI::base() . 'components/com_igallery/cerabox.css');//commented by chheena temporary //moved down by chheena
	//$document->addScript(JURI::base() . 'components/com_igallery/cerabox.min.js');//commented by chheena temporary //moved down by chheena
	
	 
	?>
   

<script type="text/Javascript" language="JavaScript">
   function setOptions(chosen){
  window.location.href ='<?php echo $exp[0]; ?>&drp='+chosen;
  return;
}

    </script>

<form name="uploadForm" id="uploadForm" action="<?php echo $uri; ?>" method="post" >
	<div class="resourceoffice_mainfilter"><table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="resources_offices" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<select name="eid" onChange="setOptions(document.uploadForm.eid.options[document.uploadForm.eid.selectedIndex].value)" class="inputbox" >
<option value="" class="form_drop_down" ><?php echo JText::_('COM_EPOSTS_SELECT_EMIRATE')?> </option>
			<?php foreach($listDepartments as $xdepartment) {

			?><?php if ($tag == 'ar-AA') {
		$emirate_name = $xdepartment->emirate_name_ar;
		}else{
	$emirate_name = $xdepartment->emirate_name;
	}?>
			  
	<option value="<?php echo $xdepartment->eid?>" <?php echo ($val === $xdepartment->eid ? 'selected' : '')?>><?php echo $emirate_name; ?></option>
	</option>
			<?php	} ?>
			</select></td>
			
				<td>
			<select name="resources_title" class="inputbox" >
				<?php
				foreach($listDepartments2 as $xdepartment2) { ?>
		<?php if ($tag == 'ar-AA') {
		$ofctitle = $xdepartment2->offices_title_ar;
		}else{
	$ofctitle = $xdepartment2->offices_title;
	}?>
	<option value="<?php echo $xdepartment2->id; ?>"><?php echo $ofctitle;  ?></option>
			<?php	} ?>
			</select></td>
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>	
	<input type="hidden" name="option" value="'.$option.'" />
	<input type="hidden" name="view" value="'.$view.'" />
	<input type="hidden" name="send" value="1" />
	<input type="hidden" name="MAX_FILE_SIZE" value="'.$max_file_size.'"/>'.JHTML::_( 'form.token' ).'</form>';	

	echo $form; 
	
	if($send){
	//require_once ( JPATH_BASE .DS.'resir.php' ); //moved down by chheena
	JHTML::_("behavior.mootools"); //commented by chheena temporary //moved down by chheena
	$document->addStyleSheet(JURI::base() . 'components/com_igallery/cerabox.css');//commented by chheena temporary //moved down by chheena
	$document->addScript(JURI::base() . 'components/com_igallery/cerabox.min.js');//commented by chheena temporary //moved down by chheena
	
	if($resources_offices){
	
		$database->setQuery( "SELECT R.*, O.offices_title, O.offices_title_ar FROM #__eposts_resources R LEFT JOIN #__eposts_offices O ON  R.resources_offices = O.id WHERE O.offices_title like '%".$resources_offices."%' or O.offices_title_ar like '%".$resources_offices."%'  ORDER BY resources_title ASC");
//echo ( "SELECT R.*, O.offices_title, O.offices_title_ar FROM #__eposts_resources R LEFT JOIN #__eposts_offices O ON  R.resources_offices = O.id WHERE O.offices_title like '%".$resources_offices."%' or O.offices_title_ar like '%".$resources_offices."%'  ORDER BY resources_title ASC");
  
   }
elseif(($eid) && ($resources_title)) {
	$database->setQuery( "SELECT R.*, O.offices_title, O.offices_title_ar FROM #__eposts_resources R LEFT JOIN #__eposts_offices O ON  R.resources_offices = O.id WHERE R.resources_offices ='".$resources_title."'ORDER BY resources_title ASC");
 //  echo  ( "SELECT R.*, O.offices_title, O.offices_title_ar FROM #__eposts_resources R LEFT JOIN #__eposts_offices O ON  R.resources_offices = O.id WHERE R.resources_offices ='".$resources_title."'ORDER BY resources_title ASC");
   }//end resources


 $listOfResources = $database->loadObjectList();
//	print_r($listOfResources);
	if(count($listOfResources)){
	
$ofctitle = $listOfResources[0]->offices_title;
	if ($tag == 'ar-AA') :
		$ofctitle = $listOfResources[0]->offices_title_ar;
	endif;
	
	print '<div class="ofctitle">'.$ofctitle . '</div><br>';
	echo "<div id='resourcesoffices'>";
	$a=0;
	foreach($listOfResources as $rowResource){
		$title = $rowResource->resources_title;
		$descp = $rowResource->resources_description;
		if ($tag == 'ar-AA') :
		      $title = $rowResource->resources_title_ar;
			  $descp = $rowResource->resources_description_ar;
		   endif;
		   $title = strip_tags($title);
$config =&JFactory::getConfig();
 $filepath = $config->getValue( 'config.file_path' );
 $reverse_file_path = $config->getValue( 'config.reverse_file_path' );
 $reverse_filepath_storage = $config->getValue( 'config.reverse_filepath_storage'); 
if($rowResource->resources_logo){
 //$img = $reverse_filepath_storage.$rowResource->resources_logo;
	
			//   $img = str_replace("\\", "/", $img);
			// echo $img;
			//$filetype=string;       
	  //   $imgbinary = fread(fopen($img, "r"), filesize($img));
     // echo $imgbinary;
	 //  $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
	   
	   
	   $pieces = explode("\\",$rowResource->resources_logo);
//print_r ($pieces);

$img = $reverse_filepath_storage.'resourcegallery_img\\'.$pieces[1]; 
			//echo $img;
			//echo $filepath.'\news-thums'; 
			$img = str_replace("\\", "/", $img);	
			
$img2 = $reverse_filepath_storage.'\\'.$rowResource->resources_logo;
			  
			  $img2 = str_replace("\\", "/", $img2);
	//		 echo $img;
			//$filetype=string;       
	     //$imgbinary = fread(fopen($img, "r"), filesize($img));
			//$filetype=string;       
	     //$imgbinary2 = fread(fopen($img2, "r"), filesize($img2));
     // echo $imgbinary;
	   //$aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
       //$aaa2= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary2);
	   
	   $aaa= $img;
       $aaa2= $img2;
	}

	
else {
$aaa= 'components/com_eposts/assets/images/icons/meeting-room.png';
$aaa2= $aaa;
}


	$document->addScriptDeclaration("
		document.addEvent('domready', function() {
			$$('.thumbs_div a').cerabox({
				group: false,
				displayTitle: false

			});
		});
	");
$chk='data:image/string;base64,';

if($aaa==$chk){

$aaa=$aaa2;
}	
?>	
		<div class='resoffice'><div class='resofficeareapic'><div class="thumbs_div">
					
                		<a href="#$img_my<?php echo $a; ?>">
					<?php  $document->addScriptDeclaration(' var img_my'.$a.' = "<img src=\"'.$aaa2.'\" />";'); ?>
                        <img  src="<?php echo $aaa; ?>" width="140" height="100px" /> 
                    </a></div></div>
		<?php 
		$form = "<div class='resofficearea'><div class='resofficetitle'><a href='".JRoute::_("index.php?option=com_eposts&view=calendar&resourceid=".$rowResource->id."&resources_offices=".$rowResource->resources_offices)."'>".$title."</a></div>";		   
		
		$form .= "<div class='resofficedesc'><span class='resofficedescspan'>".JText::_('Description : ').'</span>'.$descp."</div>";
		//$form .= "<div>".$rowResource->offices_title."</div>";
		$form .= "</div></div>";
		echo $form;
		$a++;
	}
	echo '</div>';
	
	}else{
		echo '<div class="resourceoffice_bodytable">';
		echo JText::_('JGLOBAL_NOTFOUND_RESOURCE');
		echo '</div>';
		} //end if count
	} 
	
	
	//echo $footer; 
			      
}//end funtion listResources  
function listResources1($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$resources_offices = (int)JRequest::getString('resources_offices', 0);
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_LIST_RESOURCES_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_LIST_RESOURCES_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
    
    $form .= '<form name="uploadForm" id="uploadForm" action="'.$uri.'" onsubmit="return;" method="post" enctype="multipart/form-data">
	<table class="jd_upload_form" border="0" cellpadding="0" cellspacing="5" width="99%">';
	
		$form .= '<tr>
            <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
            <td><div class="select-office-dropdown">';
  			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		   $form .= $listbox_officess;
        $form .= '
                <input class="selectofficebtn" type="submit" name="submit_go" value="'.JText::_('COM_EPOSTS_GO_BTN').'"/></div></td>
        </tr>';      
		
        $form .= '
            </table>	
	<input type="hidden" name="option" value="'.$option.'" />
	<input type="hidden" name="view" value="'.$view.'" />
	<input type="hidden" name="send" value="1" />
	<input type="hidden" name="MAX_FILE_SIZE" value="'.$max_file_size.'"/>'.JHTML::_( 'form.token' ).'</form>';	

	echo $form; 
	
	if($resources_offices){
	$database->setQuery( "SELECT R.*, O.offices_title, O.offices_title_ar FROM #__eposts_resources R LEFT JOIN #__eposts_offices O ON  R.resources_offices = O.id WHERE R.resources_offices ='".$resources_offices."'ORDER BY resources_title ASC");
    $listOfResources = $database->loadObjectList();
	if(count($listOfResources)){
	$ofctitle = $listOfResources[0]->offices_title;
	if ($tag == 'ar-AA') :
		$ofctitle = $listOfResources[0]->offices_title_ar;
	endif;
	
	print '<div class="ofctitle">'.$ofctitle . '</div><br>';
	echo "<div id='resourcesoffices'>";
	foreach($listOfResources as $rowResource){
		$title = $rowResource->resources_title;
		$descp = $rowResource->resources_description;
		if ($tag == 'ar-AA') :
		      $title = $rowResource->resources_title_ar;
			  $descp = $rowResource->resources_description_ar;
		   endif;
		   $title = strip_tags($title);
$config =&JFactory::getConfig();
 $filepath = $config->getValue( 'config.file_path' ); 
if($rowResource->resources_logo){
 $img = $filepath.'\\'.$rowResource->resources_logo;
			  
			   $img = str_replace("\\", "/", $img);
	//		 echo $img;
			$filetype=string;       
	     $imgbinary = fread(fopen($img, "r"), filesize($img));
     // echo $imgbinary;
	   $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);

	}
else {
$aaa= 'components/com_eposts/assets/images/icons/meeting-room.png';

}	
		$form = "<div class='resoffice'><div class='resofficeareapic'><img src=".$aaa." border='0' alt='' width='149px' height='100px'  /></div><div class='resofficearea'><div class='resofficetitle'><a href='".JRoute::_("index.php?option=com_eposts&view=calendar&resourceid=".$rowResource->id."&resources_offices=".$resources_offices)."'>".$title."</a></div>";		   
		
		$form .= "<div class='resofficedesc'><span class='resofficedescspan'>".JText::_('Description : ').'</span>'.$descp."</div>";
		//$form .= "<div>".$rowResource->offices_title."</div>";
		$form .= "</div></div>";
		echo $form;
	}
	echo '</div>';
	
	}else{
		 echo JText::_('JGLOBAL_NOTFOUND');
		} //end if count
	}//end resources
	
	
	//echo $footer; 
			      
}//end funtion listResources  

///////////////////////////////resource Calendar//////////////////resourcesCalendar
//-------------------------------------------------------------------------------
// Define cal_days_in_month() in case server doesn't support it
//
	function cal_days_in_month($calendar,$month, $year)
		{ 
		return date('t', mktime(0, 0, 0, $month+1, 0, $year)); 
		}


//---------------------------------------------------------------------------------------------
// Get an array of day names in the current language
//
function get_day_names($start_day)
{
	$j_days = array(JText::_('SUNDAY'),JText::_('MONDAY'),JText::_('TUESDAY'),JText::_('WEDNESDAY'),JText::_('THURSDAY'),JText::_('FRIDAY'),JText::_('SATURDAY'));
	for ($i = 0; $i < 7; $i++)
		{
		$day = ($i + $start_day) % 7;
		$days[] = $j_days[$day];
		}
	return $days;
}

//---------------------------------------------------------------------------------------------
// Get a month name in the current language
//
function get_month_name($month)
{
	switch ($month)
		{
		case 1: return JText::_('JANUARY');
		case 2: return JText::_('FEBRUARY');
		case 3: return JText::_('MARCH');
		case 4: return JText::_('APRIL');
		case 5: return JText::_('MAY');
		case 6: return JText::_('JUNE');
		case 7: return JText::_('JULY');
		case 8: return JText::_('AUGUST');
		case 9: return JText::_('SEPTEMBER');
		case 10: return JText::_('OCTOBER');
		case 11: return JText::_('NOVEMBER');
		case 12: return JText::_('DECEMBER');
		}
}

//---------------------------------------------------------------------------------------------
// Draw a calendar for one month in any language
// $link is blank for no links, or a url ready to append 'p' for previous or 'n' for next
//
function make_calendar($year, $month, $link = '', $day_name_length, $start_day, $weekHdr, $debug=false)
{
				global $jlistConfig, $Itemid, $mainframe, $page_title;
     // make sure we only get one copy of the helper (allows multiple instances of the calendar)
    $user = &JFactory::getUser();
	$database = &JFactory::getDBO();
	$current_year = date('Y');
	$current_month = date('m');
	$current_day = date('d');
	$resources_offices = (int)JRequest::getString('resources_offices', 0);
    
	$resourceid = (int)JRequest::getString('resourceid', 0);
    $document=& JFactory::getDocument();
	$database->setQuery("SELECT * FROM #__eposts_resources WHERE id = '".$resourceid."'");
				
				  $Resorcename = $database->loadObject();				  

				 $database->setQuery("SELECT * FROM #__eposts_offices WHERE id = '".$resources_offices."'" );
				
				  $Resorcelocation = $database->loadObject();
   
		 

		 echo  '<div class="breadcrumbs"><div class="showCrmb">
    	<ul>
        
        
	<li><a href="#" class="pathway">'.$Resorcelocation->offices_title.'</a></li><li><span> <img src="/media/system/images/arrow.png" alt=""> '.$Resorcename->resources_title.'</span></li>    	</ul>
    </div></div>';
	
	JHTML::_("behavior.mootools");
	$document->addScript(JURI::base(true). '/templates/emiratespost/javascript/floatingtips.js');
	
	$document->addScriptDeclaration("
		window.addEvent('domready', function() {
			new FloatingTips('.bookingSlots', {html: true});
		});
	");
	//////////////////////////////////////
	

	//////////////////////////////////////
	
	$num_columns = 7;										// without week numbers, we have 7 columns
	if (($weekHdr != '') and ($start_day == 1) and (!stristr(PHP_OS, 'WIN')))
		$num_columns = 8;
	else
		$weekHdr = '';										// if start day not Monday, or we are on Windows, don't do week numbers
	
	echo "\n".'<table class="mod_minical_table" width="100%" align="center" cellspacing="0" cellpadding="0">'."\n";
	
	echo "\n<tr>";
	
// draw the month and year heading in the current language

	echo '<th colspan="'.$num_columns.'">';
	if ($link != '')
		echo '<a href="'.$link.'p"><span class="mod_minical_left">&laquo;</span></a>&nbsp;&nbsp;';
	$month_string = jlist_HTML::get_month_name($month).' '.$year;
	echo $month_string;
	if ($link != '')
		echo '&nbsp;&nbsp;<a href="'.$link.'n"><span class="mod_minical_right">&raquo;</span></a>';
	echo '</th>';
	echo '</tr>';
	print '<tr><th class="separator" colspan="'.$num_columns.'">&nbsp;</th></tr>';
	
// draw the day names heading in the current language

	if ($day_name_length > 0)
		{
		echo "\n<tr>";
		if ($weekHdr != '')
			echo "<th>".$weekHdr."</th>";
		$days = jlist_HTML::get_day_names($start_day);
		for ($i = 0; $i < 7; $i++)
			{
			$day_name = $days[$i];
			if (function_exists('mb_substr'))
				$day_short_name = mb_substr($day_name,0,$day_name_length,'UTF-8');	// prefer this if available
			else
				$day_short_name = substr($day_name,0,$day_name_length);		// use this if no mbstring library
			echo "<th>$day_short_name</th>";
			}
		echo '</tr>';
		}
	
// draw the days

	$day_time = gmmktime(5,0,0,$month,1,$year);			// GMT of first day of month
	if ($debug)
		mc_trace("\nStart:      ".gmstrftime("%Y-%m-%d %H:%M (wk %V)",$day_time));
	$first_weekday = gmstrftime("%w",$day_time);		// 0 = Sunday ... 6 = Saturday
	$first_column = ($first_weekday + 7 - $start_day) % 7; 	// column for first day
	$days_in_month = cal_days_in_month(CAL_GREGORIAN,$month,$year);
	echo '<tr>';
	if ($weekHdr != '')
		{
		$weeknumber = gmstrftime("%V",$day_time);			// first week number (doesn't work on Windows)
		echo '<td class="mod_minical_weekno">'.$weeknumber.'</td>';
		}
	for ($i = 0; $i < $first_column; $i++)
		echo '<td></td>';
	$column_count = $first_column;
	for ($day = 1; $day <= $days_in_month; $day++)
		{
		if ($column_count == 7)
			{
			echo "</tr>\n<tr>";
			$column_count = 0;
			if ($weekHdr != '')
				{
				// $day_time = strtotime(strftime('%Y-%m-%d',$day_time).' + 1 week');
				$day_time += 604800; 		// add exactly one week
				if ($debug)
					mc_trace(" next week: ".gmstrftime("%Y-%m-%d %H:%M (wk %V)",$day_time));
				$weeknumber = gmstrftime("%V",$day_time);	// week number
				echo '<td class="mod_minical_weekno">'.$weeknumber.'</td>';
				}
			}
			$bookLink = JRoute::_("index.php?option=com_eposts&view=bookingrequest&resourceid=".$resourceid."&resources_offices=".$resources_offices."&bookingdate=".$year.'-'.$month.'-'.$day);
			
			 $crr_month	= date('m', time());
			 $crr_day	= date('d', time());
			 $crr_year	= date('Y', time());
			 
			 $crr_date	= strtotime(date('m/d/y', time()));
			 $cal_date	= strtotime(date($month.'/'.$day.'/'.$year));
			
			 
	         $fullday = '<div class="slotTitle">';
			 $fullday .= ($crr_date > $cal_date) ? '' : '<a href="'.$bookLink.'">';
			 $fullday .= $day;
			 $fullday .= ($crr_date > $cal_date) ? '' : '</a>';
			 $fullday .= '</div>';
			 
				  

/*$database->setQuery("SELECT * FROM #__eposts_bookingrequest WHERE resourceid = '".$resourceid."' && bookingdate = '".$year.'-'.$month.'-'.$day."'"
						  . "\n ORDER BY from_h ASC,to_m ASC"
					  );*/
		 
$database->setQuery("SELECT * FROM #__eposts_bookingrequest WHERE resourceid = '".$resourceid."' && bookingdate = '".$year.'-'.$month.'-'.$day."'"
						  . "\n ORDER BY ABS(from_h) ASC"
					  );
				
				    $approvedResorce = $database->loadObjectList();
		
		foreach($approvedResorce as $xslot) {
				  	 if($xslot->booking_status == '1'){
						 $fullday .= '<div class="bookingSlots" style="background-color:red;" title="<h4>'.$xslot->booking_title.'</h4>'.date('D, F d,', strtotime($xslot->bookingdate)).' '.date('h:i a', strtotime($xslot->from_h.':'.$xslot->from_m)).' - '.date('h:i a', strtotime($xslot->to_h.':'.$xslot->to_m)).'<p>'.$xslot->booking_description.'</p>">&nbsp;'.date('h:i a', strtotime($xslot->from_h.':'.$xslot->from_m)).' - '.date('h:i a', strtotime($xslot->to_h.':'.$xslot->to_m)).'<br /> '.$xslot->booking_title.'</div>';
					 }elseif($xslot->booking_status == '0'){
						 $fullday .= '<div class="bookingSlots"  style="background-color:yellow;" title="<h4>'.$xslot->booking_title.'</h4>'.date('D, F d,', strtotime($xslot->bookingdate)).' '.date('h:i a', strtotime($xslot->from_h.':'.$xslot->from_m)).' - '.date('h:i a', strtotime($xslot->to_h.':'.$xslot->to_m)).'<p>'.$xslot->booking_description.'</p>">&nbsp;'.date('h:i a', strtotime($xslot->from_h.':'.$xslot->from_m)).' - '.date('h:i a', strtotime($xslot->to_h.':'.$xslot->to_m)).'<br />'.$xslot->booking_title.'</div>';
					 }else{
						 //$fullday .= '<div class="bookingSlots"><a href="'.$bookLink.'">'.$arSlot->bookingslots_title.'</a></div>';
					 }
				  }
				if (($year == $current_year) and ($month == $current_month) and ($day == $current_day)){
					
					echo '<td id="mod_minical_today"'.'>'.$fullday.'</td>';	// highlight today's date
				}else{
					echo  '<td>'.$fullday.'</td>';
				}
		$column_count ++;
		}
	for ($i = $column_count; $i < 7; $i++)
		echo '<td></td>';
	echo "</tr></table>\n";
}

function resourcesCalendar($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
// make sure we only get one copy of the helper (allows multiple instances of the calendar)
$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_LIST_RESOURCES_CALENDAR_PAGE_TITLE'));
$html_form = makeHeader(JText::_('COM_EPOSTS_LIST_RESOURCES_CALENDAR_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
// Get module parameters

$startyear 	     = (int)JRequest::getString('startyear');
$startmonth	     = (int)JRequest::getString('startmonth');
$numMonths 	     = (int)JRequest::getString('numMonths','1');
$numCols 	     = (int)JRequest::getString('numCols','1');
$links 		     = JRequest::getString('numCols',0);
$timeZone	     = JRequest::getString('timeZone',0);
$day_name_length = JRequest::getString('dayLength',9);	// length of the day names
$start_day       = JRequest::getString('firstDay',0);	// 0 for Sunday, 1 for Monday, etc
$weekHdr         = JRequest::getString('weekHdr','');
$debug 		     = JRequest::getString('debug',0);


if (($timeZone != '0') and (function_exists('date_default_timezone_set')))
	date_default_timezone_set($timeZone);

// Set the initial month and year, defaulting to the current month

if ($startyear)
	$year 	= $startyear;
else
	$year 	= date('Y');

if ($startmonth)
	$month 	= $startmonth;
else
	$month 	= date('m');

// Add in the current offset

$startdate = mktime(0,0,0,$month + $current_offset, 1, $year);
$month = date('m',$startdate);
$year = date('Y',$startdate);

$resources_offices = (int)JRequest::getString('resources_offices', 0);
$resourceid = (int)JRequest::getString('resourceid', 0);

$newLink = "index.php?option=com_eposts&view=calendar&resourceid=".$resourceid."&resources_offices=".$resources_offices;
if($month + 1 > 12){
	$nxtLink = $newLink .'&startmonth=1&startyear='.($year+1);
}else{
	$nxtLink = $newLink .'&startmonth='.($month+1).'&startyear='.$year;
}

if($month - 1 < 1){
	$preLink = $newLink .'&startmonth=12&startyear='.($year-1);
}else{
	$preLink = $newLink .'&startmonth='.($month-1).'&startyear='.$year;
}



// Draw the number of calendars requested in the module parameters
echo "<table><tr>
      <td style='background-color:red;' width='30px'></td><td></td><td>".JText::_('COM_EPOSTS_BOOKED_SLOTS')."</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
	  <!--<td style='background-color:yellow;' width='30px'></td><td></td><td>".JText::_('COM_EPOSTS_WAITING_APPROVAL')."</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>-->
	  <td width='30px'>".JText::_('COM_EPOSTS_OTHER')."</td><td></td><td>=</td><td></td><td>".JText::_('COM_EPOSTS_FREE_SLOTS')."</td><td>&nbsp;</td>
	  </tr></table>";
?>
<div>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
        	<td><a class='bookingPrevious' href="<?php print JRoute::_($preLink); ?>">&lt;&lt;<?php print JText::_('COM_EPOSTS_PREVIOUS'); ?></a></td>
            <td><a class='bookingNext' href="<?php print JRoute::_($nxtLink); ?>"><?php print JText::_('COM_EPOSTS_NEXT'); ?>&gt;&gt;</a></td>
        </tr>
    </table>
</div>
<?php
echo '<table width="100%" align="center" cellpadding="0" cellspacing="0"><tr valign="top">';
$colcount = 0;
for ($monthcount = 1; $monthcount <= $numMonths ; $monthcount ++)
	{
	$colcount ++;
	echo '<td>';
	echo jlist_HTML::make_calendar($year, $month, $link, $day_name_length, $start_day, $weekHdr, $debug);
	$link = '';						// only draw links on first calendar
	echo '</td>';
	if (($colcount == $numCols) && ($monthcount < $numMonths))
		{
		echo '</tr><tr valign="top"><td colspan="2"><div class="mod_minical_div"></div></td></tr><tr valign="top">';
		$colcount = 0;
		}
	$month ++;
	if ($month > 12)
		{
		$month = 1;
		$year ++;
		}
	}
echo '</tr></table>';
		
		
	//echo $footer; 
			      
}//end funtion resourcesCalendar 

//////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
//viewBookingRequest
function viewBookingRequest($option, $view ){
global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_BACKEND_CPANEL_MYBOOKINN'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_BACKEND_CPANEL_MYBOOKINN'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = 'WHERE 1 = 1 && publishedby  ="'.$user->id.'" ';
   

  $search = JRequest::getVar('search', '');
  
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(bookingdate) LIKE '%".$search."%'";
        $where .= " OR LOWER(booking_title) LIKE '%".$search."%') ";
    }    
	
	   $database->SetQuery( "SELECT count(*) FROM #__eposts_bookingrequest ".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_bookingrequest
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
   
	$newlink = JRoute::_("index.php?option=com_eposts&view=listresources");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Booking</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td  align="right" colspan="7">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
       </tr>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_MYBOOKINN_LIST_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_MYBOOKINN_LIST_DATE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_MYBOOKINN_LIST_RESOURCE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('From')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('To')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_MYBOOKINN_LIST_OTERREQUIRED')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('Status')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_MYBOOKINN_LIST_ACTION')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=bookingrequest&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=viewbookingrequest&task=delbookingrequest&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
            <a href="<?php echo $link; ?>"><?php echo $row->booking_title; ?></a>
            </td>
            <td valign="top"><?php echo JText::sprintf('%s', JHtml::_('date', $row->bookingdate, JText::_('DATE_FORMAT_LC5'))); ?></td>
             <td valign="top"><?php
			$row2 = new jlist_resources( $database );
            $row2->load( $row->resourceid );
			echo $row2->resources_title;
			?></td>
            <td valign="top"><p><?php
		/*$database->setQuery("SELECT * "
						  . "\n FROM #__eposts_bookingslots "
						  . "\n WHERE id IN (SELECT slots_id FROM #__eposts_bookingrequest_slots WHERE bookingrequest_id IN (".$row->id."))"
						  . "\n ORDER BY id DESC"
					  );
				  $listslResorce = $database->loadObjectList();

			 
			 foreach($listslResorce as $arSlot){
					 //echo '<div class="bookingSlots">'.$arSlot->bookingslots_title.'</div>';
			 }*/
			 print sprintf('%02d',$row->from_h) . ':' . sprintf('%02d',$row->from_m);
			?></p></td>
            <td valign="top"><p><?php print sprintf('%02d', $row->to_h) . ':' . sprintf('%02d', $row->to_m); ?></p></td>
            <td valign="top"><?php echo $row->booking_details_description; ?></td>
            <td valign="top"><?php
			 if($row->booking_status == '1'){
				 echo "Approved";
			}elseif($row->booking_status == '2'){
				 echo "Rejected";
			}else{
				 echo "Pending";
			} 
			 
			 ?></td>
            <td valign="top"><a href="<?php echo $dellink; ?>"><?php echo JText::_('COM_EPOSTS_BACKEND_MYBOOKINN_DELETE_TITLE');?></a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer;
			      
}//end funtion viewBookingRequest 



//viewApproveBookingRequest
function viewApproveBookingRequest($option, $view ){
global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
 	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

   $where = 'WHERE submittedto = "'.$user->id.'"';
  

  $search = JRequest::getVar('search', '');
  
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(bookingdate) LIKE '%".$search."%'";
        $where .= " OR LOWER(booking_title) LIKE '%".$search."%') ";
    } 
		
	   $database->SetQuery( "SELECT count(*) FROM #__eposts_bookingrequest ".$where);
    $total = $database->loadResult();     
    
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_bookingrequest
              $where
              ORDER BY id DESC";
     // echo $query;                                                          
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();   
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td  align="right" colspan="7">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_DATE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_RESOURCE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_SLOTS')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_OTERREQUIRED')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('Requested By')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('Status')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_ACTION')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=bookingrequest&type=approvebooking&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
            <a href="<?php echo $link; ?>"><?php echo $row->booking_title; ?></a>
            </td>
            <td valign="top"><?php echo date('d-m-Y', strtotime($row->bookingdate)); ?></td>
             <td valign="top"><?php
			$row2 = new jlist_resources( $database );
            $row2->load( $row->resourceid );
			echo $row2->resources_title;
			?></td>
            <td valign="top"><p><?php echo 'From '.sprintf('%02d',$row->from_h).':'.sprintf('%02d',$row->from_m).'<br>&nbsp;&nbsp;&nbsp;&nbsp;To '. sprintf('%02d',$row->to_h).':'.sprintf('%02d',$row->to_m); ?></p></td>
            <td valign="top"><?php echo $row->booking_details_description; ?></td>
            <td valign="top"><?php
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $row->publishedby."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
			 ?></td>
            <td valign="top"><?php
			 if($row->booking_status == '1'){
				 echo "Approved";
			}elseif($row->booking_status == '2'){
				 echo "Rejected";
			}else{
				 echo "Pending";
			} 
			 
			 ?></td>
            <td valign="top">
            <?php
       
		   $applink = JRoute::_("index.php?option=com_eposts&view=approvebookingrequest&task=approve.approvebookingrequest&cirid=".$row->id);
			$rejectlink = JRoute::_("index.php?option=com_eposts&view=approvebookingrequest&task=reject.approvebookingrequest&cirid=".$row->id);
			
			if ($row->booking_status == 0) :
			?>
            <a href="<?php echo $applink; ?>"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LINK_APPROVED')." "; ?></a>&nbsp;&nbsp;<a href="<?php echo $rejectlink; ?>"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LINK_REJECTED')." "; ?></a>
            <?php endif; ?>
            </td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer;
}//end funtion viewApproveBookingRequest 

/////////////////////////////////////////////////////////////////////////////////
function addUserBookingRequest($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	
	$database = &JFactory::getDBO();
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$varString = JRequest::getString('varString');
	

	$query = "SELECT id,name,username FROM #__users Where name LIKE '%".$varString."%' ORDER BY block ASC, name ASC";
    $database->setQuery($query);
    $usersToAdd = $database->loadObjectList();
	$totalresults = count($usersToAdd);
	if(empty($varString)){
		echo "2-Please enter name of the user first.";
		exit;
	}
	if($totalresults == 0){
		echo "2-There is no match found.";
		exit;
	}
	if($totalresults > 1){
		echo "2-There are more then one results found. Please try with browsing";
		exit;
	}
	
	$toAddUsers = '';
    foreach($usersToAdd as $zuser) {
        //$toAddUsers[] = JHTML::_('select.option',$zuser->id, $zuser->name . " (" . $zuser->username . ")" );
		$toAddUsers = '1-<div id="contenContainer'.$zuser->id.'"><input name="users_selected[]" value="'.$zuser->id.'" style="display:none;" /><input type="button" value="X" onclick="removeMe(\'contenContainer'.$zuser->id.'\')"/>&nbsp;&nbsp;'.$zuser->name.'</div>';				
    }
 echo $toAddUsers;
 exit;
}//end funtion addUserBookingRequest
function ajaxListbookingRequest($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	
	$database = &JFactory::getDBO();
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);	
 ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Users List</title>
<link rel="stylesheet" href="<?php print JURI::root(true); ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php print JURI::root(true); ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php print JURI::root(true); ?>/templates/emiratespost/css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php print JURI::root(true); ?>/templates/emiratespost/css/template-rtl.css" type="text/css" />
<script language="javascript" type="text/javascript">
function addToListing(rmID, userID,userName){
opener.document.getElementById('myUsersList').innerHTML += '<div id="'+rmID+'"><input name="users_selected[]" value="'+userID+'" style="display:none;" /><input type="button" value="X" onclick="removeMe(\''+rmID+'\')"/>&nbsp;&nbsp;'+userName+'</div>';
var d = document.getElementById('myUsersList');
  var olddiv = document.getElementById(rmID);
  d.removeChild(olddiv);
}
</script>
</head>
<body>
<div id="myUsersList" style="text-align:left; padding:20px;">
    <div>
    	<form method="post" action="index.php?option=com_eposts&view=ajaxbookingrequest">
        <input type="text" name="varString" value="" /><input type="submit" value="Search" />
        </form>
    </div>
 <?php
 	$search_str = JRequest::getVar("varString");
	if ($search_str != '') :
		$where = "WHERE username LIKE '%".$search_str."%' OR name LIKE '%".$search_str."%' OR email LIKE '%".$search_str."%'";
	endif;
	$query = "SELECT id,name,username FROM #__users $where";
    $query .= "\n ORDER BY block ASC, name ASC";
    $database->setQuery($query);
    $usersToAdd = $database->loadObjectList();
	$toAddUsers = '';
    foreach($usersToAdd as $zuser) {
        //$toAddUsers[] = JHTML::_('select.option',$zuser->id, $zuser->name . " (" . $zuser->username . ")" );
		$toAddUsers .= '<div id="contenContainer'.$zuser->id.'"><input name="users_selected[]" value="'.$zuser->id.'" style="display:none" /><input type="button" value="Add" onclick="addToListing(\'contenContainer'.$zuser->id.'\','.$zuser->id.',\''.addslashes($zuser->name).'\')"/>&nbsp;&nbsp;'.$zuser->name.'</div>';
    }
 echo $toAddUsers;
 ?>
 </div>
 </body>
</html>
 <?php
 exit;
}//end funtion ajaxListbookingRequest 
////////////////////////////////////////////////////////////////////////////////

function bookingRequest($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_LIST_RESOURCES_PAGE_TITLE'));
                            
    $can_upload = false;
	
	$type = JRequest::getVar('type');
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_LIST_RESOURCES_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
	
	$cirid = (int)JRequest::getString('cirid', 0);
	
	$row = new jlist_bookingrequest( $database );
    $row->load( $cirid );
	
	if($row->id){
		$resourceid = $row->resourceid;
	    $resources_offices = $row->resources_offices;
	    $bookingdate = $row->bookingdate;		
	}else{
		$resourceid = (int)JRequest::getString('resourceid', 0);
		$resources_offices = (int)JRequest::getString('resources_offices', 0);
		$bookingdate = JRequest::getString('bookingdate', '');
	}
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>	
<script language="javascript" type="text/javascript">
    function validateform(){
        var form = document.bookingForm;
		// do field validation;
        if ( form.booking_title.value == ""
			 || tinyMCE.get('booking_description').getContent()== ""
			 ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL'); ?>" );	
			return false;
        }
		if (form.from_h.value == '00' && form.from_m.value == '00' && form.to_h.value == '00' && form.to_m.value == '00') {
			alert("<?php echo JText::_('Please select booking time.'); ?>");
			return false;
		}
		var s_time = parseInt(form.from_h.value);
		var sm_time = parseInt(form.from_m.value);
		var stime;
		stime  = s_time +':' + sm_time;
		var e_time = parseInt(form.to_h.value);
		var em_time =  parseInt(form.to_m.value)
		var etime;
		etime  = e_time +':' + em_time;
		if (Date.parse(stime) >= Date.parse(etime)){
			alert("End time should be greater than start time.");
			return false;
		}
		//alert(parseInt(form.from_h.value)+":"+parseInt(form.to_h.value));
		//users_selected[]
		if(document.getElementById('myUsersList').innerHTML == "" || document.getElementById('myUsersList').innerHTML == ""){
		   alert("Users List is empty. Please assign user(s) to continue.");
			return false;
		}
		return true;
    }
    
    // moves elements from one select box to another one
    function moveOptions(from,to) {
      // Move them over
      for (var i=0; i<from.options.length; i++) {
           var o = from.options[i];
           if (o.selected) {
               to.options[to.options.length] = new Option( o.text, o.value, false, false);
           }
       }
       // Delete them from original
       for (var i=(from.options.length-1); i>=0; i--) {
            var o = from.options[i];
            if (o.selected) {
                from.options[i] = null;
            }
       }
       from.selectedIndex = -1;
       to.selectedIndex = -1;
    }

    function allSelected(element) {
       for (var i=0; i<element.options.length; i++) {
            var o = element.options[i];
                o.selected = true;
       }
    }
	
	var xmlhttpclick;

function getallSearchClicked()
{
	//alert('CHHEENA');
xmlhttpclick  = GetXmlHttpObject100();

if (xmlhttpclick==null)
  {
  alert ("Browser does not support HTTP Request");
  return;
  }
var str = document.getElementById('searchUsrrs').value;
/* ***************** For State *************************/
var url='index.php?option=com_eposts&view=addUserBookingRequest';
url=url+'&varString='+str;
url=url+"&sid="+Math.random();
xmlhttpclick.onreadystatechange = stateChanged100;
xmlhttpclick.open("GET",url,true);
xmlhttpclick.send(null);
//alert(url);

}



function stateChanged100()
{
if (xmlhttpclick.readyState==4)
{
	var oslu = xmlhttpclick.responseText;
	 var gettheOutsdf = oslu.split("-");
	 if(gettheOutsdf[0] == '2'){
     document.getElementById('myUsersWrongMsg').innerHTML = gettheOutsdf[1];
	 document.getElementById('myUsersWrongMsg').style.display='block';
	 }else{
	  document.getElementById('myUsersList').innerHTML += gettheOutsdf[1];
	  document.getElementById('searchUsrrs').value = '';
	  document.getElementById('myUsersWrongMsg').style.display='none';
	 }
	 
}

}



function GetXmlHttpObject100()
{
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}
function restricted() {
	//alert('You are not allowd to modify this form.');
	//return false;
}
    </script>
<form name="bookingForm" id="bookingForm" class="" action="<?php echo $uri;?>" method="post" enctype="multipart/form-data"  onsubmit="<?php print $type == 'approvebooking' || $row->booking_status != 0 ? 'return restricted();' : 'return validateform();'; ?>">
	<table class="jd_upload_form adminlist" border="0" cellpadding="0" cellspacing="5" width="99%">	
	    <tr>
            <td width="140" valign="middle"><?php print JText::_('COM_EPOSTS_BOOKING_DATE'); ?></td><td>
<?php            
$createDate = explode( '-', $bookingdate );
$changeLink = "index.php?option=com_eposts&view=calendar&resourceid=".$resourceid."&resources_offices=".$resources_offices."&startmonth=".$createDate[1]."&startyear=".$createDate[0];
echo date('d-m-Y', strtotime($bookingdate));


	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Booking') : $row->booking_title;
	$length = 100;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=bookingrequest'));
	
	$ePage = JRequest::getVar('page');
?>	
<a style="display:none;" href="<?php echo JRoute::_($changeLink);?>">Change Date</a></td>
        </tr>
		<?php
		if ($ePage && $row->booking_title == '' && $_SESSION['bookingForm']['booking_title'] != '') {
			$row->booking_title = $_SESSION['bookingForm']['booking_title'];
		}
		?>
		<tr>
            <td><?php print JText::_('COM_EPOSTS_BOOKING_TITLE'); ?><font color="#990000">*</font></td><td>
			<input name="booking_title" type="text" value="<?php echo htmlspecialchars($row->booking_title, ENT_QUOTES);?>" size="80" maxlength="150"/></td>
        </tr> 
		
		<tr>
            <td><?php print JText::_('COM_EPOSTS_BOOKING_DETAILS_MEMBERS'); ?><font color="#990000">*</font></td><td>
<?php 
$editor = & JFactory::getEditor();
		
		if ($ePage && $row->booking_description == '' && $_SESSION['bookingForm']['booking_description'] != '') {
			$row->booking_description = $_SESSION['bookingForm']['booking_description'];
		}
echo $editor->display( 'booking_description',  @$row->booking_description , '100%', '10', '80', '5', false, false ) ;
?>		</td>
       </tr> 
			
								
       </tr>
<?php        
///////////////////////////////////////////////////////////////////////	
    if ($row->id) {
        $database->setQuery("SELECT * "
                . "\n FROM #__eposts_bookingslots "
                . "\n WHERE id IN (SELECT slots_id FROM #__eposts_bookingrequest_slots WHERE bookingrequest_id = " . $row->id . ")"
                . "\n ORDER BY id DESC"
            );
        $slotsInResorce = $database->loadObjectList();

        foreach($slotsInResorce as $xslot) {
            $currentSlots[] = JHTML::_('select.option',$xslot->id,$xslot->bookingslots_title);
        }

    }
    // get all other slots
    $query = "SELECT * FROM #__eposts_bookingslots ";
	 $query .= "\n WHERE id NOT IN (SELECT slots_id FROM #__eposts_bookingrequest_slots WHERE bookingrequest_id IN (
	 SELECT id FROM #__eposts_bookingrequest WHERE resourceid = '".$resourceid."' && bookingdate = '".$bookingdate."'&& booking_status IN (0,1)))" ;
/*  if ($row->id) {
        $query .= "\n && id NOT IN (SELECT slots_id FROM #__eposts_bookingrequest_slots WHERE bookingrequest_id = " . $row->id . ")" ;
    }*/
  $query .= "\n ORDER BY id DESC";
    $database->setQuery($query);
    $slotsToAdd = $database->loadObjectList();
    foreach($slotsToAdd as $zslot) {
        $toAddSlots[] = JHTML::_('select.option',$zslot->id, $zslot->bookingslots_title );
    }
	
    $slotsList = JHTML::_('select.genericlist',$currentSlots, 'slots_selected[]',
        'class="inputbox" size="16" onDblClick="moveOptions(document.bookingForm[\'slots_selected[]\'], document.bookingForm.slots_not_selected)" multiple="multiple"', 'value', 'text', null);
    $toAddSlotsList = JHTML::_('select.genericlist',$toAddSlots,
        'slots_not_selected', 'class="inputbox" size="16" onDblClick="moveOptions(document.bookingForm.slots_not_selected, document.bookingForm[\'slots_selected[]\'])" multiple="multiple"',
        'value', 'text', null);
?>			
<tr>
            <td><?php print JText::_('COM_EPOSTS_BOOKING_SELECT_TIME'); ?></td><td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><?php print JText::_('COM_EPOSTS_BOOKING_SELECT_TIME_FROM'); ?><font color="#990000">*</font></td>
    <td>&nbsp;</td>
    <td><?php print JText::_('COM_EPOSTS_BOOKING_SELECT_TIME_TO'); ?><font color="#990000">*</font></td>
  </tr>
  <tr class="time">
  						<?php 
							$bookdate = strtotime($bookingdate);
							$crrtdate = strtotime(date('Y-m-j', time()));
							
							$stHour = 0;
							$stMins = 0;
							
							if ($bookdate == $crrtdate) :
								$stHour = date('G', time());
								//$stMins = date('m', time());
							endif;
						?>
                        <td width="40%">
							<select name="from_h">
                            	<?php for($i = $stHour; $i < 24; $i++) : ?>
                                <?php $h = sprintf('%02s',$i); ?>
                            	<option value="<?php print $i; ?>" <?php print ($row->from_h == $h) ? 'selected="selected"' : ''; ?>><?php print $h; ?></option>
                                <?php endfor; ?>
                            </select>
							<select name="from_m">
                            	<?php for($i = $stMins; $i <= 60; $i++) : ?>
                                <?php $m = sprintf('%02s',$i); ?>
                            	<option value="<?php print $i; ?>" <?php print ($row->from_m == $m) ? 'selected="selected"' : ''; ?>><?php print $m; ?></option>
                                <?php endfor; ?>
                            </select><span>&nbsp;<?php print JText::_('COM_EPOSTS_BOOKING_HH_MM'); ?></span>
                        </td>
                        <td width="20%" style="vertical-align:middle;">
                        </td>
                        <td width="40%">
							<select name="to_h">
                            	<?php for($i = $stHour; $i < 24; $i++) : ?>
                                <?php $h2 = sprintf('%02s',$i); ?>
                            	<option value="<?php print $i; ?>" <?php print ($row->to_h == $h2) ? 'selected="selected"' : ''; ?>><?php print $h2; ?></option>
                                <?php endfor; ?>
                            </select>
							<select name="to_m">
                            	<?php for($i = $stMins; $i <= 60; $i++) : ?>
                                <?php $m2 = sprintf('%02s',$i); ?>
                            	<option value="<?php print $i; ?>" <?php print ($row->to_m == $m2) ? 'selected="selected"' : ''; ?>><?php print $m2; ?></option>
                                <?php endfor; ?>
                            </select><span>&nbsp;<?php print JText::_('COM_EPOSTS_BOOKING_HH_MM'); ?></span>
						</td>
                    </tr>
</table>

</td>
        </tr>
<!--
<?php        	
//////////////////////////////////////////////////////////
	
    if ($row->id) {
        $database->setQuery("SELECT id,name,username "
                . "\n FROM #__users "
                . "\n WHERE id IN (SELECT user_id FROM #__eposts_bookingrequest_members WHERE bookingrequest_id = " . $row->id . ")"
                . "\n ORDER BY block ASC, name ASC"
            );
        $usersInResorce = $database->loadObjectList();

        foreach($usersInResorce as $xuser) {
            $musers[] = JHTML::_('select.option',$xuser->id,
                    $xuser->name . " (" . $xuser->username . ")"
                    );
        }

    }
    // get all other users
    /*$query = "SELECT id,name,username FROM #__users ";
    if ($row->id) {
        $query .= "\n WHERE id NOT IN (SELECT user_id FROM #__eposts_bookingrequest_members WHERE bookingrequest_id = " . $row->id . ")" ;
    }
    $query .= "\n ORDER BY block ASC, name ASC";*/
	$query = "SELECT id,name,username FROM #__users ";
    if ($row->id) {
        $query .= "\n WHERE id IN (SELECT user_id FROM #__eposts_bookingrequest_members WHERE bookingrequest_id = " . $row->id . ")" ;
    }
    $query .= "\n ORDER BY block ASC, name ASC";
    $database->setQuery($query);
    $usersToAdd = $database->loadObjectList();
	$toAddUsers = '';
    foreach($usersToAdd as $zuser) {
        //$toAddUsers[] = JHTML::_('select.option',$zuser->id, $zuser->name . " (" . $zuser->username . ")" );
		//$toAddUsers .= '<div id="contenContainer'.$zuser->id.'"><input type="button" value="X" onclick="removeMe(\'contenContainer'.$zuser->id.'\')"/><input name="users_selected[]" value="'.$zuser->id.'" style="display:none;" />'.$zuser->name.'</div>';				
    }
	
    /*$usersList = JHTML::_('select.genericlist',$musers, 'users_selected[]',
        'class="inputbox" size="15" onDblClick="moveOptions(document.bookingForm[\'users_selected[]\'], document.bookingForm.users_not_selected)" multiple="multiple"', 'value', 'text', null);
    $toAddUsersList = JHTML::_('select.genericlist',$toAddUsers,
        'users_not_selected', 'class="inputbox" size="15" onDblClick="moveOptions(document.bookingForm.users_not_selected, document.bookingForm[\'users_selected[]\'])" multiple="multiple"',
        'value', 'text', null);*/
?>		
<tr>
            <td valign="middle" colspan="3">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><?php print JText::_('COM_EPOSTS_BOOKING_USERS_LIST'); ?></td>
    <td>&nbsp;</td>
    <td><?php print JText::_('COM_EPOSTS_BOOKING_USERS_LIST_SELECTED'); ?><font color="#990000">*</font></td>
  </tr>
  <tr>
                        <td width="40%"><?php echo $toAddUsersList;?></td>
                        <td width="20%" style="vertical-align:middle;">
                          <input style="width: 50px" type="button" name="Button" value="&gt;" onClick="moveOptions(document.bookingForm.users_not_selected, document.bookingForm['users_selected[]'])" />
                            <br /><br />
                            <input style="width: 50px" type="button" name="Button" value="&lt;" onClick="moveOptions(document.bookingForm['users_selected[]'],document.bookingForm.users_not_selected)" />
                            <br /><br />
                        </td>
                        <td width="40%"><?php echo $usersList;?></td>
                    </tr>
  <tr>
    <td valign="middle" colspan="3"><?php print JText::_('COM_EPOSTS_BOOKING_DBCLICK_USERS_MSG'); ?></td>
  </tr>
</table>

        </td>
        </tr>
--> 
<script type="text/jscript">
function removeMe(rmID){
  var d = document.getElementById('myUsersList');
  var olddiv = document.getElementById(rmID);
  d.removeChild(olddiv);
}

function addFlySearch(){
	 window.open("<?php echo 'index.php?option=com_eposts&view=ajaxbookingrequest';?>", "newwin", "height=500, width=550,toolbar=no,scrollbars=1,menubar=0,resizable=0");
     window.scroll(0,0); 
}
</script> 
<tr>
            <td valign="middle">
            <?php print JText::_('COM_EPOSTS_BOOKING_USERS_LIST'); ?><font color="#990000">*</font></td><td>
               <div id="myUsersWrongMsg" style="display:none;">
               </div>
               <div><input name="selectNewUser" id="searchUsrrs" type="text" /><input type="button" value="add" onClick="getallSearchClicked()" /><input type="button" value="search" onClick="addFlySearch()" /></div>
            </td>
</tr> 
<tr>
            <td valign="middle">
            <?php print JText::_('COM_EPOSTS_BOOKING_USERS_LIST_SELECTED'); ?></td><td>
               <div id="myUsersList"><?php echo $toAddUsers;?></div>
            </td>
</tr> 
     		
<tr>
            <td><?php print JText::_('COM_EPOSTS_BOOKING_OTHER_USERS_LIST'); ?></td><td>

<?php            
		$editor2 =& JFactory::getEditor();
		
		if ($ePage && $row->booking_details_description == '' && $_SESSION['bookingForm']['booking_details_description'] != '') {
			$row->booking_details_description = $_SESSION['bookingForm']['booking_details_description'];
		}
		
		echo $editor2->display( 'booking_details_description',  @$row->booking_details_description , '100%', '10', '80', '5', false, false );
?></td>
        </tr>      
         <tr>
            <td valign="middle">&nbsp;</td><td>
                <input class="button" type="submit" name="submit" value="<?php print JText::_('COM_EPOSTS_SUBMIT_BTN'); ?>"/>
                <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
            </td>
            </tr>
            </table>
	
	
<input type="hidden" name="resourceid" value="<?php echo $resourceid;?>"/>
<input type="hidden" name="resources_offices" value="<?php echo $resources_offices;?>"/>
<input type="hidden" name="publishedon" value="<?php if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>"/>
<input type="hidden" name="publishedby" value="<?php if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?><?php ?>"/>
<input type="hidden" name="bookingdate" value="<?php echo $bookingdate;?>"/>
<input type="hidden" name="cirid" value="<?php echo $row->id;?>" />	
<input type="hidden" name="task" value="edit.bookingrequest" />		
<input type="hidden" name="send" value="1" /><?php echo JHTML::_( 'form.token' );?></form>	
<?php
	//echo $footer;
}//end funtion bookingRequest 

//myHomePage
function myHomePage($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $ItemId = JRequest::getInt('Itemid');
	
	if ($ItemId == '147') :
		$document->setTitle($page_title.' - '.JText::_('COM_EPOST_ADMINPANEL'));
		$html_form = makeHeader(JText::_('COM_EPOST_ADMINPANEL'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
		echo $html_form;
	else : 
		$document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MY_HOME_PAGE_TITLE'));
		$html_form = makeHeader(JText::_('COM_EPOST_MYHOMEPAGE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
		echo $html_form;
	endif;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
	if ($ItemId == '147') :
		echo JText::_('COM_EPOSTS_WELCOME_ADMINPANEL_TITLE');	
	else :
		//echo JText::_('COM_EPOSTS_WELCOME_MY_HOME_PAGE_TITLE');
		
		echo JText::_('<br> <iframe src="http://hqdint1/ExchangeIntegration/AllTasks.aspx" width="100%" height="100%" scrolling="no" frameborder="0"></iframe> ');	
	endif;
	//echo $footer; 
			      
}//end funtion myHomePage 

//mainMyHomePage
function mainMyHomePage($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MY_HOME_PAGE_TITLE'));
    $html_form = makeHeader(JText::_('MS Exchnage Tasks'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	echo $html_form;
	$html_form = '';  
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
require_once( JPATH_COMPONENT_SITE.DS.'class_http.php' );
require_once( JPATH_COMPONENT_SITE.DS.'class_xml.php' );

$exchange_server = "10.1.0.210";// no trailing slash
$exchange_username = "tajammul";
$exchange_password = "Acrologix123";
 
$h = new http();
 
$h->headers["Content-Type"] = 'text/xml; charset="UTF-8"';
 
$h->headers["Depth"] = "0";
$h->headers["Translate"] = "f";
$h->xmlrequest = '<?xml version="1.0"?>';
$h->xmlrequest .= <<<end
<g:searchrequest xmlns:g="DAV:">
        <g:sql> SELECT "DAV:id" AS uid,
		"DAV:href" AS url,
		"DAV:creationdate" AS created,
		"urn:schemas:httpmail:subject",
		"urn:schemas:httpmail:textdescription",
		"urn:schemas:httpmail:sendername" AS createdby,
		"urn:schemas:httpmail:to" AS receivedby,
		"urn:schemas:httpmail:from" AS sentby,
		"urn:schemas:calendar:location" AS location,
		"http://schemas.microsoft.com/mapi/id/{00062003-0000-0000-C000-000000000046}/0x811c" AS completed,
		"http://schemas.microsoft.com/mapi/id/{00062003-0000-0000-C000-000000000046}/0x00008105" AS duedate,
		"http://schemas.microsoft.com/mapi/id/{00062003-0000-0000-C000-000000000046}/0x00008104" AS start
		FROM Scope('SHALLOW TRAVERSAL OF "/exchange/$exchange_username/Tasks"')
		WHERE "DAV:contentclass" = 'urn:content-classes:task'
		AND CAST("http://schemas.microsoft.com/mapi/id/{00062003-0000-0000-C000-000000000046}/0x811c" AS "boolean") = CAST(false AS "boolean")
		ORDER BY "DAV:creationdate" DESC
         </g:sql>
</g:searchrequest>
end;
 
if (!$h->fetch($exchange_server."/exchange/$exchange_username/Tasks", 0, null, $exchange_username, $exchange_password, "SEARCH")) {
  exit();
}
 
$x = new xml();
if (!$x->fetch($h->body)) {
    exit();
}
 
function format_time($date_string) {
	$date = str_replace("-", "", $date_string);
	$date = str_replace(":", "", $date);
	$date = substr($date, 0, -5);
	return $date;
}
function format_time_z($date_string) {
	$date = str_replace("-", "", $date_string);
	//$date = str_replace(".000Z", "Z", $date);
	$date = str_replace(":", "", $date);
	$date = substr($date, 0, -5);
	return $date."Z";
}
?>
<table width="100%" border="0" cellspacing="4" cellpadding="0" class="adminlist">
  <tr>
    <th valign="top" class="title">Title</th>
    <th valign="top" class="title">Assigned By</th>
    <th valign="top" class="title">Start Date</th>
    <th valign="top" class="title">Due Date</th>
    <th valign="top" class="title">Status</th>
    <th valign="top" class="title">Priority</th>
    <th valign="top" class="title">% Completetion</th>
  </tr>
<?php
$k = 0;
foreach($x->data->A_MULTISTATUS[0]->A_RESPONSE as $num => $event) {
	@$UID = $event->A_PROPSTAT[0]->A_PROP[0]->UID[0]->_text;
	@$SUMMARY = $event->A_PROPSTAT[0]->A_PROP[0]->D_SUBJECT[0]->_text;
	@$CREATEDBY = $event->A_PROPSTAT[0]->A_PROP[0]->CREATEDBY[0]->_text;
	@$RECEIVEDBY = $event->A_PROPSTAT[0]->A_PROP[0]->RECEIVEDBY[0]->_text;
	@$SENTBY = $event->A_PROPSTAT[0]->A_PROP[0]->SENTBY[0]->_text;
	//$DESCRIPTION = $event->A_PROPSTAT[0]->A_PROP[0]->D_TEXTDESCRIPTION[0]->_text;
	@$CREATED = format_time_z($event->A_PROPSTAT[0]->A_PROP[0]->CREATED[0]->_text);
	//$STAMP = format_time_z($event->A_PROPSTAT[0]->A_PROP[0]->START[0]->_text);
	@$START = format_time($event->A_PROPSTAT[0]->A_PROP[0]->START[0]->_text);
	@$DUE = format_time($event->A_PROPSTAT[0]->A_PROP[0]->DUEDATE[0]->_text);
	@$COMPLETED = $event->A_PROPSTAT[0]->A_PROP[0]->COMPLETED[0]->_text;
?>
 <tr  class="<?php echo "row$k"; ?>">
    <td><?php echo $SUMMARY;?></td>
    <!--<td><?php //echo $CREATEDBY . ' - ' . $RECEIVEDBY . ' - ' . $SENTBY;?></td>-->
    <td><?php echo $CREATEDBY;?></td>
    <td><?php echo $START == '' ? '' : date("M d, Y", strtotime($START)); ?></td>
    <td><?php echo $DUE == '' ? '' : date("M d, Y", strtotime($DUE)); ?></td>
    <td><?php echo '';?></td>
    <td><?php echo '';?></td>
    <td><?php echo $COMPLETED;?></td>
  </tr>
<?php	
$k = 1 - $k;
}
?>
</table>
<?php	
	//echo $footer; 			      
}//end funtion myHomePage

function getIcon($fileext) {
	switch (strtolower($fileext)){
		case 'doc':
		case 'docx':
		case 'rtf':
			$file = 'doc.png';
		break;
		case 'jpg':
		case 'jpeg':
		case 'gif':
		case 'png':
		case 'bmp':
			$file = 'jpg.png';
		break;
		case 'pdf':
			$file = 'pdf.png';
		break;
		case 'xls':
		case 'xlsx':
			$file = 'xls.png';
		break;
		default:
			$file = 'txt.png';
		break;
	}
	return $file;
}

//myDepartment
function myDepartment($option, $view, $title, $objective, $tag){
	global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;
	
	$deptid = $_SESSION['deptid'] = JRequest::getInt('cirid');
	$_SESSION['dpItemid'] = JRequest::getInt('Itemid');
	
	$user = &JFactory::getUser();
	$resources_offices = (int)JRequest::getString('resources_offices', 0);
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
	
    $document->setTitle($page_title.' - '. $title);
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
                            
    $can_upload = false;
    
    $html_form = makeHeader($title, true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';
	$document->addScriptDeclaration("
		document.addEvent('domready', function(){
			var tabPane = new TabPane('ane_area');
			var tabPane = new TabPane('pm_area');
		});
	");
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
	?>
    <div class="round_heading">
        <div class="heading"><?php print JText::_('COM_EPOSTS_DEPART_OBJECTIVE'); ?></div>
        <br>
        <div class="objective"><?php print $objective; ?></div>
        <br>
    </div>
	<div class="department_left dp_list">
    	<div class="round_heading">
            <div class="heading"><?php print JText::_('COM_EPOSTS_PUBLIC_DOCUMENTS'); ?></div>
            <div class="details">
                <?php
                    $cirid = (int)JRequest::getString('cirid', 0);
                    
                    $where = ' WHERE dms_department = "' . $cirid . '" AND dms_access = "1" AND publish = "1" ';
                    
                    $query = "SELECT * FROM #__eposts_dms
                              $where
                              ORDER BY publishedon DESC
                              LIMIT 4";
                                                                                
                    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
                    $rows = $database->loadObjectList();
                ?>
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
                    <tr>
                        <th width="10%"><?php print JText::_('COM_EPOSTS_TYPE'); ?></th>
                        <th><?php print JText::_('COM_EPOSTS_NAME'); ?></th>
                        <th width="20%"><?php print JText::_('COM_EPOSTS_DATE'); ?></th>
                        <th width="22%"><?php print JText::_('COM_EPOSTS_MODIFIED_BY'); ?></th>
                    </tr>
                    <?php
                        if (count($rows)) :
                        $k = 0;
                        
                        for($i=0, $n=count( $rows ); $i < $n; $i++) {
                            $row = &$rows[$i];
                            $link = JRoute::_("index.php?option=com_eposts&view=mainshowdocuments&cirid=".$row->id);
                            
                            $admin_id = $row->publishedby;
                            $admin = JFactory::getUser($admin_id);
                            $admin = $admin->get('username');
                            
                            $filename = $row->dms_title;
                            $fileext = explode('.', $filename);
                            $fileext = $fileext[count($fileext)-1];
                            
                            $title = $row->dms_name;
                            if ($tag == 'ar-AA') :
                                $title = $row->dms_name_ar;
                            endif;
                            $title = strip_tags($title);
                            $length = 25;
                            if (mb_strlen($title) > $length) :
                                $title = mb_substr($title, 0, $length) . '...';
                            endif;
							$length = 10;
                            if (mb_strlen($admin) > $length) :
                               $admin = mb_substr($admin, 0, $length) . '..';
                            endif;
                    ?>
                    <tr>
                        <td class="img"><img src="<?php print JURI::base(); ?>templates/emiratespost/images/icons/<?php print jlist_HTML::getIcon($fileext); ?>" /></td>
                        <td><a href="<?php print $link; ?>"><?php print $title; ?></a></td>
                        <td><?php print JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></td>
                        <td><span><?php print $admin; ?></span></td>
                    </tr>
                    <?php $k = 1 - $k;  } ?>
                    <?php else : ?>
                    <tr>
                        <td colspan="4" align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
                    </tr>
                    <?php endif; ?>
                </table>
                <a href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistdocuments&Itemid=327&doc=1&deptid=' . $cirid); ?>" class="dp_readmore"><?php print JText::_('COM_EPOSTS_DOCUMENTS_VIEWALL'); ?></a>
                <div class="clear"></div>
            </div>
        </div>
        <?php
			
			$where = ' WHERE dms_department = "' . $cirid . '" AND dms_access = "0" AND publish = "1" AND dms_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = "' . $user->id . '")';
			
			$query = "SELECT * FROM #__eposts_dms
					  $where
					  ORDER BY publishedon DESC
					  LIMIT 4";
																		
			$database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
			$rows = $database->loadObjectList();
		?>
    	<div class="round_heading">
            <div class="heading"><?php print JText::_('COM_EPOSTS_PRIVATE_DOCUMENTS'); ?></div>
            <div class="details">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
                    <tr>
                        <th width="10%"><?php print JText::_('COM_EPOSTS_TYPE'); ?></th>
                        <th><?php print JText::_('COM_EPOSTS_NAME'); ?></th>
                        <th width="20%"><?php print JText::_('COM_EPOSTS_DATE'); ?></th>
                        <th width="22%"><?php print JText::_('COM_EPOSTS_MODIFIED_BY'); ?></th>
                    </tr>
                    <?php
                        if (count($rows)) :
                        $k = 0;
                        
                        for($i=0, $n=count( $rows ); $i < $n; $i++) {
                            $row = &$rows[$i];
                            $link = JRoute::_("index.php?option=com_eposts&view=mainshowdocuments&cirid=".$row->id. '&deptid=' . $cirid);
                            
                            $admin_id = $row->publishedby;
                            $admin = JFactory::getUser($admin_id);
                            $admin = $admin->get('username');
                            
                            $filename = $row->dms_title;
                            $fileext = explode('.', $filename);
                            $fileext = $fileext[count($fileext)-1];
                            
                            $title = $row->dms_name;
                            if ($tag == 'ar-AA') :
                                $title = $row->dms_name_ar;
                            endif;
                            $title = strip_tags($title);
                            $length = 25;
                            if (mb_strlen($title) > $length) :
                                $title = mb_substr($title, 0, $length) . '...';
                            endif;
							$length = 10;
                            if (mb_strlen($admin) > $length) :
                               $admin = mb_substr($admin, 0, $length) . '..';
                            endif;
                    ?>
                    <tr>
                        <td class="img"><img src="<?php print JURI::base(); ?>templates/emiratespost/images/icons/<?php print jlist_HTML::getIcon($fileext); ?>" /></td>
                        <td><a href="<?php print $link; ?>"><?php print $title; ?></a></td>
                        <td><?php print JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></td>
                        <td><span><?php print $admin; ?></span></td>
                    </tr>
                    <?php $k = 1 - $k;  } ?>
                    <?php else : ?>
                    <tr>
                        <td colspan="4" align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
                    </tr>
                    <?php endif; ?>
                </table>
                <a href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistdocuments&Itemid=327&doc=2&deptid=' . $cirid); ?>" class="dp_readmore"><?php print JText::_('COM_EPOSTS_DOCUMENTS_VIEWALL'); ?></a>
                <div class="clear"></div>
            </div>
        </div>
        <div class="ane_area" id="ane_area">
        	<div class="tabs_area">
            	<ul class="tabs">
                	<li class="tab"><span><?php print JText::_('COM_EPOSTS_ANNOUNCEMENTS'); ?></span></li>
                	<li class="tab"><span><?php print JText::_('COM_EPOSTS_NEWS'); ?></span></li>
                	<li class="tab"><span><?php print JText::_('COM_EPOSTS_EVENTS'); ?></span></li>
                </ul>
            </div>
            <div class="contents_area">
            	<div class="content">
                	<ul>
					<?php
					$where = ' WHERE (announcements_department = "' . $cirid . '" AND announcements_access = "0" AND announcements_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = "' . $user->id . '")) OR (announcements_department = "' . $cirid . '" AND announcements_access = "1") AND publish = "1"';
						 $query = "SELECT * FROM #__eposts_announcements
									$where
									ORDER BY id DESC
									LIMIT 5";
																					  
						   $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
						   $items = $database->loadObjectList();
						   if (count($items)) :
								foreach ($items as $key => $item) :  
						$anLinks = JRoute::_('index.php?option=com_eposts&view=mainshowannouncements&Itemid=117&cirid=' .$item->id . '&deptid=' . $cirid);
							  $title = $item->announcements_name;
							  if ($tag == 'ar-AA') :
								  $title = $item->announcements_name_ar;
							  endif;
							  $title = strip_tags($title);
							  $length = 55;
								if (mb_strlen($title) > $length) :
								   $title = strip_tags($title);
								   $title = mb_substr($title, 0, $length) . '...';
								endif;	
							
						?>
                        <li><a href="<?php print $anLinks;?>"><?php print $title;?></a></li>
                        <?php endforeach; ?>
						<?php else : ?>
                        <li style="background:none; text-align:center;"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></li>
                        <?php endif; ?>
                    </ul>
                    <a href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistannouncements&Itemid=117&deptid=' . $cirid); ?>" class="dp_readmore"><?php print JText::_('COM_EPOSTS_DOCUMENTS_VIEWALL'); ?></a>
                    <div class="clear"></div>
                </div>
            	<div class="content">
                	<ul>
                    	<?php
						
					$where = ' WHERE (news_department = "' . $cirid . '" AND news_access = "0" AND news_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = "' . $user->id . '")) OR (news_department = "' . $cirid . '" AND news_access = "1") AND publish = "1"';
						$query = "SELECT * FROM #__eposts_news
									$where
									ORDER BY id DESC
									LIMIT 5";
																					  
						   $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
						   $items = $database->loadObjectList();
						   if (count($items)) :
								foreach ($items as $key => $item) :  
						$anLinks = JRoute::_('index.php?option=com_eposts&view=mainshownews&Itemid=116&cirid=' .$item->id . '&deptid=' . $cirid);
							  $title = $item->news_name;
							  if ($tag == 'ar-AA') :
								  $title = $item->news_name_ar;
							  endif;
							  $title = strip_tags($title);
							  $length = 55;
								if (mb_strlen($title) > $length) :
								   $title = strip_tags($title);
								   $title = mb_substr($title, 0, $length) . '...';
								endif;
									
							if ($item->news_source != '') :
								$target = '_blank';
								$anLinks = strstr($item->news_source, ':') ? $item->news_source : 'http://' . $item->news_source;
							endif;
							if ($item->news_source_ar != '' && $tag == 'ar-AA') :
								$target = '_blank';
								$anLinks = strstr($item->news_source_ar, ':') ? $item->news_source_ar : 'http://' . $item->news_source_ar;
							endif;
						?>
                        <li><a href="<?php print $anLinks;?>" target="<?php print $target; ?>"><?php print $title;?></a></li>
                        <?php endforeach; ?>
						<?php else : ?>
                        <li style="background:none; text-align:center;"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></li>
                        <?php endif; ?>
                    </ul>
                    <a href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistnews&Itemid=116'); ?>" class="dp_readmore"><?php print JText::_('COM_EPOSTS_DOCUMENTS_VIEWALL'); ?></a>
                    <div class="clear"></div>
                </div>
                
				<?php
					$date =& JFactory::getDate();
                    $crDate = $date->toFormat();
					
                    $where = ' WHERE ((events_department = "' . $cirid . '" AND events_access = "0" AND events_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = "' . $user->id . '")) OR (events_department = "' . $cirid . '" AND events_access = "1")) AND publish = "1" AND events_startdate <= "' . $crDate . '" AND events_enddate >= "' . $crDate . '"';
                    
                    $query = "SELECT * FROM #__eposts_events
                              $where
                              ORDER BY publishedon DESC
                              LIMIT 5";
                    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
                    $rows = $database->loadObjectList();
                ?>
            	<div class="content">
                	<ul>
                    	<?php
						if (count($rows)) :
						$k = 0;
						for($i=0, $n=count( $rows ); $i < $n; $i++) {
							$row = &$rows[$i];
							$link = JRoute::_("index.php?option=com_eposts&view=mainshowevents&Itemid=253&cirid=".$row->id);
							
							$title = $row->events_title;
							
							if ($tag == 'ar-AA') :
								$title = $row->events_title_ar;
							endif;
							$title = strip_tags($title);
							$length = 55;
							if (mb_strlen($title) > $length) :
								$title = mb_substr($title, 0, $length) . '...';
							endif; 
							
							
						?>
                        <li><a href="<?php print $link; ?>"><?php print $title; ?></a></li>
                        <?php $k = 1 - $k;  } ?>
						<?php else : ?>
                        <li style="background:none; text-align:center;"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></li>
                        <?php endif; ?>
                    </ul>
                    <a href="<?php print JRoute::_("index.php?option=com_eposts&view=mainlistevents&Itemid=253&cirid=".$row->id); ?>" class="dp_readmore"><?php print JText::_('COM_EPOSTS_DOCUMENTS_VIEWALL'); ?></a>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
	<div class="department_right dp_list">
    	<div class="dp_warper">
            <div class="pm_area" id="pm_area">
                <div class="tabs_area">
                    <ul class="tabs">
                        <li class="tab"><span><?php print JText::_('COM_EPOSTS_POLICIES_N_PROCEDURES'); ?></span></li>
                        <li class="tab"><span><?php print JText::_('COM_EPOSTS_MANUALS_N_GUIDES'); ?></span></li>
                    </ul>
                </div>
                <div class="contents_area">
					<?php 
					$where = ' WHERE (policies_department = "' . $cirid . '" AND policies_access = "0" AND publish = "1" AND policies_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = "' . $user->id . '")) OR (policies_department = "' . $cirid . '" AND policies_access = "1" AND publish = "1")';
						$query = "SELECT * FROM #__eposts_policies
									$where
									ORDER BY policies_policydate DESC
									LIMIT 3";
																					  
						   $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
						   $items = $database->loadObjectList();
						?>
                        
                    <div class="content">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
                            <tr>
                                <th><?php print JText::_('COM_EPOSTS_TYPE'); ?></th>
                                <th><?php print JText::_('COM_EPOSTS_NAME'); ?></th>
                                <th><?php print JText::_('COM_EPOSTS_MODIFIED_BY'); ?></th>
                            </tr>
                            <?php
							if (count($items)) :
                            foreach ($items as $key => $item) :  
							
								$admin_id = $item->publishedby;
                                $admin = JFactory::getUser($admin_id);
								$admin = $admin->get('username');
						
								$filename = $item->policies_doc;
								$fileext = explode('.', $filename);
								$fileext = $fileext[count($fileext)-1];
									
								$anLinks = JRoute::_('index.php?option=com_eposts&view=mainshowpolicies&Itemid=395&cirid=' .$item->id.'&deptid=' . $cirid);
							  $title = $item->policies_name;
							  if ($tag == 'ar-AA') :
								  $title = $item->policies_name_ar;
							  endif;
							  $title = strip_tags($title);
							  $length = 25;
								if (mb_strlen($title) > $length) :
								   $title = strip_tags($title);
								   $title = mb_substr($title, 0, $length) . '...';
								endif;
								
								$length = 10;
                                if (mb_strlen($admin) > $length) :
                                    $admin = mb_substr($admin, 0, $length) . '..';
                                endif;
							?>
                            <tr>
                                <td class="img"><img src="<?php print JURI::base(); ?>templates/emiratespost/images/icons/<?php print jlist_HTML::getIcon($fileext); ?>" /></td>
                                <td><a href="<?php print $anLinks; ?>"><?php print $title; ?></a></td>
                                <td><span><?php print $admin; ?></span></td>
                            </tr>
                            <?php endforeach; ?>
                            <?php else : ?>
                            <tr>
                                <td colspan="3" align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
                            </tr>
                            <?php endif; ?>
                        </table>
                        <a href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistpolicies&Itemid=395&deptid=' . $cirid); ?>" class="dp_readmore"><?php print JText::_('COM_EPOSTS_DOCUMENTS_VIEWALL'); ?></a>
                        <div class="clear"></div>
                    </div>
					<?php
					$where = ' WHERE (manuals_department = "' . $cirid . '" AND manuals_access = "0" AND publish = "1" AND manuals_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = "' . $user->id . '")) OR (manuals_department = "' . $cirid . '" AND manuals_access = "1" AND publish = "1")';
						$query = "SELECT * FROM #__eposts_manuals
									$where
									ORDER BY id DESC
									LIMIT 3";
																					  
						   $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
						   $items = $database->loadObjectList();
						?>
                    <div class="content">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
                            <tr>
                                <th><?php print JText::_('COM_EPOSTS_TYPE'); ?></th>
                                <th><?php print JText::_('COM_EPOSTS_NAME'); ?></th>
                                <th><?php print JText::_('COM_EPOSTS_MODIFIED_BY'); ?></th>
                            </tr>
							<?php
							if (count($items)) :
                            foreach ($items as $key => $item) :  
							
								$admin_id = $item->publishedby;
                                $admin = JFactory::getUser($admin_id);
								$admin = $admin->get('username');
						
								$filename = $item->manuals_doc;
								$fileext = explode('.', $filename);
								$fileext = $fileext[count($fileext)-1];
									
								$anLinks = JRoute::_('index.php?option=com_eposts&view=mainshowmanuals&Itemid=341&cirid=' . $item->id . '&deptid=' . $cirid);
							  $title = $item->manuals_name;
							  if ($tag == 'ar-AA') :
								  $title = $item->manuals_name_ar;
							  endif;
							  $title = strip_tags($title);
							  $length = 25;
								if (mb_strlen($title) > $length) :
								   $title = strip_tags($title);
								   $title = mb_substr($title, 0, $length) . '...';
								endif;
								$length = 10;
                                if (mb_strlen($admin) > $length) :
                                    $admin = mb_substr($admin, 0, $length) . '..';
                                endif;
							?>
                            <tr>
                                <td class="img"><img src="<?php print JURI::base(); ?>templates/emiratespost/images/icons/<?php print jlist_HTML::getIcon($fileext); ?>" /></td>
                                <td><a href="<?php print $anLinks; ?>"><?php print $title; ?></a></td>
                                <td><span><?php print $admin; ?></span></td>
                            </tr>
                            <?php endforeach; ?>
                            <?php else : ?>
                            <tr>
                                <td colspan="3" align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
                            </tr>
                            <?php endif; ?>
                        </table>
                        <a href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistmanuals&Itemid=341&deptid=' . $cirid); ?>" class="dp_readmore"><?php print JText::_('COM_EPOSTS_DOCUMENTS_VIEWALL'); ?></a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
			
			$where = ' WHERE publish = "1" AND ((circulars_department = "' . $cirid . '" AND circulars_access = "0" AND circulars_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = "' . $user->id . '")) OR (circulars_department = "' . $cirid . '" AND circulars_access = "1"))';
			
			$query = "SELECT * FROM #__eposts_circulars
					  $where
					  ORDER BY publishedon DESC
					  LIMIT 5";
																		
			$database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
			$rows = $database->loadObjectList();
		?>
        <div class="dp_warper">
            <div class="circulars_area">
                <h3><?php print JText::_('COM_EPOSTS_CIRCULARS'); ?></h3>
                <div class="contents_area">
                	<div class="content">
                        <ul>
                            <?php
							if (count($rows)) :
                            $k = 0;
                            for($i=0, $n=count( $rows ); $i < $n; $i++) {
                                $row = &$rows[$i];
                                $link = JRoute::_("index.php?option=com_eposts&view=mainshowcirculars&Itemid=119&cirid=".$row->id);
                                
                                $title = $row->circulars_name;
                                
                                if ($tag == 'ar-AA') :
                                    $title = $row->circulars_name_ar;
                                endif;
                                $title = strip_tags($title);
                                $length = 45;
                                if (mb_strlen($title) > $length) :
                                    $title = mb_substr($title, 0, $length) . '...';
                                endif; 
                                
                            ?>
                            <li><a href="<?php print $link; ?>"><?php print $title; ?></a></li>
                            <?php $k = 1 - $k;  } ?>
                            <?php else : ?>
                            <li style="background:none; text-align:center;"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></li>
                            <?php endif; ?>
                        </ul>
                        <a href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistcirculars&Itemid=119&deptid=' . $cirid); ?>" class="dp_readmore"><?php print JText::_('COM_EPOSTS_DOCUMENTS_VIEWALL'); ?></a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
          
		?>
        <div class="dp_warper">
            <div class="pictures_area">
                <h3><?php print JText::_('COM_EPOSTS_PICTURE_GALLERY'); ?></h3>
                <div class="contents_area" style="height:200px;">
                	<style>
.ImageRotator{
    width:290px;
    height:160px;
    /*border:1px solid black;*/
    clear: both;
    overflow: hidden;
    position:absolute;
    display:inline;
}

.ImageRotator img.first{
width:100px;
height:75px;
display: inline;
left: 20px;
top: 45px;
visibility: visible;
position: absolute;
z-index: 2;
opacity: 0.4;
}
.ImageRotator img.second{
    width:120px;
    height:120px;

display: inline;
left: 40px;
top: 20px;
visibility: visible;
position: absolute;
z-index: 4;
opacity: 0.8;
}
.ImageRotator img.pushClass,
.ImageRotator img.third{
    width:160px;
    height:160px;

display: inline;
left: 65px;
top: 0px;
visibility: visible;
position: absolute;
z-index: 6;
opacity: 1;
}
.ImageRotator img.forth{
    width:120px;
    height:120px;
display: inline;
right: 40px;
top: 20px;
visibility: visible;
position: absolute;
z-index: 4;
opacity: 0.8;
}
.ImageRotator img.fifth{
    width:100px;
    height:75px;
display: inline;
right: 20px;
top: 45px;
visibility: visible;
position: absolute;
z-index: 2;
opacity: 0.4;
}
</style>
</style>
<?php
//var delaytoscrol = 2000; 2000 = 2second


$document->addScriptDeclaration("
        var selectedImage = 0;
        var totalImages = 0;
        var delaytoscrol = 2000;
        function runAcroSlider(sliderClass){
             totalImages = document.getElementById(sliderClass).getElementsByTagName('a').length;
             if(totalImages > 0){
                 if(totalImages > 1){
                     var seleage = Math.round(totalImages/2);
                 }   
                 selectImage(seleage);             
             }
               return false;
        }
       
        function moverSliNext(){
            selectImage(selectedImage+1);
        return false;
        }
        function moverSliPre(){
            selectImage(selectedImage-1);
        return false;
        }
       
        function displySliPic(myElmt, myVl){
            if(document.getElementById('sl-'+myElmt)){
                document.getElementById('sl-'+myElmt).className = myVl;
              }
              return false;           
        }
       
        function selectImage(selectMyImage){
           
              if(selectMyImage == selectedImage){
                  return true;
              }
              if(selectMyImage >= 0 && selectMyImage <= totalImages){
                selectedImage = selectMyImage;
              }else{
                return false; 
              }
             
              displySliPic(selectedImage, 'third');
              displySliPic(selectedImage-1, 'second');
              displySliPic(selectedImage-2, 'first');
              displySliPic(selectedImage+1, 'forth');
              displySliPic(selectedImage+2, 'fifth');
              if(selectedImage < 1){
                  document.getElementById('prev').style.display='none';
              }else{
                 document.getElementById('prev').style.display='block';
              }
             
              if(selectedImage+1 >= totalImages ){
                  document.getElementById('nextv').style.display='none';
              }else{
                 document.getElementById('nextv').style.display='block';
              }
             
              return false;
            }
       
        window.addEvent('domready',function(){
                   runAcroSlider('ImageRotator');
                });
               
               
    ");
?>  

<table width="100%" cellpadding="0" cellpadding="0">
<tr>
<td valign="middle">
	<?php if ($tag == 'ar-AA') : ?>
    	<a  id='nextv' href="#"  onClick="javaScript: return moverSliNext();" ><img src="<?php print JURI::base(true) . '/templates/emiratespost/images/gallery_arrow_right.png'; ?>" /></a>
    <?php else : ?>
    	<a id='prev' href="#" onClick="javaScript: return moverSliPre();"><img src="<?php print JURI::base(true) . '/templates/emiratespost/images/gallery_arrow_left.png'; ?>" /></a>
    <?php endif; ?>
</td>
<td width="385px" height="160px" valign="top">
<div id="ImageRotator" class="ImageRotator">
<?php
$config =&JFactory::getConfig();
                $filepath = $config->getValue( 'config.file_path' );
				 $reverse_file_path = $config->getValue( 'config.reverse_file_path' );
                 $reverse_filepath_storage = $config->getValue( 'config.reverse_filepath_storage' );
                        $where = '  WHERE parent = "1" AND `department` = "'.$deptid.'" AND published = "1"';
                       
                        $query = "SELECT * FROM #__igallery
                                  $where
                                  ORDER BY date ASC
                                  LIMIT 10";
                       
                        $database->SetQuery($query);
                        $rows = $database->loadObjectList();
						$totalImages = count($rows);
                        $k = 0;
                        for($i=0, $n=count( $rows ); $i < $n; $i++) {
                            $row = &$rows[$i];
							//$link = JRoute::_("index.php?option=com_igallery&view=category&page=home&Itemid=134&igid=".$row->id);
                            $link = JRoute::_("index.php?option=com_igallery&view=category&Itemid=134&igid=".$row->id);
                           
                            $title = $row->name;
							$mImag = $row->menu_image_filename;
                           
                       		if ($mImag == '') :
                                $where = 'WHERE gallery_id = "'.$row->id.'" AND published = "1"';
                           
                                $query = "SELECT * FROM #__igallery_img
                                      $where
                                      ORDER BY date ASC
                                      LIMIT 1";
                           
                                $database->SetQuery($query);
                                $row = $database->loadObject();
                            	$image = $row->filename;
							else :
								$image = $mImag;
                          	endif;
                            $name = substr($image, 0, strrpos($image, '.'));
                            $extnt = substr($image, strrpos($image, '.'));
							
							//$img = $filepath.'\\'.$image;
							$img = $reverse_filepath_storage.$image;
			$img = str_replace("\\", "/", $img);
			
			//$filetype=string;       
	   //$imgbinary = fread(fopen($img, "r"), filesize($img));
                        
		//$aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
		$aaa= $img;
						?>

<a href="<?php print $link; ?>" onClick="javaScript: return selectImage(<?php echo $i;?>);">
<?php 
$img = JURI::base(true).'/index.php?option=com_eposts&view=loadimage&myfile='.$reverse_filepath_storage.'images\\igallery\\original\\1-100\\'.$name.$extnt;
$img = $reverse_filepath_storage.'images\\igallery\\original\\1-100\\'.$name.$extnt;
$img = str_replace("\\", "/", $img);
	$img = $reverse_filepath_storage.$image.$extnt;
			$img = str_replace("\\", "/", $img);		
?>
  <img id="sl-<?php echo $i;?>" src="<?php print $aaa; ?>"  class="pushClass"/>
</a>
<?php $k = 1 - $k;  } ?>
</div>
<div class="clear"></div>   
</td>
<td valign="middle">
	<?php if ($tag == 'ar-AA') : ?>
    	<a id='prev' href="#" onClick="javaScript: return moverSliPre();"><img src="<?php print  JURI::base(true) . '/templates/emiratespost/images/gallery_arrow_left.png'; ?>" /></a>
    <?php else : ?>
    	<a  id='nextv' href="#"  onClick="javaScript: return moverSliNext();" ><img src="<?php print JURI::base(true) . '/templates/emiratespost/images/gallery_arrow_right.png'; ?>" /></a>
    <?php endif; ?>
</td>
</tr>
</table>  
<?php if ($totalImages <= 1) {?>
<style>
#prev img, #nextv img { display:none;}
#prev, #nextv {width:10px; display:block;}
</style>
<?php } ?>
                	 <div class="controlls" style="position:static;text-align: center;">
                                                <div class="view_all">
												 <!--<a href="<?php //print JRoute::_('index.php?option=com_igallery&view=category&page=home&igid=1&Itemid=403'); ?>">View All Albums</a>-->
                                                 <a href="<?php print JRoute::_('index.php?option=com_igallery&view=category&page=category&igid=1&Itemid=134'); ?>">View All Albums</a>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                </div>
            </div>
        </div>
    </div>
	<div class="clear"></div>
	<?php
	//echo $footer; 
			      
}//end funtion myDepartment 

//departmentList
function departmentList($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$resources_offices = (int)JRequest::getString('resources_offices', 0);
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MYDEPARTMENTS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MYDEPARTMENTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
        
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	
	$where = ' WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = "' . $user->id . '")';
	
	$query = "SELECT * FROM #__eposts_departments
              $where
              ORDER BY departments_name ASC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
	
	?>
    <div class="heading"><?php echo JText::_('COM_EPOSTS_BACKEND_MYDEPARTMENT_LIST_NAME_TITLE')." "; ?></div>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
        <!--<tr>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NAME_TITLE')." "; ?></th>
        </tr> -->
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->id);
			//                       'index.php?option=com_eposts&view=newcircular&cid='.$row->id;uploadForm			
			?>
            <tr class="<?php echo "row$k"; ?>">
                <td valign="top">
                <a href="<?php echo $link; ?>"><?php echo $row->departments_name; ?></a>
                </td>
            </tr>
        <?php $k = 1 - $k;  } ?>
    </table>
	
	
	<?php
	
	$form = "<br>";
	echo $form;
	
    $html_form = makeHeader(JText::_('COM_EPOSTS_OTHER_DEPARTMENTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	
	$where = ' WHERE id NOT IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = "' . $user->id . '")';
	
	$query = "SELECT * FROM #__eposts_departments
              $where
              ORDER BY departments_name ASC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
	
	?>
    <div class="heading"><?php echo JText::_('COM_EPOSTS_BACKEND_OTDEPARTMENT_LIST_NAME_TITLE')." "; ?></div>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
        <!--<tr>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NAME_TITLE')." "; ?></th>
        </tr> -->
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->id);
			//                       'index.php?option=com_eposts&view=newcircular&cid='.$row->id;uploadForm
								
            ?>
            <tr class="<?php echo "row$k"; ?>">
                <td valign="top">
<?php if ($tag == 'ar-AA') { ?>
				               
			   <a href="<?php echo $link; ?>"><?php echo $row->departments_name_ar; ?></a>
<?php } else{ ?> 
 <a href="<?php echo $link; ?>"><?php echo $row->departments_name; ?></a>
<?php }  ?>         
		 </td>
            </tr>
        <?php $k = 1 - $k;  } ?>
    </table>
	<?php
	
	//echo $footer;
			      
}//end funtion departmentList  

//new Circular
function newCircular($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    ///////////////////////////////
$document =& JFactory::getDocument();
JHTML::_( 'behavior.mootools' );
$cf_url = JURI::root().'components/com_chronoforms/js/datepicker_moo/';
$tag = 'en-US';
$lang =& JFactory::getLanguage();
$lang_tag = $lang->getTag();
if ( !$lang_tag ) {
  $lang_tag = $lang->getDefault();
}
if ( file_exists($cf_url.'Locale.'.$lang_tag.'.DatePicker.js') ) {
  $tag = $lang_tag;
}
$document->addScript( $cf_url.'Locale.'.$tag.'.DatePicker.js' );
///////////////////////////////////////////
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_CIRCULAR_NEW_PAGE_TITLE'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);
	
	 $row = new jlist_circulars( $database );
    $row->load( $cirid );
    $cdipartment = array();
    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'circulars_department',
        'class="inputbox" ', 'value', 'text', $row->circulars_department);
		
	
	$cdAccess[] = JHTML::_('select.option',0,'No');
	$cdAccess[] = JHTML::_('select.option',1,'Yes');
	$departmentAccess =	JHTML::_('select.genericlist',$cdAccess, 'circulars_access',
        'class="inputbox" ', 'value', 'text', $row->circulars_access);
	
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);
		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_CIRCULAR_NEW_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.circulars_name.value == "" 
			 || form.circulars_name_ar.value == "" 
			 || form.circulars_date.value == "" 
<?php if(!$row->id){?>
			 || form.circulars_file.value == "" 
			 || form.circulars_file_ar.value == ""
<?php }?>
 ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php 
	$link = JRoute::_("index.php?option=com_eposts&view=newcircular&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Circular') : JText::_('Edit Circular');
	$length = 100;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newcircular'));
?>
   <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" name="adminForm" enctype="multipart/form-data" onsubmit="return validateform();">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_CIRCULARS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_CIRCULARS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="circulars_name" value="<?php echo htmlspecialchars($row->circulars_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="circulars_name_ar" value="<?php echo htmlspecialchars($row->circulars_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_DESC_TITLE')." "; ?>:</strong><td><td>
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'circulars_description',  @$row->circulars_description , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_DESC_TITLE_AR')." "; ?>:</strong><td><td>
                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'circulars_description_ar',  @$row->circulars_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?>:</strong><td><td>
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULAR_DATE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php
									// for plugin
		                            //JPluginHelper::importPlugin('datepicker');	
	                                //echo plgSystemDatePicker::calendar($row->circulars_date, 'circulars_date', 'circulars_date', '%Y-%m-%d %H:%M:%S');
									 ?>
                                 <?php JHTML::_('behavior.calendar'); ?>
                                 <input class="inputbox" type="text" name="circulars_date" id="circulars_date"  value="<?php echo strtotime($row->circulars_date) ? date('d-m-Y', strtotime(htmlspecialchars($row->circulars_date, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('circulars_date','%d-%m-%Y');"/>
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?>:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('Select file (English)')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="circulars_file" id="circulars_file" type="file" <?php print $row->circulars_document != '' ? 'style="display:none;"' :''; ?>/>
                                    <?php if ($row->circulars_document != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="circulars_file_nm"><?php print $row->circulars_document; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('circulars_file', 'circulars_file_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_FILE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="circulars_file_ar" id="circulars_file_ar" type="file" <?php print $row->circulars_document_ar != '' ? 'style="display:none;"' :''; ?>/>
                                    <?php if ($row->circulars_document_ar != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="circulars_file_nm_ar"><?php print $row->circulars_document_ar; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('circulars_file_ar', 'circulars_file_nm_ar');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?>:</strong><td><td>
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.circulars" />
    </form>
<?php
	
	$form = "<br />";
		echo $form;
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newCircular 

//listCirculars
function listCirculars($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$resources_offices = (int)JRequest::getString('resources_offices', 0);
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_CIRCULARS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_CIRCULARS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 
	
//////////////////////////////////////////////////

    $where = 'WHERE 1 = 1 && (circulars_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'") || publishedby = "'.$user->id.'")';
  

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= "AND ( LOWER(circulars_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(circulars_description)) LIKE '%".$search."%' )";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_circulars");
    $total = $database->loadResult();      
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_circulars
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->circulars_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    } 
	$newlink = JRoute::_("index.php?option=com_eposts&view=newcircular");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Circular</a>
<div class="clear"></div>
	    
        <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_DESC_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULAR_DEPARTMENT_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULAR_DATE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('Actions')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=newcircular&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newcircular&task=delcircular&cirid=".$row->id);
			//                       'index.php?option=com_eposts&view=newcircular&cid='.$row->id;uploadForm
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left">
            <a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_EDIT_TITLE');?>"><?php echo $row->circulars_name; ?></a>
           
            </td>
            <td valign="top" align="left"><?php echo $row->circulars_description; ?></td>
            <td valign="top" align="left"><?php echo $row->department; ?></td>
            <td valign="top" align="left"><?php echo JText::sprintf('%s', JHtml::_('date', $row->circulars_date, JText::_('DATE_FORMAT_LC5'))); ?></td>
            <td valign="top" align="left"> <a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php //echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php //echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	
	//print_r($user->id);
	
	/*$form = "<div>Welcome to list Circulars</div>";
		echo $form;*/
	
	
	//echo $footer; 
			      
}//end funtion listCirculars 


//new Hires
function newHires($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_CIRCULAR_NEW_PAGE_TITLE'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);
	
	$row = new jlist_newhires($database);
    $row->load( $cirid );
   $cdipartment = array();
 
/*    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'newhires_department',
        'class="inputbox" ', 'value', 'text', $row->newhires_department);*/
		
	
	/*$cdAccess[] = JHTML::_('select.option',0,'No');
	$cdAccess[] = JHTML::_('select.option',1,'Yes');
	$departmentAccess =	JHTML::_('select.genericlist',$cdAccess, 'newhires_access',
        'class="inputbox" ', 'value', 'text', $row->newhires_access ? $row->newhires_access : 1);*/
		
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);	
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('New Hires'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	?>
	<script type="text/Javascript" language="JavaScript">
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.newhires_name.value == "" 
			 || form.newhires_name_ar.value == ""
			 || form.newhires_designation.value == ""
			 || form.newhires_designation_ar.value == ""			 
<?php if(!$row->id){?>
 || form.newhires_file.value == ""
<?php }?>){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
			if(form.newhires_email.value != ""){
				var x=form.newhires_email.value;
				var atpos=x.indexOf("@");
				var dotpos=x.lastIndexOf(".");
				if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
				  {
				  alert("Please enter a valid email address");
				  return false;
				  }
			}
			
			var mobnm = /^\d+$/;
			if (mobnm.test(form.newhires_mobile.value) != true) {
				//alert("Please enter a valid number");
				//return false;
			}
            return true;
        }
    }
    </script>
<?php 
	$link = JRoute::_("index.php?option=com_eposts&view=newhires&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New New Hire') :  JText::_('Edit New Hire');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newhires'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_NEWHIRES_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_NEWHIRES_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable"  width="100%">
                             <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="newhires_name" value="<?php echo htmlspecialchars($row->newhires_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="newhires_name_ar" value="<?php echo htmlspecialchars($row->newhires_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_DESIGNETION_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="newhires_designation" value="<?php echo htmlspecialchars($row->newhires_designation, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_DESIGNETION_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="newhires_designation_ar" value="<?php echo htmlspecialchars($row->newhires_designation_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_MOBILE_TITLE')." "; ?>:</strong><td><td>
                                    <input name="newhires_mobile" value="<?php echo htmlspecialchars($row->newhires_mobile, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_EMAIL_TITLE')." "; ?>:</strong><td><td>
                                    <input name="newhires_email" value="<?php echo htmlspecialchars($row->newhires_email, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_DESC_TITLE').""; ?>:</strong><td><td>
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'newhires_description',  @$row->newhires_description , '100%', '10', '80', '5', false, false ) ;
								?> 
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_DESC_TITLE_AR').""; ?>:</strong><td><td>
                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'newhires_description_ar',  @$row->newhires_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <!--<tr>
                                <td align="right"><strong><?php //echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?>:</strong><td><td>
                                    <?php //echo $departmentAccess;?>
                                                                 
                            </tr> -->
                            <!--<tr>
                                <td align="right"><strong><?php //echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?>:</strong><td><td>
                                    <?php //echo $departmentList;?>
                                                                 
                            </tr>-->
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_FILE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="newhires_file" id="newhires_file" type="file" <?php print $row->newhires_picture != '' ? 'style="display:none"' : ''; ?>/>
                                    <?php if ($row->newhires_picture != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="newhires_file_nm"><?php print $row->newhires_picture; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('newhires_file', 'newhires_file_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?>:</strong><td><td>
                                    <?php echo $isPublist;?>                      
                            </tr>
                            <tr>
                                <td align="right"><strong>Expiry Date:</strong><td><td>
                                  <?php
								JHTML::_('behavior.calendar');
								 ?>
                                 <input class="inputbox" type="text" name="expirydate" id="expirydate"  value="<?php echo strtotime($row->expirydate) ? date('d-m-Y', strtotime(htmlspecialchars($row->expirydate, ENT_QUOTES))) : date('d-m-Y',time()); ?>" onClick="return showCalendar('expirydate','%d-%m-%Y');"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                <?php
                                $publishedon_date = ($row->publishedon) ? date('d-m-Y H:i:s', strtotime($row->publishedon)) : date('d-m-Y H:i:s');
								?>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php print $publishedon_date; ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.newhires" />
    </form>
<?php
	
	//$form = "<div>Welcome to new Circular</div>";
		//echo $form;
	
	
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newNewHires 

//listNewHires
function listNewHires($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_NEWHIRES_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_NEWHIRES_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

 //$where = 'WHERE 1 = 1 && (newhires_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'") || publishedby = "'.$user->id.'")';
  $where = 'WHERE 1 = 1 ';

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(newhires_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(newhires_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_mobile) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_email) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(newhires_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(newhires_description_ar)) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_designation) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_designation_ar) LIKE '%".$search."%' )";
    }    
   	
	$database->SetQuery( "SELECT count(*) FROM #__eposts_newhires");
    $total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_newhires
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    /*foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->newhires_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    } */   
 
	$newlink = JRoute::_("index.php?option=com_eposts&view=newhires");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Hire</a>
<div class="clear"></div>
	    
        <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_DESIGNETION_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_MOBILE_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_EMAIL_TITLE')." "; ?></th> 
            <th valign="top" align="left" class="title"><?php echo JText::_('Publish')." "; ?></th>   
            <th valign="top" align="left" class="title"><?php echo JText::_('Actions')." "; ?></th>            
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			 $link = JRoute::_("index.php?option=com_eposts&view=newhires&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newhires&task=delnewhires&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left">
            <a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_EDIT_TITLE');?>"><?php echo $row->newhires_name; ?></a>
            
            </td>
            <td valign="top" align="left"><?php echo $row->newhires_designation; ?></td>
            <td valign="top" align="left"><?php echo $row->newhires_mobile; ?></td>
            <td valign="top" align="left"><?php echo $row->newhires_email; ?></td>
            <td valign="top" align="left"><?php 
			echo ($row->publish == 1)?'Yes':'No'; 
			?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listNewHires

//new Tasawaq
function newTasawaq($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_TASAWAQ_NEW_PAGE_TITLE'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);
	
    $row = new jlist_tasawaqs( $database );
    $row->load( $cirid );
    $cdipartment = array();
    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'tasawaqs_department',
        'class="inputbox" ', 'value', 'text', $row->tasawaqs_department);
		
	
	$cdAccess[] = JHTML::_('select.option',0,'No');
	$cdAccess[] = JHTML::_('select.option',1,'Yes');
	$departmentAccess =	JHTML::_('select.genericlist',$cdAccess, 'tasawaqs_access',
        'class="inputbox" ', 'value', 'text', $row->tasawaqs_access);
		
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);
		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_CIRCULAR_NEW_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">                          
	function deletefile(filefield, filename, getUpdate) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		document.getElementById(getUpdate).value = '1';
		return false;
	}
	
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.tasawaqs_name.value == "" 
			 || form.tasawaqs_name_ar.value == ""
			 || tinyMCE.get('tasawaqs_description').getContent()== ""
			 || tinyMCE.get('tasawaqs_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=newtasqwaq&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Tasawaq') :  JText::_('Edit Tasawaq');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newtasawaq'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TASAWAQS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_TASAWAQS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="tasawaqs_name" value="<?php echo htmlspecialchars($row->tasawaqs_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="tasawaqs_name_ar" value="<?php echo htmlspecialchars($row->tasawaqs_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'tasawaqs_description',  @$row->tasawaqs_description , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'tasawaqs_description_ar',  @$row->tasawaqs_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <!--<tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?>:</strong><td><td>
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_PRICE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="tasawaqs_price" value="<?php echo htmlspecialchars($row->tasawaqs_price, ENT_QUOTES); ?>">
                                                                 
                            </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?>:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr> -->
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?>:</strong><td><td>
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('Tasawaq Partner Logo')." "; ?>:</strong><td><td>
                                    <input name="tasawaqs_p_logo" id="tasawaqs_p_logo" type="file" <?php print $row->tasawaqs_p_logo != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->tasawaqs_p_logo != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="tasawaqs_p_logo_nm"><?php print $row->tasawaqs_p_logo; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('tasawaqs_p_logo', 'tasawaqs_p_logo_nm','del_tasawaqs_p_logo');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                    <input name="del_tasawaqs_p_logo" id="del_tasawaqs_p_logo" type="hidden" value="0"/>                             
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('Tasawaq Partner Name')." "; ?>:</strong><td><td>
                                    <input type="text" name="tasawaq_p_name" id="tasawaq_p_name" value="<?php echo htmlspecialchars($row->tasawaq_p_name, ENT_QUOTES); ?>" size="80" maxlength="150" />
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('Tasawaq Contact Details (English)')." "; ?>:</strong><td><td>
                                	<?php 
									$editor3 =& JFactory::getEditor();
									echo $editor3->display( 'tasawaq_contact',  @$row->tasawaq_contact , '100%', '10', '80', '5', false, false ) ;
									?>
                                                                 
                            </tr>
							 <tr>
                                <td align="right"><strong><?php echo JText::_("Tasawaq Contact Details (Arabic)")." "; ?>:</strong><td><td>
                                	<?php 
									$editor3 =& JFactory::getEditor();
									echo $editor3->display( 'tasawaq_contact_ar',  @$row->tasawaq_contact_ar , '100%', '10', '80', '5', false, false ) ;
									?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('Valid Until')." "; ?>:</strong><td><td>
                                  <?php
								JHTML::_('behavior.calendar');
								 ?>
                                 <input class="inputbox" type="text" name="tasawaqs_expiry" id="tasawaqs_expiry"  value="<?php echo strtotime($row->tasawaqs_expiry) ? date('d-m-Y', strtotime(htmlspecialchars($row->tasawaqs_expiry, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('tasawaqs_expiry','%d-%m-%Y');"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('Tasawaq Image')." "; ?>:</strong><td><td>
                                    <input name="tasawaqs_image" id="tasawaqs_image" type="file" <?php print $row->tasawaqs_image != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->tasawaqs_image != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="tasawaqs_image_nm"><?php print $row->tasawaqs_image; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('tasawaqs_image', 'tasawaqs_image_nm','del_tasawaqs_image');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                   <input name="del_tasawaqs_image" id="del_tasawaqs_image" type="hidden" value="0"/>                               
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('Tasawaq Attachment')." "; ?>:</strong><td><td>
                                    <input name="tasawaqs_attachment" id="tasawaqs_attachment" type="file" <?php print $row->tasawaqs_attachment != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->tasawaqs_attachment != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="tasawaqs_attachment_nm"><?php print $row->tasawaqs_attachment; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('tasawaqs_attachment', 'tasawaqs_attachment_nm','del_tasawaqs_attachment');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                     <input name="del_tasawaqs_attachment" id="del_tasawaqs_attachment" type="hidden" value="0"/>                             
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.newtasawaq" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
//echo $footer; 
			      
}//end funtion newTasawaq 

//listTasawaqs
function listTasawaqs($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_TASAWAQS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_TASAWAQS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    //$where = 'WHERE 1 = 1 && (tasawaqs_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'") || publishedby = "'.$user->id.'")';
	
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(tasawaqs_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(tasawaqs_description)) LIKE '%".$search."%') ";
    }    
   	$database->SetQuery( "SELECT count(*) FROM #__eposts_tasawaqs");
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_tasawaqs
              $where
              ORDER BY id DESC";
                      //print $query;                                          
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->tasawaqs_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    }    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newtasawaq");   
	//====================== Tasawaq List
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Tasawaq</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="4">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_DESC_TITLE')." "; ?></th>
            <!--<th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_DEPARTMENT_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_PRICE')." "; ?></th> -->
            <th valign="top" align="left" class="title"><?php echo JText::_('Actions')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=newtasawaq&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newtasawaq&task=delnewtasawaq&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left">
            <a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_EDIT_TITLE');?>"><?php echo $row->tasawaqs_name; ?></a>
            
            </td>
            <td valign="top" align="left"><?php echo $row->tasawaqs_description; ?></td>
            <!--<td valign="top" align="left"><?php echo $row->department; ?></td>
            <td valign="top" align="left"><?php echo $row->tasawaqs_price; ?></td> -->
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listTasawaqs

//new Event
function newEvent($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
		///////////////////////////////
$document =& JFactory::getDocument();
JHTML::_( 'behavior.mootools' );
$cf_url = JURI::root().'components/com_chronoforms/js/datepicker_moo/';
$tag = 'en-US';
$lang =& JFactory::getLanguage();
$lang_tag = $lang->getTag();
if ( !$lang_tag ) {
  $lang_tag = $lang->getDefault();
}
if ( file_exists($cf_url.'Locale.'.$lang_tag.'.DatePicker.js') ) {
  $tag = $lang_tag;
}
$document->addScript( $cf_url.'Locale.'.$tag.'.DatePicker.js' );
///////////////////////////////////////////
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_EVENTS_NEW_PAGE_TITLE'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);
	
      $row = new jlist_events( $database );
    $row->load( $cirid );
    $cdipartment = array();
   $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'events_department',
        'class="inputbox" ', 'value', 'text', $row->events_department);
		
	
	$cdAccess[] = JHTML::_('select.option',0,'No');
	$cdAccess[] = JHTML::_('select.option',1,'Yes');
	$departmentAccess =	JHTML::_('select.genericlist',$cdAccess, 'events_access',
        'class="inputbox" ', 'value', 'text', $row->events_access);
    
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);

		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_EVENTS_NEW_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">                            
	function deletefile(filefield, filename, getUpdate) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		document.getElementById(getUpdate).value = '1';
		return false;
	}
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.events_title.value == "" 
			 || form.events_title_ar.value == "" 
			 || form.events_location.value == "" 
			 || form.events_location_ar.value == "" 
			 || form.events_organized_by.value == ""
			 || form.events_organized_by_ar.value == ""
			 || tinyMCE.get('events_description').getContent()== ""
			 || tinyMCE.get('events_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php
	$link = JRoute::_("index.php?option=com_eposts&view=newevent&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Event') :  JText::_('Edit Event');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newevent'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_EVENTS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_EVENTS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="events_title" value="<?php echo htmlspecialchars($row->events_title, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="events_title_ar" value="<?php echo htmlspecialchars($row->events_title_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LOCATION_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="events_location" value="<?php echo htmlspecialchars($row->events_location, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LOCATION_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="events_location_ar" value="<?php echo htmlspecialchars($row->events_location_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_ORGENIZED_BY_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="events_organized_by" value="<?php echo htmlspecialchars($row->events_organized_by, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_ORGENIZED_BY_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="events_organized_by_ar" value="<?php echo htmlspecialchars($row->events_organized_by_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('Event Logo').""; ?>:</strong><td><td>
                                    <input name="event_logo" id="event_logo" type="file" <?php print $row->event_logo != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->event_logo != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="event_logo_nm"><?php print $row->event_logo; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('event_logo', 'event_logo_nm','del_event_logo');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                    <input name="del_event_logo" id="del_event_logo" type="hidden" value="0"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('Event Image').""; ?>:</strong><td><td>
                                    <input name="event_image" id="event_image" type="file" <?php print $row->event_image != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->event_image != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="event_image_nm"><?php print $row->event_image; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('event_image', 'event_image_nm','del_event_image');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                    <input name="del_event_image" id="del_event_image" type="hidden" value="0"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('Event Contact Detail').""; ?>:</strong><td><td>
                                    <input name="event_contact" value="<?php echo htmlspecialchars($row->event_contact, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_START_DATE_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php
									// for plugin
		                            // JPluginHelper::importPlugin('datepicker');	
	                               // echo plgSystemDatePicker::calendar($row->events_startdate, 'events_startdate', 'events_startdate', '%Y-%m-%d %H:%M:%S');
									 ?>
                                <?php JHTML::_('behavior.calendar'); ?>
                                 <input class="inputbox" type="text" name="events_startdate" id="events_startdate"  value="<?php echo strtotime($row->events_startdate) !='' ? date('d-m-Y H:i:s', strtotime(htmlspecialchars($row->events_startdate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('events_startdate','%d-%m-%Y %H:%M:%S');"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_END_DATE_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php	
	                               // echo plgSystemDatePicker::calendar($row->events_enddate, 'events_enddate', 'events_enddate', '%Y-%m-%d %H:%M:%S');
									 ?>
                                 <input class="inputbox" type="text" name="events_enddate" id="events_enddate"  value="<?php echo strtotime($row->events_enddate) != '' ? date('d-m-Y H:i:s', strtotime(htmlspecialchars($row->events_enddate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('events_enddate','%d-%m-%Y %H:%M:%S');"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td> 
                                    <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'events_description',  @$row->events_description , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'events_description_ar',  @$row->events_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?> 
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?>:</strong><td><td>
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?>:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?>:</strong><td><td>
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                             <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.newevent" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newEvent 

//listEvents
function listEvents($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_EVENTS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_EVENTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 && (events_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'") || publishedby = "'.$user->id.'")';


    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(events_title) LIKE '%".$search."%'";
        $where .= " OR LOWER(events_title_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_location) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_location_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_organized_by) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_organized_by_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(events_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(events_description_ar)) LIKE '%".$search."%' )";
    }    
   $database->SetQuery( "SELECT count(*) FROM #__eposts_events");
    $total = $database->loadResult();      
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_events
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->events_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    }    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newevent");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Event</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LOCATION_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_START_DATE_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_END_DATE_TITLE')." "; ?></th>    
            <th valign="top" align="left" class="title"><?php echo JText::_('Actions')." "; ?></th>            
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=newevent&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newevent&task=delnewevent&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left">
            <a href="<?php echo $link; ?>"><?php echo $row->events_title; ?></a>
            
            </td>
            <td valign="top" align="left"><?php echo $row->events_location; ?></td>
            <td valign="top" align="left"><?php echo JText::sprintf('%s', JHtml::_('date', $row->events_startdate, JText::_('DATE_FORMAT_LC5'))); ?></td>
            <td valign="top" align="left"><?php echo JText::sprintf('%s', JHtml::_('date', $row->events_enddate, JText::_('DATE_FORMAT_LC5'))); ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listEvents

//newDocument
function newDocument($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_DOCUMENTS_NEW_PAGE_TITLE'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);
	
     $row = new jlist_dms( $database );
    $row->load( $cirid );
    $cdipartment = array();
    /*$database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
                . "\n ORDER BY departments_name ASC"
            );*/
	    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );		
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'dms_department',
        'class="inputbox" ', 'value', 'text', $row->dms_department);
		
	
	$cdAccess[] = JHTML::_('select.option',0,'No');
	$cdAccess[] = JHTML::_('select.option',1,'Yes');
	$departmentAccess =	JHTML::_('select.genericlist',$cdAccess, 'dms_access',
        'class="inputbox" ', 'value', 'text', $row->dms_access);
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_DOCUMENTS_NEW_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
	
    
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">                       
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.dms_name.value == "" 
			 || form.dms_name_ar.value == "" 
			 <?php if(!$row->id){?>
			 || form.dms_file.value == ""
			 <?php } ?> 
			 || tinyMCE.get('dms_description').getContent()== ""
			 || tinyMCE.get('dms_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php
	$link = JRoute::_("index.php?option=com_eposts&view=newdocument&cirid=".$row->id);
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_DMS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_DMS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="dms_name" value="<?php echo htmlspecialchars($row->dms_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="dms_name_ar" value="<?php echo htmlspecialchars($row->dms_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?>:</strong><td><td> 
                                  <?php 
								 $editor =& JFactory::getEditor();
								 echo $editor->display( 'dms_description',  @$row->dms_description , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong><td><td> 
                                    <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'dms_description_ar',  @$row->dms_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?>:</strong><td><td>
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?>:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_FILE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="dms_file" id="dms_file" type="file" <?php print $row->dms_title != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->dms_title != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="dms_title_nm"><?php print $row->dms_title; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('dms_file', 'dms_title_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Publish:</strong><td><td>
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>                  
                            <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.newdocument" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newDocument 

//listDocuments
function listDocuments($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_DOCUMENTS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_DOCUMENTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 && dms_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'")';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(dms_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(dms_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(dms_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(dms_description_ar)) LIKE '%".$search."%'";
		$where .= " OR LOWER(dms_title) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(dms_file_data)) LIKE '%".$search."%') ";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_dms ".$where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_dms
              $where
              ORDER BY id DESC";
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->dms_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    }    
    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newdocument");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Document</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_ACCESS')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_DEPARTMENT_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_DOC_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('Actions')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=newdocument&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newdocument&task=delnewdocument&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left">
            <a href="<?php echo $link; ?>"><?php echo $row->dms_name; ?></a>
            
            </td>
            <td valign="top" align="left"><?php echo ($row->dms_access)? 'Public': 'Private'; ?></td>
            <td valign="top" align="left"><?php echo $row->department; ?></td>
            <td valign="top" align="left"><?php echo $row->dms_title; ?><?php //echo '<br />'.$row->dms_path; ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listDocuments

//Medialibrary
//newMedia
function newMedia($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_DWNMEDIA_NEW_PAGE_TITLE'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);
	
    $row = new jlist_dwnmedias( $database );
    $row->load( $cirid );
    $cdipartment = array();
    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'media_department',
        'class="inputbox" ', 'value', 'text', $row->media_department);
		
	
	$cdAccess[] = JHTML::_('select.option',0,'No');
	$cdAccess[] = JHTML::_('select.option',1,'Yes');
	$departmentAccess =	JHTML::_('select.genericlist',$cdAccess, 'media_access',
        'class="inputbox" ', 'value', 'text', $row->media_access);
		
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);	
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_DWNMEDIA_NEW_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">                               
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.media_name.value == "" 
			 || form.media_name_ar.value == ""
<?php if(!$row->id){?>
  || form.media_file.value == ""
<?php }?>
			 || tinyMCE.get('media_description').getContent()== ""
			 || tinyMCE.get('media_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php 
	$link = JRoute::_("index.php?option=com_eposts&view=newMedia&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Media') :  JText::_('Edit Media');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newmedia'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?>:</strong></td><td>
                                    <input name="media_name" value="<?php echo htmlspecialchars($row->media_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong></td><td>
                                    <input name="media_name_ar" value="<?php echo htmlspecialchars($row->media_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?>:</strong></td><td>
                                  <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'media_description',  @$row->media_description , '100%', '10', '80', '5', false, false ) ;
								?> 
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?>:</strong></td><td>
                                    <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'media_description_ar',  @$row->media_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?> 
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?>:</strong></td><td>
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?>:</strong></td><td>
                                    <?php echo $departmentList;?></td>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_FILE')."<font color='#990000'>*</font>"; ?>:</strong></td><td>
                                    <input name="media_file" id="media_file" type="file" <?php print $row->media_document != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->media_document != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="media_document_nm"><?php print $row->media_document; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('media_file', 'media_document_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?></td>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></strong></td><td>
                                    <?php echo $isPublist;?></td>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong></td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong></td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>                                                
                            <tr>
                                <td align="center">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.media" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newMedia 

//listMedia
function listMedia($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_DWNMEDIAS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_DWNMEDIAS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 && (media_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'") || publishedby = "'.$user->id.'")';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(media_name) LIKE '%".$search."%'";
        $where  .= " OR LOWER(media_name_ar) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(media_description)) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(media_description_ar)) LIKE '%".$search."%' )";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_dwnmedias".$where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_dwnmedias
              $where
              ORDER BY id DESC";                   
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->media_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    }    
   
    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newmedia");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Media</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
  <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="6">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_DESC_TITLE')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('Publish')." "; ?></th>
            <th valign="top" align="left" class="title"><?php echo JText::_('Actions')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=newmedia&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newmedia&task=delmedia&cirid=".$row->id);            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left">
            <a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_EDIT_TITLE');?>"><?php echo $row->media_name; ?></a>
            
            </td>
            <td valign="top" align="left"><?php echo $row->media_description; ?></td>
            <td valign="top"><?php
			echo ($row->publish == 1)? 'Yes':'No';
			 ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listMedia

//myMedia
function myMedia($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_DWNMEDIAS_LIST_PAGE_TITLE_FRONT'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_DWNMEDIAS_LIST_PAGE_TITLE_FRONT'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 ';
  

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(media_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(media_description)) LIKE '%".$search."%' )";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_dwnmedias");
    $total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_dwnmedias
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->media_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    }    
   
    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newmedia");   
?>	
<!--<a class="brightNew" href="<?php echo $newlink;?>">New Media</a>
<div class="clear"></div>-->
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
  <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td  align="right" colspan="3">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_DESC_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIA_DEPARTMENT_TITLE')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = '#';//JRoute::_("index.php?option=com_eposts&view=newmedia&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newmedia&task=delmedia&cirid=".$row->id);            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
            <a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_EDIT_TITLE');?>"><?php echo $row->media_name; ?></a>
            <!--<a href="<?php echo $dellink; ?>">Delete</a>-->
            </td>
            <td valign="top"><?php echo $row->media_description; ?></td>
            <td valign="top"><?php echo $row->department; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion myMedia

//List Complaints

function listComplaints($option, $view) {
	global $jlistConfig, $Itemid, $mainframe, $page_title, $limit;
	$database = &JFactory::getDBO();
	$user = &JFactory::getUser();
    $document= &JFactory::getDocument();
	$language = &JFactory::getLanguage();
	
	$database->SetQuery( "SELECT count(*) FROM #__eposts_scgdata WHERE type = '3' AND for_deptid IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."') ORDER BY id DESC");
    $total = $database->loadResult(); 
	
	$limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
	jimport('joomla.html.pagination'); 
	$pageNav = new JPagination( $total, $limitstart, $limit );
	if ($pageNav->limitstart != $limitstart){
		$session->set('jdlimitstart', $pageNav->limitstart);
		$limitstart = $pageNav->limitstart;
	}
	
	$database->setQuery("SELECT * FROM #__eposts_scgdata WHERE type = '3' AND for_deptid IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."') ORDER BY id DESC", $pageNav->limitstart, $pageNav->limit);
	$rows = $database->loadObjectList();
	?>
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
        <tr>
            <th valign="top" align="left" class="title">Employee Name</th>
            <th valign="top" align="left" class="title">Department Name</th>
            <th valign="top" align="left" class="title">Designation</th>
            <th valign="top" align="left" class="title">Complaint For</th>
        </tr>
        <?php
		$count = 1;
        foreach($rows as $row) {
			$k = $count%2;
			
			$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->user_deptid."'");
			$department = $database->loadObject();
			
			$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->for_deptid."'");
			$departmentfor = $database->loadObject();
        ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php print JRoute::_('index.php?option=com_eposts&view=detailcomplaints&Itemid=364&cirid='.$row->id); ?>"><?php echo $row->name; ?></a></td>
            <td valign="top" align="left"><?php echo $department->departments_name; ?></td>
            <td valign="top" align="left"><?php echo $row->designation; ?></td>
            <td valign="top" align="left"><?php echo $departmentfor->departments_name; ?></td>
        </tr>
        <?php $count++; } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <?php
}

function detailComplaints($option, $view) {
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	
	$database		= &JFactory::getDBO();
	$user			= &JFactory::getUser();
    $document		= &JFactory::getDocument();
	$language		= &JFactory::getLanguage();
	$breadcrumbs	= &$mainframe->getPathWay();
	
	$cirid = JRequest::getCmd('cirid');
	
	$database->setQuery("SELECT * FROM #__eposts_scgdata WHERE id = '".$cirid."'");
	$row = $database->loadObject();
	
	$document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_COMPLAINTS_DETAIL'));
	$breadcrumbs->addItem(JText::_('COM_EPOSTS_COMPLAINTS_DETAIL'), JRoute::_('index.php?option=com_eposts&view=listcomplaints&Itemid=364'));
			
	$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->user_deptid."'");
	$department = $database->loadObject();
	
	$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->for_deptid."'");
	$departmentfor = $database->loadObject();
	?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                  <tr>
                      <td valign="top">
                          <table class="admintable" width="100%">
                             <tr>
                                <td width="200"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_EMPID'); ?>:</strong></td>
                                <td><?php print $row->employeeid; ?></td>
                             </tr>
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_NAME'); ?>:</strong></td>
                                <td><?php print $row->name; ?></td>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DESIGNATIONS'); ?>:</strong></td>
                                <td><?php print $row->designation; ?></td>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DEPARTMENT'); ?>:</strong></td>
                                <td><?php print $department->departments_name; ?></td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_SECTION'); ?>:</strong></td>
                                <td><?php print $row->section; ?></td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_COMPLAINTS_DETAILS'); ?>:</strong></td>
                                <td><?php print $row->details; ?></td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_COMPLAINTS_FOR'); ?>:</strong></td>
                                <td><?php print $departmentfor->departments_name; ?></td>
                                                                 
                            </tr>
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
	<?php
}

//Complaints
function newComplaints($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_COMPLAINTS_NEW_PAGE_TITLE'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	
    $row = new jlist_tasawaqs( $database );
    $row->load( $cirid );
    $cdipartment = array();
    $blocked = JText::_('COM_EPOSTS_BACKEND_TASAWAQS_EDIT_USER_BLOCKED');
	
	$database->setQuery("SELECT id,departments_name,departments_name_ar "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
			$xdepartment->departments_name = ($tag == 'ar-AA') ? $xdepartment->departments_name_ar : $xdepartment->departments_name;
            $mydepartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $myDepartmentList = JHTML::_('select.genericlist',$mydepartment, 'department',
        'class="inputbox" ', 'value', 'text', $row->department);
		

    $database->setQuery("SELECT id,departments_name,departments_name_ar "
                . "\n FROM #__eposts_departments "
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
			$xdepartment->departments_name = ($tag == 'ar-AA') ? $xdepartment->departments_name_ar : $xdepartment->departments_name;
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'for_department',
        'class="inputbox" ', 'value', 'text', $row->for_department);

		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_COMPLAINTS_NEW_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.name.value == "" 
			 || form.designation.value == ""
			 || form.employeeid.value == ""
			 || form.department.value == ""
			 || form.section.value == ""
			 || form.for_department.value == ""
			 || tinyMCE.get('details').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php 
	$link = JRoute::_("index.php?option=com_eposts&view=complaints");
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
               <!--<tr>
                      <th class="adminheading" align="left"></?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TASAWAQS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_TASAWAQS_ADD_TITLE');?></th>
                  </tr>-->
                  <tr>
                      <td valign="top">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_EMPID')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="employeeid" value="" size="80" maxlength="150"/>
                                                                 
                            </tr>
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_NAME')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="name" value="" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DESIGNATIONS')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="designation" value="" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DEPARTMENT')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php echo $myDepartmentList;?> 
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_SECTION')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="section" value="" size="80" maxlength="150"/>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_COMPLAINTS_DETAILS')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'details',  @$row->details , '100%', '10', '80', '5', false, false ) ;
								?>
                                </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_COMPLAINTS_FOR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                         <!--<tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly="readonly" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly="readonly"  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr> -->
                            <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="<?php print JText::_('COM_EPOSTS_SAVE_BTN'); ?>"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="<?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?>"/>                            </td></tr>
                          </table>
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.complaints" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newComplaints 

//List Suggestions

function listSuggestions($option, $view) {
	global $jlistConfig, $Itemid, $mainframe, $page_title, $limit;
	$database = &JFactory::getDBO();
	$user = &JFactory::getUser();
    $document= &JFactory::getDocument();
	$language = &JFactory::getLanguage();
	
	$database->SetQuery( "SELECT count(*) FROM #__eposts_scgdata WHERE type = '1' AND for_deptid IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."') ORDER BY id DESC");
    $total = $database->loadResult(); 
	
	$limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
	jimport('joomla.html.pagination'); 
	$pageNav = new JPagination( $total, $limitstart, $limit );
	if ($pageNav->limitstart != $limitstart){
		$session->set('jdlimitstart', $pageNav->limitstart);
		$limitstart = $pageNav->limitstart;
	}
	
	$database->setQuery("SELECT * FROM #__eposts_scgdata WHERE type = '1'  AND for_deptid IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."') ORDER BY id DESC", $pageNav->limitstart, $pageNav->limit);
	$rows = $database->loadObjectList();
	?>
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
        <tr>
            <th valign="top" align="left" class="title">Employee Name</th>
            <th valign="top" align="left" class="title">Department Name</th>
            <th valign="top" align="left" class="title">Designation</th>
            <th valign="top" align="left" class="title">Suggestion For</th>
        </tr>
        <?php
		$count = 1;
        foreach($rows as $row) {
			$k = $count%2;
			
			$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->user_deptid."'");
			$department = $database->loadObject();
			
			$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->for_deptid."'");
			$departmentfor = $database->loadObject();
        ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php print JRoute::_('index.php?option=com_eposts&view=detailsuggestions&Itemid=362&cirid='.$row->id); ?>"><?php echo $row->name; ?></a></td>
            <td valign="top" align="left"><?php echo $department->departments_name; ?></td>
            <td valign="top" align="left"><?php echo $row->designation; ?></td>
            <td valign="top" align="left"><?php echo $departmentfor->departments_name; ?></td>
        </tr>
        <?php $count++; } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <?php
}

function detailSuggestions($option, $view) {
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	
	$database		= &JFactory::getDBO();
	$user			= &JFactory::getUser();
    $document		= &JFactory::getDocument();
	$language		= &JFactory::getLanguage();
	$breadcrumbs	= &$mainframe->getPathWay();
	
	$cirid = JRequest::getCmd('cirid');
	
	$database->setQuery("SELECT * FROM #__eposts_scgdata WHERE id = '".$cirid."'");
	$row = $database->loadObject();
	
	$document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_SUGGESTIONS_DETAIL'));
	$breadcrumbs->addItem(JText::_('COM_EPOSTS_SUGGESTIONS_DETAIL'), JRoute::_('index.php?option=com_eposts&view=listsuggestions&Itemid=362'));
			
	$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->user_deptid."'");
	$department = $database->loadObject();
	
	$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->for_deptid."'");
	$departmentfor = $database->loadObject();
	?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                  <tr>
                      <td valign="top">
                          <table class="admintable" width="100%">
                             <tr>
                                <td width="200"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_EMPID'); ?>:</strong></td>
                                <td><?php print $row->employeeid; ?></td>
                             </tr>
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_NAME'); ?>:</strong></td>
                                <td><?php print $row->name; ?></td>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DESIGNATIONS'); ?>:</strong></td>
                                <td><?php print $row->designation; ?></td>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DEPARTMENT'); ?>:</strong></td>
                                <td><?php print $department->departments_name; ?></td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_SECTION'); ?>:</strong></td>
                                <td><?php print $row->section; ?></td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_SUGGESTIONS_DETAILS'); ?>:</strong></td>
                                <td><?php print $row->details; ?></td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_SUGGESTIONS_FOR'); ?>:</strong></td>
                                <td><?php print $departmentfor->departments_name; ?></td>
                                                                 
                            </tr>
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
	<?php
}

//Suggestions
function newSuggestions($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_SUGGESTIONS_NEW_PAGE_TITLE'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	
    $row = new jlist_tasawaqs( $database );
    $row->load( $cirid );
    $cdipartment = array();
    $blocked = JText::_('COM_EPOSTS_BACKEND_TASAWAQS_EDIT_USER_BLOCKED');
	
	$database->setQuery("SELECT id,departments_name,departments_name_ar "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
			$xdepartment->departments_name  = ($tag == 'ar-AA') ? $xdepartment->departments_name_ar :$xdepartment->departments_name;
            $mydepartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $myDepartmentList = JHTML::_('select.genericlist',$mydepartment, 'department',
        'class="inputbox" ', 'value', 'text', $row->department);
		

    $database->setQuery("SELECT id,departments_name,departments_name_ar "
                . "\n FROM #__eposts_departments "
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
			$xdepartment->departments_name  = ($tag == 'ar-AA') ? $xdepartment->departments_name_ar :$xdepartment->departments_name;
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'for_department',
        'class="inputbox" ', 'value', 'text', $row->for_department);

		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_SUGGESTIONS_NEW_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.name.value == "" 
			 || form.designation.value == ""
			 || form.employeeid.value == ""
			 || form.department.value == ""
			 || form.section.value == ""
			 || form.for_department.value == ""
			 || tinyMCE.get('details').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php 
	$link = JRoute::_("index.php?option=com_eposts&view=suggestions");
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data"  onsubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
               <!--<tr>
                      <th class="adminheading" align="left"></?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TASAWAQS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_TASAWAQS_ADD_TITLE');?></th>
                  </tr>-->
                  <tr>
                      <td valign="top">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_EMPID')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="employeeid" value="" size="80" maxlength="150"/>
                                                                 
                            </tr>
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_NAME')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="name" value="" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DESIGNATIONS')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="designation" value="" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DEPARTMENT')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php echo $myDepartmentList;?> 
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_SECTION')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="section" value="" size="80" maxlength="150"/>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_SUGGESTIONS_DETAILS')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'details',  @$row->details , '100%', '10', '80', '5', false, false ) ;
								?>
                                </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_SUGGESTIONS_FOR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                         <!--<tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly="readonly" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly="readonly"  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr> -->
                            <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="<?php print JText::_('COM_EPOSTS_SAVE_BTN'); ?>"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="<?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?>" />                               
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.suggestions" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newSuggestions

//List Grievances

function listGrievances($option, $view) {
	global $jlistConfig, $Itemid, $mainframe, $page_title, $limit;
	$database = &JFactory::getDBO();
	$user = &JFactory::getUser();
    $document= &JFactory::getDocument();
	$language = &JFactory::getLanguage();
	
	$database->SetQuery( "SELECT count(*) FROM #__eposts_scgdata WHERE type = '2' AND for_deptid IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."') ORDER BY id DESC");
    $total = $database->loadResult(); 
	
	$limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
	jimport('joomla.html.pagination'); 
	$pageNav = new JPagination( $total, $limitstart, $limit );
	if ($pageNav->limitstart != $limitstart){
		$session->set('jdlimitstart', $pageNav->limitstart);
		$limitstart = $pageNav->limitstart;
	}
	
	$database->setQuery("SELECT * FROM #__eposts_scgdata WHERE type = '2' AND for_deptid IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."') ORDER BY id DESC", $pageNav->limitstart, $pageNav->limit);
	$rows = $database->loadObjectList();
	?>
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
        <tr>
            <th valign="top" align="left" class="title">Employee Name</th>
            <th valign="top" align="left" class="title">Department Name</th>
            <th valign="top" align="left" class="title">Designation</th>
            <th valign="top" align="left" class="title">Grievance For</th>
        </tr>
        <?php
		$count = 1;
        foreach($rows as $row) {
			$k = $count%2;
			
			$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->user_deptid."'");
			$department = $database->loadObject();
			
			$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->for_deptid."'");
			$departmentfor = $database->loadObject();
        ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php print JRoute::_('index.php?option=com_eposts&view=detailgrievances&Itemid=363&cirid='.$row->id); ?>"><?php echo $row->name; ?></a></td>
            <td valign="top" align="left"><?php echo $department->departments_name; ?></td>
            <td valign="top" align="left"><?php echo $row->designation; ?></td>
            <td valign="top" align="left"><?php echo $departmentfor->departments_name; ?></td>
        </tr>
        <?php $count++; } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <?php
}

function detailGrievances($option, $view) {
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	
	$database		= &JFactory::getDBO();
	$user			= &JFactory::getUser();
    $document		= &JFactory::getDocument();
	$language		= &JFactory::getLanguage();
	$breadcrumbs	= &$mainframe->getPathWay();
	
	$cirid = JRequest::getCmd('cirid');
	
	$database->setQuery("SELECT * FROM #__eposts_scgdata WHERE id = '".$cirid."'");
	$row = $database->loadObject();
	
	$document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_GRIEVANCES_DETAIL'));
	$breadcrumbs->addItem(JText::_('COM_EPOSTS_GRIEVANCES_DETAIL'), JRoute::_('index.php?option=com_eposts&view=listgrievances&Itemid=363'));
			
	$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->user_deptid."'");
	$department = $database->loadObject();
	
	$database->setQuery("SELECT * FROM #__eposts_departments WHERE id = '".$row->for_deptid."'");
	$departmentfor = $database->loadObject();
	?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                  <tr>
                      <td valign="top">
                          <table class="admintable" width="100%">
                             <tr>
                                <td width="200"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_EMPID'); ?>:</strong></td>
                                <td><?php print $row->employeeid; ?></td>
                             </tr>
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_NAME'); ?>:</strong></td>
                                <td><?php print $row->name; ?></td>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DESIGNATIONS'); ?>:</strong></td>
                                <td><?php print $row->designation; ?></td>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DEPARTMENT'); ?>:</strong></td>
                                <td><?php print $department->departments_name; ?></td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_SECTION'); ?>:</strong></td>
                                <td><?php print $row->section; ?></td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_GRIEVANCES_DETAILS'); ?>:</strong></td>
                                <td><?php print $row->details; ?></td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_GRIEVANCES_FOR'); ?>:</strong></td>
                                <td><?php print $departmentfor->departments_name; ?></td>
                                                                 
                            </tr>
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
	<?php
}

//Grievances
function newGrievances($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_GRIEVANCES_NEW_PAGE_TITLE'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	
    $row = new jlist_tasawaqs( $database );
    $row->load( $cirid );
    $cdipartment = array();
    $blocked = JText::_('COM_EPOSTS_BACKEND_TASAWAQS_EDIT_USER_BLOCKED');
	
	$database->setQuery("SELECT id,departments_name,departments_name_ar "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
			$xdepartment->departments_name = ($tag == 'ar-AA') ? $xdepartment->departments_name_ar : $xdepartment->departments_name;
            $mydepartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $myDepartmentList = JHTML::_('select.genericlist',$mydepartment, 'department',
        'class="inputbox" ', 'value', 'text', $row->department);
		

    $database->setQuery("SELECT id,departments_name,departments_name_ar "
                . "\n FROM #__eposts_departments "
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
			$xdepartment->departments_name = ($tag == 'ar-AA') ? $xdepartment->departments_name_ar : $xdepartment->departments_name;
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'for_department',
        'class="inputbox" ', 'value', 'text', $row->for_department);

		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_GRIEVANCES_NEW_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.name.value == "" 
			 || form.designation.value == ""
			 || form.employeeid.value == ""
			 || form.department.value == ""
			 || form.section.value == ""
			 || form.for_department.value == ""
			 || tinyMCE.get('details').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php 
	$link = JRoute::_("index.php?option=com_eposts&view=grievances");
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
               <!--<tr>
                      <th class="adminheading" align="left"></?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TASAWAQS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_TASAWAQS_ADD_TITLE');?></th>
                  </tr>-->
                  <tr>
                      <td valign="top">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_EMPID')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="employeeid" value="" size="80" maxlength="150"/>
                                                                 
                            </tr>
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_NAME')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="name" value="" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DESIGNATIONS')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="designation" value="" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_DEPARTMENT')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php echo $myDepartmentList;?> 
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CGS_SECTION')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="section" value="" size="80" maxlength="150"/>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_GRIEVANCES_DETAILS')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'details',  @$row->details , '100%', '10', '80', '5', false, false ) ;
								?>
                                </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_GRIEVANCES_FOR')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                         <!--<tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly="readonly" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly="readonly"  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr> -->
                            <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="<?php print JText::_('COM_EPOSTS_SAVE_BTN'); ?>"/>
                                   <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="<?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?>"/>                              
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.grievances" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newGrievances


//////////////////////////////////////////////NEW PAGES////////////////////////////////////
function mainListCirculars($option, $view ){
$language = &JFactory::getLanguage();
$tag = $language->get('tag');	
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_CIRCILARS_LIST_PAGE_TITLE'));

	$jdeptid = JRequest::getCmd('deptid');
	$deptid = !($jdeptid) ? $deptid : $jdeptid;
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_CIRCILARS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   
   $deptquery = "";
	$page = JRequest::getCmd('page');
 
   if($user->id && $deptid && !$page){
$deptquery .= " (circulars_access = 1 || circulars_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && circulars_department = '".$deptid."' ";
   }elseif($deptid && !$page){
	    $deptquery .= " circulars_access = 1 && circulars_department = '".$deptid."' ";
   }else{
	    $deptquery .= " circulars_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' ';
   

    $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}
	if ($search=="البحث بالإسم"){
	$search ="";
	}
	if ($announcements_expirydate=="Search by Calendar"){
	$announcements_expirydate ="";
	}
	if ($announcements_expirydate=="البحث عن طريق التقويم"){
	$announcements_expirydate ="";
	}
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
 $announcements_expirydate1=date("Y-m-d", strtotime($announcements_expirydate));    
   if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(circulars_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(circulars_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(circulars_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(circulars_description_ar)) LIKE '%".$search."%'";
		$where .= " OR LOWER(circulars_date) LIKE '%".$search."%' )";
    }elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND circulars_department = '".$deparrtment."'"; 
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	
} elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(circulars_department) = '".$deparrtment."'";
	
	}  	
   //  echo ( "SELECT count(*) FROM #__eposts_circulars".$where);    
   $database->SetQuery( "SELECT count(*) FROM #__eposts_circulars".$where);
    $total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_circulars
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name, departments_name_ar "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->circulars_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
			if ($tag == 'ar-AA') :
				$row->department = $department->departments_name_ar;
			endif;
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
  JHTML::_('behavior.calendar');  
?>
<form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<div class="circulars_mainfilter"><table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area_calander" type="text"  name="announcements_expirydate" id="announcements_expirydate" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>') {this.value = '';}" ></td>
			<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
				<td>
			<select name="deparrtment" class="inputbox" >
				<option value="" ><?php echo JText::_('COM_EPOSTS_SELECT_DEPARTMENT'); ?></option>

				<?php

 

$database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "               
            );
   
       $listDepartments2 = $database->loadObjectList();

				foreach($listDepartments2 as $xdepartment2) { ?>
		
	<?php if ($tag == 'ar-AA') {
		$department_name = $xdepartment2->departments_name_ar;
		}else{
	$department_name = $xdepartment2->departments_name;
	}?>
	<option value="<?php echo $xdepartment2->id; ?>"<?php if (!(strcmp($xdepartment2->id,$deparrtment))) {echo "selected=\"selected\"";} ?>><?php echo $department_name; ?></option>
			<?php	} ?>
			</select></td>
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>';	

	echo $form;  ?>
     <div class="circulars_bodytable"><table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <!--  <tr>
          <td class="main_filter">
            <?php //echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php //echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php // echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  //echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>-->
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
		
            $row = &$rows[$i];
			
			$database->setQuery("SELECT departments_name, departments_name_ar "
					. "\n FROM #__eposts_departments "
					. "\n WHERE id = " . $row->circulars_department
				);
	   
		   $dasfasdf = $database->loadObject();
				$row->department = $dasfasdf->departments_name;
				if ($tag == 'ar-AA') :
					$row->department = $dasfasdf->departments_name_ar;
				endif;
			
			$deptid = JRequest::getInt('deptid');
			$homeLink = $page ? '&page=home' : '';
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowcirculars".$homeLink."&cirid=".$row->id.'&deptid='.$deptid);
			$title = $row->circulars_name;
			$descp = $row->circulars_description;
			$document=$row->circulars_document;
			if ($tag == 'ar-AA') :
				$title = $row->circulars_name_ar;
				$descp = $row->circulars_description_ar;
				$document=$row->circulars_document_ar;
			endif;
			$title = strip_tags($title);
			$length = 400;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
			
$extension = end(explode('.', $document));           
		   ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div id="default-colm-two">
			<div class="default-colm-left">
			<div class="default-list-title"><?php echo $title;?></div>
            <div class="default-list-decs"> 
			<?php echo $descp; ?>
            </div>
            </div>
			<div class="default-colm-right">
			<div class="sub_title"><span class="default-list-decs-head"><?php echo $row->department ?></span><br/><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
            <div class="sub_title"><div class="sub_title"><div class="form_download_icon"><a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $document); ?>">
   <?php if (($extension=="docx") or ($extension=="dot")or ($extension=="doc")or ($extension=="docm")or ($extension=="dotm")or ($extension=="dotx"))   { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-word.png" border="0" alt="" /></a></div></div>
   <?php }elseif ($extension=="pdf") { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-pdf.png" border="0" alt="" /></a></div></div>
  <?php }elseif (($extension=="xls") or ($extension=="xlt")or ($extension=="xlm")or ($extension=="xlsx")or ($extension=="xltx")or ($extension=="xltm"))   { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-excel.png" border="0" alt="" /></a></div></div>
   <?php }else { ?>
   
   <img src="components/com_eposts/assets/images/icons/forms-download.png" border="0" alt="" /><div class="forms-download-text"><?php print JText::_('COM_EPOSTS_FORMS_DOWNLOAD_TEXT'); ?></a></div></div></div>
   <?php } ?>
   </div></div>
			</div>
			</div>
			<div>&nbsp;</div>            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND_CIRCULARS'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table></div>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListCirculars

function mainShowCirculars($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_SHOW_CIRCILARS_LIST_PAGE_TITLE'));

	$jdeptid = JRequest::getCmd('deptid');
	$deptid = !($jdeptid) ? $deptid : $jdeptid;
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_SHOW_CIRCILARS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   
   $deptquery = "";
 
   if($user->id && $deptid){
$deptquery .= " (circulars_access = 1 || circulars_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && circulars_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " circulars_access = 1 && circulars_department = '".$deptid."' ";
   }else{
	    $deptquery .= " circulars_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' && id = "'.$cirid.'"';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(circulars_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(circulars_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(circulars_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(circulars_description_ar)) LIKE '%".$search."%'";
		$where .= " OR LOWER(circulars_date) LIKE '%".$search."%' )";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_circulars".$where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_circulars
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->circulars_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->circulars_department."&Itemid=103");
			$title = $row->circulars_name;
			$descp = $row->circulars_description;
			if ($tag == 'ar-AA') :
				$title = $row->circulars_name_ar;
				$descp = $row->circulars_description_ar;
			endif;
			$title = strip_tags($title);
			$length = 20;
			if (mb_strlen($title) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$length = 20;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
        	$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainlistcirculars'));
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title"><?php echo $title; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date('d-m-Y', strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?>, <?php echo JText::_('COM_EPOSTS_MAIN_DATE_CIRCULAR'); ?> <?php echo date('d-m-Y', strtotime($row->circulars_date)); ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_DOWNLOAD'); ?> 
            <?php if ($tag == 'ar-AA') : ?>
            	<a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->circulars_document_ar); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a>
            <?php else : ?>
            	<a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->circulars_document); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a>
            <?php endif; ?>
            <div class="body">
			<?php echo $descp; ?>
            </div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowCirculars

function mainListTasawaqs($option, $view ){

	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');

	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_TASAWAQS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_TASAWAQS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   $deptid = (int)JRequest::getString('deptid', 0);
   
   $deptquery = "";

   
    $where = ' WHERE publish = 1 ';
   

   
	  $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}
	if ($search=="البحث بالإسم"){
	$search ="";
	}
	if ($announcements_expirydate=="Search by Calendar"){
	$announcements_expirydate ="";
	}
	if ($announcements_expirydate=="البحث عن طريق التقويم"){
	$announcements_expirydate ="";
	}
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
 $announcements_expirydate1=date("Y-m-d", strtotime($announcements_expirydate));   
  if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(tasawaqs_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(tasawaqs_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(tasawaqs_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(tasawaqs_description_ar)) LIKE '%".$search."%' )";
    }
elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND media_department = '".$deparrtment."'"; 
        $where .= " AND  publishedon LIKE '".$announcements_expirydate."%'";
	
	
} elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(media_department) = '".$deparrtment."'";
	
	}  	
   // echo ( "SELECT count(*) FROM #__eposts_tasawaqs".$where);    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_tasawaqs".$where);
    $total = $database->loadResult(); 
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_tasawaqs
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name, departments_name_ar "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->tasawaqs_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
			if ($tag == 'ar-AA') :
            $row->department = $departments->departments_name_ar;
			endif;
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
 JHTML::_('behavior.calendar');   
?>
<form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<div class="policies_mainfilter"><table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area_calander" type="text"  name="announcements_expirydate" id="announcements_expirydate" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>') {this.value = '';}" ></td>
			
				
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>';	

	echo $form;  ?>
     <div class="tasawaqs_bodytable"><table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowtasawaqs&page=home&cirid=".$row->id);
	
			$title = $row->tasawaqs_name;
			$descp = $row->tasawaqs_description;
			if ($tag == 'ar-AA') :
				$title = $row->tasawaqs_name_ar;
				$descp = $row->tasawaqs_description_ar;
			endif;
			
			$length = 100;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
            ?>
	   <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div id="default-colm-two">
			<div class="default-colm-left">
			<div class="default-list-title"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
            <div class="default-list-decs"> 
			<?php echo $descp; ?>
            </div>
            </div>
			<div class="default-colm-right">
			<div class="sub_title"><span class="default-list-decs-head"><?php echo $row->department ?></span><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>

   </div></div>
			</div>
			</div>
			<div>&nbsp;</div>            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND_TASAWAQ'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table></div>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
//echo $footer; 
}//end funtion mainListTasawaqs

function mainShowTasawaqs($option, $view ){
    global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
    
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
	
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_SHOW_TASAWAQS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_SHOW_TASAWAQS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   $deptid = (int)JRequest::getString('deptid', 0);
   
$deptquery = "";
 
    $where = ' WHERE publish = 1 AND id = "' . $cirid . '"';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(tasawaqs_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(tasawaqs_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(tasawaqs_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(tasawaqs_description_ar)) LIKE '%".$search."%' )";
    }    
 $database->SetQuery( "SELECT count(*) FROM #__eposts_tasawaqs".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_tasawaqs
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->tasawaqs_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
			if ($tag == 'ar-AA') :
            $row->department = $departments->departments_name_ar;
			endif;
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->tasawaqs_department."&Itemid=103");
			$title = $row->tasawaqs_name;
			$descp = $row->tasawaqs_description;
			if ($tag == 'ar-AA') :
			$title = $row->tasawaqs_name_ar;
			$descp = $row->tasawaqs_description_ar;
			endif;
			$length = 80;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
			$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowtasawaqs'));
            ?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top" class="">
			<div class="title_news">
			<?php 
			if ($row->tasawaqs_p_logo) :
			   //$img = JRoute::_('index.php?option=com_eposts&view=loadimage&myfile='.$filepath.'\\'.$row->tasawaqs_p_logo);
			   $img = $filepath.'\\'.$row->tasawaqs_p_logo;
			  
			   $img = str_replace("\\", "/", $img);
	//		 echo $img;
			$filetype=string;       
	     $imgbinary = fread(fopen($img, "r"), filesize($img));
     // echo $imgbinary;
	   $aaa1= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
		//echo $aaa1;	?>
            <div class="title_tasawaq_img"><img src="<?php echo $aaa1; ?>" width="100" /></div>
			</div>
			<div class="title_tasawaq"><div class="title_tasawaq_name"><?php endif; echo $title; ?></div>
			</div>
            <?php 
			if ($row->tasawaqs_image) : 
			//$img2 = JRoute::_('index.php?option=com_eposts&view=loadimage&myfile='.$filepath.'\\'.$row->tasawaqs_image);
			$img2 = $filepath.'\\'.$row->tasawaqs_image;
			//echo $img2;
			$img2 = str_replace("\\", "/", $img2);
			$filetype=string;       
	     $imgbinary = fread(fopen($img2, "r"), filesize($img2));
     // echo $imgbinary;
	   $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
//attachment download
$download1 = $filepath.'\\'. $row->tasawaqs_attachment;
			  //echo $download;
			   $download = str_replace("\\", "/", $download);
	//		 echo $img;
			$filetype=string;       
	     $imgbinary = fread(fopen($img, "r"), filesize($download));
     // echo $imgbinary;
	   $download= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
			JHTML::_("behavior.mootools");
	$document->addStyleSheet(JURI::base() . 'components/com_igallery/cerabox.css');
	$document->addScript(JURI::base() . 'components/com_igallery/cerabox.min.js');
	$document->addScriptDeclaration("
		document.addEvent('domready', function() {
			$$('.thumbs_div a').cerabox({
			group: false,
			displayTitle: false
			});
		});
	");
			?>
           <div class="thumbs_div">
					
                		<a href="#$img_my" >
		   <div class="title_tasawaq_fullimg"><?php  $document->addScriptDeclaration('var img_my = "<img src=\"'.$aaa.'\" />";'); ?>
                        <img  src="<?php echo $aaa; ?>" width="200"  /> 
                    </a></div>
            <?php endif; ?>
           
		   		
			<div class="sub_title"><?php print $row->tasawaq_p_name; ?></div>
			<div class="body p2">
			<?php echo $descp; ?>
            </div>
        <?php if ($tag == 'ar-AA') { ?> 
		   <div class="sub_title"><?php print $row->tasawaq_contact_ar; ?></div>
		    <?php }else { ?> 
			<div class="sub_title_tasawaq_contact"><?php print $row->tasawaq_contact; ?></div>
		    <?php } ?> 
		   <?php if ($row->tasawaqs_attachment) : ?>
            <div class="inner_pages_link_download_view">
            	<a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename='.$row->tasawaqs_attachment); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a>
            </div>
			<?php endif; ?>
			<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowTasawaqs

function mainListEvents($option, $view ){

	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_EVENTS_LIST_PAGE_TITLE'));

	$jdeptid = JRequest::getCmd('deptid');
	$deptid = !($jdeptid) ? $deptid : $jdeptid;
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_EVENTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////

   $deptquery = "";
   $page = JRequest::getCmd('page');
   if($user->id && $deptid && !$page){
$deptquery .= " (events_access = 1 || events_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) AND events_department = '".$deptid."'";
   }elseif($deptid && !$page){
	    $deptquery .= " events_access = 1 && events_department = '".$deptid."' ";
   }else{
	    $deptquery .= " events_access = 1 ";	 
   }
   
   $evntDate = JRequest::getCmd('date');
   if (strtotime($evntDate)) :
	   $evntDate = date('Y-m-d', strtotime($evntDate));
	   $evntDate = $evntDate . ' 00:00:00';
	   
	   $dateQuery = ' AND DATE(events_startdate) <= DATE("'.$evntDate.'") AND DATE(events_enddate) >= DATE("'.$evntDate.'")';
   endif;
    $where = ' WHERE publish = 1 && '.$deptquery.' ' . $dateQuery;
  

     $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}	
	if ($search=="البحث بالإسم"){
	$search ="";
	}
	if ($announcements_expirydate=="Search by Calendar"){
	$announcements_expirydate ="";
	}
	if ($announcements_expirydate=="البحث عن طريق التقويم"){
	$announcements_expirydate ="";
	}
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
 $announcements_expirydate1=date("Y-m-d", strtotime($announcements_expirydate));    
   if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(events_title) LIKE '%".$search."%'";
        $where .= " OR LOWER(events_title_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_location) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_location_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_organized_by) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_organized_by_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(events_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(events_description_ar)) LIKE '%".$search."%' )";
    }  elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND events_department = '".$deparrtment."'"; 
        $where .= " AND  publishedon LIKE '".$announcements_expirydate."%'";
	
	
} elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(events_department) = '".$deparrtment."'";
	
	}  	
    //echo ( "SELECT count(*) FROM #__eposts_events".$where);  
 $database->SetQuery( "SELECT count(*) FROM #__eposts_events".$where);
    $total = $database->loadResult();      
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_events
              $where
              ORDER BY events_startdate DESC";
                                                              
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->events_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
			if ($tag == 'ar-AA') :
            $row->department = $departments->departments_name_ar;
			endif;
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
   JHTML::_('behavior.calendar'); 
?>
<form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<div class="events_mainfilter"><table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area_calander" type="text"  name="announcements_expirydate" id="announcements_expirydate" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>') {this.value = '';}" ></td>
			<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
				<td>
			<select name="deparrtment" class="inputbox" >
				<option value="" ><?php echo JText::_('COM_EPOSTS_SELECT_DEPARTMENT'); ?></option>

				<?php

 

$database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "               
            );
   
       $listDepartments2 = $database->loadObjectList();

				foreach($listDepartments2 as $xdepartment2) { ?>
		
	<?php if ($tag == 'ar-AA') {
		$department_name = $xdepartment2->departments_name_ar;
		}else{
	$department_name = $xdepartment2->departments_name;
	}?>
	<option value="<?php echo $xdepartment2->id; ?>"<?php if (!(strcmp($xdepartment2->id,$deparrtment))) {echo "selected=\"selected\"";} ?>><?php echo $department_name; ?></option>
			<?php	} ?>
			</select></td>
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>';	

	echo $form;  ?>
 
     <div class="events_bodytable"><table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
    <!--  <tr>
          <td class="main_filter">
            <?php //echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php //echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php //echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  //echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr> -->
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$pagelink = !$page ? '' : '&page=home';
            $link = JRoute::_("index.php?option=com_eposts".$pagelink."&view=mainshowevents&cirid=".$row->id);
			
			$title = $row->events_title;
			$descp = $row->events_description;
			$location = $row->events_location;
			$orgBy = $row->events_organized_by;
			if ($tag == 'ar-AA') :
				$title = $row->events_title_ar;
				$descp = $row->events_description_ar;
				$location = $row->events_location_ar;
				$orgBy = $row->events_organized_by_ar;
			endif;
			$title = strip_tags($title);
			$length = 100;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
            ?>
	   <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div id="default-colm-two">
			<div class="default-colm-left">
			<div class="default-list-title"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
            <div class="default-list-decs"> 
			<?php echo $descp; ?>
            </div>
            </div>
			<div class="default-colm-right">
			<div class="sub_title"><span class="default-list-decs-head"><?php echo $row->department ?></span><br/><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
           
			<div>&nbsp;</div>            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND_EVENTS'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table></div>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListEvents

function mainShowEvents($option, $view ){
    global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
	
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_SHOW_EVENTS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_SHOW_EVENTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   $deptquery = "";
 
   if($user->id){
$deptquery .= " (events_access = 1 || events_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) ";
   }elseif($deptid){
	    $deptquery .= " events_access = 1 && events_department = '".$deptid."' ";
   }else{
	    $deptquery .= " events_access = 1 ";	 
   }
   
 $where = ' WHERE publish = 1 && '.$deptquery.' && id = "'.$cirid.'" ';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(events_title) LIKE '%".$search."%'";
        $where .= " OR LOWER(events_title_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_location) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_location_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_organized_by) LIKE '%".$search."%'";
		$where .= " OR LOWER(events_organized_by_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(events_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(events_description_ar)) LIKE '%".$search."%' )";
    }    
 $database->SetQuery( "SELECT count(*) FROM #__eposts_events".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_events
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->events_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
			if ($tag == 'ar-AA') :
            $row->department = $departments->departments_name_ar;
			endif;
	   $title = strip_tags($title);
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->events_department."&Itemid=103");
			
			$title = $row->events_title;
			$descp = $row->events_description;
			$location = $row->events_location;
			$orgBy = $row->events_organized_by;
			if ($tag == 'ar-AA') :
				$title = $row->events_title_ar;
				$descp = $row->events_description_ar;
				$location = $row->events_location_ar;
				$orgBy = $row->events_organized_by_ar;
			endif;
			$title = strip_tags($title);
			$length = 100;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
       		$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowevents'));
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title_events"><?php echo $title; ?></div>
            <?php 
			if ($row->event_logo) : 
			
			$img = $filepath.'\\'.$row->event_logo;
		//	echo $img ;
			   $img = str_replace("\\", "/", $img);
	//		 echo $img;
			$filetype=string;       
	     $imgbinary = fread(fopen($img, "r"), filesize($img));
     // echo $imgbinary;
	   $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
			?>
            <div class="sub_title"><img src="<?php echo $aaa ; ?>" width="100" /></div>
            <?php endif; ?>
            <div class="body p2">
			<?php echo $descp; ?>
            </div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_EVENTS_LOCATION'); ?> <?php echo $location; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_EVENTS_START_DATE'); ?> <?php echo JText::sprintf('%s', JHtml::_('date', $row->events_startdate, JText::_('DATE_FORMAT_LC2'))); ?></div>
			<div class="sub_title"> <?php echo JText::_('COM_EPOSTS_MAIN_EVENTS_END_DATE'); ?> <?php echo JText::sprintf('%s', JHtml::_('date', $row->events_enddate, JText::_('DATE_FORMAT_LC2'))); ?></div>
            <?php if ($row->event_contact) : ?>
            <div class="sub_title"><?php print JText::_('COM_EPOSTS_MAIN_CONTACT') . ': ' . $row->event_contact; ?></div>
			<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_EVENTS_ORGANIZEDBY'); ?> <?php echo $orgBy; ?></div>
			</br>
            <?php endif; ?>
            <?php 
			if ($row->event_image) : 
			//$img = JRoute::_('index.php?option=com_eposts&view=loadimage&myfile='.$filepath.'\\'.$row->event_image);
			$img = $filepath.'\\'.$row->event_image;
		//	echo $img ;
			   $img = str_replace("\\", "/", $img);
	//		 echo $img;
			$filetype=string;       
	     $imgbinary = fread(fopen($img, "r"), filesize($img));
     // echo $imgbinary;
	   $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
			?>
            <div class="sub_title"><img src="<?php print $aaa; ?>" /></div>
            <?php endif; ?>
            </br>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowEvents

function mainListMedias($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_MEDIAS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_MEDIAS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   $deptid = (int)JRequest::getString('deptid', 0);
   
   $deptquery = "";
 
   if($user->id ){
$deptquery .= " (media_access = 1 || media_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) ";
   }else{
	    $deptquery .= " media_access = 1 ";	 
   }
   
    $where = ' WHERE 1 = 1 && publish = 1 && '.$deptquery.' ';
  

    $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}
	if ($search=="البحث بالإسم"){
	$search ="";
	}
	if ($announcements_expirydate=="Search by Calendar"){
	$announcements_expirydate ="";
	}
	if ($announcements_expirydate=="البحث عن طريق التقويم"){
	$announcements_expirydate ="";
	}
// echo  sprintf('%s',$announcements_expirydate,'DATE_FORMAT_LC5');   
   if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
  $announcements_expirydate1=date("Y-m-d", strtotime($announcements_expirydate)); 
	
   if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(media_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(media_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(media_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(media_description_ar)) LIKE '%".$search."%' )";
    } elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND media_department = '".$deparrtment."'";
        $where .= " AND  publishedon LIKE '".$announcements_expirydate."%'";
	
} elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(media_department) = '".$deparrtment."'";
	
	}  	
   //  echo ( "SELECT count(*) FROM #__eposts_dwnmedias".$where);
	
	$database->SetQuery( "SELECT count(*) FROM #__eposts_dwnmedias".$where);
   
	$total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_dwnmedias
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name,departments_name_ar "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->media_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
		if ($tag == 'ar-AA') :
				$row->department = $departments->departments_name_ar;
			endif;
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
    
?>
<?php
								JHTML::_('behavior.calendar');
								/*// for plugin
								 JPluginHelper::importPlugin('datepicker');	
								echo plgSystemDatePicker::calendar($row->news_newsdate, 'news_newsdate', 'news_newsdate', '%Y-%m-%d');*/
								 ?>
<form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<div class="forms_mainfilter"><table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area_calander" type="text"  name="announcements_expirydate" id="announcements_expirydate" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>') {this.value = '';}" ></td>
			<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
				<td>
			<select name="deparrtment" class="inputbox" >
			
				<option value="" ><?php echo JText::_('COM_EPOSTS_SELECT_DEPARTMENT'); ?></option>
				<?php

 

$database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "               
            );
   
       $listDepartments2 = $database->loadObjectList();

				foreach($listDepartments2 as $xdepartment2) { ?>
		<?php if ($tag == 'ar-AA') {
		$department_name = $xdepartment2->departments_name_ar;
		}else{
	$department_name = $xdepartment2->departments_name;
	}?>
	<option value="<?php echo $xdepartment2->id; ?>"<?php if (!(strcmp($xdepartment2->id,$deparrtment))) {echo "selected=\"selected\"";} ?>><?php echo $department_name; ?></option>
			<?php	} ?>
			</select></td>
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>';	

	echo $form;  ?>

     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <br/> 

 	   <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->media_department."&Itemid=103");
			$title = $row->media_name;
			$descp = $row->media_description;
			if ($tag == 'ar-AA') :
			$title = $row->media_name_ar;
			$descp = $row->media_description_ar;
			endif;
			$title = strip_tags($title);
			$extension = end(explode('.', $row->media_document));
			$length = 2000;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
            ?>
    
	   <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div id="form-colm-two">
			<div class="form-colm-left">
			<div class="form-list-title"><?php echo $title;?></div>
            <div class="form-list-decs"> 
			<p><?php echo $descp; ?></p>
            </div>
            </div>
			<div class="form-colm-right">
			<div class="sub_title"><span class="form-list-decs-head"><?php echo $row->department ?></span><br/><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
            <div class="sub_title"><div class="sub_title"><div class="form_download_icon"><a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->media_document); ?>">
   <?php if (($extension=="docx") or ($extension=="dot")or ($extension=="doc")or ($extension=="docm")or ($extension=="dotm")or ($extension=="dotx"))   { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-word.png" border="0" alt="" /></a></div></div>
   <?php }elseif ($extension=="pdf") { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-pdf.png" border="0" alt="" /></a></div></div>
  <?php }elseif (($extension=="xls") or ($extension=="xlt")or ($extension=="xlm")or ($extension=="xlsx")or ($extension=="xltx")or ($extension=="xltm"))   { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-excel.png" border="0" alt="" /></a></div></div>
   <?php }else { ?>
   
   <img src="components/com_eposts/assets/images/icons/forms-download.png" border="0" alt="" /><div class="forms-download-text"><?php print JText::_('COM_EPOSTS_FORMS_DOWNLOAD_TEXT'); ?></a></div></div></div>
   <?php } ?>
   </div></div>
			</div>
			</div>
			<div>&nbsp;</div>            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND_FORMS'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
<?php
//echo $footer; 
}//end funtion mainListMedias

function mainListDocuments($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
	$doc_type = JRequest::getCmd("doc");
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_DOCUMENTS_LIST_PAGE_TITLE'));

	$jdeptid = JRequest::getCmd('deptid');
	$deptid = !($jdeptid) ? $deptid : $jdeptid;
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_DOCUMENTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   
   $deptquery = "";
 
   if($user->id && $deptid && $doc_type == '1'){
$deptquery .= " (dms_access = 1 AND publish = '1' AND dms_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && dms_department = '".$deptid."' ";
   }elseif($user->id && $deptid && $doc_type == '2'){
$deptquery .= " (dms_access = 0 AND publish = '1' AND dms_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && dms_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " dms_access = 1  AND publish = '1' && dms_department = '".$deptid."' ";
   }else{
	    $deptquery .= " dms_access = 1  AND publish = '1' ";	 
   }
   
    $where = ' WHERE 1 = 1 && '.$deptquery.' ';
   
   //print $where;
   //exit;

   $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}
	if ($search=="البحث بالإسم"){
	$search ="";
	}
	if ($announcements_expirydate=="Search by Calendar"){
	$announcements_expirydate ="";
	}
	if ($announcements_expirydate=="البحث عن طريق التقويم"){
	$announcements_expirydate ="";
	}
// echo  sprintf('%s',$announcements_expirydate,'DATE_FORMAT_LC5');   
   if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
  $announcements_expirydate1=date("Y-m-d", strtotime($announcements_expirydate)); 
	 
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(dms_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(dms_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(dms_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(dms_description_ar)) LIKE '%".$search."%'";
		$where .= " OR LOWER(dms_title) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(dms_file_data)) LIKE '%".$search."%' )";
    } elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND media_department = '".$deparrtment."'";
        $where .= " AND  publishedon LIKE '".$announcements_expirydate."%'";
	
} elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(dms_department) = '".$deparrtment."'";
	
	}  	    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_dms".$where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
  $query = "SELECT * FROM #__eposts_dms
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->dms_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
			if ($tag == 'ar-AA') :
            $row->department = $departments->departments_name_ar;
			endif;
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }  JHTML::_('behavior.calendar');      
?>
 
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td class="main_filter">
           <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<div class="forms_mainfilter"><table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area_calander" type="text"  name="announcements_expirydate" id="announcements_expirydate" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>') {this.value = '';}" ></td>
			<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
				<td>
			<select name="deparrtment" class="inputbox" >
				<option value="" ><?php echo JText::_('COM_EPOSTS_SELECT_DEPARTMENT'); ?></option>

				<?php

 

$database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "               
            );
   
       $listDepartments2 = $database->loadObjectList();

				foreach($listDepartments2 as $xdepartment2) { ?>
		<?php if ($tag == 'ar-AA') {
		$department_name = $xdepartment2->departments_name_ar;
		}else{
	$department_name = $xdepartment2->departments_name;
	}?>
	<option value="<?php echo $xdepartment2->id; ?>"<?php if (!(strcmp($xdepartment2->id,$deparrtment))) {echo "selected=\"selected\"";} ?>><?php echo $department_name; ?></option>
			<?php	} ?>
			</select></td>
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>';	

	echo $form;  ?>
          </td>
      </tr>
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowdocuments&Itemid=327&deptid=".$row->dms_department."&cirid=".$row->id);
			$title = $row->dms_name;
			$descp = $row->dms_description;
			if ($tag == 'ar-AA') :
				$title = $row->dms_name_ar;
				$descp = $row->dms_description_ar;
			endif;
			$title = strip_tags($title);
$extension = end(explode('.', $row->dms_path));           
		   ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div id="form-colm-two">
			<div class="form-colm-left">
			<div class="form-list-title"><?php echo $title;?></div>
            <div class="form-list-decs"> 
			<?php echo $descp; ?>
            </div>
            </div>
            

			
						<div class="form-colm-right">
			<div class="sub_title"><span class="form-list-decs-head"><?php echo $row->department ?></span><br/><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
            <div class="sub_title"><div class="sub_title"><div class="form_download_icon"><a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->dms_path); ?>">
   <?php if (($extension=="docx") or ($extension=="dot")or ($extension=="doc")or ($extension=="docm")or ($extension=="dotm")or ($extension=="dotx"))   { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-word.png" border="0" alt="" /></a></div></div>
   <?php }elseif ($extension=="pdf") { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-pdf.png" border="0" alt="" /></a></div></div>
  <?php }elseif (($extension=="xls") or ($extension=="xlt")or ($extension=="xlm")or ($extension=="xlsx")or ($extension=="xltx")or ($extension=="xltm"))   { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-excel.png" border="0" alt="" /></a></div></div>
   <?php }else { ?>
   
   <img src="components/com_eposts/assets/images/icons/forms-download.png" border="0" alt="" /><div class="forms-download-text"><?php print JText::_('COM_EPOSTS_FORMS_DOWNLOAD_TEXT'); ?></a></div></div></div>
   <?php } ?>
   </div></div>
			</div>
			</div>
			
			
			
			
			<div>&nbsp;</div>   
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">

<?php
	//echo $footer; 
}//end funtion mainListDocuments

function mainShowDocuments($option, $view ){

	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_SHOW_DOCUMENTS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_SHOW_DOCUMENTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   $deptid = (int)JRequest::getString('deptid', 0);
   
   $deptquery = "";
 
   if($user->id && $deptid){
$deptquery .= " (dms_access = 1 || dms_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && dms_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " dms_access = 1 && dms_department = '".$deptid."' ";
   }else{
	    $deptquery .= " dms_access = 1 ";	 
   }
   
    $where = ' WHERE 1 = 1 && '.$deptquery.' && id = "'.$cirid.'"';


    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(dms_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(dms_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(dms_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(dms_description_ar)) LIKE '%".$search."%'";
		$where .= " OR LOWER(dms_title) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(dms_file_data)) LIKE '%".$search."%' )";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_dms".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_dms
              $where
              ORDER BY id DESC";                                                          
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->dms_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
		if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
	    endif;	
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->dms_department."&Itemid=103");
			$title = $row->dms_name;
			$descp = $row->dms_description;
			if ($tag == 'ar-AA') :
				$title = $row->dms_name_ar;
				$descp = $row->dms_description_ar;
			endif;
			$title = strip_tags($title);
			$length = 20;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			
			$breadcrumbs =& $mainframe->getPathWay();
			$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowdocuments'));
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title"><?php echo $title; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_DOWNLOAD'); ?> <a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->dms_path); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a></div>
            <div class="body">
			<?php echo $descp; ?>
            </div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowDocuments


function mainListNewHires($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_DOCUMENTS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_DOCUMENTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   $deptid = (int)JRequest::getString('deptid', 0);
   
 /*  $deptquery = "";
 
   if($user->id && $deptid){
$deptquery .= " (newhires_access = 1 || newhires_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && newhires_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " newhires_access = 1 && newhires_department = '".$deptid."' ";
   }else{
	    $deptquery .= " newhires_access = 1 ";	 
   }
   $where = ' WHERE publish = 1 && '.$deptquery.' ';*/
   
    $where = ' WHERE 1=1 ';
   

     $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}
	if ($search=="البحث بالإسم"){
	$search ="";
	}
	if ($announcements_expirydate=="Search by Calendar"){
	$announcements_expirydate ="";
	}
	if ($announcements_expirydate=="البحث عن طريق التقويم"){
	$announcements_expirydate ="";
	}
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
 $announcements_expirydate1=date("Y-m-d", strtotime($announcements_expirydate));    
   if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(newhires_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(newhires_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_mobile) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_email) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(newhires_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(newhires_description_ar)) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_designation) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_designation_ar) LIKE '%".$search."%' )";
    }  elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND newhires_department = '".$deparrtment."'"; 
        $where .= " AND  publishedon LIKE '".$announcements_expirydate."%'";
	
	
}elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(newhires_department) = '".$deparrtment."'";
	
	}  	
    // echo ( "SELECT count(*) FROM #__eposts_newhires".$where);   
    $database->SetQuery( "SELECT count(*) FROM #__eposts_newhires ".$where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_newhires
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       /*$database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->newhires_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
		if ($tag == 'ar-AA') :
		    $row->department = $departments->departments_name_ar;
		endif;	*/
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    } 
JHTML::_('behavior.calendar');	
?>
<form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<div class="newhires_mainfilter"><table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area_calander" type="text"  name="announcements_expirydate" id="announcements_expirydate" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>') {this.value = '';}" ></td>
			<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
				<td>
			<select name="deparrtment" class="inputbox" >
				<option value="" ><?php echo JText::_('COM_EPOSTS_SELECT_DEPARTMENT'); ?></option>

				<?php

 

$database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "               
            );
   
       $listDepartments2 = $database->loadObjectList();

				foreach($listDepartments2 as $xdepartment2) { ?>
		
	<?php if ($tag == 'ar-AA') {
		$department_name = $xdepartment2->departments_name_ar;
		}else{
	$department_name = $xdepartment2->departments_name;
	}?>
	<option value="<?php echo $xdepartment2->id; ?>"<?php if (!(strcmp($xdepartment2->id,$deparrtment))) {echo "selected=\"selected\"";} ?>><?php echo $department_name; ?></option>
			<?php	} ?>
			</select></td>
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>';	

	echo $form;  ?>
     <div class="newhires_bodytable"><table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
     
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshownewhires&page=home&cirid=".$row->id);
			$title = $row->newhires_name;
			$desination = $row->newhires_designation;
			$descp = $row->newhires_description;
			if ($tag == 'ar-AA') :
				$title = $row->newhires_name_ar;
				$desination = $row->newhires_designation_ar;
				$descp = $row->newhires_description_ar;
			endif;
			$title = strip_tags($title);
			$length = 2000;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
			$config =&JFactory::getConfig();
 $filepath = $config->getValue( 'config.file_path' );
 $reverse_file_path = $config->getValue( 'config.reverse_file_path' );
 $reverse_filepath_storage = $config->getValue( 'config.reverse_filepath_storage' ); 
if($row->newhires_picture){
 //$img = $filepath.'\\'.$row->newhires_picture;
  $img = $reverse_filepath_storage.$row->newhires_picture;
			  
			   $img = str_replace("\\", "/", $img);
	//		 echo $img;
			//$filetype=string;       
	     //$imgbinary = fread(fopen($img, "r"), filesize($img));
     // echo $imgbinary;
	   $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
	   $aaa= $img;
}
else {
$aaa= 'components/com_eposts/assets/images/icons/staff-m-pic.png';

}
JHTML::_("behavior.mootools");
	$document->addStyleSheet(JURI::base() . 'components/com_igallery/cerabox.css');
	$document->addScript(JURI::base() . 'components/com_igallery/cerabox.min.js');
	$document->addScriptDeclaration("
		document.addEvent('domready', function() {
			$$('.thumbs_div a').cerabox({
				group: false,
				displayTitle: false
			});
		});
	");
			if (strtotime($row->expirydate) > time()) :
            ?>
        <tr >
            
       
    <!--<td width="25%">
    <?php 
	    $img = str_replace('\\', '/', $row->newhires_picture);
	    echo '<a href="'.$link.'" target="' . $target . '"><img src="'.$img.'" width="200px" height="120px" style="margin:10px;" border="0" alt="" /></a>';
	?>
    </td> -->
  <td valign="top">
			
<div id="resourcesoffices">	
		<div class="resoffice"><div class="resofficeareapic">
		<div class="thumbs_div">
					
                		<a href="#$img_my<?php echo $i; ?>" >
					<?php  $document->addScriptDeclaration(' var img_my'.$i.' = "<img src=\"'.$aaa.'\" />";'); ?>
                        <img src="<?php echo $aaa; ?>" border="0" alt="" width="90px" height="100px" />
                    </a></div></div>
		<div class="resofficearea">
		
		
		<div class="publish-news">
			<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"> <?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
			
</div>


		<div class="title_news"><?php echo $title; ?></div> </br>
            <div class="resofficedesc">
<span class="resofficedescspan"><?php echo $desination; ?></span>
<div class="resofficedescspan"> <?php echo $row->newhires_mobile; ?></div>
<div class="resofficedescspan"><?php echo $row->newhires_email; ?> </div>
<p></p>
<div class="resofficedescspan"><?php echo $descp; ?> </div>

</div></div>
			
			
			</div>
			
		</div>	


           
    </td>
      
           
            
        </tr>
        <?php $k = 1 - $k; endif; } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND_NEW_RECRUITS'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table></div>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListNewHires

function mainShowNewHires($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_SHOW_NEWHIRES_LIST_PAGE_TITLE'));
	//file://hqdintdev2/media/albumSSSSS1.png
				$config =& JFactory::getConfig();
                $filepath = $config->getValue( 'config.file_path' );
				 $reverse_file_path = $config->getValue( 'config.reverse_file_path' );
  $reverse_filepath_storage = $config->getValue( 'config.reverse_filepath_storage' );
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_SHOW_NEWHIRES_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid////////////////////////////
   $cirid = (int)JRequest::getString('cirid', 0);
   $deptid = (int)JRequest::getString('deptid', 0);
   
/*   $deptquery = "";
 
   if($user->id && $deptid){
$deptquery .= " (newhires_access = 1 || newhires_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && newhires_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " newhires_access = 1 && newhires_department = '".$deptid."' ";
   }else{
	    $deptquery .= " newhires_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' && id = "'.$cirid.'" ';*/
  $where = ' WHERE 1 = 1 && id = "'.$cirid.'" ';

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(newhires_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(newhires_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_mobile) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_email) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(newhires_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(newhires_description_ar)) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_designation) LIKE '%".$search."%'";
		$where .= " OR LOWER(newhires_designation_ar) LIKE '%".$search."%' )";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_newhires ".$where);
    $total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_newhires
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       /*$database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->newhires_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
		if ($tag == 'ar-AA') :
		    $row->department = $departments->departments_name_ar;
		endif;*/	
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->newhires_department."&Itemid=103");
			$title = $row->newhires_name;
			$desination = $row->newhires_designation;
			$descp = $row->newhires_description;
			if ($tag == 'ar-AA') :
				$title = $row->newhires_name_ar;
				$desination = $row->newhires_designation_ar;
				$descp = $row->newhires_description_ar;
			endif;
			$title = strip_tags($title);
			$length = 80;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
			$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainlistnewhires'));
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title_newhire"><?php echo $title; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_SHOW_DESIGNATION_LIST_PAGE_TITLE'); ?> <?php echo $desination; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_SHOW_MOBILE_LIST_PAGE_TITLE'); ?> <?php echo $row->newhires_mobile; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_SHOW_EMAIL_LIST_PAGE_TITLE'); ?> <?php echo $row->newhires_email; ?></div>
            <div>
    <?php 
	$img = '';
			  if ($row->newhires_picture != '') {
					//$img = JRoute::_('index.php?option=com_eposts&view=loadimage&myfile='.$filepath.'\\'.$row->newhires_picture);
					//$img = 'file:'.$filepath.'\\'.$row->newhires_picture;
                    //$img = str_replace("\\", "/", $img);
					$img = $reverse_filepath_storage.$row->newhires_picture;
                    $img = str_replace("\\", "/", $img);
			  }
	    echo '<img src="'.$img.'" style="margin:10px;" border="0" alt="" width="100" />';
	?>
            </div>
            <div class="body p2">
			<?php echo $descp; ?>
            </div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowNewHires

///////////////////////////////////////////New Pages Phase II//////////////////////////

function mainListStaffDirectories($option, $view ){

global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;

$language = &JFactory::getLanguage();

$tag = $language->get('tag');

$user = &JFactory::getUser();

$database = &JFactory::getDBO();

    $document=& JFactory::getDocument();

    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_DOCUMENTS_LIST_PAGE_TITLE'));

 

$menu = &JSite::getMenu();

$active = $menu->getActive();

 

//print_r($active->id);




$jdeptid = JRequest::getCmd('deptid');

$deptid = !($jdeptid) ? $deptid : $jdeptid;

                            

    $can_upload = false;

    

    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_DOCUMENTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');

//echo $html_form;

$html_form = '';  

 

    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

//////////////////////////////////////////////////

   

   $deptquery = "";

 

   if($deptid && $active->id != 106){

      $deptquery .= " && id IN (SELECT user_id FROM #__eposts_department_members WHERE department_id = '".$deptid."') ";

   }

   

    //$where = ' WHERE 1 = 1 && '.$deptquery.' ';

$where = ' WHERE 1 = 1  && block != 1 '.$deptquery.' ';

   




    $search = JRequest::getVar('search', '');

$alpha = JRequest::getVar('alpha', '');

//echo $alpha; 

//echo $uri;

if(mb_strtolower( $search )== "find people..."){

$search="";

} 

//echo $search;

$staffdirectory_department = JRequest::getVar('staffdirectory_department', '');

$deptquery = "";

 

  /* if($staffdirectory_department){

      $deptquery .= " && id IN (SELECT user_id FROM #__eposts_department_members WHERE department_id = '".$staffdirectory_department."') ";

    $where = ' WHERE 1 = 1 '.$deptquery.' ';

   }*/ 

   

$session = JFactory::getSession();

 

    if (get_magic_quotes_gpc()) {

        $search = stripslashes( $search );

    }    

   

if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));

        $where .= " AND ( LOWER(name) LIKE '%".$search."%'";

        $where .= " OR LOWER(designation) LIKE '%".$search."%'";

$where .= " OR LOWER(phone) LIKE '%".$search."%' )";

 

    }   

if (($search=="") && ($staffdirectory_department=="")) {

        $alpha =  $database->getEscaped( trim( mb_strtolower( $alpha )));

        $where .= " AND  name LIKE '".$alpha."%'";

   

    } 

 

 

 

 

 

if ( $staffdirectory_department ) {

        $staffdirectory_department =  $database->getEscaped( trim( mb_strtolower( $staffdirectory_department )));

        

$where .= " AND LOWER(department) LIKE '%".$staffdirectory_department."%'";

    

$session->set('where',$where);

} 




$where1=$session->get('where');

if($where1==""){

$database->SetQuery( "SELECT count(*) FROM #__users ".$where);

}else{

  

 

 $database->SetQuery( "SELECT count(*) FROM #__users ".$where1);

  }

//echo ( "SELECT count(*) FROM #__users ".$where);

$total = $database->loadResult();  

   $limit = intval( JRequest::getInt( 'limit',0 ) );

 

   

   

   if(!$limit){

 $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);

      

       $query = "SELECT * FROM #__users $where ORDER BY name ASC";                                                         




   }else{

  $session->set('limit', $limit);

 

  $where1=$session->get('where');

  $query = "SELECT * FROM #__users $where1 ORDER BY name ASC";

   }

   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );

    jimport('joomla.html.pagination'); 

    $pageNav = new JPagination( $total, $limitstart, $limit );

    if ($pageNav->limitstart != $limitstart){

       $session->set('jdlimitstart', $pageNav->limitstart);

        $limitstart = $pageNav->limitstart;

     }

//echo '<br />'; 

//echo $query;

$database->SetQuery( $query ,$pageNav->limitstart, $pageNav->limit );

    $rows = $database->loadObjectList();

 

 

$uri2=substr_replace(JURI::root(),'',-1).'?option=com_eposts&view=mainliststaffdirectories&Itemid=106';

$database->setQuery("SELECT DISTINCT department "

                . "\n FROM #__users ORDER BY department ASC  "

 

            );

        $listDepartments = $database->loadObjectList();

$config =& JFactory::getConfig();

$host = $config->getValue( 'host' ); 

$user = $config->getValue( 'user' ); 

$password = $config->getValue( 'password' ); 

 $db = $config->getValue( 'db' ); 




 //echo substr_replace(JURI::root(),'',-1).'/configuration.php';;

//echo JPATH_BASE .DS.'configuration.php';

//echo substr_replace(JURI::root(),'',-1).'/configuration.php';; 

 

?>

<style>

</style>
<script language="JavaScript" type="text/javascript" src="components\com_eposts\suggest.js"></script>
 
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" >
      <tr>
          <td class="main_filter">
		  <!-- Alphabatic Filter AWK -->
		  
		  <div class="staffsearchfilter">
		  <div class="alphafilter">  
		  
			<table class ="alphafiltertable">
			<tr>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=A';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">A </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=B';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">B </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=C';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">C </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=D';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">D </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=E';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">E </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=F';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">F </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=G';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">G </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=H';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">H </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=I';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">I </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=J';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">J </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=K';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">K </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=L';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">L </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=M';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">M </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=N';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">N </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=O';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">O </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=P';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">P </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=Q';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">Q </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=R';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">R </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=S';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">S </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=T';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">T </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=U';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">U </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=V';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">V </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=W';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">W </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=X';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">X </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=Y';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">Y </a>
				</td>
				<td style="min-width:18px; min-height:22px; background-color:#1aa2de; text-align:center;">
					<a href="<?php echo $uri2.'&alpha=Z';?>" onChange="document.uploadForm.submit();" style="color:#ffffff !important;">Z </a>
				</td>
				
			</tr>
			<tr><td style="min-height:5px;"></td></tr>
		  </table>

		  
		  </div>
	<div class="searchfilterarea">
		<div class="searchbytext">
		  <!-- <strong>Search by Name</strong>  -->
		  <?php echo JText::_('')." "; ?>

				<input type="text" id="dbTxt" name="search" value="<?php echo JText::_('COM_EPOSTS_FINDPEOPLE'); ?>"  alt="Search Criteria" onKeyUp="searchSuggest();" onSubmit="document.uploadForm.submit();" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_FINDPEOPLE'); ?>';}"  onfocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_FINDPEOPLE'); ?>') {this.value = '';}"  /><?php //}  ?> <button type="submit" onSubmit="document.uploadForm.submit();"><!--<img src="/templates/emiratespost/images/icons/search-button.png">--></button>
				<div id="layer1"></div>
			</div>			  
		 <div class="searchbydept">  
		 <!-- <strong>Search by Department</strong>  -->
		  <select id="staffdirectory_department" name="staffdirectory_department"  onChange="document.uploadForm.submit();" >
			<?php  if($staffdirectory_department !=""){  ?>
			
			<option value="<?php echo $staffdirectory_department; ?>"><?php echo $staffdirectory_department; ?></option>
			<?php  } //else {  ?>
			<option value=""><?php echo JText::_('COM_EPOSTS_STAFFSELECT'); ?></option>
	<?php // } ?>
	<?php foreach($listDepartments as $xdepartment) {
if($xdepartment->department !=""){ 
	?>
	
	<option value="<?php echo $xdepartment->department; ?>"><?php echo $xdepartment->department;  ?></option>
	<?php } } ?>
</select>
</div>
    </div>      
           
			<?php // echo JText::_('')." ";
                  // echo $pageNav->getLimitBox();
            ?></div>
          </td>
      </tr>
        <?php
	
     $config =& JFactory::getConfig();
       $filepath = $config->getValue( 'config.file_path' );
	    $reverse_file_path = $config->getValue( 'config.reverse_file_path' );
        $reverse_filepath_storage = $config->getValue( 'config.reverse_filepath_storage' );
	 //$filepath=$filepath1.'/avatar';
	 
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowstaffdirectories&cirid=".$row->id);
if ($row->avatar_url!=""){          
		 
//$img = $filepath.$row->avatar_url;
$img = $reverse_filepath_storage.'avatar\\'.$row->avatar_url;		
				$img = str_replace("\\", "/", $img);
					
					
			//echo $img ;
		//$filetype=string;       
	    //$imgbinary = fread(fopen($img, "r"), filesize($img));
     // echo $imgbinary;
	   $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
	   $aaa= $img;
		}
$aboutname = explode(" ", $row->name);		
			?>
                <tr class="<?php echo "row$k"; ?>">
            <td valign="top" style="background-color:#ffffff;">
				<table style="background-color:#ffffff;" border="0">
					<tr>
						<td style="background-color:#ffffff; ">
							
							<div class="sub_title">
							<?php if ($row->avatar_url==""){ ?>
							<img src="components/com_eposts/assets/images/icons/staff-m-pic.png" border="0" alt="" /><?php } else { ?>
								<img src="<?php echo $aaa ?>" border="0" alt="" align="right" width="100px" height="110px" /><?php } ?>
							</div>
						</td>
						<td style="background-color:#ffffff; padding-left: 20px;">
							<div class="staff_detailsarea">
							<div class="staff_topdiv">
							<table border="0">
								<tr>
									<td style="background-color:#ffffff;">
										<div  class="staff_name_title" class="fontCSS" ><?php echo $row->name; ?></div>
									</td>
									
								</tr>
							</table>
							</div>
							<div class="staff_bottomdiv">
							<table  border="0"  cellspacing="0" cellpadding="10" style="background-color:#f4f4f4 !important;padding:10px;" >
								<tr>
									<td width="350px">
										<div class="dept_sub_title"><?php echo JText::_('COM_EPOSTS_DESIGNATION'); ?> <span class="dept_sub_title_name"><?php echo $row->designation; ?></span></div>
									</td>
									<td width="150px">
										<div class="staffphoneicon"> 
											<img src="components/com_eposts/assets/images/icons/phone-icon.png" border="0" alt="Phone" align="right"  title="Phone" width="26px" height="24px" />
										</div>
										<!--<div class="staffphone"> &nbsp; &nbsp;<?php //echo $row->phone; ?></div>-->
										<div class="staffmobile"> &nbsp; &nbsp;<?php echo $row->phone; ?></div>
									</td>
									<td width="300px">
										<div class="stafffaxicon"> 
											<img src="components/com_eposts/assets/images/icons/fax-icon.png" border="0" alt="Fax" title="Fax" align="right" width="26px" height="24px" />
										</div>
										<div class="stafffax"> &nbsp; &nbsp;<?php echo $row->fax; ?></div>
									</td>
									
								</tr>
								
								<tr >
									<td width="350px">
										<div class="dept_sub_title"><?php echo JText::_('COM_EPOSTS_DEPAETMENTS'); ?> <span class="dept_sub_title_name"><?php echo $row-> department; ?></span></div>
									</td>
									<td width="150px">
										<div class="staffmobileicon"> 
											<img src="components/com_eposts/assets/images/icons/mobile-icon.png" border="0" alt="Mobile" title="Mobile" align="right" width="26px" height="24px" />
										</div>
										<div class="staffmobile"> &nbsp; &nbsp;<?php echo $row->cell; ?></div>
									</td>
									<td width="300px">
										<div class="staffemailicon"> 
											<img src="components/com_eposts/assets/images/icons/email-icon.png" border="0" alt="email" title="email" align="right" width="26px" height="24px" />
										</div>
										<div class="staffemail"> &nbsp; &nbsp;<?php echo $row->email; ?></div>
									</td>
								</tr>
								

								
							</table>
							</div>	
							</div>
						</td>
					</tr>
					
					<tr>	
						<td>
						</td>
						
						<td>
                          <?php if($row->descp){ ?>						
						<div class="user-about-decp">
							<span class="user-about-decp-head"><?php echo JText::_('About '); ?><?php echo $aboutname[0] ; ?></span> <br/>
								<?php echo $row->descp; ?>
							</div>
						 <?php } ?>		
						</td>
					</tr>
					
				</table>
			
			
                        
            
            
            
            <div>&nbsp;</div>
            
        </tr>

        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND_STAFF_DIRECTORY'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
//echo $footer; 

}//end funtion mainListStaffDirectories


function mainShowStaffDirectories($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_SHOW_NEWHIRES_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_SHOW_NEWHIRES_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid////////////////////////////
   $cirid = (int)JRequest::getString('cirid', 0);
   $deptid = (int)JRequest::getString('deptid', 0);
   
   $deptquery = "";

   
    $where = ' WHERE 1 = 1 && block != 1  && id = "'.$cirid.'" ';
 $database->SetQuery( "SELECT count(*) FROM #__users".$where);
    $total = $database->loadResult();    
    
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__users
              $where
              ORDER BY name ASC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->newhires_department."&Itemid=103");
			$title = $row->newhires_name;
			$desination = $row->newhires_designation;
			$descp = $row->newhires_description;
			if ($tag == 'ar-AA') :
				$title = $row->newhires_name_ar;
				$desination = $row->newhires_designation_ar;
				$descp = $row->newhires_description_ar;
			endif;
			$title = strip_tags($title);
			$where = 'WHERE id IN(SELECT department_id FROM #__eposts_department_members WHERE user_id = "' . $row->id . '")';
			$query = "SELECT * FROM #__eposts_departments
              $where
              ORDER BY departments_name ASC";
                                                                
			$database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
			$departments = $database->loadObjectList();
			$listDepartment = '';
			foreach($departments as $key => $department) {
				if ($key == '0') :
					$listDepartment .= $department->departments_name;
				else :
					$listDepartment .= ',' . $department->departments_name;
				endif;
			}
			
			$breadcrumbs =& $mainframe->getPathWay();
			$breadcrumbs->addItem($row->name, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowstaffdirectories'));
            ?>
        <tr class="<?php echo "row$k"; ?>">
			<td valign="top">
			<div class="title"><?php echo $row->name; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_DESIGNATION'); ?> <?php echo $row->designation; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_DEPAETMENTS'); ?> <?php print $listDepartment; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_PHONE'); ?> <?php echo $row->phone; ?></div>
            
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MOBILE'); ?> <?php echo $row->cell; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_FAXNM'); ?> <?php echo $row->fax; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_EMAIL'); ?> <?php echo $row->email; ?></div>
            <div class="body">
			<?php echo $row->descp; ?>
            </div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowStaffDirectories

function mainListOrgCharts($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
    $tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_ORGCHARTS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
   // $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_ORGCHARTS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid////////////////////////////
    $where = ' WHERE 1 = 1 ';
  

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
   /* if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(departments_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(departments_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(departments_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(departments_description_ar)) LIKE '%".$search."%')";
    } */   
  $database->SetQuery( "SELECT count(*) FROM #__eposts_departments ".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_departments
              $where
              ORDER BY departments_name ASC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
$uri = '';	
?> <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
     <tr>
          <td>
            <?php 
			$database->setQuery('SELECT * FROM #__eposts_visions WHERE id = 1');
            $TopManagement = $database->loadObject(); 

			$toddesc = $TopManagement->visions_longdescription;

			if ($tag == 'ar-AA') :
				$toddesc = $TopManagement->visions_longdescription_ar;
			endif;
			
			echo $toddesc;
			?>
          </td>
      	</tr>
      	<!--<tr>
          <td align="right">
            <?php //echo JText::_('Search')." "; ?>
                <input type="text" name="search" value="<?php //echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php //echo JText::_('Limit')." ";
                  //echo $pageNav->getLimitBox();
            ?>
          </td>
      	</tr>-->
        <tr>
          <td align="center" colspan="7">&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="7">
		  	<div><img src="images/orgchart.png" border="0" alt="" width="960" usemap="#Map" />
<map name="Map" id="Map">
  <area shape="rect" coords="397,75,610,126" href="#" />
  <area shape="rect" coords="650,54,896,115" href="#" />
  <area shape="rect" coords="398,154,609,204" href="#" />
  <area shape="rect" coords="108,190,319,242" href="#" />
  <area shape="rect" coords="101,248,325,314" href="#" />
  <area shape="rect" coords="650,253,894,311" href="#" />
  <area shape="rect" coords="671,318,890,369" href="#" />
  <area shape="rect" coords="671,377,897,430" href="#" />
  <area shape="rect" coords="100,319,327,375" href="#" />
  <area shape="rect" coords="106,383,327,457" href="#" />
  <area shape="rect" coords="670,436,897,480" href="#" />
  <area shape="rect" coords="100,474,330,534" href="#" />
  <area shape="rect" coords="670,485,891,541" href="#" />
  <area shape="rect" coords="115,593,327,656" href="#" />
  <area shape="rect" coords="397,596,608,659" href="#" />
  <area shape="rect" coords="679,594,888,657" href="#" />
</map></div>
          </td>
    	</tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="mainlistorgcharts" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListOrgCharts

function mainShowOrgCharts($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_SHOW_NEWHIRES_LIST_PAGE_TITLE'));
    $config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' ); 
	                         
    $can_upload = false;
    
   // $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_SHOW_NEWHIRES_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid////////////////////////////
$cirid = (int)JRequest::getString('cirid', $_SESSION['deptid']);
$database->setQuery('SELECT * FROM #__eposts_departments WHERE id = "'.$cirid.'"');
            $row = $database->loadObject(); 

			
if(!empty($row->departments_map)){
            //$map_image = JRoute::_('index.php?option=com_eposts&view=loadimage&myfile='.$filepath.'\\'.$row->departments_map);
			$map_image = 'file:'.$filepath.'\\'.$row->departments_map;
            $map_image = str_replace("\\", "/", $map_image);	
   echo "<img src='".$map_image."' border='0' width='950' />";
}else{
   echo "Map does not exist."; 
}
	//echo $footer; 
}//end funtion mainShowOrgCharts

function mainListLinks($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_LINKS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_LINKS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid////////////////////////////
    $where = ' WHERE 1 = 1 ';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(links_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(links_link) LIKE '%".$search."%') ";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_links". $where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_links
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){ 
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;      
    }    

    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td class="main_filter">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$title = $row->links_name;
			$newlink = $row->links_link;
			if ($tag == 'ar-AA') :
				$title = $row->links_name_ar;
				$newlink = $row->links_link_ar;
			endif;
$title = strip_tags($title);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title"><a href="<?php echo $newlink; ?>" target="_blank"><?php echo $title; ?></a></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <div class="body">
			<a href="<?php echo $newlink; ?>" target="_blank"><?php echo $row->links_link; ?></a>
            </div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListLinks

function mainShowEmpInfo($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_SHOW_EMPINFO_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_SHOW_EMPINFO_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid////////////////////////////
?>
new Page
<?php
	//echo $footer; 
}//end funtion mainShowEmpInfo

function mainAddEmpInfo($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_SHOW_ADDEMPINFO_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_SHOW_ADDEMPINFO_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid////////////////////////////
?>
new Page
<?php
	//echo $footer; 
}//end funtion mainAddEmpInfo

	///////////////////////////////////Depevloment Phase IV ////////////////////////////
//new Announcements
function newAnnouncements($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Announcements'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);
	
    $row = new jlist_announcements( $database );
    $row->load( $cirid );
    $cdipartment = array();
 
    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'announcements_department',
        'class="inputbox" ', 'value', 'text', $row->announcements_department);
		
	
	$cdAccess[] = JHTML::_('select.option',0,'No');
	$cdAccess[] = JHTML::_('select.option',1,'Yes');
	$departmentAccess =	JHTML::_('select.genericlist',$cdAccess, 'announcements_access',
        'class="inputbox" ', 'value', 'text', $row->announcements_access);
	
	$cdshow[] = JHTML::_('select.option',0,'No');
	$cdshow[] = JHTML::_('select.option',1,'Yes');
	$showOnTop =	JHTML::_('select.genericlist',$cdshow, 'announcements_showontop',
        'class="inputbox" ', 'value', 'text', $row->announcements_showontop);	
		
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);
		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Announcements'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.announcements_name.value == "" 
			 || form.announcements_name_ar.value == ""
			 || form.announcements_expirydate.value == ""
			 || tinyMCE.get('announcements_description').getContent()== ""
			 || tinyMCE.get('announcements_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listannouncements&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Announcement') :  JText::_('Edit Announcement');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newannouncements'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit Announcement' : 'Add Announcement';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong>Announcement Title (English)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="announcements_name" value="<?php echo htmlspecialchars($row->announcements_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Announcement Title (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="announcements_name_ar" value="<?php echo htmlspecialchars($row->announcements_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Announcement Category (English):</strong><td>
                                <?php $anCat = $row->announcement_category; ?>
                                <td>
                                	<select name="announcement_category">
                                    	<option value="">- Select -</option>
                                        <option <?php print $anCat == 'New Baby Girl' ? 'selected="selected"' : ''; ?> value="New Baby Girl">New Baby Girl</option>
                                        <option <?php print $anCat == 'New Baby Boy' ? 'selected="selected"' : ''; ?> value="New Baby Boy">New Baby Boy</option>
                                        <option <?php print $anCat == 'Death Announcement – Staff' ? 'selected="selected"' : ''; ?> value="Death Announcement – Staff">Death Announcement – Staff</option>
                                        <option <?php print $anCat == 'Death Announcement – Staff  Family Member' ? 'selected="selected"' : ''; ?> value="Death Announcement – Staff  Family Member">Death Announcement – Staff  Family Member</option>
                                        <option <?php print $anCat == 'Graduation Greetings' ? 'selected="selected"' : ''; ?> value="Graduation Greetings">Graduation Greetings</option>
                                        <option <?php print $anCat == 'Wedding Greetings' ? 'selected="selected"' : ''; ?> value="Wedding Greetings">Wedding Greetings</option>
                                        <option <?php print $anCat == 'Competitions' ? 'selected="selected"' : ''; ?> value="Competitions">Competitions</option>
                                        <option <?php print $anCat == 'Volunteering' ? 'selected="selected"' : ''; ?> value="Volunteering">Volunteering</option>
                                        <option <?php print $anCat == 'Events' ? 'selected="selected"' : ''; ?> value="Events">Events</option>
                                        <option <?php print $anCat == 'CEO Message' ? 'selected="selected"' : ''; ?> value="CEO Message">CEO Message</option>
                                        <option <?php print $anCat == 'Employee Satisfaction Survey' ? 'selected="selected"' : ''; ?> value="Employee Satisfaction Survey">Employee Satisfaction Survey</option>
                                        <option <?php print $anCat == 'Campaign' ? 'selected="selected"' : ''; ?> value="Campaign">Campaign</option>
                                        <option <?php print $anCat == 'CSR' ? 'selected="selected"' : ''; ?> value="CSR">CSR</option>
                                        <option <?php print $anCat == 'Government Communication' ? 'selected="selected"' : ''; ?> value="Government Communication">Government Communication</option>
                                        <option <?php print $anCat == 'Excellence Newsletter' ? 'selected="selected"' : ''; ?> value="Excellence Newsletter">Excellence Newsletter</option>
                                        <option <?php print $anCat == 'Competitions' ? 'selected="selected"' : ''; ?> value="Competitions">Competitions</option>
                                        <option <?php print $anCat == 'Sheikh Khalifa Award' ? 'selected="selected"' : ''; ?> value="Sheikh Khalifa Award">Sheikh Khalifa Award</option>
                                        <option <?php print $anCat == 'Events' ? 'selected="selected"' : ''; ?> value="Events">Events</option>
                                        <option <?php print $anCat == 'Internal recruitment' ? 'selected="selected"' : ''; ?> value="Internal recruitment">Internal recruitment</option>
                                        <option <?php print $anCat == 'Welcome on Board' ? 'selected="selected"' : ''; ?> value="Welcome on Board">Welcome on Board</option>
                                        <option <?php print $anCat == 'HR Policies' ? 'selected="selected"' : ''; ?> value="HR Policies">HR Policies</option>
                                        <option <?php print $anCat == 'Rules & Regulations' ? 'selected="selected"' : ''; ?> value="Rules & Regulations">Rules & Regulations</option>
                                        <option <?php print $anCat == 'Health Insurance' ? 'selected="selected"' : ''; ?> value="Health Insurance">Health Insurance</option>
                                        <option <?php print $anCat == 'Events' ? 'selected="selected"' : ''; ?> value="Events">Events</option>
                                        <option <?php print $anCat == 'New Stamp Release' ? 'selected="selected"' : ''; ?> value="New Stamp Release">New Stamp Release</option>
                                        <option <?php print $anCat == 'Customer Service Tips' ? 'selected="selected"' : ''; ?> value="Customer Service Tips">Customer Service Tips</option>
                                        <option <?php print $anCat == 'Events' ? 'selected="selected"' : ''; ?> value="Events">Events</option>
                                        <option <?php print $anCat == 'System Down' ? 'selected="selected"' : ''; ?> value="System Down">System Down</option>
                                        <option <?php print $anCat == 'Systems / Applications Not Available' ? 'selected="selected"' : ''; ?> value="Systems / Applications Not Available">Systems / Applications Not Available</option>
                                        <option <?php print $anCat == 'Flashes' ? 'selected="selected"' : ''; ?> value="Flashes">Flashes</option>
										<option <?php print $anCat == 'Action World' ? 'selected="selected"' : ''; ?> value="Action World">Action World</option>
									</select>
                                </td>
                           	</tr>
                            <tr>
                                <td align="right"><strong>Announcement Category (Arabic):</strong><td>
                                <?php
                                $anCat2 = $row->announcement_category_ar;
								$opt_01 = JText::_('COM_EPOSTS_ANN_OPT_01');
								$opt_02 = JText::_('COM_EPOSTS_ANN_OPT_02');
								?>
                                <td>
                                	<select name="announcement_category_ar">
                                    	<option value="">- Select -</option>
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_01') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_01'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_01'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_02') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_02'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_02'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_03') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_03'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_03'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_04') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_04'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_04'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_05') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_05'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_05'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_06') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_06'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_06'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_07') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_07'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_07'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_08') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_08'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_08'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_09') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_09'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_09'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_10') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_10'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_10'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_11') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_11'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_11'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_12') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_12'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_12'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_13') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_13'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_13'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_14') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_14'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_14'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_15') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_15'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_15'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_16') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_16'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_16'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_17') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_17'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_17'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_18') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_18'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_18'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_19') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_19'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_19'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_20') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_20'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_20'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_21') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_21'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_21'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_22') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_22'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_22'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_23') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_23'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_23'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_24') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_24'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_24'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_25') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_25'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_25'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_26') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_26'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_26'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_27') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_27'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_27'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_28') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_28'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_28'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_29') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_29'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_29'); ?></option>
										<option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_30') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_30'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_30'); ?></option>
										<option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_31') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_31'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_31'); ?></option>
									</select>
                                </td>
                           	</tr>
                            <tr>
                                <td align="right"><strong>Announcement Body (English)<font color='#990000'>*</font>:</strong><td><td> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'announcements_description',  @$row->announcements_description , '100%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>Announcement Body (Arabic)<font color='#990000'>*</font>:</strong><td><td>                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'announcements_description_ar',  @$row->announcements_description_ar , '100%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Expiry Date<font color='#990000'>*</font>:</strong><td><td>
                                <?php
								// for plugin
								// JPluginHelper::importPlugin('datepicker');	
								//echo plgSystemDatePicker::calendar($row->announcements_expirydate, 'announcements_expirydate', 'announcements_expirydate', '%Y-%m-%d %H:%M:%S');
								 ?>
                                  <?php
								JHTML::_('behavior.calendar');
								/*// for plugin
								 JPluginHelper::importPlugin('datepicker');	
								echo plgSystemDatePicker::calendar($row->news_newsdate, 'news_newsdate', 'news_newsdate', '%Y-%m-%d');*/
								 ?>
                                 <input class="inputbox" type="text" name="announcements_expirydate" id="announcements_expirydate"  value="<?php echo strtotime($row->announcements_expirydate) ? date('d-m-Y', strtotime(htmlspecialchars($row->announcements_expirydate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Show On Top:</strong><td><td>
                                    <?php echo $showOnTop;?>
                                                                 
                            </tr>
                           <tr>
                                <td align="right"><strong>Is Public?:</strong><td><td>
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           
                            <tr>
                                <td align="right"><strong>Department / Section:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Publish:</strong><td><td>
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                             <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                </td>
                             </tr>
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.announcements" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newAnnouncements 

//listAnnouncements
function listAnnouncements($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('List Announcements'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('List Announcements'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 && ( announcements_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'")  || publishedby = "'.$user->id.'")';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(announcements_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(announcements_name_ar) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(announcementsdescription)) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(announcements_description_ar)) LIKE '%".$search."%' ) ";
    }    
 	
$database->SetQuery( "SELECT count(*) FROM #__eposts_announcements".$where);
    $total = $database->loadResult();    
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_announcements
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->announcements_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    }    

    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newannouncements");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Announcement</a>
<div class="clear"></div>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">

<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title">Announcement Title</th>
            <th valign="top" align="left" class="title">Department Name</th>
            <th valign="top" align="left" class="title">Expiry Date</th>
            <th valign="top" align="left" class="title">Publish</th>
            <th valign="top" align="left" class="title">Actions</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newannouncements&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newannouncements&task=delannouncements&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php echo $link; ?>" title="Edit Announcement"><?php echo $row->announcements_name; ?></a></td>
            <td valign="top" align="left"><?php echo $row->department; ?></td>
            <td valign="top" align="left"><?php echo JText::sprintf('%s', JHtml::_('date', $row->announcements_expirydate, JText::_('DATE_FORMAT_LC5'))); ?></td>
            <td valign="top" align="left"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listAnnouncements

//new News
function newNews($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('News'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);
	
    $row = new jlist_news( $database );
    $row->load( $cirid );
    $cdipartment = array();
 
    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'news_department',
        'class="inputbox" ', 'value', 'text', $row->news_department);
		
	
	$cdAccess[] = JHTML::_('select.option',0,'No');
	$cdAccess[] = JHTML::_('select.option',1,'Yes');
	$departmentAccess =	JHTML::_('select.genericlist',$cdAccess, 'news_access',
        'class="inputbox" ', 'value', 'text', $row->news_access);
		
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);
		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('News'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">                      
	function deletefile(filefield, filename, getUpdate) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		document.getElementById(getUpdate).value = '1';
		return false;
	}
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.news_name.value == "" 
			 || form.news_name_ar.value == ""
			 || form.news_newsdate.value == ""
			 || tinyMCE.get('news_description').getContent()== ""
			 || tinyMCE.get('news_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listnews&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New News') :  JText::_('Edit News');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newnews'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit News' : 'Add News';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong>News Title (English)<font color='#990000'>*</font>:</strong><td><td>
                                <input name="news_name" value="<?php echo htmlspecialchars($row->news_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </td></tr>
                            <tr>
                                <td align="right"><strong>News Title (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="news_name_ar" value="<?php echo htmlspecialchars($row->news_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                           </td> <!--</tr>
                            <tr>
                                <td align="right"><strong>News Source (English):<td><td>
                                    <input name="news_source" value="<?php echo htmlspecialchars($row->news_source, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </td></tr>
                            <tr>
                                <td align="right"><strong>News Source (Arabic):</strong><td><td>
                                    <input name="news_source_ar" value="<?php echo htmlspecialchars($row->news_source_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </td></tr> -->
                            </tr>
                            <tr>
                                <td align="right"><strong>News Subtitle (English):<td><td>
                                    <input name="news_subtitle" value="<?php echo htmlspecialchars($row->news_subtitle, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </td></tr>
                            <tr>
                                <td align="right"><strong>News Subtitle (Arabic):</strong><td><td>
                                    <input name="news_subtitle_ar" value="<?php echo htmlspecialchars($row->news_subtitle_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </td></tr>
                            <tr>
                                <td align="right"><strong>News Short Description (English)<font color='#990000'>*</font>:</strong><td><td> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'news_description',  @$row->news_description , '100%', '10', '80', '5', true ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>News Short Description (Arabic)<font color='#990000'>*</font>:</strong><td><td>                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'news_description_ar',  @$row->news_description_ar , '100%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>News Body (English):</strong><td><td> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'news_longdescription',  @$row->news_longdescription , '100%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>News Body (Arabic):</strong><td><td>                                <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'news_longdescription_ar',  @$row->news_longdescription_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>News Date<font color='#990000'>*</font>:</strong><td><td>
                                <?php
								JHTML::_('behavior.calendar');
								/*// for plugin
								 JPluginHelper::importPlugin('datepicker');	
								echo plgSystemDatePicker::calendar($row->news_newsdate, 'news_newsdate', 'news_newsdate', '%Y-%m-%d');*/
								 ?>
                                 <input class="inputbox" type="text" name="news_newsdate" id="news_newsdate"  value="<?php echo strtotime($row->news_newsdate) != '' ? date('d-m-Y', strtotime(htmlspecialchars($row->news_newsdate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('news_newsdate','%d-%m-%Y');"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>News Image (English):</strong><td><td>
                                <input name="news_file" id="news_file" type="file" <?php print $row->news_image != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->news_image != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="news_image_nm"><?php print $row->news_image; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('news_file', 'news_image_nm','del_news_image');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                    <input name="del_news_image" id="del_news_image" type="hidden" value="0"/>                             
                            </td></tr>
                            <tr>
                                <td align="right"><strong>News Image (Arabic):</strong><td><td>
                                <input name="news_file_ar" id="news_file_ar" type="file" <?php print $row->news_file_ar != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->news_file_ar != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="news_file_ar_nm"><?php print $row->news_file_ar; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('news_file_ar', 'news_file_ar_nm','del_news_file_ar');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                    <input name="del_news_file_ar" id="del_news_file_ar" type="hidden" value="0"/>
                                                                 
                            </td></tr>
                           <tr>
                                <td align="right"><strong>Is Public?:</strong><td><td>
                                    <?php echo $departmentAccess;?>
                                                                 
                            </td></tr>
                           
                            <tr>
                                <td align="right"><strong>Department / Section:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </td></tr>
                            <tr>
                                <td align="right"><strong>Publish:</strong><td><td>
                                    <?php echo $isPublist;?>
                                                                 
                           </td> </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </td></tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </td></tr>         
                             <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.news" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newNews 

//listNews
function listNews($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('List News'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('List News'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 && (news_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'") || publishedby = "'.$user->id.'")';
	
  

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(news_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(news_description)) LIKE '%".$search."%') ";
    }    
$database->SetQuery( "SELECT count(*) FROM #__eposts_news". $where);
    $total = $database->loadResult();      
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_news
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->news_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    }    

    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newnews");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New News</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">

<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title">News Title</th>
            <th valign="top" align="left" class="title">Department Name</th>
            <th valign="top" align="left" class="title">News Date</th>
            <th valign="top" align="left" class="title">Publish</th>
            <th valign="top" align="left" class="title">Actions</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newnews&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newnews&task=delnews&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php echo $link; ?>" title="Edit News"><?php echo $row->news_name; ?></a></td>
            <td valign="top" align="left"><?php echo $row->department; ?></td>
            <td valign="top" align="left"><?php echo JText::sprintf('%s', JHtml::_('date', $row->news_newsdate, JText::_('DATE_FORMAT_LC5'))); ?></td>
            <td valign="top" align="left"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
           <td valign="top" align="left"><a href="<?php echo $dellink; ?>" onClick="javascript:return confirm('So you wish to delete this item?')";>Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listNews


//new Visions
function newVisions($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Our Vision'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

    $row = new jlist_visions( $database );
    $row->load( $cirid );
    $cdipartment = array();
		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Our Vision'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.visions_name.value == "" 
			 || form.visions_name_ar.value == ""
			 || tinyMCE.get('visions_description').getContent()== ""
			 || tinyMCE.get('visions_description_ar').getContent() == ""
			 || tinyMCE.get('visions_longdescription').getContent()== ""
			 || tinyMCE.get('visions_longdescription_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listvisions&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Vision') :  JText::_('Edit Vision');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newvisions'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit Vision' : 'Add Vision';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong>Vision Title (English)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="visions_name" value="<?php echo htmlspecialchars($row->visions_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Vision Title (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="visions_name_ar" value="<?php echo htmlspecialchars($row->visions_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Vision Short Description (English)<font color='#990000'>*</font>:</strong><td><td> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'visions_description',  @$row->visions_description , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>Vision Short Description (Arabic)<font color='#990000'>*</font>:</strong><td><td>                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'visions_description_ar',  @$row->visions_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Vision Body (English)<font color='#990000'>*</font>:</strong><td><td> 
                                <?php 
								$editor3 =& JFactory::getEditor();
								echo $editor3->display( 'visions_longdescription',  @$row->visions_longdescription , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>Vision Body (Arabic)<font color='#990000'>*</font>:</strong><td><td>                                 <?php 
								 $editor4 =& JFactory::getEditor();
								 echo $editor4->display( 'visions_longdescription_ar',  @$row->visions_longdescription_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Mission Body (English):</strong><td><td>                                 
								<?php echo $editor->display( 'visions_mission',  @$row->visions_mission , '100%', '10', '80', '5', true, false ) ; ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Mission Body (Arabic):</strong><td><td>                                 <?php 
								 echo $editor->display( 'visions_mission_ar',  @$row->visions_mission_ar , '100%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Values Body (English):</strong><td><td>                                <?php 
								 echo $editor->display( 'visions_values',  @$row->visions_values , '100%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Values Body (Arabic):</strong><td><td>                                 <?php 
								 echo $editor->display( 'visions_values_ar',  @$row->visions_values_ar , '100%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                             <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.visions" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newVisions 

//listVisions
function listVisions($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Our Vision'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Our Vision'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 ';
	
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(visions_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(visions_description)) LIKE '%".$search."%' ) ";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_visions". $where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_visions
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();

    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newvisions");   
?>	
<!--<a class="brightNew" href="<?php //echo $newlink;?>">New Record</a>-->
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">

    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <!--<tr>
          <td  align="right" colspan="3">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr> -->
        <tr>
            <th valign="top" align="left" class="title">Vision Title</th>
            <th valign="top" align="left" class="title">Short Description</th>
            <!--<th valign="top" align="left" class="title">Actions</th>-->
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newvisions&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newvisions&task=delvisions&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php echo $link; ?>"><?php echo $row->visions_name; ?></a></td>
            <td valign="top" align="left"><?php echo $row->visions_description; ?></td>
            <!--<td valign="top" align="left"><a href="<?php //echo $dellink; ?>">Delete</a></td>-->
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listVisions

//new Alerts
function newAlerts($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Alerts'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

    ///////////////////////////////
$document =& JFactory::getDocument();
JHTML::_( 'behavior.mootools' );
$cf_url = JURI::root().'components/com_chronoforms/js/datepicker_moo/';
$tag = 'en-US';
$lang =& JFactory::getLanguage();
$lang_tag = $lang->getTag();
if ( !$lang_tag ) {
  $lang_tag = $lang->getDefault();
}
if ( file_exists($cf_url.'Locale.'.$lang_tag.'.DatePicker.js') ) {
  $tag = $lang_tag;
}
$document->addScript( $cf_url.'Locale.'.$tag.'.DatePicker.js' );
///////////////////////////////////////////

    $row = new jlist_alerts( $database );
    $row->load( $cirid );
  
		
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);
		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Alerts'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.alerts_name.value == "" 
			 || form.alerts_name_ar.value == ""
			 || form.alerts_enddate.value == ""
			 || tinyMCE.get('alerts_description').getContent()== ""
			 || tinyMCE.get('alerts_description_ar').getContent() == ""
			 || tinyMCE.get('alerts_longdescription').getContent()== ""
			 || tinyMCE.get('alerts_longdescription_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listalerts&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Alert') :  JText::_('Edit Alert');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newalerts'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit Alerts' : 'Add Alerts';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong>Alert Title (English)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="alerts_name" value="<?php echo htmlspecialchars($row->alerts_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Alert Title (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="alerts_name_ar" value="<?php echo htmlspecialchars($row->alerts_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Alert Short Description (English)<font color='#990000'>*</font>:</strong><td><td> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'alerts_description',  @$row->alerts_description , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>Alert Short Description (Arabic)<font color='#990000'>*</font>:</strong><td><td>                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'alerts_description_ar',  @$row->alerts_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Alert Body (English)<font color='#990000'>*</font>:</strong><td><td> 
                                <?php 
								$editor3 =& JFactory::getEditor();
								echo $editor3->display( 'alerts_longdescription',  @$row->alerts_longdescription , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>Alert Body (Arabic)<font color='#990000'>*</font>:</strong><td><td>                                 <?php 
								 $editor4 =& JFactory::getEditor();
								 echo $editor4->display( 'alerts_longdescription_ar',  @$row->alerts_longdescription_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Alert Date<font color='#990000'>*</font>:</strong><td><td>
                                <?php
								// for plugin
								// JPluginHelper::importPlugin('datepicker');	
								// echo plgSystemDatePicker::calendar($row->alerts_startdate, 'alerts_startdate', 'alerts_startdate', '%Y-%m-%d %H:%M:%S');
								JHTML::_('behavior.calendar');
								 ?>
                                 <input class="inputbox" type="text" name="alerts_startdate" id="alerts_startdate"  value="<?php echo strtotime($row->alerts_startdate) ? date('d-m-Y', strtotime(htmlspecialchars($row->alerts_startdate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('alerts_startdate','%d-%m-%Y');"/>                                                               
                            </tr>
                            <tr>
                                <td align="right"><strong>End Date<font color='#990000'>*</font>:</strong><td><td>
                                <?php
								// for plugin
								// JPluginHelper::importPlugin('datepicker');	
								//echo plgSystemDatePicker::calendar($row->alerts_enddate, 'alerts_enddate', 'alerts_enddate', '%Y-%m-%d %H:%M:%S');
								 ?> 
                                 <?php
								JHTML::_('behavior.calendar');
								 ?>
                                 <input class="inputbox" type="text" name="alerts_enddate" id="alerts_enddate"  value="<?php echo strtotime($row->alerts_enddate) ? date('d-m-Y', strtotime(htmlspecialchars($row->alerts_enddate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('alerts_enddate','%d-%m-%Y');"/>                                                                
                            </tr>
                            <tr>
                                <td align="right"><strong>Publish:</strong><td><td>
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;} ?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.alerts" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newAlerts 

//listAlerts
function listAlerts($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Alerts'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Alerts'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 ';
	
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(alerts_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(alerts_name_ar) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(alerts_description)) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(alerts_description_ar)) LIKE '%".$search."%' ) ";
    }    
   $database->SetQuery( "SELECT count(*) FROM #__eposts_alerts".$where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_alerts
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();

    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newalerts");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Alert</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title">Alert Title</th>
            <th valign="top" align="left" class="title">Alert Date</th>
            <th valign="top" align="left" class="title">End Date</th>
            <th valign="top" align="left" class="title">Publish</th>
            <th valign="top" align="left" class="title">Actions</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newalerts&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newalerts&task=delalerts&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php echo $link; ?>" title="Edit Alert"><?php echo $row->alerts_name; ?></a></td>
            <td valign="top" align="left"><?php echo JText::sprintf('%s', JHtml::_('date', $row->alerts_startdate, JText::_('DATE_FORMAT_LC5'))); ?></td>
            <td valign="top" align="left"><?php echo JText::sprintf('%s', JHtml::_('date', $row->alerts_enddate, JText::_('DATE_FORMAT_LC5'))); ?></td>
            <td valign="top" align="left"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listAlerts

//new Policies
function newPolicies($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Policies & Procedures'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

    ///////////////////////////////
$document =& JFactory::getDocument();
JHTML::_( 'behavior.mootools' );
$cf_url = JURI::root().'components/com_chronoforms/js/datepicker_moo/';
$tag = 'en-US';
$lang =& JFactory::getLanguage();
$lang_tag = $lang->getTag();
if ( !$lang_tag ) {
  $lang_tag = $lang->getDefault();
}
if ( file_exists($cf_url.'Locale.'.$lang_tag.'.DatePicker.js') ) {
  $tag = $lang_tag;
}
$document->addScript( $cf_url.'Locale.'.$tag.'.DatePicker.js' );
///////////////////////////////////////////

    $row = new jlist_policies( $database );
    $row->load( $cirid );
 
    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'policies_department',
        'class="inputbox" ', 'value', 'text', $row->policies_department);
		

	
	$cdAccess[] = JHTML::_('select.option',0,'No');
	$cdAccess[] = JHTML::_('select.option',1,'Yes');
	$departmentAccess =	JHTML::_('select.genericlist',$cdAccess, 'policies_access',
        'class="inputbox" ', 'value', 'text', $row->policies_access);
		
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Plicies & Procedures'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">                             
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.policies_name.value == "" 
			 || form.policies_name_ar.value == ""
			 || form.policies_policydate.value == ""
 <?php if(!$row->id){?>
             || form.policies_file.value == ""
			 || form.policies_file_ar.value == "" 
<?php }?>
			 ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listpolicies&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Policy') :  JText::_('Edit Policy');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newpolicies'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit Policy' : 'Add Policy';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong>Policy Title (English)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="policies_name" value="<?php echo htmlspecialchars($row->policies_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Policy Title (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="policies_name_ar" value="<?php echo htmlspecialchars($row->policies_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                           <tr>
                                <td align="right"><strong>Policy Date<font color='#990000'>*</font>:</strong><td><td>
                                <?php
								// for plugin
								// JPluginHelper::importPlugin('datepicker');	
								//echo plgSystemDatePicker::calendar($row->policies_policydate, 'policies_policydate', 'policies_policydate', '%Y-%m-%d');
								 ?>
                                 <?php
								JHTML::_('behavior.calendar');
								 ?>
                                 <input class="inputbox" type="text" name="policies_policydate" id="policies_policydate"  value="<?php echo strtotime($row->policies_policydate) != '' ? date('d-m-Y', strtotime(htmlspecialchars($row->policies_policydate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('policies_policydate','%d-%m-%Y');"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Policy Document (English)<font color='#990000'>*</font>:</strong><td><td>
                                <input name="policies_file" id="policies_file" type="file" <?php print $row->policies_doc != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->policies_doc != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="policies_doc_nm"><?php print $row->policies_doc; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('policies_file', 'policies_doc_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Policy Document (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                <input name="policies_file_ar" id="policies_file_ar" type="file" <?php print $row->policies_doc_ar != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->policies_doc_ar != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="policies_doc_ar_nm"><?php print $row->policies_doc_ar; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('policies_file_ar', 'policies_doc_ar_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                           <tr>
                                <td align="right"><strong>Is Public?:</strong><td><td>
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           
                            <tr>
                                <td align="right"><strong>Department / Section:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Publish:</strong><td><td>
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.policies" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newPolicies 

//listPolicies
function listPolicies($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Policies & Procedures'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Policies & Procedures'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 && (policies_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'") || publishedby = "'.$user->id.'")';
	
  

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(policies_name) LIKE '%".$search."%' ";
        $where  .= " OR LOWER(policies_name_ar) LIKE '%".$search."%' ) ";
    }    
   $database->SetQuery( "SELECT count(*) FROM #__eposts_policies".$where);
    $total = $database->loadResult();    
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_policies
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->policies_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    }    

    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newpolicies");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Policy</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title">Policy Title</th>
            <th valign="top" align="left" class="title">Department Name</th>
            <th valign="top" align="left" class="title">Policy Date</th>
            <th valign="top" align="left" class="title">Publish</th>
            <th valign="top" align="left" class="title">Actions</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newpolicies&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newpolicies&task=delpolicies&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php echo $link; ?>" title="Edit Policy"><?php echo $row->policies_name; ?></a></td>
            <td valign="top" align="left"><?php echo $row->department; ?></td>
            <td valign="top" align="left"><?php echo JText::sprintf('%s', JHtml::_('date', $row->policies_policydate, JText::_('DATE_FORMAT_LC5'))); ?></td>
            <td valign="top" align="left"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listPolicies

//new Quotes
function newQuotes($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Quotes'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

    ///////////////////////////////
$document =& JFactory::getDocument();
JHTML::_( 'behavior.mootools' );
$cf_url = JURI::root().'components/com_chronoforms/js/datepicker_moo/';
$tag = 'en-US';
$lang =& JFactory::getLanguage();
$lang_tag = $lang->getTag();
if ( !$lang_tag ) {
  $lang_tag = $lang->getDefault();
}
if ( file_exists($cf_url.'Locale.'.$lang_tag.'.DatePicker.js') ) {
  $tag = $lang_tag;
}
$document->addScript( $cf_url.'Locale.'.$tag.'.DatePicker.js' );
///////////////////////////////////////////
    $row = new jlist_quotes( $database );
    $row->load( $cirid );
	
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Quotes'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">                               
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.quotes_name.value == "" 
			 || form.quotes_name_ar.value == ""
			 || tinyMCE.get('quotes_description').getContent()== ""
			 || tinyMCE.get('quotes_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listquotes&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Quote') :  JText::_('Edit Quote');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newquotes'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit Quote' : 'Add Quote';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong>Quote Title (English)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="quotes_name" value="<?php echo htmlspecialchars($row->quotes_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Quote Title (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="quotes_name_ar" value="<?php echo htmlspecialchars($row->quotes_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Quote Image:</strong><td><td>
                                    <input name="quotes_file" id="quotes_file" type="file" <?php print $row->quotes_file != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->quotes_file != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="quotes_file_nm"><?php print $row->quotes_file; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('quotes_file', 'quotes_file_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Description (English)<font color='#990000'>*</font>:</strong><td><td> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'quotes_description',  @$row->quotes_description , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>Description (Arabic)<font color='#990000'>*</font>:</strong><td><td>                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'quotes_description_ar',  @$row->quotes_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>Publish:</strong><td><td>
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                             <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.quotes" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newQuotes 

//listQuotes
function listQuotes($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Quotes'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Quotes'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 ';
	
    

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(quotes_name) LIKE '%".$search."%'";
        $where  .= " OR LOWER(quotes_name_ar) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(quotes_description)) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(quotes_description_ar)) LIKE '%".$search."%' ) ";
    }    
     $database->SetQuery( "SELECT count(*) FROM #__eposts_quotes".$where);
    $total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_quotes
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();   

    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newquotes");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Quote</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="6">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title">Quote Title (English)</th>
            <th valign="top" align="left" class="title">Quote Title (Arabic)</th>
            <th valign="top" align="left" class="title">Description (English)</th>
            <th valign="top" align="left" class="title">Description (Arabic)</th>
            <th valign="top" align="left" class="title">Publish</th>
            <th valign="top" align="left" class="title">Actions</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newquotes&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newquotes&task=delquotes&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php echo $link; ?>" title="Edit Quote"><?php echo $row->quotes_name; ?></a></td>
           <td valign="top" align="left"><?php echo $row->quotes_name_ar; ?></td>
            <td valign="top" align="left"><?php echo $row->quotes_description; ?></td>
            <td valign="top" align="left"><?php echo $row->quotes_description_ar; ?></td>
            <td valign="top" align="left"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listQuotes

//new Manuals
function newManuals($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Manuals & Guides'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

    ///////////////////////////////
$document =& JFactory::getDocument();
JHTML::_( 'behavior.mootools' );
$cf_url = JURI::root().'components/com_chronoforms/js/datepicker_moo/';
$tag = 'en-US';
$lang =& JFactory::getLanguage();
$lang_tag = $lang->getTag();
if ( !$lang_tag ) {
  $lang_tag = $lang->getDefault();
}
if ( file_exists($cf_url.'Locale.'.$lang_tag.'.DatePicker.js') ) {
  $tag = $lang_tag;
}
$document->addScript( $cf_url.'Locale.'.$tag.'.DatePicker.js' );
///////////////////////////////////////////
    $row = new jlist_manuals( $database );
    $row->load( $cirid );
    $cdipartment = array();
 
    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'manuals_department',
        'class="inputbox" ', 'value', 'text', $row->manuals_department);
		
	
	$cdAccess[] = JHTML::_('select.option',0,'No');
	$cdAccess[] = JHTML::_('select.option',1,'Yes');
	$departmentAccess =	JHTML::_('select.genericlist',$cdAccess, 'manuals_access',
        'class="inputbox" ', 'value', 'text', $row->manuals_access);
		
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Manuals & Guides'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">                              
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.manuals_name.value == "" 
			 || form.manuals_name_ar.value == ""
			 || form.manuals_manualdate.value == ""
<?php if(!$row->id){?>
			 || form.manuals_file.value == ""
			 || form.manuals_file_ar.value == "" 
<?php }?>
			 ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listmanuals&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Manual') :  JText::_('Edit Manual');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newmanuals'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit Manual' : 'Add Manual';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong>Manual Title (English)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="manuals_name" value="<?php echo htmlspecialchars($row->manuals_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Manual Title (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="manuals_name_ar" value="<?php echo htmlspecialchars($row->manuals_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                           <tr>
                                <td align="right"><strong>Manual Date<font color='#990000'>*</font>:</strong><td><td>
                                <?php
								// for plugin
								// JPluginHelper::importPlugin('datepicker');	
								// echo plgSystemDatePicker::calendar($row->manuals_manualdate, 'manuals_manualdate', 'manuals_manualdate', '%Y-%m-%d');
								 ?>
                                 <?php
								JHTML::_('behavior.calendar');
								 ?>
                                 <input class="inputbox" type="text" name="manuals_manualdate" id="manuals_manualdate"  value="<?php echo strtotime($row->manuals_manualdate) ? date('d-m-Y', strtotime(htmlspecialchars($row->manuals_manualdate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('manuals_manualdate','%d-%m-%Y');"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Manual Document (English)<font color='#990000'>*</font>:</strong><td><td>
                                <input name="manuals_file" id="manuals_file" type="file" <?php print $row->manuals_doc != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->manuals_doc != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="manuals_doc_nm"><?php print $row->manuals_doc; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('manuals_file', 'manuals_doc_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Manual Document (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                <input name="manuals_file_ar" id="manuals_file_ar" type="file" <?php print $row->manuals_doc_ar != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->manuals_doc_ar != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="manuals_doc_ar_nm"><?php print $row->manuals_doc_ar; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return deletefile('manuals_file_ar', 'manuals_doc_ar_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                           <tr>
                                <td align="right"><strong>Is Public?:</strong><td><td>
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           
                            <tr>
                                <td align="right"><strong>Department / Section:</strong><td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Publish:</strong><td><td>
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                             <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.manuals" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newManuals 

//listManuals
function listManuals($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Manuals & Guides'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Manuals & Guides'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 && (manuals_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'") || publishedby = "'.$user->id.'")';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(manuals_name) LIKE '%".$search."%' ) ";
    }    
 	$database->SetQuery( "SELECT count(*) FROM #__eposts_manuals". $where);
    $total = $database->loadResult();    
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_manuals
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->manuals_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    }    
   

    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newmanuals");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Manual</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title">Manual Title</th>
            <th valign="top" align="left" class="title">Department Name</th>
            <th valign="top" align="left" class="title">Manual Date</th>
            <th valign="top" align="left" class="title">Publish</th>
            <th valign="top" align="left" class="title">Actions</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newmanuals&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newmanuals&task=delmanuals&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php echo $link; ?>" title="Edit Manual"><?php echo $row->manuals_name; ?></a></td>
          <td valign="top" align="left"><?php echo $row->department; ?></td>
            <td valign="top" align="left"><?php echo JText::sprintf('%s', JHtml::_('date', $row->manuals_manualdate, JText::_('DATE_FORMAT_LC5'))); ?></td>
            <td valign="top" align="left"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listManuals


//new Benchmarks
function newBenchmarks($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Benchmarks'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

    ///////////////////////////////
$document =& JFactory::getDocument();
JHTML::_( 'behavior.mootools' );
$cf_url = JURI::root().'components/com_chronoforms/js/datepicker_moo/';
$tag = 'en-US';
$lang =& JFactory::getLanguage();
$lang_tag = $lang->getTag();
if ( !$lang_tag ) {
  $lang_tag = $lang->getDefault();
}
if ( file_exists($cf_url.'Locale.'.$lang_tag.'.DatePicker.js') ) {
  $tag = $lang_tag;
}
$document->addScript( $cf_url.'Locale.'.$tag.'.DatePicker.js' );
///////////////////////////////////////////
$row = new jlist_benchmarks( $database );
    $row->load( $cirid );
    $cdipartment = array();

    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
				. "\n WHERE id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ='".$user->id."')"
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->id,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'benchmarks_department',
        'class="inputbox" ', 'value', 'text', $row->benchmarks_department);
	
	$cdPublish[] = JHTML::_('select.option',0,'No');
	$cdPublish[] = JHTML::_('select.option',1,'Yes');
	$isPublist =	JHTML::_('select.genericlist',$cdPublish, 'publish',
        'class="inputbox" ', 'value', 'text', $row->publish);	
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Benchmarks'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.benchmarks_name.value == "" 
			 || form.benchmarks_name_ar.value == ""
			 || tinyMCE.get('benchmarks_description').getContent()== ""
			 || tinyMCE.get('benchmarks_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listbenchmarks&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Benchmark') :  JText::_('Edit Benchmark');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newbenchmarks'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit Benchmarks' : 'Add Benchmarks';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong>Benchmarks Title (English)<font color='#990000'>*</font>:</strong></td><td>
                                    <input name="benchmarks_name" value="<?php echo htmlspecialchars($row->benchmarks_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Benchmarks Title (Arabic)<font color='#990000'>*</font>:</strong></td><td>
                                    <input name="benchmarks_name_ar" value="<?php echo htmlspecialchars($row->benchmarks_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr> 
                            <tr>
                                <td align="right"><strong>Benchmarks Description (English)<font color='#990000'>*</font>:</strong></td><td>
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'benchmarks_description',  @$row->benchmarks_description , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>Benchmarks Description (Arabic)<font color='#990000'>*</font>:</strong></td><td>
                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'benchmarks_description_ar',  @$row->benchmarks_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>                          
                            <tr>
                                <td align="right"><strong>Department / Section:</strong></td><td>
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></strong></td><td>
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published On:</strong></td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong></td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                             <tr>
                                <td align="center">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.benchmarks" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newBenchmarks

//listBenchmarks
function listBenchmarks($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Benchmarks'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Benchmarks'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

   $where = ' WHERE 1 = 1  && (benchmarks_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'") || publishedby = "'.$user->id.'")';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(benchmarks_name) LIKE '%".$search."%' ";
        $where  .= " OR LOWER(benchmarks_name_ar) LIKE '%".$search."%' ) ";
    }    
	$database->SetQuery( "SELECT count(*) FROM #__eposts_benchmarks".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_benchmarks
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->benchmarks_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
       }       
    }    

	$newlink = JRoute::_("index.php?option=com_eposts&view=newbenchmarks");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Benchmark</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="4">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title">Benchmark Title</th>
            <th valign="top" align="left" class="title">Department Name</th>
            <th valign="top" align="left" class="title">Benchmark Description</th>
            <th valign="top" align="left" class="title">Publish</th>
            <th valign="top" align="left" class="title">Actions</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newbenchmarks&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newbenchmarks&task=delbenchmarks&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php echo $link; ?>"><?php echo $row->benchmarks_name; ?></a></td>
            <td valign="top" align="left"><?php echo $row->department; ?></td>
            <td valign="top" align="left"><?php echo $row->benchmarks_description; ?></td>
            <td valign="top" align="left"><?php echo ($row->publish == 1)?'Yes':'No'; ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listBenchmarks

//new FAQs
function newFaqs($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('FAQs'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

    ///////////////////////////////
$document =& JFactory::getDocument();
///////////////////////////////////////////
$row = new jlist_faqs( $database );
    $row->load( $cirid );
    $cdipartment = array();

//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('FAQs'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.faqs_name.value == "" 
			 || form.faqs_name_ar.value == ""
			 || tinyMCE.get('faqs_description').getContent()== ""
			 || tinyMCE.get('faqs_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listfaqs&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New FAQ') :  JText::_('Edit FAQ');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newfaqs'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit FAQs' : 'Add FAQs';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong>FAQ Question (English)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="faqs_name" value="<?php echo htmlspecialchars($row->faqs_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>FAQ Question (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="faqs_name_ar" value="<?php echo htmlspecialchars($row->faqs_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr> 
                            <tr>
                                <td align="right"><strong>Description (English)<font color='#990000'>*</font>:</strong><td><td>
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'faqs_description',  @$row->faqs_description , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>Description (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'faqs_description_ar',  @$row->faqs_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                             <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.faqs" />
    </form>
<?php
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newFaqs

//listFaqs
function listFaqs($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('FAQs'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('FAQs'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 ';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(faqs_name) LIKE '%".$search."%'  ";
		$where  .= " ||  LOWER(faqs_name_ar) LIKE '%".$search."%'  ";
		$where  .= " ||  LOWER(faqs_description) LIKE '%".$search."%'  ";
		$where  .= " ||  LOWER(faqs_description_ar) LIKE '%".$search."%' ) ";
    }    
	$database->SetQuery( "SELECT count(*) FROM #__eposts_faqs".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_faqs
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
 

	$newlink = JRoute::_("index.php?option=com_eposts&view=newfaqs");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New FAQ</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="3">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title">FAQ Question</th>
            <th valign="top" align="left" class="title"> Description</th>
            <th valign="top" align="left" class="title">Actions</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newfaqs&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newfaqs&task=delfaqs&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php echo $link; ?>" title="Edit FAQ"><?php echo $row->faqs_name; ?></a></td>
            <td valign="top" align="left"><?php echo $row->faqs_description; ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listFaqs


function mainListFaqs($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('FAQs'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('FAQs'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   $where = ' WHERE 1 = 1 ';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(faqs_name) LIKE '%".$search."%' ";
		$where  .= " || LOWER(faqs_name_ar) LIKE '%".$search."%' ";
		$where  .= " || LOWER(faqs_description) LIKE '%".$search."%' ";
		$where  .= " || LOWER(faqs_description_ar) LIKE '%".$search."%' ) ";
    }    
 	$database->SetQuery( "SELECT count(*) FROM #__eposts_faqs".$where);
    $total = $database->loadResult();    
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_faqs
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){

	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;      
    }    
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td class="main_filter">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowfaqs&cirid=".$row->id);
			$title = $row->faqs_name;
			$descp = $row->faqs_description;
			if ($tag == 'ar-AA') :
				$title = $row->faqs_name_ar;
				$descp = $row->faqs_description_ar;
			endif;
			$title = strip_tags($title);
			$length = 400;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
<div id="default-colm-two">
			<div class="default-colm-left">
			<div class="default-list-title"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
            <div class="default-list-decs"> 
			<?php echo $descp; ?>
            </div>
            </div>
			<div class="default-colm-right">
			<div class="sub_title"><span class="default-list-decs-head"><?php echo $row->department ?></span><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>

   </div></div>
			</div>
			</div>
			<div>&nbsp;</div>      
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListFaqs

function mainShowFaqs($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('FAQs'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('FAQs'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   $where = ' WHERE 1 = 1 && id = "'.$cirid.'"';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(benchmarks_name) LIKE '%".$search."%' ";
		$where  .= " || LOWER(benchmarks_name) LIKE '%".$search."%' ";
		$where  .= " || LOWER(benchmarks_name) LIKE '%".$search."%' ";
		$where  .= " || LOWER(benchmarks_name) LIKE '%".$search."%' ) ";
    }    
	$database->SetQuery( "SELECT count(*) FROM #__eposts_faqs".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_faqs
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){ 
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;     
    }    
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$title = $row->faqs_name;
			$descp = $row->faqs_description;
			if ($tag == 'ar-AA') :
				$title = $row->faqs_name_ar;
				$descp = $row->faqs_description_ar;
			endif;
			$title = strip_tags($title);
			$length = 20;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
			$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowfaqs'));
            ?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title_news"><?php echo $title; ?></div> </br>
            
            <div class="body p2">
            <?php echo $descp; ?>
            </div>
            <div>&nbsp;</div> 
			<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo $row->publishedon; ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
//echo $footer; 
}//end funtion mainShowFaqs

//new Links
function newLinks($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Links'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);
    $row = new jlist_links( $database );
    $row->load( $cirid );		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Links'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.links_name.value == "" 
			 || form.links_name_ar.value == ""
			 || form.links_link.value == "" 
			 || form.links_link_ar.value == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listlinks&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Link') :  JText::_('Edit Link');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newlinks'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit Link' : 'Add Link';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td align="right"><strong>Title (English)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="links_name" value="<?php echo htmlspecialchars($row->links_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Title (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="links_name_ar" value="<?php echo htmlspecialchars($row->links_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Link (English)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="links_link" value="<?php echo htmlspecialchars($row->links_link, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Link (Arabic)<font color='#990000'>*</font>:</strong><td><td>
                                    <input name="links_link_ar" value="<?php echo htmlspecialchars($row->links_link_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr> 
                            <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                           <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.links" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newLinks 

//listLinks
function listLinks($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Links'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Links'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1 ';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(links_name) LIKE '%".$search."%' ) ";
    }    
	$database->SetQuery( "SELECT count(*) FROM #__eposts_links".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_links
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();    

	$newlink = JRoute::_("index.php?option=com_eposts&view=newlinks");   
?>	
<a class="brightNew" href="<?php echo $newlink;?>">New Link</a>
<div class="clear"></div>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" align="left" class="title">Title (English)</th>
            <th valign="top" align="left" class="title">Title (Arabic)</th>
            <th valign="top" align="left" class="title">Link (English)</th>
            <th valign="top" align="left" class="title">Link (Arabic)</th>
            <th valign="top" align="left" class="title">Actions</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newlinks&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newlinks&task=dellinks&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
          <td valign="top" align="left"><a href="<?php echo $link; ?>" title="Edit Link"><?php echo $row->links_name; ?></a></td>
            <td valign="top" align="left"><?php echo $row->links_name_ar; ?></td>
            <td valign="top" align="left"><?php echo $row->links_link; ?></td>
            <td valign="top" align="left"><?php echo $row->links_link_ar; ?></td>
            <td valign="top" align="left"><a href="<?php echo $dellink; ?>">Delete</a></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listLinks


function mainListAnnouncements($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('List of Announcements'));
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('List of Announcements'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   
   $deptquery = "";
   $page = JRequest::getCmd('page');
   if($user->id && $deptid && !$page){
$deptquery .= " (announcements_access = 1 || announcements_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && announcements_department = '".$deptid."' ";
   }elseif($deptid && !$page){
	    $deptquery .= " announcements_access = 1 && announcements_department = '".$deptid."' ";
   }else{
	    $deptquery .= " announcements_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' ';

  $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}
	if ($search=="البحث بالإسم"){
	$search ="";
	}
	if ($announcements_expirydate=="Search by Calendar"){
	$announcements_expirydate ="";
	}
	if ($announcements_expirydate=="البحث عن طريق التقويم"){
	$announcements_expirydate ="";
	}
	
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
 $announcements_expirydate1=date("Y-m-d", strtotime($announcements_expirydate));    
   if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(announcements_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(announcements_name_ar) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(announcements_description)) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(announcements_description_ar)) LIKE '%".$search."%' ) ";
    } elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND announcements_department = '".$deparrtment."'"; 
        $where .= " AND  publishedon LIKE '".$announcements_expirydate."%'";
	
	
} elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(announcements_department) = '".$deparrtment."'";
	
	}  	
   // echo ( "SELECT count(*) FROM #__eposts_announcements".$where); 
	$database->SetQuery( "SELECT count(*) FROM #__eposts_announcements".$where);
    $total = $database->loadResult();       
  
   
      $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_announcements
              $where
              ORDER BY id DESC";
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->announcements_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
       }  
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;     
    } 
  JHTML::_('behavior.calendar');  
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<div class="announcements_mainfilter"><table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area_calander" type="text"  name="announcements_expirydate" id="announcements_expirydate" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>') {this.value = '';}" ></td>
			<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
				<td>
			<select name="deparrtment" class="inputbox" >
				<option value="" ><?php echo JText::_('COM_EPOSTS_SELECT_DEPARTMENT'); ?></option>

				<?php

 

$database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "               
            );
   
       $listDepartments2 = $database->loadObjectList();

				foreach($listDepartments2 as $xdepartment2) { ?>
		
	<?php if ($tag == 'ar-AA') {
		$department_name = $xdepartment2->departments_name_ar;
		}else{
	$department_name = $xdepartment2->departments_name;
	}?>
	<option value="<?php echo $xdepartment2->id; ?>"<?php if (!(strcmp($xdepartment2->id,$deparrtment))) {echo "selected=\"selected\"";} ?>><?php echo $department_name; ?></option>
			<?php	} ?>
			</select></td>
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>';	

	echo $form;  ?>
     <div class="announcements_bodytable"><table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
     <!-- <tr>
          <td class="main_filter">
            <?php //echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php //echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php// echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  //echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>-->
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$homeLink = $page ? '&page=home' : '';
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowannouncements".$homeLink."&cirid=".$row->id);
				$title = $row->announcements_name;
				$descp = $row->announcements_description;
				if ($tag == 'ar-AA') :
					$title = $row->announcements_name_ar;
					$descp = $row->announcements_description_ar;
				endif;
				$title = strip_tags($title);
				$length = 100;
				$descp = strip_tags($descp);
				if (mb_strlen($descp) > $length) :
					$descp = mb_substr($descp, 0, $length) . '...';
				endif;
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div id="default-colm-two">
			<div class="default-colm-left">
			<div class="default-list-title"><a href="<?php echo $link; ?>"><?php echo $title; ?> <!--(<?php echo ($row->announcements_access)? JText::_('COM_EPOSTS_PUBLIC'): JText::_('COM_EPOSTS_PRIVATE'); ?> <?php print JText::_('COM_EPOSTS_FOR'); ?> <?php echo $row->department; ?>)--></a></div>
            <div class="default-list-decs"> 
			<?php echo $descp; ?>
            </div>
            </div>
			<div class="default-colm-right">
			<div class="sub_title"><span class="default-list-decs-head"><?php echo $row->department ?></span><br/><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
            
			<div>&nbsp;</div>            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND_ANNOUNCEMENTS'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table></div>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListAnnouncements

function mainShowAnnouncements($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Announcements'));

	$jdeptid = JRequest::getCmd('deptid');
	$deptid = !($jdeptid) ? $deptid : $jdeptid;
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Announcements'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   
   $deptquery = "";
   if($user->id && $deptid){
$deptquery .= " (announcements_access = 1 || announcements_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && announcements_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " announcements_access = 1 && announcements_department = '".$deptid."' ";
   }else{
	    $deptquery .= " announcements_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' && id = "'.$cirid.'"';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND( LOWER(announcements_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(announcements_description)) LIKE '%".$search."%')";
    }    
$database->SetQuery( "SELECT count(*) FROM #__eposts_announcements".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_announcements
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->announcements_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
       } 
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;      
    }     
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->announcements_department."&Itemid=103");
			
			$title = $row->announcements_name;
			$descp = $row->announcements_description;
			if ($tag == 'ar-AA') :
				$title = $row->announcements_name_ar;
				$descp = $row->announcements_description_ar;
			endif;
			$title = strip_tags($title);
			$length = 100;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
       		$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowannouncements'));
            ?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title_announcements"><?php echo $title; ?> <!--(<?php echo ($row->announcements_access)? JText::_('COM_EPOSTS_PUBLIC'): JText::_('COM_EPOSTS_PRIVATE'); ?> <?php print JText::_('COM_EPOSTS_FOR'); ?> <a href="<?php echo $deptlink; ?>"><?php echo $row->department; ?></a>)--></div>
            
            <div class="body p2">
			<?php echo $descp; ?>
            </div>
	<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?>, <?php echo JText::_('COM_EPOSTS_EXPDATE'); ?>: <?php echo JText::sprintf('%s', JHtml::_('date', $row->announcements_expirydate, JText::_('DATE_FORMAT_LC5'))); ?></div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowAnnouncements

function mainListNews($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('List News'));

	$jdeptid = JRequest::getCmd('deptid');
	$deptid = !($jdeptid) ? $deptid : $jdeptid;
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('List News'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   
   $deptquery = "";
	$page = JRequest::getCmd('page');
 
   if($user->id && $deptid && !$page){
$deptquery .= " (news_access = 1 || news_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && news_department = '".$deptid."' ";
   }elseif($deptid && !$page){
	    $deptquery .= " news_access = 1 && news_department = '".$deptid."' ";
   }else{
	    $deptquery .= " news_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' ';
  

    $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}
	if ($search=="البحث بالإسم"){
	$search ="";
	}
	if ($announcements_expirydate=="Search by Calendar"){
	$announcements_expirydate ="";
	}
	if ($announcements_expirydate=="البحث عن طريق التقويم"){
	$announcements_expirydate ="";
	}
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
 $announcements_expirydate1=date("Y-m-d", strtotime($announcements_expirydate));    
   if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(news_name) LIKE '%".$search."%' ";
		$where .= " OR LOWER(news_name_ar) LIKE '%".$search."%' ";
		$where .= " OR LOWER(fnStripTags(news_description_ar)) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(news_description)) LIKE '%".$search."%' ) ";
    }    elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND news_department = '".$deparrtment."'"; 
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	
} elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(news_department) = '".$deparrtment."'";
	
	}  	
   // echo ( "SELECT count(*) FROM #__eposts_news".$where);
 
 $database->SetQuery( "SELECT count(*) FROM #__eposts_news". $where);
    $total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 5 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_news
              $where
              ORDER BY id DESC";
	
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->news_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
       } 
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;      
    }    
JHTML::_('behavior.calendar');
    
?>
<form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<div class="news_mainfilter">
	<table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area_calander" type="text"  name="announcements_expirydate" id="announcements_expirydate" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>') {this.value = '';}" ></td>
			<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
				<td>
			<select name="deparrtment" class="inputbox" >
				<option value="" ><?php echo JText::_('COM_EPOSTS_SELECT_DEPARTMENT'); ?></option>

				<?php

 


$database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "               
            );
   
       $listDepartments2 = $database->loadObjectList();
				foreach($listDepartments2 as $xdepartment2) { ?>
		
	<?php if ($tag == 'ar-AA') {
		$department_name = $xdepartment2->departments_name_ar;
		}else{
	$department_name = $xdepartment2->departments_name;
	}?>
	<option value="<?php echo $xdepartment2->id; ?>"<?php if (!(strcmp($xdepartment2->id,$deparrtment))) {echo "selected=\"selected\"";} ?>><?php echo $department_name; ?></option>
			<?php	} ?>
			</select></td>
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>';	

	echo $form;  ?>
     <div class="news_bodytable"><table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
     <!--  <tr>
          <td class="main_filter">
            <?php // echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php// echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php //echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                 // echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>-->
        <?php
        $k = 0;
		if (count($rows)) :
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshownews&cirid=".$row->id);
			
			$title = $row->news_name;
			$descp = $row->news_description;
			if ($tag == 'ar-AA') :
				$title = $row->news_name_ar;
				$descp = $row->news_description_ar;
			endif;
			$title = strip_tags($title);
			
			$length = 300;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
			
			if ($row->news_source != '') :
				$target = '_blank';
				$link = strstr($row->news_source, ':') ? $row->news_source : 'http://' . $row->news_source;
			endif;
			if ($row->news_source_ar != '' && $tag == 'ar-AA') :
				$target = '_blank';
				$link = strstr($row->news_source_ar, ':') ? $row->news_source_ar : 'http://' . $row->news_source_ar;
			endif;
           
$config =&JFactory::getConfig();
 //$filepath = $config->getValue( 'config.file_path' ); 
 $reverse_file_path = $config->getValue( 'config.reverse_file_path' );
  $reverse_filepath_storage = $config->getValue( 'config.reverse_filepath_storage' );
// $row->news_image;
$pieces = explode("\\",$row->news_image);
//print_r ($pieces);
if($pieces[1]){
	//PortalStorage\igallery_img\far-news_launched_crm4.jpg
// $img = $filepath.'\\'.'igallery_img\\'.$pieces[1]; 
$img = $reverse_filepath_storage.'igallery_img/'.$pieces[1];
			//echo $img;
			//echo $filepath.'\news-thums'; 
			$img = str_replace("\\", "/", $img);	
			
  //$img2 = $reverse_file_path.'\\'.$row->news_image;
			  
			 // $img2 = str_replace("\\", "/", $img2);
			 $img2 = $img;
	//		 echo $img;
			/*$filetype=string;       
	     $imgbinary = fread(fopen($img, "r"), filesize($img));
			$filetype=string;       
	     $imgbinary2 = fread(fopen($img2, "r"), filesize($img2));
     // echo $imgbinary;
	   $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
       $aaa2= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary2);*/
	   $aaa= $img;
       $aaa2= $img2;
	}
else {
$aaa= 'components/com_eposts/assets/images/icons/meeting-room.png';
}
JHTML::_("behavior.mootools");
	$document->addStyleSheet(JURI::base() . 'components/com_igallery/cerabox.css');
	$document->addScript(JURI::base() . 'components/com_igallery/cerabox.min.js');
	$document->addScriptDeclaration("
		document.addEvent('domready', function() {
			$$('.thumbs_div a').cerabox({
group: false,
			displayTitle: false
			});
		});
	");
$chk='data:image/string;base64,';

if($aaa==$chk){

$aaa=$aaa2;
}
		   ?>
        
			
			
			
			
			
			
			
		</div>
			
<tr >
            <td valign="top">
			
<div id="resourcesoffices">	
		<div class="resoffice"><div class="resofficeareapic">
		<div class="thumbs_div">
					
                		<a href="#$img_my<?php echo $i; ?>" >
					<?php  $document->addScriptDeclaration(' var img_my'.$i.' = "<img src=\"'.$aaa2.'\" />";'); ?>
                        <img  src="<?php echo $aaa; ?>" width="140" height="100px" /> 
                    </a></div></div>
		<div class="resofficearea">
		
		
		<div class="publish-news">
			<div class="sub_title"><span class="resofficedescspan"><?php print $row->department; ?></span></br><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"> <?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
			
</div>


		<div class="title_news"><a href="<?php echo $link; ?>" target="<?php print $target; ?>"><?php echo $title; ?></a></div>
            <div class="resofficedesc">

<p><?php echo $descp; ?></p>
</div></div>
			
			
			</div>
			
		</div>	


           
    </td>
          
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND_NEWS'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListNews

function mainShowNews($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('News'));

	$jdeptid = JRequest::getCmd('deptid');
	$deptid = !($jdeptid) ? $deptid : $jdeptid;
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('News'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);

   $deptquery = "";
 
   if($user->id && $deptid){
$deptquery .= " (news_access = 1 || news_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && news_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " news_access = 1 && news_department = '".$deptid."' ";
   }else{
	    $deptquery .= " news_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' && id = "'.$cirid.'"';
  

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(news_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(news_description)) LIKE '%".$search."%') ";
    }    
 $database->SetQuery( "SELECT count(*) FROM #__eposts_news". $where);
    $total = $database->loadResult();  
	    
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_news
              $where
              ORDER BY id DESC";
	                                  
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->news_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
       } 
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;      
    }  
	JHTML::_("behavior.mootools");
	$document->addStyleSheet(JURI::base() . 'components/com_igallery/cerabox.css');
	$document->addScript(JURI::base() . 'components/com_igallery/cerabox.min.js');
	$document->addScriptDeclaration("
		document.addEvent('domready', function() {
			$$('.thumbs_div a').cerabox({
group: false,
			displayTitle: false
			});
		});
	");		
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->news_department."&Itemid=103");
			
            $title = $row->news_name;
			$stitle = $row->news_subtitle;
            $descp = $row->news_longdescription;
			if ($tag == 'ar-AA') :
				$title = $row->news_name_ar;
				$stitle = $row->news_subtitle_ar;
				$descp = $row->news_longdescription_ar;
			endif;
			$title = strip_tags($title);
			$length = 100;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
			$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshownews'));
            ?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title_news"><?php echo $title; ?></div>
            <div class="sub_title"><?php echo $stitle; ?></div>
            <?php 
			    //file://hqdintdev2/medifa/albumSSSSS1.png
				$config =& JFactory::getConfig();
                $filepath = $config->getValue( 'config.file_path' );
				 $reverse_file_path = $config->getValue( 'config.reverse_file_path' );
                 $reverse_filepath_storage = $config->getValue( 'config.reverse_filepath_storage' );
 /*function base64_encode_image ($filename=string,$filetype=string) {
    if ($filename) {
        $imgbinary = fread(fopen($filename, "r"), filesize($filename));
        return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
    }
}*/
if ($row->news_image != '') {
$pieces = explode("\\",$row->news_image);
//print_r ($pieces);
if($pieces[1]){
	//PortalStorage\igallery_img\far-news_launched_crm4.jpg
// $img = $filepath.'\\'.'igallery_img\\'.$pieces[1]; 
$img = $reverse_filepath_storage.'igallery_img/'.$pieces[1];
}
					//$img = JRoute::_('index.php?option=com_eposts&view=loadimage&myfile='.$filepath.'\\'.$row->news_image);
					//$img = $reverse_filepath_storage.$row->news_image;
					//$img = str_replace("\\", "/", $img); \
					
					?>
					
				<div class="thumbs_div">
					
                		<a href="#$img_my" >
						<?php  //$document->addScriptDeclaration(' var img_my = "<img src=\"'.base64_encode_image($img).'\" />";'); ?>
                        <!--<img  src="<?php //echo base64_encode_image($img); ?>" style="margin:10px;" width="300" border="0" alt="" />-->
					<?php  $document->addScriptDeclaration(' var img_my = "<img src=\"'.$img.'\" />";'); ?>
                        <img  src="<?php echo $img; ?>" style="margin:10px;" width="300" border="0" alt="" /> 
                    </a></div>
			<?php 	}
				else if ($row->news_file_ar != '') {
				$pieces = explode("\\",$row->news_file_ar);
//print_r ($pieces);
if($pieces[1]){
	//PortalStorage\igallery_img\far-news_launched_crm4.jpg
// $img = $filepath.'\\'.'igallery_img\\'.$pieces[1]; 
$img = $reverse_filepath_storage.'igallery_img/'.$pieces[1];
}

					//$img = JURI::root().'index.php?option=com_eposts&view=loadimage&myfile='.$filepath.'\\'.$row->news_file_ar;
					//$img = $filepath.'\\'.$row->news_file_ar;
					//$img = $reverse_filepath_storage.$row->news_file_ar;
					//$img = str_replace("\\", "/", $img);
					//echo '<img src="'.base64_encode_image($img).'" style="margin:10px;" width="300" border="0" alt="" />';
					echo '<img src="'.$img.'" style="margin:10px;" width="300" border="0" alt="" />';
				}
				
			?>
            </div>
            <div class="body p2">
			<?php echo $descp; ?>
            </div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?>, <?php echo JText::_('COM_EPOSTS_MAIN_NEWS_DATE'); ?>: <?php echo date('d-m-Y', strtotime($row->news_newsdate)); ?></div>
            <div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
//echo $footer; 
}//end funtion mainShowNews

function mainListVisions($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('List of Visions'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('List of Visions'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
  
    $where = ' WHERE 1 = 1 ';
  

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(visions_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(visions_name_ar) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(visions_description)) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(visions_description_ar)) LIKE '%".$search."%' ) ";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_visions". $where);
    $total = $database->loadResult();  
	    
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_visions
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
			foreach ($rows as $row){
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td class="main_filter">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowvisions&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title"><a href="<?php echo $link; ?>"><?php echo $row->visions_name; ?></a></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <div class="body">
			<?php echo $row->visions_description; ?>
            <?php echo $row->visions_description_ar; ?>
            </div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
//echo $footer; 
}//end funtion mainListVisions

function mainShowVisions($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Visions'));
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Visions'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
//$where = ' WHERE publish = 1 && '.$deptquery.' && id = "'.$cirid.'"';
   $cirid = (int)JRequest::getString('cirid', 0);
   
$where = ' WHERE 1 = 1 ';
  

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(visions_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(visions_description)) LIKE '%".$search."%' ) ";
    }    
     $database->SetQuery( "SELECT count(*) FROM #__eposts_visions". $where);
    $total = $database->loadResult();  
	   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_visions
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
		foreach ($rows as $row){
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			
			$title = $row->visions_name;
			$descp = $row->visions_longdescription;
			$mison = $row->visions_mission;
			$value = $row->visions_values;
			if ($tag == 'ar-AA') :
				$title = $row->visions_name_ar;
				$descp = $row->visions_longdescription_ar;
				$mison = $row->visions_mission_ar;
				$value = $row->visions_values_ar;
			endif;
			$title = strip_tags($title);
            ?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title"><?php echo $title; ?></div>
            
            <div class="body">
            <p><strong><?php print JText::_('COM_EPOSTS_VISION'); ?>:</strong></p>
			<?php echo $descp; ?>
            </div>
            <div class="body">
            <p><strong><?php print JText::_('COM_EPOSTS_MISSION'); ?>:</strong></p>
			<?php echo $mison; ?>
            </div>
            <div class="body">
            <p><strong><?php print JText::_('COM_EPOSTS_VALUES'); ?>:</strong></p>
			<?php echo $value; ?>
            </div>
			<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <?php if ($row->vision_file) : ?>
            <div class="sub_title"><!--<a href="<?php echo JURI::root(true) . '/eposts/' . $row->vision_file; ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a> -->
            	<a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=/eposts/' . $row->vision_file); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a>
            </div>
            <?php endif; ?>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowVisions

function mainListAlerts($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('List Alerts'));
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('List Alerts'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   
    $where = ' WHERE publish = 1 ';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(alerts_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(alerts_name_ar) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(alerts_description)) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(alerts_description_ar)) LIKE '%".$search."%' ) ";
    }    
	
 $database->SetQuery( "SELECT count(*) FROM #__eposts_alerts".$where);
    $total = $database->loadResult(); 
	   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_alerts
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
		foreach ($rows as $row){
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    } 
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td class="main_filter">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowalerts&page=home&cirid=".$row->id);
			
			$title = $row->alerts_name;
			$descp = $row->alerts_description;
			if ($tag == 'ar-AA') :
				$title = $row->alerts_name_ar;
				$descp = $row->alerts_description_ar;
			endif;
			$title = strip_tags($title);
			$length = 400;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <div class="sub_title"><?php echo JText::_('Start Date:'); ?> <?php echo JText::sprintf('%s', JHtml::_('date', $row->alerts_startdate, JText::_('DATE_FORMAT_LC5'))); ?>, <?php echo JText::_('End Date:'); ?> <?php echo JText::sprintf('%s', JHtml::_('date', $row->alerts_enddate, JText::_('DATE_FORMAT_LC5'))); ?></div>
            <div class="body">
			<?php echo $descp; ?>
            </div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListAlerts

function mainShowAlerts($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Alets'));
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Aletts'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   
    $where = ' WHERE publish = 1 && id = "'.$cirid.'" ';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(alerts_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(alerts_description)) LIKE '%".$search."%' ) ";
    }    
  $database->SetQuery( "SELECT count(*) FROM #__eposts_alerts".$where);
    $total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_alerts
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
	foreach ($rows as $row){
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }  
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			
			$title = $row->alerts_name;
			$descp = $row->alerts_longdescription;
			if ($tag == 'ar-AA') :
				$title = $row->alerts_name_ar;
				$descp = $row->alerts_longdescription_ar;
			endif;
			$title = strip_tags($title);
			$length = 100;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
       		$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowalerts'));
            ?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title_news"><?php echo $title; ?></div>
            <div>&nbsp;</div>
            <div class="sub_title">
			<?php echo $descp; ?>
            </div>
            <div>&nbsp;</div>
			<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <!-- <div class="sub_title"><?php echo JText::_('Start Date:'); ?> <?php echo JText::sprintf('%s', JHtml::_('date', $row->alerts_startdate, JText::_('DATE_FORMAT_LC5'))); ?>, <?php echo JText::_('End Date:'); ?> <?php echo JText::sprintf('%s', JHtml::_('date', $row->alerts_enddate, JText::_('DATE_FORMAT_LC5'))); ?></div>  -->
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowAlerts

function mainListPolicies($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Policies & Procedures'));

	$jdeptid = JRequest::getCmd('deptid');
	$deptid = !($jdeptid) ? $deptid : $jdeptid;
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Policies & Procedures'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   //$deptid = (int)JRequest::getString('deptid', 0);
   
   $deptquery = "";
 	$page = JRequest::getCmd('page');
   if($user->id && $deptid && !$page){
$deptquery .= " (policies_access = 1 || policies_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && policies_department = '".$deptid."' ";
   }elseif($deptid && !$page){
	    $deptquery .= " policies_access = 1 && policies_department = '".$deptid."' ";
   }else{
	    $deptquery .= " policies_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' ';
 

    $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}
	if ($search=="البحث بالإسم"){
	$search ="";
	}
	if ($announcements_expirydate=="Search by Calendar"){
	$announcements_expirydate ="";
	}
	if ($announcements_expirydate=="البحث عن طريق التقويم"){
	$announcements_expirydate ="";
	}
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
     $announcements_expirydate1=date("Y-m-d", strtotime($announcements_expirydate)); 
	if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(policies_name) LIKE '%".$search."%' ";
        $where  .= " OR LOWER(policies_name_ar) LIKE '%".$search."%' ) ";
    } elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND 	policies_department	 = '".$deparrtment."'"; 
        $where .= " AND  publishedon LIKE '".$announcements_expirydate."%'";
	
	
} elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(	policies_department	) = '".$deparrtment."'";
	
	}  	
    // echo ( "SELECT count(*) FROM #__eposts_policies".$where);   
  $database->SetQuery( "SELECT count(*) FROM #__eposts_policies".$where);
    $total = $database->loadResult();      
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_policies
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name,departments_name_ar"
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->policies_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
       } 
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;      
    } JHTML::_('behavior.calendar');       
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<div class="policies_mainfilter"><table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area_calander" type="text"  name="announcements_expirydate" id="announcements_expirydate" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>') {this.value = '';}" ></td>
			<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
				<td>
			<select name="deparrtment" class="inputbox" >
				<option value="" ><?php echo JText::_('COM_EPOSTS_SELECT_DEPARTMENT'); ?> </option>

				<?php

 

$database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "               
            );
   
       $listDepartments2 = $database->loadObjectList();

				foreach($listDepartments2 as $xdepartment2) { ?>
		
	<?php if ($tag == 'ar-AA') {
		$department_name = $xdepartment2->departments_name_ar;
		}else{
	$department_name = $xdepartment2->departments_name;
	}?>
	<option value="<?php echo $xdepartment2->id; ?>"<?php if (!(strcmp($xdepartment2->id,$deparrtment))) {echo "selected=\"selected\"";} ?>><?php echo $department_name; ?></option>
			<?php	} ?>
			</select></td>
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>';	

	echo $form;  ?>
     <div class="policies_bodytable"><table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <!--<tr>
          <td class="main_filter">
            <?php //echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php //echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php //echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  //echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>-->
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$homeLink = $page ? '&page=home' : '';
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowpolicies".$homeLink."&Itemid=395&cirid=".$row->id);
			$title = $row->policies_name;
			$document=$row->policies_document;
$document=$row->policies_doc;
			
			$title = strip_tags($title);
			$length = 400;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
			
$extension = end(explode('.', $document));   
			if ($tag == 'ar-AA') :
				$title = $row->policies_name_ar;
				$document=$row->policies_doc_ar;
			endif;
			$title = strip_tags($title);
			
$extension = end(explode('.', $document));   
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div id="default-colm-two">
			<div class="default-colm-left">
			<div class="default-list-title"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
            <div class="default-list-decs"> 
			<?php echo $descp; ?>
            </div>
            </div>
			<div class="default-colm-right">
			<div class="sub_title"><span class="default-list-decs-head"><?php echo $row->department ?></span><br/><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
            <div class="sub_title"><div class="sub_title"><div class="form_download_icon"><a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $document); ?>">
   <?php if (($extension=="docx") or ($extension=="dot")or ($extension=="doc")or ($extension=="docm")or ($extension=="dotm")or ($extension=="dotx"))   { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-word.png" border="0" alt="" /></a></div></div>
   <?php }elseif ($extension=="pdf") { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-pdf.png" border="0" alt="" /></a></div></div>
  <?php }elseif (($extension=="xls") or ($extension=="xlt")or ($extension=="xlm")or ($extension=="xlsx")or ($extension=="xltx")or ($extension=="xltm"))   { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-excel.png" border="0" alt="" /></a></div></div>
   <?php }else { ?>
   
   <img src="components/com_eposts/assets/images/icons/forms-download.png" border="0" alt="" /><div class="forms-download-text"><?php print JText::_('COM_EPOSTS_FORMS_DOWNLOAD_TEXT'); ?></a></div></div></div>
   <?php } ?></a>
   </div></div>
			</div>
			</div>
			<div>&nbsp;</div>            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND_POLICIES'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table></div>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
//echo $footer; 
}//end funtion mainListPolicies

function mainShowPolicies($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Policies & Procedures'));
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Policies & Procedures'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   $deptid = (int)JRequest::getString('deptid', 0);
   
   $deptquery = "";
 
   if($user->id && $deptid){
$deptquery .= " (policies_access = 1 || policies_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && policies_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " policies_access = 1 && policies_department = '".$deptid."' ";
   }else{
	    $deptquery .= " policies_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' && id = "'.$cirid.'"';
  

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(policies_name) LIKE '%".$search."%' ) ";
    }    
  $database->SetQuery( "SELECT count(*) FROM #__eposts_policies".$where);
    $total = $database->loadResult();     
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_policies
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->policies_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
					$row->department = $department[departments_name_ar];
			endif;
       } 
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;      
    
}    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->policies_department."&Itemid=103");
			$title = $row->policies_name;
			if ($tag == 'ar-AA') :
				$title = $row->policies_name_ar;
			endif;
			$title = strip_tags($title);
			$length = 20;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
       		$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowpolicies'));
            ?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title_policies"><?php echo $title; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_DOWNLOADS'); ?>: 
            <?php if ($tag == 'ar-AA') : ?>
            <a  class="title_policies" href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->policies_doc_ar); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a>
            <?php else : ?>
            <a  class="title_policies" href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->policies_doc); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a>
            <?php endif; ?>
			</div>
			<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?>, <?php echo JText::_('Policy Date'); ?>: <?php echo JText::sprintf('%s', JHtml::_('date', $row->policies_policydate, JText::_('DATE_FORMAT_LC5'))); ?></div>
            <div class="body">
			<?php echo '&nbsp;'; ?>
            </div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
//echo $footer; 
}//end funtion mainShowPolicies

function mainListQuotes($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
$config =&JFactory::getConfig();
 $filepath = $config->getValue( 'config.file_path' ); 
    $document->setTitle($page_title.' - '.JText::_('List of Quotes'));
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('List of Quotes'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   
    $where = ' WHERE publish = 1 ';
   

   $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}
	if ($search=="البحث بالإسم"){
	$search ="";
	}
	if ($announcements_expirydate=="Search by Calendar"){
	$announcements_expirydate ="";
	}
	if ($announcements_expirydate=="البحث عن طريق التقويم"){
	$announcements_expirydate ="";
	}
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
 $announcements_expirydate1=date("Y-m-d", strtotime($announcements_expirydate));    
   if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(quotes_name) LIKE '%".$search."%'";
        $where  .= " OR LOWER(quotes_name_ar) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(quotes_description)) LIKE '%".$search."%' ";
        $where .= " OR LOWER(fnStripTags(quotes_description_ar)) LIKE '%".$search."%' ) ";
    }  elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND media_department = '".$deparrtment."'"; 
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	
} elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate1."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(media_department) = '".$deparrtment."'";
	
	}  	
  //  echo ( "SELECT count(*) FROM #__eposts_quotes".$where);   
   $database->SetQuery( "SELECT count(*) FROM #__eposts_quotes".$where);
    $total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_quotes
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
	foreach ($rows as $row){	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
    
 JHTML::_('behavior.calendar');   
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<div class="quotes_mainfilter"><table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>"  class="text_area" alt="Search Criteria" autocomplete="off" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_NAME'); ?>') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area_calander" type="text"  name="announcements_expirydate" id="announcements_expirydate" value="<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');" onBlur="if (this.value == '') {this.value = '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>r';}" onFocus="if (this.value == '<?php echo JText::_('COM_EPOSTS_SEARCH_BY_CALENDAR'); ?>') {this.value = '';}" ></td>
			<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
				
			<div class="select-office-dropdown">
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table></div>';	

	echo $form;  ?>
 
     <div class="quotes_bodytable"><table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
     
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowquotes&cirid=".$row->id);
			
			$title = $row->quotes_name;
			$descp = $row->quotes_description;
			if ($tag == 'ar-AA') :
				$title = $row->quotes_name_ar;
				$descp = $row->quotes_description_ar;
			endif;
			$title = strip_tags($title);
			$descp = strip_tags($descp);
			
			$length = '100';
			
			if(mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
			
            ?>
	   <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div id="default-colm-two">
			<div class="default-colm-left">
			<div class="default-list-title"><?php echo $title; ?></div>
            <div class="default-list-decs"> 
			<?php echo $descp; ?>
            </div>
			 <?php if ($row->quotes_file != '') : ?>
            <div class="inner_pages_link_download_view">
                	<?php
						JHTML::_("behavior.mootools");
	$document->addStyleSheet(JURI::base() . 'components/com_igallery/cerabox.css');
	$document->addScript(JURI::base() . 'components/com_igallery/cerabox.min.js');
	$document->addScriptDeclaration("
		document.addEvent('domready', function() {
			$$('.thumbs_div a').cerabox({
group: false,
			displayTitle: false
			});
		});
	");                
            
				//print_r($row);		
				if ($row->quotes_file != '') {
					//$img = JRoute::_('index.php?option=com_eposts&view=loadimage&myfile='.$filepath.'\\'.$row->quotes_file);
					//$img = 'file:'.$filepath.'\\'.$row->quotes_file;
					$img = $filepath.'\\'.$row->quotes_file;
					
					$img = str_replace("\\", "/", $img);
					
					$img = $filepath.'\\'.$row->quotes_file;
			$img = str_replace("\\", "/", $img);
			
			$filetype=string;       
	   $imgbinary = fread(fopen($img, "r"), filesize($img));
      // echo $imgbinary;
	   $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
					
				}
                    ?>
                	
<div class="thumbs_div"><a href="#$img_my<?php echo $i; ?>" >
					<?php  $document->addScriptDeclaration(' var img_my'.$i.' = "<img src=\"'.$aaa.'\" />";'); ?>
                        <?php print JText::_('EMIRATES_VIEWIMAGE'); ?>

                    </a></div>
					
            </div>
            <?php endif;?>
            </div>
			<div class="default-colm-right">
			<div class="sub_title"><span class="default-list-decs-head"><?php echo $row->department ?></span><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>

			</div>
			</div>
			<div>&nbsp;</div>            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND_QUOTES'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table></div>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListQuotes

function mainShowQuotes($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Quotes'));
	
	JHTML::_("behavior.mootools");
	$document->addStyleSheet(JURI::base() . 'components/com_igallery/cerabox.css');
	$document->addScript(JURI::base() . 'components/com_igallery/cerabox.min.js');
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	
	$config =& JFactory::getConfig();
    $filepath = $config->getValue( 'config.file_path' );
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Quotes'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   $where = ' WHERE publish = 1 && id = "'.$cirid.'" ';
  

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(quotes_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(quotes_description)) LIKE '%".$search."%' ) ";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_quotes".$where);
    $total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_quotes
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
	
	foreach ($rows as $row){	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }  
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			
			$title = $row->quotes_name;
			$descp = $row->quotes_description;
			$image = $row->quotes_file;
			if($tag == 'ar-AA') :
				$title = $row->quotes_name_ar;
				$descp = $row->quotes_description_ar;
			endif;
			$title = strip_tags($title);
			$length = 20;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
			$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowquotes'));
            ?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title_quote"><?php echo $title; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <div class="body p2">
			<?php echo $descp; ?>
            </div>
            <?php if ($image != '') : ?>
            <div class="inner_pages_link_download_view">
                	<?php
						$document->addScriptDeclaration("
							document.addEvent('domready', function() {
								$$('.quote  a').cerabox({
									displayTitle: false
								});
							});
						");
				//print_r($row);		
				if ($row->quotes_file != '') {
					//$img = JRoute::_('index.php?option=com_eposts&view=loadimage&myfile='.$filepath.'\\'.$row->quotes_file);
					//$img = 'file:'.$filepath.'\\'.$row->quotes_file;
					$img = $filepath.'\\'.$row->quotes_file;
					$img2 = $filepath.'\\'.$row->quotes_file;
					$img = str_replace("\\", "/", $img);
					
					$img = $filepath.'\\'.$row->quotes_file;
			$img = str_replace("\\", "/", $img);
			
			$filetype=string;       
	   $imgbinary = fread(fopen($img, "r"), filesize($img));
      // echo $imgbinary;
	   $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
					
				}
                    ?>
                	<a href="#$img_my">
					<?php  $document->addScriptDeclaration(' var img_my = "<img src=\"'.$aaa.'\" />";'); ?>
                		<?php print JText::_('EMIRATES_VIEWIMAGE'); ?>
                    </a>
            </div>
            <?php endif;?>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowQuotes

function mainListManuals($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Manuals & Guides'));
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Manuals & Guides'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   $deptid = (int)JRequest::getString('deptid', 0);
   
   $deptquery = "";
 
   if($user->id && $deptid){
$deptquery .= " (manuals_access = 1 || manuals_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && manuals_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " manuals_access = 1 && manuals_department = '".$deptid."' ";
   }else{
	    $deptquery .= " manuals_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' ';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(manuals_name) LIKE '%".$search."%' ) ";
    }    
	
   	$database->SetQuery( "SELECT count(*) FROM #__eposts_manuals". $where);
    $total = $database->loadResult(); 
	 
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_manuals
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->manuals_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
       }
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;       
    }
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td class="main_filter">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowmanuals&cirid=".$row->id);
			
			$title = $row->manuals_name;
			if ($tag == 'ar-AA') :
				$title = $row->manuals_name_ar;
			endif;
			$title = strip_tags($title);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?>, <?php echo JText::_('Manual Date:'); ?> <?php echo date('d-m-Y', strtotime($row->manuals_manualdate)); ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_DOWNLOAD'); ?> 
            <?php if ($tag == 'ar-AA') : ?>
            <a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->manuals_doc_ar); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a>
            <?php else : ?>
            <a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->manuals_doc); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a>
            <?php endif; ?>
            </div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
//echo $footer; 
}//end funtion mainListManuals

function mainShowManuals($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Manuals & Guides'));
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Manuals & Guides'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   $deptid = (int)JRequest::getString('deptid', 0);
   
$deptquery = "";
 
   if($user->id && $deptid){
$deptquery .= " (manuals_access = 1 || manuals_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) && manuals_department = '".$deptid."' ";
   }elseif($deptid){
	    $deptquery .= " manuals_access = 1 && manuals_department = '".$deptid."' ";
   }else{
	    $deptquery .= " manuals_access = 1 ";	 
   }
   
    $where = ' WHERE publish = 1 && '.$deptquery.' && id = "'.$cirid.'" ';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(manuals_name) LIKE '%".$search."%' ) ";
    }    
   	$database->SetQuery( "SELECT count(*) FROM #__eposts_manuals". $where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_manuals
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->manuals_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
       }  
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;     
    }
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->manuals_department."&Itemid=103");
						
			$title = $row->manuals_name;
			if ($tag == 'ar-AA') :
				$title = $row->manuals_name_ar;
			endif;
			$title = strip_tags($title);
			$length = 20;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			
			$breadcrumbs =& $mainframe->getPathWay();
			$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowmanuals'));
            ?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title"><?php echo $title; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date('d-m-Y', strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?>, <?php echo JText::_('Manual Date:'); ?> <?php echo date('d-m-Y', strtotime($row->manuals_manualdate)); ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_DOWNLOAD'); ?> 
            <?php if ($tag == 'ar-AA') : ?>
            <a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->manuals_doc_ar); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a>
            <?php else : ?>
            <a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->manuals_doc); ?>"><?php print JText::_('COM_EPOSTS_DOWNLOAD_LINK'); ?></a>
            <?php endif; ?>
            </div>
            <div>&nbsp;</div>

        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowManuals

function mainListBenchmarks($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title, $deptid;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Benchmarks'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Benchmarks'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   $where = ' WHERE 1 = 1 AND publish = 1 ';
   if($deptid){
   $where .= 'AND benchmarks_department = '.$deptid;
   }

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(benchmarks_name) LIKE '%".$search."%' ";
        $where  .= " OR LOWER(benchmarks_name_ar) LIKE '%".$search."%' ) ";
    }    
   	$database->SetQuery( "SELECT count(*) FROM #__eposts_benchmarks".$where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_benchmarks
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->benchmarks_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
       } 
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;      
    }    
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td class="main_filter">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = JRoute::_("index.php?option=com_eposts&view=mainshowbenchmarks&cirid=".$row->id);
			$title = $row->benchmarks_name;
			$descp = $row->benchmarks_description;
			if ($tag == 'ar-AA') :
				$title = $row->benchmarks_name_ar;
				$descp = $row->benchmarks_description_ar;
			endif;
			$title = strip_tags($title);
			$title = strip_tags($title);
			$length = 400;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <div class="body">
			<?php echo $descp; ?>
            </div>
            <div>&nbsp;</div>
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
//echo $footer; 
}//end funtion mainListBenchmarks

function mainShowBenchmarks($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Benchmarks'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Benchmarks'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = (int)JRequest::getString('cirid', 0);
   $where = ' WHERE 1 = 1 && id = "'.$cirid.'"';
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where  .= " AND ( LOWER(benchmarks_name) LIKE '%".$search."%' ) ";
    }    
   	$database->SetQuery( "SELECT count(*) FROM #__eposts_benchmarks".$where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_benchmarks
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->benchmarks_department
            );
   
       $departments = $database->loadAssocList();
       foreach ($departments as $department){
            $row->department = $department[departments_name];
			if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
       }  
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;     
    }    
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->benchmarks_department."&Itemid=103");
			$title = $row->benchmarks_name;
			$descp = $row->benchmarks_description;
			if ($tag == 'ar-AA') :
				$title = $row->benchmarks_name_ar;
				$descp = $row->benchmarks_description_ar;
			endif;
			$title = strip_tags($title);
			$length = 20;
			$title_bread = $title;
			if (mb_strlen($title_bread) > $length) :
				$title_bread = mb_substr($title, 0, $length) . '...';
			endif;
			$breadcrumbs =& $mainframe->getPathWay();
			$breadcrumbs->addItem($title_bread, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=mainshowbenchmarks'));
            ?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div class="title"><?php echo $title; ?></div>
            <div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDON'); ?> <?php echo date("d-m-Y H:i:s", strtotime($row->publishedon)); ?>, <?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
            <div class="body">
            <?php echo $descp; ?>
            </div>
            <div>&nbsp;</div> 
            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowBenchmarks

//new Objectives
function newObjectives($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Department Objective'));
/////////////////////////////////////////////////////////////////////	
	$cirid = (int)JRequest::getString('cirid', 0);

    $row = new jlist_departments( $database );
    $row->load( $cirid );
    $cdipartment = array();
		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Department Objective'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
    
    
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( tinyMCE.get('departments_description').getContent()== ""
			 || tinyMCE.get('departments_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
    </script>
<?php  
	$link = JRoute::_("index.php?option=com_eposts&view=listobjectives&cirid=".$row->id);
	
	$breadcrumbs =& $mainframe->getPathWay();
	$title = $cirid == 0 ? JText::_('New Objective') :  JText::_('Edit Objective');
	$length = 20;
	if (mb_strlen($title) > $length) :
		$title = mb_substr($title, 0, $length) . '...';
	endif;
    $breadcrumbs->addItem($title, JRoute::_('index.php?option='.$option.'&Itemid='.$Itemid.'&view=newobjectives'));
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data" onSubmit="return validateform();">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="midAdmin">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" align="left"><?php echo $row->id ? 'Edit Objective' : 'Add Objective';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                            <tr>
                                <td align="right"><strong>Department Objective (English)<font color='#990000'>*</font>:</strong><td><td> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'departments_description',  @$row->departments_description , '100%', '10', '80', '5', false, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td align="right"><strong>Department Objective (Arabic)<font color='#990000'>*</font>:</strong><td><td>                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'departments_description_ar',  @$row->departments_description_ar , '100%', '10', '80', '5', false, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly value="<?php  if($row->publishedon){ echo date('d-m-Y H:i:s', strtotime($row->publishedon));}else{ echo date('d-m-Y H:i:s');}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="center" colspan="2">&nbsp;</td><td>
                                  <input name="submit" type="submit" value="Save"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cirid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.objectives" />
    </form>
<?php
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
	
	?>
	<!--<div class="back_button" style="text-align:left"><a href="javascript:history.go(-1)"><?php print JText::_('COM_EPOSTS_CANCEL_BTN'); ?></a></div> -->
	<?php
			      
}//end funtion newObjectives 

//listObjectives
function listObjectives($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Department Objective'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Department Objective'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  

    $error_msg = str_replace('<br />', '\n', JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_NO_VALUE'));
    $error_msg_ext = JText::_('COM_EPOSTS_FRONTEND_UPLOAD_ERROR_WRONG_FILE_EXT');
  	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
	$form = '';
	echo $form; 	
//////////////////////////////////////////////////

    $where = ' WHERE 1 = 1  && (id IN (SELECT department_id FROM #__eposts_department_members WHERE user_id ="'.$user->id.'") || publishedby = "'.$user->id.'")';
	
   

    $search = JRequest::getVar('search', '');
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {

        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= " AND ( LOWER(departments_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(fnStripTags(departments_description)) LIKE '%".$search."%' ) ";
    }    
    $database->SetQuery( "SELECT count(*) FROM #__eposts_departments". $where);
    $total = $database->loadResult();  
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_departments
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();

    
	$newlink = JRoute::_("index.php?option=com_eposts&view=newobjective");   
?>	
<!--<a class="brightNew" href="<?php //echo $newlink;?>">New Record</a>-->
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">

    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
      <!--<tr>
          <td  align="right" colspan="3">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.uploadForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr> -->
        <tr>
            <th valign="top" align="left" class="title">Department Name</th>
            <th valign="top" align="left" class="title">Objective</th>
            <!--<th valign="top" align="left" class="title">Actions</th>-->
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$link = JRoute::_("index.php?option=com_eposts&view=newobjectives&cirid=".$row->id);
			$dellink = JRoute::_("index.php?option=com_eposts&view=newobjectives&task=delvisions&cirid=".$row->id);
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left"><a href="<?php echo $link; ?>" title="Edit Objective"><?php echo $row->departments_name; ?></a></td>
            <td valign="top" align="left"><?php echo $row->departments_description; ?></td>
            <!--<td valign="top" align="left"><a href="<?php //echo $dellink; ?>">Delete</a></td>-->
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $view; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion listObjectives

	
	//////////////////////////////END Depevloment Phase IV////////////////////////////////
	

///////////////////////////////////Depevloment Phase VI ////////////////////////////

//mainApplyJobs
function mainApplyJobs($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Job Application'));
/////////////////////////////////////////////////////////////////////	
	$JobId = JRequest::getString('cirid', 0);
	$JobTitle = JRequest::getString('jobtitle');
	

    $database->setQuery("SELECT id,departments_name "
                . "\n FROM #__eposts_departments "
                . "\n ORDER BY departments_name ASC"
            );
        $listDepartments = $database->loadObjectList();

        foreach($listDepartments as $xdepartment) {
            $cdipartment[] = JHTML::_('select.option',$xdepartment->departments_name,
                    $xdepartment->departments_name 
                    );
        }
    
    $departmentList = JHTML::_('select.genericlist',$cdipartment, 'department',
        'class="inputbox" ', 'value', 'text', '');

		
//////////////////////////////////////////////
	                        
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('mainApplyJobs'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);     
	    
    $uri = JFactory::getURI();
    $uri = $uri->toString();
    $uri = escape_string($uri);
?>
	<script type="text/Javascript" language="JavaScript">
   function validateform(){
        var form = document.uploadForm;

		// do field validation
        if ( form.staffId.value == "" 
		     || form.staffName.value == ""
			 || form.currentGrade.value == ""
			 || form.currentSalary.value == ""
			 || form.companyName.value == ""
			 || form.department.value == ""
			 || form.location.value == ""
			 || form.nationality.value == ""
			 || form.totalExperienceInYears.value == ""
			 || form.cv_file.value == ""
			  ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
			return false;
        } else {
            return true;
        }
    }
	
	function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57)){
			 alert('Only integer value is allowed.');
            return false;
		 }
         return true;
      }
	    function minmax(value, min, max) 
{
    if(parseInt(value) < 0 || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > 60) 
        return 60; 
    else return value;
}

    </script>
<?php 


require_once dirname(__FILE__).'/nusoap/lib/nusoap.php';

                                                //$client = new nusoap_client("http://intranet/irecwstest/WS_ApplicantManager.svc?wsdl", true);
												$client = new nusoap_client("http://intranet/irecruitmentservice/WS_ApplicantManager.svc?wsdl", true);  // Production Webservice

                                                $error = $client->getError();

                                                if ($error) {

                                                                echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";

                                                }

                                                $client->soap_defencoding = 'UTF-8';

                                                $result = $client->call("GetOpenJobs", array("candidateid" => "", "target"=>"Internal", "targetSpecified"=>"True" ));

                                                $postParams = array(

                                                            "candidateId" => $user->username, 

                                                                                                "vacancyCode" => $JobId                                                                                             

                                                                                                );

                                                $result = $client->call("CheckAppliedJob", $postParams);

                $output = $result['CheckAppliedJobResult'];

                

//print_r($output);

 //echo $user->email;
     if($output == 'true'){ 
	 
	 $from = "﻿﻿spsadmin@epg.gov.ae"; // sender
    $subject = "Applied for vacancy ".$JobTitle."";
    $message = "Thank you for Applying for the Vacancy ".$JobTitle.". To follow up on your application, please contact Careers@epg.gov.ae / 04-2303999 .";
    // message lines should not exceed 70 characters (PHP rule), so wrap it
    $message = wordwrap($message, 70);
    // send mail
    mail($user->email,$subject,$message,"From: $from\n");
	 ?>
	 
	 <div style="ine-height:30px; text-align:center;"><?php print JText::_('EPOSTS_JOB_MSG03'); ?> "<?php print  $JobTitle; ?>". <?php print JText::_('EPOSTS_JOB_MSG04'); ?> <a href='mailto:Careers@epg.gov.ae'>Careers@epg.gov.ae</a> / 04-2303999.</div>
			

<?php
  
} 
else{
         

	
	$link = JRoute::_("index.php?option=com_eposts&amp;view=mainApplyJobs");
?>
    <form name="uploadForm" id="uploadForm" action="<?php echo $link;?>" method="post" enctype="multipart/form-data"  onsubmit="return validateform();">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                   <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable"> 
                             <tr>
                                <td align="right"style="display:none;"><strong><?php echo JText::_('COM_EPOSTS_STAFF_ID')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="staffId" value="0"style="display:none;" size="80" maxlength="20"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_STAFF_NAME')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="staffName" value="" size="80" maxlength="30"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_CRR_GRADE')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="currentGrade" onKeyPress="return isNumberKey(event)" value="" size="80" maxlength="2"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_CRR_SALARY')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="currentSalary" value="" onKeyPress="return isNumberKey(event)" size="80" maxlength="6"/>
                                                                 
                            </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_COMPANY')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="companyName" value="" size="80" maxlength="100"/>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_DEP_SEC')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <?php //echo $departmentList;?> 
                                    <input name="department" value="" size="80" maxlength="100"/>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_LOCATION')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="location" value="" size="80" maxlength="100"/>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_NATIONALITY')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="nationality" value="" size="80" maxlength="100"/>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_EXPER')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="totalExperienceInYears" value="" onKeyPress="return isNumberKey(event)" size="80" maxlength="2"onkeyup="this.value = minmax(this.value, 0, 60)"/>
                                   </td>
                           </tr>
                           <tr>
                                <td align="right"><strong><?php echo JText::_('COM_EPOSTS_CV')."<font color='#990000'>*</font>"; ?>:</strong><td><td>
                                    <input name="cv_file" type="file"/>
                                   </td>
                           </tr>
                           <!--<tr>
                                <td align="right"><strong>Published On:</strong><td><td>
                                   <input name="firstshow" disabled="disabled" readonly="readonly" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td align="right"><strong>Published By:</strong><td><td>
                                    <input name="seconfshow" disabled="disabled" readonly="readonly"  value="<?php  
									$showpub = ($row->publishedby)? $row->publishedby:$user->id;
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $showpub."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
									?>" size="80" maxlength="150"/>
                                                                 
                            </tr> -->
                           <tr>
                                <td align="center" colspan="4">
                                  <input name="submit" type="submit" value="Apply"/>
                                  <input name="submit" type="reset" onClick="javascript:history.go(-1)" value="Cancel"/>
                                                                 
                            </td></tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="jobtitle" value="<?php echo $JobTitle; ?>" />
        <input type="hidden" name="initiatedById" value="<?php echo $user->username; ?>" />
        <input type="hidden" name="JobId" value="<?php echo $JobId; ?>" />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="view" value="<?php  echo $view;?>" />
        <input type="hidden" name="task" value="edit.mainapplyjobs" />
    </form>
<?php
}
	/*$form = "<div>Welcome to new Circular</div>";
		echo $form;*/
//echo $footer; 
			      
}//end funtion mainApplyJobs


function mainShowJobs($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Job Board'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('Job Board'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid
   $cirid = JRequest::getString('cirid');
            require_once dirname(__FILE__).'/nusoap/lib/nusoap.php';
			//$client = new nusoap_client("http://intranet/irecruitmentservice/WS_ApplicantManager.svc?wsdl", true);
			$client = new nusoap_client("http://intranet/irecruitmentservice/WS_ApplicantManager.svc?wsdl", true);  // Production Webservice
			$error = $client->getError();
			if ($error) {
				echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
			}
			$client->soap_defencoding = 'UTF-8';
			$result = $client->call("GetOpenJobs", array("candidateid" => "", "target"=>"Internal", "targetSpecified"=>"True" ));
    
?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" >
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
     <?php	
if(count($result) > 0){
				
			$output = simplexml_load_string($result['GetOpenJobsResult'], NULL, LIBXML_NOCDATA);
			$limit = 5;	
			$i = 0;	
			$k = 0;
			foreach ($output->vaccancyinforow as $item) {
				if($cirid == ((string)$item->VACCANCYCODE)){
				$applylink = JRoute::_("index.php?option=com_eposts&view=mainapplyjobs&page=home&cirid=".((string)$item->VACCANCYCODE).'&Itemid=303&jobtitle='.((string)$item->JOBTITLE));
				?>
         <tr class="<?php echo "row$k"; ?>">
            <td valign="top" align="left">
            <div><?php print JText::_('Reference Code: ') . ((string)$item->VACCANCYCODE); ?></div>
			<div class="title"><?php print JText::_('Job Title: '); ?><?php echo ((string)$item->JOBTITLE); ?></div>
            <div><?php print JText::_('Job Description: ') . ((string)$item->JOBDESCRIPTION); ?></div>
            <div><?php print JText::_('Company: ') . ((string)$item->COMPANY); ?></div>
            <div><?php print JText::_('Department: ') . ((string)$item->DEPARTMENT); ?></div>
            <div><?php print JText::_('Grade: ')  . ((string)$item->JOBGRADE); ?></div>
            <div><?php print JText::_('Last Date: ') . date('d-m-Y', strtotime(((string)$item->CLOSEDATE))); ?></div>
            <!--<div><?php print JText::_('More Details: ') . ''; ?></div>
            <div><?php print JText::_('Apply: '); ?><a href="<?php echo $applylink; ?>">Click Here</a></div> -->
            <div class="body">
			<?php //echo $row['JOBDESCRIPTION'][0]['values'][0]; ?>
            </div>
            <div>&nbsp;</div>
            <div>
                <div class="controlls">
                	<a class="readmore" style="float:none; color:#FFFFFF;" href="<?php echo $applylink; ?>"><span><?php print JText::_("COM_EPOSTS_APPLY_BTN");?></span></a>
                </div>
            </div>
        </tr>
        <?php 
				}
				$k = 1 - $k;
			}			
}
?> 
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainShowJobs

function mainListJobs($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('Job Board'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('mainListJobs'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////

///////////////////////////				
            require_once dirname(__FILE__).'/nusoap/lib/nusoap.php';
			//$client = new nusoap_client("http://intranet/irecruitmentservice/WS_ApplicantManager.svc?wsdl", true);  // Production Webservice
			$client = new nusoap_client("http://intranet/irecwstest/WS_ApplicantManager.svc?wsdl", true); //Development WebService
			$error = $client->getError();
			if ($error) {
				echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
			}
			$client->soap_defencoding = 'UTF-8';
			$client->decode_utf8 = false;
			$client->encode_utf8 = true;
			//$result = $client->call("GetOpenJobs", array("candidateid" => "", "target"=>"Internal", "targetSpecified"=>"True" ));
			if ($_REQUEST['lang']=="ar")
			{
				$lang='ar';	
			}else{
				$lang='en';	
			}
			$result = $client->call("GetOpenJobsByLanguage", array("candidateid" => "", "target"=>"Internal", "targetSpecified"=>"True" , "language"=>$lang ));
    
 ?>
 <form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" >
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist" dir="ltr">
    <?php	
if(count($result) > 0){
				
			$output = simplexml_load_string($result['GetOpenJobsByLanguageResult'], NULL, LIBXML_NOCDATA);
			//$output = array_reverse($output);
			$limit = 5;	
			$i = 0;	
			$k = 0;
			foreach ($output->vaccancyinforow as $item) {
				if($i >= $limit) continue;
				//$applylink = JRoute::_("index.php?option=com_eposts&view=mainshowjobs&page=home&cirid=".((string)$item->VACCANCYCODE).'&Itemid=303&jobtitle='.((string)$item->JOBTITLE));
				$applylink = JRoute::_("index.php?option=com_eposts&view=mainapplyjobs&page=home&cirid=".((string)$item->VACCANCYCODE).'&Itemid=303&jobtitle='.((string)$item->JOBTITLE));
				/*echo 'VACCANCYCODE'.((string)$item->VACCANCYCODE);
				echo 'JOBTITLE'.((string)$item->JOBTITLE);
				echo 'COMPANY'.((string)$item->COMPANY);
				echo 'DEPARTMENT'.((string)$item->DEPARTMENT);
				echo 'LOCATION'.((string)$item->LOCATION);
				echo 'CLOSEDATE'.((string)$item->CLOSEDATE);*/
				?>
                 <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			 <?php if ($_REQUEST['lang']=="ar")
{?>
			<div id="default-colm-two">
			<div class="default-colm-left">
			<div dir="rtl"style="font-family:Tahoma;"><?php print JText::_(' الرمز المرجعي: ') . ((string)$item->VACCANCYCODE); ?></div>
			<div dir="rtl" class="default-list-title" style="font-family:Tahoma;"><?php print JText::_('المسمى الوظيفي: '); ?><?php echo ((string)$item->JOBTITLE); ?></div>
            <div class="default-list-decs"> 
			<span class="default-list-decs-head" style="font-family:Tahoma;"><!--<div><?php print JText::_('الوظيفي وصف : ') . ((string)$item->JOBDESCRIPTION); ?></div> -->
            <div dir="rtl" style="font-family:Tahoma;"><?php print JText::_('الشركة: ') . ((string)$item->COMPANY); ?></div>
            <div dir="rtl" style="font-family:Tahoma;"><?php print JText::_('لإدارة:') . ((string)$item->DEPARTMENT); ?></div>
            <div dir="rtl" style="font-family:Tahoma;"><?php print JText::_('الدرجة الوظيفية : ')  . ((string)$item->JOBGRADE); ?></div>
            <div dir="rtl" style="font-family:Tahoma;"><?php print JText::_('رمز الوظيفة: ') . ((string)$item->IREC_TBLCOL_JOBCODE); ?></div>
            <div dir="rtl" style="font-family:Tahoma;"><?php print JText::_('موقع: ') . ((string)$item->LOCATION); ?></div>
            <div dir="rtl" style="font-family:Tahoma;"><?php print JText::_('قومية: ') . ((string)$item->NATIONALITY); ?></div>
			<div dir="rtl" style="font-family:Tahoma;"><?php print JText::_('آخر يوم للتقديم: ') . date('d-m-Y', strtotime(((string)$item->CLOSEDATE))); ?></div>
			
            <!--<div><?php print JText::_('More Details: ') . ''; ?></div> --></div>
			<div><a style="COLOR: #ffffff;  float:right;" class=readmore href="<?php echo $applylink; ?>"><SPAN> التقديم</SPAN></a></div>
            </div>
            <div class="default-colm-right">
			<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
            <div class="sub_title"><div class="sub_title"><div class="form_download_icon"><a href="modules/mod_jobboard/tmpl/download.php?hello=true&jbcode=<?php echo ((string)$item->IREC_TBLCOL_JOBCODE); ?>">
				<img src="components/com_eposts/assets/images/icons/icon-pdf.png" border="0" alt="" /></a></div></div>
  
   </div></div>
			</div>
			</div>
			<?php } else{?>
			<div id="default-colm-two">
			<div class="default-colm-left">
			<div><?php print JText::_('Reference Code: ') . ((string)$item->VACCANCYCODE); ?></div>
			<div class="default-list-title"><?php print JText::_('Job Title: '); ?><?php echo ((string)$item->JOBTITLE); ?></div>
            <div class="default-list-decs"> 
			<span class="default-list-decs-head"><!--<div><?php print JText::_('Job Description: ') . ((string)$item->JOBDESCRIPTION); ?></div> -->
            <div><?php print JText::_('Company: ') . ((string)$item->COMPANY); ?></div>
            <div><?php print JText::_('Department: ') . ((string)$item->DEPARTMENT); ?></div>
            <div><?php print JText::_('Grade: ')  . ((string)$item->JOBGRADE); ?></div>
            <div><?php print JText::_('Job Code: ')  . ((string)$item->JOBCODE); ?></div>
            <div><?php print JText::_('Location: ') . ((string)$item->LOCATION); ?></div>
            <div><?php print JText::_('Nationality: ') . ((string)$item->NATIONALITY); ?></div>
			<div><?php print JText::_('Last Date: ') . date('d-m-Y', strtotime(((string)$item->CLOSEDATE))); ?></div>
			
			
            <!--<div><?php print JText::_('More Details: ') . ''; ?></div> -->
			
			</div>
			<div><a style="COLOR: #ffffff;  float:left;" class=readmore href="<?php echo $applylink; ?>"><SPAN>Apply</SPAN></a></div>
            </div>
            <div class="default-colm-right">
			<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
            <div class="sub_title"><div class="sub_title"><div class="form_download_icon"><a href="modules/mod_jobboard/tmpl/download.php?hello=true&jbcode=<?php echo ((string)$item->JOBCODE); ?>">
				<img src="components/com_eposts/assets/images/icons/icon-pdf.png" border="0" alt="" /></a></div></div>
  
   </div></div>
			</div>
			</div>
			<?php }?>
			<div>&nbsp;</div>            
        </tr>
				<?php
				$k = 1 - $k;
			}
}
?> 
  </table></div>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="deptid" value="<?php echo $deptid; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php
	//echo $footer; 
}//end funtion mainListJobs

function mainShowTasks($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('mainShowTasks'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('mainShowTasks'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////cirid

	//echo $footer; 
}//end funtion mainShowTasks

///////////////////////////////////END Depevloment Phase VI ////////////////////////////

//end of class
}
?>