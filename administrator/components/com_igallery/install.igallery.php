<?php
defined('_JEXEC') or die('Restricted access');	

define('IG_ADMIN_ROOT', JPATH_ADMINISTRATOR.DS.'components'.DS.'com_igallery');
define('IG_FRONTEND_ROOT', JPATH_SITE.DS.'components'.DS.'com_igallery');

$filesToDelete = array();
$filesToDelete[] = IG_FRONTEND_ROOT.DS.'views'.DS.'category'.DS.'tmpl'.DS.'default.php';

for($i=0; $i<count($filesToDelete); $i++)
{
    if(JFile::exists($filesToDelete[$i]))
    {
        if( !JFile::delete($filesToDelete[$i]) )
        {
            echo 'The Unused File: '.$filesToDelete[$i]. ' could not be removed, 
            please remove it manually <br />';
        }
    }
}

$iDb =& JFactory::getDBO();
$queries = array();
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `asset_id` int(10) NOT NULL";
$queries[] = "ALTER TABLE `#__igallery_img` ADD `moderate` INT(1) NOT NULL DEFAULT '1'";
$queries[] = "ALTER TABLE `#__igallery` ADD `moderate` INT(1) NOT NULL DEFAULT '1'";
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `slideshow_position` VARCHAR(24) NOT NULL DEFAULT 'below'";
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `lbox_slideshow_position` VARCHAR(24) NOT NULL DEFAULT 'below'";
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `show_filename` VARCHAR(24) NOT NULL DEFAULT 'none'";
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `lbox_show_filename` VARCHAR(24) NOT NULL DEFAULT 'none'";
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `show_numbering` INT(1) NOT NULL DEFAULT '0'";
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `lbox_show_numbering` INT(1) NOT NULL DEFAULT '0'";
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `show_thumb_info` VARCHAR(24) NOT NULL DEFAULT 'none'";
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `lbox_show_thumb_info` VARCHAR(24) NOT NULL DEFAULT 'none'";
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `plus_one` INT(1) NOT NULL DEFAULT '0'";
$queries[] = "ALTER TABLE `#__igallery_profiles` ADD `lbox_plus_one` INT(1) NOT NULL DEFAULT '0'";



for($i=0;$i<count($queries); $i++)
{
	$iDb->setQuery($queries[$i]);
	$iDb->query();
}
?>
    