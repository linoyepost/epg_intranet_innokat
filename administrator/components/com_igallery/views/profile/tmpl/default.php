<?php defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.framework', true);
?>
<form action="index.php?option=com_igallery" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
	<div class="width-100 fltlft">
		
		<?php echo JHtml::_('tabs.start','igallery_profiles', array('useCookie'=>1)); ?>
		
		<?php echo JHtml::_('tabs.panel',JText::_('GENERAL'), 'igallery_profiles_general'); ?>
		<fieldset class="adminform">
		<legend><?php echo JText::_('GENERAL'); ?></legend>
			<ul class="adminformlist">
			
			<li><?php echo $this->form->getLabel('name'); ?>
			<?php echo $this->form->getInput('name'); ?></li>
			
			<li><?php echo $this->form->getLabel('published'); ?>
			<?php echo $this->form->getInput('published'); ?></li>
			
			<li><?php echo $this->form->getLabel('access'); ?>
			<?php echo $this->form->getInput('access'); ?></li>
			
			<li><?php echo $this->form->getLabel('img_quality'); ?>
			<?php echo $this->form->getInput('img_quality'); ?></li>
			
			<li><?php echo $this->form->getLabel('show_search'); ?>
			<?php echo $this->form->getInput('show_search'); ?></li>
			
			<li><?php echo $this->form->getLabel('search_results'); ?>
			<?php echo $this->form->getInput('search_results'); ?></li>
			
			<li><?php echo $this->form->getLabel('show_cat_title'); ?>
			<?php echo $this->form->getInput('show_cat_title'); ?></li>
			
			<li><?php echo $this->form->getLabel('style'); ?>
			<?php echo $this->form->getInput('style'); ?></li>
			
			<li><?php echo $this->form->getLabel('refresh_mode'); ?>
			<?php echo $this->form->getInput('refresh_mode'); ?></li>
		</ul>
		</fieldset>
		
		<fieldset class="adminform">
		<legend><?php echo JText::_('CORNERS'); ?></legend>
		<ul class="adminformlist">
		
		<li><?php echo $this->form->getLabel('round_large'); ?>
			<?php echo $this->form->getInput('round_large'); ?></li>
			
		<li><?php echo $this->form->getLabel('round_thumb'); ?>
			<?php echo $this->form->getInput('round_thumb'); ?></li>
		
		<li><?php echo $this->form->getLabel('round_menu'); ?>
			<?php echo $this->form->getInput('round_menu'); ?></li>
		
		<li><?php echo $this->form->getLabel('round_fill'); ?>
			<?php echo $this->form->getInput('round_fill'); ?></li>
		</ul>
		</fieldset>
		
		<fieldset class="adminform">
		<legend><?php echo JText::_('WATERMARK'); ?></legend>
		<ul class="adminformlist">
			<li><?php echo $this->form->getLabel('watermark'); ?>
			<?php echo $this->form->getInput('watermark'); ?></li>
			
			<li><?php echo $this->form->getLabel('watermark_position'); ?>
			<?php echo $this->form->getInput('watermark_position'); ?></li>
			
			<li><?php echo $this->form->getLabel('watermark_transparency'); ?>
			<?php echo $this->form->getInput('watermark_transparency'); ?></li>
			
			<li><?php echo $this->form->getLabel('watermark_text'); ?>
			<?php echo $this->form->getInput('watermark_text'); ?></li>
			
			<li><?php echo $this->form->getLabel('watermark_text_color'); ?>
			<?php echo $this->form->getInput('watermark_text_color'); ?></li>
			
			<li><?php echo $this->form->getLabel('watermark_text_size'); ?>
			<?php echo $this->form->getInput('watermark_text_size'); ?></li>
			
			<li><?php echo $this->form->getLabel('watermark_filename'); ?>
			
			<?php if( strlen($this->item->watermark_filename) > 1 ): ?>
		  		<img src="<?php echo IG_IMAGE_HTML_WATERMARK; ?><?php echo $this->item->watermark_filename; ?>" alt=""/>
			<?php endif; ?>	
			
		<?php echo $this->form->getInput('watermark_filename'); ?></li>
			
			<li><?php echo $this->form->getLabel('remove_wm_image'); ?>
			<?php echo $this->form->getInput('remove_wm_image'); ?></li>
		</ul>
		</fieldset>
	
	
	<?php echo JHtml::_('tabs.panel',JText::_('MENU'), 'igallery_profiles_menu'); ?>
		<fieldset class="adminform">
		<ul class="adminformlist">
		
			<li><?php echo $this->form->getLabel('menu_access'); ?>
			<?php echo $this->form->getInput('menu_access'); ?></li>
			
			<li><?php echo $this->form->getLabel('menu_max_width'); ?>
			<?php echo $this->form->getInput('menu_max_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('menu_max_height'); ?>
			<?php echo $this->form->getInput('menu_max_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('crop_menu'); ?>
			<?php echo $this->form->getInput('crop_menu'); ?></li>
			
			<li><?php echo $this->form->getLabel('menu_image_defaults'); ?>
			<?php echo $this->form->getInput('menu_image_defaults'); ?></li>
			
			<li><?php echo $this->form->getLabel('columns'); ?>
			<?php echo $this->form->getInput('columns'); ?></li>
			
			<li><?php echo $this->form->getLabel('show_category_hits'); ?>
			<?php echo $this->form->getInput('show_category_hits'); ?></li>
			
			<li><?php echo $this->form->getLabel('menu_pagination'); ?>
			<?php echo $this->form->getInput('menu_pagination'); ?></li>
			
			<li><?php echo $this->form->getLabel('menu_pagination_amount'); ?>
			<?php echo $this->form->getInput('menu_pagination_amount'); ?></li>
		</ul>
		
		</fieldset>
		
		
		<?php echo JHtml::_('tabs.panel',JText::_('MAIN_IMAGE'), 'igallery_profiles_mainimage'); ?>
		<fieldset class="adminform">
		<ul class="adminformlist">
			<li><?php echo $this->form->getLabel('show_large_image'); ?>
			<?php echo $this->form->getInput('show_large_image'); ?></li>
			
			<li><?php echo $this->form->getLabel('max_width'); ?>
			<?php echo $this->form->getInput('max_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('max_height'); ?>
			<?php echo $this->form->getInput('max_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('crop_main'); ?>
			<?php echo $this->form->getInput('crop_main'); ?></li>
			
			<li><?php echo $this->form->getLabel('img_container_width'); ?>
			<?php echo $this->form->getInput('img_container_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('img_container_height'); ?>
			<?php echo $this->form->getInput('img_container_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('fade_duration'); ?>
			<?php echo $this->form->getInput('fade_duration'); ?></li>
			
			<li><?php echo $this->form->getLabel('preload'); ?>
			<?php echo $this->form->getInput('preload'); ?></li>
			
			<li><?php echo $this->form->getLabel('magnify'); ?>
			<?php echo $this->form->getInput('magnify'); ?></li>
		</ul>
		</fieldset>
		
		<?php echo JHtml::_('tabs.panel',JText::_('MAIN_THUMBNAILS'), 'igallery_profiles_mainimage'); ?>
		<fieldset class="adminform">
		<ul class="adminformlist">
			<li><?php echo $this->form->getLabel('show_thumbs'); ?>
			<?php echo $this->form->getInput('show_thumbs'); ?></li>
			
			<li><?php echo $this->form->getLabel('thumb_width'); ?>
			<?php echo $this->form->getInput('thumb_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('thumb_height'); ?>
			<?php echo $this->form->getInput('thumb_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('crop_thumbs'); ?>
			<?php echo $this->form->getInput('crop_thumbs'); ?></li>
			
			<li><?php echo $this->form->getLabel('thumb_position'); ?>
			<?php echo $this->form->getInput('thumb_position'); ?></li>
			
			<li><?php echo $this->form->getLabel('thumb_container_width'); ?>
			<?php echo $this->form->getInput('thumb_container_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('thumb_container_height'); ?>
			<?php echo $this->form->getInput('thumb_container_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('images_per_row'); ?>
			<?php echo $this->form->getInput('images_per_row'); ?></li>
			
			<li><?php echo $this->form->getLabel('thumb_scrollbar'); ?>
			<?php echo $this->form->getInput('thumb_scrollbar'); ?></li>
			
			<li><?php echo $this->form->getLabel('arrows_up_down'); ?>
			<?php echo $this->form->getInput('arrows_up_down'); ?></li>
			
			<li><?php echo $this->form->getLabel('arrows_left_right'); ?>
			<?php echo $this->form->getInput('arrows_left_right'); ?></li>
			
			<li><?php echo $this->form->getLabel('show_thumb_info'); ?>
			<?php echo $this->form->getInput('show_thumb_info'); ?></li>
			
			<li><?php echo $this->form->getLabel('scroll_speed'); ?>
			<?php echo $this->form->getInput('scroll_speed'); ?></li>
			
			<li><?php echo $this->form->getLabel('scroll_boundary'); ?>
			<?php echo $this->form->getInput('scroll_boundary'); ?></li>
			
			<li><?php echo $this->form->getLabel('thumb_pagination'); ?>
			<?php echo $this->form->getInput('thumb_pagination'); ?></li>
			
			<li><?php echo $this->form->getLabel('thumb_pagination_amount'); ?>
			<?php echo $this->form->getInput('thumb_pagination_amount'); ?></li>
			
		</ul>
		</fieldset>	
		
		<?php echo JHtml::_('tabs.panel',JText::_('MAIN_OTHER'), 'igallery_profiles_mainimage'); ?>
		<fieldset class="adminform">
		<ul class="adminformlist">
			
			<li><?php echo $this->form->getLabel('allow_comments'); ?>
			<?php echo $this->form->getInput('allow_comments'); ?></li>
			
			<li><?php echo $this->form->getLabel('allow_rating'); ?>
			<?php echo $this->form->getInput('allow_rating'); ?></li>
			
			<li><?php echo $this->form->getLabel('gallery_des_position'); ?>
			<?php echo $this->form->getInput('gallery_des_position'); ?></li>
			
			<li><?php echo $this->form->getLabel('align'); ?>
			<?php echo $this->form->getInput('align'); ?></li>
			
			<li><?php echo $this->form->getLabel('download_image'); ?>
			<?php echo $this->form->getInput('download_image'); ?></li>
			
			<li><?php echo $this->form->getLabel('show_tags'); ?>
			<?php echo $this->form->getInput('show_tags'); ?></li>
			
			<li><?php echo $this->form->getLabel('report_image'); ?>
			<?php echo $this->form->getInput('report_image'); ?></li>
			
			<li><?php echo $this->form->getLabel('share_facebook'); ?>
			<?php echo $this->form->getInput('share_facebook'); ?></li>
			
			<li><?php echo $this->form->getLabel('plus_one'); ?>
			<?php echo $this->form->getInput('plus_one'); ?></li>
			
			<li><?php echo $this->form->getLabel('show_numbering'); ?>
			<?php echo $this->form->getInput('show_numbering'); ?></li>
			
		</ul>
		</fieldset>
		
		<fieldset class="adminform">
		<legend><?php echo JText::_('SLIDESHOW'); ?></legend>
		<ul class="adminformlist">
			<li><?php echo $this->form->getLabel('show_slideshow_controls'); ?>
			<?php echo $this->form->getInput('show_slideshow_controls'); ?></li>
			
			<li><?php echo $this->form->getLabel('slideshow_position'); ?>
			<?php echo $this->form->getInput('slideshow_position'); ?></li>
			
			<li><?php echo $this->form->getLabel('slideshow_autostart'); ?>
			<?php echo $this->form->getInput('slideshow_autostart'); ?></li>
			
			<li><?php echo $this->form->getLabel('slideshow_pause'); ?>
			<?php echo $this->form->getInput('slideshow_pause'); ?></li>
		</ul>
		</fieldset>
		
		<fieldset class="adminform">
		<legend><?php echo JText::_('IMAGE_DESCRIPTIONS'); ?></legend>
		<ul class="adminformlist">	
			<li><?php echo $this->form->getLabel('show_descriptions'); ?>
			<?php echo $this->form->getInput('show_descriptions'); ?></li>
			
			<li><?php echo $this->form->getLabel('photo_des_position'); ?>
			<?php echo $this->form->getInput('photo_des_position'); ?></li>
			
			<li><?php echo $this->form->getLabel('photo_des_width'); ?>
			<?php echo $this->form->getInput('photo_des_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('photo_des_height'); ?>
			<?php echo $this->form->getInput('photo_des_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('show_filename'); ?>
			<?php echo $this->form->getInput('show_filename'); ?></li>
		</ul>
		</fieldset>	
		
		<?php echo JHtml::_('tabs.panel',JText::_('LIGHTBOX_IMAGE'), 'igallery_profiles_mainimage'); ?>
		<fieldset class="adminform">
		<ul class="adminformlist">
			<li><?php echo $this->form->getLabel('lightbox'); ?>
			<?php echo $this->form->getInput('lightbox'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_max_width'); ?>
			<?php echo $this->form->getInput('lbox_max_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_max_height'); ?>
			<?php echo $this->form->getInput('lbox_max_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('crop_lbox'); ?>
			<?php echo $this->form->getInput('crop_lbox'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_img_container_width'); ?>
			<?php echo $this->form->getInput('lbox_img_container_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_img_container_height'); ?>
			<?php echo $this->form->getInput('lbox_img_container_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_fade_duration'); ?>
			<?php echo $this->form->getInput('lbox_fade_duration'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_preload'); ?>
			<?php echo $this->form->getInput('lbox_preload'); ?></li>
			
		</ul>
		</fieldset>	
		
		<?php echo JHtml::_('tabs.panel',JText::_('LIGHTBOX_THUMBNAILS'), 'igallery_profiles_mainimage'); ?>
		<fieldset class="adminform">
		<ul class="adminformlist">
		<li><?php echo $this->form->getLabel('lbox_show_thumbs'); ?>
			<?php echo $this->form->getInput('lbox_show_thumbs'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_thumb_width'); ?>
			<?php echo $this->form->getInput('lbox_thumb_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_thumb_height'); ?>
			<?php echo $this->form->getInput('lbox_thumb_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_crop_thumbs'); ?>
			<?php echo $this->form->getInput('lbox_crop_thumbs'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_thumb_position'); ?>
			<?php echo $this->form->getInput('lbox_thumb_position'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_thumb_container_width'); ?>
			<?php echo $this->form->getInput('lbox_thumb_container_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_thumb_container_height'); ?>
			<?php echo $this->form->getInput('lbox_thumb_container_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_images_per_row'); ?>
			<?php echo $this->form->getInput('lbox_images_per_row'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_thumb_scrollbar'); ?>
			<?php echo $this->form->getInput('lbox_thumb_scrollbar'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_arrows_up_down'); ?>
			<?php echo $this->form->getInput('lbox_arrows_up_down'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_arrows_left_right'); ?>
			<?php echo $this->form->getInput('lbox_arrows_left_right'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_show_thumb_info'); ?>
			<?php echo $this->form->getInput('lbox_show_thumb_info'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_scroll_speed'); ?>
			<?php echo $this->form->getInput('lbox_scroll_speed'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_scroll_boundary'); ?>
			<?php echo $this->form->getInput('lbox_scroll_boundary'); ?></li>
		</ul>
		</fieldset>
		
		<?php echo JHtml::_('tabs.panel',JText::_('LIGHTBOX_OTHER'), 'igallery_profiles_mainimage'); ?>
		<fieldset class="adminform">
		<ul class="adminformlist">
			
			<li><?php echo $this->form->getLabel('lbox_allow_comments'); ?>
			<?php echo $this->form->getInput('lbox_allow_comments'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_allow_rating'); ?>
			<?php echo $this->form->getInput('lbox_allow_rating'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_close_position'); ?>
			<?php echo $this->form->getInput('lbox_close_position'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_scalable'); ?>
			<?php echo $this->form->getInput('lbox_scalable'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_download_image'); ?>
			<?php echo $this->form->getInput('lbox_download_image'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_show_tags'); ?>
			<?php echo $this->form->getInput('lbox_show_tags'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_report_image'); ?>
			<?php echo $this->form->getInput('lbox_report_image'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_share_facebook'); ?>
			<?php echo $this->form->getInput('lbox_share_facebook'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_plus_one'); ?>
			<?php echo $this->form->getInput('lbox_plus_one'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_show_numbering'); ?>
			<?php echo $this->form->getInput('lbox_show_numbering'); ?></li>
			
		</ul>
		</fieldset>
		
		<fieldset class="adminform">
		<legend><?php echo JText::_('SLIDESHOW'); ?></legend>
		<ul class="adminformlist">
			<li><?php echo $this->form->getLabel('lbox_show_slideshow_controls'); ?>
			<?php echo $this->form->getInput('lbox_show_slideshow_controls'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_slideshow_position'); ?>
			<?php echo $this->form->getInput('lbox_slideshow_position'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_slideshow_autostart'); ?>
			<?php echo $this->form->getInput('lbox_slideshow_autostart'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_slideshow_pause'); ?>
			<?php echo $this->form->getInput('lbox_slideshow_pause'); ?></li>
			</ul>
		</fieldset>
		
		<fieldset class="adminform">
		<legend><?php echo JText::_('IMAGE_DESCRIPTIONS'); ?></legend>
		<ul class="adminformlist">	
			<li><?php echo $this->form->getLabel('lbox_show_descriptions'); ?>
			<?php echo $this->form->getInput('lbox_show_descriptions'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_photo_des_position'); ?>
			<?php echo $this->form->getInput('lbox_photo_des_position'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_photo_des_width'); ?>
			<?php echo $this->form->getInput('lbox_photo_des_width'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_photo_des_height'); ?>
			<?php echo $this->form->getInput('lbox_photo_des_height'); ?></li>
			
			<li><?php echo $this->form->getLabel('lbox_show_filename'); ?>
			<?php echo $this->form->getInput('lbox_show_filename'); ?></li>
		</ul>
		</fieldset>
		
		<?php echo JHtml::_('tabs.panel',JText::_('JCONFIG_PERMISSIONS_LABEL'), 'igallery_profiles_acl'); ?>
		<fieldset class="adminform">
		<ul class="adminformlist">
		
		<?php echo JHtml::_('sliders.start','permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
	
		<?php echo JHtml::_('sliders.panel',JText::_('PERMISSIONS'), 'access-rules'); ?>	
			<fieldset class="panelform">
				<?php echo $this->form->getLabel('rules'); ?>
				<?php echo $this->form->getInput('rules'); ?>
			</fieldset>
		
		<?php echo JHtml::_('sliders.end'); ?>
	
	
	<?php echo JHtml::_('tabs.end'); ?>
		
	</div>
	<div class="clr"></div>

	<?php echo $this->form->getInput('id'); ?>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>