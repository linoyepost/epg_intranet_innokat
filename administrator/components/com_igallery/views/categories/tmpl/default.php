<?php defined('_JEXEC') or die;
JHTML::_('behavior.framework', true);
if($this->isSite == true)
{
	echo JToolBar::getInstance('toolbar')->render('toolbar');
}

?>

<script type="text/javascript">
var iToolbarIds = ['toolbar-new','toolbar-publish','toolbar-unpublish'];
iToolbarIds.each(function(id, index)
{
	if( $chk( document.id(id) ) )
	{
		document.id(id).getElement('a').set('href', 'javascript: void(0)');
	}	
});	
</script>

<div style="clear: both"></div>
<?php if ($this->isSite == true) : ?>
<div class="igallery_form">
<?php endif; ?>
<form action="index.php?option=com_igallery&view=categories<?php if($this->isSite == true){ echo '&Itemid='. JRequest::getInt('Itemid', 0); } ?>" method="post" name="adminForm" id="adminForm">

<fieldset id="filter-bar">
	<div class="filter-search fltlft">
		<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
		<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->state->get('filter.search'); ?>" />
		<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
		<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
	</div>
	<div class="filter-select fltrt">
		<select name="filter_published" class="inputbox" onchange="this.form.submit()">
			<option value=""><?php echo JText::_('JOPTION_SELECT_PUBLISHED');?></option>
			<?php echo JHtml::_('select.options', JHtml::_('jgrid.publishedOptions',array('archived'=>0,'trash'=>0,'all'=>0)), 'value', 'text', $this->state->get('filter.published'), true);?>
		</select>
	</div>
</fieldset>
<div class="clr"> </div>

<table class="adminlist">
	<thead>
		<tr>
			
			<th width="20">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
			</th>
			
			<th>
				<?php echo JText::_('JGLOBAL_TITLE'); ?>
			</th>
			
			<th>
				<?php echo JText::_( 'MANAGE_IMAGES' ); ?>
			</th>
			
			<th>
				<?php echo JText::_('JAUTHOR' ); ?>
			</th>
			<?php if ($this->isSite == false) : ?>
			<th>
				<?php echo JText::_('PROFILE'); ?>
			</th>
			<?php endif; ?>
			<th width="5%">
				<?php echo JText::_('JPUBLISHED'); ?>
			</th>
			
			<?php if( !empty($this->moderate) ): ?>
			<th width="5%">
				<?php echo JText::_( 'APPROVED' ); ?>
			</th>
			<?php endif; ?>
			<?php if ($this->isSite == false) : ?>
			<th style="width:120px;">
				<?php echo JText::_('JGRID_HEADING_ORDERING'); ?>
				<?php echo JHtml::_('grid.order',  $this->items, 'filesave.png', 'categories.saveorder'); ?>
			</th>
			<?php endif; ?>
			<?php if($this->isSite == false): ?>
			<th width="1%" class="nowrap">
					<?php echo JText::_('JGRID_HEADING_ID'); ?>
			</th>
			<?php endif; ?>
		</tr>
	</thead>

<?php $footerColumns = empty($this->moderate) ? 8 : 9; ?>		
	<tfoot>
		<tr>
			<td colspan="<?php echo $footerColumns; ?>">
			<?php //if($this->isSite == false): ?>
				<?php echo $this->pagination->getListFooter(); ?>
			<?php //endif; ?>
			</td>
		</tr>
	</tfoot>
	
	<tbody>	
    <?php
	$db	=& JFactory::getDBO();
	
	
	foreach($this->items as $i => $item):
		
		$query = 'SELECT id FROM #__igallery_img where gallery_id = '.(int)$item->id;
		$db->setQuery($query);
		$db->query($query);
		$numPhotos = $db->getNumRows();
		
		$editOk = igGeneralHelper::authorise('core.edit', $item->id);
		$editStateOk = igGeneralHelper::authorise('core.edit.state', $item->id);
		
		if ($item->id != '1') :
		
		?>
		<tr class="row<?php echo $i % 2; ?>">
			
			<td class="center">
				<?php if ($item->id != '1') : ?>
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
                <?php endif; ?>
			</td>
			
			<td>
				<?php if($editOk && $item->id != '1'): ?>
				<a href="<?php echo JRoute::_('index.php?option=com_igallery&view=icategory&id='.$item->id, false); ?>">
				<?php endif; ?>
				
					<?php $item->name = strlen($item->name) < 1 ? '____' : $item->name ?>
                    <?php $item->treename = str_replace('|_', ' ', $item->treename); ?>
                    <?php $item->treename = str_replace('.', ' ', $item->treename); ?>
					<?php echo $this->escape($item->name); ?>
				
				<?php if($editOk && $item->id != '1'): ?>
				</a>
				<?php endif; ?>
			</td>
			
			<td class="center">
			<?php if($editOk && $item->id != '1'): ?>
				<a href="<?php echo JRoute::_('index.php?option=com_igallery&view=images&catid='.$item->id, false); ?>">
			<?php endif; ?>
            	<?php if ($item->id != '1') : ?>
					<?php echo JText::_( 'MANAGE_IMAGES' ); ?> (<?php echo $numPhotos; ?>)
                <?php endif; ?>
			<?php if($editOk && $item->id != '1'): ?>
				</a>
			<?php endif; ?>
			</td>
			
			<td class="center">
				<?php echo empty($item->name_of_user) ? '' : $item->name_of_user; ?>
			</td>
			<?php if ($this->isSite == false) : ?>
			<td class="center">
			<?php if($this->canConfigure && $this->isSite == false): ?>
				<a href="index.php?option=com_igallery&view=profile&id=<?php echo $item->profile; ?>">
			<?php endif; ?>
			
			<?php echo $item->profile_name; ?>
			
			<?php if($this->canConfigure && $this->isSite == false): ?>
				</a>
			<?php endif; ?>
			
			</td>
			<?php endif; ?>
            <?php if($this->isSite == false): ?>
			<td class="center">
				<?php if ($item->id != '1') : ?>
				<?php echo JHtml::_('jgrid.published', $item->published, $i, 'categories.', $editStateOk, 'cb', $item->publish_up, $item->publish_down);?>
                <?php endif; ?>
			</td>
			<?php endif; ?>
            <?php if($this->isSite == true): ?>
			<td class="center">
                <?php
                	$glryStatus = $item->published == 1 ? 'publish' : 'unpublish';
				?>
                <a class="jgrid" href="javascript: void(0)">
                	<span class="state <?php print $glryStatus; ?>"></span>
                </a>
            </td>
			<?php endif; ?>
			<?php if( !empty($this->moderate) ): ?>
			<td align="center">
				<?php echo igHtmlHelper::moderateImage($item, $i, 'categories', $this->isSite); ?>
			</td>	
			<?php endif; ?>
			<?php if ($this->isSite == false) : ?>
			<td class="order" style="width:120px;">
			<?php
				$showOrderUp = false;
				$showOrderDown = false;
				foreach($this->items as $key =>$value)
				{
					if ($item->parent == $value->parent && $item->ordering > $value->ordering)
					{
						$showOrderUp = true;
					}
					
					if ($item->parent == $value->parent && $item->ordering < $value->ordering)
					{
						$showOrderDown = true;
					}
				}
    		?>
			
				<span><?php echo $this->pagination->orderUpIcon($i, $showOrderUp, 'categories.orderup', 'JLIB_HTML_MOVE_UP', $editStateOk);?></span>
				<span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, $showOrderDown, 'categories.orderdown', 'JLIB_HTML_MOVE_DOWN', $editStateOk ); ?></span>
				<input type="text" name="order[]" size="5" value="<?php echo $item->ordering;?>" class="text-area-order" />

			</td>
			<?php endif; ?>
			<?php if($this->isSite == false): ?>
			<td class="center">
				<?php echo $item->id; ?>
			</td>
			<?php endif; ?>
			
		</tr>
		<?php endif; endforeach; ?>
</tbody>
</table>

<?php if ($this->isSite == true) : ?>
</div>
<?php endif; ?>
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<?php echo JHtml::_('form.token'); ?>
</form>

