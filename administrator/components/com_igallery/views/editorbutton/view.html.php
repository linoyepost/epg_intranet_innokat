<?php
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class igalleryViewEditorbutton extends JView
{
	function display($tpl = null)
	{
		$this->state	= $this->get('State');
		$this->item		= $this->get('Item');
		$this->form		= $this->get('Form');
		
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		$name = JRequest::getCmd('e_name');
		$rand = rand(1,9999);

	    $js = '
	    function insertToken()
	    {
	    	var type = $(\'jform_ig_type\').get(\'value\');
    		var catid = $(\'jformig_category\').value;
    		var children = $(\'jform_ig_include_child\').value;
    		var showmenu = $(\'jform_ig_show_menu_images\').value;
    		var profile = $(\'jformig_profile\').value;
    		var tags = $(\'jform_ig_tags\').value;
    		var limit = $(\'jform_ig_max_images\').value;
    		
    		var token = \'{igallery id=\' + '.$rand.' + \'|cid=\' + catid + \'|pid=\' + profile + \'|type=\' + type + \'|children=\' + children + \'|showmenu=\' + showmenu + \'|tags=\' + tags + \'|limit=\' + limit + \'}\';
    		window.parent.jInsertEditorText(token, \''.$name.'\');
    		return false;
		}';

	    JHTML::_('behavior.mootools');
		
	    $document  =& JFactory::getDocument();
	    $document->addScriptDeclaration($js);

		parent::display($tpl);
    }
}