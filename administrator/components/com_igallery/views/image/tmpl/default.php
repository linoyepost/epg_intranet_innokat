<?php defined('_JEXEC') or die();
JHTML::_('behavior.framework', true);
if($this->isSite == true)
{
	echo JToolBar::getInstance('toolbar')->render('toolbar');
}
?>

<script type="text/javascript">
var iToolbarIds = ['toolbar-apply','toolbar-save','toolbar-forward','toolbar-cancel'];
iToolbarIds.each(function(id, index)
{
	if( $chk( document.id(id) ) )
	{
		document.id(id).getElement('a').set('href', 'javascript: void(0)');
	}	
});	
</script>

<form action="<?php JRoute::_('index.php?option=com_igallery'); ?>" method="post" name="adminForm"  id="adminForm">
	<?php if($this->isSite == false) : ?>
    <div class="width-60 fltlft">
		<fieldset class="adminform">
    <?php else : ?>
    <div class="igallery_form">
    <?php endif; ?>
			
			<ul class="adminformlist">
			
			<li><?php echo $this->form->getLabel('name'); ?>
			<?php echo $this->form->getInput('name'); ?></li>
			
			<li><?php echo $this->form->getLabel('name_ar'); ?>
			<?php echo $this->form->getInput('name_ar'); ?></li>
			<li>
            	<label>&nbsp;</label>
			<!--<img src="<?php echo IG_IMAGE_HTML_RESIZE; ?><?php echo $this->thumbFile['folderName']; ?>/<?php echo $this->thumbFile['fullFileName']; ?>"
			width="<?php echo $this->thumbFile['width']; ?>" height="<?php echo $this->thumbFile['height'] ?>" alt="<?php echo $this->item->alt_text; ?>"/>-->
            <?php
            $img = 'file:'.IG_IMAGE_HTML_RESIZE.$this->thumbFile['folderName'].'\\'.$this->thumbFile['fullFileName'];
            $img = str_replace("\\", "/", $img);
			?>
            <!--<img src="<?php echo JURI::root().'index.php?option=com_eposts&view=loadimage&myfile='.IG_IMAGE_HTML_RESIZE.$this->thumbFile['folderName'].'\\'.$this->thumbFile['fullFileName']; ?>"
			width="<?php echo $this->thumbFile['width']; ?>" height="<?php echo $this->thumbFile['height'] ?>" alt="<?php echo $item->alt_text; ?>"/>-->
            <img src="<?php echo $img; ?>"
			width="<?php echo $this->thumbFile['width']; ?>" height="<?php echo $this->thumbFile['height'] ?>" alt="<?php echo $item->alt_text; ?>"/>
			</li>
			
			<li><?php echo $this->form->getLabel('gallery_id'); ?>
			<?php echo $this->form->getInput('gallery_id'); ?></li>
			
			<?php if($this->isSite == false): ?>
			<li><?php echo $this->form->getLabel('user'); ?>
			<?php echo $this->form->getInput('user'); ?>
			</li>
			<?php endif; ?>
			
			<li><?php echo $this->form->getLabel('department'); ?>
			<?php echo $this->form->getInput('department'); ?></li>
			
			<li><?php echo $this->form->getLabel('public'); ?>
			<?php echo $this->form->getInput('public'); ?></li>
			
			<li><?php echo $this->form->getLabel('published'); ?>
			<?php echo $this->form->getInput('published'); ?></li>
			<?php //if($this->isSite == false): ?>
			<?php if($this->isSite == false && false): ?>
			<li><?php echo $this->form->getLabel('publish_up'); ?>
				<?php echo $this->form->getInput('publish_up'); ?></li>

			<li><?php echo $this->form->getLabel('publish_down'); ?>
			<?php echo $this->form->getInput('publish_down'); ?></li>
			<?php endif; ?>
				
			<li><?php echo $this->form->getLabel('date'); ?>
			<?php echo $this->form->getInput('date'); ?></li>
            
			<li><?php echo $this->form->getLabel('user'); ?>
			<?php echo $this->form->getInput('user'); ?></li>
			
			</ul>
			<?php echo $this->form->getInput('id'); ?>
    <?php if($this->isSite == false) : ?>
		</fieldset>
    <?php endif; ?>
	</div>
	<div class="clr"></div>

	<input type="hidden" name="task" value="" />
	<?php if($this->isSite == true): ?>
	<input type="hidden" name="Itemid" value="<?php echo JRequest::getInt('Itemid', 0); ?>" />
	<?php endif; ?>
	<input type="hidden" name="catid" value="<?php echo $this->category->id; ?>" />
	<?php echo JHtml::_('form.token'); ?>
</form>