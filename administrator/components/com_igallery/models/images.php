<?php
defined( '_JEXEC' ) or die();

jimport('joomla.application.component.modellist');

class igalleryModelImages extends JModelList
{
	function getListQuery($resolveFKs = true)
	{
		$query = $this->_db->getQuery(true);

		$query->select('i.*');
		$query->from('#__igallery_img AS i');

		$query->select('c.name');
		$query->join('INNER', '`#__igallery` AS c ON c.id = i.gallery_id');
		
		$query->select('p.max_width, p.max_height, p.thumb_width, '.
		'p.thumb_height, p.crop_thumbs, p.img_quality, '.
		'p.watermark, p.watermark_filename, p.watermark_position, '.
		'p.watermark_transparency, p.crop_main, p.crop_lbox, p.round_thumb,
		p.round_fill, p.round_large, p.watermark_text, p.watermark_text_color, p.watermark_text_size');
		$query->join('INNER', '`#__igallery_profiles` AS p ON p.id = c.profile');
		
		$query->select('v.title as access_group_name');
		$query->join('INNER', '`#__viewlevels` AS v ON v.id = i.access');

		$published = $this->getState('filter.published');
		if (is_numeric($published))
		{
			$query->where('i.published = ' . (int)$published);
		}
		else if ($published === '')
		{
			$query->where('(i.published IN (0, 1))');
		}

		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('i.id = '.(int)substr($search, 3));
			}
			else
			{
				$search = $this->_db->Quote('%'.$this->_db->getEscaped($search, true).'%');
				$query->where('(i.filename LIKE '.$search.' OR i.description LIKE '.$search.' OR i.tags LIKE '.$search.' OR i.alt_text LIKE '.$search.')');
			}
		}
		
		$catid = $this->getState('filter.catid');
		if (!empty($catid))
		{
			$query->where('i.gallery_id = ' . (int)$catid);
		}

		$query->order($this->_db->getEscaped($this->getState('list.ordering', 'i.ordering')).' '.$this->_db->getEscaped($this->getState('list.direction', 'ASC')));

		return $query;
	}
    
	protected function populateState($ordering = null, $direction = null)
	{
		$app		= JFactory::getApplication();

		$search = $app->getUserStateFromRequest($this->context.'.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context.'.published', 'filter_published', '');
		$this->setState('filter.published', $published);
		
		$catid = $app->getUserStateFromRequest($this->context.'.catid', 'catid', '');
		$this->setState('filter.catid', (int)$catid);
		
			parent::populateState('i.ordering', 'asc');
	}
	
	function getCategory($id)
	{
		$query = 'SELECT * FROM #__igallery WHERE id = '.(int)$id;
		$this->_db->setQuery($query);
		$category = $this->_db->loadObject();
		
		if($category == null)
		{
			$category = new stdClass();
			$category->id = 0;
			$category->name = '';
		}
		
	    return $category;
    }
    
	function getProfile($id)
	{
		$query = 'SELECT * FROM #__igallery_profiles WHERE id = '.(int)$id;
		$this->_db->setQuery($query);
		$profile = $this->_db->loadObject();
	    return $profile;
    }
}