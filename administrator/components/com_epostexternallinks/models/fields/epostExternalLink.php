<?php
// No direct access to this file
defined('_JEXEC') or die;

// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('lists');

/**
 * HelloWorld Form Field class for the HelloWorld component
 */
class JFormFieldepostExternalLink extends JFormFieldList
{
	/**
	 * The field type.
	 *
	 * @var		string
	 */
	protected $type = 'epostExternalLink';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return	array		An array of JHtml options.
	 */
	protected function getOptions() 
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('id,Title,RedirectLink');
		$query->from('jos_epostexternallinks');
		$db->setQuery((string)$query);
		$messages = $db->loadObjectList();
		$options = array();
		if ($messages)
		{
			foreach($messages as $message) 
			{
				$options[] = JHtml::_('select.option', $message->id, $message->Title, $message->RedirectLink);
			}
		}

		$options = array_merge(parent::getOptions(), $options);
		return $options;
	}
}
