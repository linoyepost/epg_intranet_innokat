<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_redirect
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Link Table for Redirect.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_redirect
 * @version		1.6
 */
class epostexternallinksTablelist extends JTable
{
	/**
	 * Constructor
	 *
	 * @param	object	Database object
	 *
	 * @return	void
	 * @since	1.6
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__epostexternallinks', 'id', $db);
	}

	/**
	 * Overloaded check function
	 *
	 * @return boolean
	 * @since	1.6
	 */
	public function check()
	{

		$user =& JFactory::getUser();
		
		$this->created_by = $user->get( 'id' );
		$this->modified_by = $user->get( 'id' );

		$this->Title = trim($this->Title);
		$this->RedirectLink = trim($this->RedirectLink);

		$this->UserName = trim($this->UserName);
		$this->Email = trim($this->Email);
		$this->Phone = trim($this->Phone);
		$this->userrole = trim($this->userrole);

		$this->NewTab = trim($this->NewTab);



		$this->FormMethod = trim($this->FormMethod);


		// Check for valid name.
		if (empty($this->RedirectLink)) {
			$this->setError(JText::_('Must have a redirect url'));
			return false;
		}

		// Check for valid name.
		if (empty($this->Title)) {
			$this->setError(JText::_('Must have a title'));
			return false;
		}

		// Check for duplicates
		/*if ($this->old_url == $this->new_url) {
			$this->setError(JText::_('COM_REDIRECT_ERROR_DUPLICATE_URLS'));
			return false;
		}

		$db = $this->getDbo();

		// Check for existing name
		$query = 'SELECT id FROM epostexternallinks WHERE old_url ='.$db->Quote($this->old_url);
		$db->setQuery($query);

		$xid = intval($db->loadResult());

		if ($xid && $xid != intval($this->id)) {
			$this->setError(JText::_('COM_REDIRECT_ERROR_DUPLICATE_OLD_URL'));
			return false;
		}*/

		return true;
	}

	/**
	 * Overriden store method to set dates.
	 *
	 * @param	boolean	True to update fields even if they are null.
	 *
	 * @return	boolean	True on success.
	 * @see		JTable::store
	 * @since	1.6
	 */
	public function store($updateNulls = false)
	{

		// Initialise variables.
		$date = JFactory::getDate()->toSql();

		if ($this->id) {
			// Existing item
			$this->modified_date = $date;
			$this->created_by = $date;
		} else {
			// New record.
			$this->created_date = $date;
			$this->modified_by = $date;
		}

		return parent::store($updateNulls);
	}
}