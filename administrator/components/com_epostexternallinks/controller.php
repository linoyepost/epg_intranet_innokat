<?php
/**
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Redirect master display controller.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_redirect
 * @since		1.6
 */

class epostExternalLinksController extends JController
{ 
// import Joomla controller library jimport('joo{
	/**
	 * display task
	 *
	 * @return void
	 */


	protected $default_view = 'lists';


	function display($cachable = false, $urlparams = false) 
	{

		require_once JPATH_COMPONENT.'/helper/redirect1.php';

		// set default view if not set
		//$input = JFactory::getApplication()->input;
		//$input->set('view', $input->getCmd('view', 'list'));

		$view		= JRequest::getCmd('view', 'links');
		$layout 	= JRequest::getCmd('layout', 'default');
		$id			= JRequest::getInt('id');
 

		// call parent behavior
		parent::display();
	}
}