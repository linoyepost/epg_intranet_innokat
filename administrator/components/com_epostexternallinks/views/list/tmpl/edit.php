<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_redirect
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

// Include the HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{

		if (task == 'list.cancel' || document.formvalidator.isValid(document.id('list-form'))) {
			Joomla.submitform(task, document.getElementById('list-form'));
		}
	}

	function setval(val){
		if(document.getElementById("jform_"+val).checked){
			document.getElementById("hdn_"+val).value = 1;
		}else {
			document.getElementById("hdn_"+val).value = 0;
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_epostexternallinks&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="list-form" class="form-validate">
	
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend><?php echo empty($this->item->id) ? "New" : "Edit" //JText::sprintf('', $this->item->id); ?></legend>
			<ul class="adminformlist">
			<li><?php echo $this->form->getLabel('Title'); ?>
			<?php echo $this->form->getInput('Title'); ?></li>

			<li><?php echo $this->form->getLabel('RedirectLink'); ?>
			<?php echo $this->form->getInput('RedirectLink'); ?></li>

			<li><?php echo $this->form->getLabel('UserName'); ?>
			<?php echo $this->form->getInput('UserName'); ?></li>

			<li><?php echo $this->form->getLabel('Email'); ?>
			<?php echo $this->form->getInput('Email'); ?></li>
			
			<li><?php echo $this->form->getLabel('Phone'); ?>
			<?php echo $this->form->getInput('Phone'); ?></li>

			<li><?php echo $this->form->getLabel('userrole'); ?>
			<?php echo $this->form->getInput('userrole'); ?></li>
			
			<li><?php echo $this->form->getLabel('id'); ?>
			<?php echo $this->form->getInput('id'); ?></li>
			</ul>
		
		</fieldset>
	</div>

	<div class="width-40 fltrt">
		<fieldset class="adminform">
			<legend><?php echo "Option"; ?></legend>
			<ul class="adminformlist">
				<li><?php echo $this->form->getLabel('NewTab'); ?>
				<?php echo $this->form->getInput('NewTab'); ?></li>

				<li><?php echo $this->form->getLabel('FormMethod'); ?>
				<?php echo $this->form->getInput('FormMethod'); ?></li>
			</ul>

		</fieldset>
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
	<div class="clr"></div>
	<input type="hidden" id="hdn_newtab" name="jform[NewTab]" value="<?php if($task = 'edit' and $this->item->NewTab == '1'){echo '1';} else {echo '0';}?>">
	<input type="hidden" id="hdn_username" name="jform[UserName]" value="<?php if($task = 'edit' and $this->item->UserName == '1'){echo '1';} else {echo '0';}?>">
	<input type="hidden" id="hdn_email" name="jform[Email]" value="<?php if($task = 'edit' and $this->item->Email == '1'){echo '1';} else {echo '0';}?>">
	<input type="hidden" id="hdn_phone" name="jform[Phone]" value="<?php if($task = 'edit' and $this->item->Phone == '1'){echo '1';} else {echo '0';}?>">
	<input type="hidden" id="hdn_userrole" name="jform[userrole]" value="<?php if($task = 'edit' and $this->item->userrole == '1'){echo '1';} else {echo '0';}?>">
</form>