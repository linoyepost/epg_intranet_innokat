<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

$user		= JFactory::getUser();
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
?>

<?php 

	function numhash($n) {

    	return (((0x0000FFFF & $n) << 16) + ((0xFFFF0000 & $n) >> 16));
	}

	foreach($this->items as $i => $item): 

			
			$secureid = numhash($item->id);

			$canCreate	= $user->authorise('core.create', 'com_epostexternallinks');
			$canEdit	= $user->authorise('core.edit', 'com_epostexternallinks');
			$canChange	= $user->authorise('core.edit.state', 'com_epostexternallinks');
	?>

	<tr class="row<?php echo $i % 2; ?>">
		
		<td class="center" width="">
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
		</td>
		<td width="">
			<?php echo $item->id; ?>
		</td>

		<td width=""> 

			<?php if ($canEdit) : ?>
				<a href="<?php echo JRoute::_('index.php?option=com_epostexternallinks&view=list&layout=edit&id='.$item->id);?>" title="<?php echo $this->escape($item->Title); ?>">
							<?php echo $this->escape(str_replace(JURI::root(), '', $item->Title)); ?></a>
					<?php else : ?>
							<?php echo $this->escape(str_replace(JURI::root(), '', $item->Title)); ?>
					<?php endif; ?>
		</td>

		<td width="">
			<?php echo $item->RedirectLink; ?>
		</td>
		<td width="">
		<input type="text" class="epgurl_input" id="<?php echo "epg_url_".$item->id; ?>" value="<?php echo 'epgintranet/index.php?option=com_epostexternallinks&view=list&type=d&Itemid='.$secureid;?>" readonly>
			
			<!--<label id="<?php //echo "epg_url_".$item->id; ?>">
				<?php //echo 'epgintranet/index.php?option=com_epostexternallinks&view=list&Itemid='.$item->id; ?>
			</label>-->

			<label class="fltrt copy_btn" onclick="copyurl(<?php echo $item->id;?>)">
				Copy Url
			</label>
		</td>
	</tr>
<?php endforeach; ?>


