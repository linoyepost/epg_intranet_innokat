<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

// load tooltip behavior
JHtml::_('behavior.tooltip');

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$user		= JFactory::getUser();
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));

?>
<style type="text/css">

 .copy_btn {

 	 padding: 5px;
    background: #146295;
    display: block;
    color: #fff;
 }

 .epgurl_input {
 	border: none;;
 	width: 100%;
 	padding: 4px;
 }
	
</style>
<script type="text/javascript">
	function copyurl(id){
		var epg_url = document.getElementById("epg_url_"+id);
		epg_url.select();
		 document.execCommand("Copy");
	}

</script>

<form action="<?php echo JRoute::_('index.php?option=com_epostExternalLinks'); ?>" method="post" name="adminForm">
	
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
			<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo "Search" ?>" />
			<button type="submit"><?php echo "Submit" ?></button>
			<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo "Clear"; ?></button>
		</div>
	</fieldset>
	<div class="clr"> </div>


	<table class="adminlist">	
		<thead><?php echo $this->loadTemplate('head');?></thead>
		<tfoot><?php echo $this->loadTemplate('foot');?></tfoot>
		<tbody><?php echo $this->loadTemplate('body');?></tbody>
	</table>
	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
