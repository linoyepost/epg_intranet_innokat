<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of redirection links.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_redirect
 * @since		1.6
 */
class epostexternallinksViewlists extends JViewLegacy
{
	protected $enabled;
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @since	1.6
	 */
	public function display($tpl = null)
	{
		$this->enabled		= Redirect1Helper::isEnabled();
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');


		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		parent::display($tpl);
		$this->addToolbar();
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		$state	= $this->get('State');
		$canDo	= Redirect1Helper::getActions();


		JToolBarHelper::title(JText::_('Manage Site External Links'), 'menumgr.png');
		
		if ($canDo->get('core.create')) {
			JToolBarHelper::addNew('list.add');
		}
		
		if ($canDo->get('core.edit')) {
			JToolBarHelper::editList('list.edit');
		}
		
		//JToolBarHelper::deleteList('lists.delete');
		JToolBarHelper::deleteList('', 'lists.delete');
				
	}
}
