<?php
/**
* @version 1.5
* @package JDownloads
* @copyright (C) 2009 www.eposts.com
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* 
*
*/
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT.DS.'toolbar.eposts.html.php' );

switch ( $task ) {
	////////////////////////////////DEVELOPMENT PHASE III /////////////////////
/*Announcements*/	
    case 'view.announcements':
    menujlist::LIST_ANNOUNCEMENTS();
    break;

    case 'edit.announcements':
     menujlist::EDIT_ANNOUNCEMENTS();
    break;
	
/*News*/	
    case 'view.news':
    menujlist::LIST_NEWS();
    break;

    case 'edit.news':
    menujlist::EDIT_NEWS();
    break;
	
/*Our Vision*/	
    case 'view.visions':
    menujlist::LIST_VISIONS();
    break;

    case 'edit.visions':
    menujlist::EDIT_VISIONS();
    break;
	
/*Alerts*/	
    case 'view.alerts':
    menujlist::LIST_ALERTS();
    break;

    case 'edit.alerts':
    menujlist::EDIT_ALERTS();
    break;
	
/*Policies & Procedures*/	
    case 'view.policies':
    menujlist::LIST_POLICIES();
    break;

    case 'edit.policies':
    menujlist::EDIT_POLICIES();
    break;
	
/*Quotes*/	
    case 'view.quotes':
    menujlist::LIST_QUOTES();
    break;

    case 'edit.quotes':
    menujlist::EDIT_QUOTES();
    break;	
	
/*Manuals*/	
    case 'view.manuals':
    menujlist::LIST_MANUALS();
    break;

    case 'edit.manuals':
    menujlist::EDIT_MANUALS();
    break;
	
/*FAQS*/	
    case 'view.faqs':
    menujlist::LIST_FAQS();
    break;

    case 'edit.faqs':
    menujlist::EDIT_FAQS();
    break;		
	
/*Benchmarks*/	
    case 'view.benchmarks':
    menujlist::LIST_BENCHMARKS();
    break;

    case 'edit.benchmarks':
    menujlist::EDIT_BENCHMARKS();
    break;
/*Links*/	
    case 'view.links':
    menujlist::LIST_LINKS();
    break;

    case 'edit.links':
    menujlist::EDIT_LINKS();
    break;	
	/////////////////////////////////END/////////////////////////////
		
	    case "view.dms":
        menujlist::LIST_DMS();
        break;
        
        case "edit.dms":
        menujlist::EDIT_DMS();
        break;
		
	    case "view.bookingslots":
        menujlist::LIST_BOOKINSLOTS();
        break;
        
        case "edit.bookingslots":
        menujlist::EDIT_BOOKINSLOTS();
        break;
		
		case "view.resources":
        menujlist::LIST_RESOURCES();
        break;

        case "view.resourcesrights":
        menujlist::LIST_RESOURCESRIGHTS();
        break;
        
        case "edit.resources":
        menujlist::EDIT_RESOURCES();
        break;
		
		case "view.offices":
        menujlist::LIST_OFFICES();
        break;
        
        case "edit.offices":
        menujlist::EDIT_OFFICES();
        break;
		
	    case "view.events":
        menujlist::LIST_EVENTS();
        break;
        
        case "edit.events":
        menujlist::EDIT_EVENTS();
        break;
		
	    case "view.newhires":
        menujlist::LIST_NEWHIRES();
        break;
        
        case "edit.newhires":
        menujlist::EDIT_NEWHIRES();
        break;
		
	    case "view.tasawaqs":
        menujlist::LIST_TASAWAQS();
        break;
        
        case "edit.tasawaqs":
        menujlist::EDIT_TASAWAQS();
        break;
		
	    case "view.circulars":
        menujlist::LIST_CIRCULARS();
        break;
        
        case "edit.circulars":
        menujlist::EDIT_CIRCULARS();
        break;
		
		case "view.dwnmedias":
        menujlist::LIST_DWNMEDIAS();
        break;
        
        case "edit.dwnmedias":
        menujlist::EDIT_DWNMEDIAS();
        break;
		
	    case "view.departments":
        menujlist::LIST_DEPARTMENTS();
        break;
        
        case "edit.departments":
        menujlist::EDIT_DEPARTMENTS();
        break;

		case "new":
		case "edit":
		case "editA":
		case "copy":
		menujlist::EDIT_MENU();
		break;
        
        case "license.list":
			menujlist::LICENSE_LIST();
		break;

		case "license.edit":
			menujlist::LICENSE_EDIT();
		break;

        case "templates.menu":
			menujlist::TEMPLATES_MENU();
		break;
		
        case "templates.list.cats":
			menujlist::TEMPLATES_LIST_CATS();
		break;

		case "templates.edit.cats":
			menujlist::TEMPLATES_EDIT_CATS();
		break;

        case "templates.list.files":
			menujlist::TEMPLATES_LIST_FILES();
		break;

		case "templates.edit.files":
			menujlist::TEMPLATES_EDIT_FILES();
		break;
        
        case "templates.list.details":
            menujlist::TEMPLATES_LIST_DETAILS();
        break;

        case "templates.edit.details":
            menujlist::TEMPLATES_EDIT_DETAILS();
        break;        

        case "templates.list.summary":
			menujlist::TEMPLATES_LIST_SUMMARY();
		break;

		case "templates.edit.summary":
			menujlist::TEMPLATES_EDIT_SUMMARY();
		break;
		
        case "css.edit":
    	menujlist::CSS_EDIT();
    	break;

        case "language.edit":
    	menujlist::LANG_EDIT();
    	break;

		case "config.show":
    	menujlist::SETTINGS_MENU();
    	break;

		case "restore":
    	menujlist::RESTORE_MENU();
    	break;

 		case "info":
    	menujlist::INFO_MENU();
    	break;
        
        case "view.logs":
        menujlist::LIST_LOGS();
        break;        

        case "files.upload":
        menujlist::FILES_UPLOAD();
        break;
        
        case "manage.files":
        menujlist::MANAGE_FILES();
        break;

        case "add.ip":
        menujlist::ADD_IP();
        break;
                
		default:
		menujlist::_DEFAULT();
		break;
        
	}
?>