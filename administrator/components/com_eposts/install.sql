	CREATE TABLE IF NOT EXISTS `#__eposts_config` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `setting_name` varchar(64) NOT NULL default '',
	  `setting_value` text NOT NULL,
	  PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;
     
	CREATE TABLE IF NOT EXISTS `#__eposts_cats` (
		`cat_id` int(11) NOT NULL AUTO_INCREMENT,
		`cat_dir` text NOT NULL,
		`parent_id` int(11) NOT NULL ,
		`cat_title` VARCHAR( 255 ) NOT NULL,
		`cat_alias` VARCHAR( 255 ) NOT NULL,
		`cat_description` TEXT NOT NULL,
	  	`cat_pic` VARCHAR( 255 ) NOT NULL,
	  	`cat_access` VARCHAR( 3 ) NOT NULL default '00',
		`cat_department_access` int(11) NOT NULL default '0',
        `metakey` TEXT NOT NULL default '',
		`metadesc` TEXT NOT NULL default '',
		`jaccess` tinyint(3) NOT NULL default '0',
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
	  	`ordering` int(11) NOT NULL default '0',
	  	`published` tinyint(1) NOT NULL default '0',
	  	`checked_out` int(11) NOT NULL default '0',
	  	`checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
       PRIMARY KEY  (`cat_id`)
    ) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;
    
	CREATE TABLE IF NOT EXISTS `#__eposts_files` (
		  `file_id` int(11) NOT NULL AUTO_INCREMENT,
		  `file_title` varchar(255) NOT NULL default '',
		  `file_alias` varchar(255) NOT NULL default '',
		  `description` longtext NOT NULL default '',
		  `description_long` longtext NOT NULL default '',
          `file_pic` varchar(255) NOT NULL default '',
    	  `thumbnail` varchar(255) NOT NULL default '',
    	  `thumbnail2` varchar(255) NOT NULL default '',
    	  `thumbnail3` varchar(255) NOT NULL default '',		  
          `price` varchar(20) NOT NULL default '',
          `release` varchar(255) NOT NULL default '',
    	  `language` tinyint(2) NOT NULL default '0',
    	  `system` tinyint(2) NOT NULL default '0',
		  `license` varchar(255) NOT NULL default '',
		  `url_license` varchar(255) NOT NULL default '',
		  `license_agree` tinyint(1) NOT NULL default '0',
		  `size` varchar(255) NOT NULL default '',
		  `date_added` datetime NOT NULL default '0000-00-00 00:00:00',
		  `file_date` datetime NOT NULL default '0000-00-00 00:00:00',
		  `publish_from` datetime NOT NULL default '0000-00-00 00:00:00',
		  `publish_to` datetime NOT NULL default '0000-00-00 00:00:00',
          `use_timeframe` tinyint(1) NOT NULL default '0',
		  `url_download` varchar(255) NOT NULL default '',
          `extern_file` varchar(255) NOT NULL default '',
          `extern_site` tinyint(1) NOT NULL default '0',
          `mirror_1` varchar(255) NOT NULL default '',
          `mirror_2` varchar(255) NOT NULL default '',
	      `extern_site_mirror_1` tinyint(1) NOT NULL default '0',
          `extern_site_mirror_2` tinyint(1) NOT NULL default '0',
          `url_home` varchar(255) NOT NULL default '',
		  `author` varchar(255) NOT NULL default '',
		  `url_author` varchar(255) NOT NULL default '',
		  `created_by` varchar(255) NOT NULL default '',
		  `created_id` int(11) NOT NULL default '0',
		  `created_mail` varchar(255) NOT NULL default '',
		  `modified_by` varchar(255) NOT NULL default '',
		  `modified_id` int(11) NOT NULL default '0',
		  `modified_date` datetime NOT NULL default '0000-00-00 00:00:00',
		  `submitted_by` int(11) NOT NULL default '0',
		  `set_aup_points` tinyint(1) NOT NULL default '0',
		  `downloads` int(11) NOT NULL default '0',
		  `cat_id` int(11) NOT NULL default '0',
          `metakey` TEXT NOT NULL default '',
          `metadesc` TEXT NOT NULL default '',
          `update_active` tinyint(1) NOT NULL default '0',
		  `custom_field_1` tinyint(2) NOT NULL default '0',
		  `custom_field_2` tinyint(2) NOT NULL default '0',
		  `custom_field_3` tinyint(2) NOT NULL default '0',
		  `custom_field_4` tinyint(2) NOT NULL default '0',
		  `custom_field_5` tinyint(2) NOT NULL default '0',
		  `custom_field_6` varchar(255) NOT NULL default '',
		  `custom_field_7` varchar(255) NOT NULL default '',
		  `custom_field_8` varchar(255) NOT NULL default '',
		  `custom_field_9` varchar(255) NOT NULL default '',
		  `custom_field_10` varchar(255) NOT NULL default '',
		  `custom_field_11` date NOT NULL default '0000-00-00',
		  `custom_field_12` date NOT NULL default '0000-00-00',
		  `custom_field_13` TEXT NOT NULL default '',
		  `custom_field_14` TEXT NOT NULL default '',
    	  `jaccess` tinyint(3) NOT NULL default '0',
		  `jlanguage` VARCHAR( 7 ) NOT NULL default '',
		  `ordering` int(11) NOT NULL default '0',
		  `published` tinyint(1) NOT NULL default '0',
		  `checked_out` int(11) NOT NULL default '0',
		  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
		  PRIMARY KEY  (`file_id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;
    
	CREATE TABLE IF NOT EXISTS `#__eposts_license` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `license_title` varchar(64) NOT NULL default '',
	  `license_text` longtext NOT NULL,
      `license_url` varchar(255) NOT NULL default '',
	  `jlanguage` VARCHAR( 7 ) NOT NULL default '',
      `checked_out` int(11) NOT NULL default '0',
	  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
	  PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;
    
	CREATE TABLE IF NOT EXISTS `#__eposts_templates` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `template_name` varchar(64) NOT NULL default '',
	  `template_typ` tinyint(2) NOT NULL default '0',
	  `template_header_text` longtext NOT NULL,
	  `template_subheader_text` longtext NOT NULL,
	  `template_footer_text` longtext NOT NULL,
      `template_text` longtext NOT NULL,
	  `template_active` tinyint(1) NOT NULL default '0',
	  `locked` tinyint(1) NOT NULL default '0',
      `note` tinytext NOT NULL,
	  `cols` tinyint(1) NOT NULL default '1',
      `checkbox_off` tinyint(1) NOT NULL default '0',
      `symbol_off` tinyint(1) NOT NULL default '0',
	  `jlanguage` VARCHAR( 7 ) NOT NULL default '',
      `checked_out` int(11) NOT NULL default '0',
	  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
	  PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;




#DROP TABLE IF EXISTS `#__eposts_bookingrequest_members`;
CREATE TABLE IF NOT EXISTS `#__eposts_bookingrequest_members` (
		`id` int(11) NOT NULL auto_increment,
		`bookingrequest_id` int(11) NOT NULL,
           `user_id` int(11) NOT NULL,
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;
	
#DROP TABLE IF EXISTS `#__eposts_bookingrequest_slots`;
CREATE TABLE IF NOT EXISTS `#__eposts_bookingrequest_slots` (
		`id` int(11) NOT NULL auto_increment,
		`bookingrequest_id` int(11) NOT NULL,
           `slots_id` int(11) NOT NULL,
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;	
	
#DROP TABLE IF EXISTS `#__eposts_bookingrequest`;
CREATE TABLE IF NOT EXISTS `#__eposts_bookingrequest` (
		`id` int(11) NOT NULL auto_increment,
		  `bookingdate` date NOT NULL default '0000-00-00',
          `booking_title` text NOT NULL,
	      `booking_description` longtext,
	      `booking_details_description` longtext,
		  `resourceid` int(11),
          `resources_offices` int(11),
	      `booking_status` tinyint(4) NOT NULL default '0',
          `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
		  `publishedby` int(11),
          `submittedto` int(11),
          `approvedon` datetime NOT NULL default '0000-00-00 00:00:00',
          `approvedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

############################## Development Phase III ##############################

#DROP TABLE IF EXISTS `#__eposts_links`;
CREATE TABLE IF NOT EXISTS `#__eposts_links` (
		`id` int(11) NOT NULL auto_increment,
		   `links_name` text NOT NULL,
           `links_name_ar` text NOT NULL,		   
		   `links_link` longtext,
           `links_link_ar` longtext,
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`
	
#DROP TABLE IF EXISTS `#__eposts_faqs`;
CREATE TABLE IF NOT EXISTS `#__eposts_faqs` (
		`id` int(11) NOT NULL auto_increment,
		   `faqs_name` text NOT NULL,
           `faqs_name_ar` text NOT NULL,		   
		   `faqs_description` longtext,
           `faqs_description_ar` longtext,
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;	

#DROP TABLE IF EXISTS `#__eposts_benchmarks`;
CREATE TABLE IF NOT EXISTS `#__eposts_benchmarks` (
		`id` int(11) NOT NULL auto_increment,
		   `benchmarks_name` text NOT NULL,
           `benchmarks_name_ar` text NOT NULL,		   
		   `benchmarks_description` longtext,
           `benchmarks_description_ar` longtext,
           `benchmarks_department` int(11),
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_manuals`;
CREATE TABLE IF NOT EXISTS `#__eposts_manuals` (
		`id` int(11) NOT NULL auto_increment,
		   `manuals_name` text NOT NULL,
           `manuals_name_ar` text NOT NULL,
		   `manuals_manualdate` datetime NOT NULL default '0000-00-00 00:00:00',
		   `manuals_access` tinyint(4) NOT NULL default '0',
           `manuals_department` int(11),
		   `manuals_doc` text NOT NULL,
		   `manuals_doc_ar` text NOT NULL,
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_policies`;
CREATE TABLE IF NOT EXISTS `#__eposts_policies` (
		`id` int(11) NOT NULL auto_increment,
		   `policies_name` text NOT NULL,
           `policies_name_ar` text NOT NULL,
		   `policies_policydate` datetime NOT NULL default '0000-00-00 00:00:00',
		   `policies_access` tinyint(4) NOT NULL default '0',
           `policies_department` int(11),
		   `policies_doc` text NOT NULL,
		   `policies_doc_ar` text NOT NULL,
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_quotes`;
CREATE TABLE IF NOT EXISTS `#__eposts_quotes` (
		`id` int(11) NOT NULL auto_increment,
		   `quotes_name` text NOT NULL,
           `quotes_name_ar` text NOT NULL,
		   `quotes_description` longtext,
           `quotes_description_ar` longtext,
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_alerts`;
CREATE TABLE IF NOT EXISTS `#__eposts_alerts` (
		`id` int(11) NOT NULL auto_increment,
		   `alerts_name` text NOT NULL,
           `alerts_name_ar` text NOT NULL,
		   `alerts_description` longtext,
           `alerts_description_ar` longtext,
		   `alerts_longdescription` longtext,
           `alerts_longdescription_ar` longtext,
		   `alerts_startdate` datetime NOT NULL default '0000-00-00 00:00:00',
		   `alerts_enddate` datetime NOT NULL default '0000-00-00 00:00:00',
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_visions`;
CREATE TABLE IF NOT EXISTS `#__eposts_visions` (
		`id` int(11) NOT NULL auto_increment,
		   `visions_name` text NOT NULL,
           `visions_name_ar` text NOT NULL,
		   `visions_description` longtext,
           `visions_description_ar` longtext,
		   `visions_longdescription` longtext,
           `visions_longdescription_ar` longtext,
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_news`;
CREATE TABLE IF NOT EXISTS `#__eposts_news` (
		`id` int(11) NOT NULL auto_increment,
		   `news_name` text NOT NULL,
           `news_name_ar` text NOT NULL,
		   `news_source` text NOT NULL,
		   `news_source_ar` text NOT NULL,
		   `news_description` longtext,
           `news_description_ar` longtext,
		   `news_longdescription` longtext,
           `news_longdescription_ar` longtext,
		   `news_newsdate` date NOT NULL default '0000-00-00',
		   `news_access` tinyint(4) NOT NULL default '0',
           `news_department` int(11),
		   `news_image` text NOT NULL,
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_announcements`;
CREATE TABLE IF NOT EXISTS `#__eposts_announcements` (
		`id` int(11) NOT NULL auto_increment,
		`announcements_name` text NOT NULL,
           `announcements_name_ar` text NOT NULL,
		`announcements_description` longtext,
           `announcements_description_ar` longtext,
		   `announcements_expirydate` datetime NOT NULL default '0000-00-00 00:00:00',
		   `announcements_showontop` tinyint(4) NOT NULL default '0',
		`announcements_access` tinyint(4) NOT NULL default '0',
           `announcements_department` int(11),
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

############################## END Development Phase III ##############################







#DROP TABLE IF EXISTS `#__eposts_booking`;
CREATE TABLE IF NOT EXISTS `#__eposts_booking` (
		`id` int(11) NOT NULL auto_increment,
           `booking_tiltle` text NOT NULL,
	      `resourceid` int(11) NOT NULL,
	      `resources_offices` int(11) NOT NULL,
		 `booking_date` date NOT NULL default '0000-00-00',
           `bookedslots` longtext,
	      `booking_members` longtext,
	      `booking_description` longtext,
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;


DROP TABLE IF EXISTS `#__eposts_bookingslots`;
CREATE TABLE IF NOT EXISTS `#__eposts_bookingslots` (
		`id` int(11) NOT NULL auto_increment,
		`bookingslots_title` text NOT NULL,
           `bookingslots_title_ar` text NOT NULL,
	      `bookingslots_description` longtext,
           `bookingslots_description_ar` longtext,
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

INSERT INTO `#__eposts_bookingslots` (`id`, `bookingslots_title`, `bookingslots_title_ar`, `bookingslots_description`, `bookingslots_description_ar`, `publishedon`, `publishedby`, `jlanguage`) VALUES
(1, '09:00 am to 09:15 am', '09:00 am to 09:15 am', '090000', '091500', '2013-01-22 09:51:17', 35, ''),
(2, '09:15 am to 09:30 am', '09:15 am to 09:30 am', '091500', '093000', '2013-01-22 09:51:17', 35, ''),
(3, '09:30 am to 09:45 am', '09:30 am to 09:45 am', '093000', '104500', '2013-01-22 09:51:41', 35, ''),
(4, '09:45 am to 10:00 am', '09:45 am to 10:00 am', '094500', '100000', '2013-01-22 09:51:41', 35, ''),
(5, '10:00 am to 10:15 am', '10:00 am to 10:15 am', '100000', '101500', '2013-01-22 09:52:07', 35, ''),
(6, '10:15 am to 10:30 am', '10:15 am to 10:30 am', '101500', '103000', '2013-01-22 09:52:07', 35, ''),
(7, '10:30 am to 10:45 am', '10:30 am to 10:45 am', '103000', '114500', '2013-01-22 09:52:29', 35, ''),
(8, '10:45 am to 11:00 am', '10:45 am to 11:00 am', '104500', '110000', '2013-01-22 09:52:29', 35, ''),
(9, '11:00 am to 11:15 am', '11:00 am to 11:15 am', '110000', '111500', '2013-01-22 09:52:54', 35, ''),
(10, '11:15 am to 11:30 am', '11:15 am to 11:30 am', '111500', '113000', '2013-01-22 09:52:54', 35, ''),
(11, '11:30 am to 11:45 pm', '11:30 am to 11:45 pm', '113000', '114500', '2013-01-22 09:53:28', 35, ''),
(12, '11:45 am to 12:00 pm', '11:45 am to 12:00 pm', '114500', '120000', '2013-01-22 09:53:28', 35, ''),
(13, '12:00 pm to 12:15 pm', '12:00 pm to 12:15 pm', '120000', '121500', '2013-01-22 09:53:48', 35, ''),
(14, '12:15 pm to 12:30 pm', '12:15 pm to 12:30 pm', '121500', '123000', '2013-01-22 09:53:48', 35, ''),
(15, '12:30 pm to 12:45 pm', '12:30 pm to 12:45 pm', '123000', '124500', '2013-01-22 09:54:10', 35, ''),
(16, '12:45 pm to 01:00 pm', '12:45 pm to 01:00 pm', '124500', '130000', '2013-01-22 09:54:10', 35, ''),
(17, '01:00 pm to 01:15 pm', '01:00 pm to 01:15 pm', '130000', '131500', '2013-01-22 10:32:28', 35, ''),
(18, '01:15 pm to 01:30 pm', '01:15 pm to 01:30 pm', '131500', '133000', '2013-01-22 10:32:28', 35, ''),
(19, '01:30 pm to 01:45 pm', '01:30 pm to 01:45 pm', '133000', '134500', '2013-01-22 10:33:26', 35, ''),
(20, '01:45 pm to 02:00 pm', '01:45 pm to 02:00 pm', '134500', '140000', '2013-01-22 10:33:26', 35, ''),
(21, '02:00 pm to 02:15 pm', '02:00 pm to 02:15 pm', '140000', '141500', '2013-01-22 10:34:09', 35, ''),
(22, '02:15 pm to 02:30 pm', '02:15 pm to 02:30 pm', '141500', '143000', '2013-01-22 10:34:09', 35, ''),
(23, '02:30 pm to 02:45 pm', '02:30 pm to 02:45 pm', '143000', '144500', '2013-01-22 10:34:43', 35, ''),
(24, '02:45 pm to 03:00 pm', '02:45 pm to 03:00 pm', '144500', '150000', '2013-01-22 10:34:43', 35, ''),
(25, '03:00 pm to 03:15 pm', '03:00 pm to 03:15 pm', '150000', '151500', '2013-01-22 10:35:26', 35, ''),
(26, '03:15 pm to 03:30 pm', '03:15 pm to 03:30 pm', '151500', '153000', '2013-01-22 10:35:26', 35, ''),
(27, '03:30 pm to 03:45 pm', '03:30 pm to 03:45 pm', '153000', '154500', '2013-01-22 10:35:53', 35, ''),
(28, '03:45 pm to 04:00 pm', '03:45 pm to 04:00 pm', '154500', '160000', '2013-01-22 10:35:53', 35, ''),
(29, '04:00 pm to 04:15 pm', '04:00 pm to 04:15 pm', '160000', '161500', '2013-01-22 10:36:23', 35, ''),
(30, '04:15 pm to 04:30 pm', '04:15 pm to 04:30 pm', '161500', '163000', '2013-01-22 10:36:23', 35, ''),
(31, '04:30 pm to 04:45 pm', '04:30 pm to 04:45 pm', '163000', '164500', '2013-01-22 10:37:12', 35, ''),
(32, '04:45 pm to 05:00 pm', '04:45 pm to 05:00 pm', '164500', '170000', '2013-01-22 10:37:12', 35, '');

#DROP TABLE IF EXISTS `#__eposts_resources`;
CREATE TABLE IF NOT EXISTS `#__eposts_resources` (
		`id` int(11) NOT NULL auto_increment,
		`resources_title` text NOT NULL,
           `resources_title_ar` text NOT NULL,
	      `resources_description` longtext,
           `resources_description_ar` longtext,
		`resources_controller` int(11) NOT NULL,
           `resources_members` longtext,
           `resources_offices` int(11),
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_resources_members`;
CREATE TABLE IF NOT EXISTS `#__eposts_resources_members` (
		`id` int(11) NOT NULL auto_increment,
		`resource_id` int(11) NOT NULL,
           `user_id` int(11) NOT NULL,
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_offices`;
CREATE TABLE IF NOT EXISTS `#__eposts_offices` (
		`id` int(11) NOT NULL auto_increment,
		`offices_title` text NOT NULL,
           `offices_title_ar` text NOT NULL,
		`offices_location` text NOT NULL,
           `offices_location_ar` text NOT NULL,
           `offices_phone` text NOT NULL,
           `offices_description` longtext,
           `offices_description_ar` longtext,
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_events`;
CREATE TABLE IF NOT EXISTS `#__eposts_events` (
		`id` int(11) NOT NULL auto_increment,
		`events_title` text NOT NULL,
           `events_title_ar` text NOT NULL,
		   `events_location` text NOT NULL,
           `events_location_ar` text NOT NULL,
		   `events_organized_by` text NOT NULL,
           `events_organized_by_ar` text NOT NULL,
           `events_startdate` datetime NOT NULL default '0000-00-00 00:00:00',
           `events_enddate` datetime NOT NULL default '0000-00-00 00:00:00',
		   `events_description` longtext,
           `events_description_ar` longtext,
		   `events_access` tinyint(4) NOT NULL default '1',
           `events_department` int(11),
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_newhires`;
CREATE TABLE IF NOT EXISTS `#__eposts_newhires` (
		`id` int(11) NOT NULL auto_increment,
		`newhires_name` text NOT NULL,
           `newhires_name_ar` text NOT NULL,
		`newhires_designation` text NOT NULL,
           `newhires_designation_ar` text NOT NULL,
           `newhires_mobile` text NOT NULL,
           `newhires_email` text NOT NULL,
		`newhires_description` longtext,
           `newhires_description_ar` longtext,
		`newhires_access` tinyint(4) NOT NULL default '1',
           `newhires_picture` text,
           `newhires_department` int(11),
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_dms`;
CREATE TABLE IF NOT EXISTS `#__eposts_dms` (
		`id` int(11) NOT NULL auto_increment,
		`dms_name` text NOT NULL,
           `dms_name_ar` text NOT NULL,
		`dms_description` longtext,
           `dms_description_ar` longtext,
		`dms_access` tinyint(4) NOT NULL default '1',
           `dms_title` text,
	      `dms_path` text NOT NULL,
		`dms_file_data` longtext,
           `dms_department` int(11),
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_circulars`;
CREATE TABLE IF NOT EXISTS `#__eposts_circulars` (
		   `id` int(11) NOT NULL auto_increment,
		   `circulars_name` text NOT NULL,
           `circulars_name_ar` text NOT NULL,
		   `circulars_description` longtext,
           `circulars_description_ar` longtext,
		   `circulars_access` tinyint(4) NOT NULL default '1',
           `circulars_document` text,
		   `circulars_document_ar` text,
	       `circulars_date` datetime NOT NULL default '0000-00-00 00:00:00',
           `circulars_department` int(11),
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_dwnmedias`;
CREATE TABLE IF NOT EXISTS `#__eposts_dwnmedias` (
		`id` int(11) NOT NULL auto_increment,
		`media_name` text NOT NULL,
           `media_name_ar` text NOT NULL,
		`media_description` longtext,
           `media_description_ar` longtext,
		`media_access` tinyint(4) NOT NULL default '1',
           `media_document` text,
           `media_department` int(11),
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

#DROP TABLE IF EXISTS `#__eposts_tasawaqs`;
CREATE TABLE IF NOT EXISTS `#__eposts_tasawaqs` (
		`id` int(11) NOT NULL auto_increment,
		`tasawaqs_name` text NOT NULL,
           `tasawaqs_name_ar` text NOT NULL,
		`tasawaqs_description` longtext,
           `tasawaqs_description_ar` longtext,
		`tasawaqs_access` tinyint(4) NOT NULL default '1',
           `tasawaqs_price` text,
           `tasawaqs_department` int(11),
		   `publish` tinyint(4) NOT NULL default '0',
           `publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

	#DROP TABLE IF EXISTS `#__eposts_departments`;
	CREATE TABLE IF NOT EXISTS `#__eposts_departments` (
		`id` int(11) NOT NULL auto_increment,
		`departments_name` text NOT NULL,
           `departments_name_ar` text NOT NULL,
		   `departments_email` text NOT NULL,
		`departments_description` longtext,
           `departments_description_ar` longtext,
		`departments_access` tinyint(4) NOT NULL default '1',
		`departments_map` text,
		`departments_members` text,
		`publishedon` datetime NOT NULL default '0000-00-00 00:00:00',
           `publishedby` int(11),
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;
#DROP TABLE IF EXISTS `#__eposts_department_members`;
CREATE TABLE IF NOT EXISTS `#__eposts_department_members` (
		`id` int(11) NOT NULL auto_increment,
		`department_id` text NOT NULL,
           `user_id` text NOT NULL,
		PRIMARY KEY  (`id`)
	) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;
	
	CREATE TABLE IF NOT EXISTS `#__eposts_log` (
		`id` int(11) NOT NULL auto_increment,
		`type` tinyint(1) NOT NULL default '1',
		`log_file_id` int(11) NOT NULL,
		`log_ip` varchar(25) NOT NULL default '',
		`log_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
		`log_user` int(11) NOT NULL default '0',
		`log_browser` varchar(255) NOT NULL default '',
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
		PRIMARY KEY  (`id`)
        ) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;

    CREATE TABLE IF NOT EXISTS `#__eposts_rating` (
        `file_id` int(11) NOT NULL default '0',
        `rating_sum` int(11) unsigned NOT NULL default '0',
        `rating_count` int(11) unsigned NOT NULL default '0',
        `lastip` varchar(50) NOT NULL default '',
		`jlanguage` VARCHAR( 7 ) NOT NULL default '',
        PRIMARY KEY  (`file_id`)
        ) ENGINE=MyISAM CHARACTER SET `utf8` COLLATE `utf8_general_ci`;