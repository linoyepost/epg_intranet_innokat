<?php
/**
* @version 1.6
* @package JDownloads
* @copyright (C) 2009 www.eposts.com
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* functions to check db after restore backup file!
*
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
  

function checkAfterRestore() {
    global $jlistConfig;
    $database = &JFactory::getDBO();

    // insert the new default header, subheader and footer layouts in every layout.
    require_once(JPATH_SITE."/administrator/components/com_eposts/helpers/jd_layouts.php");

    
  //*********************************************
  // JD VERSION:
     $jd_version = '1.9.1';
     $jd_version_state = 'Stable';
     $jd_version_svn = '931'; 
  //*********************************************
    
    $output = '';
    
//********************************************************************************************
// insert default config data - if not exist
// *******************************************************************************************
      $root_dir = '';
      $sum_configs = 0;
      
        $database->setQuery("SELECT * FROM #__eposts_config WHERE setting_name = 'files.uploaddir'");
        $temp = $database->loadResult();
        if (!$temp) {
            $database->SetQuery("INSERT INTO #__eposts_config (setting_name, setting_value) VALUES ('files.uploaddir', 'eposts')");
            $database->query();
            $sum_configs++;
        }  else {
            $database->setQuery("SELECT setting_value FROM #__eposts_config WHERE setting_name = 'files.uploaddir'");
            $dir = $database->loadResult();
            $root_dir = JPATH_SITE.'/'.$dir.'/';   
        }    

        
        $database->setQuery("SELECT * FROM #__eposts_config WHERE setting_name = 'global.datetime'");
        $temp = $database->loadResult();
        if (!$temp) {
            $database->SetQuery("INSERT INTO #__eposts_config (setting_name, setting_value) VALUES ('global.datetime', '".JText::_('COM_EPOSTS_INSTALL_DEFAULT_DATE_FORMAT')."')");
            $database->query();
            $sum_configs++;
        } else {
            $database->setQuery("UPDATE #__eposts_config SET setting_value = '".JText::_('COM_EPOSTS_INSTALL_DEFAULT_DATE_FORMAT')."' WHERE setting_name = 'global.datetime'");
            $database->query();
            $jlistConfig['global.datetime'] = JText::_('COM_EPOSTS_INSTALL_DEFAULT_DATE_FORMAT');
        }
        
        $database->setQuery("UPDATE #__eposts_config SET setting_value = '0' WHERE setting_name = 'jcomments.active'");
        $database->query();
        $jlistConfig['jcomments.active'] = '0';
        
        
        $database->setQuery("UPDATE #__eposts_config SET setting_value = '0' WHERE setting_name = 'view.jom.comment'");
        $database->query();
        $jlistConfig['view.jom.comment'] = '0';
        
                
        /*
        // new param f�r versionsnummer von jd
        $database->setQuery("SELECT * FROM #__eposts_config WHERE setting_name = 'jd.version'");
        $temp = $database->loadResult();
        if (!$temp) {
            $database->SetQuery("INSERT INTO #__eposts_config (setting_name, setting_value) VALUES ('jd.version','$jd_version')");
            $database->query();
            $sum_configs++;
        } else {
            // set new value
            $database->setQuery("UPDATE #__eposts_config SET setting_value = '$jd_version' WHERE setting_name = 'jd.version'");  
            $database->query();
        } 
        
        // new param f�r versions status von jd
        $database->setQuery("SELECT * FROM #__eposts_config WHERE setting_name = 'jd.version.state'");
        $temp = $database->loadResult();
        if (!$temp) {
            $database->SetQuery("INSERT INTO #__eposts_config (setting_name, setting_value) VALUES ('jd.version.state','$jd_version_state')");
            $database->query();
            $sum_configs++;
        } else {
            // set new value
            $database->setQuery("UPDATE #__eposts_config SET setting_value = '$jd_version_state' WHERE setting_name = 'jd.version.state'");  
            $database->query();
        }    

        // new param f�r svn version von jd
        $database->setQuery("SELECT * FROM #__eposts_config WHERE setting_name = 'jd.version.svn'");
        $temp = $database->loadResult();
        if (!$temp) {
            $database->SetQuery("INSERT INTO #__eposts_config (setting_name, setting_value) VALUES ('jd.version.svn','$jd_version_svn')");
            $database->query();
            $sum_configs++;
        } else {
            // set new value
            $database->setQuery("UPDATE #__eposts_config SET setting_value = '$jd_version_svn' WHERE setting_name = 'jd.version.svn'");  
            $database->query();
        }
        */
         // new in 1.5
        
                          
        if ($sum_configs == 0) {
            $output .= '<font color="green"><strong> '.JText::_('COM_EPOSTS_INSTALL_1').'</strong></font><br />';
        } else {
            $output .= '<font color="green"> '.$sum_configs.' '.JText::_('COM_EPOSTS_INSTALL_2').'</font><br />';
        }

        //***************************** config data end **********************************************

        // add new fields for joomla 1.6 version
        $sum_added_fields = 0;
        $prefix = $database->getPrefix();
        $tables = array( $prefix.'eposts_cats' );
        $result = $database->getTableFields( $tables );
        
        if (!$result[$prefix.'eposts_cats']['jaccess']){
            $database->SetQuery("ALTER TABLE #__eposts_cats ADD jaccess TINYINT(3) NOT NULL DEFAULT 0 AFTER metadesc");
            if ($database->query()) {
                $sum_added_fields++;
            }
            $database->SetQuery("ALTER TABLE #__eposts_cats ADD jlanguage VARCHAR(7) NOT NULL DEFAULT '' AFTER jaccess");
            if ($database->query()) {
                $sum_added_fields++;
            }      
        }

        $tables = array( $prefix.'eposts_files' );
        $result = $database->getTableFields( $tables );
        
        if (!$result[$prefix.'eposts_files']['jaccess']){
            $database->SetQuery("ALTER TABLE #__eposts_files ADD jaccess TINYINT(3) NOT NULL DEFAULT 0 AFTER custom_field_14");
            if ($database->query()) {
                $sum_added_fields++;
            } 
            $database->SetQuery("ALTER TABLE #__eposts_files ADD jlanguage VARCHAR(7) NOT NULL DEFAULT '' AFTER jaccess"); 
            if ($database->query()) {
                $sum_added_fields++;   
            }
        }

        if (!$result[$prefix.'eposts_files']['created_id']){
            $database->SetQuery("ALTER TABLE #__eposts_files ADD created_id INT(11) NOT NULL DEFAULT 0 AFTER created_by");
            if ($database->query()) {
                $sum_added_fields++;
            } 
            $database->SetQuery("ALTER TABLE #__eposts_files ADD modified_id INT(11) NOT NULL DEFAULT 0 AFTER modified_by");
            if ($database->query()) {
                $sum_added_fields++;   
            }
        }
        
        $tables = array( $prefix.'eposts_license' );
        $result = $database->getTableFields( $tables );
        
        if (!$result[$prefix.'eposts_license']['jlanguage']){
            $database->SetQuery("ALTER TABLE #__eposts_license ADD jlanguage VARCHAR(7) NOT NULL DEFAULT '' AFTER license_url"); 
            if ($database->query()) {
                $sum_added_fields++;   
            }
        }        
        
        $tables = array( $prefix.'eposts_templates' );
        $result = $database->getTableFields( $tables );
        
        if (!$result[$prefix.'eposts_templates']['jlanguage']){
            $database->SetQuery("ALTER TABLE #__eposts_templates ADD jlanguage VARCHAR(7) NOT NULL DEFAULT '' AFTER symbol_off"); 
            if ($database->query()) {
                $sum_added_fields++;   
            }
        }
        
        $tables = array( $prefix.'eposts_departments' );
        $result = $database->getTableFields( $tables );
        
        if (!$result[$prefix.'eposts_departments']['jlanguage']){
            $database->SetQuery("ALTER TABLE #__eposts_departments ADD jlanguage VARCHAR(7) NOT NULL DEFAULT '' AFTER departments_members"); 
            if ($database->query()) {
                $sum_added_fields++;   
            }
        }        
        
        $tables = array( $prefix.'eposts_log' );
        $result = $database->getTableFields( $tables );
        
        if (!$result[$prefix.'eposts_log']['jlanguage']){
            $database->SetQuery("ALTER TABLE #__eposts_log ADD jlanguage VARCHAR(7) NOT NULL DEFAULT '' AFTER log_browser"); 
            if ($database->query()) {
                $sum_added_fields++;   
            }
        }         
        
        $tables = array( $prefix.'eposts_rating' );
        $result = $database->getTableFields( $tables );
        
        if (!$result[$prefix.'eposts_rating']['jlanguage']){
            $database->SetQuery("ALTER TABLE #__eposts_rating ADD jlanguage VARCHAR(7) NOT NULL DEFAULT '' AFTER lastip"); 
            if ($database->query()) {
                $sum_added_fields++;   
            }
        }        
        
        if ($sum_added_fields == 0) {
            $output .= "<font color='green'><strong> ".JText::_('COM_EPOSTS_INSTALL_1_2')."</strong></font><br />";
        } else {
            $output .= "<font color='green'> ".$sum_added_fields." ".JText::_('COM_EPOSTS_INSTALL_2_2')."</font><br />";        
        }
        
        // add newer layouts when not exists
        $database->setQuery("SELECT * FROM #__eposts_templates WHERE template_typ = '2' AND locked = '1'  AND  template_name ='".$COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_FILES_DEFAULT_NEW_SIMPLE_3_NAME."'");
        $temp = $database->loadResult();
        if (!$temp) {
              $file_layout = stripslashes($COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_FILES_DEFAULT_NEW_SIMPLE_3); 
              $database->setQuery("INSERT INTO #__eposts_templates (template_name, template_typ, template_text, template_active, locked, note, checkbox_off, symbol_off)  VALUES ('".$COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_FILES_DEFAULT_NEW_SIMPLE_3_NAME."', 2, '".$file_layout."', 0, 1, '', 1, 1)");
              $database->query();
              $sum_layouts++;
              $database->setQuery("UPDATE #__eposts_templates SET template_header_text = '$files_header', template_subheader_text = '$files_subheader', template_footer_text = '$files_footer' WHERE template_typ = '2' AND template_header_text = ''");
              $database->query();
        }

   
   return $output;
}      
?>