<?php
/**
* @version 1.5
* @package JDownloads
* @copyright (C) 2009 www.eposts.com
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* 
*
*/

defined( '_JEXEC' ) or die( 'Restricted access' );

class menujlist {
		////////////////////////////////DEVELOPMENT PHASE III /////////////////////
/*Announcements*/	
	function LIST_ANNOUNCEMENTS(){
        JToolBarHelper::title( JText::_('Announcements'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.announcements');
        JToolBarHelper::deleteList('Do you really want to delete the record? ','delete.announcements');
    } 
    
    function EDIT_ANNOUNCEMENTS(){
        JToolBarHelper::title( 'Add/Edit Announcement', 'jdlogo' ); 
        JToolBarHelper::save('save.announcements');
        JToolBarHelper::apply('apply.announcements');
        JToolBarHelper::cancel('cancel.announcements');
    }
/*News*/
	function LIST_NEWS(){
        JToolBarHelper::title( JText::_('News'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.news');
        JToolBarHelper::deleteList('Do you really want to delete the record? ','delete.news');
    } 
    
    function EDIT_NEWS(){
        JToolBarHelper::title( 'Add/Edit News', 'jdlogo' ); 
        JToolBarHelper::save('save.news');
        JToolBarHelper::apply('apply.news');
        JToolBarHelper::cancel('cancel.news');
    }	
/*Our Vision*/	
	function LIST_VISIONS(){
        JToolBarHelper::title( JText::_('Our Vision'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.visions');
        JToolBarHelper::deleteList('Do you really want to delete the record? ','delete.visions');
    } 
    
    function EDIT_VISIONS(){
        JToolBarHelper::title( 'Add/Edit Our Vision', 'jdlogo' ); 
        JToolBarHelper::save('save.visions');
        JToolBarHelper::apply('apply.visions');
        JToolBarHelper::cancel('cancel.visions');
    }
/*Alerts*/	
	function LIST_ALERTS(){
        JToolBarHelper::title( JText::_('Alerts'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.alerts');
        JToolBarHelper::deleteList('Do you really want to delete the record? ','delete.alerts');
    } 
    
    function EDIT_ALERTS(){
        JToolBarHelper::title( 'Add/Edit Alert', 'jdlogo' ); 
        JToolBarHelper::save('save.alerts');
        JToolBarHelper::apply('apply.alerts');
        JToolBarHelper::cancel('cancel.alerts');
    }
/*Policies & Procedures*/
	function LIST_POLICIES(){
        JToolBarHelper::title( JText::_('Policies & Procedures'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.policies');
        JToolBarHelper::deleteList('Do you really want to delete the record? ','delete.policies');
    } 
    
    function EDIT_POLICIES(){
        JToolBarHelper::title( 'Add/Edit Policy & Procedure', 'jdlogo' ); 
        JToolBarHelper::save('save.policies');
        JToolBarHelper::apply('apply.policies');
        JToolBarHelper::cancel('cancel.policies');
    }	
/*Quotes*/	
	function LIST_QUOTES(){
        JToolBarHelper::title( JText::_('Quotes'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.quotes');
        JToolBarHelper::deleteList('Do you really want to delete the record? ','delete.quotes');
    } 
    
    function EDIT_QUOTES(){
        JToolBarHelper::title( 'Add/Edit Quote', 'jdlogo' ); 
        JToolBarHelper::save('save.quotes');
        JToolBarHelper::apply('apply.quotes');
        JToolBarHelper::cancel('cancel.quotes');
    }	
/*Manuals*/	
	function LIST_MANUALS(){
        JToolBarHelper::title( JText::_('Manuals'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.manuals');
        JToolBarHelper::deleteList('Do you really want to delete the record? ','delete.manuals');
    } 
    
    function EDIT_MANUALS(){
        JToolBarHelper::title( 'Add/Edit Manual', 'jdlogo' ); 
        JToolBarHelper::save('save.manuals');
        JToolBarHelper::apply('apply.manuals');
        JToolBarHelper::cancel('cancel.manuals');
    }
/*FAQs*/
	function LIST_FAQS(){
        JToolBarHelper::title( JText::_('FAQs'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.faqs');
        JToolBarHelper::deleteList('Do you really want to delete the record? ','delete.faqs');
    } 
    
    function EDIT_FAQS(){
        JToolBarHelper::title( 'Add/Edit FAQs', 'jdlogo' ); 
        JToolBarHelper::save('save.faqs');
        JToolBarHelper::apply('apply.faqs');
        JToolBarHelper::cancel('cancel.faqs');
    }
		
/*Benchmarks*/
	function LIST_BENCHMARKS(){
        JToolBarHelper::title( JText::_('Benchmarks'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.benchmarks');
        JToolBarHelper::deleteList('Do you really want to delete the record? ','delete.benchmarks');
    } 
    
    function EDIT_BENCHMARKS(){
        JToolBarHelper::title( 'Add/Edit Benchmark', 'jdlogo' ); 
        JToolBarHelper::save('save.benchmarks');
        JToolBarHelper::apply('apply.benchmarks');
        JToolBarHelper::cancel('cancel.benchmarks');
    }
/*Links*/
	function LIST_LINKS(){
        JToolBarHelper::title( JText::_('Links'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.links');
        JToolBarHelper::deleteList('Do you really want to delete the record? ','delete.links');
    } 
    
    function EDIT_LINKS(){
        JToolBarHelper::title( 'Add/Edit Link', 'jdlogo' ); 
        JToolBarHelper::save('save.links');
        JToolBarHelper::apply('apply.links');
        JToolBarHelper::cancel('cancel.links');
    }		
	/////////////////////////////////END/////////////////////////////
	
	
	function LIST_DMS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_DMS'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.dms');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_DMS_DEL_QUEST'),'delete.dms');
    } 
    
    function EDIT_DMS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_DMS'), 'jdlogo' ); 
        JToolBarHelper::save('save.dms');
        JToolBarHelper::apply('apply.dms');
        JToolBarHelper::cancel('cancel.dms');
    }
	
	function LIST_BOOKINSLOTS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_CPANEL_BOOKINSLOTS'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.bookingslots');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_BOOKINSLOTS_DEL_QUEST'),'delete.bookingslots');
    } 
    
    function EDIT_BOOKINSLOTS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_CPANEL_BOOKINSLOTS'), 'jdlogo' ); 
        JToolBarHelper::save('save.bookingslots');
        JToolBarHelper::apply('apply.bookingslots');
        JToolBarHelper::cancel('cancel.bookingslots');
    }
	
	function LIST_RESOURCES(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_CPANEL_RESOURCES'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.resources');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_RESOURCES_DEL_QUEST'),'delete.resources');
    } 
    
    function EDIT_RESOURCES(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_CPANEL_RESOURCES'), 'jdlogo' ); 
        JToolBarHelper::save('save.resources');
        JToolBarHelper::apply('apply.resources');
        JToolBarHelper::cancel('cancel.resources');
    }
	
	function LIST_OFFICES(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_CPANEL_OFFICES'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.offices');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_OFFICES_DEL_QUEST'),'delete.offices');
    } 
    
    function EDIT_OFFICES(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_CPANEL_OFFICES'), 'jdlogo' ); 
        JToolBarHelper::save('save.offices');
        JToolBarHelper::apply('apply.offices');
        JToolBarHelper::cancel('cancel.offices');
    }
	
	function LIST_EVENTS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_CPANEL_EVENTS'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.events');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_EVENTS_DEL_QUEST'),'delete.events');
    } 

    function LIST_RESOURCESRIGHTS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_CPANEL_MANAGERESOURCES'), 'jdlogo' ); 
        JToolBarHelper::save('save.manageresources',"Assign");
        JToolBarHelper::cancel('cancel.manageresources',"Un Assign");
    } 
    
    function EDIT_EVENTS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_CPANEL_EVENTS'), 'jdlogo' ); 
        JToolBarHelper::save('save.events');
        JToolBarHelper::apply('apply.events');
        JToolBarHelper::cancel('cancel.events');
    }
	
	function LIST_NEWHIRES(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_NEWHIRES'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.newhires');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_NEWHIRES_DEL_QUEST'),'delete.newhires');
    } 
    
    function EDIT_NEWHIRES(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_NEWHIRES_EDIT_TITLE'), 'jdlogo' ); 
        JToolBarHelper::save('save.newhires');
        JToolBarHelper::apply('apply.newhires');
        JToolBarHelper::cancel('cancel.newhires');
    }
		
	function LIST_TASAWAQS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_TASAWAQS'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.tasawaqs');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_TASAWAQS_DEL_QUEST'),'delete.tasawaqs');
    } 
    
    function EDIT_TASAWAQS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_TASAWAQS_EDIT_TITLE'), 'jdlogo' ); 
        JToolBarHelper::save('save.tasawaqs');
        JToolBarHelper::apply('apply.tasawaqs');
        JToolBarHelper::cancel('cancel.tasawaqs');
    }
	    
    function LIST_CIRCULARS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_CIRCULARS'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.circulars');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_CIRCULARS_DEL_QUEST'),'delete.circulars');
    } 
    
    function EDIT_CIRCULARS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CIRCULARS_EDIT_TITLE'), 'jdlogo' ); 
        JToolBarHelper::save('save.circulars');
        JToolBarHelper::apply('apply.circulars');
        JToolBarHelper::cancel('cancel.circulars');
    }
	
	function LIST_DWNMEDIAS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.dwnmedias');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_DWNMEDIAS_DEL_QUEST'),'delete.dwnmedias');
    } 
    
    function EDIT_DWNMEDIAS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_EDIT_TITLE'), 'jdlogo' ); 
        JToolBarHelper::save('save.dwnmedias');
        JToolBarHelper::apply('apply.dwnmedias');
        JToolBarHelper::cancel('cancel.dwnmedias');
    }
	    
    function LIST_DEPARTMENTS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS'), 'jdlogo' ); 
        JToolBarHelper::addNewX('edit.departments');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_DEPARTMENTS_DEL_QUEST'),'delete.departments');
    } 
    
    function EDIT_DEPARTMENTS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_DEPARTMENTS_EDIT_TITLE'), 'jdlogo' ); 
        JToolBarHelper::save('save.departments');
        JToolBarHelper::apply('apply.departments');
        JToolBarHelper::cancel('cancel.departments');
    }
	
    function _DEFAULT(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_MAIN'), 'jdlogo' ); 
    }

	function EDIT_MENU(){ 
		JToolBarHelper::title( JText::_(''), 'jdlogo' ); 
        JToolBarHelper::save();
		JToolBarHelper::cancel();
	}

	function SETTINGS_MENU(){ 
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_SETTINGS'), 'jdlogo' ); 
        JToolBarHelper::save('config.save');
        JToolBarHelper::apply('config.apply');
        if (JFactory::getUser()->authorise('core.admin', 'com_eposts')) {
            JToolBarHelper::divider(); 
            JToolBarHelper::preferences('com_eposts');
            JToolBarHelper::divider(); 
        }
		JToolBarHelper::cancel('',JText::_('COM_EPOSTS_BACKEND_TOOLBAR_CLOSE'));
	}

	function CATEGORIES_LIST(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_CATEGORIES'), 'jdlogo' ); 
        JToolBarHelper::addNewX('categories.edit');
		JToolBarHelper::publish('categories.publish');
		JToolBarHelper::unpublish('categories.unpublish');
		JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_CATSLIST_DEL_QUEST'),'categories.delete');
	}

	function CATEGORIES_ADD(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CATSEDIT_TITLE'), 'jdlogo' ); 
        JToolBarHelper::save('categories.save');
        JToolBarHelper::apply('categories.apply');
		JToolBarHelper::cancel('categories.cancel');
	}

	function FILES_LIST(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_FILES'), 'jdlogo' ); 
        JToolBarHelper::custom( 'files.upload', 'upload32.png', 'upload32.png', JText::_('COM_EPOSTS_BACKEND_FILESLIST_TITLE_FILES_UPLOAD'), false, true ); 
        JToolBarHelper::custom( 'manage.files', 'files32.png', 'files32.png', JText::_('COM_EPOSTS_BACKEND_FILESLIST_TITLE_MANAGE_FILES'), false, false );
        JToolBarHelper::divider();
        JToolBarHelper::addNewX('files.edit');
        JToolBarHelper::custom( 'files.move', 'move.png', 'move_f2.png', 'Move', false ); 
        JToolBarHelper::custom( 'files.copy', 'copy.png', 'copy_f2.png', 'Copy', false );     
		JToolBarHelper::publish('files.publish');
		JToolBarHelper::unpublish('files.unpublish');
		JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_FILESLIST_DEL_QUEST'),'files.delete');
	}

	function FILES_EDIT(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_FILESEDIT_TITLE'), 'jdlogo' ); 
        JToolBarHelper::save('files.save');
		JToolBarHelper::apply('files.apply');
		JToolBarHelper::cancel('files.cancel');
	}
    
    function FILES_MOVE(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_FILES_MOVE_TITLE'), 'jdlogo' );
        JToolBarHelper::save('files.move.save');
        JToolBarHelper::cancel('files.list');
    }     
    
    function FILES_COPY(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_FILES_COPY_TITLE'), 'jdlogo' ); 
        JToolBarHelper::save('files.copy.save');
        JToolBarHelper::cancel('files.list'); 
    }

    function FILES_UPLOAD(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_FILESLIST_TITLE_FILES_UPLOAD'), 'jdlogo' ); 
        JToolBarHelper::custom( 'manage.files', 'files32.png', 'files32.png', JText::_('COM_EPOSTS_BACKEND_FILESLIST_TITLE_MANAGE_FILES'), false, false );
        JToolBarHelper::custom( 'files.list', 'downloads_2.png', 'downloads_2.png', JText::_('COM_EPOSTS_BACKEND_CPANEL_FILES'), false, false );
        JToolBarHelper::cancel('',JText::_('COM_EPOSTS_BACKEND_TOOLBAR_CLOSE'));
    }
    
    function MANAGE_FILES(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_FILESLIST_TITLE_MANAGE_FILES'), 'jdlogo' ); 
        JToolBarHelper::custom( 'files.upload', 'upload32.png', 'upload32.png', JText::_('COM_EPOSTS_BACKEND_FILESLIST_TITLE_FILES_UPLOAD'), false, true );
        JToolBarHelper::custom( 'files.list', 'downloads_2.png', 'downloads_2.png', JText::_('COM_EPOSTS_BACKEND_CPANEL_FILES'), false, false );
        JToolBarHelper::divider();
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_FILESLIST_DEL_QUEST_FILES'),'delete.root.files');
    }    
       
    function LICENSE_LIST(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_LICENSE'), 'jdlogo' ); 
        JToolBarHelper::addNew('license.edit');
		JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_LICLIST_DEL_QUEST'),'license.delete');
	}

	function LICENSE_EDIT(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_LICEDIT_TITLE'), 'jdlogo' );
        JToolBarHelper::save('license.save');
		JToolBarHelper::cancel('license.cancel');
    }
    
	function TEMPLATES_MENU(){
	    JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_TEMPLATES_NAME'), 'jdlogo' ); 
    }

    function TEMPLATES_LIST_CATS(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_TEMPLIST_CATTITLE_HEAD'), 'jdlogo' );
        JToolBarHelper::custom( 'templates.list.files', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_FILESTITLE_HEAD'), false );
        JToolBarHelper::custom( 'templates.list.details', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_DETAILSTITLE_HEAD'), false );
        JToolBarHelper::custom( 'templates.list.summary', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_SUMTITLE_HEAD'), false );
        JToolBarHelper::divider();
        JToolBarHelper::custom( 'templates.active.cats', 'apply.png', 'apply_f2.png', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_MENU_TEXT_ACTIVE'), true );
		JToolBarHelper::addNew('templates.edit.cats');
		JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_TEMPLIST_DEL_QUEST'),'templates.delete.cats');
	}

	function TEMPLATES_EDIT_CATS(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_CATTITLE'), 'jdlogo' ); 
        JToolBarHelper::save('templates.save.cats');
		JToolBarHelper::apply('templates.apply.cats');
		JToolBarHelper::cancel('templates.cancel.cats');
    }

    function TEMPLATES_LIST_FILES(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_TEMPLIST_FILESTITLE_HEAD'), 'jdlogo' );
        JToolBarHelper::custom( 'templates.list.cats', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_CATTITLE_HEAD'), false );
        JToolBarHelper::custom( 'templates.list.details', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_DETAILSTITLE_HEAD'), false );
        JToolBarHelper::custom( 'templates.list.summary', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_SUMTITLE_HEAD'), false );
        JToolBarHelper::divider(); 
        JToolBarHelper::custom( 'templates.active.files', 'apply.png', 'apply_f2.png', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_MENU_TEXT_ACTIVE'), true );
		JToolBarHelper::addNew('templates.edit.files');
		JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_TEMPLIST_DEL'),'templates.delete.files');
	}

	function TEMPLATES_EDIT_FILES(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_FILESTITLE'), 'jdlogo' ); 
        JToolBarHelper::save('templates.save.files');
		JToolBarHelper::apply('templates.apply.files');
		JToolBarHelper::cancel('templates.cancel.files');
    }
    
    function TEMPLATES_LIST_DETAILS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_TEMPLIST_DETAILSTITLE_HEAD'), 'jdlogo' );
        JToolBarHelper::custom( 'templates.list.cats', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_CATTITLE_HEAD'), false );
        JToolBarHelper::custom( 'templates.list.files', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_FILESTITLE_HEAD'), false );        
        JToolBarHelper::custom( 'templates.list.summary', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_SUMTITLE_HEAD'), false );
        JToolBarHelper::divider(); 
        JToolBarHelper::custom( 'templates.active.details', 'apply.png', 'apply_f2.png', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_MENU_TEXT_ACTIVE'), true );
        JToolBarHelper::addNew('templates.edit.details');
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_TEMPLIST_DEL'),'templates.delete.details');
    }

    function TEMPLATES_EDIT_DETAILS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_DETAILSTITLE'), 'jdlogo' ); 
        JToolBarHelper::save('templates.save.details');
        JToolBarHelper::apply('templates.apply.details');
        JToolBarHelper::cancel('templates.cancel.details');
    }    

    function TEMPLATES_LIST_SUMMARY(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_TEMPLIST_SUMTITLE_HEAD'), 'jdlogo' );
        JToolBarHelper::custom( 'templates.list.cats', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_CATTITLE_HEAD'), false );
        JToolBarHelper::custom( 'templates.list.files', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_FILESTITLE_HEAD'), false );  
        JToolBarHelper::custom( 'templates.list.details', 'template', 'template', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_DETAILSTITLE_HEAD'), false ); 
        JToolBarHelper::divider();         
        JToolBarHelper::custom( 'templates.active.summary', 'apply.png', 'apply_f2.png', JText::_('COM_EPOSTS_BACKEND_TEMPLIST_MENU_TEXT_ACTIVE'), true );
		JToolBarHelper::addNew('templates.edit.summary');
		JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_TEMPLIST_DEL'),'templates.delete.summary');
	}

	function TEMPLATES_EDIT_SUMMARY(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SUMTITLE'), 'jdlogo' ); 
        JToolBarHelper::save('templates.save.summary');
		JToolBarHelper::apply('templates.apply.summary');
		JToolBarHelper::cancel('templates.cancel.summary');
    }
    
 	function CSS_EDIT(){
		JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_EDIT_CSS_TITLE_EDIT'), 'jdlogo' ); 
        JToolBarHelper::save('css.save');
		JToolBarHelper::cancel('templates.menu');
    }
    
	function LANG_EDIT(){
		JToolBarHelper::title( JText::_(''), 'jdlogo' ); 
        JToolBarHelper::save('language.save');
		JToolBarHelper::cancel('');
    }

	function RESTORE_MENU(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_RESTORE'), 'jdlogo' ); 
	}
    
    function LIST_LOGS(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_DOWNLOAD_LOGS'), 'jdlogo' ); 
        JToolBarHelper::custom( 'add.ip', 'cancel.png', 'cancel.png', JText::_('COM_EPOSTS_BACKEND_LOG_LIST_BLOCK_IP'), true );
        JToolBarHelper::deleteList(JText::_('COM_EPOSTS_BACKEND_VIEW_LOGS_DEL_QUEST'),'delete.logs');
    } 
	
	function INFO_MENU(){
        JToolBarHelper::title( JText::_('COM_EPOSTS_BACKEND_CPANEL_INFO'), 'jdlogo' ); 
	}
    
}

?>