<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_redirect
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of redirect links.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_redirect
 * @since		1.6
 */
class LikePagesModelLikes extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'id',
				'Page_Title', 'page_name',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{	
		// Initialise variables.
		$app = JFactory::getApplication('administrator');
		
		
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		
		$state = $this->getUserStateFromRequest($this->context.'.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $state);

		
		$state = $this->getUserStateFromRequest($this->context.'.filter.state.department', 'filter_state_department', '', 'string');
		$this->setState('filter.state.department', $state);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_likepages');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('page_name', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string	A prefix for the store id.
	 *
	 * @return	string	A store id.
	 * @since	1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery()
	{
		// Filter by published state
	 	$state = $this->getState('filter.state');
		
		if ($state==0) {
		
				// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);

			
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'pl.id,p.title As page_name , count(case when pl.Is_Like = 1 then 1 else null end ) as TotalLike , count(case when pl.Is_Like = 2 then 2 else null end ) as TotalUnLike, sa.Views_Count'
			)
		);
		
		//$query->from($db->quoteName('#__likepages').' AS a');
		$query->from($db->quoteName('#__content').' AS p');
		$query->join('LEFT', $db->quoteName('#__like_pages', 'pl') . ' ON (' . $db->quoteName('p.id') . ' = ' . $db->quoteName('pl.Item_Id') . ')');
		$query->join('LEFT', $db->quoteName('#__site_analytics', 'sa') . ' ON (' . $db->quoteName('sa.Item_ID') . ' = ' . $db->quoteName('p.Id') . ')');
		
		// Filter the items over the search string if set.
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
				$query->where(
					'('.$db->quoteName('p.title').' LIKE '.$search .')'
				);
			}
		}
			
		$query->group('p.title');
		
		return $query;
		
		}else if($state==1) {
				// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
			
			// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'p.id,p.news_name As page_name , count(case when pl.Is_Like = 1 then 1 else null end ) as TotalLike , count(case when pl.Is_Like = 2 then 2 else null end ) as TotalUnLike, sa.Views_Count'
			)
		);
		//$query->from($db->quoteName('#__likepages').' AS a');

		$query->from($db->quoteName('#__eposts_news').' AS p');
		$query->join('LEFT', $db->quoteName('#__like_news', 'pl') . ' ON (' . $db->quoteName('p.id') . ' = ' . $db->quoteName('pl.Item_Id') . ')');
		$query->join('LEFT', $db->quoteName('#__site_analytics', 'sa') . ' ON (' . $db->quoteName('sa.Item_ID') . ' = ' . $db->quoteName('p.Id') . ')');
		
		
		$state_department = $this->getState('filter.state.department');
		
		
		if ($state_department!=0) {
				$query->where('p.news_department = '.$state_department);
			}


		// Filter the items over the search string if set.
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
				$query->where(
					'('.$db->quoteName('p.news_name').' LIKE '.$search .')'
				);
			}
		}
		
		$query->group('p.news_name');
		
		// Add the list ordering clause.
		$query->order('p.id DESC');

		return $query;
		
		}else if($state==2) {
			
			// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
			
			// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'pl.id,p.policies_name As page_name , count(case when pl.Is_Like = 1 then 1 else null end ) as TotalLike , count(case when pl.Is_Like = 2 then 2 else null end ) as TotalUnLike, sa.Views_Count'
			)
		);
		//$query->from($db->quoteName('#__likepages').' AS a');

		$query->from($db->quoteName('#__eposts_policies').' AS p');
		$query->join('LEFT', $db->quoteName('#__like_policies', 'pl') . ' ON (' . $db->quoteName('p.id') . ' = ' . $db->quoteName('pl.Item_Id') . ')');
		$query->join('LEFT', $db->quoteName('#__site_analytics', 'sa') . ' ON (' . $db->quoteName('sa.Item_ID') . ' = ' . $db->quoteName('p.Id') . ')');
		
		$state_department = $this->getState('filter.state.department');
		
		
		if ($state_department!=0) {
				$query->where('p.policies_department = '.$state_department);
			}		
		

		// Filter the items over the search string if set.
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
				$query->where(
					'('.$db->quoteName('p.policies_name').' LIKE '.$search .')'
				);
			}
		}
		$query->group('p.policies_name');
		$query->orderby('p.id DESC');
		
		return $query;
						
		} else if($state==3) {
			
			// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
			
			// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'pl.id,p.announcements_name As page_name , count(case when pl.Is_Like = 1 then 1 else null end ) as TotalLike , count(case when pl.Is_Like = 2 then 2 else null end ) as TotalUnLike, sa.Views_Count'
			)
		);

	$query->from($db->quoteName('#__eposts_announcements').' AS p');
	$query->join('LEFT', $db->quoteName('#__like_announcement', 'pl') . ' ON (' . $db->quoteName('p.id') . ' = ' . $db->quoteName('pl.Item_Id') . ')');
	$query->join('LEFT', $db->quoteName('#__site_analytics', 'sa') . ' ON (' . $db->quoteName('sa.Item_ID') . ' = ' . $db->quoteName('p.Id') . ')');
		
	$state_department = $this->getState('filter.state.department');
		if ($state_department!=0) {
			$query->where('p.announcements_department = '.$state_department);
		}

		
	// Filter the items over the search string if set.
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
				$query->where(
					'('.$db->quoteName('p.announcements_name').' LIKE '.$search .')'
				);
			}
		}
		$query->group('p.announcements_name');
		$query->order('p.id DESC');
		return $query;
						
		} else if($state==4) {
			
		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
			
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'pl.id,p.circulars_name As page_name , count(case when pl.Is_Like = 1 then 1 else null end ) as TotalLike , count(case when pl.Is_Like = 2 then 2 else null end ) as TotalUnLike, sa.Views_Count'
			)
		);
		
		$query->from($db->quoteName('#__eposts_circulars').' AS p');
		$query->join('LEFT', $db->quoteName('#__like_circulars', 'pl') . ' ON (' . $db->quoteName('p.id') . ' = ' . $db->quoteName('pl.Item_Id') . ')');
		$query->join('LEFT', $db->quoteName('#__site_analytics', 'sa') . ' ON (' . $db->quoteName('sa.Item_ID') . ' = ' . $db->quoteName('p.Id') . ')');
		
		$state_department = $this->getState('filter.state.department');
		if ($state_department!=0) {
			$query->where('p.circulars_department = '.$state_department);
		}

		
		// Filter the items over the search string if set.
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
				$query->where(
					'('.$db->quoteName('p.circulars_name').' LIKE '.$search .')'
				);
			}
		}
		$query->group('p.circulars_name');
		$query->order('p.id DESC');
		return $query;
						
		}else if($state==5) {
			
		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
			
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'pl.id,p.media_name As page_name , count(case when pl.Is_Like = 1 then 1 else null end ) as TotalLike , count(case when pl.Is_Like = 2 then 2 else null end ) as TotalUnLike, sa.Views_Count'
			)
		);
	
		$query->from($db->quoteName('#__eposts_dwnmedias').' AS p');
		$query->join('LEFT', $db->quoteName('#__like_forms', 'pl') . ' ON (' . $db->quoteName('p.id') . ' = ' . $db->quoteName('pl.Item_Id') . ')');
		$query->join('LEFT', $db->quoteName('#__site_analytics', 'sa') . ' ON (' . $db->quoteName('sa.Item_ID') . ' = ' . $db->quoteName('p.Id') . ')');
		
		
		$state_department = $this->getState('filter.state.department');
		if ($state_department!=0) {
			$query->where('p.media_department = '.$state_department);
		}


		// Filter the items over the search string if set.
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
				$query->where(
					'('.$db->quoteName('p.media_name').' LIKE '.$search .')'
				);
			}
		}
		$query->group('p.media_name');
		$query->order('p.id DESC');
		
		return $query;
						
		} else if($state==6) {
			
			// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
			
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'pl.id,p.filename As page_name , count(case when pl.Is_Like = 1 then 1 else null end ) as TotalLike , count(case when pl.Is_Like = 2 then 2 else null end ) as TotalUnLike, sa.Views_Count'
			)
		);

		$query->from($db->quoteName('#__igallery_img').' AS p');
		$query->join('LEFT', $db->quoteName('#__like_images', 'pl') . ' ON (' . $db->quoteName('p.id') . ' = ' . $db->quoteName('pl.Item_Id') . ')');
		$query->join('LEFT', $db->quoteName('#__site_analytics', 'sa') . ' ON (' . $db->quoteName('sa.Item_ID') . ' = ' . $db->quoteName('p.gallery_id') . ')');
		
		
		$state_department = $this->getState('filter.state.department');
	
		if ($state_department!=0) {
			$query->where('p.department = '.$state_department);
		}	


		// Filter the items over the search string if set.
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
				$query->where(
					'('.$db->quoteName('p.filename').' LIKE '.$search .')'
				);
			}
		}
		$query->group('p.filename');
		$query->order('p.id DESC');
		
		return $query;
						
		}else if($state==7) {
			
		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
			
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'pl.id,p.dms_name As page_name , count(case when pl.Is_Like = 1 then 1 else null end ) as TotalLike , count(case when pl.Is_Like = 2 then 2 else null end ) as TotalUnLike, sa.Views_Count'
			)
		);
		
		$query->from($db->quoteName('#__eposts_dms').' AS p');
		$query->join('LEFT', $db->quoteName('#__like_documents', 'pl') . ' ON (' . $db->quoteName('p.id') . ' = ' . $db->quoteName('pl.Item_Id') . ')');
		$query->join('LEFT', $db->quoteName('#__site_analytics', 'sa') . ' ON (' . $db->quoteName('sa.Item_ID') . ' = ' . $db->quoteName('p.Id') . ')');
		
		
		$state_department = $this->getState('filter.state.department');
	
		if ($state_department!=0) {
			$query->where('p.dms_department = '.$state_department);
		}

		// Filter the items over the search string if set.
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
				$query->where(
					'('.$db->quoteName('p.dms_name').' LIKE '.$search .')'
				);
			}
		}
		$query->group('p.dms_name');
		$query->order('p.id DESC');
		return $query;
						
		}
	}
}
