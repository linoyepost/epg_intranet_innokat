<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_redirect
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$user		= JFactory::getUser();
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$current_state = $this->state->get('filter.state');

?>

<form action="<?php echo JRoute::_('index.php?option=com_likepages&view=likes'); ?>" method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
			<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_LIKEPAGES_SEARCH_Likes'); ?>" />
			<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
		</div>
			
        <div class="filter-select fltrt">
			<select name="filter_state_department" class="inputbox" onchange="this.form.submit()">
				<?php echo JHtml::_('select.options', LikePagesHelper::DepartmentOptions(), 'value', 'text', $this->state->get('filter.state.department'), true);?>
			</select>
		</div>
		<div class="filter-select fltrt">
			<select name="filter_state" class="inputbox" onchange="this.form.submit()">
				
				<?php echo JHtml::_('select.options', LikePagesHelper::publishedOptions(), 'value', 'text', $this->state->get('filter.state'), true);?>
			</select>
		</div>
	</fieldset>
	<div class="clr"> </div>

	<table class="adminlist">
		<thead>
			<tr>
				<th width="2%">
					<?php echo JText::_('Sr.No'); ?>
				</th>
				<th class="title"  width="20%">
					<?php  echo JText::_('PAGE NAME'); //echo JHtml::_('grid.sort', 'PAGE_NAME', 'page_name'); ?>
				</th>
				<!--<th width="25%">
					<?php //echo JHtml::_('grid.sort', 'PAGE_URL', 'a.Page_Url'); ?>
				</th>-->
                <th width="3%">
                	<?php echo JText::_('Total Likes'); ?>
                </th>
                 <th width="3%">
                	<?php echo JText::_('Total Dislikes'); ?>
                </th>
		<th width="3%">
                	<?php 
                	if($current_state==2 || $current_state==4 || $current_state==5 || $current_state== 7 ){
                		echo JText::_('Totalt Downloads'); 	
                	}else {
                		echo JText::_('Totalt Views'); 	
                	}
                 ?>
                </th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="8">
					<?php echo $this->pagination->getListFooter(); ?>
					<p class="footer-tip">
						<?php if ($this->enabled) : ?>
							<span class="enabled"><?php //echo JText::_('COM_LIKEPAGES_PLUGIN_ENABLED'); ?></span>
						<?php else : ?>
							<span class="disabled"><?php //echo JText::_('COM_LIKEPAGES_PLUGIN_DISABLED'); ?></span>
						<?php endif; ?>
					</p>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php $j = 1; foreach ($this->items as $i => $item) :
		
			?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo $j++; ?>
				</td>
				<td>
					<?php echo $this->escape($item->page_name); ?>
				</td>
                <td  class="center">
                	<?php echo $this->escape($item->TotalLike); ?>
                </td>
                <td  class="center">
                	<?php echo $this->escape($item->TotalUnLike); ?>
                </td>
		<td  class="center">
                	<?php echo ($this->escape($item->Views_Count)!= null ? $this->escape($item->Views_Count) : 0   ); ?>
                </td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
