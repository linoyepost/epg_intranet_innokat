<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Redirect component helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_likepages
 * @since		1.6
 */
class LikePagesHelper
{
	public static $extension = 'com_likepages';

	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	The name of the active view.
	 */
	public static function addSubmenu($vName)
	{
		// No submenu for this component.
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 */
	public static function getActions()
	{
		$user		= JFactory::getUser();
		$result		= new JObject;
		$assetName	= 'com_likepages';

		$actions = JAccess::getActions($assetName);

		foreach ($actions as $action) {
			$result->set($action->name,	$user->authorise($action->name, $assetName));
		}

		return $result;
	}

	/**
	 * Returns an array of standard published state filter options.
	 *
	 * @return	string			The HTML code for the select tag
	 */
	public static function publishedOptions()
	{
		// Build the active state filter options.
		$options	= array();
		$options[]	= JHtml::_('select.option', '0', 'Pages');
		$options[]	= JHtml::_('select.option', '1', 'News');
		$options[]	= JHtml::_('select.option', '2', 'Policies');
		$options[]	= JHtml::_('select.option', '3', 'Announcements');
		$options[]	= JHtml::_('select.option', '4', 'Circulars');
		$options[]	= JHtml::_('select.option', '5', 'Forms');
		$options[]	= JHtml::_('select.option', '6', 'Images');
		$options[]	= JHtml::_('select.option', '7', 'Documents');
		
		return $options;
	}

	public static function DepartmentOptions()
	{
		$database		= &JFactory::getDBO();
		$query = "SELECT id, departments_name FROM #__eposts_departments";
		$database->setQuery( $query );
		$src_list = $database->loadObjectList();
			$options1[]	= JHtml::_('select.option', '0', 'Select Department');
		foreach($src_list as $department){
			$options1[] = JHTML::_('select.option', $department->id, $department->departments_name );
		}
		
		return $options1;
	}

	/**
	 * Determines if the plugin for Redirect to work is enabled.
	 *
	 * @return	boolean
	 */
	public static function isEnabled()
	{
		//Nothing 
	}
}
