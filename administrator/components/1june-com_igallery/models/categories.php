<?php
defined( '_JEXEC' ) or die();

jimport('joomla.application.component.modellist');

class igalleryModelcategories extends JModelList
{
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		$search = $app->getUserStateFromRequest($this->context.'.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context.'.published', 'filter_published', '');
		$this->setState('filter.published', $published);
		
		parent::populateState('ordering', 'asc');
	}
	
	function getListQuery()
	{
		$query = 'SELECT i.id, i.ordering, i.name, i.profile, i.parent, '.
		'i.user, i.published, i.moderate, i.date, i.publish_up, i.publish_down, u.name AS name_of_user, '.
		'p.name AS profile_name from #__igallery as i '.
		'INNER JOIN `#__users` AS u ON u.id = i.user '.
		'INNER JOIN `#__igallery_profiles` AS p ON p.id = i.profile '.
		'ORDER BY i.id DESC';
		
		return $query;
	}
    
    function getItems()
	{
		$app = JFactory::getApplication();
		$query = $this->getListQuery();
		$user =& JFactory::getUser();
		
		$this->_db->setQuery($query);
		$categories = $this->_db->loadObjectList();
		
		for($i=0; $i<count($categories); $i++)
		{
			$categories[$i]->parent_id = $categories[$i]->parent;
			$categories[$i]->title = null;
		}
		
		$children = array();
    
    	foreach ($categories as $category )
    	{
    		$parent = $category->parent;
    		$list = @$children[$parent] ? $children[$parent] : array();
    		array_push($list, $category);
    		$children[$parent] = $list;
    	}
    
    	$list = JHTML::_('menu.treerecurse',  0, '', array(), $children, max( 0, 9 ) );
    	
    	$search = $this->getState('filter.search');

    	if(strpos($search, '"') !== false)
    	{
			$search = str_replace(array('=', '<'), '', $search);
		}
		$search = JString::strtolower($search);
		
		if($search)
		{
			$query = 'SELECT id FROM #__igallery WHERE '.
			'LOWER(name) LIKE '.$this->_db->Quote( '%'.$this->_db->getEscaped( $search, true ).'%', false );
			$this->_db->setQuery( $query );
			$search_rows = $this->_db->loadResultArray();
			
			$list1 = array();

			foreach ($search_rows as $sid )
			{
				foreach ($list as $item)
				{
					if ($item->id == $sid) 
					{
						$list1[] = $item;
					}
				}
			}
			
			$list = $list1;
		}
		
		$filter_state = $this->getState('filter.published');
		
		if( is_numeric($filter_state) )
		{
			$publishedWhere = '';
			if($filter_state == 1)
			{
				$publishedWhere = 'published = 1';
			}
			else if($filter_state == 0 )
			{
				$publishedWhere = 'published = 0';
			}
			
			$query = 'SELECT id FROM #__igallery WHERE '.$publishedWhere;
			$this->_db->setQuery($query);
			$published_rows = $this->_db->loadResultArray();
			
			$list2 = array();

			foreach ($published_rows as $sid )
			{
				foreach ($list as $item)
				{
					if ($item->id == $sid) 
					{
						$list2[] = $item;
					}
				}
			}
			
			$list = $list2;
		}
		
		if( JFactory::getApplication()->isSite() )
		{
			foreach ($list as $key => $value)
			{
				$editOk = igGeneralHelper::authorise('core.edit', $value->id);
				$editOwn = igGeneralHelper::authorise('core.edit.own', $value->id) && $value->user == $user->id;
				if(!$editOk && !$editOwn)
				{
					unset($list[$key]);
				}
			}
		}
			
        $orderedList = array_values($list);
        
        $limit = $this->getState('list.limit') == 0 ? 9999 : $this->getState('list.limit');
        
			$items = array_slice( $orderedList, $this->getState('list.start'), $limit );
		
        return $items;
	}
}	