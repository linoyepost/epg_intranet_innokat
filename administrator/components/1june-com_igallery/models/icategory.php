<?php
defined('_JEXEC') or die( 'Restricted access' );

class igalleryModelicategory extends igalleryModelBase
{
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			if(empty($item->id))
			{
				$params =& JComponentHelper::getParams('com_igallery');
				
				$item->published = $params->get('cat_published', 0);
				$item->parent    = $params->get('default_parent', 0);
				$item->profile   = $params->get('default_profile', 0);
			}
			
			$item->remove_menu_image = 0;
		}
		
		return $item;
	}
	
	public function getForm($data = array(), $loadData = true)
	{
		JForm::addFieldPath(IG_ADMINISTRATOR_COMPONENT.DS.'models'.DS.'fields');
		$app	= JFactory::getApplication();
		$form = $this->loadForm('com_igallery.category', IG_ADMINISTRATOR_COMPONENT.DS.'models'.DS.'forms'.DS.'category.xml', array('control' => 'jform', 'load_data' => $loadData));
		
		if( empty($form) )
		{
			return false;
		}
		
		return $form;
	}
	
	protected function loadFormData()
	{
		$data = $this->getItem();
		return $data;
	}
	
	public function getTable($type = 'igallery', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	function checkProfileExists()
	{
	    $query = 'SELECT id FROM #__igallery_profiles';
		$this->_db->setQuery($query);
		$this->_db->query();
		$numRows = $this->_db->getNumRows();
	    if($numRows == 0)
		{
		    return false;
		}
		
		return true;
	}
	
	protected function getReorderConditions($table = null)
	{
		$condition = array();
		$condition[] = 'parent = '.(int)$table->parent;
		
		return $condition;
	}
	
	function save($data)
	{
		$row =& $this->getTable('igallery');
		$user =& JFactory::getUser();
		$app = JFactory::getApplication();
		$params = JComponentHelper::getParams('com_igallery');
		$isSite =  JFactory::getApplication()->isSite();
		
		if (!$row->bind($data)) 
		{
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
			$query = 'SELECT menu_image_filename FROM #__igallery WHERE id = '.(int)$data['id'];
            $this->_db->setQuery($query);
            $category = $this->_db->loadObject();
		
		if(strlen($_FILES['jform']['name']['upload_image']) > 2 )
		{
			$fileName = $_FILES['jform']['name']['upload_image'];
			$tmpPath = $_FILES['jform']['tmp_name']['upload_image'];
			$uploadError = $_FILES['jform']['error']['upload_image'];
			
			if(!$fileArray = igFileHelper::processUploadedImage($fileName, $tmpPath, $uploadError, 'igallery', true) )
			{
				return false;
			}
			
			$row->menu_image_filename = $fileArray['filename'];
			
			$params =& JComponentHelper::getParams('com_igallery');
			
			if($params->get('import_exif_data', 0) == 1)
			{
				$row->menu_description = $fileData['exif_des'];
			}
		}

		if(empty($row->id) ) 
		{
		    $firstLast = $params->get('new_cat_ordering', 'last');
		    $row->ordering = $firstLast == 'first' ? 0 : $row->getNextOrder('parent = '.(int)$row->parent);
		    
		    $row->profile = empty($row->profile) ? $params->get('default_profile', 1) : $row->profile;
		    $row->parent = empty($row->parent) ? $params->get('default_parent', 0) : $row->parent;
		    
		}
		
		$row->moderate = $params->get('moderate_cat', 0) == 0 || $isSite == false ? 1 : 0;
		$row->user = empty($row->user) ? $user->id : $row->user;
		$row->alias = empty($row->alias) ? JFilterOutput::stringURLSafe($row->name) : JFilterOutput::stringURLSafe($row->alias);
		$row->name = empty($row->name) ? '____' : $row->name;
		
		if($data['remove_menu_image'] == 1)
		{
		    $query = 'SELECT menu_image_filename FROM #__igallery WHERE menu_image_filename = '.$this->_db->Quote($category->menu_image_filename);
    		$this->_db->setQuery($query);
    		$this->_db->query();
    		$numRows = $this->_db->getNumRows();
    		$deleteImg = $numRows > 1 ? false: true;
    		
    		if($deleteImg)
    		{
    	       //igFileHelper::deleteImage($category->menu_image_filename, $deleteImg);
			   unlink(IG_IMAGE_HTML_ORIG.'1-100\\'.$category->menu_image_filename);
    		}
    	    
    	    $row->menu_image_filename =  '';
		}
		
		if(!$row->store()) 
		{
			$this->setError( $this->_db->getErrorMsg() );
			return false;
		}else {
		  if(strlen($_FILES['jform']['name']['upload_image']) > 2  && !empty($row->id))
		  {
			  unlink(IG_IMAGE_HTML_ORIG.'1-100\\'.$category->menu_image_filename);
		  }
		}
		
		if(trim(str_replace('-','',$row->alias)) == '' )
		{
			$row->alias = 'category-'.(int)$row->id;
			if(!$row->store()) 
			{
				$this->setError( $this->_db->getErrorMsg() );
				return false;
			}
		}
		
		$query = 'SELECT alias from #__igallery where alias = "'.$this->_db->getEscaped($row->alias).'"';
		$this->_db->setQuery($query);
        $rows = $this->_db->loadObjectList();
        
        if( count($rows) > 1)
        {
        	$row->alias = $row->alias.'-'.(int)$row->id;
			if(!$row->store()) 
			{
				$this->setError( $this->_db->getErrorMsg() );
				return false;
			}
        }
		
		if($firstLast == 'first')
	    {
	       $row->reorder('parent = '.(int)$row->parent);
	    }
	    
	    if($params->get('notify_new_category', 0) == 1 && $isSite == true)
	    {
	    	$siteConfig =& JFactory::getConfig();
			$from = $siteConfig->getValue('config.mailfrom');
			$fromname = $siteConfig->getValue('config.fromname');
			$recipient = explode(',', $params->get('notify_emails', '') );
	    	$subject = $siteConfig->getValue('config.sitename').' : '.JText::_('NEW_CATEGORY_ADDED');
			
			$body   = 
			JText::_('JGLOBAL_USERNAME').': '.$user->name." \n\n ".
			JText::_('JCATEGORY').': '.$row->name." \n\n ".
			IG_HOST.'administrator/index.php?option=com_igallery&view=icategory&id='.$row->id." \n\n ".
			IG_HOST.'index.php?option=com_igallery&view=category&igid='.$row->id;
			
		    for($i=0; $i<count($recipient); $i++)
			{
				JUtility::sendMail($from, $fromname, $recipient[$i], $subject, $body);
				if($i > 5){break;}
			}
	    }
	    	
		return true;
	}
	
	function delete($cid)
	{
		
		for($i = 0; $i<count($cid); $i++){  // count albums
		$id = is_array($cid) ? (int)$cid[$i] : (int)$cid;
				
		$query = 'SELECT * FROM #__igallery WHERE id = '.(int)$id;
        $this->_db->setQuery($query);
        $category = $this->_db->loadObject();
        
		if( strlen($category->menu_image_filename) > 2 )
        {
            $query = 'SELECT menu_image_filename FROM #__igallery WHERE menu_image_filename = '.$this->_db->Quote($category->menu_image_filename);
    		$this->_db->setQuery($query);
    		$this->_db->query();
    		$numRows = $this->_db->getNumRows();
    		$deleteOrig = $numRows > 1 ? false: true;
			//echo $category->menu_image_filename;
			//exit;
    		
            igFileHelper::deleteImage($category->menu_image_filename, $deleteOrig);
        }
        
        $query = 'SELECT id, filename FROM #__igallery_img WHERE gallery_id = '.(int)$id;
        $this->_db->setQuery($query);
        $photoList = $this->_db->loadObjectList();
        
        for($i=0; $i<count($photoList); $i++)
		{
		    $photo =& $photoList[$i];
		    
		    $query = 'SELECT filename FROM #__igallery_img WHERE filename = '.$this->_db->Quote($photo->filename);
    		$this->_db->setQuery($query);
    		$this->_db->query();
    		$numRows = $this->_db->getNumRows();
    		$deleteOrig = $numRows > 1 ? false: true;
    		
    	    igFileHelper::deleteImage($photo->filename, $deleteOrig);
            
    		$query = 'DELETE FROM #__igallery_img WHERE id = '.(int)$photo->id;
    		$this->_db->setQuery($query);
    		if(!$this->_db->query())
    		{
    			$this->setError($this->_db->getErrorMsg());
    			return false;
    		}
    		
		}
	    
		$query = 'DELETE FROM #__igallery WHERE id = '.(int)$id;
		$this->_db->setQuery($query);
		
		if(!$this->_db->query())
		{
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		$query = 'UPDATE #__igallery SET parent = 0 WHERE parent = '.(int)$id;
		$this->_db->setQuery($query);
		if(!$this->_db->query())
		{
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		} //end for($i = 0; $i<count($cid); $i++){  // count albums
		
		$row =& $this->getTable('igallery');
    	$row->reorder('parent = '.(int)$category->parent);
		
		return true;
	}
	
	function copy()
	{
		$cid = JRequest::getVar('cid', array(0), '', 'array');
		$id = (int)$cid[0];
		
	    $row =& $this->getTable('igallery');
        $row->load($id);
        
        $row->id = null;
        $row->ordering = $row->getNextOrder('parent = '.(int)$row->parent);
        $row->name = JText::_('COPY_OF').' '.$row->name;
        $row->alias = JFilterOutput::stringURLSafe($row->name);
        
        if (!$row->store())
    	{
    		JError::raise(2, 500, $row->getError());
    		return false;
    	}
    	
    	$catId = $this->_db->insertid();
    	
    	$query = 'SELECT * FROM #__igallery_img WHERE gallery_id = '.(int)$id;
		$photos = $this->_getList($query);
		
		$row =& $this->getTable('igallery_img');
		
		for($i=0; $i<count($photos); $i++)
		{
		    $photo =& $photos[$i];
		    
            $row->load($photo->id);
            $row->id = null;
            $row->gallery_id = $catId;
		   
            if(!$row->store())
            {
               JError::raise(2, 500, $row->getError());
               return false;
            }
		}
		
		return true;
	}
	
	function moderate($moderate)
	{
	    $cid = JRequest::getVar( 'cid', array(), '', 'array' );
		JArrayHelper::toInteger($cid);
		
		if (count($cid))
		{
			$cids = implode( ',', $cid );

			$query = 'UPDATE #__igallery SET moderate = '.(int)$moderate
			. ' WHERE id IN ( '.$cids.' )';
			$this->_db->setQuery($query);
			if (!$this->_db->query()) 
			{
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		return true;
	}
	
}