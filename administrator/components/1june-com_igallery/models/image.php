<?php
defined('_JEXEC') or die( 'Restricted access' );

class igalleryModelimage extends igalleryModelBase
{
	public function getItem($pk = null)
	{
		$item = parent::getItem($pk);
		return $item;
	}
    
	function getPhoto($id)
	{
		$query = 'SELECT * FROM #__igallery_img WHERE id = '. (int)$id;
		$this->_db->setQuery($query);
		$photo = $this->_db->loadObject();
		return $photo;
	}
	
	public function getForm($data = array(), $loadData = true)
	{
		$app	= JFactory::getApplication();
		
		$form = $this->loadForm('com_igallery.image', IG_ADMINISTRATOR_COMPONENT.DS.'models'.DS.'forms'.DS.'image.xml', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		
		return $form;
	}
	
	protected function loadFormData()
	{
		$data = $this->getItem();
		return $data;
	}
	
	public function getTable($type = 'igallery_img', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	protected function getReorderConditions($table = null)
	{
		$condition = array();
		$condition[] = 'gallery_id = '.(int)$table->gallery_id;
		
		return $condition;
	}
	
	function store($fileData)
	{
	    $row =& $this->getTable('igallery_img');
		$user =& JFactory::getUser();
		$configArray =& JComponentHelper::getParams('com_igallery');
		$app =& JFactory::getApplication();
		$isSite =  $app->isSite();
		
		$row->id = null;
        $row->user = $user->id;
		$row->target_blank = $configArray->get('target_blank', 1);
		$row->access = $configArray->get('new_image_access', 1);
		$row->published = $configArray->get('new_image_published', 1);
		$row->gallery_id = JRequest::getInt('catid',0);
		$row->filename = $fileData['filename'];
		$row->moderate = $configArray->get('moderate_img', 0) == 0 || $isSite == false ? 1 : 0;

		$firstLast = $configArray->get('new_image_ordering', 'last');
		$row->ordering = $firstLast == 'first' ? 0 : $row->getNextOrder('gallery_id = '.JRequest::getInt('catid',0) );
		
		if( isset($fileData['exif_des']) )
		{
			$row->description = strlen($fileData['exif_des']) > 0 ? $fileData['exif_des'] : $row->description;
		}
		
		if( isset($fileData['iptc_des']) )
		{
			$row->description = strlen($fileData['iptc_des']) > 0 ? $fileData['iptc_des'] : $row->description;
		}
			
        if( !$row->store() )
		{
		  	echo $row->getError();
		  	return false;
		}
		

		if($firstLast == 'first')
	    {
	       $row->reorder('gallery_id = '.(int)$row->gallery_id );
	    }
	    
	    $sessionValue = $app->getUserState('com_igallery.moderateimg'.$row->gallery_id, 0);
	   
	    if( $isSite && empty($sessionValue) )
	    {
			if($configArray->get('notify_new_image', 0) == 1)
			{
				$siteConfig =& JFactory::getConfig();
				$from = $siteConfig->getValue('config.mailfrom');
				$fromname = $siteConfig->getValue('config.fromname');
				$recipient = explode(',', $configArray->get('notify_emails', '') );
		    	$subject = $siteConfig->getValue('config.sitename').' : '.JText::_('NEW_IMAGES_ADDED');
				
				$body   = 
				JText::_('JGLOBAL_USERNAME').': '.$user->name." \n\n ".
				IG_HOST.'administrator/index.php?option=com_igallery&view=images&catid='.$row->gallery_id." \n\n ".
				IG_HOST.'index.php?option=com_igallery&view=category&igid='.$row->gallery_id;
				
			    for($i=0; $i<count($recipient); $i++)
				{
					JUtility::sendMail($from, $fromname, $recipient[$i], $subject, $body);
					if($i > 5){break;}
				}
				
				$app->setUserState( 'com_igallery.moderateimg'.$row->gallery_id, 1);
			}
	    }
	    
	    return true;
	}
	
	function delete($cid)
	{
		JArrayHelper::toInteger($cid);
		
		for($i=0; $i<count($cid); $i++)
		{
		    $query = 'SELECT filename FROM #__igallery_img WHERE id = '.(int)$cid[$i];
            $this->_db->setQuery($query);
            $photo = $this->_db->loadObject();
            
		
    		$query = 'SELECT filename FROM #__igallery_img WHERE filename = '.$this->_db->Quote($photo->filename);
    		$this->_db->setQuery($query);
    		$this->_db->query();
    		$numRows = $this->_db->getNumRows();
    		$deleteOrig = $numRows > 1 ? false: true;
    		
    	    igFileHelper::deleteImage($photo->filename, $deleteOrig);

    		$query = 'DELETE FROM #__igallery_img WHERE id = '.(int)$cid[$i];
    		$this->_db->setQuery($query);
    		if(!$this->_db->query())
    		{
    			$this->setError($this->_db->getErrorMsg());
    			return false;
    		}
    	}
		
		$row =& $this->getTable('igallery_img');
    	$row->reorder('gallery_id = '.JRequest::getInt('catid',0) );
		
		return true;
	}
	
	function copy_move()
	{
	    $targetCat = JRequest::getInt('cat_id_copy_move');
	    if($targetCat == 0)
        {
            JError::raise(2, 500, JText::_('JERROR_NO_ITEMS_SELECTED') );
            return false; 
        }
        
        $cid = JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);
		
		$copyMove = JRequest::getWord('copy_move','copy');
		
		$row =& $this->getTable('igallery_img');
        
        for($i=0; $i<count($cid); $i++)
		{
            $row->load( (int)$cid[$i]);
            $origId = $row->id;
            
            $row->id = null;
            $row->gallery_id = (int)$targetCat;
            $row->ordering = $row->getNextOrder('gallery_id = '.(int)$targetCat );
		   
            if(!$row->store())
            {
               $this->setError($this->_db->getErrorMsg());
               return false;
            }

            if($copyMove == 'move')
            {
                $query = 'DELETE FROM #__igallery_img WHERE id = '.(int)$origId;
        		$this->_db->setQuery($query);
        		if(!$this->_db->query())
        		{
        			$this->setError($this->_db->getErrorMsg());
        			return false;
        		}
            }
		}
		return true;
	}
	
	function save($data)
	{
	    $row =& $this->getTable('igallery_img');
	    
	    if (!$row->bind($data)) 
		{
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		$row->alt_text = htmlspecialchars($row->alt_text, ENT_QUOTES);

		if (!$row->store()) 
		{
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		$query = 'SELECT gallery_id, ordering from #__igallery_img WHERE id = '.(int)JRequest::getInt('id',0);;
    	$this->_db->setQuery($query);
    	$currentRow = $this->_db->loadObject();
		
		$nextOrdering = $currentRow->ordering + 1;
		$query = 'SELECT id from #__igallery_img WHERE gallery_id = '.(int)$currentRow->gallery_id.' AND ordering = '.(int)$nextOrdering.' LIMIT 1';
    	$this->_db->setQuery($query);
    	$nextRow = $this->_db->loadObject();
    	
		return $nextRow->id;
	}
	
	function rotate()
	{
		$id = JRequest::getInt('id', 0);
		$rvalue = JRequest::getInt('rvalue', 0);
		
		$row =& $this->getTable('igallery_img');
		$row->load( (int)$id );
		
		if($rvalue == 0)
		{
			$row->rotation = $row->rotation == 0 ? 270 : $row->rotation - 90;
		}
		else
		{
			$row->rotation = $row->rotation == 270 ? 0 : $row->rotation + 90;
		}
		
		if (!$row->store()) 
		{
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		return true;
	}
	
	function assignMenuImage()
	{
		$id = JRequest::getInt('id', 0);
		$catid = JRequest::getInt('catid', 0);
		$cvalue = JRequest::getInt('cvalue', 0);
		
		$query = 'UPDATE #__igallery_img SET menu_image = 0 WHERE gallery_id = '.(int)$catid;
		$this->_db->setQuery($query);
		if(!$this->_db->query())
    	{
    		$this->setError($this->_db->getErrorMsg());
    		return false;
    	}
    	
		$query = 'UPDATE #__igallery_img SET menu_image = '.(int)$cvalue.' WHERE id = '.(int)$id;
		$this->_db->setQuery($query);
		if(!$this->_db->query())
    	{
    		$this->setError($this->_db->getErrorMsg());
    		return false;
    	}
		
    	return true;
	}
	
	function moderate($moderate)
	{
	    $cid = JRequest::getVar( 'cid', array(), '', 'array' );
		JArrayHelper::toInteger($cid);
		
		if (count($cid))
		{
			$cids = implode( ',', $cid );

			$query = 'UPDATE #__igallery_img SET moderate = '.(int)$moderate
			. ' WHERE id IN ( '.$cids.' )';
			$this->_db->setQuery($query);
			if (!$this->_db->query()) 
			{
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		return true;
	}
		
}