<?php
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

class JFormFieldEPDepartment extends JFormField
{
	protected $type = 'epdepartment';

	protected function getInput()
	{
		$user = &JFactory::getUser();
		
		$selected = $this->form->getValue('department');
		
		$database = &JFactory::getDBO();
		$database->setQuery("SELECT id,departments_name "
			. "\n FROM #__eposts_departments "
			. "\n WHERE departments_members LIKE '%".$user->id."%' "
			. "\n ORDER BY departments_name ASC"
		);
		$listDepartments = $database->loadObjectList();

		//$cdipartment[]   = JHTML::_('select.option', '0', JText::_('COM_IGALLERY_DEPART_SELECT'));
		
		foreach($listDepartments as $xdepartment) {
			$cdipartment[] = JHTML::_('select.option', $xdepartment->id, $xdepartment->departments_name);
		}
		
	
		$departmentList = JHTML::_('select.genericlist', $cdipartment, $this->name, 'class="inputbox" ', 'value', 'text', $selected);
	   
	    return $departmentList;
	}
}