<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

function com_uninstall()
{
	jimport('joomla.filesystem.file');
	jimport('joomla.filesystem.folder');
	require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_igallery'.DS.'defines.php');
	
	if ( JFolder::exists(IG_IMAGE_PATH) )
	{
		JFolder::delete(IG_IMAGE_PATH); 
	}
}
?>