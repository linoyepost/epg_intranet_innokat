<?php
defined('_JEXEC') or die('Restricted access');

class igHtmlHelper
{
    function getCategorySelect($name, $key, $value, $exclude, $selectText, $top, $size, $submit, $active, $tree=true)
	{
	    $db	=& JFactory::getDBO();

	    $where = empty($exclude) ? '' : ' WHERE id != '.(int)$exclude;

	    $query = 'SELECT * FROM #__igallery '.$where.' ORDER BY parent, ordering';
		$db->setQuery($query);
		$categories = $db->loadObjectList();
		
		for($i=0; $i<count($categories); $i++)
		{
			$categories[$i]->parent_id = $categories[$i]->parent;
			$categories[$i]->title = null;
		}

		$children = array();

		if( !empty($categories) )
		{
			if($tree)
			{
				foreach($categories as $category)
				{
					$parent = $category->parent;

					if(@$children[$parent])
					{
						$list = $children[$parent];
					}
					else
					{
						$list = array();
					}

					array_push($list,$category);

					$children[$parent] = $list;
				}
			}
		}

		if($tree)
		{
			$menuTree = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0 );
		}
		else
		{
			for($i=0; $i<count($categories); $i++)
			{
				$categories[$i]->treename = $categories[$i]->name;
			}
			$menuTree = $categories;
		}

		$selectOptions     = array();

		if($selectText)
		{
		    $selectOptions[]   = JHTML::_('select.option', '0', JText::_('JOPTION_SELECT_CATEGORY'), $key, $value);
		}

		if($top)
		{
		    $selectOptions[]   = JHTML::_('select.option', '0', JText::_('JGLOBAL_TOP'), $key, $value );
		}

		foreach($menuTree as $branch)
		{
			$selectOptions[] = JHTML::_('select.option', $branch->id, $branch->name, $key, $value);
		}

		$html = 'class="inputbox" size="'.$size.'" ';
        $javascript = $submit == true ? 'onchange="document.adminForm.submit();"' : '';

		$parentSelectList = JHTML::_("select.genericlist", $selectOptions, $name, $html.$javascript, $key, $value, $active );
		
		return $parentSelectList;
	}
	
	function moderateImage($row, $i, $prefix, $frontend)
	{
		$img 	= $row->moderate ? 'apply.png' : 'revert.png';
		$task 	= $row->moderate  ? $prefix.'.unmoderate' : $prefix.'.moderate';
		$alt 	= $row->published ? JText::_( 'APPROVE' ) : JText::_( 'UNAPPROVE' );

		$href = '';
		if(!$frontend)
		{
			$href .= '<a class="jgrid" href="javascript:void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $task .'\')" title="'. $alt .'">';
		}

		$href .= '<img src="'.IG_IMAGE_ASSET_PATH.'menu'.'/'.$img.'" border="0" alt="'. $alt .'" style="float: none;" />';

		if(!$frontend)
		{
			$href .= '</a>';
		}

		return $href;
	}

	function addSubmenu()
	{
		$vName = JRequest::getCmd('view', 'categories');
		
		JSubMenuHelper::addEntry(JText::_('COM_IGALLERY_CATEGORIES'),
		'index.php?option=com_igallery&view=categories', $vName == 'categories');
		
		JSubMenuHelper::addEntry(JText::_('IMAGES'),
		'index.php?option=com_igallery&view=images', $vName == 'images');
		
		if(igGeneralHelper::authorise('core.admin'))
		{
			JSubMenuHelper::addEntry(JText::_('PROFILES'),
			'index.php?option=com_igallery&view=profiles', $vName == 'profiles');
		}
	}
	
}