<?php
defined('_JEXEC') or die('Restricted access');

class igUploadHtml5form
{
	function html5formHeadJs()
	{
		return;
	}
	
	function html5formHTML()
	{
		$params =& JComponentHelper::getParams('com_igallery');
		$moderate = $params->get('moderate_img', 0) == 1 && JFactory::getApplication()->isAdmin() == false ? 1 : 0;
		
		?>
        <script>
		function validate_file(){
			var form = document.browserUploaderForm;
			var filefield = form.uploads;
			/*if (filefield.value == '') {
				alert('Please select some Image.');
				return false;
			}*/
			if (document.getElementById("upload_files").files.length < 1) {
				alert('Please select some Image.');
				return false;
			}
			return true;
		}
        </script>
		<form action="index.php?option=com_igallery" method="post" name="browserUploaderForm" enctype="multipart/form-data" onsubmit="return validate_file();">
		
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'JLIB_HTML_BEHAVIOR_UPLOADER_CURRENT_TITLE' ); ?></legend>
		
		<input name='uploads[]' id="upload_files" type="file" multiple="true"/>
		<!--<input name='uploads' type="file"/>-->
		<input type="hidden" name="task" value="images.browserUpload" />

		<input type="hidden" name="catid" value="<?php echo JRequest::getInt('catid',0); ?>" />
		<input type="hidden" name="moderateMsg" value="<?php echo $moderate; ?>" />
		<input type="hidden" name="Itemid" value="<?php echo JRequest::getInt('Itemid'); ?>" />
		
		<div class="clr"></div>
		<input type="submit" name="submit" value="<?php echo JText::_( 'JTOOLBAR_UPLOAD' ); ?>" />
		<div class="clr"></div>
		<p><?php echo JText::_( 'HOLD_CONTROL_SHIFT' ); ?></p>
		</fieldset>
		</form>
		<?php
	}
}