<?php defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.framework', true);
JHTML::_('behavior.calendar');
JHtml::_('behavior.formvalidation');
global $jlistConfig; 
$config =& JFactory::getConfig();
$storgae_server_path = $config->getValue('config.storgae_server_path');
$filepath = $config->getValue( 'config.file_path' );
$document =& JFactory::getDocument();

$document->addScript(JURI::base().'components/com_eposts/eposts.js');

if($this->isSite == true)
{
	echo JToolBar::getInstance('toolbar')->render('toolbar');
}
?>
<script type="text/javascript">
var iToolbarIds = ['toolbar-apply','toolbar-save','toolbar-cancel'];
iToolbarIds.each(function(id, index)
{
	if( $chk( document.id(id) ) )
	{
		document.id(id).getElement('a').set('href', 'javascript: void(0)');
	}	
});	

</script>


<div style="clear: both"></div>
<form action="index.php?option=com_igallery" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
	<?php if($this->isSite == false) : ?>
    <div class="width-60 fltlft">
		<fieldset class="adminform">
    <?php else : ?>
    <div class="igallery_form">
    <?php endif; ?>
			<ul class="adminformlist">
			
			<li><?php echo $this->form->getLabel('name'); ?>
			<?php echo $this->form->getInput('name'); ?></li>
			
			<li><?php echo $this->form->getLabel('name_ar'); ?>
			<?php echo $this->form->getInput('name_ar'); ?></li>
			
			<?php if( $this->isSite == false || $this->params->get('allow_frontend_cat_alias', 0) == 1): ?>
				<li><?php echo $this->form->getLabel('alias'); ?>
				<?php echo $this->form->getInput('alias'); ?></li>
			<?php endif; ?>
			
			<?php if( $this->isSite == false || $this->params->get('allow_frontend_cat_image', 0) == 1): ?>	
				<li><?php echo $this->form->getLabel('upload_image'); ?>
			
				<?php 
				//print_r($this->item);
				if(!empty($this->item->menu_image_filename)): 
				$img = $filepath.base64_encode(IG_ORIG_PATH.'\\1-100\\'.'\\'.$this->item->menu_image_filename);
				///////////////////// wa
			$img = $filepath.'\\'.$this->item->menu_image_filename;
			$img = str_replace("\\", "/", $img);		
			
$filetype=string;       
	   $imgbinary = fread(fopen($img, "r"), filesize($img));
      // echo $imgbinary;
	   $aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
				?>
                        <!--<img src="<?php //echo IG_IMAGE_HTML_RESIZE; ?><?php //echo $this->fileArray['folderName']; ?>/<?php //echo $this->fileArray['fullFileName']; ?>" alt=""/>-->
					    <img src="<?php echo $aaa; ?>" alt="" width="100px" height="100px"/>
				<?php endif; ?>
				
				<?php echo $this->form->getInput('upload_image'); ?>
				</li>
			<?php endif; ?>
				
			
			<?php if( $this->isSite == false || $this->params->get('allow_frontend_cat_remove', 0) == 1): ?>	
				<li><?php echo $this->form->getLabel('remove_menu_image'); ?>
				<?php echo $this->form->getInput('remove_menu_image'); ?></li>
			<?php endif; ?>
			
			<li><?php echo $this->form->getLabel('department'); ?>
			<?php echo $this->form->getInput('department'); ?></li>
			
			<?php if( $this->isSite == false || $this->params->get('allow_frontend_cat_profile', 0) == 1): ?>
				<li><?php echo $this->form->getLabel('profile'); ?>
				<?php echo $this->form->getInput('profile'); ?></li>
			<?php endif; ?>
			
			<?php if( $this->isSite == false || $this->params->get('allow_frontend_cat_parent', 0) == 1): ?>	
				<li><?php echo $this->form->getLabel('parent'); ?>
				<?php echo $this->form->getInput('parent'); ?></li>
			<?php endif; ?>
				
			<?php if($this->isSite == false): ?>
				<li><?php echo $this->form->getLabel('user'); ?>
				<?php echo $this->form->getInput('user'); ?></li>
			<?php endif; ?>
			
				
			<li><?php echo $this->form->getLabel('public'); ?>
			<?php echo $this->form->getInput('public'); ?></li>
			
				
			<li><?php echo $this->form->getLabel('published'); ?>
			<?php echo $this->form->getInput('published'); ?></li>
			
            <!--<li><?php echo $this->form->getLabel('publish_up'); ?>
                <?php //echo $this->form->getInput('publish_up'); ?>
                <input type="text" class="inputbox" value="<?php echo strtotime($this->item->publish_up) != '' ? htmlspecialchars(date('Y-m-d', strtotime($this->item->publish_up)), ENT_QUOTES) : ''; ?>" id="jform_publish_up" name="jform[publish_up]" onClick="return showCalendar('jform_publish_up','%Y-%m-%d');" />
                </li>
        

            <li><?php echo $this->form->getLabel('publish_down'); ?>
            <?php //echo $this->form->getInput('publish_down'); ?>
                <input type="text" class="inputbox" value="<?php echo strtotime($this->item->publish_down) != '' ? htmlspecialchars(date('Y-m-d', strtotime($this->item->publish_down)), ENT_QUOTES) : ''; ?>" id="jform_publish_down" name="jform[publish_down]" onClick="return showCalendar('jform_publish_down','%Y-%m-%d');" />
            </li> -->
				
			<li><?php echo $this->form->getLabel('date'); ?>
			<input name="" disabled="disabled" value="<?php print date('d-m-Y H:i:s', time()); ?>" size="40" /></li>
            
			<li><?php echo $this->form->getLabel('user'); ?>
            <?php $user = JFactory::getUser(); ?>
			<input name="" disabled="disabled" value="<?php print $user->name; ?>" size="40" /></li>
            
			<li>
			<?php echo $this->form->getInput('id'); ?></li>
			</ul>
	<?php if($this->isSite == false) : ?>
		</fieldset>
    <?php endif; ?>
	</div>
	<div class="clr"></div>

	<input type="hidden" name="task" value="" />
	<?php if($this->isSite == true): ?>
	<input type="hidden" name="Itemid" value="<?php echo JRequest::getInt('Itemid', 0); ?>" />
	<?php endif; ?>
	<?php echo JHtml::_('form.token'); ?>
</form>