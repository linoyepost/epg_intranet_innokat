<?php
/**
* @version 1.5
* @package JDownloads
* @copyright (C) 2009 www.eposts.com
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* 
*
*/
defined( '_JEXEC' ) or die( 'Restricted access' );

global $mainframe;

$params   = JComponentHelper::getParams('com_languages');
$frontend_lang = $params->get('site', 'en-GB');
$language = JLanguage::getInstance($frontend_lang);

class jlist_HTML{
////////////////////                DMS              ///////////////////////
function editDms($option, $row, $departmentList, $departmentAccess, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">                           
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.dms') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.dms_name.value == "" 
			 <?php if (!$row->id) : ?>
			 || form.dms_name_ar.value == "" 
			 || form.dms_file.value == "" 
			 <?php endif; ?>
			 || tinyMCE.get('dms_description').getContent()== ""
			 || tinyMCE.get('dms_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_DMS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_DMS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="dms_name" value="<?php echo htmlspecialchars($row->dms_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="dms_name_ar" value="<?php echo htmlspecialchars($row->dms_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?></strong><br /> 
                                  <?php 
								 $editor =& JFactory::getEditor();
								 echo $editor->display( 'dms_description',  @$row->dms_description , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br /> 
                                    <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'dms_description_ar',  @$row->dms_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?></strong><br />
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?></strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_FILE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="dms_file" id="dms_file" type="file" <?php print $row->dms_title != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->dms_title != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="dms_title_nm"><?php print $row->dms_title; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('dms_file', 'dms_title_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Publish:</strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.dms" />
    </form>
<?php
}   


function listDms($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', true);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
   
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_ID_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_ACCESS')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_DEPARTMENT_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_DOC_TITLE')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.dms&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_DMS_LIST_NAME_TITLE');?>"><?php echo $row->dms_name; ?></a></td>
            <td valign="top"><?php echo ($row->dms_access)? 'Public': 'Private'; ?></td>
            <td valign="top"><?php echo $row->department; ?></td>
            <td valign="top"><?php echo $row->dms_title; ?><?php //echo '<br />'.$row->dms_path; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 	
////////////////////                BOOKINGSLOTS              ///////////////////////
function editBookingSlots($option, $row, $departmentList, $departmentAccess){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.bookingslots') {
            submitform( pressbutton );
            return;
        }

        // do field validation
        if (form.bookingslots_title.value == ""){
            alert( "<?php echo JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_EDIT_ERROR_TITLE');?>" );
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_LIST_NAME_TITLE')." "; ?></strong><br />
                                    <input name="bookingslots_title" value="<?php echo htmlspecialchars($row->bookingslots_title, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_LIST_NAME_TITLE_AR')." "; ?></strong><br />
                                    <input name="bookingslots_title_ar" value="<?php echo htmlspecialchars($row->bookingslots_title_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_LIST_DESC_TITLE')." "; ?></strong><br />
                                    <textarea name="bookingslots_description" rows="5" cols="45"><?php echo htmlspecialchars($row->bookingslots_description, ENT_QUOTES); ?></textarea> 
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_LIST_DESC_TITLE_AR')." "; ?></strong><br />
                                    <textarea name="bookingslots_description_ar" rows="5" cols="45"><?php echo htmlspecialchars($row->bookingslots_description_ar, ENT_QUOTES); ?></textarea> 
                                   </td>
                           </tr>
                         </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.bookingslots" />
    </form>
<?php
}   


function listBookingSlots($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_LIST_ID_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_LIST_DESC_TITLE')." "; ?></th>            
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.bookingslots&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_BOOKINSLOTS_LIST_NAME_TITLE');?>"><?php echo $row->bookingslots_title; ?></a></td>
            <td valign="top"><?php echo $row->bookingslots_description; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 		
////////////////////                RESOURCES              ///////////////////////	
function editResources($option, $row, $usersList, $toAddUsersList, $controllerList, $officesList){
global $mainframe, $jlistConfig;
 $user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
 function deletefile(filefield, filename, getUpdate) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		document.getElementById(getUpdate).value = '1';
		return false;
	}   
   Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.resources') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.resources_title.value == "" 
			 || form.resources_title_ar.value == ""
			 || tinyMCE.get('resources_description').getContent()== ""
			 || tinyMCE.get('resources_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
        } else {
            allSelected(document.adminForm['users_selected[]']);
            Joomla.submitform( pressbutton );
            return true;
        }
    }
    
    // moves elements from one select box to another one
    function moveOptions(from,to) {
      // Move them over
      for (var i=0; i<from.options.length; i++) {
           var o = from.options[i];
           if (o.selected) {
               to.options[to.options.length] = new Option( o.text, o.value, false, false);
           }
       }
       // Delete them from original
       for (var i=(from.options.length-1); i>=0; i--) {
            var o = from.options[i];
            if (o.selected) {
                from.options[i] = null;
            }
       }
       from.selectedIndex = -1;
       to.selectedIndex = -1;
    }

    function allSelected(element) {
       for (var i=0; i<element.options.length; i++) {
            var o = element.options[i];
                o.selected = true;
       }
    }
    </script>       
    
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_RESOURCES_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_RESOURCES_ADD_TITLE');?></th>
                  </tr>
                  
							<tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="resources_title" value="<?php echo htmlspecialchars($row->resources_title, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="resources_title_ar" value="<?php echo htmlspecialchars($row->resources_title_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_OFFICE_TITLE')." "; ?></strong><br />
                                   <?php echo $officesList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'resources_description',  @$row->resources_description , '50%', '10', '80', '5', true, false ) ;
								?> 
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'resources_description_ar',  @$row->resources_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                            <tr>
                                <td><strong><?php echo JText::_('resources_logo'); ?></strong><br />
                                    <input name="resources_logo" id="resources_logo" type="file" <?php print $row->resources_logo != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->resources_logo != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="resources_logo_nm"><?php print $row->resources_logo; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('resources_logo', 'resources_logo_nm','del_resources_logo');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                     <input name="del_resources_logo" id="del_resources_logo" type="hidden" value="0"/>                             
                            </tr><tr>
							<tr>
                                <td><strong><?php echo "Resource Controller"." "; ?></strong><br />
                                    <?php //echo $controllerList;?>
                                    <select id="resources_controller" name="resources_controller" class="inputbox">
	<option value="425" selected="selected">425-spsadmin (spsadmin)</option>
</select>
                                                                 
                            </td>                             
                            </tr>
                            <tr>
                                <td>
              <table class="admintable">
                    <tr>
                        <td colspan="4">
                            <?php echo '<b>'."Privileged Groups".'</b>'; ?>
                        </td>
                    </tr>
                    <tr>    
                        <td><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_SELECT_USER_TITLE');?></td>
                        <td width="20%">&nbsp;</td>
                        <td><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_EXISTS_USER_TITLE');?></td>
                    </tr>
                    <tr>
                        <td width="40%"><?php echo $toAddUsersList;?></td>
                        <td width="20%">
                          <input style="width: 50px" type="button" name="Button" value="&gt;" onClick="moveOptions(document.adminForm.users_not_selected, document.adminForm['users_selected[]'])" />
                            <br /><br />
                            <input style="width: 50px" type="button" name="Button" value="&lt;" onClick="moveOptions(document.adminForm['users_selected[]'],document.adminForm.users_not_selected)" />
                            <br /><br />
                        </td>
                        <td width="40%"><?php echo $usersList;?></td>
                    </tr>
                    <tr>
                        <td colspan="4"><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_EDIT_SELECT_INFO');?></td>
                    </tr>
                </table>                       
                            </tr>
                         </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="resources_members" value="<?php  echo $row->resources_members;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.resources" />
    </form>
<?php
}   


function listResources($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', true);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_LIST_ID_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_OFFICE_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo "Resource Controller"." "; ?></th>
            <th valign="top" class="title"><?php echo "Privileged Groups"." "; ?></th>            
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.resources&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_RESOURCES_LIST_NAME_TITLE');?>"><?php echo $row->resources_title; ?></a></td>
            <td valign="top"><?php echo $row->offices_title; ?></td>
            <td valign="top"><?php echo $row->controller; ?></td>
            <td valign="top"><?php echo $row->members; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 	

////////////////////                OFFICES              ///////////////////////
function editOffices($option, $row, $departmentList, $departmentAccess){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.offices') {
            submitform( pressbutton );
            return;
        }
       // do field validation
        if ( form.offices_title.value == "" 
			 || form.offices_title_ar.value == "" 
			 || form.offices_location.value == "" 
			 || form.offices_location_ar.value == ""
			 || tinyMCE.get('offices_description').getContent()== ""
			 || tinyMCE.get('offices_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
               
			   <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_OFFICES_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_OFFICES_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                            
<tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                            
 <tr>
                                <td><strong><?php echo JText::_('Emirate Name')." "; ?></strong><br />
                                   <?php 
								   $database = &JFactory::getDBO();
								   $database->setQuery("SELECT * FROM #__emirate  ORDER BY emirate_name ASC ");
			$listDepartments2 = $database->loadObjectList();?>
			<select name="emirate_id" >
				

				<?php

 



				foreach($listDepartments2 as $xdepartment2) { ?>
		
	<option value="<?php echo $xdepartment2->eid; ?>"><?php echo $xdepartment2->emirate_name;  ?></option>
			<?php	} ?>
			</select></td>
                                                                 
                            </tr>
							<tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="offices_title" value="<?php echo htmlspecialchars($row->offices_title, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="offices_title_ar" value="<?php echo htmlspecialchars($row->offices_title_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LOCATION_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="offices_location" value="<?php echo htmlspecialchars($row->offices_location, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LOCATION_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="offices_location_ar" value="<?php echo htmlspecialchars($row->offices_location_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_PHONE_TITLE')." "; ?></strong><br />
                                    <input name="offices_phone" value="<?php echo htmlspecialchars($row->offices_phone, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'offices_description',  @$row->offices_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'offices_description_ar',  @$row->offices_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?> 
                                   </td>
                           </tr>
                         </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.offices" />
    </form>
<?php
}   


function listOffices($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', true);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LIST_ID_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LOCATION_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_PHONE_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LIST_DESC_TITLE')." "; ?></th>            
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.offices&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_OFFICES_LIST_NAME_TITLE');?>"><?php echo $row->offices_title; ?></a></td>
            <td valign="top"><?php echo $row->offices_location; ?></td>
            <td valign="top"><?php echo $row->offices_phone; ?></td>
            <td valign="top"><?php echo $row->offices_description; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 	
////////////////////                EVENTS              ///////////////////////	

function editEvents($option, $row, $departmentList, $departmentAccess, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">                       
	function deletefile(filefield, filename, getUpdate) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		document.getElementById(getUpdate).value = '1';
		return false;
	}
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.events') {
            submitform( pressbutton );
            return;
        }

        // do field validation
        if ( form.events_title.value == "" 
			 || form.events_title_ar.value == "" 
			 || form.events_location.value == "" 
			 || form.events_location_ar.value == "" 
			 || form.events_organized_by.value == ""
			 || form.events_organized_by_ar.value == ""
			 || tinyMCE.get('events_description').getContent()== ""
			 || tinyMCE.get('events_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_EVENTS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_EVENTS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="events_title" value="<?php echo htmlspecialchars($row->events_title, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="events_title_ar" value="<?php echo htmlspecialchars($row->events_title_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LOCATION_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="events_location" value="<?php echo htmlspecialchars($row->events_location, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LOCATION_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="events_location_ar" value="<?php echo htmlspecialchars($row->events_location_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_ORGENIZED_BY_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="events_organized_by" value="<?php echo htmlspecialchars($row->events_organized_by, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_ORGENIZED_BY_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="events_organized_by_ar" value="<?php echo htmlspecialchars($row->events_organized_by_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            
                            <tr>
                                <td><strong><?php echo JText::_('Event Logo').""; ?>:</strong><br />
                                    <input name="event_logo" id="event_logo" type="file" <?php print $row->event_logo != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->event_logo != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="event_logo_nm"><?php print $row->event_logo; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('event_logo', 'event_logo_nm','del_event_logo');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                    <input name="del_event_logo" id="del_event_logo" type="hidden" value="0"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('Event Image').""; ?>:</strong><br />
                                    <input name="event_image" id="event_image" type="file" <?php print $row->event_image != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->event_image != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="event_image_nm"><?php print $row->event_image; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('event_image', 'event_image_nm','del_event_image');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                    <input name="del_event_image" id="del_event_image" type="hidden" value="0"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('Event Contact Detail').""; ?>:</strong><br />
                                    <input name="event_contact" value="<?php echo htmlspecialchars($row->event_contact, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_START_DATE_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <?php
									// for plugin
		                             JPluginHelper::importPlugin('datepicker');	
	                                echo plgSystemDatePicker::calendar($row->events_startdate, 'events_startdate', 'events_startdate', '%Y-%m-%d %H:%M:%S');
									 ?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_END_DATE_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <?php	
	                                echo plgSystemDatePicker::calendar($row->events_enddate, 'events_enddate', 'events_enddate', '%Y-%m-%d %H:%M:%S');
									 ?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?></strong><br /> 
                                    <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'events_description',  @$row->events_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'events_description_ar',  @$row->events_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?> 
                                   </td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?></strong><br />
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?></strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>                                    
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.events" />
    </form>
<?php
}   


function listEvents($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', true);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_ID_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_START_DATE_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_END_DATE_TITLE')." "; ?></th> 
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></th>           
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.events&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_EVENTS_LIST_NAME_TITLE');?>"><?php echo $row->events_title; ?></a></td>
            <td valign="top"><?php echo $row->events_startdate; ?></td>
            <td valign="top"><?php echo $row->events_enddate; ?></td>
            <td valign="top"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 

////////////////////                NEW HIRES              ///////////////////////
function editNewHires($option, $row, $departmentList, $departmentAccess, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">                         
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.newhires') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.newhires_name.value == "" 
			 || form.newhires_name_ar.value == ""
			 || form.newhires_designation.value == ""
			 || form.newhires_designation_ar.value == ""
<?php if(!$row->id){?>
 || form.newhires_file.value == ""
<?php }?>
			 || tinyMCE.get('newhires_description').getContent()== ""
			 || tinyMCE.get('newhires_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
        } else {
			if(form.newhires_email.value != ""){
				var x=form.newhires_email.value;
				var atpos=x.indexOf("@");
				var dotpos=x.lastIndexOf(".");
				if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
				  {
				  alert("Please enter a valid email address");
				  return false;
				  }
			}
			  
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_NEWHIRES_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_NEWHIRES_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable"  width="100%">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="newhires_name" value="<?php echo htmlspecialchars($row->newhires_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="newhires_name_ar" value="<?php echo htmlspecialchars($row->newhires_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_DESIGNETION_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="newhires_designation" value="<?php echo htmlspecialchars($row->newhires_designation, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_DESIGNETION_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="newhires_designation_ar" value="<?php echo htmlspecialchars($row->newhires_designation_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_MOBILE_TITLE')." "; ?></strong><br />
                                    <input name="newhires_mobile" value="<?php echo htmlspecialchars($row->newhires_mobile, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_EMAIL_TITLE')." "; ?></strong><br />
                                    <input name="newhires_email" value="<?php echo htmlspecialchars($row->newhires_email, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'newhires_description',  @$row->newhires_description , '50%', '10', '80', '5', true, false ) ;
								?> 
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'newhires_description_ar',  @$row->newhires_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <!--<tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?></strong><br />
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr> -->
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?></strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_FILE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="newhires_file" id="newhires_file" type="file" <?php print $row->newhires_picture != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->newhires_picture != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="newhires_picture_nm"><?php print $row->newhires_picture; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('newhires_file', 'newhires_picture_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('Expiry Date'); ?></strong><br />
                                <?php JHTML::_('behavior.calendar'); ?>
                                 <input class="inputbox" type="text" name="expirydate" id="expirydate"  value="<?php echo strtotime($row->expirydate) ? date('d-m-Y', strtotime(htmlspecialchars($row->expirydate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('expirydate','%d-%m-%Y');"/>
                                                                 
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="newhires_picture" value="<?php echo $row->newhires_picture; ?>" />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.newhires" />
    </form>
<?php
}   


function listNewHires($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', true);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_ID_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_DESIGNETION_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_NEWHIRES_MOBILE_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></th>            
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.newhires&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_EDIT_TITLE');?>"><?php echo $row->newhires_name; ?></a></td>
            <td valign="top"><?php echo $row->newhires_designation; ?></td>
            <td valign="top"><?php echo $row->newhires_mobile; ?></td>
            <td valign="top"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 
	
////////////////////                TASAWAQS              ///////////////////////	
function editTasawaqs($option, $row, $departmentList, $departmentAccess, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">                            
	function deletefile(filefield, filename, getUpdate) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		document.getElementById(getUpdate).value = '1';
		return false;
	}
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.tasawaqs') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.tasawaqs_name.value == "" 
			 || form.tasawaqs_name_ar.value == ""
			 || tinyMCE.get('tasawaqs_description').getContent()== ""
			 || tinyMCE.get('tasawaqs_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    
    <form action="index.php" method="post" name="adminForm">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TASAWAQS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_TASAWAQS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="tasawaqs_name" value="<?php echo htmlspecialchars($row->tasawaqs_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="tasawaqs_name_ar" value="<?php echo htmlspecialchars($row->tasawaqs_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?></strong><br /> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'tasawaqs_description',  @$row->tasawaqs_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'tasawaqs_description_ar',  @$row->tasawaqs_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <!--<tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?></strong><br />
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_PRICE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="tasawaqs_price" value="<?php echo htmlspecialchars($row->tasawaqs_price, ENT_QUOTES); ?>">
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?></strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr> -->
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('Tasawaq Partner Logo'); ?></strong><br />
                                    <input name="tasawaqs_p_logo" id="tasawaqs_p_logo" type="file" <?php print $row->tasawaqs_p_logo != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->tasawaqs_p_logo != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="tasawaqs_p_logo_nm"><?php print $row->tasawaqs_p_logo; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('tasawaqs_p_logo', 'tasawaqs_p_logo_nm','del_tasawaqs_p_logo');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                     <input name="del_tasawaqs_p_logo" id="del_tasawaqs_p_logo" type="hidden" value="0"/>                             
                            </tr><tr>
                                <td><strong><?php echo JText::_('Tasawaq Partner Name'); ?></strong><br />
                                    <input type="text" name="tasawaq_p_name" id="tasawaq_p_name" value="<?php echo htmlspecialchars($row->tasawaq_p_name, ENT_QUOTES); ?>" size="80" maxlength="150" />
                                                                 
                            </tr><tr>
                                <td><strong><?php echo JText::_('Tasawaq Contact Details'); ?></strong><br />
                                    <?php 
									$editor3 =& JFactory::getEditor();
									echo $editor3->display( 'tasawaq_contact',  @$row->tasawaq_contact , '100%', '10', '80', '5', false, false ) ;
									?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('Valid Until'); ?></strong><br />
                                    <?php
								JHTML::_('behavior.calendar');
								 ?>
                                 <input class="inputbox" type="text" name="tasawaqs_expiry" id="tasawaqs_expiry"  value="<?php echo strtotime($row->tasawaqs_expiry) ? date('d-m-Y', strtotime(htmlspecialchars($row->tasawaqs_expiry, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('tasawaqs_expiry','%d-%m-%Y');"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('Tasawaq Image'); ?></strong><br />
                                    <input name="tasawaqs_image" id="tasawaqs_image" type="file" <?php print $row->tasawaqs_image != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->tasawaqs_image != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="tasawaqs_image_nm"><?php print $row->tasawaqs_image; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('tasawaqs_image', 'tasawaqs_image_nm','del_tasawaqs_image');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                    <input name="del_tasawaqs_image" id="del_tasawaqs_image" type="hidden" value="0"/>                             
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('Tasawaq Attachment'); ?></strong><br />
                                    <input name="tasawaqs_attachment" id="tasawaqs_attachment" type="file" <?php print $row->tasawaqs_attachment != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->tasawaqs_attachment != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="tasawaqs_attachment_nm"><?php print $row->tasawaqs_attachment; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('tasawaqs_attachment', 'tasawaqs_attachment_nm','del_tasawaqs_attachment');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                    <input name="del_tasawaqs_attachment" id="del_tasawaqs_attachment" type="hidden" value="0"/>                             
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.tasawaqs" />
    </form>
<?php
}   


function listTasawaqs($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', true);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td  align="right" colspan="5">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_ID_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_LIST_NAME_TITLE')." "; ?></th>
            <!--<th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_DEPARTMENT_TITLE')." "; ?></th> 
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TASAWAQS_PRICE')." "; ?></th>-->
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.tasawaqs&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_EDIT_TITLE');?>"><?php echo $row->tasawaqs_name; ?></a></td>
            <!--<td valign="top"><?php echo $row->department; ?></td>
            <td valign="top"><?php echo $row->tasawaqs_price; ?></td> -->
            <td valign="top"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 
////////////////////                Booking Request              ///////////////////////
function listApproveBookingRequest($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $jlistConfig, $Itemid, $mainframe, $page_title;
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', true);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
//////////////////////////////////////////////////

    $where = 'WHERE 1 = 1';
	
	   $database->SetQuery( "SELECT count(*) FROM #__eposts_bookingrequest ".$where);
    $total = $database->loadResult();    

  $search = $mainframe->getUserStateFromRequest( "search{$option}", 'search', '' );
  
    if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( strtolower( $search )));
        $where  .= " AND ( LOWER(bookingdate) LIKE '%".$search."%'";
        $where .= " OR LOWER(booking_title) LIKE '%".$search."%') ";
    }    
    
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
        $session = JFactory::getSession();
        $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
    $query = "SELECT * FROM #__eposts_bookingrequest
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
   
?>	
 <form name="adminForm" id="adminForm" action="index.php" >
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_DATE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_RESOURCE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_SLOTS')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_OTERREQUIRED')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('Requested By')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_STAUS')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LIST_ACTION')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $row->booking_title; ?></td>
            <td valign="top"><?php echo $row->bookingdate; ?></td>
             <td valign="top"><?php
			$row2 = new jlist_resources( $database );
            $row2->load( $row->resourceid );
			echo $row2->resources_title;
			?></td>
            <td valign="top"><p><?php echo 'From '.sprintf('%02d',$row->from_h).':'.sprintf('%02d',$row->from_m).'<br>&nbsp;&nbsp;&nbsp;&nbsp;To '. sprintf('%02d',$row->to_h).':'.sprintf('%02d',$row->to_m); ?></p></td>
            <td valign="top"><?php echo $row->booking_details_description; ?></td>
            <td valign="top"><?php
									$database->setQuery("SELECT name "
										. "\n FROM #__users "
										. "\n WHERE id = '" . $row->publishedby."' "
									);
						   
							        $bubby = $database->loadObject();
									echo $bubby->name;
			 ?></td>
            <td valign="top"><?php
			 if($row->booking_status == '1'){
				 echo "Approved";
			}elseif($row->booking_status == '2'){
				 echo "Rejected";
			}else{
				 echo "Pending";
			} 
			 
			 ?></td>
            <td valign="top">
            <?php
            $applink = "index.php?option=com_eposts&task=approve.approvebookingrequest&cirid=".$row->id;
			$rejectlink = "index.php?option=com_eposts&task=reject.approvebookingrequest&cirid=".$row->id;
			?>
            <a href="<?php echo $applink; ?>"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LINK_APPROVED')." "; ?></a>&nbsp;&nbsp;<a href="<?php echo $rejectlink; ?>"><?php echo JText::_('COM_EPOSTS_BACKEND_APPROVEBOOKINN_LINK_REJECTED')." "; ?></a>
            </td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
}

////////////////////                Media Library              ///////////////////////
function editDwnmedias($option, $row, $departmentList, $departmentAccess, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">                           
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.dwnmedias') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.media_name.value == "" 
			 || form.media_name_ar.value == "" 
<?php if(!$row->id){?>
 || form.media_file.value == ""
<?php }?>
			 || tinyMCE.get('media_description').getContent()== ""
			 || tinyMCE.get('media_description_ar').getContent() == "" ){
				 alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="media_name" value="<?php echo htmlspecialchars($row->media_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="media_name_ar" value="<?php echo htmlspecialchars($row->media_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                  <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'media_description',  @$row->media_description , '50%', '10', '80', '5', true, false ) ;
								?> 
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'media_description_ar',  @$row->media_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?> 
                                   </td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?></strong><br />
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?></strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_FILE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="media_file" id="media_file" type="file" <?php print $row->media_document != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->media_document != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="media_document_nm"><?php print $row->media_document; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('media_file', 'media_document_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.dwnmedias" />
    </form>
<?php
}   


function listDwnmedias($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', true);	
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIA_LIST_ID_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_LIST_DESC_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('Publish')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.dwnmedias&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_DWNMEDIAS_EDIT_TITLE');?>"><?php echo $row->media_name; ?></a></td>
            <td valign="top"><?php echo $row->media_description; ?></td>
            <td valign="top"><?php
			echo ($row->publish == 1)? 'Yes':'No';
			 ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 


////////////////////                CIRCULARS              ///////////////////////
function editCirculars($option, $row, $departmentList, $departmentAccess, $isPublist){
global $mainframe, $jlistConfig;
$user = &JFactory::getUser();
?>
    <script language="javascript" type="text/javascript">                          
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.circulars') {
            submitform( pressbutton );
            return;
        }

        // do field validation
        if ( form.circulars_name.value == "" 
			 || form.circulars_name_ar.value == "" 
			 || form.circulars_date.value == "" 
<?php if(!$row->id){?>
			 || form.circulars_file.value == "" 
			 || form.circulars_file_ar.value == ""
<?php }?> ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>  
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_CIRCULARS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_CIRCULARS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="circulars_name" value="<?php echo htmlspecialchars($row->circulars_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="circulars_name_ar" value="<?php echo htmlspecialchars($row->circulars_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_DESC_TITLE')." "; ?></strong><br />
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'circulars_description',  @$row->circulars_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_DESC_TITLE_AR')." "; ?></strong><br />
                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'circulars_description_ar',  @$row->circulars_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_IS_PUBLIC_TITLE')." "; ?></strong><br />
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULAR_DATE')."<font color='#990000'>*</font>"; ?></strong><br />
								<?php JHTML::_('behavior.calendar'); ?>
                                 <input class="inputbox" type="text" name="circulars_date" id="circulars_date"  value="<?php echo strtotime($row->circulars_date) ? date('d-m-Y', strtotime(htmlspecialchars($row->circulars_date, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('circulars_date','%d-%m-%Y');"/>
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_DEPARTMENT')." "; ?></strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_FILE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="circulars_file" id="circulars_file" type="file" <?php print $row->circulars_document != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->circulars_document != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="circulars_file_nm"><?php print $row->circulars_document; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('circulars_file', 'circulars_file_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SELECT_FILE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="circulars_file_ar" id="circulars_file_ar" type="file" <?php print $row->circulars_document_ar != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->circulars_document_ar != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="circulars_document_ar_nm"><?php print $row->circulars_document_ar; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('circulars_file_ar', 'circulars_document_ar_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.circulars" />
    </form>
<?php
}   


function listCirculars($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', true);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULAR_LIST_ID_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULAR_DEPARTMENT_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULAR_DATE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.circulars&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CIRCULARS_EDIT_TITLE');?>"><?php echo $row->circulars_name; ?></a></td>
            <td valign="top"><?php echo $row->department; ?></td>
            <td valign="top"><?php echo date('d-m-Y', strtotime($row->circulars_date)); ?></td>
            <td valign="top"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 

/*
/  Display the main component control panel
*/

function controlPanel($option, $task) {
    global $mainframe, $jlistConfig;
    jimport( 'joomla.html.pane');
    $database = &JFactory::getDBO();
        
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', true);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here     
?>
        
<!-- ICON begin -->

   <div class="adminform">
    <div class="cpanel-left">
     <div id="cpanel">
     
     <div class="icon-wrapper">
          <div class="icon"> 
                <a href="index.php?option=com_eposts&task=view.departments" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS');?>">
                <img src="components/com_eposts/images/groups.png" width="48px" height="48px" align="middle" border="0"/>
                <span><?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS');?></span>
                </a>
            </div>
        </div>
        
        <div class="icon-wrapper">
          <div class="icon"> 
        	    <a href="index.php?option=com_eposts&task=view.dms" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_DMS');?>">
                <img src="components/com_eposts/images/template.png" width="48px" height="48px" align="middle" border="0"/>
                <span><?php echo JText::_('COM_EPOSTS_DMS');?></span>
                </a>
            </div>
        </div>
       
       <div class="icon-wrapper">
          <div class="icon"> 
        	    <a href="index.php?option=com_eposts&task=view.circulars" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_CIRCULARS');?>">
                <img src="components/com_eposts/images/categories.png" width="48px" height="48px" align="middle" border="0"/>
                <span><?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_CIRCULARS');?></span>
                </a>
            </div>
        </div>
         
         <div class="icon-wrapper">
          <div class="icon"> 
        	    <a href="index.php?option=com_eposts&task=view.tasawaqs" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_TASAWAQS');?>">
                <img src="components/com_eposts/images/downloads.png" width="48px" height="48px" align="middle" border="0"/>
                <span><?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_TASAWAQS');?></span>
                </a>
            </div>
        </div> 
        
        <div class="icon-wrapper">
          <div class="icon"> 
        	    <a href="index.php?option=com_eposts&task=view.newhires" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_NEWHIRES');?>">
                <img src="components/com_eposts/images/backup.png" width="48px" height="48px" align="middle" border="0"/>
                <span><?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_NEWHIRES');?></span>
                </a>
            </div>
        </div>  
        
        <div class="icon-wrapper">
          <div class="icon"> 
        	    <a href="index.php?option=com_eposts&task=view.events" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_CPANEL_EVENTS');?>">
                <img src="components/com_eposts/images/files48.png" width="48px" height="48px" align="middle" border="0"/>
                <span><?php echo JText::_('COM_EPOSTS_CPANEL_EVENTS');?></span>
                </a>
            </div>
        </div> 

       <div class="icon-wrapper">
          <div class="icon"> 
                <a href="index.php?option=com_eposts&task=view.offices" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_CPANEL_OFFICES');?>">
                <img src="components/com_eposts/images/restore.png" width="48px" height="48px" align="middle" border="0"/>
                <span><?php echo JText::_('COM_EPOSTS_CPANEL_OFFICES');?></span>
                </a>
            </div>
        </div>
        
         <div class="icon-wrapper">
          <div class="icon"> 
                <a href="index.php?option=com_eposts&task=view.resources" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_CPANEL_RESOURCES');?>">
                <img src="components/com_eposts/images/licenses.png" width="48px" height="48px" align="middle" border="0"/>
                <span><?php echo JText::_('COM_EPOSTS_CPANEL_RESOURCES');?></span>
                </a>
            </div>
        </div>
        
        <!--<div class="icon-wrapper">
          <div class="icon"> 
                <a href="index.php?option=com_eposts&task=view.bookingslots" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_CPANEL_BOOKINSLOTS');?>">
                <img src="components/com_eposts/images/logs.png" width="48px" height="48px" align="middle" border="0"/>
                <span><?php echo JText::_('COM_EPOSTS_CPANEL_BOOKINSLOTS');?></span>
                </a>
            </div>
        </div>-->

	</div>
    </div>

    <!-- ICON END -->



<!-- TABS begin -->
	<div class="cpanel-right">
	  <div id="panel-sliders" class="pane-sliders">
        <div style="display: none;">
            <div></div>
        </div>
        <div class="panel">
			
            <form action="index.php" method="post" name="adminlist">

              <?php	$pane =& JPane::getInstance('Tabs');
                    echo $pane->startPane('paneltabs');
                    echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_PANEL_TABTEXT_5'),'server limits');
              ?>
              <table width="95%" border="0">
               <tr>
                    <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SERVER_INFOS_TAB_TITLE')." "; ?></th>
               </tr>

                  <tr>
                     <td colspan="2" valign="top" align="left" width="100%">
                         <?php echo JText::_('COM_EPOSTS_BACKEND_SERVER_INFOS_TAB_DESC'); ?>
                  </td>
                  
               </tr>
               <tr>
                 <td width="40%" style="background-color:#DCDCDC;">
                 <?php echo JText::_('COM_EPOSTS_BACKEND_SERVER_INFOS_TAB_FILE_UPLOADS'); ?>
                 </td>
                 <td width="60%" style="background-color:#DCDCDC;">
                 <?php if (get_cfg_var(file_uploads)){ echo JText::_('COM_EPOSTS_FE_YES'); } else { echo JText::_('COM_EPOSTS_FE_NO'); } ?> 
                 </td>
               </tr>
               <tr>  
                 <td width="40%" style="background-color:#DCDCDC;">
                 <?php echo JText::_('COM_EPOSTS_BACKEND_SERVER_INFOS_TAB_MAX_FILESIZE'); ?>
                 </td>
                 <td width="60%" style="background-color:#DCDCDC;">
                 <?php echo get_cfg_var (upload_max_filesize); ?>
                 </td>
               </tr>  
               <tr>  
                 <td width="40%" style="background-color:#DCDCDC;">
                 <?php echo JText::_('COM_EPOSTS_BACKEND_SERVER_INFOS_TAB_POST_MAX_SIZE'); ?>
                 </td>
                 <td width="60%" style="background-color:#DCDCDC;">
                 <?php echo get_cfg_var (post_max_size); ?>
                 </td>
               </tr>  
               <tr>  
                 <td width="40%" style="background-color:#DCDCDC;">
                 <?php echo JText::_('COM_EPOSTS_BACKEND_SERVER_INFOS_TAB_MEMORY_LIMIT'); ?>
                 </td>
                 <td width="60%" style="background-color:#DCDCDC;">
                 <?php echo get_cfg_var (memory_limit); ?>
                 </td>
               </tr>  
               <tr>  
                 <td width="40%" style="background-color:#DCDCDC;">
                 <?php echo JText::_('COM_EPOSTS_BACKEND_SERVER_INFOS_TAB_MAX_INPUT_TIME'); ?>
                 </td>
                 <td width="60%" style="background-color:#DCDCDC;">
                 <?php echo get_cfg_var (max_input_time); ?>
                 </td>
               </tr>  
               <tr>  
                 <td width="40%" style="background-color:#DCDCDC;">
                 <?php echo JText::_('COM_EPOSTS_BACKEND_SERVER_INFOS_TAB_MAX_EXECUTION_TIME'); ?>
                 </td>
                 <td width="60%" style="background-color:#DCDCDC;">
                 <?php echo get_cfg_var (max_execution_time); ?>
                 </td>
               </tr>  
              </table>
              <?php
               echo $pane->endPanel();
               
               echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_PANEL_TABTEXT_2'),'version');              
              ?>
              <table width="95%" border="0">
               <tr>
                    <th class="adminheading"><?php echo JText::_('COM_EPOSTS_BACKEND_PANEL_STATUS_VERSION_HEADER')." "; ?></th>
               </tr>

   		       <tr>
   		          <td valign="top" align="left" width="100%">
           	          <?php echo '<b><font color="#990000">ePosts '.$jlistConfig['jd.version'].' '.$jlistConfig['jd.version.state'].' Build '.$jlistConfig['jd.version.svn'].'</font></b>';?>
                      <br /><br />
                  </td>
               </tr>
              </table>
              <?php
               echo $pane->endPanel();
               
               echo $pane->endPane('paneltabs');?>               
        </form> 
        </div>
    </div>
    </div>

<!-- TABS END -->



<?php
}

/**********************************************
/ Licenses
/ ********************************************/

// Licenses list

function listLicense($rows, $option){
	global $mainframe, $jlistConfig;

    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here

	JHTML::_('behavior.tooltip');
?>
	<form action="index.php" method="post" name="adminForm" id="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
	  <tr>
		  <td colspan="7">&nbsp;</td>
	  </tr>
		<tr>
			<th width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_LICLIST_TITLE')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_LICLIST_LINK')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_LICEDIT_LIC_DESCRIPTION')." "; ?></th>
        </tr>
		<?php
		$k = 0;
		for($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			//$row->id = $row->cat_id;
			$link 		= 'index.php?option=com_eposts&task=license.edit&hidemainmenu=1&cid='.$row->id;
			$checked 	= JHTML::_('grid.checkedout', $row, $i );
			?>
		<tr class="<?php echo "row$k"; ?>">
			<td><?php echo $checked; ?></td>
			<td><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_LICEDIT_EDIT');?>"><?php echo $row->license_title; ?></a></td>

			<td><a href="<?php echo $row->license_url; ?>" target="_blank"><?php echo $row->license_url; ?></a></td>
            <td>
            <?php
                if (strlen($row->license_text) > 200 ) {
                   echo substr(stripslashes($row->license_text), 0, 197).'...';
                } else {
                   echo stripslashes($row->license_text);
                }
            ?>
            </td>

            <?php $k = 1 - $k;  } ?>
		</tr>
	</table>
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="hidemainmenu" value="0">
</form>

<?php
}

function editLicense($option, $row){
global $mainframe, $jlistConfig;
$editor =& JFactory::getEditor();
	?>

	<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'license.cancel') {
			submitform( pressbutton );
			return;
		}

		// do field validation
		if (form.license_title.value == ""){
			alert( "<?php echo JText::_('COM_EPOSTS_BACKEND_LICEDIT_ERROR_TITLE');?>" );
		} else {
			submitform( pressbutton );
		}
	}
	</script>

	<form action="index.php" method="post" name="adminForm" id="adminForm">
	<table width="100%" border="0">
		<tr>
			<td width="100%" valign="top">
			<table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_LICEDIT_EDIT') : JText::_('COM_EPOSTS_BACKEND_LICEDIT_ADD');?></th>
		      	</tr>
		      	<tr>
		      		<td valign="top" align="left" width="100%">
		      			<table>
		  					<tr>
		    					<td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_LICEDIT_LIC_TITLE')." "; ?></strong><br />
		    						<input name="license_title" value="<?php echo stripslashes($row->license_title); ?>" size="70" maxlength="50"/>
                                </td>
		  					</tr>
                            <tr>
		    					<td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_LICEDIT_LIC_URL')." "; ?></strong><br />
		    						<input name="license_url" value="<?php echo $row->license_url; ?>" size="70" maxlength="255"/>
		       					</td>
                                <td><?php echo JText::_('COM_EPOSTS_BACKEND_LICEDIT_LIC_URL_DESC')." "; ?>
                                </td>                                
		  					</tr>
		  					<tr>
		  						<td colspan="2"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_LICEDIT_LIC_DESCRIPTION')." "; ?></strong><br />
	  							<?php
                                 if ($jlistConfig['licenses.editor'] == "1") {
									echo $editor->display( 'license_text',  @stripslashes($row->license_text) , '100%', '500', '80', '5' ) ;
									
                                 } else {?>
                                    <textarea name="license_text" rows="20" cols="80"><?php echo stripslashes($row->license_text); ?></textarea>
                                <?php
                                } ?>

		  						</td>
		  					</tr>
		  				</table>
		  			</td>
		  		</tr>
			</table>
			</td>
		</tr>
	</table>
<br /><br />

		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0">
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="task" value="" />
	</form>

<?php
}
                               
function listLogs($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe, $jlistConfig;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td align="left" colspan ="7">
              <?php
               if (!$jlistConfig['activate.download.log']){
                   echo  JText::_('COM_EPOSTS_BACKEND_LOG_LIST_SETTINGS_OFF');
               } else {
                   $plugin =& JPluginHelper::getPlugin('system', 'eposts');
                   $pluginParams = new JParameter( $plugin->params );
                   $reduce_log_data = $pluginParams->get('reduce_log_data_sets_to', 0);
                   if ($reduce_log_data > 0){
                       echo JText::_('COM_EPOSTS_BACKEND_LOG_LIST_INFO').'<br />'.sprintf(JText::_('COM_EPOSTS_BACKEND_LOG_LIST_REDUCE_ON'), $reduce_log_data);
                   } else {
                       echo JText::_('COM_EPOSTS_BACKEND_LOG_LIST_INFO').'<br />'.JText::_('COM_EPOSTS_BACKEND_LOG_LIST_REDUCE_OFF');
                   }  
               }    ?>
          </td>
          </tr>
          <tr> 
          <td align="right" colspan="7">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_VIEW_LOGS_COL_TITLE_DATE')." "; ?></th>
            <th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_VIEW_LOGS_COL_TITLE_USER')." "; ?></th>
            <th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_VIEW_LOGS_COL_TITLE_IP')." "; ?></th>
            <th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_VIEW_LOGS_COL_TITLE_FILE')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link         = 'http://www.ip-whois-lookup.com/lookup.php?ip='.$row->log_ip;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td><?php echo $checked; ?></td>
            <td><?php echo $row->log_datetime; ?></td>
            <td><?php echo $row->user; ?></td>
            <td><a href="<?php echo $link; ?>" target="_blank" title="<?php echo JText::_('COM_EPOSTS_BACKEND_VIEW_LOGS_TITLE_CHECK_IP');?>"><?php echo $row->log_ip; ?></a></td>            
            <td><?php echo $row->file_title; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>

<?php   
    
} 

function listDepartments($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', true);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_ID_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NAME_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_DESC_TITLE')." "; ?></th>
            <th valign="top" align="center" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_COUNT_TITLE')." "; ?></th>
            <th valign="top" class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENTS_EMAIL_TITLE')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.departments&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENTS_EDIT_TITLE');?>"><?php echo $row->departments_name; ?></a></td>
            <td valign="top"><?php echo $row->departments_description; ?></td>
            <td valign="top" align="center"><?php echo $row->sum_members; ?></td>
            <td valign="top"><?php echo $row->departments_email; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 

function listSyncUsers($option, $task, $limitstart) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', true);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
	
	$database = &JFactory::getDBO();
	
	//$userAr = array_merge(jlist_HTML::get_members("CN=Users"), jlist_HTML::get_members("OU=Acrologix"));
	$userAr = jlist_HTML::get_members();
	$update = 0;
	$addnew	= 0;
	$faildd = 0;
	foreach($userAr as $user) {
		$username = $user['samaccountname'];
		
		$query = "SELECT * FROM #__users WHERE username = '".$username."'";                              
		$database->setQuery($query);
		$row = $database->loadResult();
		if ($row > 0 ) {
			$query = "UPDATE #__users SET `name` = '".$user['displayname']."', `email` = '".$user['mail']."', `designation` = '".$user['title']."', `department` = '".$user['department']."', `phone` = '".$user['homephone']."', `cell` = '".$user['mobile']."', `fax` = '".$user['facsimiletelephonenumber']."' WHERE `username` = '".$username."';";
			$database->setQuery($query);
			if ($database->query()) {
				$update = (int)$update + 1;
			} else {
				$faildd = (int)$faildd + 1;
			}
		} else {
			$query = "INSERT INTO #__users (`id`, `name`, `username`, `email`, `block`, `sendEmail`, `registerDate`, `designation`, `department`, `phone`, `cell`, `fax`) VALUES  (NULL, '".$user['displayname']."', '".$user['samaccountname']."', '".$user['mail']."', '0', '0', NOW(), '".$user['title']."', '".$user['department']."', '".$user['homephone']."', '".$user['mobile']."', '".$user['facsimiletelephonenumber']."')";
			$database->setQuery($query);
			if ($database->query()) {
				$addnew = (int)$addnew + 1;
			} else {
				$faildd = (int)$faildd + 1;
			}
		}
	}
	
	print 'New Users: ' .$addnew . '<br>';
	print 'Updated Users: ' .$update . '<br>';
	print 'Failed: ' .$faildd . '<br>';
?>
<?php
}

function get_members($group=FALSE,$inclusive=FALSE) {
	$plugin = JPluginHelper::getPlugin('authentication', 'jmapmyldap');
	$pluginParams = new JRegistry();
	$pluginParams->loadString($plugin->params);
	
	//$hostnm = $pluginParams->get('host');
	//$cnusnm = $pluginParams->get('connect_username');
	//$cnusps = $pluginParams->get('connect_password');
	//$basedn = $pluginParams->get('base_dn');
	
	
    $ldap_host = "10.1.20.1";
    
    $ldap_dn = "OU=Emirates Post,DC=epost,DC=local";
    $ldap_usr_dom = "@".$ldap_host;

    $user = "spsadmin";
    $password = "spsadmin";

	
	
	
	
	
    // Active Directory server
    //$ldap_host = $hostnm;
	//$ldap_dn = "OU=innokat,OU=innokat,$basedn";
    //$ldap_usr_dom = "@".$ldap_host;
    // Active Directory user
    //$user = $cnusnm;
    //$password = $cnusps;
    // User attributes we want to keep
    // List of User Object properties:
    // http://www.dotnetactivedirectory.com/Understanding_LDAP_Active_Directory_User_Object_Properties.html
    $keep = array(
        "samaccountname",
        "distinguishedname",
		"givenname",
		"displayname",
		"mailnickname",
		"name",
		"userprincipalname",
		"objectcategory",
		"mail",
		"title",
		"department",
		"facsimiletelephonenumber",
		"homephone",
		"mobile"
    );
    // Connect to AD
    $ldap = ldap_connect($ldap_host) or die("Could not connect to LDAP");
	//ldap_bind($ldap,$user.$ldap_usr_dom,$password) or die("Could not bind to LDAP");
    ldap_bind($ldap,$user,$password) or die("Could not bind to LDAP");
    $query .= "(&(objectClass=user)(objectCategory=person))";
    // Uncomment to output queries onto page for debugging
    // print_r($query);
 
    // Search AD
    $results = ldap_search($ldap,$ldap_dn,$query);
    $entries = ldap_get_entries($ldap, $results);
    // Remove first entry (it's always blank)
    array_shift($entries);
    $output = array(); // Declare the output array
     $i = 0; // Counter
    // Build output array
    foreach($entries as $u) {
        foreach($keep as $x) {
        	// Check for attribute
    		if(isset($u[$x][0])) $attrval = $u[$x][0]; else $attrval = NULL;

        	// Append attribute to output array
        	$output[$i][$x] = $attrval;
        }
        $i++;
    }
    return $output;
}

function editDepartments($option, $row, $usersList, $toAddUsersList){
global $mainframe, $jlistConfig;
$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.departments') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.departments_name.value == "" 
			 || form.departments_name_ar.value == "" 
			 || form.departments_email.value == ""
			 || tinyMCE.get('departments_description').getContent() == ""
			 || tinyMCE.get('departments_description_ar').getContent() == "" ){
             alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
			return false;
        } else {
			if(form.departments_email.value != ""){
				var x=form.departments_email.value;
				var atpos=x.indexOf("@");
				var dotpos=x.lastIndexOf(".");
				if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
				  {
				  alert("Please enter a valid email address");
				  return false;
				  }
			}
            allSelected(document.adminForm['users_selected[]']);
            Joomla.submitform( pressbutton );
            return true;
        }
    }
    
    // moves elements from one select box to another one
    function moveOptions(from,to) {
      // Move them over
      for (var i=0; i<from.options.length; i++) {
           var o = from.options[i];
           if (o.selected) {
               to.options[to.options.length] = new Option( o.text, o.value, false, false);
           }
       }
       // Delete them from original
       for (var i=(from.options.length-1); i>=0; i--) {
            var o = from.options[i];
            if (o.selected) {
                from.options[i] = null;
            }
       }
       from.selectedIndex = -1;
       to.selectedIndex = -1;
    }

    function allSelected(element) {
       for (var i=0; i<element.options.length; i++) {
            var o = element.options[i];
                o.selected = true;
       }
    }
    </script>    
    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_DEPARTMENTS_EDIT_TITLE') : JText::_('COM_EPOSTS_BACKEND_DEPARTMENTS_ADD_TITLE');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NAME_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="departments_name" value="<?php echo htmlspecialchars($row->departments_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NAME_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="departments_name_ar" value="<?php echo htmlspecialchars($row->departments_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENTS_EMAIL_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                    <input name="departments_email" value="<?php echo htmlspecialchars($row->departments_email, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_DESC_TITLE')."<font color='#990000'>*</font>"; ?></strong><br />
                                 <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'departments_description',  @$row->departments_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_DESC_TITLE_AR')."<font color='#990000'>*</font>"; ?></strong><br />
                                  <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'departments_description_ar',  @$row->departments_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>                                                   
                          </table>
                          
                <table class="admintable">
                    <tr>
                        <td colspan="4">
                            <?php echo '<b>'.JText::_('COM_EPOSTS_BACKEND_DEPARTMENTS_EDIT_DESC').'</b>'; ?>
                        </td>
                    </tr>
                    <tr>    
                        <td><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENTS_SELECT_USER_TITLE');?></td>
                        <td width="20%">&nbsp;</td>
                        <td><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENTS_EXISTS_USER_TITLE');?></td>
                    </tr>
                    <tr>
                        <td width="40%"><?php echo $toAddUsersList;?></td>
                        <td width="20%">
                          <input style="width: 50px" type="button" name="Button" value="&gt;" onClick="moveOptions(document.adminForm.users_not_selected, document.adminForm['users_selected[]'])" />
                            <br /><br />
                            <input style="width: 50px" type="button" name="Button" value="&lt;" onClick="moveOptions(document.adminForm['users_selected[]'],document.adminForm.users_not_selected)" />
                            <br /><br />
                        </td>
                        <td width="40%"><?php echo $usersList;?></td>
                    </tr>
                    <tr>
                        <td colspan="4"><?php echo JText::_('COM_EPOSTS_BACKEND_DEPARTMENTS_EDIT_SELECT_INFO');?></td>
                    </tr>
                    <tr>
                        <td><strong>Department Map (image file)</strong><br />
                        <input name="department_file" type="file"/>
                                                                 
                    </tr>
                </table>                          
                          
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="departments_members" value="<?php  //echo $row->departments_members;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.departments" />
    </form>
<?php
}   

/**********************************************
/ Templates
/ ********************************************/

 /* Display the submenu for templates
 */

function controlPanelTemplate($option) {
    global $mainframe;
    jimport( 'joomla.html.pane');
    
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>

<!-- ICON begin -->

   <div class="adminform">
    <div class="cpanel-left">
     <div id="cpanel">
       <div class="icon-wrapper">
          <div class="icon"> 
				<a href="index.php?option=com_eposts&task=templates.list.cats" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_TEMPLATES_CATS');?>">
	            <img src="components/com_eposts/images/template.png" width="48px" height="48px" align="middle" border="0"/>
                <br />
	            <?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_TEMPLATES_CATS');?>
	            </a>
			</div>
		</div>

       <div class="icon-wrapper">
          <div class="icon">
	            <a href="index.php?option=com_eposts&task=templates.list.files" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_TEMPLATES_FILES');?>">
	            <img src="components/com_eposts/images/template.png" width="48px" height="48px" align="middle" border="0"/>
	            <br />
	            <?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_TEMPLATES_FILES');?>
	            </a>
            </div>
		</div>

       <div class="icon-wrapper">
          <div class="icon">
                <a href="index.php?option=com_eposts&task=templates.list.details" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_TEMPLATES_DETAILS');?>">
                <img src="components/com_eposts/images/template.png" width="48px" height="48px" align="middle" border="0"/>
                <br />
                <?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_TEMPLATES_DETAILS');?>
                </a>
            </div>
        </div>
        
       <div class="icon-wrapper">
          <div class="icon">
	            <a href="index.php?option=com_eposts&task=templates.list.summary" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_TEMPLATES_SUMMARY');?>">
	            <img src="components/com_eposts/images/template.png" width="48px" height="48px" align="middle" border="0"/>
	            <br />
	            <?php echo JText::_('COM_EPOSTS_BACKEND_CPANEL_TEMPLATES_SUMMARY');?>
	            </a>
			</div>
		</div>

       <div class="icon-wrapper">
          <div class="icon">
	            <a href="index.php?option=com_eposts&task=css.edit" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_BACKEND_EDIT_CSS_TITLE');?>">
	            <img src="components/com_eposts/images/css.png" width="48px" height="48px" align="middle" border="0"/>
	            <br />
	            <?php echo JText::_('COM_EPOSTS_BACKEND_EDIT_CSS_TITLE');?>
	            </a>
			</div>
		</div>

       <div class="icon-wrapper">
          <div class="icon">
                <a href="index.php?option=com_eposts&task=language.edit" style="text-decoration:none;" title="<?php echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_TITLE');?>">
                <img src="components/com_eposts/images/langmanager.png" width="48px" height="48px" align="middle" border="0"/>
                <br />
                <?php echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_TITLE');?>
                </a>
            </div>
        </div>
      </div>
    </div>

    <!-- ICON END -->



<!-- TABS begin -->
    <div class="cpanel-right">
      <div id="panel-sliders" class="pane-sliders">
        <div style="display: none;">
            <div></div>
        </div>
        <div class="panel">    

              <?php	$pane =& JPane::getInstance('Tabs');
                    echo $pane->startPane('temppaneltabs');
                    echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_TEMPPANEL_TABTEXT_INFO'),'Info');            
              ?>
              <form action="index.php" method="post" name="adminlist"> 
              <table width="100%">
   		       <tr>
   		          <td valign="top" align="left" width="100%">
                     <?php echo  JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEAD'); ?>
                     <?php echo  JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEAD_INFO').JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEAD_INFO2'); ?>
                  </td>
               </tr>
              </table>
              <?php
               echo $pane->endPanel();
               echo $pane->endPane('temppaneltabs');?>
             </form>
        </div>
<!-- TABS END -->
            </div>
        </div>
      </div>
   
	

<?php
}


// Templates list

function listTemplatesCats($rows, $option){
	global $mainframe, $jlistConfig;

    $temp_typ = array();
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP1');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP2');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP3');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP4');
    
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
	JHTML::_('behavior.tooltip');
?>
	<form action="index.php" method="post" name="adminForm" id="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
	  <tr align="right">
		  <td colspan="7"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_LOCKED_DESC'); ?></td>
	  </tr>
		<tr>
			<th width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_TITLE')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_TYP')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_ACTIVE')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_LOCKED')." "; ?></th>
        </tr>
		<?php
		$k = 0;
		for($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			$link 		= 'index.php?option=com_eposts&task=templates.edit.cats&hidemainmenu=1&cid='.$row->id;
			$checked 	= JHTML::_('grid.checkedout', $row, $i );
			?>
		<tr class="<?php echo "row$k"; ?>">
			<td><?php echo $checked; ?></td>
			<td><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT');?>"><?php echo $row->template_name; ?></a></td>

            <td>
            <?php
            echo $temp_typ[$row->template_typ -1]; ?>
            </td>

            <td>
            <?php
            if ($row->template_active > 0) { ?>
                <img src="components/com_eposts/images/active.png" width="12px" height="12px" align="middle" border="0"/>
            <?php } ?>
            </td>
            
			<td>
            <?php
            if ($row->locked > 0) { ?>
                <img src="components/com_eposts/images/active.png" width="12px" height="12px" align="middle" border="0"/>
            <?php } ?>
            </td>

            <?php $k = 1 - $k;  } ?>
		</tr>
	</table>
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="hidemainmenu" value="0">
</form>

<?php
}

function editTemplatesCats($option, $row){
    global $mainframe, $jlistConfig;
    $editor =& JFactory::getEditor();
    jimport( 'joomla.html.pane');
    ?>
    <script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'templates.cancel.cats') {
			submitform( pressbutton );
			return;
		}

		if (form.template_name.value == ""){
			alert( "<?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ERROR_TITLE');?>" );
		} else {
			submitform( pressbutton );
		}
	}
	</script>

	<form action="index.php" method="post" name="adminForm" id="adminForm">
	
<?php    
$pane =& JPane::getInstance('Tabs');
echo $pane->startPane('template_cat');
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_CATTITLE'),'catedit');
?>    
	<table width="100%" border="0">
		<tr>
			<td width="100%" valign="top">
			<table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT') : JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ADD');?></th>
		      	</tr>
		      	<tr>
		      		<td valign="top" align="left" width="100%">
		      			<table>
		  					<tr valign="top">
		    					<td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME')." "; ?></strong><br />
		    					    <?php 
                                          if ($row->locked){
                                             $dis = 'disabled="disabled"'; 	
                                          } else {
                                             $dis = '';
                                          } ?>
                                    <input name="template_name" value="<?php echo htmlspecialchars($row->template_name); ?>" <?php echo $dis; ?> size="70" maxlength="64"/>
		       					</td>
                                <td><?php if (!$dis) { echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME_DESCRIPTION'); 
                                        } else { echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME_DESCRIPTION').'<br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TITLE_NOT_ALLOWED_TO_CHANGE_DESK'); }?>
                                   </td>
   		  					</tr>
                            <tr valign="top">
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NOTE_TITLE')." "; ?></strong><br />
                                    <textarea name="note" rows="1" cols="80"><?php echo htmlspecialchars($row->note); ?></textarea>
                                   </td>
                                   <td><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NOTE_DESC'); ?>
                                   </td>
                            </tr>
                            <tr valign="top">
                                <td><strong><?php echo JText::_('COM_EPOSTS_EDIT_TEMPL_AMOUNT_COL_TITLE')." "; ?></strong><br />
                                    <input name="cols" value="<?php echo $row->cols; ?>" size="5" maxlength="1"/> 
                                   </td>
                                   <td><?php echo JText::_('COM_EPOSTS_EDIT_TEMPL_AMOUNT_COL_DESC'); ?>
                                   </td>
                              </tr>
                            
                              <tr>
		  						<td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TEXT')." "; ?></strong><br />
		  							<?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                    	echo $editor->display( 'template_text',  @$row->template_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_text" rows="30" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_text)); ?></textarea>
                                        <?php
                                        } ?>
		  						</td>
		       					<td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_CATS_DESC').'<br /><br />'.JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_CATS_DESC2').'<br /><br />'.JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_CATS_DESC3');
                                   ?>
		       					</td>
		  					</tr>

                            <tr>
		      		           <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_CATS_INFO_TITLE')." "; ?>
                               </th>
		      	            </tr>
                            <tr>
                               <td>
                               <br />
                                <div><img src="components/com_eposts/images/downloads_cats.gif" alt="Templates Infos" border="1" /></div>
                              </td>
                              <td valign="top"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_CATS_INFO_TEXT');?>
                              </td>
                            </tr>

		  				</table>
		  			</td>
		  		</tr>
			</table>
			</td>
		</tr>
	</table>
    
<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TABTEXT_EDIT_HEADER'),'editheader');
?>    
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT') : JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ADD');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table>
                            <!-- HEADER LAYOUT BEGIN -->
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_HEADER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_header_text',  @$row->template_header_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_header_text" rows="10" cols="80"><?php echo htmlspecialchars($row->template_header_text); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_HEADER_DESC').'<br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_PREVIEW_NOTE');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.$row->template_header_text;
                                   ?>
                                   </td>
                            </tr>
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SUBHEADER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_subheader_text',  @$row->template_subheader_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_subheader_text" rows="10" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_subheader_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SUBHEADER_DESC');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_subheader_text);
                                   ?>
                                   </td>
                            </tr>
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_FOOTER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_footer_text',  @$row->template_footer_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_footer_text" rows="4" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_footer_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_FOOTER_FILES_CATS_DESC');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_footer_text);
                                   ?>
                                   </td>
                            </tr>                            


                            <!-- HEADER LAYOUT END -->                            
                            <tr>
                                 <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEADER_INFO_TITLE')." "; ?>
                               </th>
                              </tr>
                            <tr>
                               <td>
                               <br />
                                <div><img src="components/com_eposts/images/eposts_header_sample.gif" alt="Templates Infos" border="1" /></div>
                              </td>
                              <td valign="top"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEADER_INFO_DESC');?>
                              </td>
                            </tr>

                          </table>
                      </td>
                  </tr>
            </table>
            </td>
        </tr>
    </table>
<?php
echo $pane->endPanel();
echo $pane->endPane('template_cat');
?>

<br /><br />

		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
        <input type="hidden" name="tempname" value="<?php echo htmlspecialchars($row->template_name); ?>" />
        <input type="hidden" name="locked" value="<?php echo $row->locked; ?>" />        
		<input type="hidden" name="task" value="" />
	</form>

<?php
}

// Templates list

function listTemplatesFiles($rows, $option){
	global $mainframe, $jlistConfig;

    $temp_typ = array();
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP1');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP2');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP3');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP4');
    
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
	JHTML::_('behavior.tooltip');
?>
	<form action="index.php" method="post" name="adminForm" id="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
	  <tr align="right">
		  <td colspan="7"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_LOCKED_DESC'); ?></td>
	  </tr>
		<tr>
			<th width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_TITLE')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_TYP')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_ACTIVE')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_LOCKED')." "; ?></th>
        </tr>
		<?php
		$k = 0;
		for($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			$link 		= 'index.php?option=com_eposts&task=templates.edit.files&hidemainmenu=1&cid='.$row->id;
			$checked 	= JHTML::_('grid.checkedout', $row, $i );
			?>
		<tr class="<?php echo "row$k"; ?>">
			<td><?php echo $checked; ?></td>
			<td><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT');?>"><?php echo $row->template_name; ?></a></td>

            <td>
            <?php
            echo $temp_typ[$row->template_typ -1]; ?>
            </td>

            <td>
            <?php
            if ($row->template_active > 0) { ?>
                <img src="components/com_eposts/images/active.png" width="12px" height="12px" align="middle" border="0"/>
            <?php } ?>

            </td>
            
            <td>
            <?php
            if ($row->locked > 0) { ?>
                <img src="components/com_eposts/images/active.png" width="12px" height="12px" align="middle" border="0"/>
            <?php } ?>
            </td>

            <?php $k = 1 - $k;  } ?>
		</tr>
	</table>
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="hidemainmenu" value="0" />
</form>

<?php
}

function listTemplatesDetails($rows, $option){
    global $mainframe, $jlistConfig;

    $temp_typ = array();
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP1');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP2');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP3');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP4');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP5');
    
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
    JHTML::_('behavior.tooltip');
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr align="right">
          <td colspan="7"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_LOCKED_DESC'); ?></td>
      </tr>
        <tr>
            <th width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_TITLE')." "; ?></th>
            <th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_TYP')." "; ?></th>
            <th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_ACTIVE')." "; ?></th>
            <th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_LOCKED')." "; ?></th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link         = 'index.php?option=com_eposts&task=templates.edit.details&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td><?php echo $checked; ?></td>
            <td><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT');?>"><?php echo $row->template_name; ?></a></td>

            <td>
            <?php
            echo $temp_typ[$row->template_typ -1]; ?>
            </td>

            <td>
            <?php
            if ($row->template_active > 0) { ?>
                <img src="components/com_eposts/images/active.png" width="12px" height="12px" align="middle" border="0"/>
            <?php } ?>

            </td>
            
            <td>
            <?php
            if ($row->locked > 0) { ?>
                <img src="components/com_eposts/images/active.png" width="12px" height="12px" align="middle" border="0"/>
            <?php } ?>
            </td>

            <?php $k = 1 - $k;  } ?>
        </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="hidemainmenu" value="0" />
</form>

<?php
}

function editTemplatesFiles($option, $row){
global $mainframe, $jlistConfig;

jimport( 'joomla.html.pane');
$editor =& JFactory::getEditor();
    ?>
    <script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'templates.cancel.files') {
			submitform( pressbutton );
			return;
		}

		if (form.template_name.value == ""){
			alert( "<?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ERROR_TITLE');?>" );
		} else {
			submitform( pressbutton );
		}
	}
	</script>

	<form action="index.php" method="post" name="adminForm" id="adminForm">
    
<?php    
$pane =& JPane::getInstance('Tabs');
echo $pane->startPane('template_files');
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_FILESTITLE'),'filesedit');
?>   	
	<table width="100%" border="0">
		<tr>
			<td width="100%" valign="top">
			<table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT') : JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ADD');?></th>
		      	</tr>
		      	<tr>
		      		<td valign="top" align="left" width="100%">
		      			<table>
		  					<tr valign="top">
		    					<td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME')." "; ?></strong><br />
		    						<?php 
                                          if ($row->locked){
                                             $dis = 'disabled="disabled"';     
                                          } else {
                                             $dis = '';
                                          } ?>
                                    <input name="template_name" value="<?php echo htmlspecialchars($row->template_name); ?>" <?php echo $dis; ?> size="70" maxlength="64"/>
		       					</td>
		       					<td><?php if (!$dis) { echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME_DESCRIPTION'); 
                                        } else { echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME_DESCRIPTION').'<br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TITLE_NOT_ALLOWED_TO_CHANGE_DESK'); }?>
                                   </td>
		  					</tr>

                            <tr valign="top">
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NOTE_TITLE')." "; ?></strong><br />
                                    <textarea name="note" rows="1" cols="80"><?php echo htmlspecialchars($row->note); ?></textarea>
                                   </td>
                                   <td><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NOTE_DESC'); ?>
                                   </td>
                              </tr>
                              
                             <tr valign="top">
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_CHECKBOX_TITLE')." "; ?></strong><br />
                                   <?php 
                                   $checkboxed = array();
                                   $checkboxed[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_FE_YES'));
                                   $checkboxed[] = JHTML::_('select.option', '1', JText::_('COM_EPOSTS_FE_NO'));
                                   echo JHTML::_('select.genericlist', $checkboxed, 'checkbox_off', 'size="1" class="inputbox"', 'value', 'text', $row->checkbox_off );
                                  ?>                    
                                   </td>
                                   <td><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_CHECKBOX_DESC').' '.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_CHECKBOX_DESC2'); ?>
                                   </td>
                              </tr>                              

                              <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SYMBOLE_TITLE')." "; ?></strong><br />
                                   <?php 
                                   $symbols = array();
                                   $symbols[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_FE_YES'));
                                   $symbols[] = JHTML::_('select.option', '1', JText::_('COM_EPOSTS_FE_NO'));
                                   echo JHTML::_('select.genericlist', $symbols, 'symbol_off', 'size="1" class="inputbox"', 'value', 'text', $row->symbol_off );
                                  ?>                    
                                   </td>
                                   <td><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SYMBOLE_DESC'); ?>
                                   </td>
                              </tr>                            
                            
		  					<tr>
		  						<td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TEXT')." "; ?></strong><br />
		  							<?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                    	echo $editor->display( 'template_text',  @$row->template_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_text" rows="30" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_text)); ?></textarea>
                                        <?php
                                        } ?>
		  						</td>
		       					<td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_FILES_DESC').'<br /><br />'.JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_FILES_DESC2').'<br /><br />'.JText::_('COM_EPOSTS_BACKEND_TEMPLATE_EDIT_CUSTOM_FIELD_INFO').'<br /><br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_INFO_LIGHTBOX').'<br /><br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_INFO_LIGHTBOX2');
                                   ?>
		       					</td>
		  					</tr>

                            <tr>
	      		              <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_FILES_INFO_TITLE')." "; ?></th>
           	                  </tr>
                              <tr><td>
                              <br />
                              <div><img src="components/com_eposts/images/downloads_files.gif" alt="Templates Infos" border="1" /></div>
                              </td>
                              <td valign="top"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_FILES_INFO_TEXT');?>
                              </td>
                              </tr>

		  				</table>
		  			</td>
		  		</tr>
			</table>
			</td>
		</tr>
	</table>
    
<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TABTEXT_EDIT_HEADER'),'editheader');
?>       
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT') : JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ADD');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table>
                            <!-- HEADER LAYOUT BEGIN -->
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_HEADER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_header_text',  @$row->template_header_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_header_text" rows="10" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_header_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_HEADER_DESC').'<br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_PREVIEW_NOTE');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_header_text);
                                   ?>
                                   </td>
                            </tr>
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SUBHEADER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_subheader_text',  @$row->template_subheader_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_subheader_text" rows="10" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_subheader_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SUBHEADER_DESC');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_subheader_text);
                                   ?>
                                   </td>
                            </tr>
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_FOOTER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_footer_text',  @$row->template_footer_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_footer_text" rows="4" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_footer_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_FOOTER_FILES_CATS_DESC');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_footer_text);
                                   ?>
                                   </td>
                            </tr>                            


                            <!-- HEADER LAYOUT END -->                            
                            <tr>
                                 <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEADER_INFO_TITLE')." "; ?>
                               </th>
                              </tr>
                            <tr>
                               <td>
                               <br />
                                <div><img src="components/com_eposts/images/eposts_header_sample.gif" alt="Templates Infos" border="1" /></div>
                              </td>
                              <td valign="top"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEADER_INFO_DESC');?>
                              </td>
                            </tr>

                          </table>
                      </td>
                  </tr>
            </table>
            </td>
        </tr>
    </table>
<?php
echo $pane->endPanel();
echo $pane->endPane('template_files');
?>    
<br /><br />

		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
        <input type="hidden" name="tempname" value="<?php echo htmlspecialchars($row->template_name); ?>" />
        <input type="hidden" name="locked" value="<?php echo $row->locked; ?>" />
        <input type="hidden" name="task" value="" />
	</form>

<?php
}

function editTemplatesDetails($option, $row){
global $mainframe, $jlistConfig;

jimport( 'joomla.html.pane');
$editor =& JFactory::getEditor();
    ?>
    <script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
        var form = document.adminForm;
        if (pressbutton == 'templates.cancel.details') {
            submitform( pressbutton );
            return;
        }

        if (form.template_name.value == ""){
            alert( "<?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ERROR_TITLE');?>" );
        } else {
            submitform( pressbutton );
        }
    }
    </script>

    <form action="index.php" method="post" name="adminForm" id="adminForm">

<?php    
$pane =& JPane::getInstance('Tabs');
echo $pane->startPane('template_details');
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_DETAILSTITLE'),'detailsedit');
?>      
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT') : JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ADD');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table>
                              <tr valign="top">
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME')." "; ?></strong><br />
                                    <?php 
                                          if ($row->locked){
                                             $dis = 'disabled="disabled"';     
                                          } else {
                                             $dis = '';
                                          } ?>
                                    <input name="template_name" value="<?php echo htmlspecialchars($row->template_name); ?>" <?php echo $dis; ?> size="70" maxlength="64"/>
                                   </td>
                                   <td><?php if (!$dis) { echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME_DESCRIPTION'); 
                                        } else { echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME_DESCRIPTION').'<br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TITLE_NOT_ALLOWED_TO_CHANGE_DESK'); }?>
                                   </td>
                              </tr>
                              <tr valign="top">
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NOTE_TITLE')." "; ?></strong><br />
                                    <textarea name="note" rows="1" cols="80"><?php echo htmlspecialchars($row->note); ?></textarea>
                                   </td>
                                   <td><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NOTE_DESC'); ?>
                                   </td>
                              </tr>
                              <tr>
                                  <td valign="top"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_text',  @$row->template_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_text" rows="35" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_DETAILS_DESC').'<br /><br />'.JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_DETAILS_DESC_FOR_TABS').'<br /><br />'.JText::_('COM_EPOSTS_BACKEND_TEMPLATE_EDIT_CUSTOM_FIELD_INFO').
                                        '<br /><br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_INFO_LIGHTBOX').'<br /><br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_INFO_LIGHTBOX2');
                                   ?>
                                   </td>
                              </tr>

                            <tr>
                                <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_DETAILS_INFO_TITLE')." "; ?></th>
                                 </tr>
                              <tr><td>
                              <br />
                              <div><img src="components/com_eposts/images/downloads_details.gif" alt="Templates Infos" border="1" /></div>
                              </td>
                              <td valign="top"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_DETAILS_INFO_TEXT');?>
                              </td>
                              </tr>

                          </table>
                      </td>
                  </tr>
            </table>
            </td>
        </tr>
    </table>
    
<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TABTEXT_EDIT_HEADER'),'editheader');
?>       
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT') : JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ADD');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table>
                            <!-- HEADER LAYOUT BEGIN -->
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_HEADER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_header_text',  @$row->template_header_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_header_text" rows="10" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_header_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_HEADER_DESC').'<br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_PREVIEW_NOTE');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_header_text);
                                   ?>
                                   </td>
                            </tr>
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SUBHEADER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_subheader_text',  @$row->template_subheader_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_subheader_text" rows="10" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_subheader_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SUBHEADER_DETAIL_DESC');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_subheader_text);
                                   ?>
                                   </td>
                            </tr>
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_FOOTER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_footer_text',  @$row->template_footer_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_footer_text" rows="4" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_footer_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_FOOTER_OTHER_DESC');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_footer_text);
                                   ?>
                                   </td>
                            </tr>                            


                            <!-- HEADER LAYOUT END -->                            
                            <tr>
                                 <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEADER_INFO_TITLE')." "; ?>
                               </th>
                              </tr>
                            <tr>
                               <td>
                               <br />
                                <div><img src="components/com_eposts/images/eposts_header_sample.gif" alt="Templates Infos" border="1" /></div>
                              </td>
                              <td valign="top"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEADER_INFO_DESC');?>
                              </td>
                            </tr>

                          </table>
                      </td>
                  </tr>
            </table>
            </td>
        </tr>
    </table>
<?php
echo $pane->endPanel();
echo $pane->endPane('template_details');
?>    
<br /><br />

        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="hidemainmenu" value="0" />
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="id" value="<?php echo $row->id; ?>" />
        <input type="hidden" name="tempname" value="<?php echo htmlspecialchars($row->template_name); ?>" />
        <input type="hidden" name="locked" value="<?php echo $row->locked; ?>" />
        <input type="hidden" name="task" value="" />
    </form>

<?php
}

// Templates list

function listTemplatesSummary($rows, $option){
	global $mainframe, $jlistConfig;

    $temp_typ = array();
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP1');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP2');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP3');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP4');
    $temp_typ[] = JText::_('COM_EPOSTS_BACKEND_TEMP_TYP5'); 
    
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
    
	JHTML::_('behavior.tooltip');
?>
	<form action="index.php" method="post" name="adminForm" id="adminForm">
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
	  <tr align="right">
		  <td colspan="7"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_LOCKED_DESC'); ?></td>
	  </tr>
		<tr>
			<th width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_TITLE')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_TYP')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_ACTIVE')." "; ?></th>
			<th class="title"><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPLIST_LOCKED')." "; ?></th>
        </tr>
		<?php
		$k = 0;
		for($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			$link 		= 'index.php?option=com_eposts&task=templates.edit.summary&hidemainmenu=1&cid='.$row->id;
			$checked 	= JHTML::_('grid.checkedout', $row, $i );
			?>
		<tr class="<?php echo "row$k"; ?>">
			<td><?php echo $checked; ?></td>
			<td><a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT');?>"><?php echo $row->template_name; ?></a></td>

            <td>
            <?php
            echo $temp_typ[$row->template_typ -1]; ?>
            </td>

            <td>
            <?php
            if ($row->template_active > 0) { ?>
                <img src="components/com_eposts/images/active.png" width="12px" height="12px" align="middle" border="0"/>
            <?php } ?>
            </td>

			<td>
            <?php
            if ($row->locked > 0) { ?>
                <img src="components/com_eposts/images/active.png" width="12px" height="12px" align="middle" border="0"/>
            <?php } ?>
            </td>

            <?php $k = 1 - $k;  } ?>
		</tr>
	</table>
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="hidemainmenu" value="0" />
</form>

<?php
}

function editTemplatesSummary($option, $row){
global $mainframe, $jlistConfig;

jimport( 'joomla.html.pane');
$editor =& JFactory::getEditor();
    ?>
    <script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'templates.cancel.summary') {
			submitform( pressbutton );
			return;
		}

		if (form.template_name.value == ""){
			alert( "<?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ERROR_TITLE');?>" );
		} else {
			submitform( pressbutton );
		}
	}
	</script>

	<form action="index.php" method="post" name="adminForm" id="adminForm">

<?php    
$pane =& JPane::getInstance('Tabs');
echo $pane->startPane('template_summary');
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SUMTITLE'),'summaryedit');
?>   
    
	<table width="100%" border="0">
		<tr>
			<td width="100%" valign="top">
			<table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT') : JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ADD');?></th>
		      	</tr>
		      	<tr>
		      		<td valign="top" align="left" width="100%">
		      			<table>
		  					<tr valign="top">
		    					<td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME')." "; ?></strong><br />
		    					<?php 
                                          if ($row->locked){
                                             $dis = 'disabled="disabled"';     
                                          } else {
                                             $dis = '';
                                          } ?>
                                    <input name="template_name" value="<?php echo htmlspecialchars($row->template_name); ?>" <?php echo $dis; ?> size="70" maxlength="64"/>
		       					</td>
		       					<td><?php if (!$dis) { echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME_DESCRIPTION'); 
                                        } else { echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NAME_DESCRIPTION').'<br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TITLE_NOT_ALLOWED_TO_CHANGE_DESK'); }?>
                                   </td>
		  					</tr>
                            <tr valign="top">
                                <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NOTE_TITLE')." "; ?></strong><br />
                                    <textarea name="note" rows="1" cols="80"><?php echo htmlspecialchars($row->note); ?></textarea>
                                   </td>
                                   <td><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_NOTE_DESC'); ?>
                                   </td>
                              </tr>
		  					<tr>
		  						<td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TEXT')." "; ?></strong><br />
		  							<?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                    	echo $editor->display( 'template_text',  @$row->template_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_text" rows="30" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_text)); ?></textarea>
                                        <?php
                                        } ?>
		  						</td>
		       					<td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_FINAL_DESC');
                                   ?>
		       					</td>
		  					</tr>

                            <tr>
	      		              <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_FINAL_INFO_TITLE')." "; ?></th>
           	                  </tr>
                              <tr><td>
                              <br />
                              <div><img src="components/com_eposts/images/summary.gif" alt="Templates Infos" border="1" /></div>
                              </td>
                              <td valign="top"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_FINAL_INFO_TEXT');?>
                              </td>
                              </tr>

		  				</table>
		  			</td>
		  		</tr>
			</table>
			</td>
		</tr>
	</table>
    
<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_TABTEXT_EDIT_HEADER'),'editheader');
?>       
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_EDIT') : JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_ADD');?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table>
                            <!-- HEADER LAYOUT BEGIN -->
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_HEADER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_header_text',  @$row->template_header_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_header_text" rows="10" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_header_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_HEADER_DESC').'<br />'.JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_PREVIEW_NOTE');                                           
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_header_text);
                                   ?>
                                   </td>
                            </tr>
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SUBHEADER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_subheader_text',  @$row->template_subheader_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_subheader_text" rows="10" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_subheader_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_SUBHEADER_SUMMARY_DESC');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_subheader_text);
                                   ?>
                                   </td>
                            </tr>
                                  <td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_FOOTER_TEXT')." "; ?></strong><br />
                                      <?php
                                    if ($jlistConfig['layouts.editor'] == "1") {
                                        echo $editor->display( 'template_footer_text',  @$row->template_footer_text , '100%', '500', '80', '5' ) ;
                                        } else {?>
                                        <textarea name="template_footer_text" rows="4" cols="80"><?php echo stripslashes(htmlspecialchars($row->template_footer_text)); ?></textarea>
                                        <?php
                                        } ?>
                                  </td>
                                   <td valign="top">
                                   <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_TEMPEDIT_FOOTER_OTHER_DESC');
                                        echo '<br /><br /><font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_CATSLIST_LINK_TEXT').':</font><br /><br />'.stripslashes($row->template_footer_text);
                                   ?>
                                   </td>
                            </tr>                            


                            <!-- HEADER LAYOUT END -->                            
                            <tr>
                                 <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEADER_INFO_TITLE')." "; ?>
                               </th>
                              </tr>
                            <tr>
                               <td>
                               <br />
                                <div><img src="components/com_eposts/images/eposts_header_sample.gif" alt="Templates Infos" border="1" /></div>
                              </td>
                              <td valign="top"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_TEMPLATES_HEADER_INFO_DESC');?>
                              </td>
                            </tr>

                          </table>
                      </td>
                  </tr>
            </table>
            </td>
        </tr>
    </table>
<?php
echo $pane->endPanel();
echo $pane->endPane('template_summary');
?>    
<br /><br />

		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
        <input type="hidden" name="tempname" value="<?php echo htmlspecialchars($row->template_name); ?>" />
        <input type="hidden" name="locked" value="<?php echo $row->locked; ?>" />
		<input type="hidden" name="task" value="" />
	</form>

<?php
}

// css edit
function cssEdit($option, $css_file, $css_writable) {

    //$css_file = stripslashes($css_file);
    $f=fopen($css_file,"r");
    $css_text = fread($f, filesize($css_file));
    $css_text = htmlspecialchars($css_text);
    
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
    ?>

	<form action="index.php" method="post" name="adminForm" id="adminForm">
	
	<table width="100%" border="0">
		<tr>
			<td width="100%" valign="top">
			<table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_EDIT_CSS_TITLE');?> </th>
		      	</tr>
		      	<tr>
		      		<td valign="top" align="left" width="100%">
		      			<table>
		  					<tr colspan="2" valign="top">
		    					<td><strong>
                                    <?php
                                        echo JText::_('COM_EPOSTS_BACKEND_EDIT_CSS_WRITE_STATUS_TEXT')." ";
                                        if ($css_writable) {
                                            echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_CSS_FILE_WRITABLE_YES');
                                        } else {
                                            echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_CSS_FILE_WRITABLE_NO');
                                            ?>
                                            </strong><br />
                                            <?php echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_CSS_FILE_WRITABLE_INFO'); ?><br />
                                        <?php } ?>
		       					</td>
		  					</tr>
		  					<tr>
		  						<td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EDIT_CSS_FIELD_TITLE').": ".$css_file; ?></strong><br />
                                    <textarea name="css_text" rows="36" cols="100"><?php echo $css_text ?></textarea>
		  						</td>
		  					</tr>
		  				</table>
		  			</td>
		  		</tr>
			</table>
			</td>
		</tr>
	</table>
<br /><br />

		<input type="hidden" name="css_file" value="<?php echo $css_file; ?>" />
		<input type="hidden" name="hidemainmenu" value="0" />
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="task" value="" />
	</form>
	<?php
}

// language edit
function languageEdit($option, $lang_file, $lang_writable) {

    //$lang_file = stripslashes($lang_file);
    $f=fopen($lang_file,"r");
    $lang_text = fread($f, filesize($lang_file));
    $lang_text = htmlspecialchars($lang_text);
    
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
   ?>
    
	<form action="index.php" method="post" name="adminForm" id="adminForm">

	<table width="100%" border="0">
		<tr>
			<td width="100%" valign="top">
			<table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_TITLE');?> </th>
		      	</tr>
		      	<tr>
		      		<td valign="top" align="left" width="100%">
		      			<table>
		  					<tr colspan="2" valign="top">
		    					<td><strong>
                                <?php
                                echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_WRITE_STATUS_TEXT')." ";
                                if ($lang_writable) {
                                    echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_CSS_FILE_WRITABLE_YES');
                                } else {
                                    echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_CSS_FILE_WRITABLE_NO');
                                    ?>
                                    </strong><br />
		       					    <?php echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_CSS_FILE_WRITABLE_INFO'); ?>
                                    <?php } ?>
		       					</td>
		  					</tr>

		  					<tr>
		  						<td><strong><?php echo JText::_('COM_EPOSTS_BACKEND_EDIT_LANG_FIELD_TITLE').": ".$lang_file; ?></strong><br />
                                    <textarea name="lang_text" rows="36" cols="100"><?php echo $lang_text ?></textarea>
		  						</td>
		  					</tr>
		  				</table>
		  			</td>
		  		</tr>
			</table>
			</td>
		</tr>
	</table>
<br /><br />
		<input type="hidden" name="lang_file" value="<?php echo $lang_file; ?>" />
		<input type="hidden" name="hidemainmenu" value="0" />
		<input type="hidden" name="option" value="<?php echo $option; ?>" />
		<input type="hidden" name="task" value="" />
	</form>
	<?php
}

/* ==============================================
/  Configuration
/ =============================================== */
function showConfig($option, $list_sortorder, $cats_sortorder, $user_box, $file_plugin_inputbox, $file_plugin_inputbox2, $inputbox_pic, $inputbox_pic_file, $inputbox_hot, $inputbox_new, $inputbox_down, $inputbox_down2, $inputbox_mirror_1, $inputbox_mirror_2, $inputbox_upd, $inputbox_down_plg, $departments_box, $tabs_box, $edit_departments_box){
	global $jlistConfig, $mainframe;
	jimport( 'joomla.html.pane');
    
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
  ?>
  <form action="index.php" method="post" name="adminForm" id="adminForm">
	<div>&nbsp;<br /></div>

<?php	
$pane =& JPane::getInstance('Tabs');
echo $pane->startPane('jdconfig');
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_TABTEXT_DOWNLOADS'),'downloads');
?>
<table width="97%" border="0">
	<tr>
		<td width="40%" valign="top">

<?php /* Global config */ ?>
                <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_FILES_HEAD')." "; ?></th>
		      	</tr>

		      	<tr>
		      		<td valign="top" align="left" width="100%">
		      			<table width="100%">
						<tr>
       					<td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_UPLOADDIR')." "; ?></strong><br />
	    					<?php echo JPATH_SITE.'/';?>
		    					<input name="jlistConfig[files.uploaddir]" value="<?php echo $jlistConfig['files.uploaddir']; ?>" size="50" />/<br />
	    					<?php echo (is_writable(JPATH_SITE.DS.$jlistConfig['files.uploaddir'])) ? JText::_('COM_EPOSTS_BACKEND_FILESEDIT_URL_DOWNLOAD_WRITABLE') : JText::_('COM_EPOSTS_BACKEND_FILESEDIT_URL_DOWNLOAD_NOTWRITABLE');?></td>
	   					<td>
	    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_UPLOADDIR_DESC');?>
	   					</td>
	  					</tr>

                        <tr>
    					<td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_UPLOADDIRTEMP')." "; ?></strong><br />
	    					<?php echo JPATH_SITE.'/'.$jlistConfig['files.uploaddir'].'/tempzipfiles';?>
                            <br />
	    					<?php echo (is_writable(JPATH_SITE.DS.$jlistConfig['files.uploaddir'].'/tempzipfiles')) ? JText::_('COM_EPOSTS_BACKEND_FILESEDIT_URL_TEMP_WRITABLE') : JText::_('COM_EPOSTS_BACKEND_FILESEDIT_URL_TEMP_NOTWRITABLE');?></td>
    					<td>
	    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_UPLOADDIRTEMP_DESC');?>
    					</td>
	  					</tr>

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DATETIME')." "; ?></strong><br />
                                <input name="jlistConfig[global.datetime]" value="<?php echo $jlistConfig['global.datetime']; ?>" size="30" maxlength="50"/></td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DATETIME_DESC');?>
                        </td>
                          </tr>                        
                        
                        <tr>
    					<td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_ZIPFILE_PREFIX_TEXT')." "; ?></strong><br />
		    					<input name="jlistConfig[zipfile.prefix]" value="<?php echo $jlistConfig['zipfile.prefix']; ?>" size="30" maxlength="50"/></td>
    					<td>
                            <br />
	    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_ZIPFILE_PREFIX_DESC');?>
    					</td>
	  					</tr>

 						<tr>
    					<td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEL_TEMPFILE_TIME')." "; ?></strong><br />
		    					<input name="jlistConfig[tempfile.delete.time]" value="<?php echo $jlistConfig['tempfile.delete.time']; ?>" size="10" maxlength="10"/></td>
    					<td>
                            <br />
	    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEL_TEMPFILE_TIME_DESC');?>
    					</td>
	  					</tr>

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DIRECT_DOWNLOAD_ACTIVE_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[direct.download]","",($jlistConfig['direct.download']) ? 1:0); ?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DIRECT_DOWNLOAD_ACTIVE_DESC').'<br />'.JText::_('COM_EPOSTS_BACKEND_SETTINGS_DIRECT_DOWNLOAD_ACTIVE_DESC2');?>
                        </td>
                          </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_USE_SRIPT_FOR_DOWNLOAD_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[use.php.script.for.download]","",($jlistConfig['use.php.script.for.download']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_USE_SRIPT_FOR_DOWNLOAD_DESC');?>
                        </td>
                          </tr>                            	  					

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_LOGGING_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[activate.download.log]","",($jlistConfig['activate.download.log']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SET_LOGGING_DESC');?>
                        </td>
                          </tr>                        

                        <tr>
                              <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_VIEW_FILE_TYPES')." "; ?></strong><br />
                               <input name="jlistConfig[file.types.view]" value="<?php echo $jlistConfig['file.types.view']; ?>" size="50" maxlength="500"/>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_VIEW_FILE_TYPES_DESC');?>
                        </td>                
                        </tr>
                         <tr><td colspan="2"><hr></td></tr> 
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_AMOUNTS_FILE_LIMIT_TITLE')." "; ?></strong><br />
                                <input name="jlistConfig[limited.download.number.per.day]" value="<?php echo $jlistConfig['limited.download.number.per.day']; ?>" size="10" maxlength="4"/></td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SET_AMOUNTS_FILE_LIMIT_DESC');?>
                        </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_AUP_FE_MESSAGE_NO_DOWNLOAD_EDIT')." "; ?></strong><br />
                                <textarea name="jlistConfig[limited.download.reached.message]" rows="3" cols="40"><?php echo htmlspecialchars($jlistConfig['limited.download.reached.message'], ENT_QUOTES ); ?></textarea>
                                </td>
                                <td valign="top"><br />
                                </td>
                        </tr>                        
                          <tr><td colspan="2"><hr></td></tr> 
                        <tr>
                              <td width="330"><strong><font color="#990000"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_RESET_COUNTER_TITEL')." "; ?></font></strong><br />
                               <?php echo booleanlist("jlistConfig[reset.counters]","",($jlistConfig['reset.counters']) ? 1:0);?> 
                        </td>
                        <td>
                        <br />
                               <font color="#990000"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_RESET_COUNTER_DESC');?></font>
                        </td>                
                        </tr>                                                                          

                    </table>
                   </td>
                  </tr>
                 </table>
			</td>
		</tr>
	</table>

<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_FAUTO_TAB_TITLE'),'autodetect');
?>
<table width="97%" border="0">
    <tr>
        <td width="40%" valign="top">

<?php /* config for autodetect downloads/files */ ?>
                <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FAUTO_HEADER_TITLE')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table width="100%">

                       <tr>
                        <td valign="top" width="330"><strong><font color="#990000"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FAUTO')." "; ?></font></strong><br />
                            <?php echo booleanlist("jlistConfig[files.autodetect]","",($jlistConfig['files.autodetect']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FAUTO_DESC');?>
                        </td>
                          </tr>
                        
                        <tr>
                        <td valign="top" width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FAUTO_ALL_FILES_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[all.files.autodetect]","",($jlistConfig['all.files.autodetect']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FAUTO_ALL_FILES_DESC');?>
                        </td>
                        </tr> 
                        
                        <tr>
                        <td valign="top" width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FAUTO_ONLY_THIS_FILES_TITLE')." "; ?></strong><br />
                               <input name="jlistConfig[file.types.autodetect]" value="<?php echo $jlistConfig['file.types.autodetect']; ?>" size="50" maxlength="500"/>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FAUTO_ONLY_THIS_FILES_DESC');?>
                        </td>                
                        </tr>                       
                        
                        <tr>
                        <td valign="top" width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DOWNLOADS_AUTOPUBLISH_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[autopublish.founded.files]","",($jlistConfig['autopublish.founded.files']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DOWNLOADS_AUTOPUBLISH_DESC');?>
                        </td>
                        </tr>
                    </table>
                   </td>
                  </tr>
                 </table>
            </td>
        </tr>
    </table>

<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_TABTEXT_FRONTEND'),'frontend');
?>
<table width="97%" border="0">
	<tr>
		<td width="40%" valign="top">

<?php /* Frontend config */ ?>
                <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_FRONTEND_HEAD')." "; ?></th>
		      	</tr>

		      	<tr>
		      		<td valign="top" align="left" width="100%">
		      			<table width="100%">

						<tr>
    					<td width="330"><strong><font color="#990000"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_HEADER_TITLE')." "; ?></font></strong><br />
                               <input name="jlistConfig[jd.header.title]" value="<?php echo htmlspecialchars($jlistConfig['jd.header.title'], ENT_QUOTES ); ?>" size="50" maxlength="80" />
                        </td>
    					<td>
    					<br />
    					       <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_HEADER_DESC').'<br />'.JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_HEADER_DESC2');?>
    					</td>
	  					</tr>                        
						
						<tr>
    					<td width="330"><strong><font color="#990000"><?php echo JText::_('COM_EPOSTS_BACKEND_OFFLINE_OPTION_TITLE')." "; ?></font></strong><br />
                               <?php echo booleanlist("jlistConfig[offline]","",($jlistConfig['offline']) ? 1:0);?>
                        </td>
    					<td>
    					<br />
    					       <?php echo JText::_('COM_EPOSTS_BACKEND_OFFLINE_OPTION_DESC');?>
    					</td>
	  					</tr>

                        <tr>
		    			<td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_OFFLINE_MESSAGE_TITLE')." "; ?></strong><br />
                                <textarea name="jlistConfig[offline.text]" rows="10" cols="40"><?php echo htmlspecialchars($jlistConfig['offline.text'], ENT_QUOTES ); ?></textarea>
                                </td>
		    					<td valign="top"><br />
		    					<?php echo JText::_('COM_EPOSTS_BACKEND_OFFLINE_MESSAGE_DESC');?>
		    					</td>
    					</tr>
                        <tr><td colspan="2"><hr></td></tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_COMPO_TEXT')." "; ?></strong><br />
                                <textarea name="jlistConfig[downloads.titletext]" rows="4" cols="40"><?php echo htmlspecialchars($jlistConfig['downloads.titletext'], ENT_QUOTES ); ?></textarea>
                                </td>
                                <td valign="top"><br />
                                <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_COMPO_TEXT_DESC');?>
                                </td>
                        </tr>

                        <tr>
		    			<td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_FOOTER_TEXT_TITLE')." "; ?></strong><br />
                                <textarea name="jlistConfig[downloads.footer.text]" rows="4" cols="40"><?php echo htmlspecialchars($jlistConfig['downloads.footer.text'], ENT_QUOTES ); ?></textarea>
                                </td>
		    					<td valign="top"><br />
		    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_FOOTER_TEXT_DESC');?>
		    					</td>
    					</tr>
                        <tr><td colspan="2"><hr></td></tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_CATLISTBOX_ACTIVE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[show.header.catlist]","",($jlistConfig['show.header.catlist']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_CATLISTBOX_DESC');?>
                        </td>                
                        </tr>                        
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_CAT_VIEW_INFO_IN_LISTS_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[view.category.info]","",($jlistConfig['view.category.info']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_CAT_VIEW_INFO_IN_LISTS_TEXT');?>
                        </td>                
                        </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_DETAILSITE_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[view.detailsite]","",($jlistConfig['view.detailsite']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_DETAILSITE_DESC').' <font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_DETAILSITE_DESC2').'</font>';?>
                        </td>
                        </tr>                         

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_USE_TITLE_AS_LINK_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[use.download.title.as.download.link]","",($jlistConfig['use.download.title.as.download.link']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_USE_TITLE_AS_LINK_DESC');?>
                        </td>
                        </tr>                         
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_PAGENAVI_TOP_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[option.navigate.top]","",($jlistConfig['option.navigate.top']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_PAGENAVI_TOP_TEXT');?>
                        </td>                
                        </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_PAGENAVI_BOTTOM_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[option.navigate.bottom]","",($jlistConfig['option.navigate.bottom']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_PAGENAVI_BOTTOM_TEXT');?>
                        </td>                
                        </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_VIEW_FE_ORDERING_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[view.sort.order]","",($jlistConfig['view.sort.order']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_VIEW_FE_ORDERING_DESC');?>
                        </td>                
                        </tr> 
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_BACK_BUTTON')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[view.back.button]","",($jlistConfig['view.back.button']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_BACK_BUTTON_DESC');?>
                        </td>                
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_USE_PIC_AND_TEXT_FOR_DOWNLOAD_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[view.also.download.link.text]","",($jlistConfig['view.also.download.link.text']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_USE_PIC_AND_TEXT_FOR_DOWNLOAD_TITLE_DESC');?>
                        </td>                
                        </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_REMOVE_TITLE_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[remove.field.title.when.empty]","",($jlistConfig['remove.field.title.when.empty']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_REMOVE_TITLE_DESC');?>
                        </td>                
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_DEL_EMPTY_TAGS_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[remove.empty.tags]","",($jlistConfig['remove.empty.tags']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_DEL_EMPTY_TAGS_DESC');?>
                        </td>                
                        </tr>                        
                                                
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_USE_LIGHTBOX_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[use.lightbox.function]","",($jlistConfig['use.lightbox.function']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_USE_LIGHTBOX_DESC');?>
                        </td>                
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_CONTENT_SUPPORT_FOR_ALL_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[activate.general.plugin.support]","",($jlistConfig['activate.general.plugin.support']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_CONTENT_SUPPORT_FOR_ALL_DESC');?>
                        </td>                
                        </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_CONTENT_SUPPORT_FOR_DESC_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[use.general.plugin.support.only.for.descriptions]","",($jlistConfig['use.general.plugin.support.only.for.descriptions']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_CONTENT_SUPPORT_FOR_DESC_DESC');?>
                        </td>                
                        </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_SEF_ROUTER_OPTION_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[use.sef.with.file.titles]","",($jlistConfig['use.sef.with.file.titles']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_SEF_ROUTER_OPTION_DESC');?>
                        </td>                
                        </tr>                        

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_REDIRECT_FOR_DOWNLOAD_TITLE')." "; ?></strong><br />
                               <input name="jlistConfig[redirect.after.download]" value="<?php echo $jlistConfig['redirect.after.download']; ?>" size="10" maxlength="10"/>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_REDIRECT_FOR_DOWNLOAD_DESC');?>
                        </td>                
                        </tr>
                        <tr><td colspan="2"><hr></td></tr> 
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_USE_TABS_TITLE')." "; ?></strong><br />
                               <?php echo $tabs_box; ?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_USE_TABS_DESC');?>
                        </td>                
                        </tr>
                         <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_CUSTOM_TAB_TITLES_TITLE')." (1)"; ?></strong><br />
                               <input name="jlistConfig[additional.tab.title.1]" value="<?php echo htmlspecialchars($jlistConfig['additional.tab.title.1'], ENT_QUOTES ); ?>" size="50" maxlength="70"/>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_CUSTOM_TAB_TITLES_DESC');?>
                        </td>                
                        </tr>
                         <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_CUSTOM_TAB_TITLES_TITLE')." (2)"; ?></strong><br />
                               <input name="jlistConfig[additional.tab.title.2]" value="<?php echo htmlspecialchars($jlistConfig['additional.tab.title.2'], ENT_QUOTES ); ?>" size="50" maxlength="70"/>
                        </td>
                        <td>
                        </tr>
                         <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_CUSTOM_TAB_TITLES_TITLE')." (3)"; ?></strong><br />
                               <input name="jlistConfig[additional.tab.title.3]" value="<?php echo htmlspecialchars($jlistConfig['additional.tab.title.3'], ENT_QUOTES ); ?>" size="50" maxlength="70"/>
                        </td>
                        </tr>
                                                                                                
                        <tr><td colspan="2"><hr></td></tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_USE_CUT_DESCRIPTION_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[auto.file.short.description]","",($jlistConfig['auto.file.short.description']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_USE_CUT_DESCRIPTION_TITLE_DESC');?>
                        </td>
                        </tr>                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_USE_CUT_DESCRIPTION_LENGTH_TITLE')." "; ?></strong><br />
                                <input name="jlistConfig[auto.file.short.description.value]" value="<?php echo $jlistConfig['auto.file.short.description.value']; ?>" size="10" maxlength="10"/></td>
                        <td>
                        <br />
                        <?php echo '';?>
                        </td>
                        </tr>                        
                        
                        <tr><td colspan="2"><hr></td></tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_CONFIG_REPORT_FILE_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[use.report.download.link]","",($jlistConfig['use.report.download.link']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_CONFIG_REPORT_FILE_DESC');?>
                        </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_CONFIG_REPORT_FILE_REGGED_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[report.link.only.regged]","",($jlistConfig['report.link.only.regged']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_CONFIG_REPORT_FILE_REGGED_DESC');?>
                        </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_CONFIG_REPORT_FILE_MAIL_TITLE')." "; ?></strong><br />
                                <textarea name="jlistConfig[send.mailto.report]" rows="2" cols="40"><?php echo $jlistConfig['send.mailto.report']; ?></textarea> 
                        <td valign="top">
                            <br />
                            <?php echo JText::_('COM_EPOSTS_CONFIG_REPORT_FILE_MAIL_DESC');?>
                        </td>
                        </tr>                                                
                        <tr><td colspan="2"><hr></td></tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_RATING_ON_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[view.ratings]","",($jlistConfig['view.ratings']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_RATING_ON_DESC');?>
                        </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_RATING_ONLY_REGGED_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[rating.only.for.regged]","",($jlistConfig['rating.only.for.regged']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_RATING_ONLY_REGGED_DESC');?>
                        </td>
                        </tr>
                        <tr><td colspan="2"><hr></td></tr>                                                
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_CHECKBOX_TEXT')." "; ?></strong><br />
		    					<input name="jlistConfig[checkbox.top.text]" value="<?php echo htmlspecialchars($jlistConfig['checkbox.top.text'], ENT_QUOTES ); ?>" size="50" maxlength="80"/></td>
    					<td>
    					<br />
    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_CHECKBOX_TEXT_DESC').'<br />'.JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_USE_CHECKBOX_INFO');?>
    					</td>
	  					</tr>

						<tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_CATS_PER_SIDE')." "; ?></strong><br />
		    					<input name="jlistConfig[categories.per.side]" value="<?php echo $jlistConfig['categories.per.side']; ?>" size="10" maxlength="10"/></td>
    					<td>
    					<br />
    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_CATS_PER_SIDE_DESC');?>
    					</td>
	  					</tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_FILES_PER_SIDE')." "; ?></strong><br />
                                <input name="jlistConfig[files.per.side]" value="<?php echo $jlistConfig['files.per.side']; ?>" size="10" maxlength="10"/></td>
                        <td>
                        <br />
                        <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_FILES_PER_SIDE_DESC');?>
                        </td>
                        </tr>                        
                        <tr><td colspan="2"><hr></td></tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_SORTCATSORDER_TEXT')." "; ?></strong><br />
                        <?php
                        $catsorder = (int)$jlistConfig['cats.order'];
                        $inputbox = JHTML::_('select.genericlist', $cats_sortorder, 'jlistConfig[cats.order]' , 'size="3" class="inputbox"', 'value', 'text', $catsorder );
                        echo $inputbox; ?>
                        </td>
                        <td valign="top">
                        <br />
                        <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_SORTCATSORDER_DESC');?>
                        </td>
                          </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_SORTFILESORDER_TEXT')." "; ?></strong><br />
                        <?php
                        $filesorder = (int)$jlistConfig['files.order'];
                        $inputbox = JHTML::_('select.genericlist', $list_sortorder, 'jlistConfig[files.order]' , 'size="5" class="inputbox"', 'value', 'text', $filesorder );
                        echo $inputbox; ?>
                        </td>
    					<td valign="top">
    					<br />
    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_SORTFILESORDER_DESC');?>
    					</td>
	  					</tr>
                        <tr><td colspan="2"><hr></td></tr>
                        
                        <tr>
                        <td width="330"><font color="#990000"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_JCOMMENTS_TITLE')." "; ?></strong></font><br />
                               <?php echo booleanlist("jlistConfig[jcomments.active]","",($jlistConfig['jcomments.active']) ? 1:0);?>
                        </td>
                        <td>
                               <?php 
                               $jcomments = $mainframe->getCfg('absolute_path') . '/components/com_jcomments/jcomments.php';
                               if (file_exists($jcomments)) {
                                    echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_JCOMMENTS_EXISTS_DESC');
                               } else {
                                    echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_JCOMMENTS_DESC');
                               } ?>
                        </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_JCOMMENTS_VIEW_SUM_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[view.sum.jcomments]","",($jlistConfig['view.sum.jcomments']) ? 1:0);?>
                        </td>
                        <td valign="top">
                        <br />
                        <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_JCOMMENTS_VIEW_SUM_DESC');?>
                        </td>
                        </tr>
                        <!--
                        <tr><td colspan="2"><hr></td></tr>
                        <tr>
                        <td width="330"><font color="#990000"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_JOM_COMMENT_TITLE')." "; ?></strong></font><br />
                               <?php echo booleanlist("jlistConfig[view.jom.comment]","",($jlistConfig['view.jom.comment']) ? 1:0);?>
                        </td>
                        <td>
                               <?php 
                               if (file_exists(JPATH_PLUGINS . DS . 'content' . DS . 'jom_comment_bot.php')){
                                    echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_JOM_COMMENT_EXISTS_DESC');
                               } else {
                                    echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_JOM_COMMENT_DESC');
                               } ?>
                        </td>  
                        </tr>
                        <tr><td colspan="2"><font color="#990000"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_JOM_COMMENT_NOTE'); ?></font></td></tr>
                        -->
		  				</table>
		  			</td>
		  		</tr>
				</table>
					</td>
				</tr>
				</table>						
				
            </td>
		</tr>
	</table>

<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BE_CONFIG_TAB_TITLE_FE_EDIT'),'frontendedit');
?>
<table width="97%" border="0">
    <tr>
        <td width="40%" valign="top">
                <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BE_CONFIG_HEAD_TITLE_FE_EDIT')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                        <table width="100%">
                        <tr>

                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BE_CONFIG_EDIT_ACCESS_RIGHTS_USERS')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[uploader.can.edit.fe]","",($jlistConfig['uploader.can.edit.fe']) ? 1:0);?>
                        </td>
                        </tr>                        

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BE_CONFIG_EDIT_ACCESS_RIGHTS_TITLE')." "; ?></strong><br />
                           <?php echo $edit_departments_box; ?>
                        </td>
                        <td valign="top">
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BE_CONFIG_EDIT_ACCESS_RIGHTS_TITLE_DESC');?>
                        </td>                
                        </tr> 
                        </table>
                    </td>
                </tr>
                </table>                        
                
            </td>
        </tr>
    </table>

<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_TABTEXT_BACKEND'),'backend');
?>
<table width="97%" border="0">
	<tr>
		<td width="40%" valign="top">

<?php /* Backend config */ ?>
                <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_BACKEND_HEAD')." "; ?></th>
		      	</tr>
                <tr>
                    <td valign="top" align="left" width="100%">
                       <table width="100%">
                
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_CONFIG_CREATE_AUTO_DIR_NAME_TITLE')." "; ?></strong><br />
                                <?php echo booleanlist("jlistConfig[create.auto.cat.dir]","",($jlistConfig['create.auto.cat.dir']) ? 1:0);?>
                        <td>
                        <br />
                        <?php echo JText::_('COM_EPOSTS_CONFIG_CREATE_AUTO_DIR_NAME_DESC');?>
                        </td>
                        </tr>                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_NEW_FILES_ORDER_FIRST_TITLE')." "; ?></strong><br />
                                <?php echo booleanlist("jlistConfig[be.new.files.order.first]","",($jlistConfig['be.new.files.order.first']) ? 1:0);?>
                        <td>
                        <br />
                        <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_NEW_FILES_ORDER_FIRST_DESC');?>
                        </td>
                          </tr>                          					    
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BE_SETTINGS_FILES_PER_SIDE_BE_TITLE')." "; ?></strong><br />
		    					<input name="jlistConfig[files.per.side.be]" value="<?php echo $jlistConfig['files.per.side.be']; ?>" size="10" maxlength="5"/>
    					<td>
    					<br />
    					<?php echo JText::_('COM_EPOSTS_BE_SETTINGS_FILES_PER_SIDE_BE_DESC');?>
    					</td>
	  					</tr>

                        <tr>
    					<td width="330"><br /><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_EDITOR_FILES')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[files.editor]","",($jlistConfig['files.editor']) ? 1:0);?>
                        </td>
    					<td>
    					       <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_EDITOR_FILES_DESC');?>
    					</td>
	  					</tr>

                        <tr>
    					<td width="330"><br /><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_EDITOR_CATS')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[categories.editor]","",($jlistConfig['categories.editor']) ? 1:0);?>
                        </td>
    					<td>
    					       <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_EDITOR_CATS_DESC');?>
    					</td>
	  					</tr>

                        <tr>
    					<td width="330"><br /><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_EDITOR_LICENSES')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[licenses.editor]","",($jlistConfig['licenses.editor']) ? 1:0);?>
                        </td>
    					<td>
    					       <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_EDITOR_LICENSES_DESC');?>
    					</td>
	  					</tr>

                        <tr>
    					<td width="330"><br /><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_EDITOR_LAYOUTS')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[layouts.editor]","",($jlistConfig['layouts.editor']) ? 1:0);?>
                        </td>
    					<td>
    					       <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_EDITOR_LAYOUTS_DESC');?>
    					</td>
	  					</tr>
	  					
                        <tr>
		    			<td width="330"><br /><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILE_LANGUAGE_LIST')." "; ?></strong><br />
                                <textarea name="jlistConfig[language.list]" rows="4" cols="40"><?php echo $jlistConfig['language.list']; ?></textarea>
                                </td>
		    					<td valign="top"><br />
		    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILE_LANGUAGE_LIST_DESC');?>
		    					</td>
    					</tr>

                        <tr>
		    			<td width="330"><br /><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILE_SYSTEM_LIST')." "; ?></strong><br />
                                <textarea name="jlistConfig[system.list]" rows="4" cols="40"><?php echo $jlistConfig['system.list']; ?></textarea>
                                </td>
		    					<td valign="top"><br />
		    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILE_SYSTEM_LIST_DESC');?>
		    					</td>
    					</tr>

                      </table>
                  </td>      
                </tr>                         
			</table>
			</td>
  		</tr>
</table>

<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SET_TAB_ADD_FIELDS_TITLE'),'customfields');
?>
<table width="97%" border="0">
    <tr>
        <td width="40%" valign="top">

<?php /* custom fields config */ ?>
                <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SET_TAB_ADD_FIELDS_TITLE')." "; ?></th>
                  </tr>
                  <tr>
                    <td valign="top" align="left" width="100%">
                       <table width="100%">
                       <tr>
                        <td colspan="2"><b><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_DESC')." "; ?></b>
                        </td>
                       </tr>

                       <tr>                                                                                     
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_FIELD_TYPE')." ".JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_SELECT_FIELD_TYPE'); ?></th>
                      </tr>
                       <tr>
                        <td width="200"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_FIELD_TITLE')." "; ?></strong><br />
                                1. <input class="jdinput" name="jlistConfig[custom.field.1.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.1.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        <td width="230"><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_SELECT_FIELD_VALUE')." "; ?><br />
                                <input name="jlistConfig[custom.field.1.values]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.1.values'])); ?>" size="150" maxlength="2000"/>
                        </td>
                        </tr>
                       <tr>
                        <td width="200">
                                2. <input class="jdinput" name="jlistConfig[custom.field.2.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.2.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        <td width="230">
                                <input name="jlistConfig[custom.field.2.values]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.2.values'])); ?>" size="150" maxlength="2000"/>
                        </td>
                        </tr>
                       <tr>
                        <td width="200">
                                3. <input class="jdinput" name="jlistConfig[custom.field.3.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.3.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        <td width="230">
                                <input name="jlistConfig[custom.field.3.values]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.3.values'])); ?>" size="150" maxlength="2000"/>
                        </td>
                        </tr>
                       <tr>
                        <td width="200">
                                4. <input class="jdinput" name="jlistConfig[custom.field.4.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.4.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        <td width="230">
                                <input name="jlistConfig[custom.field.4.values]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.4.values'])); ?>" size="150" maxlength="2000"/>
                        </td>
                        </tr>
                       <tr>
                        <td width="200">
                                5. <input class="jdinput" name="jlistConfig[custom.field.5.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.5.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        <td width="230">
                                <input name="jlistConfig[custom.field.5.values]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.5.values'])); ?>" size="150" maxlength="2000"/>
                        </td>
                        </tr>
                        <tr>
                         <td colspan="2"><font color="#990000">
                        <?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_SELECT_FIELD_VALUE_NOTE');?>
                        </font>
                        </td> 
                       </tr>
                       
                       <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_FIELD_TYPE')." ".JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_INPUT_FIELD_TYPE'); ?></th>     
                      </tr>                       
                       <tr>
                        <td width="200"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_FIELD_TITLE')." "; ?></strong><br />
                                6. <input class="jdinput" name="jlistConfig[custom.field.6.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.6.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        <td width="230"><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_INPUT_DEFAULT_VALUE')." "; ?><br />
                                <input name="jlistConfig[custom.field.6.values]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.6.values'])); ?>" size="150" maxlength="256"/>
                        </td>
                        </tr>
                       <tr>
                        <td width="200">
                                7. <input class="jdinput" name="jlistConfig[custom.field.7.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.7.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        <td width="230">
                                <input name="jlistConfig[custom.field.7.values]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.7.values'])); ?>" size="150" maxlength="256"/>
                        </td>
                        </tr>
                       <tr>
                        <td width="200">
                                8. <input class="jdinput" name="jlistConfig[custom.field.8.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.8.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        <td width="230">
                                <input name="jlistConfig[custom.field.8.values]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.8.values'])); ?>" size="150" maxlength="256"/>
                        </td>                                         
                        </tr>
                       <tr>
                        <td width="200">
                                9. <input class="jdinput" name="jlistConfig[custom.field.9.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.9.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        <td width="230">
                                <input name="jlistConfig[custom.field.9.values]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.9.values'])); ?>" size="150" maxlength="256"/>
                        </td>
                        </tr>
                       <tr>
                        <td width="200">
                                10. <input class="jdinput" name="jlistConfig[custom.field.10.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.10.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        <td width="230">
                                <input name="jlistConfig[custom.field.10.values]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.10.values'])); ?>" size="150" maxlength="256"/>
                        </td>
                        </tr>
                        <tr>
                         <td colspan="2"><font color="#990000">
                        <?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_INPUT_FIELD_VALUE_NOTE');?>
                        </font>
                        </td> 
                       </tr>                                                                                               
                       
                        <tr>
                      
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_FIELD_TYPE')." ".JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_DATE_FIELD_TYPE'); ?></th>     
                      </tr>                       
                       <tr>
                        <td width="200"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_FIELD_TITLE')." "; ?></strong><br />
                                11. <input class="jdinput" name="jlistConfig[custom.field.11.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.11.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        </tr><tr>
                        <td width="200">
                                12. <input class="jdinput" name="jlistConfig[custom.field.12.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.12.title'])); ?>" size="50" maxlength="256"/>
                        </td>
                        </tr>
                        
                      <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_FIELD_TYPE')." ".JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_TEXT_FIELD_TYPE'); ?></th>     
                      </tr>                       
                       <tr>
                        <td width="200"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_ADD_FIELDS_FIELD_TITLE')." "; ?></strong><br />
                                13. <input class="jdinput" name="jlistConfig[custom.field.13.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.13.title'])); ?>" size="50" maxlength="100"/>
                        </td>
                        </tr><tr>
                        <td width="200">
                                14. <input class="jdinput" name="jlistConfig[custom.field.14.title]" value="<?php echo stripslashes(htmlspecialchars($jlistConfig['custom.field.14.title'])); ?>" size="50" maxlength="256"/>
                        </td>
                        </tr>

                      </table> 
                  </table>
                  </td>      
                </tr>  
              </table>
        </td>
     </tr>
</table>

<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_TABTEXT_IMAGES'),'pics');
?>
<table width="97%" border="0">
	<tr>
		<td width="40%" valign="top">

<?php /* Images config */ ?>
                <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_IMAGES_HEAD')." "; ?></th>
		      	</tr>

		      	<tr>
		      		<td valign="top" align="left" width="100%">
		      			<table width="100%">
                        <tr>
                        <td colspan="2">
                        <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_IMAGES_NOTE')." "; ?>
                        </td>
                        </tr>
                        <tr>
    					<td valign="top" width="330"><br /><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_MINIPICS_SIZE')." "; ?></strong><br />
                          <input name="jlistConfig[info.icons.size]" value="<?php echo $jlistConfig['info.icons.size']; ?>" size="5" maxlength="5"/> px 
                          </td>
    					<td valign="top">
	    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_MINIPICS_SIZE_DESC').'<br />'.JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_USE_SYMBOLE_INFO').'<br />';
                                  $msize =  $jlistConfig['info.icons.size'];
                                  $sample_path = JURI::root().'images/eposts/miniimages/'; 
                                  $sample_pic = '<img src="'.$sample_path.'date.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="" /> ';
                                  $sample_pic .= '<img src="'.$sample_path.'language.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="" /> ';
                                  $sample_pic .= '<img src="'.$sample_path.'weblink.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="" />';
                                  $sample_pic .= '<img src="'.$sample_path.'stuff.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="" /> ';
                                  $sample_pic .= '<img src="'.$sample_path.'contact.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="" /> ';
                                  $sample_pic .= '<img src="'.$sample_path.'system.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="" />';
                                  $sample_pic .= '<img src="'.$sample_path.'currency.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="" /> ';
                                  $sample_pic .= '<img src="'.$sample_path.'download.png" align="middle" width="'.$msize.'" height="'.$msize.'" border="0" alt="" />';
                                  echo $sample_pic; ?>
    					</td>
	  					</tr>
                        <tr><td colspan="3"><hr></td></tr>
                        <tr>
                        <td valign="top" width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_CATPICS_SIZE')." "; ?></strong><br />
                                <input name="jlistConfig[cat.pic.size]" value="<?php echo $jlistConfig['cat.pic.size']; ?>" size="5" maxlength="5"/> px</td>
                        <td valign="top">
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_CATPICS_SIZE_DESC');?>
                        </td>
                        </tr>
                        <tr>
                          <td valign="top"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_PIC_TITLE')." "; ?></strong><br />
                             <?php echo $inputbox_pic; ?>
                          </td>
                          <td valign ="top"><?php echo ' '.JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_PIC_DESC'); ?>
                          </td>
                        </tr>
                        <tr>
                             <td valign="top">
                               <script language="javascript" type="text/javascript">
                                if (document.adminForm.cat_pic.options.value!=''){
                                    jsimg="<?php echo JURI::root().'images/eposts/catimages/'; ?>" + getSelectedText( 'adminForm', 'cat_pic' );
                                } else {
                                     jsimg='';
                                }
                                document.write('<img src=' + jsimg + ' name="imagelib" width="<?php echo $jlistConfig['cat.pic.size']; ?>" height="<?php echo $jlistConfig['cat.pic.size']; ?>" border="1" alt="<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_FILE_NO_DEFAULT_PIC'); ?>" />');
                               </script>
                             </td>
                        </tr> 
                         <tr><td colspan="3"><hr></td></tr>
                        <tr>
                        <td valign="top" width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_FILEPICS_SIZE')." "; ?></strong><br />
                                <input name="jlistConfig[file.pic.size]" value="<?php echo $jlistConfig['file.pic.size']; ?>" size="5" maxlength="5"/> px</td>
                        <td valign="top">
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_FILEPICS_SIZE_DESC');?>
                        </td>
                        </tr>
                        <tr>
                          <td valign="top"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_FILE_PIC_TITLE')." "; ?></strong><br />
                             <?php echo $inputbox_pic_file; ?>
                          </td>
                          <td valign ="top"><?php echo ' '.JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_FILE_PIC_DESC'); ?>
                          </td>
                          </tr>

                           <tr>
                             <td valign="top">
                               <script language="javascript" type="text/javascript">
                                if (document.adminForm.file_pic.options.value!=''){
                                    jsimg="<?php echo JURI::root().'images/eposts/fileimages/'; ?>" + getSelectedText( 'adminForm', 'file_pic' );
                                } else {
                                     jsimg='';
                                }
                                document.write('<img src=' + jsimg + ' name="imagelib2" width="<?php echo $jlistConfig['file.pic.size']; ?>" height="<?php echo $jlistConfig['file.pic.size']; ?>" border="1" alt="<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_FILE_NO_DEFAULT_PIC'); ?>" />');
                               </script>
                             </td>
                          </tr>
                          <tr><td colspan="3"><hr></td></tr> 
                          <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_NEW_TITLE')." "; ?></strong><br />
                                <input name="jlistConfig[days.is.file.new]" value="<?php echo $jlistConfig['days.is.file.new']; ?>" size="5" maxlength="5"/></td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_NEW_DESC');?>
                        </td>
                          </tr>
                          
                         <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_HOT_NEW_PIC_TITLE')." "; ?></strong><br />
                            <?php echo $inputbox_new; ?>     
                        </td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_HOT_NEW_PIC_DESC2');?>
                        </td>
                          </tr>
                          <tr>
                          <td valign="top">
                               <script language="javascript" type="text/javascript">
                                if (document.adminForm.new_pic.options.value!=''){
                                    jsimg="<?php echo JURI::root().'images/eposts/newimages/'; ?>" + getSelectedText( 'adminForm', 'new_pic' );
                                } else {
                                     jsimg='';
                                }
                                document.write('<img src=' + jsimg + ' name="imagelib4" border="1" alt="<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_FILE_NO_DEFAULT_PIC'); ?>" />');
                               </script>
                          </td>
                        </tr>  
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_HOT_TITLE')." "; ?></strong><br />
                                <input name="jlistConfig[loads.is.file.hot]" value="<?php echo $jlistConfig['loads.is.file.hot']; ?>" size="5" maxlength="10"/></td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_HOT_DESC');?>
                        </td>
                          </tr>  
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_HOT_NEW_PIC_TITLE')." "; ?></strong><br />
                            <?php echo $inputbox_hot; ?>
                        </td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_HOT_NEW_PIC_DESC');?>
                        </td>
                        </tr>
                        <tr>
                          <td valign="top">
                               <script language="javascript" type="text/javascript">
                                if (document.adminForm.hot_pic.options.value!=''){
                                    jsimg="<?php echo JURI::root().'images/eposts/hotimages/'; ?>" + getSelectedText( 'adminForm', 'hot_pic' );
                                } else {
                                     jsimg='';
                                }
                                document.write('<img src=' + jsimg + ' name="imagelib3" border="1" alt="<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_FILE_NO_DEFAULT_PIC'); ?>" />');
                               </script>
                          </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_UPD_TITLE')." "; ?></strong><br />
                                <input name="jlistConfig[days.is.file.updated]" value="<?php echo $jlistConfig['days.is.file.updated']; ?>" size="5" maxlength="5"/></td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_UPD_DESC');?>
                        </td>
                          </tr>
                          
                         <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_HOT_NEW_PIC_TITLE')." "; ?></strong><br />
                            <?php echo $inputbox_upd; ?>     
                        </td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_HOT_NEW_PIC_DESC3');?>
                        </td>
                          </tr>
                          <tr>
                          <td valign="top">
                               <script language="javascript" type="text/javascript">
                                if (document.adminForm.upd_pic.options.value!=''){
                                    jsimg="<?php echo JURI::root().'images/eposts/updimages/'; ?>" + getSelectedText( 'adminForm', 'upd_pic' );
                                } else {
                                     jsimg='';
                                }
                                document.write('<img src=' + jsimg + ' name="imagelib8" border="1" alt="<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_FILE_NO_DEFAULT_PIC'); ?>" />');
                               </script>
                          </td>
                        </tr> 
                        <tr><td colspan="3"><hr></td></tr>   
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DETAILS_DOWNLOAD_BUTTON_TITLE')." "; ?></strong><br />
                             <?php echo $inputbox_down; ?>     
                         <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DETAILS_DOWNLOAD_BUTTON_DESC');?>
                        </td>
                        </tr>                            
                        <tr>
                          <td valign="top">
                               <script language="javascript" type="text/javascript">
                                if (document.adminForm.down_pic.options.value!=''){
                                    jsimg="<?php echo JURI::root().'images/eposts/downloadimages/'; ?>" + getSelectedText( 'adminForm', 'down_pic' );
                                } else {
                                     jsimg='';
                                }
                                document.write('<img src=' + jsimg + ' name="imagelib5" border="1" alt="<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_FILE_NO_DEFAULT_PIC'); ?>" />');
                               </script>
                          </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILES_DOWNLOAD_BUTTON_TITLE')." "; ?></strong><br />
                             <?php echo $inputbox_down2; ?>     
                         <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILES_DOWNLOAD_BUTTON_DESC');?>
                        </td>
                        </tr>                            
                        <tr>
                          <td valign="top">
                               <script language="javascript" type="text/javascript">
                                if (document.adminForm.down_pic2.options.value!=''){
                                    jsimg="<?php echo JURI::root().'images/eposts/downloadimages/'; ?>" + getSelectedText( 'adminForm', 'down_pic2' );
                                } else {
                                     jsimg='';
                                }
                                document.write('<img src=' + jsimg + ' name="imagelib9" border="1" alt="<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_FILE_NO_DEFAULT_PIC'); ?>" />');
                               </script>
                          </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DETAILS_MIRROR_BUTTON_TITLE1')." "; ?></strong><br />
                             <?php echo $inputbox_mirror_1; ?>     
                         <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DETAILS_DOWNLOAD_BUTTON_DESC');?>
                        </td>
                        </tr>                            
                        <tr>
                          <td valign="top">
                               <script language="javascript" type="text/javascript">
                                if (document.adminForm.mirror_1_pic.options.value!=''){
                                    jsimg="<?php echo JURI::root().'images/eposts/downloadimages/'; ?>" + getSelectedText( 'adminForm', 'mirror_1_pic' );
                                } else {
                                     jsimg='';
                                }
                                document.write('<img src=' + jsimg + ' name="imagelib6" border="1" alt="<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_FILE_NO_DEFAULT_PIC'); ?>" />');
                               </script>
                          </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DETAILS_MIRROR_BUTTON_TITLE2')." "; ?></strong><br />
                             <?php echo $inputbox_mirror_2; ?>     
                         <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DETAILS_DOWNLOAD_BUTTON_DESC');?>
                        </td>
                        </tr>                            
                        <tr>
                          <td valign="top">
                               <script language="javascript" type="text/javascript">
                                if (document.adminForm.mirror_2_pic.options.value!=''){
                                    jsimg="<?php echo JURI::root().'images/eposts/downloadimages/'; ?>" + getSelectedText( 'adminForm', 'mirror_2_pic' );
                                } else {
                                     jsimg='';
                                }
                                document.write('<img src=' + jsimg + ' name="imagelib7" border="1" alt="<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_FILE_NO_DEFAULT_PIC'); ?>" />');
                               </script><br /><br /> 
                          </td>
                        </tr>
                        
                      <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_THUMBNAILS_HEAD')." "; ?></th>
                      </tr>
                        
                        <tr><td colspan="3"><strong><?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_THUMBS_TITLE')." "; ?></strong><br />
                            <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_THUMBS_INFO')." "; ?><br /><br />
                            <?php if (function_exists(gd_info)){
                                        echo '<font color="green">'.JText::_('COM_EPOSTS_CONFIG_SETTINGS_THUMBS_STATUS_GD_OK').'</font>';
                                  } else {
                                        echo '<font color="red">'.JText::_('COM_EPOSTS_CONFIG_SETTINGS_THUMBS_STATUS_GD_NOT_OK').'</font>';
                                  } ?>   
                        </td></tr>
                        <tr>
                        <td width="330">
                                <input name="jlistConfig[thumbnail.size.height]" value="<?php echo $jlistConfig['thumbnail.size.height']; ?>" size="6" maxlength="5"/> px</td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_THUMBS_SIZE_HEIGHT');?>
                        </td>
                          </tr>
                          <tr>
                           <td width="330">
                                <input name="jlistConfig[thumbnail.size.width]" value="<?php echo $jlistConfig['thumbnail.size.width']; ?>" size="6" maxlength="5"/> px</td>
                           <td>
                            <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_THUMBS_SIZE_WIDTH');?>
                           </td>
                        </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_THUMBS_CREATE_ALL_NEW_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist('resize_thumbs', 'class="inputbox"', 0);?>
                        </td>
                        <td>
                               <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_THUMBS_CREATE_ALL_NEW_DESC').' '.JText::_('COM_EPOSTS_CONFIG_SETTINGS_THUMBS_CREATE_ALL_NEW_DESC2');?>
                        </td>
                        </tr>
                        <tr><td colspan="3"><hr></td></tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CREATE_AUTO_THUMBS_BY_PICS_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[create.auto.thumbs.from.pics]","",($jlistConfig['create.auto.thumbs.from.pics']) ? 1:0);?>
                        </td>
                        <td>
                               <?php echo JText::_('COM_EPOSTS_BACKEND_CREATE_AUTO_THUMBS_BY_PICS_DESC');?>
                        </td>
                        </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CREATE_AUTO_THUMBS_BY_PICS_SCAN_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[create.auto.thumbs.from.pics.by.scan]","",($jlistConfig['create.auto.thumbs.from.pics.by.scan']) ? 1:0);?>
                        </td>
                        <td>
                               <?php echo JText::_('COM_EPOSTS_BACKEND_CREATE_AUTO_THUMBS_BY_PICS_SCAN_DESC');?>
                        </td>
                        </tr> 

                        <tr>
                        <td width="330">
                                <input name="jlistConfig[create.auto.thumbs.from.pics.image.height]" value="<?php echo $jlistConfig['create.auto.thumbs.from.pics.image.height']; ?>" size="6" maxlength="5"/> px</td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_BIG_IMAGES_HEIGHT');?>
                        </td>
                          </tr>
                          <tr>
                           <td width="330">
                                <input name="jlistConfig[create.auto.thumbs.from.pics.image.width]" value="<?php echo $jlistConfig['create.auto.thumbs.from.pics.image.width']; ?>" size="6" maxlength="5"/> px</td>
                           <td>
                            <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_BIG_IMAGES_WIDTH');?>
                           </td>
                        </tr>                        

                        <tr><td colspan="3"><hr></td></tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CREATE_PDF_THUMB_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[create.pdf.thumbs]","",($jlistConfig['create.pdf.thumbs']) ? 1:0);?>
                        </td>
                        <td>
                               <?php echo JText::_('COM_EPOSTS_BACKEND_CREATE_PDF_THUMB_DESC');?>
                        </td>
                        </tr>
                        <tr><td width="330">      
                               <?php if (extension_loaded('imagick')){
                                        echo '<font color="green">'.JText::_('COM_EPOSTS_BACKEND_IMAGICK_STATE_ON').'</font>';
                                  } else {
                                        echo '<font color="red">'.JText::_('COM_EPOSTS_BACKEND_IMAGICK_STATE_OFF').'</font>';
                                  } ?> 
                        </td>
                        </tr>
                        
                        <?php $types[] = JHTML::_('select.option', 'GIF', 'GIF');
                              $types[] = JHTML::_('select.option', 'PNG', 'PNG');
                              $types[] = JHTML::_('select.option', 'JPG', 'JPG');
                              $file_type_inputbox = JHTML::_('select.genericlist', $types, "jlistConfig[pdf.thumb.image.type]" , 'size="1" class="inputbox"', 'value', 'text', $jlistConfig['pdf.thumb.image.type'] );
                        ?>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CREATE_AUTO_THUMBS_BY_PICS_SCAN_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[create.pdf.thumbs.by.scan]","",($jlistConfig['create.pdf.thumbs.by.scan']) ? 1:0);?>
                        </td>
                        <td>
                               <?php echo JText::_('COM_EPOSTS_BACKEND_CREATE_PDF_THUMB_SCAN_DESC');?>
                        </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_PDF_THUMBS_IMAGE_TYPE_TITLE')." "; ?></strong><br />
                              <?php echo $file_type_inputbox; ?>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_PDF_THUMBS_IMAGE_TYPE'); ?>
                        </td>
                        </tr>
                        <tr>
                        <td width="330">
                                <input name="jlistConfig[pdf.thumb.height]" value="<?php echo $jlistConfig['pdf.thumb.height']; ?>" size="6" maxlength="5"/> px</td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_PDF_THUMBS_HEIGHT');?>
                        </td>
                        </tr>
                        <tr>
                           <td width="330">
                                <input name="jlistConfig[pdf.thumb.width]" value="<?php echo $jlistConfig['pdf.thumb.width']; ?>" size="6" maxlength="5"/> px</td>
                           <td>
                            <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_PDF_THUMBS_WIDTH');?>
                           </td>
                        </tr>
                        <tr>
                        <td width="330">
                                <input name="jlistConfig[pdf.thumb.pic.height]" value="<?php echo $jlistConfig['pdf.thumb.pic.height']; ?>" size="6" maxlength="5"/> px</td>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_PDF_BIG_THUMBS_HEIGHT');?>
                        </td>
                          </tr>
                          <tr>
                           <td width="330">
                                <input name="jlistConfig[pdf.thumb.pic.width]" value="<?php echo $jlistConfig['pdf.thumb.pic.width']; ?>" size="6" maxlength="5"/> px</td>
                           <td>
                            <?php echo JText::_('COM_EPOSTS_CONFIG_SETTINGS_PDF_BIG_THUMBS_WIDTH');?>
                           </td>
                        </tr> 
                        
                        <tr><td colspan="3"><hr></td></tr>       
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_PLACEHOLDER_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[thumbnail.view.placeholder]","",($jlistConfig['thumbnail.view.placeholder']) ? 1:0);?>
                               <br />
                               <?php 
                               $nopic = '<img src="'.JURI::root().'images/eposts/screenshots/thumbnails/no_pic.gif" width="'.$jlistConfig['thumbnail.size.width'].'" height="'.$jlistConfig['thumbnail.size.height'].'" />';
                               echo $nopic;
                               ?> 
                        </td>
                        <td valign="top">
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_PLACEHOLDER_TEXT');?>
                        </td>                
                        </tr>
                        
                        <tr>
                        <td width="330"><br /><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_PLACEHOLDER_IN_LIST_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[thumbnail.view.placeholder.in.lists]","",($jlistConfig['thumbnail.view.placeholder.in.lists']) ? 1:0);?>
                        </td>
                        <td valign="top">
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_VIEW_PLACEHOLDER_IN_LIST_TEXT');?>
                        </td>                
                        </tr>
                        
                           
                           
                        </table>
		  			</td>
		  		</tr>
  			</table>
       </td>
	</tr>
</table>

<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_MEDIA_TAB_TITLE'),'multimedia');
?>
<table width="97%" border="0">
    <tr>
        <td width="40%" valign="top">
                <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MEDIA_TITLE')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table width="100%">
                       <tr>
                        <td colspan="2"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MP3_DESC1')." "; ?></strong>
                        </td>
                       </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MP3_CONFIG_TITLE')." "; ?></strong><br />
                            <textarea name="jlistConfig[mp3.player.config]" rows="6" cols="40"><?php echo htmlspecialchars($jlistConfig['mp3.player.config'], ENT_QUOTES); ?></textarea>
                        </td>
                        <td valign="top">
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MP3_CONFIG_DESC');?>
                        </td>
                        </tr> 
                        <tr>
                        <td width="330"><br /><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MP3_CONFIG_VIEW_ID3_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[mp3.view.id3.info]","",($jlistConfig['mp3.view.id3.info']) ? 1:0);?>
                        </td>
                        <td valign="top">
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MP3_CONFIG_VIEW_ID3_DESC');?>
                        </td>                
                        </tr>
                       
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MP3_CONFIG_VIEW_ID3_LAY_TITLE')." "; ?></strong><br />
                            <textarea name="jlistConfig[mp3.info.layout]" rows="12" cols="40"><?php echo htmlspecialchars($jlistConfig['mp3.info.layout'], ENT_QUOTES); ?></textarea>
                        </td>
                        <td valign="top">
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MP3_CONFIG_VIEW_ID3_LAY_DESC');?>
                        </td>
                        </tr>
                        <tr>
                        <td colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MP3_DESC2')." "; ?>
                        </td>                
                        </tr>                       
                    </table>
                   </td>
                  </tr>
                 </table>
            </td>
        </tr>
    </table>

<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_FE_UPLOAD_TAB_TITLE'),'upload');
?>
<table width="97%" border="0">
    <tr>
        <td width="40%" valign="top">
                 <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_CONFIG_UPLOAD_GENERAL_HEAD')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table width="100%">
                       <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_CONFIG_UPLOAD_FILENAME_BLANK_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[fix.upload.filename.blanks]","",($jlistConfig['fix.upload.filename.blanks']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_CONFIG_UPLOAD_FILENAME_BLANK_DESC');?>
                        </td>                
                        </tr>                          

                       <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_CONFIG_UPLOAD_FILENAME_LOW_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[fix.upload.filename.uppercase]","",($jlistConfig['fix.upload.filename.uppercase']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_CONFIG_UPLOAD_FILENAME_LOW_DESC');?>
                        </td>                
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_CONFIG_UPLOAD_FILENAME_SPECIAL_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[fix.upload.filename.specials]","",($jlistConfig['fix.upload.filename.specials']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_CONFIG_UPLOAD_FILENAME_SPECIAL_DESC');?>
                        </td>                
                        </tr>
                        <tr><td colspan="2"><font color="#990000"><?php echo JText::_('COM_EPOSTS_CONFIG_UPLOAD_IMAGE_FILENAME_NOTE');?></font></td></tr>                                                  
                          
                         </table>
                      </td>
                   </tr>
                </table>          
<?php /* upload config */ ?>
                <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FE_UPLOAD_TITLE_HEAD')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table width="100%">

                        <tr>
                        <td width="330"><strong><font color="#990000"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_UPLOADS_ACTIVE')." "; ?></font></strong><br />
                               <?php echo booleanlist("jlistConfig[frontend.upload.active]","",($jlistConfig['frontend.upload.active']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_UPLOADS_ACTIVE_DESC');?>
                        </td>                
                        </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_UPLOADS_PERMISSIONS')." "; ?></strong><br />
                            <?php echo $user_box; ?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_UPLOADS_PERMISSIONS_DESC');?>
                        </td>                
                        </tr>                        

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_CATSEDIT_CAT_DEPARTMENT_ACCESS_TITLE')." "; ?></strong><br />
                           <?php echo $departments_box; ?>
                        </td>
                        <td valign="top">
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_CAT_DEPARTMENT_UPLOAD_ACCESS_DESC');?>
                        </td>                
                        </tr> 
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_FRONTEND_UPLOAD_AUTO_PUBLISH')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[upload.auto.publish]","",($jlistConfig['upload.auto.publish']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_FRONTEND_UPLOAD_AUTO_PUBLISH_DESC');?>
                        </td>                
                        </tr>                        
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_UPLOADS_FILETYPES')." "; ?></strong><br />
                               <input name="jlistConfig[allowed.upload.file.types]" value="<?php echo $jlistConfig['allowed.upload.file.types']; ?>" size="50" maxlength="500"/>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_UPLOADS_FILETYPES_DESC');?>
                        </td>                
                        </tr>                            

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_UPLOADS_FILESIZE')." "; ?></strong><br />
                               <input name="jlistConfig[allowed.upload.file.size]" value="<?php echo $jlistConfig['allowed.upload.file.size']; ?>" size="50" maxlength="80"/><br />
                               <?php echo JText::_('COM_EPOSTS_UPLOAD_MAX_FILESIZE_INFO_TITLE').' '. ini_get('upload_max_filesize'); ?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_UPLOADS_FILESIZE_DESC');?>
                        </td>                
                        </tr>

                         <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_UPLOADS_FORM_TEXT')." "; ?></strong><br />
                                <textarea name="jlistConfig[upload.form.text]" rows="8" cols="40"><?php echo htmlspecialchars($jlistConfig['upload.form.text'], ENT_QUOTES); ?></textarea>
                                </td>
                                <td valign="top"><br />
                                <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FRONTEND_UPLOADS_FORM_TEXT_DESC');?>
                                </td>
                        </tr>
                        
                        </table>
                      </td>
                  </tr>
                </table>
       </td>
    </tr>
    <tr>
      <td>
         <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_DESC_TITLE_HEAD')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                         <table width="100%">
                           <tr>
                              <td colspan="2">
                                <?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_DESC_OPTION')." "; ?>
                              </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_AUTHOR_NAME');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.author]","",($jlistConfig['fe.upload.view.author']) ? 1:0);?>
                            </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_AUTHOR_URL');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.author.url]","",($jlistConfig['fe.upload.view.author.url']) ? 1:0);?>
                            </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_RELEASE_VIEW');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.release]","",($jlistConfig['fe.upload.view.release']) ? 1:0);?>
                            </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_PRICE_VIEW');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.price]","",($jlistConfig['fe.upload.view.price']) ? 1:0);?>
                            </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_LICENSE_VIEW');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.license]","",($jlistConfig['fe.upload.view.license']) ? 1:0);?>
                            </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_LANGUAGE_VIEW');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.language]","",($jlistConfig['fe.upload.view.language']) ? 1:0);?>
                            </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_SYSTEM_VIEW');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.system]","",($jlistConfig['fe.upload.view.system']) ? 1:0);?>
                            </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_LONG_DESC_VIEW');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.desc.long]","",($jlistConfig['fe.upload.view.desc.long']) ? 1:0);?>
                            </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_SHORT_DESC_VIEW');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.desc.short]","",($jlistConfig['fe.upload.view.desc.short']) ? 1:0);?>
                            </td>
                            <td>
                            <br />
                               <?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_SHORT_DESC_DESC');?>
                            </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_FILE_PIC_VIEW');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.pic.upload]","",($jlistConfig['fe.upload.view.pic.upload']) ? 1:0);?>
                            </td>
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_EXTERN_FILE_VIEW');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.extern.file]","",($jlistConfig['fe.upload.view.extern.file']) ? 1:0);?>
                            </td>
                            <td>
                            <br />
                               <?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_EXTERN_FILE_DESC');?>
                            </td> 
                           </tr>
                           <tr>     
                            <td width="330"><?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_UPLOAD_FILE_VIEW');?><br />
                               <?php echo booleanlist("jlistConfig[fe.upload.view.select.file]","",($jlistConfig['fe.upload.view.select.file']) ? 1:0);?>
                            </td>
                            <td>
                            <br />
                               <?php echo JText::_('COM_EPOSTS_CONFIG_FEUPLOAD_UPLOAD_FILE_DESC');?>
                            </td> 
                           </tr>
                         </table>     
                      </td>    
                 </tr>
          </table>       
       </td>
    </tr>
</table>

<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_TABTEXT_SECURITY'),'security');
?>
<table width="97%" border="0">
	<tr>
		<td width="40%" valign="top">

<?php /* Backend config */ ?>
                <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_SECURITY_HEAD')." "; ?></th>
		      	</tr>
                <tr>
                      <td valign="top" align="left" width="100%">
                          <table width="100%">
                <tr>
                    <td width="330" valign="top">
                        <strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_ANTILEECH_TITLE')." "; ?></strong><br />
                         <?php echo booleanlist("jlistConfig[anti.leech]","",($jlistConfig['anti.leech']) ? 1:0);?>
                    </td>
                    <td>
                        <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_ANTILEECH_DESK');?>
                    </td>
                  </tr> 
                <tr><td colspan="2"><hr></td></tr> 
                <tr>
                    <td width="330" valign="top">
                        <strong><?php echo JText::_('COM_EPOSTS_STOP_LEECHING_OPTION_TITLE')." "; ?></strong><br />
                         <?php echo booleanlist("jlistConfig[check.leeching]","",($jlistConfig['check.leeching']) ? 1:0);?>
                    </td>
                    <td>
                        <?php echo JText::_('COM_EPOSTS_STOP_LEECHING_OPTION_DESC');?>
                    </td>
                  </tr>
                  <tr>
                    <td width="330" valign="top">
                        <strong><?php echo JText::_('COM_EPOSTS_STOP_LEECHING_OPTION_NO_REFERER_TITLE')." "; ?></strong><br />
                         <?php echo booleanlist("jlistConfig[block.referer.is.empty]","",($jlistConfig['block.referer.is.empty']) ? 1:0);?>
                    </td>
                    <td>
                        <?php echo JText::_('COM_EPOSTS_STOP_LEECHING_OPTION_NO_REFERER_DESC');?>
                    </td>
                  </tr>
                  <tr>
                     <td width="330" valign="top"><strong><?php echo JText::_('COM_EPOSTS_STOP_LEECHING_ALLOWED_SITES_OPTION_TITLE')." "; ?></strong><br />
                             <textarea name="jlistConfig[allowed.leeching.sites]" rows="4" cols="40"><?php echo $jlistConfig['allowed.leeching.sites']; ?></textarea>
                     </td>
                     <td valign="top">
                              <?php echo JText::_('COM_EPOSTS_STOP_LEECHING_ALLOWED_SITES_OPTION_DESC');?>
                     </td>
                   </tr>                  
                <tr><td colspan="2"><hr></td></tr> 
                <tr>
    				<td width="330">
						<strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MAIL_SECURITY_TITEL')." "; ?></strong><br />
                     	<?php echo booleanlist("jlistConfig[mail.cloaking]","",($jlistConfig['mail.cloaking']) ? 1:0);?>
                    </td>
    				<td>
    					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_MAIL_SECURITY_DESC');?>
    				</td>
	  			</tr>
                <tr><td colspan="2"><hr></td></tr>
                <tr>
                    <td width="330">
                        <strong><?php echo JText::_('COM_EPOSTS_BACKEND_USE_BLACKLIST_TITLE')." "; ?></strong><br />
                         <?php echo booleanlist("jlistConfig[use.blocking.list]","",($jlistConfig['use.blocking.list']) ? 1:0);?>
                    </td>
                    <td>
                        <?php echo JText::_('COM_EPOSTS_BACKEND_USE_BLACKLIST_DESC');?>
                    </td>
                  </tr>
                  <tr>
                     <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_BLACKLIST_TITLE')." "; ?></strong><br />
                             <textarea name="jlistConfig[blocking.list]" rows="15" cols="40"><?php echo $jlistConfig['blocking.list']; ?></textarea>
                     </td>
                     <td valign="top">
                              <?php echo JText::_('COM_EPOSTS_BACKEND_BLACKLIST_DESC');?>
                     </td>
                   </tr>                
                   </table>
                   </td>
                   </tr>
  				</table>
  			</td>
  		</tr>
</table>

<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_TABTEXT_EMAIL'),'email');
?>
<table width="97%" border="0">
	<tr>
		<td width="40%" valign="top">

<?php /* E-Mail config */ ?>
			<table cellpadding="4" cellspacing="1" border="0" class="adminlist">
		    	<tr>
		      		<th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_MAIL_HEAD')." "; ?></th>
		      	</tr>

		      	<tr>
		      		<td valign="top" align="left" width="100%">
		      			<table width="100%">

                        <tr>
    					<td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_OPTION')." "; ?></strong><br />
                                <?php echo booleanlist("jlistConfig[send.mailto.option]","",($jlistConfig['send.mailto.option']) ? 1:0);?>
                        </td>
    					<td>
    					       <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_OPTION_DESC');?>
    					</td>
	  					</tr>

                        <tr>
    					<td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_HTML')." "; ?></strong><br />
                                <?php echo booleanlist("jlistConfig[send.mailto.html]","",($jlistConfig['send.mailto.html']) ? 1:0);?>
                        </td>
    					<td>
    					       <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_HTML_DESC');?>
    					</td>
	  					</tr>

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO')." "; ?></strong><br />
                                <textarea name="jlistConfig[send.mailto]" rows="2" cols="40"><?php echo $jlistConfig['send.mailto']; ?></textarea> 
    					<td>
                            <br />
        					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_DESC');?>
    					</td>
	  					</tr>

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_BETREFF')." "; ?></strong><br />
		    					<input name="jlistConfig[send.mailto.betreff]" value="<?php echo htmlspecialchars($jlistConfig['send.mailto.betreff'], ENT_QUOTES ); ?>" size="50" maxlength="80"/></td>
    					<td>
                            <br />
        					<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_BETREFF_DESC');?>
    					</td>
	  					</tr>
                        
                        <tr>
                          <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_MAIL_UPLOAD_TEMPLATE_TITLE')." "; ?></strong><br />
                              <textarea name="jlistConfig[send.mailto.template.download]" rows="10" cols="40"><?php echo htmlspecialchars($jlistConfig['send.mailto.template.download'], ENT_QUOTES ); ?></textarea>
                          </td>
                          <td valign="top">
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_MAIL_DOWNLOAD_TEMPLATE_DESC');?>
                        </td>
                          
                        </tr>
                    </table>
		  			</td>
		  		</tr>
                  
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_MAIL_UPLOAD_HEAD')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table width="100%">

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_OPTION')." "; ?></strong><br />
                                <?php echo booleanlist("jlistConfig[send.mailto.option.upload]","",($jlistConfig['send.mailto.option.upload']) ? 1:0);?>
                        </td>
                        <td>
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_OPTION_UPLOAD_DESC');?>
                        </td>
                          </tr>

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_HTML')." "; ?></strong><br />
                                <?php echo booleanlist("jlistConfig[send.mailto.html.upload]","",($jlistConfig['send.mailto.html.upload']) ? 1:0);?>
                        </td>
                        <td>
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_HTML_DESC');?>
                        </td>
                          </tr>

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO')." "; ?></strong><br />
                                <textarea name="jlistConfig[send.mailto.upload]" rows="2" cols="40"><?php echo $jlistConfig['send.mailto.upload']; ?></textarea> 
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_UPLOAD_DESC');?>
                        </td>
                          </tr>

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_BETREFF')." "; ?></strong><br />
                                <input name="jlistConfig[send.mailto.betreff.upload]" value="<?php echo htmlspecialchars($jlistConfig['send.mailto.betreff.upload'], ENT_QUOTES ); ?>" size="50" maxlength="80"/></td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_SEND_MAILTO_BETREFF_DESC');?>
                        </td>
                          </tr>
                        <tr>
                          <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_MAIL_UPLOAD_TEMPLATE_TITLE')." "; ?></strong><br />
                              <textarea name="jlistConfig[send.mailto.template.upload]" rows="10" cols="40"><?php echo htmlspecialchars($jlistConfig['send.mailto.template.upload'], ENT_QUOTES ); ?></textarea>
                          </td>
                          <td valign="top">
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_MAIL_UPLOAD_TEMPLATE_DESC');?>
                        </td>
                          
                        </tr>                          
                          
                    </table>
                      </td>
                  </tr>
                  
				</table>
                
                </td>
    </tr>
</table>
                
<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_TABTEXT_SPECIALS'),'specials');
?>
<table width="97%" border="0">
    <tr>
        <td width="40%" valign="top">

<?php /* upload config */ ?>
           <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_ADSENSE_TITLE')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table width="100%">
                          <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_ADSENSE_ACTIVATE_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[google.adsense.active]","",($jlistConfig['google.adsense.active']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_ADSENSE_ACTIVATE_DESC');?>
                        </td>                
                        </tr>

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_ADSENSE_CODE_TITLE')." "; ?></strong><br />
                                <textarea name="jlistConfig[google.adsense.code]" rows="8" cols="40"><?php echo htmlspecialchars($jlistConfig['google.adsense.code'], ENT_QUOTES ); ?></textarea>
                                </td>
                                <td valign="top"><br />
                                <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_ADSENSE_CODE_DESC');?>
                                </td>
                        </tr>
                        
                        </table>
                      </td>
                  </tr>
                </table>
       </td>
    </tr>
    <tr>
      <td>
         <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_WAITING_HEADER')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                         <table width="100%">
                          <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_WAITING_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[countdown.active]","",($jlistConfig['countdown.active']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_WAITING_DESC');?>
                        </td>                
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_WAITING_VALUE_TITLE')." "; ?></strong><br />
                                <input name="jlistConfig[countdown.start.value]" value="<?php echo $jlistConfig['countdown.start.value']; ?>" size="5" /></td>
                        <td>
                            <br />
                        
                        </td>
                          </tr>                        

                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_WAITING_NOTE_TITLE')." "; ?></strong><br />
                                <textarea name="jlistConfig[countdown.text]" rows="8" cols="40"><?php echo htmlspecialchars($jlistConfig['countdown.text'], ENT_QUOTES ); ?></textarea>
                                </td>
                                <td valign="top"><br />
                                <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_WAITING_NOTE_DESC');?>
                                </td>
                        </tr>

                         </table>     
                      </td>    
                 </tr>
                 
        
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SET_AUP_HEADER')." "; ?></th>
                  </tr>
                  <tr><td><font color="#990000"><?php echo JText::_('COM_EPOSTS_BACKEND_SET_AUP_HEADER_TEXT'); ?></font></td></tr>
                  <tr>
                  <td valign="top" align="left" width="100%">
                      <table width="100%">
                  <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_AUP_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[use.alphauserpoints]","",($jlistConfig['use.alphauserpoints']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_AUP_DESC');?>
                        </td>                
                        </tr>
                          <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_AUP_DOWNLOAD_WHEN_ZERO_POINTS_TEXT')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[user.can.download.file.when.zero.points]","",($jlistConfig['user.can.download.file.when.zero.points']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_AUP_DOWNLOAD_WHEN_ZERO_POINTS_DESC');?>
                        </td>                
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_AUP_FE_MESSAGE_NO_DOWNLOAD_EDIT')." "; ?></strong><br />
                                <textarea name="jlistConfig[user.message.when.zero.points]" rows="3" cols="40"><?php echo htmlspecialchars($jlistConfig['user.message.when.zero.points'], ENT_QUOTES ); ?></textarea>
                                </td>
                                <td valign="top"><br />
                                </td>
                        </tr>
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SET_AUP_USE_FILE_PRICE_TITLE')." "; ?></strong><br />
                               <?php echo booleanlist("jlistConfig[use.alphauserpoints.with.price.field]","",($jlistConfig['use.alphauserpoints.with.price.field']) ? 1:0);?>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SET_AUP_USE_FILE_PRICE_DESC');?>
                        </td>                
                        </tr>                                                
                         </table>     
                  </td>    
             </tr> 
                 
<?php if ($jlistConfig['pad.exists']){ ?>
             </table> 
                 <tr>
                     <td>
                 
                 <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_PAD_HEAD_TITLE')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table width="100%">
                       <tr>
                        <td colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_PAD_INFO_TEXT'); ?>
                        </td>
                       </tr>
                       <tr>
                        <td width="330"><strong><font color="#990000"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_PAD_ACTIVATE_TITLE')." "; ?></font></strong><br />
                            <?php echo booleanlist("jlistConfig[pad.use]","",($jlistConfig['pad.use']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_PAD_ACTIVATE_DESC');?>
                        </td>
                          </tr>
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_PAD_FOLDER_TITLE')." "; ?></strong><br />
                            <input name="jlistConfig[pad.folder]" value="<?php echo $jlistConfig['pad.folder']; ?>" disabled="disabled" size="50" maxlength="100"/><br />
                            <?php echo (is_writable(JPATH_SITE.DS.$jlistConfig['pad.folder'])) ? JText::_('COM_EPOSTS_BACKEND_FILESEDIT_URL_DOWNLOAD_WRITABLE') : JText::_('COM_EPOSTS_BACKEND_FILESEDIT_URL_DOWNLOAD_NOTWRITABLE');?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_PAD_FOLDER_DESC');?>
                        </td>
                        </tr> 
                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_PAD_LANGUAGE_TITLE')." "; ?></strong><br />
                               <input name="jlistConfig[pad.language]" value="<?php echo $jlistConfig['pad.language']; ?>" size="50" maxlength="50"/>
                        </td>
                        <td>
                        <br />
                               <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_PAD_LANGUAGE_DESC');?>
                        </td>                
                        </tr>                       
                    
                   </td>
                  </tr>
                 </table>                 
   <?php } ?>
                     </td>
                 </tr>
                 
          </table>
                           <tr>
                     <td>
                 
                 <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_POST_COM_ID_TITLE')." "; ?></th>
                  </tr>

                  <tr>
                      <td valign="top" align="left" width="100%">
                       <table width="100%">
                     <?php
                     if ($jlistConfig['com'] == ''){ ?>   

                       <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_POST_COM_ID_TITLE2')." "; ?></strong><br />
                               <input name="com" value="" size="50" maxlength="50"/>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_POST_COM_ID_DESC');?>
                        </td>
                        </tr>              
                     <?php } else { ?>
                       <tr>
                        <td>
                            <?php echo JText::_('COM_EPOSTS_BACKEND_POST_COM_ID_ACTIVE');?>
                        </td>
                        </tr>              
                     <?php } ?>
                        
                        </table>
                        </td>
                    </tr>
                  </table>         
       </td>
    </tr>
</table>




<?php
echo $pane->endPanel();
echo $pane->startPanel(JText::_('COM_EPOSTS_BACKEND_SETTINGS_TABTEXT_PLUGINS'),'plugins');
?>
<table width="97%" border="0">
  <tr>
    <td width="40%" valign="top">

      <?php /* File Plugin config */ ?>
      <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
         <tr>
            <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_GLOBAL_FILEPLUGIN_HEAD'); ?></th>
         </tr>
         <tr>
           <td valign="top" align="left" width="100%">
              <table width="100%">

                <?php
                if (!$file_plugin_inputbox){
                ?> <tr>
                     <td width="330"><strong>
                     <?php echo '<font color="#990000">'.JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_NOT_INSTALLED').'</font>'; ?>
                     </td>
                   </tr>
                <?php
                } else {
                ?>
               <tr>
                <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_DEFAULTLAYOUT')." "; ?></strong><br />
                   <?php
                     echo( $file_plugin_inputbox);
                   ?>
                </td>
                <td>
                   <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_DEFAULTLAYOUT_DESC');?>
                </td>
               </tr>
               <tr>
               <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DETAILS_DOWNLOAD_BUTTON_TITLE')." "; ?></strong><br />
                  <?php echo $inputbox_down_plg; ?>     
                 <td>
                  
                </td>
                </tr>                            
                <tr>
                    <td valign="top">
                         <script language="javascript" type="text/javascript">
                          if (document.adminForm.down_pic_plg.options.value!=''){
                              jsimg="<?php echo JURI::root().'images/eposts/downloadimages/'; ?>" + getSelectedText( 'adminForm', 'down_pic_plg' );
                          } else {
                              jsimg='';
                          }
                          document.write('<img src=' + jsimg + ' name="imagelib10" border="1" alt="<?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_DEFAULT_CAT_FILE_NO_DEFAULT_PIC'); ?>" />');
                          </script>
                     </td>
                </tr>
               <tr>
                <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_ENABLEPLUGIN')." "; ?></strong><br />
                   <?php echo booleanlist("jlistConfig[fileplugin.enable_plugin]","",($jlistConfig['fileplugin.enable_plugin']) ? 1:0);?>
                </td>
                <td>
                   <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_ENABLEPLUGIN_DESC');?>
                </td>
               </tr>
               <tr>
                <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_SHOWDISABLED')." "; ?></strong><br />
                   <?php echo booleanlist("jlistConfig[fileplugin.show_jdfiledisabled]","",($jlistConfig['fileplugin.show_jdfiledisabled']));?>
                </td>
                <td>
                   <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_SHOWDISABLED_DESC');?>
                </td>
               </tr>
               <tr>
                <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_DOWNLOADTITLE')." "; ?></strong><br />
                   <?php echo booleanlist("jlistConfig[fileplugin.show_downloadtitle]","",($jlistConfig['fileplugin.show_downloadtitle']) ? 1:0);?>
                </td>
                <td>
                   <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_DOWNLOADTITLE_DESC');?>
                </td>
               </tr>

               <tr>
                 <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_OFFLINETITLE')." "; ?></strong><br />
                     <textarea name="jlistConfig[fileplugin.offline_title]" rows="3" cols="40"><?php echo htmlspecialchars($jlistConfig['fileplugin.offline_title'], ENT_QUOTES ); ?></textarea>
                 </td>
                 <td valign="top"><br />
                    <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_OFFLINETITLE_DESC');?>
                 </td>
               </tr>

               <tr>
                 <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_OFFLINEDESC')." "; ?></strong><br />
                     <textarea name="jlistConfig[fileplugin.offline_descr]" rows="3" cols="40"><?php echo htmlspecialchars($jlistConfig['fileplugin.offline_descr'], ENT_QUOTES ); ?></textarea>
                 </td>
                 <td valign="top"><br />
                    <?php echo JText::_('COM_EPOSTS_BACKEND_SETTINGS_FILEPLUGIN_OFFLINEDESC_DESC');?>
                 </td>
               </tr>
               <tr><td colspan="2"><hr></td></tr>
               <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_USE_CUT_DESCRIPTION_TITLE')." "; ?></strong><br />
                            <?php echo booleanlist("jlistConfig[plugin.auto.file.short.description]","",($jlistConfig['plugin.auto.file.short.description']) ? 1:0);?>
                        </td>
                        <td>
                            <br />
                            <?php echo JText::_('COM_EPOSTS_BACKEND_USE_CUT_DESCRIPTION_TITLE_DESC');?>
                        </td>
                        </tr>                        
                        <tr>
                        <td width="330"><strong><?php echo JText::_('COM_EPOSTS_BACKEND_USE_CUT_DESCRIPTION_LENGTH_TITLE')." "; ?></strong><br />
                                <input name="jlistConfig[plugin.auto.file.short.description.value]" value="<?php echo $jlistConfig['plugin.auto.file.short.description.value']; ?>" size="10" maxlength="10"/></td>
                        <td>
                        <br />
                        <?php echo '';?>
                        </td>
                        </tr> 

                <?php
                }
                ?>

              </table>
           </td>
         </tr>
      </table>
    </td>
  </tr>
</table>

<?php
echo $pane->endPanel();
echo $pane->endPane('jdconfig');
?>

	<input type="hidden" name="option" value="<?php echo $option;?>" />
	<input type="hidden" name="root_dir" value="<?php echo $jlistConfig['files.uploaddir'];?>" />	
	<input type="hidden" name="task" value="" />
	</form>
	<?php
}


// show restore
function showRestore($option, $task) {
	global $mainframe;
    
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
 	?>

	<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
	<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
		
		<tr>
			<th class="adminheading" colspan="3"><?php echo JText::_('COM_EPOSTS_BACKEND_RESTORE_TITLE_HEAD').' '; ?></th>
        </tr>
        <tr>
            <td style="padding:10px;" colspan="3"><?php echo JText::_('COM_EPOSTS_BACKEND_RESTORE_WARNING'); ?>
            </td>
        </tr>
        <tr>
            <td style="padding:20px;" align="center" colspan="3"><b><?php echo JText::_('COM_EPOSTS_BACKEND_RESTORE_FILE'); ?></b><br /><br />
                <input type="file" size="50" name="restore_file">
            </td>
        </tr>
		<tr>
			<td style="padding:20px;" align="center" colspan="3"><input type="submit" name="submitbutton" value="<?php echo JText::_('COM_EPOSTS_BACKEND_RESTORE_RUN');?>" onclick="return confirm('<?php echo JText::_('COM_EPOSTS_BACKEND_RESTORE_RUN_FINAL');?>');">
			</td>
		</tr>

	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="restore.run" />
	<input type="hidden" name="hidemainmenu" value="0" />
   </table>
   </form>
 	<?php
}

function scanFiles($option, $task){    
   global $mainframe;
   ?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
     <table width="100%" border="0">
      <tr>
        <td width="40%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo JText::_('COM_EPOSTS_RUN_MONITORING_TITLE')." "; ?></th>
                  </tr>

                  <tr valign="top" align="center" width="100%">
                     <td style="padding:10px;" colspan="2"><?php echo JText::_('COM_EPOSTS_RUN_MONITORING_INFO'); ?>
                     </td>
                </tr>
                  <tr valign="top" align="center" width="100%">
                    
                    
                    <?php checkFiles($task); ?>
                    
                </tr>
            </table>
        </td>
      </tr>
     </table>

     <br /><br />
     <?php
        echo '<a href="index.php?option=com_eposts"><big><strong>'.JText::_('COM_EPOSTS_BACKEND_INFO_LINK_BACK').'</strong></big></a>';
     ?>

      <input type="hidden" name="boxchecked" value="0" />
      <input type="hidden" name="option" value="<?php echo $option; ?>" />
      <input type="hidden" name="task" value="" />
      <input type="hidden" name="hidemainmenu" value="0" />
    </form>
    <?php
}  

////////////////////////////////////// DEVELOPMENT PHASE III ///////////////////////////////////
////////////////////                Announcements              ///////////////////////	
function editAnnouncements($option, $row, $departmentList, $departmentAccess, $isPublist, $showOnTop){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
    function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.announcements') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.announcements_name.value == "" 
			 || form.announcements_name_ar.value == ""
			 || form.announcements_expirydate.value == ""
			 || tinyMCE.get('announcements_description').getContent()== ""
			 || tinyMCE.get('announcements_description_ar').getContent() == "" ){
             alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
			return false;	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    <form action="index.php" method="post" name="adminForm">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? 'Edit Announcement' : 'Add Announcement';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong>Announcement Title (English)<font color='#990000'>*</font></strong><br />
                                    <input name="announcements_name" value="<?php echo htmlspecialchars($row->announcements_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Announcement Title (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="announcements_name_ar" value="<?php echo htmlspecialchars($row->announcements_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Announcement Category (English):</strong><br />
                                <?php $anCat = $row->announcement_category; ?>
                                	<select name="announcement_category">
                                    	<option value="">- Select -</option>
                                        <option <?php print $anCat == 'New Baby Girl' ? 'selected="selected"' : ''; ?> value="New Baby Girl">New Baby Girl</option>
                                        <option <?php print $anCat == 'New Baby Boy' ? 'selected="selected"' : ''; ?> value="New Baby Boy">New Baby Boy</option>
                                        <option <?php print $anCat == 'Death Announcement � Staff' ? 'selected="selected"' : ''; ?> value="Death Announcement � Staff">Death Announcement � Staff</option>
                                        <option <?php print $anCat == 'Death Announcement � Staff �Family Member' ? 'selected="selected"' : ''; ?> value="Death Announcement � Staff �Family Member">Death Announcement � Staff �Family Member</option>
                                        <option <?php print $anCat == 'Graduation Greetings' ? 'selected="selected"' : ''; ?> value="Graduation Greetings">Graduation Greetings</option>
                                        <option <?php print $anCat == 'Wedding Greetings' ? 'selected="selected"' : ''; ?> value="Wedding Greetings">Wedding Greetings</option>
                                        <option <?php print $anCat == 'Competitions' ? 'selected="selected"' : ''; ?> value="Competitions">Competitions</option>
                                        <option <?php print $anCat == 'Volunteering' ? 'selected="selected"' : ''; ?> value="Volunteering">Volunteering</option>
                                        <option <?php print $anCat == 'Events' ? 'selected="selected"' : ''; ?> value="Events">Events</option>
                                        <option <?php print $anCat == 'CEO Message' ? 'selected="selected"' : ''; ?> value="CEO Message">CEO Message</option>
                                        <option <?php print $anCat == 'Employee Satisfaction Survey' ? 'selected="selected"' : ''; ?> value="Employee Satisfaction Survey">Employee Satisfaction Survey</option>
                                        <option <?php print $anCat == 'Campaign' ? 'selected="selected"' : ''; ?> value="Campaign">Campaign</option>
                                        <option <?php print $anCat == 'CSR' ? 'selected="selected"' : ''; ?> value="CSR">CSR</option>
                                        <option <?php print $anCat == 'Government Communication' ? 'selected="selected"' : ''; ?> value="Government Communication">Government Communication</option>
                                        <option <?php print $anCat == 'Excellence Newsletter' ? 'selected="selected"' : ''; ?> value="Excellence Newsletter">Excellence Newsletter</option>
                                        <option <?php print $anCat == 'Competitions' ? 'selected="selected"' : ''; ?> value="Competitions">Competitions</option>
                                        <option <?php print $anCat == 'Sheikh Khalifa Award' ? 'selected="selected"' : ''; ?> value="Sheikh Khalifa Award">Sheikh Khalifa Award</option>
                                        <option <?php print $anCat == 'Events' ? 'selected="selected"' : ''; ?> value="Events">Events</option>
                                        <option <?php print $anCat == 'Internal recruitment' ? 'selected="selected"' : ''; ?> value="Internal recruitment">Internal recruitment</option>
                                        <option <?php print $anCat == 'Welcome on Board' ? 'selected="selected"' : ''; ?> value="Welcome on Board">Welcome on Board</option>
                                        <option <?php print $anCat == 'HR Policies' ? 'selected="selected"' : ''; ?> value="HR Policies">HR Policies</option>
                                        <option <?php print $anCat == 'Rules & Regulations' ? 'selected="selected"' : ''; ?> value="Rules & Regulations">Rules & Regulations</option>
                                        <option <?php print $anCat == 'Health Insurance' ? 'selected="selected"' : ''; ?> value="Health Insurance">Health Insurance</option>
                                        <option <?php print $anCat == 'Events' ? 'selected="selected"' : ''; ?> value="Events">Events</option>
                                        <option <?php print $anCat == 'New Stamp Release' ? 'selected="selected"' : ''; ?> value="New Stamp Release">New Stamp Release</option>
                                        <option <?php print $anCat == 'Customer Service Tips' ? 'selected="selected"' : ''; ?> value="Customer Service Tips">Customer Service Tips</option>
                                        <option <?php print $anCat == 'Events' ? 'selected="selected"' : ''; ?> value="Events">Events</option>
                                        <option <?php print $anCat == 'System Down' ? 'selected="selected"' : ''; ?> value="System Down">System Down</option>
                                        <option <?php print $anCat == 'Systems / Applications Not Available' ? 'selected="selected"' : ''; ?> value="Systems / Applications Not Available">Systems / Applications Not Available</option>
                                    </select>
                           	</tr>
                            <tr>
                                <td><strong>Announcement Category (Arabic):</strong><br />
                                <?php
                                $anCat2 = $row->announcement_category_ar;
								$opt_01 = JText::_('COM_EPOSTS_ANN_OPT_01');
								$opt_02 = JText::_('COM_EPOSTS_ANN_OPT_02');
								?>
                                	<select name="announcement_category_ar">
                                    	<option value="">- Select -</option>
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_01') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_01'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_01'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_02') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_02'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_02'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_03') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_03'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_03'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_04') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_04'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_04'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_05') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_05'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_05'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_06') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_06'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_06'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_07') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_07'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_07'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_08') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_08'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_08'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_09') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_09'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_09'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_10') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_10'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_10'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_11') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_11'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_11'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_12') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_12'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_12'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_13') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_13'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_13'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_14') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_14'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_14'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_15') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_15'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_15'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_16') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_16'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_16'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_17') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_17'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_17'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_18') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_18'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_18'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_19') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_19'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_19'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_20') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_20'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_20'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_21') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_21'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_21'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_22') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_22'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_22'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_23') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_23'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_23'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_24') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_24'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_24'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_25') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_25'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_25'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_26') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_26'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_26'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_27') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_27'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_27'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_28') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_28'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_28'); ?></option>
                                        
                                        <option <?php print $anCat2 == JText::_('COM_EPOSTS_ANN_OPT_29') ? 'selected="selected"' : ''; ?> value="<?php print JText::_('COM_EPOSTS_ANN_OPT_29'); ?>"><?php print JText::_('COM_EPOSTS_ANN_OPT_29'); ?></option>
                                    </select>
                           	</tr>
                            <tr>
                                <td><strong>Announcement Body (English)<font color='#990000'>*</font></strong><br /> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'announcements_description',  @$row->announcements_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>Announcement Body (Arabic)<font color='#990000'>*</font></strong><br />                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'announcements_description_ar',  @$row->announcements_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong>Expiry Date<font color='#990000'>*</font></strong><br />
                                 <?php JHTML::_('behavior.calendar'); ?>
                                 <input class="inputbox" type="text" name="announcements_expirydate" id="announcements_expirydate"  value="<?php echo strtotime($row->announcements_expirydate) ? date('d-m-Y', strtotime(htmlspecialchars($row->announcements_expirydate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('announcements_expirydate','%d-%m-%Y');"/>
                                                                 
                            </tr>
			
            <tr>
                                <td>
                                <table>
<tr>
                                <td align="left"><strong>Announcement File:</strong></td>
                                <td><input name="announcements_file" id="announcements_file" type="file" <?php print $row->announcements_file != '' ? 'style="display:none;"' : ''; ?>/> 
									 <?php if ($row->announcements_file != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="announcements_doc_nm"><?php print $row->announcements_file; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('announcements_file', 'announcements_doc_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?></td>
                                                                 
                            </tr>
							 <tr>
                                <td align="left"><strong>Announcement File2:</strong></td>
                                <td>
                                 <input name="announcements_file1" id="announcements_file1" type="file" <?php print $row->announcements_file1 != '' ? 'style="display:none;"' : ''; ?>/> 
									 <?php if ($row->announcements_file1 != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="announcements_doc_nm"><?php print $row->announcements_file1; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('announcements_file1', 'announcements_doc_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                  </td>                               
                            </tr>
							 <tr>
                                <td align="left"><strong>Announcement File3:</strong></td>
                                <td>
                                <input name="announcements_file2" id="announcements_file2" type="file" <?php print $row->announcements_file2 != '' ? 'style="display:none;"' : ''; ?>/> 
									 <?php if ($row->announcements_file2 != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="announcements_doc_nm"><?php print $row->announcements_file2; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('announcements_file2', 'announcements_doc_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                </td>                                 
                            </tr>
                            </table>
                                </td>
                                                                 
                            </tr>
            			


							

                            <tr>
                                <td><strong>Show On Top</strong><br />
                                    <?php echo $showOnTop;?>
                                                                 
                            </tr>
                           <tr>
                                <td><strong>Is Public?</strong><br />
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           
                            <tr>
                                <td><strong>Select Department</strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Publish</strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.announcements" />
    </form>
<?php
}   


function listAnnouncements($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', true);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title">ID</th>
            <th valign="top" class="title">Announcement Title</th>
            <th valign="top" class="title">Department Name</th>
            <th valign="top" class="title">Expiry Date</th>
            <th valign="top" class="title">Publish</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.announcements&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit Announcement"><?php echo $row->announcements_name; ?></a></td>
            <td valign="top"><?php echo $row->department; ?></td>
            <td valign="top"><?php echo date('d-m-Y', strtotime($row->announcements_expirydate)); ?></td>
            <td valign="top"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 

////////////////////                News              ///////////////////////	
function editNews($option, $row, $departmentList, $departmentAccess, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">                           
	function deletefile(filefield, filename, getUpdate) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		document.getElementById(getUpdate).value = '1';
		return false;
	}
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.news') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.news_name.value == "" 
			 || form.news_name_ar.value == ""
			 || form.news_newsdate.value == ""
			 <?php if(!$row->id){
			   //|| form.news_file.value == ""
			  }?>
			 || tinyMCE.get('news_description').getContent()== ""
			 || tinyMCE.get('news_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
			return false;	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? 'Edit News' : 'Add News';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong>News Title (English)<font color='#990000'>*</font></strong><br />
                                    <input name="news_name" value="<?php echo htmlspecialchars($row->news_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>News Title (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="news_name_ar" value="<?php echo htmlspecialchars($row->news_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <!--<tr>
                                <td><strong>News Source (English)<br />
                                    <input name="news_source" value="<?php echo htmlspecialchars($row->news_source, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>News Source (Arabic)</strong><br />
                                    <input name="news_source_ar" value="<?php echo htmlspecialchars($row->news_source_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr> -->
                            <tr>
                            	<td>
                                	<strong>News Subtitle (English)</strong><br />
                                     <input name="news_subtitle" value="<?php echo htmlspecialchars($row->news_subtitle, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	<strong>News Subtitle (Arabic)</strong><br />
                                     <input name="news_subtitle_ar" value="<?php echo htmlspecialchars($row->news_subtitle_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>News Short Description (English)<font color='#990000'>*</font></strong><br /> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'news_description',  @$row->news_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>News Short Description (Arabic)<font color='#990000'>*</font></strong><br />                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'news_description_ar',  @$row->news_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong>News Body (English)</strong><br /> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'news_longdescription',  @$row->news_longdescription , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>News Body (Arabic)</strong><br />                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'news_longdescription_ar',  @$row->news_longdescription_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong>News Date<font color='#990000'>*</font></strong><br />
                                <?php
								JHTML::_('behavior.calendar');
								/*// for plugin
								 JPluginHelper::importPlugin('datepicker');	
								echo plgSystemDatePicker::calendar($row->news_newsdate, 'news_newsdate', 'news_newsdate', '%Y-%m-%d');*/
								 ?>
                                 <input class="inputbox" type="text" name="news_newsdate" id="news_newsdate"  value="<?php echo strtotime($row->news_newsdate) != '' ? date('d-m-Y', strtotime(htmlspecialchars($row->news_newsdate, ENT_QUOTES))) : ''; ?>" onclick="return showCalendar('news_newsdate','%d-%m-%Y');"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>News Image (English)</strong><br />
                                <input name="news_file" id="news_file" type="file" <?php print $row->news_image != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->news_image != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="news_image_nm"><?php print $row->news_image; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('news_file', 'news_image_nm','del_news_image');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                    <input name="del_news_image" id="del_news_image" type="hidden" value="0"/                             
                            </tr>
                            <tr>
                                <td><strong>News Image (Arabic)</strong><br />
                                <input name="news_file_ar" id="news_file_ar" type="file" <?php print $row->news_file_ar != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->news_file_ar != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="news_file_ar_nm"><?php print $row->news_file_ar; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('news_file_ar', 'news_file_ar_nm','del_news_file_ar');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                <input name="del_news_file_ar" id="del_news_file_ar" type="hidden" value="0"/>                             
                            </tr>
                           <tr>
                                <td><strong>Is Public?</strong><br />
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           
                            <tr>
                                <td><strong>Select Department</strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Publish</strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.news" />
    </form>
<?php
}   


function listNews($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', true);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title">ID</th>
            <th valign="top" class="title">News Title</th>
            <th valign="top" class="title">Department Name</th>
            <th valign="top" class="title">News Date</th>
            <th valign="top" class="title">Publish</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.news&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit News"><?php echo $row->news_name; ?></a></td>
            <td valign="top"><?php echo $row->department; ?></td>
            <td valign="top"><?php echo $row->news_newsdate; ?></td>
            <td valign="top"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 

////////////////////                Our Vision   	          ///////////////////////
function editOurVision($option, $row){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.visions') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.visions_name.value == "" 
			 || form.visions_name_ar.value == ""
			 || tinyMCE.get('visions_description').getContent()== ""
			 || tinyMCE.get('visions_description_ar').getContent() == ""
			 || tinyMCE.get('visions_longdescription').getContent()== ""
			 || tinyMCE.get('visions_longdescription_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
			return false;	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? 'Edit Vision' : 'Add Vision';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong>Vision Title (English)<font color='#990000'>*</font></strong><br />
                                    <input name="visions_name" value="<?php echo htmlspecialchars($row->visions_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Vision Title (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="visions_name_ar" value="<?php echo htmlspecialchars($row->visions_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Vision Attachment</strong><br />
                                    <input name="vision_file" id="vision_file" type="file"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Vision Short Description (English)<font color='#990000'>*</font></strong><br /> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'visions_description',  @$row->visions_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>Vision Short Description (Arabic)<font color='#990000'>*</font></strong><br />                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'visions_description_ar',  @$row->visions_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong>Vision Body (English)<font color='#990000'>*</font></strong><br /> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'visions_longdescription',  @$row->visions_longdescription , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>Vision Body (Arabic)<font color='#990000'>*</font></strong><br />                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'visions_longdescription_ar',  @$row->visions_longdescription_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong>Mission Body (English)</strong><br />
                                <?php echo $editor->display( 'visions_mission',  @$row->visions_mission , '50%', '10', '80', '5', true, false ) ; ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong>Mission Body (Arabic)</strong><br />
                                <?php 
								 echo $editor->display( 'visions_mission_ar',  @$row->visions_mission_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong>Values Body (English)</strong><br />
                                <?php 
								 echo $editor->display( 'visions_values',  @$row->visions_values , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong>Values Body (Arabic)</strong><br />
                                <?php 
								 echo $editor->display( 'visions_values_ar',  @$row->visions_values_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.visions" />
    </form>
<?php
}   


function listOurVision($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', true);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title">ID</th>
            <th valign="top" class="title">Vision Title (English)</th>
            <th valign="top" class="title">Vision Title (Arabic)</th>
            <th valign="top" class="title">Short Description (English)</th>
            <th valign="top" class="title">Short Description (Arabic)</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.visions&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit Announcement"><?php echo $row->visions_name; ?></a></td>
            <td valign="top"><?php echo $row->visions_name_ar; ?></td>
            <td valign="top"><?php echo $row->visions_description; ?></td>
            <td valign="top"><?php echo $row->visions_description_ar; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 

////////////////////                Alerts              ///////////////////////	
function editAlerts($option, $row, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.alerts') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.alerts_name.value == "" 
			 || form.alerts_name_ar.value == ""
			 || form.alerts_enddate.value == ""
			 || tinyMCE.get('alerts_description').getContent()== ""
			 || tinyMCE.get('alerts_description_ar').getContent() == ""
			 || tinyMCE.get('alerts_longdescription').getContent()== ""
			 || tinyMCE.get('alerts_longdescription_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
			return false;	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? 'Edit Alerts' : 'Add Alerts';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong>Alert Title (English)<font color='#990000'>*</font></strong><br />
                                    <input name="alerts_name" value="<?php echo htmlspecialchars($row->alerts_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Alert Title (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="alerts_name_ar" value="<?php echo htmlspecialchars($row->alerts_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Alert Short Description (English)<font color='#990000'>*</font></strong><br /> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'alerts_description',  @$row->alerts_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>Alert Short Description (Arabic)<font color='#990000'>*</font></strong><br />                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'alerts_description_ar',  @$row->alerts_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                           <tr>
                                <td><strong>Alert Body (English)<font color='#990000'>*</font></strong><br /> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'alerts_longdescription',  @$row->alerts_longdescription , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>Alert Body (Arabic)<font color='#990000'>*</font></strong><br />                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'alerts_longdescription_ar',  @$row->alerts_longdescription_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>
                          <tr>
                                <td><strong>Alert Date<font color='#990000'>*</font></strong><br />
                                <?php JHTML::_('behavior.calendar'); ?>
                                 <input class="inputbox" type="text" name="alerts_startdate" id="alerts_startdate"  value="<?php echo strtotime($row->alerts_startdate) ? date('d-m-Y', strtotime(htmlspecialchars($row->alerts_startdate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('alerts_startdate','%d-%m-%Y');"/>
                            </tr>
                            <tr>
                                <td><strong>End Date<font color='#990000'>*</font></strong><br />
                                <?php JHTML::_('behavior.calendar'); ?>
                                 <input class="inputbox" type="text" name="alerts_enddate" id="alerts_enddate"  value="<?php echo strtotime($row->alerts_enddate) ? date('d-m-Y', strtotime(htmlspecialchars($row->alerts_enddate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('alerts_enddate','%d-%m-%Y');"/>
                            </tr>
                            <tr>
                                <td><strong>Publish</strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="alerts_startdate" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.alert" />
    </form>
<?php
}   


function listAlerts($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', true);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title">ID</th>
            <th valign="top" class="title">Alert Title</th>
            <th valign="top" class="title">Start Date</th>
            <th valign="top" class="title">End Date</th>
            <th valign="top" class="title">Publish</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.alerts&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit Alert"><?php echo $row->alerts_name; ?></a></td>
            <td valign="top"><?php echo date('d-m-Y', strtotime($row->alerts_startdate)); ?></td>
            <td valign="top"><?php echo date('d-m-Y', strtotime($row->alerts_enddate)); ?></td>
            <td valign="top"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 

////////////////////                Quotes   	          ///////////////////////
function editQuotes($option, $row, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.quotes') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.quotes_name.value == "" 
			 || form.quotes_name_ar.value == ""
			 || tinyMCE.get('quotes_description').getContent()== ""
			 || tinyMCE.get('quotes_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
			return false;	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? 'Edit Quote' : 'Add Quote';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong>Quote Title (English)<font color='#990000'>*</font></strong><br />
                                    <input name="quotes_name" value="<?php echo htmlspecialchars($row->quotes_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Quote Title (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="quotes_name_ar" value="<?php echo htmlspecialchars($row->quotes_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                            	<td>
                                	<strong>Image</strong><br />
                                    <input name="quotes_file" id="quotes_file" type="file"/>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Description (English)<font color='#990000'>*</font></strong><br /> 
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'quotes_description',  @$row->quotes_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>Description (Arabic)<font color='#990000'>*</font></strong><br />                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'quotes_description_ar',  @$row->quotes_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>Publish</strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>                                                                              
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.quotes" />
    </form>
<?php
}   


function listQuotes($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', true);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title">ID</th>
            <th valign="top" class="title">Quote Title (English)</th>
            <th valign="top" class="title">Quote Title (Arabic)</th>
            <th valign="top" class="title">Description (English)</th>
            <th valign="top" class="title">Description (Arabic)</th>
            <th valign="top" class="title">Publish</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.quotes&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit Quote"><?php echo $row->quotes_name; ?></a></td>
            <td valign="top"><?php echo $row->quotes_name_ar; ?></td>
            <td valign="top"><?php echo $row->quotes_description; ?></td>
            <td valign="top"><?php echo $row->quotes_description_ar; ?></td>
            <td valign="top"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 

////////////////////                Policies              ///////////////////////	
function editPolicies($option, $row, $departmentList, $departmentAccess, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">						
                                    
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.policies') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.policies_name.value == "" 
			 || form.policies_name_ar.value == ""
			 || form.policies_policydate.value == ""
 <?php if(!$row->id){?>
             || form.policies_file.value == ""
			 || form.policies_file_ar.value == "" 
<?php }?>
 ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
			return false;	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? 'Edit Policy' : 'Add Policy';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong>Policy Title (English)<font color='#990000'>*</font></strong><br />
                                    <input name="policies_name" value="<?php echo htmlspecialchars($row->policies_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Policy Title (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="policies_name_ar" value="<?php echo htmlspecialchars($row->policies_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                           <tr>
                                <td><strong>Policy Date<font color='#990000'>*</font></strong><br />
                                <?php JHTML::_('behavior.calendar'); ?>
                                 <input class="inputbox" type="text" name="policies_policydate" id="policies_policydate"  value="<?php echo strtotime($row->policies_policydate) ? date('d-m-Y', strtotime(htmlspecialchars($row->policies_policydate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('policies_policydate','%d-%m-%Y');"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Policy Document (English)<font color='#990000'>*</font></strong><br />
                                <input name="policies_file" id="policies_file" type="file" <?php print $row->policies_doc != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->policies_doc != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="policies_doc_nm"><?php print $row->policies_doc; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('policies_file', 'policies_doc_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Policy Document (Arabic)<font color='#990000'>*</font></strong><br />
                                <input name="policies_file_ar" id="policies_file_ar" type="file" <?php print $row->policies_doc_ar != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->policies_doc_ar != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="policies_doc_ar_nm"><?php print $row->policies_doc_ar; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('policies_file_ar', 'policies_doc_ar_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                           <tr>
                                <td><strong>Is Public?</strong><br />
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           
                            <tr>
                                <td><strong>Select Department</strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Publish</strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.policies" />
    </form>
<?php
}   


function listPolicies($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', true);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title">ID</th>
            <th valign="top" class="title">Policy Title</th>
            <th valign="top" class="title">Department Name</th>
            <th valign="top" class="title">Policy Date</th>
            <th valign="top" class="title">Publish</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.policies&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit Policy"><?php echo $row->policies_name; ?></a></td>
            <td valign="top"><?php echo $row->department; ?></td>
            <td valign="top"><?php echo date('d-m-Y', strtotime($row->policies_policydate)); ?></td>
            <td valign="top"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 

////////////////////                Manuals              ///////////////////////	
function editManuals($option, $row, $departmentList, $departmentAccess, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">                          
	function deletefile(filefield, filename) {
		document.getElementById(filename).style.display = 'none';
		document.getElementById(filefield).style.display = 'block';
		return false;
	}
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.manuals') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.manuals_name.value == "" 
			 || form.manuals_name_ar.value == ""
			 || form.manuals_manualdate.value == ""
<?php if(!$row->id){?>
			 || form.manuals_file.value == ""
			 || form.manuals_file_ar.value == "" 
<?php }?>
){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
			return false;	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? 'Edit Manual' : 'Add Manual';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong>Manual Title (English)<font color='#990000'>*</font></strong><br />
                                    <input name="manuals_name" value="<?php echo htmlspecialchars($row->manuals_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Manual Title (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="manuals_name_ar" value="<?php echo htmlspecialchars($row->manuals_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                           <tr>
                                <td><strong>Manual Date<font color='#990000'>*</font></strong><br />
                                <?php JHTML::_('behavior.calendar'); ?>
                                 <input class="inputbox" type="text" name="manuals_manualdate" id="manuals_manualdate"  value="<?php echo strtotime($row->manuals_manualdate) ? date('d-m-Y', strtotime(htmlspecialchars($row->manuals_manualdate, ENT_QUOTES))) : ''; ?>" onClick="return showCalendar('manuals_manualdate','%d-%m-%Y');"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Manual Document (English)<font color='#990000'>*</font></strong><br />
                                <input name="manuals_file" id="manuals_file" type="file" <?php print $row->manuals_doc != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->manuals_doc != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="manuals_doc_nm"><?php print $row->manuals_doc; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('manuals_file', 'manuals_doc_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Manual Document (Arabic)<font color='#990000'>*</font></strong><br />
                                <input name="manuals_file_ar" id="manuals_file_ar" type="file" <?php print $row->manuals_doc_ar != '' ? 'style="display:none;"' : ''; ?>/>
                                    <?php if ($row->manuals_doc_ar != '') : ?>
                                    	<div style="color:#135D8C; line-height:22px;" id="manuals_doc_ar_nm"><?php print $row->manuals_doc_ar; ?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="return deletefile('manuals_file_ar', 'manuals_doc_ar_nm');" style="color:#FF0000;"><img src="<?php echo JURI::base()?>images/DeleteRed.png" border="0" width="16" height="16" /></a></div>
                                    <?php endif;?>
                                                                 
                            </tr>
                           <tr>
                                <td><strong>Is Public?</strong><br />
                                    <?php echo $departmentAccess;?>
                                                                 
                            </tr>
                           
                            <tr>
                                <td><strong>Select Department</strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Publish</strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.manuals" />
    </form>
<?php
}   


function listManuals($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', true);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title">ID</th>
            <th valign="top" class="title">Manual Title</th>
            <th valign="top" class="title">Department Name</th>
            <th valign="top" class="title">Manual Date</th>
            <th valign="top" class="title">Publish</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.manuals&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit Manual"><?php echo $row->manuals_name; ?></a></td>
            <td valign="top"><?php echo $row->department; ?></td>
            <td valign="top"><?php echo date('d-m-Y', strtotime($row->manuals_manualdate)); ?></td>
            <td valign="top"><?php echo ($row->publish)? 'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
} 

////////////////////                Benchmarks              ///////////////////////	
function editBenchmarks($option, $row, $departmentList, $isPublist){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.benchmarks') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.benchmarks_name.value == "" 
			 || form.benchmarks_name_ar.value == ""
			 || tinyMCE.get('benchmarks_description').getContent()== ""
			 || tinyMCE.get('benchmarks_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
			return false;	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? 'Edit Benchmarks' : 'Add Benchmarks';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong>Benchmarks Title (English)<font color='#990000'>*</font></strong><br />
                                    <input name="benchmarks_name" value="<?php echo htmlspecialchars($row->benchmarks_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Benchmarks Title (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="benchmarks_name_ar" value="<?php echo htmlspecialchars($row->benchmarks_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr> 
                            <tr>
                                <td><strong>Benchmarks Description (English)<font color='#990000'>*</font></strong><br />
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'benchmarks_description',  @$row->benchmarks_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>Benchmarks Description (Arabic)<font color='#990000'>*</font></strong><br />
                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'benchmarks_description_ar',  @$row->benchmarks_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>                          
                            <tr>
                                <td><strong>Select Department</strong><br />
                                    <?php echo $departmentList;?>
                                                                 
                            </tr>
                            <tr>
                                <td><strong><?php echo JText::_('COM_EPOSTS_PUBLISH_TITLE_ALL')." "; ?></strong><br />
                                    <?php echo $isPublist;?>
                                                                 
                            </tr>
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.benchmarks" />
    </form>
<?php
}   


function listBenchmarks($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', true);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title">ID</th>
            <th valign="top" class="title">Benchmarks Title</th>
            <th valign="top" class="title">Department Name</th>
            <th valign="top" class="title">Benchmarks Description</th>
            <th valign="top" class="title">Publish</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.benchmarks&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit Bunchmark"><?php echo $row->benchmarks_name; ?></a></td>
            <td valign="top"><?php echo $row->department; ?></td>
            <td valign="top"><?php echo $row->benchmarks_description; ?></td>
            <td valign="top"><?php echo ($row->publish == 1)?'Yes':'No'; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
}

////////////////////                FAQs              ///////////////////////	
function editFaqs($option, $row, $departmentList){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.faqs') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.faqs_name.value == "" 
			 || form.faqs_name_ar.value == ""
			 || tinyMCE.get('faqs_description').getContent()== ""
			 || tinyMCE.get('faqs_description_ar').getContent() == "" ){
            alert( "<?php echo JText::_('COM_EPOSTS_STARIC_TITLE_ALL');?>" );
			return false;	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? 'Edit FAQs' : 'Add FAQs';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong>FAQ Question (English)<font color='#990000'>*</font></strong><br />
                                    <input name="faqs_name" value="<?php echo htmlspecialchars($row->faqs_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>FAQ Question (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="faqs_name_ar" value="<?php echo htmlspecialchars($row->faqs_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr> 
                            <tr>
                                <td><strong>Description (English)<font color='#990000'>*</font></strong><br />
                                <?php 
								$editor =& JFactory::getEditor();
								echo $editor->display( 'faqs_description',  @$row->faqs_description , '50%', '10', '80', '5', true, false ) ;
								?>
                                   </td>
                           </tr> 
                           <tr>
                                <td><strong>Description (Arabic)<font color='#990000'>*</font></strong><br />
                                 <?php 
								 $editor2 =& JFactory::getEditor();
								 echo $editor2->display( 'faqs_description_ar',  @$row->faqs_description_ar , '50%', '10', '80', '5', true, false ) ;
								 ?>
                                   </td>
                           </tr>                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.faqs" />
    </form>
<?php
}   


function listFaqs($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', false);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', true);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="4">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title">ID</th>
            <th valign="top" class="title">FAQ Question</th>
            <th valign="top" class="title">Description</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.faqs&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit Bunchmark"><?php echo $row->faqs_name; ?></a></td>
            <td valign="top"><?php echo $row->faqs_description; ?></td>
            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
}

////////////////////                Links              ///////////////////////	
function editLinks($option, $row, $departmentList){
global $mainframe, $jlistConfig;

$user = &JFactory::getUser(); 
    ?>
    <script language="javascript" type="text/javascript">
    
    Joomla.submitbutton = function(pressbutton) 
    {
        var form = document.adminForm;
        if (pressbutton == 'cancel.links') {
            submitform( pressbutton );
            return;
        }

		// do field validation
        if ( form.links_name.value == "" 
			 || form.links_name_ar.value == ""
			 || form.links_link.value == "" 
			 || form.links_link_ar.value == "" );
			return false;	
        } else {
            Joomla.submitform( pressbutton );
            return true;
        }
    }
 
    </script>    
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" border="0">
        <tr>
            <td width="100%" valign="top">
            <table cellpadding="4" cellspacing="1" border="0" class="adminlist">
                <tr>
                      <th class="adminheading" colspan="2"><?php echo $row->id ? 'Edit Link' : 'Add Link';?></th>
                  </tr>
                  <tr>
                      <td valign="top" align="left" width="100%">
                          <table class="admintable" width="100%">
                             <tr>
                                <td><strong>Title (English)<font color='#990000'>*</font></strong><br />
                                    <input name="links_name" value="<?php echo htmlspecialchars($row->links_name, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Title (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="links_name_ar" value="<?php echo htmlspecialchars($row->links_name_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Link (English)<font color='#990000'>*</font></strong><br />
                                    <input name="links_link" value="<?php echo htmlspecialchars($row->links_link, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr>
                            <tr>
                                <td><strong>Link (Arabic)<font color='#990000'>*</font></strong><br />
                                    <input name="links_link_ar" value="<?php echo htmlspecialchars($row->links_link_ar, ENT_QUOTES); ?>" size="80" maxlength="150"/>
                                                                 
                            </tr> 
                                                                               
                          </table>                         
                      </td>
                  </tr>
            </table>
            </td>                                                               
        </tr>
    </table>
<br /><br />
<?php
$user		= JFactory::getUser();
?>
        <input type="hidden" name="hidemainmenu" value="1">
        <input type="hidden" name="option" value="<?php echo $option; ?>" />
        <input type="hidden" name="cid" value="<?php echo $row->id;?>" />
        <input type="hidden" name="publishedon" value="<?php  if($row->publishedon){ echo $row->publishedon;}else{ echo date($jlistConfig['global.datetime']);}?>" />
        <input type="hidden" name="publishedby" value="<?php  if($row->publishedby){ echo $row->publishedby;}else{ echo $user->id;}?>" />
        <input type="hidden" name="task" value="edit.link" />
    </form>
<?php
}   


function listLinks($task, $rows, $option, $pageNav, $search, $limitstart, $limit) {
   global $mainframe;
   
    //Create menu
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_MAIN' ), 'index.php?option=com_eposts', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DEPARTMENTS' ), 'index.php?option=com_eposts&task=view.departments', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DMS' ), 'index.php?option=com_eposts&task=view.dms', false);
    JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_CIRCULARS' ), 'index.php?option=com_eposts&task=view.circulars', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_TASAWAQS' ), 'index.php?option=com_eposts&task=view.tasawaqs', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_NEWHIRES' ), 'index.php?option=com_eposts&task=view.newhires', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_EVENTS' ), 'index.php?option=com_eposts&task=view.events', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_OFFICES' ), 'index.php?option=com_eposts&task=view.offices', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_CPANEL_RESOURCES' ), 'index.php?option=com_eposts&task=view.resources', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_DWNMEDIAS' ), 'index.php?option=com_eposts&task=view.dwnmedias', false);
	JSubMenuHelper::addEntry( JText::_( 'COM_EPOSTS_BACKEND_CPANEL_APPROVEBOOKINN' ), 'index.php?option=com_eposts&task=view.approvebookingrequest', false);
	 // get stats data
	JSubMenuHelper::addEntry( 'Announcements', 'index.php?option=com_eposts&task=view.announcements', false);
	JSubMenuHelper::addEntry( 'News', 'index.php?option=com_eposts&task=view.news', false);
	JSubMenuHelper::addEntry( 'Our Vision', 'index.php?option=com_eposts&task=view.visions', false);
	JSubMenuHelper::addEntry( 'Alerts', 'index.php?option=com_eposts&task=view.alerts', false);
	JSubMenuHelper::addEntry( 'Policies & Procedures', 'index.php?option=com_eposts&task=view.policies', false);
	JSubMenuHelper::addEntry( 'Quotes', 'index.php?option=com_eposts&task=view.quotes', false);
	JSubMenuHelper::addEntry( 'Manuals & Guides', 'index.php?option=com_eposts&task=view.manuals', false);
	JSubMenuHelper::addEntry( 'Benchmarks', 'index.php?option=com_eposts&task=view.benchmarks', false);
	JSubMenuHelper::addEntry( 'Links', 'index.php?option=com_eposts&task=view.links', true);
	JSubMenuHelper::addEntry( 'Sync Users', 'index.php?option=com_eposts&task=view.syncusers', false);
	JSubMenuHelper::addEntry( 'FAQs', 'index.php?option=com_eposts&task=view.faqs', false);
// Put auto AD user add here
?>
    <form action="index.php" method="post" name="adminForm" id="adminForm">
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      <tr>
          <td colspan="4" align="left">
                <?php //echo ' '.JText::_('COM_EPOSTS_BACKEND_DEPARTMENT_LIST_NOTE'); ?>
            </td>
          <td  align="right" colspan="0">
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH')." "; ?>
                <input type="text" name="search" value="<?php echo htmlspecialchars(stripslashes($search));?>" class="text_area" onChange="document.adminForm.submit();" />
            <?php echo JText::_('COM_EPOSTS_BACKEND_CATSLIST_SEARCH_LIMIT')." ";
                  echo $pageNav->getLimitBox();
            ?>
          </td>
      </tr>
        <tr>
            <th valign="top" width="5" align="left"><input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $rows ); ?>);" /></th>
            <th valign="top" width="10" align="left" class="title">ID</th>
            <th valign="top" class="title">Title (English)</th>
            <th valign="top" class="title">Title (Arabic)</th>
            <th valign="top" class="title">Link (English)</th>
            <th valign="top" class="title">Link (Arabic)</th>
        </tr>
        <?php
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
            $link = 'index.php?option=com_eposts&task=edit.links&hidemainmenu=1&cid='.$row->id;
            $checked     = JHTML::_('grid.checkedout', $row, $i );
            ?>
        <tr class="<?php echo "row$k"; ?>">
            <td valign="top"><?php echo $checked; ?></td>
            <td valign="top"><?php echo $row->id; ?></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit Link"><?php echo $row->links_name; ?></a></td>
            <td valign="top"><a href="<?php echo $link; ?>" title="Edit Link"><?php echo $row->links_name_ar; ?></a></td>
            <td valign="top"><?php echo $row->links_link; ?></td>
            <td valign="top"><?php echo $row->links_link_ar; ?></td>

            <?php $k = 1 - $k;  } ?>
        </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr>
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="option" value="<?php echo $option; ?>" />
    <input type="hidden" name="task" value="<?php echo $task; ?>" />
    <input type="hidden" name="limitstart" value="<?php echo $limitstart; ?>" />
    <input type="hidden" name="hidemainmenu" value="0">
</form>
<?php   
}


//////////////////////////////////////END DEVELOPMENT PHASE III///////////////////////////////////  
  

}//end class

?>