<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_media
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
$params = new JRegistry;
$dispatcher	= JDispatcher::getInstance();
$dispatcher->trigger('onContentBeforeDisplay', array('com_media.file', &$this->_tmp_img, &$params));

$config =& JFactory::getConfig();
$filepath = $config->getValue( 'config.file_path' );

$this->baseURL = $filepath == '' ? $this->baseURL : $filepath;
//echo $this->baseURL;

$img =  $this->baseURL.'\\images\\'.$this->_tmp_img->name;

$img = str_replace("\\", "/", $img);
$str2 = substr('data:image/jpeg;base64,', 2);

 $imgbinary = fread(fopen($img, "r"), filesize($img));
      // echo $imgbinary;
	   $aaa= 'data:image/jpeg;base64,' . base64_encode($imgbinary);
	   $str2 = substr($aaa, 4);

?>
	
	<div class="item">
			
		<a href="javascript:ImageManager.populateFields('<?php  echo 'data:image/jpeg;base64,' . base64_encode($imgbinary) ; ?>')" title="<?php echo $this->_tmp_img->name; ?>" >
<img  height=" 100px" width="100px" src="<?php echo $aaa; ?>" />
</a>
		</div>
<?php
$dispatcher->trigger('onContentAfterDisplay', array('com_media.file', &$this->_tmp_img, &$params));
?>
