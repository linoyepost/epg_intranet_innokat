<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AriAdminViewAriAdmins extends JView
{
	function display($tpl = null) 
	{
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		
		$this->addToolBar();
		
		parent::display($tpl);
	}

	protected function addToolBar() 
	{
		JToolBarHelper::title(JText::_('COM_ARIADMIN_ARIADMIN'), 'generic.png');
		JToolBarHelper::addNew('ariadmin.add', 'JTOOLBAR_NEW');
		JToolBarHelper::editList('ariadmin.edit', 'JTOOLBAR_EDIT');
		JToolBarHelper::deleteList('', 'ariadmin.delete', 'JTOOLBAR_DELETE');
		JToolBarHelper::divider();
		JToolBarHelper::preferences('com_ariadmin');
	}
}
