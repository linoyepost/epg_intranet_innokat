<?php
defined('_JEXEC') or die;

JHtml::_('behavior.multiselect');
?>
<form action="<?php echo JRoute::_('index.php?option=com_ariadmin&view=ariadmins');?>" method="post" name="adminForm" id="adminForm">
	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th>
					<?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'a.title', $listDirn, $listOrder); ?>
				</th>
				<th width="10%">
					<?php echo JHtml::_('grid.sort', 'COM_ARIADMIN_QUESTIONS', 'a.title', $listDirn, $listOrder); ?>
				</th>
				<th width="5%">
					<?php echo JHtml::_('grid.sort', 'JSTATUS', 'a.published', $listDirn, $listOrder); ?>
				</th>
				<th width="1%" class="nowrap">
					<?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="15">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php
			foreach ($this->items as $i => $item) :
			?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center">
						<?php echo JHtml::_('grid.id', $i, $item->QuizId); ?>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_ariadmin&task=ariadmin.edit&QuizId='.$item->QuizId);?>">
							<?php echo $this->escape($item->QuizName); ?>
                        </a>
					</td>
					<td class="center">
						<a href="<?php print JRoute::_('index.php?option=com_ariadmin&view=ariquestions&QuizId='.$item->QuizId); ?>"><?php echo JText::_('COM_ARIADMIN_VIEW'); ?></a>
					</td>
					<td class="center">
						<?php echo JHtml::_('jgrid.published', $item->Status, $i);?>
					</td>
					<td class="center">
						<?php echo (int) $item->QuizId; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="extension" value="<?php echo $extension;?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<input type="hidden" name="original_order_values" value="<?php echo implode($originalOrders, ','); ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
