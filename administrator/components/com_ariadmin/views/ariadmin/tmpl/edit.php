<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_categories
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
?>
<form action="<?php echo JRoute::_('index.php?option=com_ariadmin&layout=edit&QuizId='.(int) $this->item->QuizId); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_CATEGORIES_FIELDSET_DETAILS');?></legend>
			<ul class="adminformlist">
            <li><?php echo $this->form->getLabel('QuizId'); ?>
            <?php echo $this->form->getInput('QuizId'); ?></li>
            
            <li><?php echo $this->form->getLabel('QuizName'); ?>
            <?php echo $this->form->getInput('QuizName'); ?></li>
            
            <li><?php echo $this->form->getLabel('QuizName_ar'); ?>
            <?php echo $this->form->getInput('QuizName_ar'); ?></li>
            
            <li><?php echo $this->form->getLabel('CategoryId'); ?>
            <?php echo $this->form->getInput('CategoryId'); ?></li>
            
            <li><?php echo $this->form->getLabel('Status'); ?>
            <?php echo $this->form->getInput('Status'); ?></li>
            
            <li><?php echo $this->form->getLabel('TotalTime'); ?>
            <?php echo $this->form->getInput('TotalTime'); ?></li>
            
            <li><?php echo $this->form->getLabel('PassedScore'); ?>
            <?php echo $this->form->getInput('PassedScore'); ?></li>
            
            <li><?php echo $this->form->getLabel('QuestionCount'); ?>
            <?php echo $this->form->getInput('QuestionCount'); ?></li>
            
            <li><?php echo $this->form->getLabel('QuestionTime'); ?>
            <?php echo $this->form->getInput('QuestionTime'); ?></li>
			</ul>
            <div>
            <?php echo $this->form->getLabel('Description'); ?>
            <div style="clear:both;"></div>
            <?php echo $this->form->getInput('Description'); ?>
            <div style="clear:both;"></div>
            
            <?php echo $this->form->getLabel('Description_ar'); ?>
            <div style="clear:both;"></div>
            <?php echo $this->form->getInput('Description_ar'); ?>
            <div style="clear:both;"></div>
            </div>
		</fieldset>
	</div>

	<div class="width-40 fltrt">
	</div>
	<div class="clr"></div>
	<div>
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
