<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AriAdminModelAriQuestions extends JModelList
{
	protected function getListQuery() 
	{
		$QuizId = JRequest::getCmd('QuizId');
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		
		$query->select('QQC.QuestionId as QuestionId,QQC.QuestionVersionId as QuestionVersionId,QQC.Status as Status, QQV.Question as Question');
		$query->from('#__ariquizquestion as QQC');
		$query->join('LEFT', '#__ariquizquestionversion as QQV ON QQV.QuestionVersionId = QQC.QuestionVersionId');
		$query->where('QQC.QuizId = "' . $QuizId . '"');
		
		return $query;
	}
}
