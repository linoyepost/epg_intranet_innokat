<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class AriAdminModelAriCategory extends JModelAdmin
{
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_ariadmin.ariadmin', 'ariadmin', array('control' => 'jform', 'load_data' => $loadData));
		return $form;
	}

	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_ariadmin.edit.ariadmin.data', array());
		if(empty($data)){
			$data = $this->getItem();
		}
		return $data;
	}
    
	public function getTable($name = 'AriCategory', $prefix = 'AriAdminTable', $options = array())
	{
		return parent::getTable($name, $prefix, $options);
	}
}
