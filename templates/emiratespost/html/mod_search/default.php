<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_search
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
JHTML::_('behavior.mootools');
$document = &JFactory::getDocument();
$document->addScriptDeclaration("
	window.addEvent('domready',function(){
		var value = $$('.search_area .search input.searchbox').get('value');
		if (value != '' && value != '" . $text . "') {
			$$('.search_area .search a.clearsearch').setStyle('display', 'block');
		}
		$$('.search_area .search input.searchbox').addEvent('keydown', function(){
			$$('.search_area .search a.clearsearch').setStyle('display', 'block');
		});
		$$('.search_area .search a.clearsearch').addEvent('click', function(e){
			Event.stop(e);
			$$('.search_area .search input.searchbox').set('value', '" . $text . "');
			this.setStyle('display', 'none');
		});
	});
");
?>

<form action="<?php echo JRoute::_('index.php');?>" method="post">
	<div class="search <?php echo $moduleclass_sfx ?>">
		<?php
			$output = '<input name="searchword" id="mod-search-searchword"  class="inputbox'.$moduleclass_sfx.' searchbox" type="text" size="'.$width.'" value="'.$text.'"  onblur="if (this.value==\'\') this.value=\''.$text.'\';" onfocus="if (this.value==\''.$text.'\') this.value=\'\';" />';
			?>
			<a href="#" class="clearsearch">clear</a>
			<?php

			if ($button) :
				if ($imagebutton) :
					$button = '<input type="image" value="'.$button_text.'" class="button'.$moduleclass_sfx.'" src="'.$img.'" onclick="this.form.searchword.focus();"/>';
				else :
					$button = '<input type="submit" value="'.$button_text.'" class="button'.$moduleclass_sfx.'" onclick="this.form.searchword.focus();"/>';
				endif;
			endif;

			switch ($button_pos) :
				case 'top' :
					$button = $button.'<br />';
					$output = $button.$output;
					break;

				case 'bottom' :
					$button = '<br />'.$button;
					$output = $output.$button;
					break;

				case 'right' :
					$output = $output.$button;
					break;

				case 'left' :
				default :
					$output = $button.$output;
					break;
			endswitch;

			echo $output;
			$mitemid = '312';
		?>
	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="394" />
	</div>
</form>
