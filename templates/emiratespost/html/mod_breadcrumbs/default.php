<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_breadcrumbs
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$option = JRequest::getVar('option');
$view = JRequest::getVar('view');

$app = JFactory::getApplication();
$menu = $app->getMenu();

$dptItemid = $_SESSION['dpItemid'];
$dptlink = $menu->getItem($dptItemid);

$language = &JFactory::getLanguage();
$tag = $language->get('tag');

$page = JRequest::getCmd('page');
?>

<div class="breadcrumbs<?php echo $moduleclass_sfx; ?>">
	<div class="showHere"><a href="<?php print JURI::base(); ?>"><?php print JText::_('EMIRATES_TEMPLATE_BACKPORTAL'); ?></a></div>
    <div class="showCrmb">
    	<ul>
        
        
	<?php 
	if($page == 'home'){
		unset($_SESSION['deptid']);
	}
	//print_r($list);
	for ($i = 0; $i < $count; $i ++) :
        // Workaround for duplicate Home when using multilanguage
		//if ($i == 1 && !empty($list[$i]->link) && !empty($list[$i-1]->link) && $list[$i]->link == $list[$i-1]->link) {
       		
		if($list[$i]->name == "Links"  || $list[$i]->name == "Departments"){
				continue;
		}
		
		/*if($view == 'mainshownewhires'  && $i > 2){
				continue;
		}*/
				
		if ($i == 1 && $_SESSION['deptid'] > 0 && $page != 'home' && (($option == 'com_igallery' && $view == 'category') || ($option == 'com_eposts' && (strstr($view, 'main') || strstr($view, 'show')) && !($view == 'mainlistmedias' || $view == 'mainliststaffdirectories' || $view == 'mainlistlinks')))) :
			$database = &JFactory::getDBO();
			$database->setQuery("SELECT departments_name, departments_name_ar FROM #__eposts_departments WHERE id = " . $_SESSION['deptid']);
       		$department = $database->loadObject();
			$dpTitle = $department->departments_name;
			if ($tag == 'ar-AA') :
				$dpTitle = $department->departments_name_ar;
			endif;
			print '<li>';
			print '<a href="'.$dptlink->link.'&Itemid='.$dptItemid.'">';
			echo ' '.$separator.' ';
                        //Bread crumb length will be defined here
			//print substr($dpTitle,0,30);
			print substr($dpTitle,0,35);
			print '</a>';
			
			print '</li>';
		endif;
		
		 if ($i == 1 && !empty($list[$i]->link) && !empty($list[$i-1]->link) && $list[$i]->link == $list[$i-1]->link) {
            continue;
        }
        // If not the last item in the breadcrumbs add the separator
		////////////////////////////////////////////////////////////////////////////////////////////
		   /*if($list[$i-1]->name != "Links" && $list[$i-1]->name != "Departments"){
            if($i > 0){
                echo ' '.$separator.' ';
            }
			}else{
				echo '&nbsp;';
			}*/
		////////////////////////////////////////////////////////////////////////////////////////////	
			
        if ($i < $count -1) {
			
            print '<li>';
			if (!empty($list[$i]->link)) {
                echo '<a href="'.$list[$i]->link.'" class="pathway">';
				if($i > 0){ echo ' '.$separator.' '; }
				echo $list[$i]->name.'</a>';
            } else {				
                echo '<span>';
				if($i > 0){ echo ' '.$separator.' '; }
                echo $list[$i]->name;
                echo '</span>';
            }
            print '</li>';
        } elseif ($params->get('showLast', 1)) { // when $i == $count -1 and 'showLast' is true
            print '<li>';				
             echo '<span>';
			    if($i > 0){ echo ' '.$separator.' '; }
                            //Bread crumb length will be defined here
			    //echo substr($list[$i]->name,0,30);
			    echo substr($list[$i]->name,0,35);
			 echo '</span>';
            print '</li>';
        }
    	endfor;
		?>
    	</ul>
    </div>
</div>
