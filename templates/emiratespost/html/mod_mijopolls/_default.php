<?php
/**
* @version		1.0.0
* @package		MijoPolls
* @subpackage	MijoPolls
* @copyright	2009-2011 Mijosoft LLC, www.mijosoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		GNU/GPL based on AcePolls www.joomace.net
*
* Based on Apoll Component
* @copyright (C) 2009 - 2011 Hristo Genev All rights reserved
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @link http://www.afactory.org
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$language = &JFactory::getLanguage();
$tag = $language->get('tag');
 
$document = JFactory::getDocument();	 
$document->addStyleDeclaration("div#poll_loading_".$poll->id." {
	background: url(media/system/images/mootree_loader.gif) 0% 50% no-repeat;
	width:100%;
	height:20px;
	padding: 4px 0 0 20px;
}
");
?>

<div class="poll<?php echo $params->get('moduleclass_sfx'); ?>" style="border:none; padding:1px;">
  <div class="polldiv" id="polldiv_<?php echo $poll->id;?>">
    <?php if ($display_poll) { ?>
    <form action="<?php echo JRoute::_('index.php');?>" method="post" name="poll_vote_<?php echo $poll->id;?>" id="poll_vote_<?php echo $poll->id;?>">
      <div class="pollform<?php echo $poll->id;?>" id="pollform<?php echo $poll->id;?>">
        <div class="poll_chart"></div>
        <div class="poll_options">
          <?php if ($params->get('show_poll_title')) : ?>
          <?php
          	if ($tag == 'ar-AA') :
				$poll->title = $poll->title_ar;
			endif;
		  ?>
          <h4><?php echo $poll->title; ?></h4>
          <?php endif; ?>
          <div class="poll_optionscn">
			  <?php for ($i = 0, $n = count($options); $i < $n; $i ++) { ?>
              <?php
              	if ($tag == 'ar-AA') :
					$options[$i]->text = $options[$i]->text_ar;
				endif;
			  ?>
              <label for="mod_voteid<?php echo $options[$i]->id;?>" class="<?php echo $tabclass_arr[$tabcnt].$params->get('moduleclass_sfx'); ?>" style="display:block; padding:2px;">
              <input type="radio" name="voteid" id="mod_voteid<?php echo $options[$i]->id;?>" value="<?php echo $options[$i]->id;?>" alt="<?php echo $options[$i]->id;?>" <?php echo $disabled; ?> />
              <?php echo $options[$i]->text; ?> </label>
              <?php $tabcnt = 1 - $tabcnt; } 
                
                //show messages box
                if($params->get('show_msg')) : 
                    echo '<div id="mod_poll_messages_'.$poll->id.'" style="margin:5px;">'.JText::_($msg);
                    if($params->get('show_detailed_msg')) echo " ".$details;
                    echo '</div>';
                endif;
              ?>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="pollresults" id="pollresults<?php echo $poll->id;?>"></div>
      <div class="buttons">
        <div id="poll_buttons_<?php echo $poll->id;?>" class="votebtn">
          <input type="submit" id="submit_vote_<?php echo $poll->id; ?>" name="task_button" class="<?php echo (MijopollsHelper::is30() ? 'btn btn-primary' : 'button'); ?>" value="<?php echo JText::_('MOD_MIJOPOLLS_VOTE'); ?>" <?php echo $disabled; ?> />
        </div>
        <a class="viewresults" id="viewresults<?php echo $poll->id;?>" href="#" <?php if ($results[0]->votes == 0) :?><?php endif;?>><?php print JText::_('MOD_MIJOPOLLS_VIEW_RESULTS'); ?></a>
        <a class="viewresults" id="backbtn<?php echo $poll->id;?>" style="display:none;" href="#"><?php print JText::_('MOD_MIJOPOLLS_BACKBTN'); ?></a>
        <div class="clear"></div>
        <div class="view_all"><a class="poll_result_link" id="poll_result_link<?php echo $poll->id;?>" href="<?php echo JRoute::_('index.php?option=com_mijopolls&view=polls&Itemid=129'); ?>"><?php echo JText::_('MOD_MIJOPOLLS_VIEW_DETAILS'); ?></a></div>
      </div>
      <div id="poll_loading_<?php echo $poll->id;?>" style="display:none;"><?php echo JText::_('MOD_MIJOPOLLS_PROCESSING'); ?> </div>
      <input type="hidden" name="option" value="com_mijopolls" />
      <input type="hidden" name="id" value="<?php echo $poll->id;?>" />
      <?php if ($params->get('ajax')) { ?>
      <input type="hidden" name="format" value="raw" />
      <input type="hidden" name="view" value="poll" />
      <?php } else { ?>
      <input type="hidden" name="task" value="vote" />
      <?php };
	echo "<div>".JHTML::_('form.token')."</div>";  ?>
    </form>
    <?php if($params->get('ajax')) {
// add mootools
if (!MijopollsHelper::is30()) {
	JHTML::_('behavior.mootools');
}

$updateValue = '';
$poll_bars_color = $params->get('poll_bars_color');

$updateValue .= "<h4>' + poltitle.item(0).firstChild.nodeValue + '</h4>";

for ($i = 0; $i < count($results); $i++) {
	if ($params->get('only_one_color')) {
		$background_color = $poll_bars_color;
	}
	else {
		$background_color = "' + options.item($i).attributes[2].nodeValue + '";	
	}

	$updateValue .= "<div style=\"width:100%\"><div style=\"padding: 3px;\">' + text.item($i).firstChild.nodeValue + ' - ' + options.item($i).attributes[1].nodeValue + '%</div><div class=\"poll_module_bar_holder\" id=\"poll_module_bar_holder".$i."\" style=\"width: 98%; height: 10px; padding:1px; border:1px solid #".$params->get('poll_bars_border_color').";\"><div id=\"poll_module_bar'+options.item($i).attributes[0].nodeValue+'\" style=\"background:#$background_color; width:' + options.item($i).attributes[1].nodeValue + '%; height:10px;\"></div></div></div>";
}

if ($params->get('show_total')) {
	$updateValue .= "<br /><b>".JText::_('MOD_MIJOPOLLS_TOTAL_VOTES')."</b>: ' + voters.item(0).firstChild.nodeValue + '";
}

//$updateValue .= '<div class="view_all"><a class="poll_result_link" id="poll_result_link'.$poll->id.'" href="'.JRoute::_('index.php?option=com_mijopolls&view=polls&Itemid=129').'">'.JText::_('MOD_MIJOPOLLS_VIEW_DETAILS').'</a></div>';

$js = "
/* <![CDATA[ */

window.addEvent('load', function()
    {
		var slide2 = new Fx.Slide('pollresults".$poll->id."').hide();
		var req = new Request({
			url: '" . JRoute::_('index.php?option=com_mijopolls&view=poll&format=result&id='.$poll->id) . "',
			onSuccess: function(response, responseXML){
				// get the XML nodes
				var root    = responseXML.documentElement;
				var poltitle = root.getElementsByTagName('poltitle');
				var options = root.getElementsByTagName('option');
				var text    = root.getElementsByTagName('text');
				var voters  = root.getElementsByTagName('voters');
				
				$('submit_vote_".$poll->id."').setStyle('display','none');
				$('viewresults".$poll->id."').setStyle('display','none');
				$('backbtn".$poll->id."').setStyle('display','block');
				
				// update the page element
				var slide1 = new Fx.Slide('pollform".$poll->id."').hide();
				var slide2 = new Fx.Slide('pollresults".$poll->id."').hide();
					try{
				$('pollresults".$poll->id."').innerHTML = '".$updateValue."';
					}
catch(err)
  {
  //Handle errors here
   //alert('chheena');
  }
				slide2.slideIn();
			},
			onFailure: function (e) {
				alert('d');
			}
		});
		$('viewresults".$poll->id."').addEvent('click', function(e) {
			new Event(e).stop();
			req.send();
		});
		$('backbtn".$poll->id."').addEvent('click', function(e){
			new Event(e).stop();
			$('submit_vote_".$poll->id."').setStyle('display','block');
			$('viewresults".$poll->id."').setStyle('display','block');
			$('backbtn".$poll->id."').setStyle('display','none');
			
			var slide1 = new Fx.Slide('pollform".$poll->id."').hide();
			var slide2 = new Fx.Slide('pollresults".$poll->id."').hide();
			slide1.slideIn();
		});
		$('poll_vote_".$poll->id."').addEvent('submit', function(e) {
				// Prevent the submit event
			    new Event(e).stop();

				var options = $('poll_vote_".$poll->id."').getElements('input[name=voteid]');

				var nothing_selected = 1;
				
				options.each(function(item, index){
					if(item.checked==1) {
						nothing_selected = 0;
					}
				});
				
				if (nothing_selected) {
					alert('".JText::_('COM_MIJOPOLLS_SELECT_ERROR')."');
					return false;
				}
				else {
					$('submit_vote_".$poll->id."').disabled = 1;
					$('poll_loading_".$poll->id."').setStyle('display', '');
					
					// Update the page
					this.set('send', {
					onComplete: function(response, responseXML)
					{			
						// get the XML nodes
						var root    = responseXML.documentElement;		
						if (!root) {
							alert('Unable to find the poll. Please try again.');
							window.location = '".JURI::base()."';
						} else {
							var poltitle = root.getElementsByTagName('poltitle');
							var options = root.getElementsByTagName('option');
							var text    = root.getElementsByTagName('text');
							var voters  = root.getElementsByTagName('voters');	
							
							// update the page element
							var slide3 = new Fx.Slide('pollform".$poll->id."').hide();
							$('pollform".$poll->id."').innerHTML = '".$updateValue."';
							slide3.slideIn();
							
							$('submit_vote_".$poll->id."').setStyle('display','none');
							$('viewresults".$poll->id."').setStyle('display','none');
							$('backbtn".$poll->id."').setStyle('display','none');
							$('poll_loading_".$poll->id."').setStyle('display','none');
						}
					},
					onFailure: function (e) {
						alert('function failed.');
					}
					}).send();
				}
        });  
    });
	
	/* ]]> */";

$document->addScriptDeclaration($js);

}
//If user has voted 
	} else {
    	if ($tag == 'ar-AA') :
			$poll->title = $poll->title_ar;
		endif;
		?>
        <div class="pollresults2">
        <h4><?php echo $poll->title; ?></h4>
	<?php
		foreach ($results as $row) :
			$percent = ($row->votes)? round((100*$row->hits)/$row->votes, 1):0;
			$width = ($percent)? $percent:2; 
			if($params->get('only_one_color')) 
				$background_color = $params->get('poll_bars_color');
			else 
				$background_color = $row->color; ?>
    <div>
      <div style="padding:3px;"><?php echo $row->text." - ".$percent; ?>%</div>
      <div style="height:10px; padding:1px; border:1px solid #<?php echo $params->get('poll_bars_border_color'); ?>;">
        <div style="width: <?php echo $width; ?>%; height:10px;background:#<?php echo $background_color; ?>;"></div>
      </div>
    </div>
    <?php  endforeach;
			if($params->get('show_total')) 
				echo "<br /><b>".JText::_('MOD_MIJOPOLLS_TOTAL_VOTES')."</b>: ".$row->votes;
			
			if($params->get('show_msg')) : 
				echo '<div id="mod_poll_messages_'.$poll->id.'" style="margin:5px;">'.JText::_($msg);
				if($params->get('show_detailed_msg')) { 
					echo " ".$details;
				}
				echo '</div>';
			endif; ?>
 	<div class="view_all" style="text-align:center; padding-top:15px;"><a class="poll_result_link" id="poll_result_link" href="<?php print JRoute::_('index.php?option=com_mijopolls&view=polls&Itemid=129'); ?>"><?php print JText::_('MOD_MIJOPOLLS_VIEW_DETAILS'); ?></a></div>
    </div>
 <?php } ?>
    <!-- End of #polldiv -->
  </div>
</div>
