<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_login
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
?>
<?php if ($type == 'logout') : ?>
	<?php if ($params->get('greeting')) : ?>
    <?php
	$username = $user->get('name');
    $length = 12;
	if (mb_strlen($username) > $length) :
		$username = mb_substr($username, 0, $length-3) . '...';
	endif;
	?>
        <div class="login-greeting">
        	<p><?php print JText::_('MOD_LOGIN_HINAME'); ?> <a href="<?php print JRoute::_('index.php?option=com_eposts&view=myhomepage&Itemid=284'); ?>"><span><?php print $username; ?></span></a></p>
        </div>
    <?php endif; ?>
<?php else : ?>
        <div class="login-greeting">
        	<p><?php print JText::_('MOD_LOGIN_HINAME'); ?> <span>Guest</span></p>
        </div>
<?php endif; ?>