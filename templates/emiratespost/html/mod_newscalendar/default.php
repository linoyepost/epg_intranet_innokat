<?php
/*------------------------------------------------------------------------
# mod_newscalendar - News Calendar
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2010 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die;

$language = &JFactory::getLanguage();
$tag = $language->get('tag');

$doc = JFactory::getDocument();
if (file_exists(JPATH_BASE.DS.'modules' . DS . 'mod_newscalendar' . DS . 'css' . DS . $params->get('stylesheet'))) {
	$stylesheet = $params->get('stylesheet', 'blue-arrows.css');
} else {
	$stylesheet = 'blue-arrows.css';
}
$doc->addStyleSheet('modules/mod_newscalendar/css/' . $stylesheet);
$script = "var sfolder = '" . JURI::base(true) . "';";
$doc->addScriptDeclaration($script);
$doc->addScript('modules/mod_newscalendar/assets/newscalendar.js');
if (!$params->get('mid')) { 
	$mid = $module->id;
} else {
	$mid = $params->get('mid');
}
?>
<?php if ($cal->ajaxed == 0) : ?>
<div class="newscalendar" id="newscalendar<?php echo $mid; ?>"><?php endif; ?>
    <input type="text" name="event_calnd" class="event_calnd" value="<?php print date('F d, Y'); ?>" readonly="readonly"  /><a href="index.php?option=com_eposts&view=mainlistevents&page=home&Itemid=401" class="view_all"><?php print JText::_('MOD_NEWSCALENDAR_VIEWALLEVNTS'); ?></a>
    <div class="clear"></div>
    <?php if ($tag != 'ar-AA') : ?>
    <div class="cal_tabs tab_greg">
        <a href="javascript:void(0)" onClick="updateNewsCalendar(0,<?php echo $cal->curmonth; ?>,<?php echo $cal->curyear; ?>,<?php echo $mid; ?>)" class="back-mth">showGarison</a>
        <a href="javascript:void(0)" onClick="updateNewsCalendar(1,<?php echo $cal->curmonth; ?>,<?php echo $cal->curyear; ?>,<?php echo $mid; ?>)" class="back-mth">showHijri</a>
    </div>
    <?php endif; ?>
    <div class="calendar_area">
    	<div class="cal_top">
        	<div id="monthyear_<?php echo $mid; ?>"><span class="monthname"><?php echo $cal->monthname; ?></span> <span class="yearname"><?php echo ($params->get('subyearname')?substr($cal->curyear,2):$cal->curyear); ?></span></div>
            <a href="javascript:void(0)" onClick="updateNewsCalendar(0,<?php echo ($cal->curmonth!=1?$cal->curmonth-1:12); ?>,<?php echo ($cal->curmonth!=1?$cal->curyear:$cal->curyear-1); ?>,<?php echo $mid; ?>)" class="back"></a>
            <a href="javascript:void(0)" onClick="updateNewsCalendar(0,<?php echo ($cal->curmonth!=12?$cal->curmonth+1:1); ?>,<?php echo ($cal->curmonth!=12?$cal->curyear:$cal->curyear+1); ?>,<?php echo $mid; ?>)" class="next"></a>
        </div>
        <table class="body_days" cellspacing="0" cellpadding="0">
                <tr> 
                <?php 
                for($i = $params->get('firstday'); $i <= $params->get('firstday')+6; $i++) : 
              ?>    <td class="dayname dayweek<?php echo (($i>6)?($i-6):($i+1)); ?>" align="center"><?php echo modNewsCalendarHelper::encode('MOD_NEWSCALENDAR_DAYNAME_' . $params->get('subdayname') . '_' . (($i>6)?($i-6):($i+1)),$params->get('encode'),$cal->ajaxed); ?></td>		
                <?php endfor; ?></tr>
            <?php 
                $fday=$params->get('firstday');
                for($i = 0; $i < count($cal->weeks); $i++)
                { 
                ?><tr class="week<?php echo $i+1; ?>">
                    <?php
                    for($j=0; $j < 7; $j++)	
                    {
                        if(!empty($cal->weeks[$i][$j])) 
                        {
                            $day = explode(' ',$cal->weeks[$i][$j]);
                            $class = 'nc-day';
							$class2 = '';
                            $ul = '';				
                            if ( $day[1] ) { 
                                $class .= " outofmonth";
                                $class2 .= " td_outofmonth";
                            } else {
                                $class .= " weekday".((($j+$fday)>6)?(($j+$fday)-6):($j+$fday+1));	
                                if ($day[0]==date('j') && $cal->curmonth==date('m')) {
                                    $class .= " today";	
									$class2 .= " td_today";
                                }
                            }
                            if (isset($cal->items[$day[0]]) && !$day[1]) {
                                $class .= " hasitems";	
								$class2 .= " td_hasitems";
                            }
                            ?><td class="<?php echo $class2; ?>">
                            <span class="<?php echo $class; ?>">
                            <?php if (isset($cal->items[$day[0]]) && !$day[1]) : ?>
                            <a href="index.php?option=com_eposts&view=mainlistevents&page=home&Itemid=401&date=<?php print $cal->curyear.'-'.$cal->curmonth.'-'.$day[0]; ?>">
                            <?php endif; ?>
							<?php 
                            //following if is just to test
                            //if(isset($cal->hj_dates[$day[0]][0]) && !$day[1]){
                                echo $day[0];
                            //}
                                if (isset($cal->items[$day[0]]) && !$day[1]) { ?>
                                
                                <ul class="nc-items"><?php
                                foreach ($cal->items[$day[0]] as $item) {
									if ($tag == 'ar-AA') :
										$item['events_title'] = $item['events_title_ar'];
									endif;
                                ?> 
                                    <li><a href="index.php?option=com_eposts&view=mainshowevents&page=home&cirid=<?php print $item['id']; ?>&Itemid=401"><?php echo $item['events_title']; ?></a></li><?php }
                                ?></ul>
                            <?php } 
                           ?>
                            <?php if (isset($cal->items[$day[0]]) && !$day[1]) : ?>
                           </a>
                            <?php endif; ?>
                            </span>
                           </td>
                        <?php } else { ?>
                        <td></td><?php
                        }
                    } ?>
                    </tr>
            <?php } ?>
        </table>
    </div>
    <?php if ($tag == 'ar-AA') : ?>
    <div class="cal_tabs tab_greg">
        <a href="javascript:void(0)" onClick="updateNewsCalendar(0,<?php echo $cal->curmonth; ?>,<?php echo $cal->curyear; ?>,<?php echo $mid; ?>)" class="back-mth">showGarison</a>
        <a href="javascript:void(0)" onClick="updateNewsCalendar(1,<?php echo $cal->curmonth; ?>,<?php echo $cal->curyear; ?>,<?php echo $mid; ?>)" class="back-mth">showHijri</a>
    </div>
    <?php endif; ?>
	<?php if ($cal->ajaxed == 0) : ?>
    <div class="clear"></div>
</div><?php endif; ?>