<?php
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;


?>
<?php
defined('_JEXEC') or die;
jimport('joomla.application.module.helper');
jimport( 'joomla.html.parameter' );
JRequest::setVar('task', '');
  $task = '';
$config =&JFactory::getConfig();
                $filepath = $config->getValue( 'config.file_path' );
          $reverse_filepath_storage = $config->getValue( 'config.reverse_filepath_storage' );

$deptid = JRequest::getCmd('deptid');

$_SESSION['deptid'] = !($deptid) ? $_SESSION['deptid'] : $deptid;

$language = &JFactory::getLanguage();
$tag = $language->get('tag');

$document = &JFactory::getDocument();
$database = &JFactory::getDBO();

JHTML::_("behavior.mootools");
$document->addScript($this->baseurl . '/templates/' . $this->template . '/javascript/tabPane.js');
if ($tag == 'ar-AA') :
  $document->addScript($this->baseurl . '/modules/mod_anews/js/slider_ar.js');
else :
  $document->addScript($this->baseurl . '/modules/mod_anews/js/slider.js');
endif;
$app = JFactory::getApplication();
$menu = $app->getMenu();

$logo01    = $this->params->get('logo01');
$logo02    = $this->params->get('logo02');
$sitemap  = $this->params->get('sitemap');

//$where = ' WHERE (publish = "1" AND newhires_access = "1")';
$where = ' WHERE (publish = "1" AND newhires_access = "1" AND expirydate > "' . date('Y-m-d', time()) . '") ';
$query = "SELECT * FROM #__eposts_newhires
      $where
      ORDER BY publishedon ASC
      LIMIT 5";
                              
$database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
$rows = $database->loadObjectList();

$newhire_js = "";

if ($rows) : 
  $newhire_js = "
        var slider01 = new anewsSlide({
          box: $('newhire_bx01'),
          items: $$('#newhire_bx01 .newhire_item'),
          autoPlay: 10000,
          interval: 10000,
          size: 464,
          handles: $$('#handles01 span'),
          onWalk: function(currentItem,currentHandle){
            this.handles.removeClass('active');
            currentHandle.addClass('active');
          }
        });
  ";
endif;

if ($menu->getActive() == $menu->getDefault()) :
  $document->addScriptDeclaration("
    document.addEvent('domready', function(){
      var tabPane01 = new TabPane('vcp_tabs');
      var tabPane02 = new TabPane('ect_tabs');
      var tabPane03 = new TabPane('tabsbtm_cn');
      var tabPane04 = new TabPane('qsp_cn');
      " . $newhire_js . "
    });
  ");
endif;
/*
$connected = @fsockopen("www.google.com", 80); //website and port
    if ($connected){
        $is_conn = true; //action when connected
        fclose($connected);
    }else{
        $is_conn = false; //action in connection failure
    }*/  
  $is_conn = true;
?>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<html lang="<?php print $this->language; ?>" dir="<?php print $this->direction; ?>" >
<head>
<jdoc:include type="head" />
<link rel="stylesheet" href="<?php print $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php print $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php print $this->baseurl ?>/templates/<?php print $this->template; ?>/css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php print $this->baseurl ?>/templates/<?php print $this->template; ?>/css/template-<?php print $this->direction; ?>.css" type="text/css" />
<?php if ($menu->getActive() == $menu->getDefault()) : ?>
<meta charset="utf-8">
<meta http-equiv="refresh" content="900">
<?php endif; ?>
<!--[if lte IE 7]>
<link rel="stylesheet" href="<?php print $this->baseurl ?>/templates/<?php print $this->template; ?>/css/style_ie.css" type="text/css" />
<![endif]-->
</head>

<body>

  <!--Main Area -->
    <div class="main_area">
      <?php if($this->countModules('emiratespost-toolbar')) : ?>
      <!--Toolbar Area -->
      <div class="toolbar_area">
          <jdoc:include type="modules" name="emiratespost-toolbar" style="xhtml" />
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <!--/Toolbar Area -->
        <?php endif; ?>
        <!--Header Area -->
        <div class="header_area">
          <div class="logo_text"><a href="<?php print $this->baseurl; ?>/"><img src="<?php print ($logo01 != '') ? $logo01 : $this->baseurl . '/templates/' . $this->template . '/images/logo_text.png'; ?>" /></a></div>
            <div class="logo_img"><a href="<?php print $tag == 'ar-AA' ? 'http://www.government.ae/web/guest/home/ar' : 'http://www.government.ae/web/guest/home/en'; ?>" target="_blank"><img src="<?php print ($logo02 != '') ? $logo02 : $this->baseurl . '/templates/' . $this->template . '/images/logo_image.png'; ?>" /></a></div>
            <div class="clear"></div>
        </div>
        <!--/Header Area -->
        <!--TopMenu Area -->
        <div class="topmenu_area">
            <div class="ancmnt_area">
              <div class="anc_mid">
                  <div class="anc_left">
                      <div class="anc_right">
                          <?php if ($menu->getActive() == $menu->getDefault()) : ?>
                          <?php if($this->countModules('emiratespost-announcement')) : ?>
                          <div class="anc_iner_mid">
                              <div class="anc_iner_left">
                                  <div class="anc_iner_right">
                                      <jdoc:include type="modules" name="emiratespost-announcement" style="xhtml" />
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <?php endif; ?>
                            <?php if ($menu->getActive() != $menu->getDefault()) : ?>
                <?php if($this->countModules('emiratespost-breadcrumb')) : ?>
                                <div class="breadcrumbs">
                                    <div class="anc_iner_mid">
                                        <div class="anc_iner_left">
                                            <div class="anc_iner_right">
                                        <jdoc:include type="modules" name="emiratespost-breadcrumb" style="xhtml" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
          <?php if($this->countModules('emiratespost-topmenu')) : ?>
          <div class="menu_area">
              <jdoc:include type="modules" name="emiratespost-topmenu" style="xhtml" />
                <div class="clear"></div>
            </div>
            <?php endif; ?>
            <div class="search_area">
              <?php if($this->countModules('emiratespost-search')) : ?>
              <jdoc:include type="modules" name="emiratespost-search" style="xhtml" />
                <div class="clear"></div>
              <?php endif; ?>
            </div>
            <div class="home_links">
                <ul>
                    <li class="home"><a href="<?php print JURI::base(); ?>"><img src="<?php print $this->baseurl . '/templates/' . $this->template; ?>/images/h_home.png" /></a></li>
                    <?php
          if ($this->params->get('sitemap') != 0) :
                    $link = $menu->getItem($this->params->get('sitemap'));
          ?>
                    <li class="sitemap"><a target="_blank" href="<?php print 'http://www.google.com/';//print $link->link . '&Itemid=' . $link->id; ?>"><img src="<?php print $this->baseurl . '/templates/' . $this->template; ?>/images/h_google.png" /></a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
        <!--/TopMenu Area -->
        <!--Body Area -->
        <div class="body_area">
          <?php if ($menu->getActive() == $menu->getDefault()) : ?>
        <?php if($this->countModules('emiratespost-news')) : ?>
                <!--News Area -->
                <div class="news_arae">
                    <div class="news_left">
                        <div class="news_right">
                            <div class="news_shadow">
                                <div class="news_wrpr">
                                    <h3><?php print JText::_('EMIRATES_NEWS'); ?> <span><?php print JText::_('EMIRATES_EPG_NEWS'); ?></span></h3>
                                    <jdoc:include type="modules" name="emiratespost-news" style="xhtml" /> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/News Area -->
                <?php endif; ?>
                <!--Our Vision -->
               <div class="vision_area">
                 <div class="moduletable">
                <div class="container2" style="padding-top: 15px;"><a href="#" onClick="javascript:window.open('https://www.mygov.ae/','pop1Win','toolbar=no scrollbars=yes, width=1004,height=800')"><span ><img src="<?php print JURI::base(); ?>templates/<?php print $app->getTemplate(); ?>/images/logo-mygov.gif" /></span></a></div>
                 </div>
              </div>               
                <!--/Our Vision -->
                
                
                
                <div class="clear"></div>
                <!--Our Vision / Circulars / Policies -->
                <div class="vcp_shadow">
                    <div class="vcp_area">
                        <div class="vcp_top">
                            <div class="vcp_btm" id="vcp_tabs">
                                <div class="tabs">
                                    <ul class="tabs">
                                        <?php if($this->countModules('emiratespost-visioncn')) : ?>
                                        <li class="tab vision"><span class="nm">01</span><?php print JText::_('EMIRATES_OURVISION'); ?> <span><?php print JText::_('EMIRATES_VISION_N_MISSION_STATEMENT'); ?></span></li>
                                        <?php endif; ?>
                                        <?php if($this->countModules('emiratespost-crclrscn')) : ?>
                                        <li class="tab crclrs"><span class="nm">02</span><?php print JText::_('EMIRATES_CIRCULARS'); ?> <span><?php print JText::_('EMIRATES_MUST_READ_ITEMS'); ?></span></li>
                                        <?php endif; ?>
                                        <?php if($this->countModules('emiratespost-plciescn')) : ?>
                                        <li class="tab plcies"><span class="nm">03</span><?php print JText::_('EMIRATES_POLICIES'); ?> <span><?php print JText::_('EMIRATES_EMPLOYEE_N_OFFICE_POLICY'); ?></span></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="items">
                                    <?php if($this->countModules('emiratespost-visioncn')) : ?>
                                    <!--<div class="content visioncn"><jdoc:include type="modules" name="emiratespost-visioncn" style="xhtml" /></div>-->
<div class="content visioncn">
<div class="moduletable">
<div class="anews_cn" style="width:px;">
<div class="anews_wrpr" id="anews_bx97">
<div class="anews_item item-0" style="width:px;">   
<?php
  $where = ' WHERE (1 = 1 )';
                            
                            $query = "SELECT * FROM #__eposts_visions
                                      $where
                                      ORDER BY id DESC
                                      LIMIT 1";
                                                                                        
                            $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
                            $row = $database->loadObject();
                              $title = $row->visions_name;
                      if ($tag == 'ar-AA') :
                        $title = $row->visions_name_ar;
                      endif;
                      
                        $descp = $row->visions_longdescription;
                        $descp2 = $row->visions_mission;
                      if ($tag == 'ar-AA') :
                        $descp = $row->visions_description_ar;
                        $descp2 = $row->visions_mission_ar;
                      endif;
                      $title = strip_tags($title);
                    ?>

                      <?php
                      $length = 200;
                      $descp = strip_tags($descp);
                      $descp2 = strip_tags($descp2);
                      if (mb_strlen($descp) > $length) :
                         $descp = mb_substr($descp, 0, $length) . '...';
                      endif;  
                      if (mb_strlen($descp2) > $length) :
                         $descp2 = mb_substr($descp2, 0, $length) . '...';
                      endif;  
                      $link = JRoute::_('index.php?option=com_eposts&view=mainshowvisions&page=home&cirid=' .$row->id . '&Itemid=118');               
                       ?>
<a href="<?php echo $link;?>"><?php print JText::_('EMIRATES_VISSION_01'); ?></a>                                

        <p><?php print strip_tags($descp);?></p>
        <br><br>
        <a href="<?php echo $link;?>"><?php print JText::_('EMIRATES_VISSION_02'); ?></a>
        <p><?php print strip_tags($descp2);?></p>

</div>                    </div>
        <div class="controlls">
            <div class="container"><a class="readmore" href="<?php print JRoute::_('index.php?option=com_eposts&view=mainshowvisions&page=home&cirid=' . $row->id . '&Itemid=118');?>"><span><?php print JText::_('EMIRATES_VIEWALL'); ?></span></a></div>
            <div class="clear"></div>
        </div>
</div>
<div class="controlls">
<div class="clear"></div>
</div>    </div>
</div>
                                    <?php endif; ?>
                                    <div class="content crclrscn">
                    <?php
                                            $where = ' WHERE (publish = "1" AND circulars_access = "1")';
                                            
                                            $query = "SELECT * FROM #__eposts_circulars
                                                      $where
                                                      ORDER BY publishedon DESC
                                                      LIMIT 7";
                                                                                                        
                                            $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
                      $circulars_js = "
                        document.addEvent('domready', function(){
                            var slider01 = new anewsSlide({
                              box: $('newhire_bx03'),
                              items: $$('#newhire_bx03 .newhire_item'),
                              autoPlay: 10000,
                              interval: 10000,
                              size: 464,
                              handles: $$('#handles03 span'),
                              onWalk: function(currentItem,currentHandle){
                                this.handles.removeClass('active');
                                currentHandle.addClass('active');
                              }
                            });
                        });
                      ";
                                            $rows = $database->loadObjectList();
                      if (count($rows) > 1) :
                        $document->addScriptDeclaration($circulars_js);
                      endif;
                                        ?>
                                        <div class="moduletable" style="color:#666666;">
                                        <h3><?php print JText::_('EMIRATES_CIRCULARS'); ?></h3>
                                            <div class="handles" id="handles03">
                                              <?php for($i=0, $n=count( $rows ); $i < $n; $i++) { ?>
                                              <span><?php print $i; ?></span>
                                                <?php } ?>
                                            </div>
                                            <div class="newhire_cn">
                                              <div class="newhire_wrpr" id="newhire_bx03">
                                              <?php
                                                $k = 0;
                                                for($i=0, $n=count( $rows ); $i < $n; $i++) {
                                                    $row = &$rows[$i];
                                                    $link = JRoute::_('index.php?option=com_eposts&view=mainshowcirculars&deptid='.$row->circulars_department.'&page=home&cirid=' .$row->id . '&Itemid=398');
                                                    
                                                    $title = $row->circulars_name;
                                                    $descp = $row->circulars_description;
                                                    if ($tag == 'ar-AA') :
                                                        $title = $row->circulars_name_ar;
                            $descp = $row->circulars_description_ar;
                                                    endif;
                          
                          $title = strip_tags($title);
                                                    
                                                    $length = 44;
                                                    if (mb_strlen($title) > $length) :
                                                        $title = mb_substr($title, 0, $length) . '...';
                                                    endif; 
                          $length = 100;
                          $descp = strip_tags($descp);
                          if (mb_strlen($descp) > $length) :
                            $descp = mb_substr($descp, 0, $length) . '...';
                          endif;
                                                    
                                                ?>
                                                <div class="newhire_item item">
                                                  <div class="inner">
                                                      <h4><?php print $title; ?></h4>
                                                      <p><?php print $descp; ?></p><br>
                                                      <p><?php echo JText::_('EMIRATES_CIRCULAR_DATE'); ?>: <?php echo JText::sprintf('%s', JHtml::_('date', $row->circulars_date, JText::_('DATE_FORMAT_LC5'))); ?></p>
                                                      <p>
                                                        <?php echo JText::_('EMIRATES_CIRCULAR_VIEW'); ?>: 
                                                        <?php if ($tag == 'ar-AA') : ?>
                                                            <a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->circulars_document_ar); ?>"><?php print JText::_('EMIRATES_CIRCULAR_DOWNLOAD'); ?></a>
                                                        <?php else : ?>
                                                            <a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->circulars_document); ?>"><?php print JText::_('EMIRATES_CIRCULAR_DOWNLOAD'); ?></a>
                                                        <?php endif; ?>
                                                      </p>
                                                      <div class="clear"></div>
                                                  </div>
                                                </div>
                                                <?php $k = 1 - $k;  } ?>
                                              </div>
                                            </div>
                                            <div class="controlls">
                                              <div class="container"><a class="readmore" href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistcirculars&page=home&Itemid=398'); ?>"><span><?php print JText::_('EMIRATES_VIEWALL'); ?></span></a></div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($this->countModules('emiratespost-plciescn')) : ?>
<!--<div class="content plciescn"><jdoc:include type="modules" name="emiratespost-plciescn" style="xhtml" /></div>-->
<div class="content plciescn">    <div class="moduletable">
          <div class="anews_cn" style="width:px;">
    <div class="anews_wrpr" id="anews_bx95">
   <ul>
     <?php
     $where = ' WHERE (publish = 1 && policies_access = 1 )';
        //$where = ' WHERE (1 = 1 )';
     $query = "SELECT * FROM #__eposts_policies
          $where
          ORDER BY id DESC
          LIMIT 5";
                                    
       $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
       $items = $database->loadObjectList();
           foreach ($items as $key => $item) :  
    $anLinks = JRoute::_('index.php?option=com_eposts&view=mainshowpolicies&page=home&cirid=' .$item->id . '&Itemid=399');
        $title = $item->policies_name;
        if ($tag == 'ar-AA') :
          $title = $item->policies_name_ar;
        endif;
        $title = strip_tags($title);
        $length = 44;
        if (mb_strlen($title) > $length) :
           $title = strip_tags($title);
           $title = mb_substr($title, 0, $length) . '...';
        endif;  
      
    ?>
        <div class="title"><h4></h4></div>
        <li>                
           <a href="<?php print $anLinks;?>"><?php print $title;?></a>                                
        </li>
<?php endforeach; ?>
     </ul>    
      </div>
</div>
<div class="controlls">
              <div class="container"><a href="<?php echo JRoute::_('index.php?option=com_eposts&view=mainlistpolicies&page=home&Itemid=115');?>" class="readmore"><span><?php print JText::_('EMIRATES_VIEWALL'); ?></span></a></div>
        <div class="container2"><a href="#" onClick="javascript:window.open('/HRManual_22-04-2014.pdf','pop1Win','toolbar=no scrollbars=yes, width=800,height=700')" class="readmore"><span><?php print JText::_('EMIRATES_EPG'); ?></span></a></div>
        <div class="clear"></div>
</div>    </div>
</div>
                                    <?php endif; ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/Our Vision / Circulars / Policies -->
                <!--Events Calander / Tasawaq -->
                <div class="ect_area" id="ect_tabs">
                    <div class="ect_wrpr">
                        <div class="ect_top">
                            <div class="ect_btm">
                                <?php if($this->countModules('emiratespost-evclndr')) : ?>
                                <div class="content"><jdoc:include type="modules" name="emiratespost-evclndr" style="xhtml" /></div>
                                <?php endif; ?>
                                <div class="content">
                  <?php
                                        $where = ' WHERE (publish = "1")';
                                        
                                        $query = "SELECT * FROM #__eposts_tasawaqs
                                                  $where
                                                  ORDER BY publishedon DESC
                                                  LIMIT 5";
                                                                                                    
                                        $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
                                        $rows = $database->loadObjectList();
                                    ?>
                                    <div class="moduletable">
                                        <ul>
                                            <?php
                                            $k = 0;
                                            for($i=0, $n=count( $rows ); $i < $n; $i++) {
                                                $row = &$rows[$i];
                                                $link = JRoute::_('index.php?option=com_eposts&view=mainshowtasawaqs&page=home&cirid=' .$row->id . '&Itemid=120');
                                                
                                                $title = $row->tasawaqs_name;
                                                
                                                if ($tag == 'ar-AA') :
                                                    $title = $row->tasawaqs_name_ar;
                                                endif;
                                                
                        $title = strip_tags($title);
                        
                                                $length = 25;
                                                if (mb_strlen($title) > $length) :
                                                    $title = mb_substr($title, 0, $length) . '...';
                                                endif; 
                                                
                                            ?>
                                            <li><a href="<?php print $link; ?>"><?php print $title; ?></a></li>
                                            <?php $k = 1 - $k;  } ?>
                                        </ul>
                                        <div class="controlls">
                                            <div class="container"><a class="readmore" href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlisttasawaqs&page=home&Itemid=120'); ?>"><span><?php print JText::_('EMIRATES_VIEWALL'); ?></span></a></div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ect_tabs">
                        <ul class="tabs">
                            <?php if($this->countModules('emiratespost-evclndr')) : ?>
                            <li class="tab evcr"></li>
                            <?php endif; ?>
                            <li class="tab tswk"></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
                <!--/Events Calander / Tasawaq -->
                <div class="clear"></div>
                <!--Announcements / Prayer Timings / Weather / Job Board / New Hire / Picture Gallery -->
                <div class="tabsbtm_area" id="tabsbtm_cn">
                    <div class="tabsbtm_mid">
                        <div class="tabsbtm_top">
                            <div class="tabsbtm_btm">
                                <div class="tabs_area">
                                    <ul class="tabs">
                                        <?php if($this->countModules('emiratespost-ancmntcn')) : ?>
                                        <li class="tab"><?php print JText::_('EMIRATES_ANNOUNCEMENTS'); ?></li>
                                        <?php endif; ?>
                                        <li class="tab" id="pic_tab"><?php print JText::_('EMIRATES_PICTUREGLRY'); ?></li>
                                        <?php if($this->countModules('emiratespost-jobbrdcn')) : ?>
                                        <li class="tab"><?php print JText::_('EMIRATES_JOBBOARD'); ?></li>
                                        <?php endif; ?>
                                        <li class="tab"><?php print JText::_('EMIRATES_NEWHIRE'); ?></li>
                                        <?php if($this->countModules('emiratespost-weathrcn')) : ?>
                                        <li class="tab"><?php print JText::_('EMIRATES_WEATHER'); ?></li>
                                        <?php endif; ?>
                                        <?php if($this->countModules('emiratespost-prayercn')) : ?>
                                        <li class="tab"><?php print JText::_('EMIRATES_PRAYERS'); ?></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="cntnt_area">
                                    <?php if($this->countModules('emiratespost-ancmntcn')) : ?>
                                    <!--<div class="content"><jdoc:include type="modules" name="emiratespost-ancmntcn" style="xhtml" /></div>-->
                                   <div class="content">
                                    <div class="moduletable">
          <div class="anews_cn" style="width:px;">
    <div class="anews_wrpr" id="anews_bx101">
   <ul>
     <?php
     $where = ' WHERE (publish = 1 && announcements_access = 1 && announcements_expirydate >= "' . date('Y-m-d H:i:s') . '")';
        //$where = ' WHERE (1 = 1 )';
     $query = "SELECT * FROM #__eposts_announcements
          $where
          ORDER BY id DESC
          LIMIT 5";
                                    
       $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
       $items = $database->loadObjectList();
           foreach ($items as $key => $item) :  
    $anLinks = JRoute::_('index.php?option=com_eposts&view=mainshowannouncements&page=home&cirid=' .$item->id . '&Itemid=397');
        $title = $item->announcements_name;
        if ($tag == 'ar-AA') :
          $title = $item->announcements_name_ar;
        endif;
        $title = strip_tags($title);
        $length = 65;
        if (mb_strlen($title) > $length) :
           $title = strip_tags($title);
           $title = mb_substr($title, 0, $length) . '...';
        endif;  
      
    ?>
        <div class="title"><h4></h4></div>
        <li>                
           <a href="<?php print $anLinks;?>"><?php print $title;?></a>
   
        </li>
<?php endforeach; ?>
     </ul>    
      </div>
</div>
<div class="controlls">
              <div class="container"><a href="<?php echo JRoute::_('index.php?option=com_eposts&view=mainlistannouncements&page=home&Itemid=397');?>" class="readmore"><span><?php print JText::_('EMIRATES_VIEWALL'); ?></span></a></div>
        <div class="clear"></div>
</div>    </div>
</div>

                                    <?php endif; ?>
                                    <style>
.ImageRotator{
    width:385px;
    height:160px;
    /*border:1px solid black;*/
    clear: both;
    overflow: hidden;
    position:absolute;
    display:inline;
}

.ImageRotator img.first{
width:100px;
height:75px;
display: inline;
left: 30px;
top: 45px;
visibility: visible;
position: absolute;
z-index: 2;
opacity: 0.4;
}
.ImageRotator img.second{
    width:120px;
    height:120px;

display: inline;
left: 60px;
top: 20px;
visibility: visible;
position: absolute;
z-index: 4;
opacity: 0.8;
}
.ImageRotator img.third{
    width:160px;
    height:160px;

display: inline;
left: 115px;
top: 0px;
visibility: visible;
position: absolute;
z-index: 6;
opacity: 1;
}
.ImageRotator img.forth{
    width:120px;
    height:120px;
display: inline;
left: 200px;
top: 20px;
visibility: visible;
position: absolute;
z-index: 4;
opacity: 0.8;
}
.ImageRotator img.fifth{
    width:100px;
    height:75px;
display: inline;
left: 250px;
top: 45px;
visibility: visible;
position: absolute;
z-index: 2;
opacity: 0.4;
}
.ImageRotator img.pushClass{
display:none;
}
</style>
<?php
//var delaytoscrol = 2000; 2000 = 2second
$document->addScriptDeclaration("
        var selectedImage = 0;
        var totalImages = 0;
        var delaytoscrol = 2000;
        function runAcroSlider(sliderClass){
             totalImages = document.getElementById(sliderClass).getElementsByTagName('a').length;
             if(totalImages > 0){
                 if(totalImages > 1){
                     var seleage = Math.round(totalImages/2);
                 }   
                 selectImage(seleage);             
             }
               return false;
        }
       
        function moverSliNext(){
            selectImage(selectedImage+1);
        return false;
        }
        function moverSliPre(){
            selectImage(selectedImage-1);
        return false;
        }
       
        function displySliPic(myElmt, myVl){
            if(document.getElementById('sl-'+myElmt)){
                document.getElementById('sl-'+myElmt).className = myVl;
              }
              return false;           
        }
       
        function selectImage(selectMyImage){
           
              if(selectMyImage == selectedImage){
                  return true;
              }
              if(selectMyImage >= 0 && selectMyImage <= totalImages){
                selectedImage = selectMyImage;
              }else{
                return false; 
              }
             
              displySliPic(selectedImage, 'third');
              displySliPic(selectedImage-1, 'second');
              displySliPic(selectedImage-2, 'first');
              displySliPic(selectedImage+1, 'forth');
              displySliPic(selectedImage+2, 'fifth');
              if(selectedImage < 1){
                  document.getElementById('prev').style.display='none';
              }else{
                 document.getElementById('prev').style.display='block';
              }
             
              if(selectedImage+1 >= totalImages ){
                  document.getElementById('nextv').style.display='none';
              }else{
                 document.getElementById('nextv').style.display='block';
              }
             
              return false;
            }
       
        window.addEvent('domready',function(){
                   runAcroSlider('ImageRotator');
                });
               
               
    ");
?>    
                                    <div class="content">
                                        <div class="moduletable newhire">

<table width="100%" cellpadding="0" cellpadding="0">
<tr>
<td valign="middle">
  <?php if ($tag == 'ar-AA') : ?>
      <a  id='nextv' href="#"  onClick="javaScript: return moverSliNext();" ><img src="<?php print ($logo02 != '') ? $logo02 : $this->baseurl . '/templates/' . $this->template . '/images/gallery_arrow_right.png'; ?>" /></a>
    <?php else : ?>
      <a id='prev' href="#" onClick="javaScript: return moverSliPre();"><img src="<?php print ($logo02 != '') ? $logo02 : $this->baseurl . '/templates/' . $this->template . '/images/gallery_arrow_left.png'; ?>" /></a>
    <?php endif; ?>
</td>
<td width="385px" height="160px" valign="top">
<div id="ImageRotator" class="ImageRotator">
<?php
                        $where = ' WHERE parent = "1" AND published = "1"';
                       
                        $query = "SELECT * FROM #__igallery
                                  $where
                                  ORDER BY date DESC
                                  LIMIT 10";
                       
                        $database->SetQuery($query);
                        $rows = $database->loadObjectList();
                        $k = 0;
                        for($i=0, $n=count( $rows ); $i < $n; $i++) {
                            $row = &$rows[$i];
                            $link = JRoute::_("index.php?option=com_igallery&view=category&page=home&Itemid=403&igid=".$row->id);
                           
                            $title = $row->name;
              $mImag = $row->menu_image_filename;
                           
                           if ($mImag == '') :
                                $where = 'WHERE gallery_id = "'.$row->id.'" AND published = "1"';
                           
                                $query = "SELECT * FROM #__igallery_img
                                      $where
                                      ORDER BY date ASC
                                      LIMIT 1";
                           
                                $database->SetQuery($query);
                                $row = $database->loadObject();
                              $image = $row->filename;
              else :
                $image = $mImag;
                            endif;
                            $name = substr($image, 0, strrpos($image, '.'));
                            $extnt = substr($image, strrpos($image, '.'));
              
              //$img = $filepath.'\\'.$image;
              $img =  $reverse_filepath_storage.'\\'.$image;
      $img = str_replace("\\", "/", $img);
      
      //$filetype=string;       
     //$imgbinary = fread(fopen($img, "r"), filesize($img));
      // echo $imgbinary;
     //$aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
     $aaa=$img;
                        ?>
<a href="<?php print $link; ?>" onClick="javaScript: return selectImage(<?php echo $i;?>);">
  <img id="sl-<?php echo $i;?>" src="<?php echo $aaa; ?>"  class="pushClass"/>
</a>
<?php $k = 1 - $k;  } ?>
</div>
<div class="clear"></div>   
</td>
<td valign="middle">
  <?php if ($tag == 'ar-AA') : ?>
      <a id='prev' href="#" onClick="javaScript: return moverSliPre();"><img src="<?php print ($logo02 != '') ? $logo02 : $this->baseurl . '/templates/' . $this->template . '/images/gallery_arrow_left.png'; ?>" /></a>
    <?php else : ?>
      <a  id='nextv' href="#"  onClick="javaScript: return moverSliNext();" ><img src="<?php print ($logo02 != '') ? $logo02 : $this->baseurl . '/templates/' . $this->template . '/images/gallery_arrow_right.png'; ?>" /></a>
    <?php endif; ?>
</td>
</tr>
</table> 

                                            <div class="controlls" style="position:static;">
                                                <div class="view_all">
                                                    <a href="<?php print JRoute::_('index.php?option=com_igallery&view=category&page=home&igid=1&Itemid=403'); ?>"><?php print JText::_("EPOSTS_VIEWALLALBUMS"); ?></a>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($this->countModules('emiratespost-jobbrdcn')) : ?>
                                    <div class="content newhire" style="position:static;"><jdoc:include type="modules" name="emiratespost-jobbrdcn" style="xhtml" /></div>
                                    <?php endif; ?>
                                    <div class="content">                                   
                    <?php
                                            $where = ' WHERE (publish = "1" AND newhires_access = "1" AND expirydate > "' . date('Y-m-d', time()) . '")';
                                            
                                            $query = "SELECT * FROM #__eposts_newhires
                                                      $where
                                                      ORDER BY publishedon ASC
                                                      LIMIT 5";
                                                                                                        
                                            $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
                                            $rows = $database->loadObjectList();
                                        ?>
                                        <div class="moduletable newhire">
                                            <h3><?php print JText::_('EMIRATES_NEWHIRE'); ?></h3>
                                            <div id="handles01" class="handles">
                                              <?php
                                                $k = 0;
                                                for($i=0, $n=count( $rows ); $i < $n; $i++) {
                        ?>
                                                <span><?php print $i+1; ?></span>
                                                <?php $k = 1 - $k;  } ?>
                                            </div>
                                            <div class="newhire_cn">
                                              <div id="newhire_bx01" class="newhire_wrpr">
                                                <?php
                                                $k = 0;
                                                for($i=0, $n=count( $rows ); $i < $n; $i++) {
                                                    $row = &$rows[$i];
                                                    $link = JRoute::_('index.php?option=com_eposts&view=mainshownewhires&page=home&cirid=' .$row->id . '&Itemid=254');
                                                    
                                                    $title = $row->newhires_name;
                          $descp = $row->newhires_description;
                          $desig = $row->newhires_designation;
                                                    
                                                    if ($tag == 'ar-AA') :
                                                        $title = $row->newhires_name_ar;
                            $descp = $row->newhires_description_ar;
                            $desig = $row->newhires_designation_ar;
                                                    endif;
                                                    $title = strip_tags($title);
                                                    $length = 45;
                                                    if (mb_strlen($title) > $length) :
                                                        $title = mb_substr($title, 0, $length) . '...';
                          endif;
                                                    $length2 = 200;
                          $descp = strip_tags($descp);
                                                    if (mb_strlen($descp) > $length2) :
                            $descp = mb_substr($descp, 0, $length2) . '...';
                                                    endif;
$config =&JFactory::getConfig();
 $filepath = $config->getValue( 'config.file_path' );
  $reverse_filepath_storage = $config->getValue( 'config.reverse_filepath_storage' ); 
if($row->newhires_picture){
 //$img = $filepath.'\\'.$row->newhires_picture;
 $img = $reverse_filepath_storage.'\\'.$row->newhires_picture;
      
         $img = str_replace("\\", "/", $img);
  //     echo $img;
      //$filetype=string;       
       //$imgbinary = fread(fopen($img, "r"), filesize($img));
     // echo $imgbinary;
     //$aaa= 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
     $aaa= $img;

  }
else {
$aaa= 'components/com_eposts/assets/images/icons/staff-m-pic.png';

}  
                          ?>
                                                  <div class="newhire_item item">
                                                      <div class="newhire_img"><img border="0" alt="" width="80" src="<?php echo $aaa;?>"></div>
                                                        <div class="newhire_details">
                                                            <h4><?php print $title; ?></h4>
                                                            <p><?php print $desig; ?></p>
                                                            <p><?php print JText::_('EMIRATES_MOBILE');?>: <?php print $row->newhires_mobile; ?></p>
                                                            <p><?php print JText::_('EMIRATES_EMAIL');?>: <?php print $row->newhires_email; ?></p>
                                                            <p><?php print $descp; ?></p>
                                                        </div>
                                                        <div class="clear"></div>
                                                  </div>
                                                <?php $k = 1 - $k;  } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="moduletable">
                                            <div class="controlls">
                                              <div class="container"><a class="readmore" href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistnewhires&page=home&Itemid=254'); ?>"><span><?php print JText::_('EMIRATES_VIEWALL'); ?></span></a></div>
                                              <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($this->countModules('emiratespost-weathrcn') && $is_conn ) : ?>
                                    <div class="content"><jdoc:include type="modules" name="emiratespost-weathrcn" style="xhtml" /></div>
                                    <?php endif; ?>
                                    <?php if($this->countModules('emiratespost-prayercn') && $is_conn ) : ?>
                                    <div class="content"><jdoc:include type="modules" name="emiratespost-prayercn" style="xhtml" /></div>
                                    <?php endif; ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/Announcements / Prayer Timings / Weather / Job Board / New Hire / Picture Gallery -->
                <!--Quotes / Surveys / Polls -->
                <div class="qsp_area" id="qsp_cn">
                    <div class="tabs_area">
                        <ul class="tabs">
                            <?php if($this->countModules('emiratespost-quotescn')) : ?>
                            <li class="tab quotes active"><span><?php print JText::_('EMIRATES_QUOTES'); ?></span></li>
                            <?php endif; ?>
                            <li class="tab surveys"><span><?php print JText::_('EMIRATES_SURVEYS'); ?></span></li>
                            <?php if($this->countModules('emiratespost-pollsccn')) : ?>
                            <li class="tab polls"><span><?php print JText::_('EMIRATES_POLLS'); ?></span></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="cntnt_area">
                        <div class="qsp_btmline">
                            <?php if($this->countModules('emiratespost-quotescn')) : ?>
                            <div class="content quotes"><jdoc:include type="modules" name="emiratespost-quotescn" style="xhtml" /></div>
                            <?php endif; ?>
                            <div class="content surveys">
                              <div class="moduletable">
                                <?php
                define('ARI_QUIZ_STATUS_ACTIVE', 1);
                $query = sprintf('SELECT QC.CategoryId,QC.CategoryName,Q.QuizName,Q.QuizName_ar,Q.QuizId' .
                  ' FROM #__ariquiz Q LEFT JOIN #__ariquizquizcategory QQC' .
                  '   ON Q.QuizId = QQC.QuizId' .
                  ' LEFT JOIN #__ariquizcategory QC' .
                  '   ON QC.CategoryId = QQC.CategoryId' .
                  ' WHERE Q.Status = %d AND QC.CategoryId = 1' .
                  ' ORDER BY Q.QuizId DESC LIMIT 4',
                  ARI_QUIZ_STATUS_ACTIVE);
                                              
                $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
                $rows = $database->loadObjectList();
                ?>
                                  <ul>
                                      <?php foreach($rows as $row) : ?>
                                        <?php
                                          if ($tag == 'ar-AA') :
                        $row->QuizName = $row->QuizName_ar;
                      endif;
                      
                      $row->QuizName = strip_tags($row->QuizName);
                      
                      $length = 25;
                      if (mb_strlen($row->QuizName) > $length) :
                        $row->QuizName = mb_substr($row->QuizName, 0, $length) . '...';
                      endif;
                    ?>
                                      <li><a href="<?php print JRoute::_('index.php?option=com_ariquizlite&task=quiz&quizId='. $row->QuizId .'&Itemid=297'); ?>"><?php print $row->QuizName; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                  <div class="controlls">
                                        <div class="container"><a class="readmore" href="<?php print JRoute::_('index.php?option=com_ariquizlite&view=quizzes&task=quiz_list&aricategory=1&Itemid=297'); ?>"><span><?php print JText::_('EMIRATES_VIEWALL'); ?></span></a></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <?php if($this->countModules('emiratespost-pollsccn')) : ?>
                            <div class="content polls"><jdoc:include type="modules" name="emiratespost-pollsccn" style="xhtml" /></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <!--/Quotes / Surveys / Polls -->
            <?php endif; ?>
            <div class="clear"></div>
            <?php if ($menu->getActive()->id == $menu->getDefault()->id) : ?>
            <?php else : ?>
              <div class="body_wrpr">
                  <div class="wrpr_top">
                      <h1><?php print $menu->getActive()->title; ?></h1>
                        <?php if ($tag != 'ar-AA') : ?>
              <?php if($this->countModules('emiratespost-left')) : ?>
                            <div class="left_area">
                                <jdoc:include type="modules" name="emiratespost-left" style="xhtml" />
                            </div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if($this->countModules('emiratespost-left')) : ?>
                        <div class="contents_area bodyw_left">
                        <?php else : ?>
                        <div class="contents_area">
                        <?php endif; ?>
                            <jdoc:include type="message" />
                            <jdoc:include type="component" />
                      </div>
                        <?php if ($tag == 'ar-AA') : ?>
              <?php if($this->countModules('emiratespost-left')) : ?>
                            <div class="left_area">
                                <jdoc:include type="modules" name="emiratespost-left" style="xhtml" />
                            </div>
                            <?php endif; ?>
                        <?php endif; ?>
						<!---Like Dislike Start --->
                        <?php
						if(JRequest::getCmd( 'view' )=="mainshownews" || JRequest::getCmd( 'view' )=="mainshowannouncements" || JRequest::getCmd( 'view' )=="mainshowpolicies" || JRequest::getCmd( 'view' )=="article"){
							$user			= &JFactory::getUser();
							$item_id = JRequest::getCmd( 'cirid' );
							if(JRequest::getCmd( 'view' )=="mainshownews"){
								
								$like_table = "like_news"; 
							
							}else if(JRequest::getCmd( 'view' )=="mainshowannouncements"){
								
								$like_table = "like_announcement";
							}else if(JRequest::getCmd( 'view' )=="mainshowpolicies"){
								
								$like_table = "like_policies";
							}else if(JRequest::getCmd( 'view' )=="article"){
								
								$like_table = "like_pages";
								$item_id = JRequest::getCmd( 'id' );
							}
							
															
							$where_condition = "Item_Id = " . $item_id . ""; 
									
							$Page_info = array(
								"Item_Id" =>  $item_id,
								"User_ID" => $user->id,
								"" => JRequest::getCmd( 'view' )
							); 
									//$myuserid = 4;
									$myuserid = $user->id;
							
						$query = "SELECT SUM( CASE WHEN Is_Like =1 THEN 1  ELSE 0  END ) AS Like_Count, 
						 		SUM( CASE WHEN Is_Like =2 THEN 1  ELSE 0 END )AS UnLike_Count	
								FROM #__".$like_table . " Where " . $where_condition ." ";
																	  
							$database->SetQuery( $query);
		   					$items = $database->loadObjectList();
							foreach($items as $count_likes) {
							 $likes = ($count_likes->Like_Count == null ? 0 : $count_likes->Like_Count);
							 $unlikes = ($count_likes->UnLike_Count == null ? 0 : $count_likes->UnLike_Count);
						}	
													
						$query2 = "SELECT SUM( CASE WHEN Is_Like =1 THEN 1  ELSE 0  END ) AS User_Like, 
						SUM( CASE WHEN Is_Like = 2 THEN 1  ELSE 0 END )AS User_UnLik FROM #__".$like_table ." Where ". $where_condition ." AND User_ID = " . $myuserid." ";
						$database->SetQuery($query2);
		   				$user_islike = $database->loadObjectList();
						foreach($user_islike as $item1) {
						$user_like = ($item1->User_Like == null ? 0 : $item1->User_Like);
						$user_unlike = ($item1->User_UnLik == nu ? 0 : $item1->User_UnLik);
						}	
						?>
							
						<div class="likebtn-wrapper" style="margin:5px;">
                        	<span class="likebtn-wrapper  like_btn   <?php echo ($user_like > 0 && $user_like != null) ? "like_btn_active" : "like_btn_inactive"; ?> lb-loaded lb-style-white lb-popup-position-top lb-popup-style-light"  >
                        	<span class="likebtn-button lb-like   " id="lb-like-0">
                        	<span  class="lb-a vote_btn_like " data-lb_index="0">
                        	<span class="likebtn-icon lb-like-icon">&nbsp;</span>
                        	<span class="likebtn-label lb-like-label"></span></span>
                        	<span class="lb-count like_total_counts" data-count="2" style="display: inline-block;"><?php echo $likes; ?></span></span>
                        	<span class="likebtn-button unlike_btn  lb-dislike  <?php echo ($user_unlike > 0 && $user_unlike != null) ? "unlike_btn_active" : "unlike_btn_inactive"; ?>  lb-voted" id="lb-dislike-0">
                        	<span  class="lb-a vote_btn_unlike " data-lb_index="0">
                        	<span class="likebtn-icon lb-dislike-icon">&nbsp;</span></span>
                        	<span class="lb-count unlike_total_counts lb-hidden" data-count="1" style="display: inline-block;">
							<?php echo $unlikes; ?></span>
                            </span></span>
                            <input type="hidden" value="<?php echo ($user_like > 0 && $user_like !=null  || $user_unlike > 0 && $user_like !=null) ? "1" : 0;  ?>" class="is_user_voted" name="is_user_voted"  >
                             <input type="hidden" value="<?php echo ($user_like > 0) ? "1" : 0;  ?>" class="is_user_voted_like" name="is_user_voted_like"  >
                             <input type="hidden" value="<?php echo ($user_unlike > 0) ? "1" : 0;  ?>" class="is_user_voted_unlike" name="is_user_voted_unlike"  ><?php 
							 
							 /*Insert For Site analytic */
             $item_id = JRequest::getCmd( 'cirid' );
			 
			 if(JRequest::getCmd( 'view' )=="article"){
				
				$item_id = JRequest::getCmd( 'id' );
			}else if (JRequest::getCmd( 'view' )=="category"){
				
				$item_id = JRequest::getCmd( 'igid' );
			}
			 
			$database = &JFactory::getDBO();				
			$uri = JFactory::getURI();
			
			$query = "SELECT Count(ID) AS Views_Count  FROM #__site_analytics where Item_ID = " . $item_id . " AND View_Name Like '%" .$menu->getActive()->title."%'";
															
			$database->SetQuery( $query );
			$site_data = $database->loadObjectList();
							 ?>
                             Total Views: <?php echo $site_data[0]->Views_Count; ?> </span>

                        </div>    
                        <?php } ?>
                        
                        <!---Like Dislike End --->
                        <div class="clear"></div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <!--/Body Area -->
    </div>
    <!--/Main Area -->
    <!--Footer Area -->
	<!--Script for Like Dislike pages--->
<?php


if(JRequest::getCmd( 'view' ) != "mainlistcirculars" && JRequest::getCmd( 'view' ) != "category" && JRequest::getCmd( 'view' ) != "mainlistmedias" && JRequest::getCmd( 'view' ) != "mainlistdocuments" && JRequest::getCmd( 'view' ) != "mainshowdocuments") {
// add jscript to rendered page
$document->addScriptDeclaration("
window.addEvent('domready', function(){ //when the DOM is ready
   // add a function to an element event (a button in this case)
  		
		
		
   $$('.vote_btn_like').addEvent('click', function(evt){
		
		var data = " . json_encode($Page_info) . "; 
      // actually call the ajax component. notice the task=test. 
      // format=raw further asks the component to only return our data
		var isVoted = document.getElements('.is_user_voted').get('value');
		var isVoted_like = document.getElements('.is_user_voted_like').get('value');
	
		//get total count like
	  	var total_like_count = document.getElements('.like_total_counts').get('text');
		var total_like_count_int = parseInt(total_like_count);
		var inc_total_like_count = total_like_count_int;
		
		//get Total Unlike Count
		var total_Unlike_count = document.getElements('.unlike_total_counts').get('text');
		var total_Unlike_count_int = parseInt(total_Unlike_count);
		
	 	if(isVoted > 0){
			if(isVoted_like > 0) {
				
				var url='index.php?option=com_eposts&task=IsLike&do=1&status=Deletel&like_view=" . JRequest::getCmd( 'view' ). "';
				inc_total_like_count = inc_total_like_count-1;
			}else {
				var url='index.php?option=com_eposts&task=IsLike&do=1&status=Update&like_view=" . JRequest::getCmd( 'view' ). "';
				total_Unlike_count_int = total_Unlike_count_int-1;
				inc_total_like_count = total_like_count_int+1;
			}
			
			
			
  		}else {	
			
			var url='index.php?option=com_eposts&task=IsLike&do=1&status=Ins&like_view=". JRequest::getCmd( 'view' ). "';
			 inc_total_like_count = total_like_count_int+1;
		}
      // json encode the data we're sending to the controller

      // send it via ajax
	//var is_like = 1;
     var myRequest = new Request({
    	url: url,
    	method: 'post',
		data: data,
    	onRequest: function(){
        	console.log('in request');
    	},
    	onSuccess: function(responseText){
			if(isVoted_like > 0){
				$$('.like_btn').removeClass('like_btn_active');
				$$('.like_btn').addClass('like_btn_inactive');
				$$('.like_total_counts').set('text', inc_total_like_count);
				$$('.is_user_voted').set('value',0);
				$$('.is_user_voted_like').set('value',0);
			}else {
				
			$$('.like_btn').addClass('like_btn_active');
			$$('.like_btn').removeClass('like_btn_inactive');
			$$('.unlike_btn').removeClass('unlike_btn_active');
			$$('.unlike_btn').addClass('unlike_btn_inactive');
			$$('.like_total_counts').set('text', inc_total_like_count);
			$$('.unlike_total_counts').set('text',total_Unlike_count_int);
			$$('.is_user_voted').set('value',1);
			$$('.is_user_voted_like').set('value',1);
			$$('.is_user_voted_unlike').set('value',0);
				}
    	},
    	onFailure: function(){
        	console.log('on failar');
    	}
		
		});
		// and to send it:
		myRequest.send(data);
	});

   });
");

$document->addScriptDeclaration("
window.addEvent('domready', function(){ //when the DOM is ready
   // add a function to an element event (a button in this case)
   $$('.vote_btn_unlike').addEvent('click', function(evt){
		
		var data = " . json_encode($Page_info) . "; 
      // actually call the ajax component. notice the task=test. 
      // format=raw further asks the component to only return our data

		var isVoted = document.getElements('.is_user_voted').get('value');
		var isVoted_unlike = document.getElements('.is_user_voted_unlike').get('value');
	 
	 //get total count unlike
	  	var totalcount = document.getElements('.unlike_total_counts').get('text');
		var totalcount = parseInt(totalcount);
		var inc_totalcount = totalcount;
	  
	  	//get Total like Count
		var total_like_count = document.getElements('.like_total_counts').get('text');
		var total_like_count_int = parseInt(total_like_count);
	 
     if(isVoted > 0){
		 if(isVoted_unlike > 0) {
				var url='index.php?option=com_eposts&task=IsLike&do=1&status=Delete&like_view=" . JRequest::getCmd( 'view' ). "';
				inc_totalcount = totalcount-1;
			}else {
				var url='index.php?option=com_eposts&task=IsLike&do=2&status=Update&like_view=" . JRequest::getCmd( 'view' ). "';
				 inc_totalcount = totalcount+1;
				total_like_count_int = total_like_count_int-1;	
			}
				  
	 }else {
			  var url='index.php?option=com_eposts&task=IsLike&do=2&status=Ins&like_view=" . JRequest::getCmd( 'view' ). "';
			  inc_totalcount = totalcount+1;
	}
		
      // json encode the data we're sending to the controller

      // send it via ajax
	//var is_like = 1;
     var myRequest = new Request({
    	url: url,
    	method: 'post',
		data: data,
    	onRequest: function(){
        	console.log('in request');
    	},
    	onSuccess: function(responseText){
			if(isVoted_unlike > 0){
				$$('.unlike_btn').removeClass('unlike_btn_active');
				$$('.unlike_btn').addClass('unlike_btn_inactive');
				$$('.unlike_total_counts').set('text', inc_totalcount);
				$$('.is_user_voted').set('value',0);
				$$('.is_user_voted_unlike').set('value',0);
			}else {
				$$('.unlike_btn').addClass('unlike_btn_active');
				$$('.unlike_btn').removeClass('unlike_btn_inactive');
				$$('.like_btn').removeClass('like_btn_active');
				$$('.like_btn').addClass('like_btn_inactive');
				$$('.unlike_total_counts').set('text', inc_totalcount);
				$$('.like_total_counts').set('text',total_like_count_int);
				$$('.is_user_voted').set('value',1);
				$$('.is_user_voted_unlike').set('value',1);
				$$('.is_user_voted_like').set('value',0);
			}
			
    	},
    	onFailure: function(){
        	console.log('on failar');
    	}
		
		});
		// and to send it:
		myRequest.send(data);
	});

   });
");
}
?>
<?php 
   			/*Insert For Site analytic */
			$user = &JFactory::getUser();
             		$item_id = JRequest::getCmd( 'cirid' );
			 
			 if(JRequest::getCmd( 'view' )=="article"){
				
				$item_id = JRequest::getCmd( 'id' );
			}else if (JRequest::getCmd( 'view' )=="category"){
				
				$item_id = JRequest::getCmd( 'igid' );
			}
			 
			$database = &JFactory::getDBO();				
			$uri = JFactory::getURI();
			$query = "SELECT Id, Item_Id, Views_Count  FROM #__site_analytics where Item_Id = " . $item_id . " AND View_Name= '" .$menu->getActive()->title ."' AND User_ID = ".$user->id;
															
			$database->SetQuery( $query );
			$site_data = $database->loadObjectList();
			
			if($site_data[0]->Id == ""){
$query = 	$database->SetQuery("INSERT INTO #__site_analytics  (Item_Id, View_Name, Url, User_Id, Views_Count) values (" . $item_id . ",'" . $menu->getActive()->title . "','". $uri->toString() ."',".$user->id.",1) ");
				/*$Views_Count = $site_data[0]->Views_Count;
				$Views_Count++;
				$query = 	$database->SetQuery("UPDATE #__site_analytics SET Views_Count = ". $Views_Count ." Where Id = ".$site_data[0]->Id ." AND View_Name= '" .$menu->getActive()->title ."'");*/
			
			}
			
			$database->query();	
			/*Insert For Site analytic*/
			?>

    <div class="footer_area">
      <div class="main_area">
          <div class="footer_warper">
              <?php if($this->countModules('emiratespost-footermenu')) : ?>
                  <div class="footer_menu">
                      <div class="footer_left">
                        <div class="footer_right">
                        <jdoc:include type="modules" name="emiratespost-footermenu" style="xhtml" />
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
              <?php if($this->countModules('emiratespost-footer')) : ?>
                  <div class="footer_links">
                    <jdoc:include type="modules" name="emiratespost-footer" style="xhtml" />
                    </div>
                <?php endif; ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!--/Footer Area -->
    <jdoc:include type="modules" name="debug" />
</body>
</html>
