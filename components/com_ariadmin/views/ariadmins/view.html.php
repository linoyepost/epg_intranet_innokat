<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AriAdminViewAriAdmins extends JView
{
	protected $items;
	protected $pagination;
	protected $state;
	
	public function display($tpl = null) 
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		
		parent::display($tpl);
	}
}
