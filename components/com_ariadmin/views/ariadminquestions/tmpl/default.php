<?php
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
?>
<form action="<?php echo JRoute::_('index.php?option=com_ariadmin&view=ariadmin');?>" method="post" name="adminForm" id="adminForm">
	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th>
					<?php echo JText::_('JGLOBAL_TITLE'); ?>
				</th>
				<th width="5%">
					<?php echo JText::_('Questions'); ?>
				</th>
				<th width="5%">
					<?php echo JText::_('JSTATUS'); ?>
				</th>
				<th width="1%" class="nowrap">
					<?php echo JText::_('JGRID_HEADING_ID'); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="15">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php
			$originalOrders = array();
			foreach ($this->items as $i => $item) :
			?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center">
						<?php echo JHtml::_('grid.id', $i, $item->QuizId); ?>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_ariadmin&view=ariadmin&layout=edit&id='.$item->QuizId);?>"><?php echo $this->escape($item->QuizName); ?></a>
					</td>
					<td class="center">
						<a href="<?php echo JRoute::_('index.php?option=com_ariadmin&view=ariadminquestions&id='.$item->QuizId);?>"><?php echo JText::_('COM_ARIADMIN_VIEW');?></a>
					</td>
					<td class="center">
						<?php echo JHtml::_('jgrid.published', $item->Status, $i);?>
					</td>
					<td class="center">
						<span><?php echo (int) $item->QuizId; ?></span>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
