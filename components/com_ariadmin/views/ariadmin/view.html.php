<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AriAdminViewAriAdmin extends JView
{
	protected $form;
	protected $item;
	
	public function display($tpl = null) 
	{
		$app		= JFactory::getApplication();
		$user		= JFactory::getUser();
		
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		
		$task = JRequest::getCmd('task');
		$id = JRequest::getCmd('id');
		
		
		$database = &JFactory::getDBO();
		if ($task == 'delete'):
			$database->setQuery("DELETE FROM #__ariquiz WHERE QuizId = '".$id."'");
			$database->query();
			$app->redirect('index.php?option=com_ariadmin&view=ariadmins&id=1&Itemid=302');
		endif;
		
		parent::display($tpl);
	}
}
