<?php
defined('_JEXEC') or die;

clearstatcache();
session_start();

jimport( 'joomla.application.component.view');
jimport( 'joomla.html.parameter' );
jimport( 'joomla.utilities.simplexml' );

global $Itemid, $mainframe, $jlistConfig, $jlistTemplates, $task, $database, $user;

$database	= &JFactory::getDBO();
$jconfig	= new JConfig();
$option		= 'com_ariadmin';

$user		= &JFactory::getUser();
$mainframe	= JFactory::getApplication();
$Itemid		= JRequest::getInt('Itemid');
$id			= JRequest::getInt('id');


$document	= &JFactory::getDocument();
$document->addScript('components/com_eposts/eposts.js');
$document->addStyleSheet("media/com_igallery/css/admin.css");

$params		= &$mainframe->getParams();

$GLOBALS['_VERSION']	= new JVersion();
$version				= $GLOBALS['_VERSION']->getShortVersion();
	
$params2		= JComponentHelper::getParams('com_languages');
$frontend_lang	= $params2->get('site', 'en-GB');
$language		= JLanguage::getInstance($frontend_lang);
    
require_once( JPATH_COMPONENT_SITE.DS.'ariadmin.html.php' ); 
require_once( JPATH_COMPONENT_SITE.DS.'ariadmin.class.php' );

$task 			= JRequest::getCmd( 'task' );
$view           = JRequest::getCmd(  'view' );

if ($task && !$view) $view = $task;

switch ($view) {
	default:
	case 'ariadmins':
		if ($task == 'save'){
			saveSurvey($id, $option, $view);
		}elseif ($task == 'delete'){
			deleteSurvey($id, $option, $view);
		}else {
			listSurveys($id, $option, $view);
		}
	break;
	case 'ariadmin':
		editSurvey($id, $option, $view);
	break;
	case 'ariquestions':
		if ($task == 'save') :
			saveQuestion($id, $option, $view);
		elseif ($task == 'delete') :
			deleteQuestion($id, $option, $view);
		else :
			listQuestions($id, $option, $view);
		endif;
	break;
	case 'ariquestion':
		editQuestion($id, $option, $view);
	break;
	case 'results':
		exportCSV($id, $option, $view);
	break;
}

function saveSurvey($id, $option, $view) {
	global $Itemid, $mainframe, $jlistConfig, $jlistTemplates, $task, $database, $user;
	
	$message = '';
	
	$jform = JRequest::getVar('jform');
	
	$jform['Description']		= JRequest::getVar('Description', '', 'post', 'string', JREQUEST_ALLOWHTML);
	$jform['Description_ar']	= JRequest::getVar('Description_ar', '', 'post', 'string', JREQUEST_ALLOWHTML);
	
	$jform['ExpiryDate'] .= ' 23:59:00';
	if ($jform['QuizId']){
		$database->setQuery("UPDATE #__ariquiz SET `QuizName` = '".$jform['QuizName']."', `QuizName_ar` = '".$jform['QuizName_ar']."', `ExpiryDate` = '".$jform['ExpiryDate']."', `ModifiedBy` = '".$user->id."', `Modified` = NOW(), `Status` = '".$jform['Status']."', `TotalTime` = '".$jform['TotalTime']."', `PassedScore` = '".$jform['PassedScore']."', `QuestionCount` = '".$jform['QuestionCount']."', `QuestionTime` = '".$jform['QuestionTime']."', `Description` = '".$jform['Description']."', `Description_ar` = '".$jform['Description_ar']."' WHERE `QuizId` = '".$jform['QuizId']."'");
		$database->query();
		
		$message = JText::sprintf('COM_ARIADMIN_SUCCESS_UPDATE', $jform['QuizName']);
	}else {
			$database->setQuery("INSERT INTO #__ariquiz (`QuizId`, `QuizName`, `QuizName_ar`, `ExpiryDate`, `CreatedBy`, `Created`, `Status`, `TotalTime`, `PassedScore`, `QuestionCount`, `QuestionTime`, `Description`, `Description_ar`) VALUES (NULL, '".$jform['QuizName']."', '".$jform['QuizName_ar']."', '".$jform['ExpiryDate']."', '".$user->id."', NOW(), '".$jform['Status']."', '".$jform['TotalTime']."', '".$jform['PassedScore']."', '".$jform['QuestionCount']."', '".$jform['QuestionTime']."', '".$jform['Description']."', '".$jform['Description_ar']."')");
		if ($database->query()) {
			$recIDas = $database->insertid();
			/*$database->setQuery("INSERT INTO #__ariquizquizcategory (`QuizId`, `CategoryId`) VALUES ('".$item."', '".$jform['CategoryId']."')");
			$database->query();*/
			$database->setQuery("INSERT INTO #__ariquizquizcategory (`QuizId`, `CategoryId`) VALUES ('".$recIDas."', '".$jform['CategoryId']."')");
			$database->query();
		
			$message = JText::sprintf('COM_ARIADMIN_SUCCESS_SAVED', $jform['QuizName']);
			
		}
	}

    if ($jform['Status'] == 1) :
        $database->setQuery("SELECT email FROM #__users WHERE `block` = '0'");
        $users = $database->loadObjectList();
		$sendTo = array();
        $mailer =& JFactory::getMailer();
		$config =& JFactory::getConfig();
		
		$fromMail = $config->getValue('config.mailfrom');
		$fromName = $config->getValue('config.fromname');
        
		$body = '';
				if ($jform['CategoryId'] == 1 ) {
                    $body   = "Hi";
                    $body    .= "<br><br><p>A new &quot;".$jform['QuizName']."&quot; Survey has been created.</p>";
                    $body    .= "<p>Thank you</p>";
                    $body    .= "<p><b>Note:</b> This is a system generated mail. Please do not reply to this email address.</p>";
                    $subject = 'Notification about new survey.';
                } else {
                    $body   = "Hi";
                    $body    .= "<br><br><p>A new &quot;".$jform['QuizName']."&quot; Quiz has been created.</p>";
                    $body    .= "<p>Thank you</p>";
                    $body    .= "<p><b>Note:</b> This is a system generated mail. Please do not reply to this email address.</p>";
                    $subject = 'Notification about new quiz.';
                }
               
        $sendTo = array();
		$html_format = true;
		//$regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
        foreach($users as $user) :
            if ($user->email != '') {
				echo $user->email.',';				 
				// Run the preg_match() function on regex against the email address
				if (filter_var($user->email, FILTER_VALIDATE_EMAIL)){
				//if (preg_match($regex, $user->email)) {
					 //echo $user->email . " is a valid email. We can accept it.";
					 //mail ( string $to , string $subject , string $message [, string $additional_headers [, string $additional_parameters ]] );
				  // $success = JUtility::sendMail($fromMail, $fromName, $user->email, $subject, $body, $html_format, NULL, NULL, NULL, $fromMail, $fromName);
				  mail($user->email, $subject, $body);
				} else { 
					 //echo $user->email . " is an invalid email. Please try again.";
				} 
				//exit;
				//$success = @JUtility::sendMail($fromMail, $fromName, $user->email, $subject, $body, TRUE, NULL, NULL, NULL, $user->email, $user->name);
                //$success = JUtility::sendMail($fromMail, $fromName, $user->email, $subject, $body, $html_format, NULL, NULL, NULL, $fromMail, $fromName);
            }
        endforeach;
       
    endif;
	$mainframe->redirect('index.php?option=com_ariadmin&view=ariadmins&id='.$id.'&Itemid='.$Itemid, $message);
}

function listSurveys($id, $option, $view) {
	JListHtml::listSurveys($id, $option, $view);
}

function editSurvey($id, $option, $view) {
	JListHtml::editSurvey($id, $option, $view);
}

function deleteSurvey($id, $option, $view) {
	global $Itemid, $mainframe, $jlistConfig, $jlistTemplates, $task, $database, $user;
	
	$QuizId = JRequest::getInt('QuizId');
	
	$database->setQuery("DELETE FROM #__ariquiz WHERE `QuizId` = " . $QuizId);
	$database->query();
	$database->setQuery("DELETE FROM #__ariquizquizcategory WHERE `QuizId` = " . $QuizId);
	$database->query();
	
	$message = '';
	
	if ($jform['QuizId']) :
	else :
	endif;
	
	if ($id == 1) :
		$message = JText::sprintf('COM_ARIADMIN_DELETE_SUCCESS', JText::_('COM_ARIADMIN_SURVEY'));
	else :
		$message = JText::sprintf('COM_ARIADMIN_DELETE_SUCCESS', JText::_('COM_ARIADMIN_QUIZ'));
	endif;
	$mainframe->redirect('index.php?option=com_ariadmin&view=ariadmins&id='.$id.'&Itemid='.$Itemid, $message);
}

function listQuestions($id, $option, $view) {
	JListHtml::listQuestions($id, $option, $view);
}

function editQuestion($id, $option, $view) {
	JListHtml::editQuestion($id, $option, $view);
}

function saveQuestion($id, $option, $view) {
	global $Itemid, $mainframe, $jlistConfig, $jlistTemplates, $task, $database, $user;
	
	$jform		= JRequest::getVar('jform');
	$QuizId		= JRequest::getVar('QuizId');
	
	$database->setQuery("SELECT count(*) FROM #__ariquizquestion WHERE `QuizId` = '".$QuizId."'");
	$record = $database->loadResult();
	
	$qCount = $record+1;
	
	$jform['Question']			= JRequest::getVar('Question', '', 'post', 'string', JREQUEST_ALLOWHTML);
	$jform['Question_ar']		= JRequest::getVar('Question_ar', '', 'post', 'string', JREQUEST_ALLOWHTML);
	$jform['Description']		= JRequest::getVar('Description', '', 'post', 'string', JREQUEST_ALLOWHTML);
	$jform['Description_ar']	= JRequest::getVar('Description_ar', '', 'post', 'string', JREQUEST_ALLOWHTML);
	$jform['QuestionTypeId']	= JRequest::getInt('QuestionTypeId');
	$QId	= ($jform['QuestionId'])? $jform['QuestionId'] : 0;

	/*echo "SELECT * FROM #__ariquizquestionversion AS a LEFT JOIN #__ariquizquestion AS b ON a.QuestionVersionId = b.QuestionVersionId WHERE a.Question ='".$jform['Question']."' AND b.QuizId = '".$QuizId."' && b.QuestionId != '".$QId."' ";
	exit;
*/	
	$database->setQuery("SELECT * FROM #__ariquizquestionversion AS a LEFT JOIN #__ariquizquestion AS b ON a.QuestionVersionId = b.QuestionVersionId WHERE a.Question ='".$jform['Question']."' AND b.QuizId = '".$QuizId."' && a.QuestionId != '".$QId."' ");
	$dRecord = $database->loadResult();
	
	if ($dRecord) {
		$message = JText::_('Question is already exist.');
		
		$mainframe->redirect('index.php?option=com_ariadmin&view=ariquestions&id='.$id.'&QuizId='.$QuizId.'&Itemid='.$Itemid, $message, 'error');
	}
	
	//$new		= JRequest::getVar('rbCorrect2');
	$rbCorrect		= JRequest::getVar('rbCorrect');
echo $rbCorrect;

	//exit;
	
	
	
	
	
	$tbxAnswer		= JRequest::getVar('tbxAnswer22');
	$tbxAnswer_ar	= JRequest::getVar('tbxAnswer_ar');
	

	//echo $rbCorrect;
	//print_r($_REQUEST['rbCorrect']);
	
	//print_r($rbCorrect);
	//print_r($tbxAnswer);
	//print_r($tbxAnswer_ar);
	//echo $qType = $jform['QuestionTypeId'];
	//exit;
	
	$qType = $jform['QuestionTypeId'];
	$count = count($tbxAnswer);
	$answer = '';
	if ($count){
		$answer .= '<answers>';
		for($i = 0; $i < $count; $i++) {
			if ($qType == 1) :
				$true = ($rbCorrect == $i) ? ' correct="true"' : '';
			
			else :
				$true = (in_array($i, $rbCorrect)) ? 'correct="true"' : '';
		
			endif;
			if ($tbxAnswer[$i] != '' || $tbxAnswer_ar[$i] != '') {
				if ($qType == 1 || $qType == 2) :
					$answer .= '<answer id="'.uniqid('', true).'"'.$true.'>'.$tbxAnswer[$i].','.$tbxAnswer_ar[$i].'</answer>';
				
				else :
					$answer .= '<answer id="'.uniqid('', true).'">'.$tbxAnswer[$i].'</answer>';
				endif;
			}
		}
		$answer .= '</answers>';
	}
	

	
	$message = '';
	
	$qTitle = strip_tags($jform['Question']);
	$length = 20;
	if (mb_strlen($qTitle) > $length) :
		$qTitle = mb_substr($qTitle, 0, $length) . '...';
	endif;
	
	if ($jform['QuestionId']) :
		$database->setQuery("UPDATE #__ariquizquestionversion SET `QuestionTypeId` = '".$jform['QuestionTypeId']."', `Question` = '".$jform['Question']."', `Question_ar` = '".$jform['Question_ar']."', `Description` = '".$jform['Description']."', `Description_ar` = '".$jform['Description_ar']."', `Score` = '".$jform['Score']."', `Data` = '".$answer."' WHERE `QuestionId` = '".$jform['QuestionId']."'");
		$database->query();
	
		$message = JText::sprintf('COM_ARIADMIN_SUCCESS_UPDATE', $qTitle);
	else :
		$database->setQuery("INSERT INTO #__ariquizquestion (`QuestionId`, `QuizId`, `CreatedBy`, `Created`, `Status`, `QuestionIndex`) VALUES (NULL, '".$QuizId."', '".$user->id."', NOW(), '1', '".$qCount."')");
		
		if ($database->query()) {
			$QuestionId = $database->insertid();
			$database->setQuery("INSERT INTO #__ariquizquestionversion (`QuestionVersionId`, `QuestionId`, `QuestionTypeId`, `Question`, `Question_ar`, `Description`, `Description_ar`, `Created`, `CreatedBy`, `Score`, `Data`) VALUES (NULL, '".$QuestionId."', '".$jform['QuestionTypeId']."', '".$jform['Question']."', '".$jform['Question_ar']."', '".$jform['Description']."', '".$jform['Description_ar']."', NOW(), '".$user->id."', '".$jform['Score']."', '".$answer."')");
			$database->query();
			$QuestionVersionId = $database->insertid();
			$database->setQuery("UPDATE #__ariquizquestion SET `QuestionVersionId` = '".$QuestionVersionId."' WHERE `QuestionId` = '".$QuestionId."'");
			$database->query();
			
		}
		$message = JText::sprintf('COM_ARIADMIN_SUCCESS_SAVED', $qTitle);
	endif;
	$mainframe->redirect('index.php?option=com_ariadmin&view=ariquestions&id='.$id.'&QuizId='.$QuizId.'&Itemid='.$Itemid, $message);
}

function deleteQuestion($id, $option, $view) {
	global $Itemid, $mainframe, $jlistConfig, $jlistTemplates, $task, $database, $user;
	
	$QuizId = JRequest::getInt('QuizId');
	$Questionid = JRequest::getInt('Questionid');
	
	$database->setQuery("DELETE FROM #__ariquizquestion WHERE `Questionid` = " . $Questionid);
	$database->query();
	$database->setQuery("DELETE FROM #__ariquizquestionversion WHERE `Questionid` = " . $Questionid);
	$database->query();
	
	$message = JText::sprintf('COM_ARIADMIN_DELETE_SUCCESS', JText::_('COM_ARIADMIN_QUESTION'));
	
	$mainframe->redirect('index.php?option=com_ariadmin&view=ariquestions&id='.$id.'&QuizId='.$QuizId.'&Itemid='.$Itemid, $message);
}

function exportCSV($id, $option, $view) {
	global $Itemid, $mainframe, $jlistConfig, $jlistTemplates, $task, $database, $user;
	
	$QuizId = JRequest::getInt('quizId');
	
	$query = "SELECT S.QuizId, SQQ.QuestionId, SQQ.QuestionTypeId, S.QuizName, SQQ.QuestionVersionId, SQQ.Question, SQQ.Data FROM jos_ariquiz as S
				LEFT JOIN jos_ariquizquestion SQ ON S.QuizId = SQ.QuizId
				LEFT JOIN jos_ariquizquestionversion SQQ ON SQ.QuestionVersionId = SQQ.QuestionVersionId 
				WHERE S.QuizId = '".$QuizId."'";
	$database->setQuery($query);
	$result = $database->loadObjectList();
	
	//print '<pre>';
	//print_r($result);
	//print '<hr>';
	$fields = array();
	
	$fields[] = '#';
	$fields[] = 'QuizId';
	$fields[] = 'QuizName';
	$fields[] = 'TotalUsers';
	
	$count = 1;
	foreach ($result as $key1 => $values) {
		foreach ($values as $key => $value) {
			if ($key == 'Question' || $key == 'Data') :
				if ($key == 'Data') :
					$xmlData = simplexml_load_string($value);
					$count2 = 1;
					foreach($xmlData as $key => $data) :
						$answers = explode(',', $data[0]);
						$fields[] = 'Option '. $count2 . ': ' . $answers[0];
						
						$count2++;
					endforeach;
				else :
					$fields[] = $key . $count . ': ' . strip_tags($value);
				endif;
			endif;
		}
		$count++;
	}
	
	$fields2 = array();
	
	$count = 1;
	foreach ($result as $key1 => $values) {
		if ($count == 1) {
			$fields2[] = $count;
			$fields2[] = $QuizId;
			$fields2[] = $values->QuizName;
		}
		
		$database->setQuery("SELECT COUNT(*) FROM #__ariquizstatistics WHERE `QuestionVersionId` = '".$values->QuestionVersionId."'");
		$totalAns = $database->loadResult();
		if ($count == 1) {
			$fields2[] = $totalAns;
		}
		$fields2[] = strip_tags($values->Question);
		
		$xmlData = simplexml_load_string($values->Data);
		foreach($xmlData as $key => $data) {
			$ans = $data->attributes();
			$answers = explode(',', $data[0]);
			
			$crrAns = $answers[0];
			if ($values->QuestionTypeId == '4') {
				$database->setQuery("SELECT * FROM #__ariquizstatistics WHERE `QuestionVersionId` = '".$values->QuestionVersionId."'");
				$crrselAns = $database->loadObjectList();
				$textAnswers = 'd';
				foreach($crrselAns as $answers) {
					$xmlData = simplexml_load_string($answers->Data);
					$anssswer = '';
					foreach($xmlData as $key => $data) {
						$anssswer = $data[0];
					}
					$textAnswers .= strip_tags($anssswer) . ', ';
				}
				$fields2[] = $textAnswers;
			} else {
				$database->setQuery("SELECT COUNT(*) FROM #__ariquizstatistics WHERE `Data` LIKE '%".$ans['id']."%'");
				$crrselAns = $database->loadResult();
				$percAns = round(($crrselAns/$totalAns) * 100);
				$fields2[] = $percAns . '%';
			}
		}
		$count++;
	}
	
	/*
	print_r($fields);
	print '<hr>';
	print_r($fields2);
	print '<hr>';
	exit;
	*/
	
	
	$fields = array_map(create_function('$v', 'return \'"\' . $v . \'"\';'), $fields);
	$csv = join("\t", $fields);
	$csv .= "\r\n";
	$fields2 = array_map(create_function('$v', 'return \'"\' . $v . \'"\';'), $fields2);
	$csv .= join("\t", $fields2);
		
	if (function_exists('iconv'))
	{
		$csv = chr(255) . chr(254) . @iconv('UTF-8', 'UTF-16LE', $csv);
	}
	else if (function_exists('mb_convert_encoding'))
	{
		$csv = chr(255) . chr(254) . @mb_convert_encoding($csv, 'UTF-16LE', 'UTF-8');
	}
	@ob_end_clean();
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="Survey - '.$result[0]->QuizName . ' - ' .date('d-m-Y H-m-s').'.csv"');
	header("Expires: 0");
	header('Accept-Ranges: bytes');
	header("Cache-control: private");
	header('Pragma: private');
	header('Content-Length: ' . (string)strlen($csv));

	echo $csv;
	exit();
}
