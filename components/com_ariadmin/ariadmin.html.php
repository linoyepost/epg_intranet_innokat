<?php
class JListHtml {
	function listSurveys($id, $option, $view) {
		global $Itemid, $mainframe, $jlistConfig, $jlistTemplates, $task, $database, $user;
		
		$user = &JFactory::getUser();
		
		$query = $database->getQuery(true);
		
		$query->select('QC.CategoryId,QC.CategoryName,Q.QuizName,Q.QuizName_ar,Q.QuizId,Q.Status 
			FROM #__ariquiz as Q 
			LEFT JOIN #__ariquizquizcategory as QQC ON Q.QuizId = QQC.QuizId
			LEFT JOIN #__ariquizcategory as QC ON QC.CategoryId = QQC.CategoryId
			WHERE QC.CategoryId = ' . (int)$id . ' ORDER BY Q.QuizId DESC');
		
		//echo $query;
		$database->setQuery($query);
		$surveys = $database->loadObjectList();
		?>
		<?php if ($id == 1) : ?>
        <a href="<?php print JRoute::_('index.php?option=com_ariadmin&view=ariadmin&layout=edit&id=1'); ?>" class="brightNew"><?php print JText::_('New Survey'); ?></a>
        <?php else : ?>
        <a href="<?php print JRoute::_('index.php?option=com_ariadmin&view=ariadmin&layout=edit&id=2'); ?>" class="brightNew"><?php print JText::_('New Quiz'); ?></a>
        <?php endif; ?>
        <div class="clear"></div>
        <br /><br />
        <div class="igallery_form">
        	<form name="adminForm" id="adminForm" action="<?php print JRoute::_('index.php?option=com_ariadmin'); ?>" method="post">
            	<table class="adminlist">
                	<thead>
                    	<tr>
                        	<th><?php print JText::_('JGLOBAL_TITLE'); ?></th>
                            <?php if ($id == 1) : ?>
                        	<th width="10%"><?php print JText::_('Export CSV'); ?></th>
                        	<th width="10%"><?php print JText::_('Results'); ?></th>
                            <?php endif; ?>
							<?php if ($id == 2) : ?>
                        	<th width="10%"><?php print JText::_('Export CSV'); ?></th>
                        	<th width="10%"><?php print JText::_('Results'); ?></th>
                            <?php endif; ?>
                        	<th width="10%"><?php print JText::_('COM_ARIADMIN_QUESTIONS'); ?></th>
                        	<th width="5%"><?php print JText::_('JSTATUS'); ?></th>
                        	<th width="10%"><?php print JText::_('Action'); ?></th>
                        </tr>
                    </thead>
                    <tfoot>
                    	<tr>
                        	<td colspan="7">&nbsp;</td>
                        </tr>
                    </tfoot>
                    <tbody>
						<?php
                        foreach($surveys as $key => $item) :
							
							$database->setQuery("SELECT count(*) FROM #__ariquizstatisticsinfo WHERE `QuizId` = '".$item->QuizId."'");
							//echo ("SELECT count(*) FROM #__ariquizstatisticsinfo WHERE `QuizId` = '".$item->QuizId."'");
							$userStatus = $database->loadResult();
							?>
							<tr class="row<?php print ($key % 2) ? 1 : 0;?>">
                            	<td>
                                	<?php if ($id == 1) : ?>
										<?php if ($userStatus < 1) : ?>
                                        <a href="<?php print JRoute::_('index.php?option=com_ariadmin&view=ariadmin&layout=edit&id='.$id.'&QuizId='.$item->QuizId);?>">
                                        <?php endif; ?>
                                            <?php print $item->QuizName; ?>
                                        <?php if ($userStatus < 1) : ?>
                                        </a>
                                        <?php endif; ?>
                                    <?php else : ?>
                                	<a href="<?php print JRoute::_('index.php?option=com_ariadmin&view=ariadmin&layout=edit&id='.$id.'&QuizId='.$item->QuizId);?>">
										<?php print $item->QuizName; ?>
                                    </a>
                                    <?php endif; ?>
                                </td>
                                <?php if ($id == 2) : ?>
                                <?php
                                	$database->setQuery("SELECT * FROM #__ariquizstatisticsinfo WHERE `QuizId` = '".$item->QuizId."'");
									$ticket = $database->LoadObject();
								?>
                                <td align="center">
                                	<?php if ($ticket->TicketId) : ?>
                                	<a href="<?php print JRoute::_('index.php?option=com_ariadmin&task=results&aricategory=1&quizId='.$item->QuizId); ?>">
                                	<?php print JText::_('Export'); ?>
                                    </a>
                                    <?php else : ?>
                                    	<?php print JText::_('N/A'); ?>
                                    <?php endif; ?>
                                </td>

                                <td align="center">
                                	<?php if ($ticket->TicketId) : ?>
                                	<a href="<?php print JRoute::_('index.php?option=com_ariquizlite&task=quiz_result&aricategory=1&ticketId='.$ticket->TicketId.'&Itemid=302'); ?>">
                                	<?php print JText::_('COM_ARIADMIN_VIEW'); ?>
                                    </a>
                                    <?php else : ?>
                                    	<?php print JText::_('N/A'); ?>
                                    <?php endif; ?>
                                </td>
                                <?php endif; ?>
								 <?php if ($id == 1) : ?>
                                <?php
                                	$database->setQuery("SELECT * FROM #__ariquizstatisticsinfo WHERE `QuizId` = '".$item->QuizId."'");
									$ticket = $database->LoadObject();
								?>
                                <td align="center">
                                	<?php if ($ticket->TicketId) : ?>
                                	<a href="<?php print JRoute::_('index.php?option=com_ariadmin&task=results&aricategory=1&quizId='.$item->QuizId); ?>">
                                	<?php print JText::_('Export'); ?>
                                    </a>
                                    <?php else : ?>
                                    	<?php print JText::_('N/A'); ?>
                                    <?php endif; ?>
                                </td>

                                <td align="center">
                                	<?php if ($ticket->TicketId) : ?>
                                	<a href="<?php print JRoute::_('index.php?option=com_ariquizlite&task=quiz_result&aricategory=1&ticketId='.$ticket->TicketId.'&Itemid=302'); ?>">
                                	<?php print JText::_('COM_ARIADMIN_VIEW'); ?>
                                    </a>
                                    <?php else : ?>
                                    	<?php print JText::_('N/A'); ?>
                                    <?php endif; ?>
                                </td>
                                <?php endif; ?>
                            	<td class="center">
									<a href="<?php print JRoute::_('index.php?option=com_ariadmin&view=ariquestions&id='.$id.'&QuizId='.$item->QuizId);?>">
										<?php print JText::_('COM_ARIADMIN_VIEW'); ?>
                                    </a>
                                </td>
                            	<td class="center"><?php echo JHtml::_('jgrid.published', $item->Status, $i);?></td>
                            	<td class="center">
                                	<a href="<?php echo JRoute::_('index.php?option=com_ariadmin&view=ariadmins&layout=edit&task=delete&id='.$id.'&QuizId='.$item->QuizId);?>">
										<?php echo JText::_('Delete');?>
                                    </a>
                                </td>
							</tr>
							<?php
                        endforeach;
                        ?>
        			</tbody>
        		</table>
        	</form>
        </div>
		<?php
	}
	
	function editSurvey($id, $option, $view) {
		global $Itemid, $mainframe, $jlistConfig, $jlistTemplates, $task, $database, $user;
		
		$editor =& JFactory::getEditor();
		$user = &JFactory::getUser();
		
		JHTML::_("behavior.mootools");
		$document = &JFactory::getDocument();
		if ($id == 1) {
			$document->addScriptDeclaration("
				document.addEvent('domready', function(){
					$('adminForm').addEvent('submit', function(e) {
						var title = $('jform_QuizName').get('value');
						var title_ar = $('jform_QuizName_ar').get('value');
						if (title == '' || title_ar == '' ) {
							new Event(e).stop();
							alert('Star field(s) must require.');
						}
					});
				});"
			);
		} else {
			$document->addScriptDeclaration("
				document.addEvent('domready', function(){
					$('adminForm').addEvent('submit', function(e) {
						var title = $('jform_QuizName').get('value');
						var ttime = $('jform_TotalTime').get('value');
						var pscor = $('jform_PassedScore').get('value');
						var qcont = $('jform_QuestionCount').get('value');
						var qtime = $('jform_QuestionTime').get('value');
						var qdate = $('ExpiryDate').get('value');
						if (title == '' || ttime == '' || qdate == '' || qdate = '') {
							new Event(e).stop();
							alert('Star field(s) must required.');
						}
						var intRegex = /^\d+$/;
						if (!intRegex.test(ttime) || !intRegex.test(pscor) || !intRegex.test(qcont) || !intRegex.test(qtime)) {
							new Event(e).stop();
							alert('The values of \"Total Time, Passed Score, Question Count and Question Time\" should be Numerics only.');
						}
					});
				});"
			);
		}
		$qid = JRequest::getCmd('QuizId', 0);
		$query = $database->getQuery(true);
		
		$query->select('QC.CategoryId, QC.CategoryName, Q.QuizName, Q.QuizName_ar, Q.QuizId, Q.Status, Q.TotalTime, Q.PassedScore, Q.QuestionCount, Q.QuestionTime, Q.Description, Q.Description_ar, Q.ExpiryDate');
		$query->from('#__ariquiz as Q');
		$query->leftJoin('#__ariquizquizcategory as QQC ON Q.QuizId = QQC.QuizId');
		$query->leftJoin('#__ariquizcategory as QC ON QC.CategoryId = QQC.CategoryId');
		$query->where('Q.QuizId = ' . (int)$qid);
		
		$database->setQuery($query);
		$item = $database->loadObject();
		?>
        <div class="adminlist">
        	<table width="100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<th align="left">
                    	<?php
						$formTitle = '';
						if ($item->QuizId) :
							$formTitle .= 'Edit ';
						else :
							$formTitle .= 'New ';
						endif;
						
                        if ($id == 1) :
							$formTitle .= 'Survey';
						else :
							$formTitle .= 'Quiz';
						endif;
						print $formTitle;
						
						$breadcrumbs =& $mainframe->getPathWay();
						$title = JText::_($formTitle);
						$length = 20;
						if (mb_strlen($title) > $length) :
							$title = mb_substr($title, 0, $length) . '...';
						endif;
						$breadcrumbs->addItem($title, JRoute::_('index.php?option=com_ariadmin&view=ariadmins&id='.$id));
						?>
                    </th>
                </tr>
            </table>
        </div>
        <div class="igallery_form">
        	<form name="adminForm" id="adminForm" action="<?php print JRoute::_('index.php?option=com_ariadmin&view=ariadmins&id='.$id.'&Itemid='.$Itemid); ?>" method="post">
            	<ul class="adminformlist">
                	<li>
                    	<input type="hidden" class="readonly" value="<?php print $item->QuizId; ?>" id="jform_QuizId" name="jform[QuizId]">
                    </li>
                    <li>
                    	<label for="jform_QuizName" id="jform_QuizName-lbl"><?php print JText::_('COM_ARIADMIN_QUIZNAME_LABEL'); ?><font color="#990000">*</font>:</label>
    					<input type="text" class="inputbox" value="<?php print $item->QuizName; ?>" id="jform_QuizName" name="jform[QuizName]">
                    </li>
                    <li>
                        <label for="jform_QuizName_ar" id="jform_QuizName_ar-lbl"><?php print JText::_('COM_ARIADMIN_QUIZNAME_LABEL_AR'); ?><font color="#990000">*</font>:</label>
                        <input type="text" class="inputbox" value="<?php print $item->QuizName_ar; ?>" id="jform_QuizName_ar" name="jform[QuizName_ar]">
                    </li>
                    <li>
                    	<input type="hidden" class="readonly" value="<?php print $id; ?>" id="jform_CategoryId" name="jform[CategoryId]">
                    </li>
                    <li>
                        <label for="jform_Status" id="jform_Status-lbl"><?php print JText::_('COM_ARIADMIN_STATUS_LABEL'); ?>:</label>
                        <select class="inputbox" name="jform[Status]" id="jform_Status">
                          <option <?php print $item->Status == 2 ? ' selected="selected"' : ''; ?> value="2">Unpublished</option>
                          <option <?php print $item->Status == 1 ? ' selected="selected"' : ''; ?> value="1">Published</option>
                        </select>
                    </li>
                    <?php if ($id != 1) : ?>
                    <li>
                        <label for="jform_TotalTime" id="jform_TotalTime-lbl"><?php print JText::_('COM_ARIADMIN_TOTALTIME_LABEL'); ?>:</label>
                        <input type="text" class="inputbox validate['digit']" value="<?php print $item->TotalTime; ?>" id="jform_TotalTime" name="jform[TotalTime]">
                    </li>
                    <li>
                        <label for="jform_PassedScore" id="jform_PassedScore-lbl"><?php print JText::_('COM_ARIADMIN_PASSEDSCORE_LABEL'); ?>:</label>
                        <input type="text" class="inputbox" value="<?php print $item->PassedScore; ?>" id="jform_PassedScore" name="jform[PassedScore]">
                    </li>
                    <li>
                        <label for="jform_QuestionCount" id="jform_QuestionCount-lbl"><?php print JText::_('COM_ARIADMIN_QUESTIONCOUNT_LABEL'); ?>:</label>
                        <input type="text" class="inputbox" value="<?php print $item->QuestionCount; ?>" id="jform_QuestionCount" name="jform[QuestionCount]">
                    </li>
                    <li>
                        <label for="jform_QuestionTime" id="jform_QuestionTime-lbl"><?php print JText::_('COM_ARIADMIN_QUESTIONTIME_LABEL'); ?>:</label>
                        <input type="text" class="inputbox" value="<?php print $item->QuestionTime; ?>" id="jform_QuestionTime" name="jform[QuestionTime]">
                    </li>
                    <?php endif; ?>
                    <li>
                    	<?php JHTML::_('behavior.calendar'); ?>
                        <label for="jform_QuestionCount" id="jform_QuestionCount-lbl"><?php print JText::_('Expiry Date'); ?><font color="#990000">*</font>:</label>
                    	<input class="inputbox" type="text" name="jform[ExpiryDate]" id="ExpiryDate"  value="<?php echo strtotime($item->ExpiryDate) ? date('Y-m-d', strtotime($item->ExpiryDate)) : ''; ?>" onclick="return showCalendar('ExpiryDate','%Y-%m-%d');"/>
                    </li>
                    <li>
                        <label for="jform_QuestionTime" id="jform_QuestionTime-lbl"><?php print JText::_('COM_ARIADMIN_DESCP_LABEL'); ?>:</label>
						<?php echo $editor->display( 'Description',  $item->Description , '', '10', '80', '5', false, false ) ;?>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <label for="jform_QuestionTime" id="jform_QuestionTime-lbl"><?php print JText::_('COM_ARIADMIN_DESCP_LABEL_AR'); ?>:</label>
						<?php echo $editor->display( 'Description_ar',  $item->Description_ar , '', '10', '80', '5', false, false ) ;?>
                    	<div class="clear"></div>
                    </li>
                    <li>
                    	<label>&nbsp;</label>
                		<input type="submit" value="Save" />
                                  <input name="submit" type="reset" onclick="javascript:history.go(-1)" value="Cancel"/>
                    </li>
                </ul>
                <div>
                    <input type="hidden" value="com_ariadmin" name="option">
                    <input type="hidden" value="ariadmins" name="view">
                    <input type="hidden" value="save" name="task">
                    <?php echo JHtml::_('form.token'); ?>
        		</div>
            </form>
        </div>
		<?php
	}
	
	function listQuestions($id, $option, $view) {
		global $Itemid, $mainframe, $jlistConfig, $jlistTemplates, $task, $database, $user;
		
		$user = &JFactory::getUser();
		
		$qid = JRequest::getCmd('QuizId');
		
		$breadcrumbs =& $mainframe->getPathWay();
		$query = $database->getQuery(true);
		
		$query->select('QC.CategoryId, QC.CategoryName, Q.QuizName, Q.QuizName_ar, Q.QuizId, Q.Status, Q.TotalTime, Q.PassedScore, Q.QuestionCount, Q.QuestionTime');
		$query->from('#__ariquiz as Q');
		$query->leftJoin('#__ariquizquizcategory as QQC ON Q.QuizId = QQC.QuizId');
		$query->leftJoin('#__ariquizcategory as QC ON QC.CategoryId = QQC.CategoryId');
		$query->where('Q.QuizId = ' . (int)$qid);
		
		$database->setQuery($query);
		$item = $database->loadObject();
		
		$title = strip_tags($item->QuizName);
		$length = 20;
		if (mb_strlen($title) > $length) :
			$title = mb_substr($title, 0, $length) . '...';
		endif;
		$breadcrumbs->addItem($title, JRoute::_('index.php?option=com_ariadmin&view=ariadmins&id='.$id));
		
		$query = $database->getQuery(true);
		
		$query->select('SQ.QuestionVersionId, SQ.QuestionIndex, SQ.Status, SQ.QuestionId, SQV.QuestionTime, SQV.Question');
		$query->from('#__ariquizquestion as SQ');
		$query->leftJoin('#__ariquizquestionversion as SQV ON SQ.QuestionVersionId = SQV.QuestionVersionId');
		$query->where("SQ.QuizId = '".(int)$qid."' AND (SQV.QuestionCategoryId = 0 OR SQV.QuestionCategoryId IS NULL)");
		
		$database->setQuery($query);
		$items = $database->loadObjectList();
		
		$database->setQuery("SELECT count(*) FROM #__ariquizstatisticsinfo WHERE `QuizId` = '".$qid."'");
		$userStatus = $database->loadResult();
		?>
        <?php if ($userStatus < 1) : ?>
        <a href="<?php print JRoute::_('index.php?option=com_ariadmin&view=ariquestion&layout=edit&id='.$id.'&QuizId='.$qid); ?>" class="brightNew"><?php print JText::_('New Question'); ?></a>
        <?php endif; ?>
        <div class="clear"></div>
        <br /><br />
        <div class="igallery_form">
        	<form name="adminForm" id="adminForm" action="<?php print JRoute::_('index.php?option=com_ariadmin'); ?>" method="post">
            	<table class="adminlist">
                	<thead>
                    	<tr>
                        	<th><?php print JText::_('JGLOBAL_TITLE'); ?></th>
                        	<!--<th width="10%"><?php print JText::_('JSTATUS'); ?></th> -->
                        	<th width="10%"><?php print JText::_('Action'); ?></th>
                        </tr>
                    </thead>
                    <tfoot>
                    	<tr>
                        	<td colspan="4">&nbsp;</td>
                        </tr>
                    </tfoot>
                    <tbody>
						<?php foreach($items as $key => $item) : ?>
							<tr class="row<?php print ($key % 2) ? 1 : 0;?>">
                            	<td>
                                	<?php if ($userStatus < 1) : ?>
                                	<a href="<?php print JRoute::_('index.php?option=com_ariadmin&view=ariquestion&layout=edit&id='.$id.'&QuizId='.$qid.'&Questionid='.$item->QuestionId);?>">
                                    <?php endif; ?>
										<?php print strip_tags($item->Question); ?>
                                	<?php if ($userStatus < 1) : ?>
                                    </a>
                                    <?php endif; ?>
                                </td>
                            	<!--<td class="center"><?php echo JHtml::_('jgrid.published', $item->Status, $i);?></td> -->
                            	<td class="center">
                                	<?php if ($userStatus < 1) : ?>
                                	<a href="<?php echo JRoute::_('index.php?option=com_ariadmin&view=ariquestions&layout=edit&task=delete&id='.$id.'&QuizId='.$qid.'&Questionid='.$item->QuestionId);?>">
                                    <?php endif; ?>
										<?php echo JText::_('Delete');?>
                                	<?php if ($userStatus < 1) : ?>
                                    </a>
                                    <?php endif; ?>
                                </td>
							</tr>
							<?php
                        endforeach;
                        ?>
        			</tbody>
        		</table>
        	</form>
        </div>
        <?php
	}
	
	function editQuestion($id, $option, $view) {
		global $Itemid, $mainframe, $jlistConfig, $jlistTemplates, $task, $database, $user;
		
		$user = &JFactory::getUser();
		$editor1 =& JFactory::getEditor();
		$editor2 =& JFactory::getEditor();
		$editor3 =& JFactory::getEditor();
		$editor4 =& JFactory::getEditor();
		$document = &JFactory::getDocument();
		
		JHTML::_("behavior.mootools");
		$document = &JFactory::getDocument();
		$document->addScriptDeclaration("
			document.addEvent('domready', function(){
				$('adminForm').addEvent('submit', function(e) {
					var title = tinyMCE.get('Question').getContent()!= '';
					var title = tinyMCE.get('Question_ar').getContent()!= '';
					if (title == '') {
					new Event(e).stop();
					alert('Question should not be empty.');
					}
				});
			});"
		);
		
		$qid = JRequest::getCmd('QuizId');
		$qsid = JRequest::getCmd('Questionid');
		
		$breadcrumbs =& $mainframe->getPathWay();
		
		$query = $database->getQuery(true);
		
		$query->select('QC.CategoryId, QC.CategoryName, Q.QuizName, Q.QuizName_ar, Q.QuizId, Q.Status, Q.TotalTime, Q.PassedScore, Q.QuestionCount, Q.QuestionTime, Q.Description, Q.Description_ar');
		$query->from('#__ariquiz as Q');
		$query->leftJoin('#__ariquizquizcategory as QQC ON Q.QuizId = QQC.QuizId');
		$query->leftJoin('#__ariquizcategory as QC ON QC.CategoryId = QQC.CategoryId');
		$query->where('Q.QuizId = ' . (int)$qid);
		
		$database->setQuery($query);
		$item = $database->loadObject();
		
		$title = strip_tags($item->QuizName);
		$length = 20;
		if (mb_strlen($title) > $length) :
			$title = mb_substr($title, 0, $length) . '...';
		endif;
		$breadcrumbs->addItem($title, JRoute::_('index.php?option=com_ariadmin&view=ariquestions&id='.$id.'&QuizId='.$qid));
		
		//////
		$query = $database->getQuery(true);
		
		$query->select("QuestionTypeId, QuestionType, `Default`");
		$query->from("#__ariquizquestiontype");
		$query->order("`Default` DESC, QuestionType ASC");
		$database->setQuery($query);
		$questionlists = $database->loadObjectList();
		foreach($questionlists as $questionlist) {
			$questionTypeList[] = JHTML::_('select.option', $questionlist->QuestionTypeId, $questionlist->QuestionType);
		}
		
		////
		
		$query = $database->getQuery(true);
		
		$query->select('SQ.QuestionId, SQ.QuestionVersionId, SQ.QuestionIndex, SQ.Status, SQV.QuestionTime, SQV.QuestionTime, SQV.Score, SQV.QuestionTypeId, SQV.Data, SQV.Question, SQV.Question_ar, SQV.Description, SQV.Description_ar');
		$query->from('#__ariquizquestion as SQ');
		$query->leftJoin('#__ariquizquestionversion as SQV ON SQ.QuestionVersionId = SQV.QuestionVersionId');
		$query->where('SQ.QuizId = '.(int)$qid.' AND SQ.QuestionId = '.(int)$qsid);
		
		$database->setQuery($query);
		$item = $database->loadObject();
		
		$title = strip_tags($item->Question);
		$length = 20;
		if (mb_strlen($title) > $length) :
			$title = mb_substr($title, 0, $length) . '...';
		endif;
		$breadcrumbs->addItem($title, JRoute::_('index.php?option=com_ariadmin&view=ariquestions&id='.$id.'&QuizId='.$qid));
		//print_r($item);
		?>
        <div class="igallery_form">
        	<form name="adminForm" id="adminForm" action="<?php print JRoute::_('index.php?option=com_ariadmin&view=ariquestions&id='.$id.'&Itemid='.$Itemid); ?>" method="post">
            	<ul class="adminformlist">
                	<li>
                    	<input type="hidden" class="readonly" value="<?php print $item->QuestionId; ?>" id="jform_QuestionId" name="jform[QuestionId]">
                    </li>
                    <li>
                        <label for="Question" id="Question-lbl"><?php print JText::_('COM_ARIADMIN_QUIZNAME_LABEL'); ?><font color="#990000">*</font>:</label>
                        <?php echo $editor1->display( 'Question',  $item->Question, '', '10', '80', '5', false, false ) ;?>
                    	<div class="clear"></div>
                    </li>
                    <li>
                        <label for="Question_ar" id="Question_ar-lbl"><?php print JText::_('COM_ARIADMIN_QUIZNAME_LABEL_AR'); ?><font color="#990000">*</font>:</label>
                        <?php echo $editor2->display( 'Question_ar',  $item->Question_ar, '', '10', '80', '5', false, false ) ;?>
                    	<div class="clear"></div>
                    </li>
                    <li>
                        <label for="Description" id="Description-lbl">Description (English):</label>
                        <?php echo $editor3->display( 'Description',  $item->Description, '', '10', '80', '5', false, false ) ;?>
                    	<div class="clear"></div>
                    </li>
                    <li>
                        <label for="Description_ar" id="Description_ar-lbl">Description (Arabic):</label>
                        <?php echo $editor4->display( 'Description_ar',  $item->Description_ar, '', '10', '80', '5', false, false ) ;?>
                    	<div class="clear"></div>
                    </li>
                    <!--<?php //if ($id == 1) : ?>
                    <li>
                        <label for="QuestionTypeId" id="QuestionTypeId-lbl"><?php print JText::_('COM_ARIADMIN_QUEST_TYPE_LABEL'); ?></label>
                        <?php
                        //$questionTypeList2[] = JHTML::_('select.option',1,'Single Question');
	                    //$questionTypeList2[] = JHTML::_('select.option',2,'Multiple Question');
						?>
                    	<?php //echo JHTML::_('select.genericlist', $questionTypeList2, 'QuestionTypeId', 'class="inputbox" ', 'value', 'text', $item->QuestionTypeId); ?>
                    </li>
                    <?php //else:?>-->
                    <li>
                        <label for="QuestionTypeId" id="QuestionTypeId-lbl"><?php print JText::_('COM_ARIADMIN_QUEST_TYPE_LABEL'); ?></label>
                    	<?php echo JHTML::_('select.genericlist', $questionTypeList, 'QuestionTypeId', 'class="inputbox" onChange="clearform();" ', 'value', 'text', $item->QuestionTypeId); ?>
                    </li>
                    <?php //endif; ?>
                    <?php if ($id != 1) : ?>
                    <li>
                        <label for="jform_QuestionTime" id="jform_QuestionTime-lbl"><?php print JText::_('COM_ARIADMIN_QUEST_SCORE_LABEL'); ?></label>
                        <input type="text" class="inputbox" value="<?php print $item->Score; ?>" id="jform_QuestionTime" name="jform[Score]">
                    </li>
                    <?php endif; ?>
                    <li id="quizAnswersArea">
                    	<label>Answers:</label>
                        <a href="#" class="addans">Add</a>
                        <br />
                        <?php $thisClass = ($item->QuestionTypeId != 4) ? 'display:block' : 'display:none'; ?>
                        <table class="anstable" id="anstable" cellpadding="0" cellspacing="0" width="70%" style=" <?php print $thisClass; ?>">
                        	<thead>
                                <tr style="line-height:22px;">
                                    <?php if ($id != 1) : ?>
                                    <th  width="3%">Correct</th>
                                    <?php else : ?>
                                    <th  width="3%">&nbsp;</th>
                                    <?php endif; ?>
                                    <th width="35%">English</th>
                                    <th width="35%">Arabic</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
								<?php $datas = simplexml_load_string($item->Data); ?>
                            <tbody id="ans_multilng">
								<?php
                                    $count = 0;
                                    foreach($datas as $key => $data) :
                                        $ans = $data->attributes();
                                        $answers = explode(',', $data[0]);
                                ?>
                                <tr id="qrow_<?php print $count; ?>">
                                	<?php if ($id != 1) : ?>
                                    <!--<td><input <?php print $item->QuestionTypeId == 1 ? 'type="radio"' : 'type="checkbox"'; ?><?php print ($ans['correct'] == 'true') ? ' checked="checked"' : ''; ?> value="<?php print $count; ?>" id="rbCorrect_<?php print $count; ?>" name="rbCorrect<?php print $ansid == 2 ? '[]' : ''; ?>" ></td>-->
                                    <td><input <?php print $item->QuestionTypeId == 1 ? 'type="radio"' : 'type="checkbox"'; ?><?php print ($ans['correct'] == 'true') ? ' checked="checked"' : ''; ?> value="<?php print $count; ?>" id="rbCorrect_<?php print $count; ?>" name="rbCorrect<?php print $item->QuestionTypeId == 2 ? '[]' : ''; ?>" ></td>
                                   <?php  $count1=$count; ?>
									<?php else : ?>
                                    <td>&nbsp;</td>
                                    <?php endif; ?>
                                    <td><input type="text" size="35" class="inputbox" name="tbxAnswer22[<?php print $count;?>]" id="tbxAnswer_<?php print $count; ?>" value="<?php print $answers[0]; ?>"></td>
                                    <td><input type="text" size="35" class="inputbox" name="tbxAnswer_ar[<?php print $count; ?>]" id="tbxAnswer_ar_<?php print $count; ?>" value="<?php print $answers[1]; ?>"></td>
                                    <td><a href="javascript:void(0); remove('qrow_<?php print $count; ?>')">Delete</a></td>
                                </tr>
                                    <?php $count++; endforeach; ?>
                        	</tbody>
                        </table>
                        <?php $thisClass2 = ($item->QuestionTypeId == 4) ? 'display:block' : 'display:none'; ?>
                        <table class="anstable" id="anstable2" cellpadding="0" cellspacing="0" width="70%" style=" <?php print $thisClass2; ?>">
                        	<thead>
                                <tr style="line-height:22px;">
                                    <?php if ($id != 1) : ?>
                                    <th  width="3%">Correct</th>
                                    <?php else : ?>
                                    <th  width="3%">&nbsp;</th>
                                    <?php endif; ?>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody id="ans_mix">
                                    <?php
                                    $count = 0;
                                    foreach($datas as $key => $data) :
                                        $answers = $data[0];
                                    ?>
                                <tr id="frow_<?php print $count; ?>">
                                    <td><input type="text" size="50" class="text_area" name="tbxAnswer[<?php print $count; ?>]" id="tbxAnswer_<?php print $key; ?>" value="<?php print $answers; ?>"></td>
                                	<td><a href="javascript:void(0); remove('frow_<?php print $count; ?>')">Delete</a></td>
                                </tr>
                                    <?php $count++; endforeach; ?>
                            </tbody>
                        </table>
                        <?php JHTML::_("behavior.mootools"); ?>
						<?php
						$inputType = '';
						$name = '';
						
						
						if ($ansid == 2) :
							$inputType = ' type=checkbox ';
							$name = ' name=rbCorrect[] ';
						else :
							$inputType = ' type=radio ';
							$name = ' name=rbCorrect ';
						$count2= $count1;						
//echo $count2;
						endif;
						
						$query = '';
						$selector = 'ans_multilng';
						
						if ($id == 1) :
						
						$query = "<tr><td>&nbsp;</td><td><input onblur=inputValue(this); type=text size=35 class=inputbox name=tbxAnswer[]></td><td><input onblur=inputValuear(this); type=text size=35 class=inputbox name=tbxAnswer_ar[]></td></tr>";
						else :
						$query = "<tr><td><input".$inputType.$name." value=".$count2." ></td><td><input onblur=inputValue(this); type=text size=35 class=inputbox name=tbxAnswer[]></td><td><input onblur=inputValuear(this); type=text size=35 class=inputbox name=tbxAnswer_ar[]></td></tr>";
						endif;
						
						$selector2 = 'ans_mix';
						
						$query2 = "<tr><td><input onblur=inputValueft(this); type=text size=50 class=text_area name=tbxAnswer[] ></td></tr>";
						if ($id == 1) {
						
						$query3 = "<td><input  type=text size=35 class=inputbox name=tbxAnswer22[]></td><td><input type=text size=35 class=inputbox name=tbxAnswer_ar[]></td></tr>";						
						$query4 = "<tr><td><input".$inputType.$name." value=".$varCount." ></td><td><input  type=text size=35 class=inputbox name=tbxAnswer22[]></td><td><input  type=text size=35 class=inputbox name=tbxAnswer_ar[]></td></tr>";
						
						}else{
						 $varCount=0;
						 $query3 = "<td><input  type=text size=35 class=inputbox name=tbxAnswer22[]></td><td><input  type=text size=35 class=inputbox name=tbxAnswer_ar[]></td></tr>";						
						$varCount++;
						$query4 = "<tr><td><input".$inputType.$name." value=".$varCount." ></td><td><input onblur=inputValue(this); type=text size=35 class=inputbox name=tbxAnswer[]></td><td><input onblur=inputValuear(this); type=text size=35 class=inputbox name=tbxAnswer_ar[]></td></tr>";	
						}
						if ($count2=="")
						{
						$count2=-1;
						}
						//echo 'count='.$count2;
                        $document->addScriptDeclaration("
						document.addEvent('domready', function(){
                            var ansType = $$('#QuestionTypeId').get('value');
                            var ansType2 = $$('#QuestionTypeId').get('value');
							var crrHtml = $$('#anstable').get('html');
							 var varCount=".$count2.";
							 var id=".$id.";
							$$('#QuestionTypeId').addEvent('change', function(){
								ansType = this.get('value');
							//alert(ansType);
								if (ansType == 4) {
									$$('#anstable').set('html', '');
									$$('#quizAnswersArea #anstable1').set('style', 'display:none');
									$$('#quizAnswersArea #anstable2').set('style', 'display:block');
								} else {
									$$('#anstable').set('html', crrHtml);
									$$('#quizAnswersArea').set('style', 'display:block');
									$$('#quizAnswersArea #anstable2').set('style', 'display:none');
									$$('#quizAnswersArea #anstable1').set('style', 'display:block');
								}
							});
							
							$$('a.addans').addEvent('click', function(e){
								new Event(e).stop();
								
								varCount++;
								//alert(varCount);
								//alert(id);
								if (ansType == '1' && id == '1')  {
									
									$('".$selector."').set('html',$('".$selector."').get('html')+'<tr><td></td>".$query3."');
								}
								if (ansType == '2' && id == '1')  {
									
									$('".$selector."').set('html',$('".$selector."').get('html')+'<tr><td></td>".$query3."');
								}else if (ansType == '1' && id == '2') {
									
									$('".$selector."').set('html',$('".$selector."').get('html')+'<tr><td><input type=radio name=rbCorrect value='+varCount+' ></td>".$query3."');
								}else if (ansType == '2') {
									$('".$selector."').set('html',$('".$selector."').get('html')+'".$query4."');	
								} else {
									$('".$selector2."').set('html',$('".$selector2."').get('html')+'".$query2."');
								}
							});
                        });
						
						function inputValue(obj) {
							obj.parentNode.innerHTML = '<input onblur=inputValue(this); type=text size=35 class=inputbox name=tbxAnswer22[] value='+obj.value+' >';
						
						}
						function inputValuear(obj) {
							obj.parentNode.innerHTML = '<input onblur=inputValue(this); type=text size=35 class=inputbox name=tbxAnswer_ar[] value='+obj.value+' >';
						}
						function inputValueft(obj) {
							obj.parentNode.innerHTML = '<input onblur=inputValueft(this); type=text size=50 class=inputbox name=tbxAnswer22[] value='+obj.value+' >';
						}
						function remove(obj) {
							document.getElementById(obj).innerHTML = '';
						}
						function clearform(){
							document.getElementById('ans_multilng').innerHTML = '';
						}
						"
						);
						?>
                    </li>
                    <li>
                    	<label>&nbsp;</label>
                		<input type="submit" value="Save" />
                                  <input name="submit" type="reset" onclick="javascript:history.go(-1)" value="Cancel"/>
                    </li>
                </ul>
                <div>
                    <input type="hidden" value="com_ariadmin" name="option">
                    <input type="hidden" value="ariquestions" name="view">
                    <input type="hidden" value="<?php print $qid; ?>" name="QuizId">
                    <input type="hidden" value="save" name="task">
                    <?php echo JHtml::_('form.token'); ?>
        		</div>
            </form>
        </div>
		<?php
	}
}
?>