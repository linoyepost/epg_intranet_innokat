<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

class AriAdminControllerAriAdmin extends JControllerForm
{
	protected $view_item = 'form';
	protected $view_list = 'categories';
	
	public function add()
	{
		if (!parent::add()) {
			$this->setRedirect($this->getReturnPage());
		}
	}
	
	public function cancel($key = 'a_id')
	{
		parent::cancel($key);
		
		$this->setRedirect($this->getReturnPage());
	}
	
	public function edit($key = null, $urlVar = 'a_id')
	{
		print 'd';
		exit;
		$result = parent::edit($key, $urlVar);

		return $result;
	}
	
	protected function postSaveHook(JModel &$model, $validData)
	{
		$task = $this->getTask();

		if ($task == 'save') {
			$this->setRedirect(JRoute::_('index.php?option=com_ariadmin&view=ariadmins&id=1', false));
		}
	}
	
	public function save($key = null, $urlVar = 'a_id')
	{

		$result = parent::save($key, $urlVar);

		// If ok, redirect to the return page.
		if ($result) {
			$this->setRedirect($this->getReturnPage());
		}

		return $result;
	}
}
