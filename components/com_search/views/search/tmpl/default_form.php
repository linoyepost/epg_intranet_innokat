<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_search
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord();
?>

<form id="searchForm" action="<?php echo JRoute::_('index.php?option=com_search');?>" method="post">
<table class="admintable" width="100%">
                             <tr>
                                <td align="right" width="15%"><strong><?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?></strong><td><td>
                                <input type="text" name="searchword" id="search-searchword" size="30" maxlength="<?php echo $upper_limit; ?>" value="<?php echo $this->escape($this->origkeyword); ?>" class="inputbox" />
		<button name="Search" onclick="this.form.submit()" class="button"><?php echo JText::_('COM_SEARCH_SEARCH');?></button>
		<input type="hidden" name="task" value="search" />
		<input type="hidden" name="Itemid" value="394" />
</td></tr>
<tr>
<td align="right"><strong><?php echo JText::_('COM_SEARCH_FOR');?></strong><td><td>
<?php echo $this->lists['searchphrase']; ?>
</td></tr>
<tr>
<td align="right"><strong><?php echo JText::_('COM_SEARCH_ORDERING');?></strong><td><td>
<?php echo $this->lists['ordering'];?>
</td></tr>	
	
<?php if ($this->params->get('search_areas', 1)) : ?>
		<tr>
<td align="right" valign="top"><strong><?php echo JText::_('COM_SEARCH_SEARCH_ONLY');?></strong><td><td>
		<?php foreach ($this->searchareas['search'] as $val => $txt) :
			$checked = is_array($this->searchareas['active']) && in_array($val, $this->searchareas['active']) ? 'checked="checked"' : '';
		?>
		<input type="checkbox" name="areas[]" value="<?php echo $val;?>" id="area-<?php echo $val;?>" <?php echo $checked;?> />
			<label for="area-<?php echo $val;?>">
				<?php echo JText::_($txt); ?>
			</label>
		<?php endforeach; ?>
	</td></tr>
	<?php endif; ?>
    		<tr>
 <tr>
<td colspan="2">&nbsp;</td></tr> 
<tr>           
<td align="center" colspan="4">
        <?php if (!empty($this->searchword)):?>
		<p><?php echo JText::plural('COM_SEARCH_SEARCH_KEYWORD_N_RESULTS', $this->total);?></p>
		<?php endif;?>
	</td></tr>
<?php if ($this->total > 0) : ?>

<td align="right" colspan="4"><strong><?php echo $this->pagination->getLimitBox(); ?></strong><td></tr>        

<?php endif; ?>
</table>
</form>