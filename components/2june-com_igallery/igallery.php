<?php
/**
* @package		Ignite Gallery
* @copyright	Copyright (C) 2010 Matthew Thomson. All rights reserved.
* @license		GNU/GPLv2
*/

defined('_JEXEC') or die('Restricted access');



//load the backend language file, theres only one language file for the component
$lang =& JFactory::getLanguage();
$lang->load('com_igallery', JPATH_ADMINISTRATOR);

jimport('joomla.application.component.controller');
require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'defines.php');

$task = JRequest::getCmd('task', 'display');
$view = JRequest::getCmd('view', 'category');
$id = JRequest::getCmd('id', 0);

if( strpos($task,'.') )
{
	$task = substr($task, strpos($task,'.') + 1);
}

$frontendTasks = array('display','reportImage','addHit','download','fbComments');
$taskMatch = false;
$frontend = false;

$mainframe = JFactory::getApplication();
if ($view == 'icategory') :
	if ($id != 0) :
		$breadcrumbs =& $mainframe->getPathWay();
		$breadcrumbs->addItem(JText::_('Edit Album'));
	else :
		$breadcrumbs =& $mainframe->getPathWay();
		$breadcrumbs->addItem(JText::_('Add Album'));
	endif;
elseif ($view == 'images') :
	$breadcrumbs =& $mainframe->getPathWay();
	$breadcrumbs->addItem(JText::_('Manage Images'));
elseif ($view == 'image') :
	$breadcrumbs =& $mainframe->getPathWay();
	$breadcrumbs->addItem(JText::_('Manage Images'));
endif;

foreach($frontendTasks as $key => $value)
{
	if($value == $task)
	{
		$taskMatch = true;
		break;
	}
}

if($taskMatch == true)
{
	if($task == 'display')
	{
		if($view == 'category')
		{
			$frontend = true;
		}
	}
	else
	{
		$frontend = true;
	}
}

if($frontend == true)
{
	$controller	= JController::getInstance('Ig', array('default_view'=>'category'));
	$controller->execute(JRequest::getCmd('task'));
	$controller->redirect();
}
else
{
	$app = JFactory::getApplication();
	$params =& $app->getParams();
	
	if($params->get('allow_frontend_creation', 0) == 0)
	{
		return JError::raiseWarning(404, JText::_('PLEASE_ENABLE_FRONTEND'));
	}
	
	$document =& JFactory::getDocument();
	$document->addStyleSheet(IG_HOST.'media/com_igallery/css/admin.css');
	
	$lang->load('', JPATH_ADMINISTRATOR);
	$lang->load('lib_joomla', JPATH_ADMINISTRATOR);
	
	jimport('joomla.html.toolbar');
	
	require_once(JPATH_ADMINISTRATOR.'/includes/toolbar.php');
	require_once(IG_ADMINISTRATOR_COMPONENT.DS.'igallery.php');
}
?>