<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');

class UserProfileModelUserDetail extends JModelItem
{
	protected $item;
	
	protected function populateState() 
	{
		$app = JFactory::getApplication();
		
		$id = JRequest::getInt('id');
		$this->setState('userdetail.id', $id);
		
		$params = $app->getParams();
		$this->setState('params', $params);
		parent::populateState();
	}
	
	public function getItem() 
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		if (!isset($this->item)) {
			$id = $this->getState('userdetail.id');
			
			$query = "SELECT u.id, u.username, u.name, u.designation, u.department, u.phone, u.cell, u.fax, u.descp, u.email, DATE_FORMAT(u.registerDate,'%d.%m.%Y') as registerDate, DATE_FORMAT(u.lastVisitDate,'%d.%m.%Y') as lastVisitDate";
			$query .= " FROM #__users u";
			$query .= " WHERE u.id = '" . $id . "'";
			$this->_db->setQuery($query);
			
			if (!$this->item = $this->_db->loadObject()) {
				$this->setError($this->_db->getError());
			}
		}
		return $this->item;
	}
}