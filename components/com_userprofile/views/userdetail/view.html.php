<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class UserProfileViewUserDetail extends JView
{
	function display($tpl = null) 
	{
		$app	= JFactory::getApplication();
		$pathway = $app->getPathway();
		
		$this->item = $this->get('item');
		$pathway->addItem($this->item->name, '');
		
		parent::display($tpl);
	}
}