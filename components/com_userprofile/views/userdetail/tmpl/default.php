<?php defined('_JEXEC') or die('Restricted access'); ?>
<h1><?php print $this->item->name; ?></h1>
<table width="100%" cellpadding="0" cellspacing="0" class="userdetail">
    <tbody>
    	<tr>
        	<th width="30%"><?php print JText::_('COM_USERPROFILE_DESIG'); ?></th>
        	<td><?php print $this->item->designation; ?></td>
        </tr>
    	<tr>
        	<th><?php print JText::_('COM_USERPROFILE_DEPT'); ?></th>
        	<td><?php print $this->item->department; ?></td>
        </tr>
    	<tr>
        	<th><?php print JText::_('COM_USERPROFILE_TELE'); ?></th>
        	<td><?php print $this->item->phone; ?></td>
        </tr>
    	<tr>
        	<th><?php print JText::_('COM_USERPROFILE_CELL'); ?></th>
        	<td><?php print $this->item->cell; ?></td>
        </tr>
    	<tr>
        	<th><?php print JText::_('COM_USERPROFILE_FAX'); ?></th>
        	<td><?php print $this->item->fax; ?></td>
        </tr>
    	<tr>
        	<th><?php print JText::_('COM_USERPROFILE_EMAIL'); ?></th>
        	<td><?php print $this->item->email; ?></td>
        </tr>
    	<tr>
        	<th colspan="2"><?php print JText::_('COM_USERPROFILE_DESCP'); ?></th>
        </tr>
    	<tr>
        	<td colspan="2"><?php print $this->item->descp; ?></td>
        </tr>
    </tbody>
</table>
<?php print_r($this->items); ?>