<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class UserProfileViewUserProfile extends JView
{
	function display($tpl = null) 
	{
		$this->items = $this->get('Items');;
		$this->pagination = $this->get('Pagination');
		
		parent::display($tpl);
		
		$this->setDocument();
		
	}
	
	function setDocument() {
	}
}