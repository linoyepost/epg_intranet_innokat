<?php defined('_JEXEC') or die('Restricted access'); ?>
<table width="100%" cellpadding="0" cellspacing="0" class="userprofile">
	<thead>
    	<tr>
        	<th><?php print JText::_('COM_USERPROFILE_NAME'); ?></th>
        	<th width="30%"><?php print JText::_('COM_USERPROFILE_DESIG'); ?></th>
        	<th width="30%" class="last"><?php print JText::_('COM_USERPROFILE_CONTACTNO'); ?></th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
        	<td colspan="3" class="last"><?php echo $this->pagination->getListFooter(); ?></td>
        </tr>
    </tfoot>
    <tbody>
    	<?php foreach($this->items as $i => $item): ?>
    	<tr class="row<?php print $i%2 ? 1 : 0; ?>">
        	<td><a href="<?php print JRoute::_("index.php?option=com_userprofile&view=userdetail&id=".$item->id); ?>"><?php print $item->name; ?></a></td>
        	<td><?php print $item->designation; ?></td>
        	<td class="last"><?php print $item->phone; ?><br /><?php print $item->cell; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
