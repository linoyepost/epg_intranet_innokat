<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class UserProfileController extends JController
{
	function display($cachable = false) 
	{
		JRequest::setVar('view', JRequest::getCmd('view', 'userprofile'));
		
		parent::display($cachable);
	}

}
