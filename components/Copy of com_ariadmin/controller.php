<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class AriAdminController extends JController
{
	function display($cachable = false) 
	{
		JRequest::setVar('view', JRequest::getCmd('view', 'AriAdmins'));
		
		parent::display($cachable);
	}

}
