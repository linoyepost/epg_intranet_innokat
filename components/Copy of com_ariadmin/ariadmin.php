<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

$document = JFactory::getDocument();
$document->addStyleSheet("media/com_igallery/css/admin.css");

$controller = JController::getInstance('AriAdmin');

$controller->execute(JRequest::getCmd('task'));

$controller->redirect();
