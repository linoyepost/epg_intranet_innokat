<?php
defined('_JEXEC') or die;

JHtml::_('behavior.formvalidation');

?>
<form action="<?php echo JRoute::_('index.php?option=com_ariadmin&view=ariadmin&layout=edit&id='.(int) $this->item->QuizId); ?>" 
	method="post" name="adminForm" id="ariadmin-form" class="form-validate">
    <fieldset class="adminform">
        <ul class="adminformlist">
            <li><?php echo $this->form->getLabel('QuizId'); ?>
            <?php echo $this->form->getInput('QuizId'); ?></li>
            
            <li><?php echo $this->form->getLabel('QuizName'); ?>
            <?php echo $this->form->getInput('QuizName'); ?></li>
            
            <li><?php echo $this->form->getLabel('QuizName_ar'); ?>
            <?php echo $this->form->getInput('QuizName_ar'); ?></li>
            
            <li><?php echo $this->form->getLabel('CategoryId'); ?>
            <?php echo $this->form->getInput('CategoryId'); ?></li>
            
            <li><?php echo $this->form->getLabel('Status'); ?>
            <?php echo $this->form->getInput('Status'); ?></li>
            
            <li><?php echo $this->form->getLabel('TotalTime'); ?>
            <?php echo $this->form->getInput('TotalTime'); ?></li>
            
            <li><?php echo $this->form->getLabel('PassedScore'); ?>
            <?php echo $this->form->getInput('PassedScore'); ?></li>
            
            <li><?php echo $this->form->getLabel('QuestionCount'); ?>
            <?php echo $this->form->getInput('QuestionCount'); ?></li>
            
            <li><?php echo $this->form->getLabel('QuestionTime'); ?>
            <?php echo $this->form->getInput('QuestionTime'); ?></li>
            
            <li><?php echo $this->form->getLabel('Description'); ?><div class="clear"></div></li>
            <li><?php echo $this->form->getInput('Description'); ?><div class="clear"></div></li>
            
            <li><?php echo $this->form->getLabel('Description_ar'); ?><div class="clear"></div></li>
            <li><?php echo $this->form->getInput('Description_ar'); ?><div class="clear"></div></li>
        </ul>
    </fieldset>
	<input type="submit" value="Save" />
	<div>
		<input type="hidden" name="option" value="com_ariadmin" />
		<input type="hidden" name="view" value="ariadmin" />
		<input type="hidden" name="task" value="save" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
