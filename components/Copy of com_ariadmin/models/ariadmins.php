<?php
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class AriAdminModelAriAdmins extends JModelList
{
	protected function populateState() 
	{
		$app = JFactory::getApplication();
		
		$aricategory = JRequest::getCmd('id');
		$this->setState('acid', $aricategory);
		
		parent::populateState();
	}
	
	public function getListQuery() 
	{
		$acid = $this->getState('acid');
		$query = $this->_db->getQuery(true);
		
		$query->select('QC.CategoryId,QC.CategoryName,Q.QuizName,Q.QuizName_ar,Q.QuizId,Q.Status');
		$query->from('#__ariquiz as Q');
		$query->leftJoin('#__ariquizquizcategory as QQC ON Q.QuizId = QQC.QuizId');
		$query->leftJoin('#__ariquizcategory as QC ON QC.CategoryId = QQC.CategoryId');
		$query->where('QC.CategoryId = ' . (int)$acid);
		
		return $query;
	}
}
