<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class IgController extends JController
{
	function __construct($config = array())
	{	
		$config['base_path'] = JPATH_SITE.DS.'components'.DS.'com_igallery';
		parent::__construct($config);
	}
	
	function display()
	{
		parent::display();
	}
}
