<?php
	defined('ARI_FRAMEWORK_LOADED') or die('Direct Access to this location is not allowed.');
	
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
	
	$quiz = $processPage->getVar('quiz');
	$ticketId = $processPage->getVar('ticketId');
	$arisI18N = $processPage->getVar('arisI18N');
	$option = $processPage->getVar('option');
	$task = $processPage->getVar('task');
	$canTakeQuiz = $processPage->getVar('canTakeQuiz');
	$Itemid = $processPage->getVar('Itemid');

	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	
	$title = $quiz->QuizName;
	$descp = $quiz->Description;
	if ($tag == 'ar-AA') :
		$title =$quiz->QuizName_ar;
		$descp = $quiz->Description_ar;
	endif;
	
	$database->setQuery("SELECT count(*) FROM #__ariquizstatisticsinfo WHERE `Status` = 'Finished' AND `QuizId` = '".$quiz->QuizId."' AND `UserId` = '".$user->id."'");
	$userStatus = $database->loadResult();
?>

<div style="margin: 4px 4px 4px 4px;">
	<form action="index.php" method="post">
	<h2><?php AriQuizWebHelper::displayDbValue($title); ?></h2>
	<?php AriQuizWebHelper::displayDbValue($descp, false); ?>
<?php
	$expDate = strtotime($quiz->ExpiryDate);
	$catgoryId = $quiz->CategoryList[0]->CategoryId;
	if ($expDate <= time()) {
		if ($catgoryId == 1) :
			print JText::_('ARI_SURVEYEXPIRED');
		else :
			print JText::_('ARI_QUIZEXPIRED');
		endif;
	} elseif ($userStatus > 0){
		if ($catgoryId == 1) :
			print JText::_('ARI_SURVEY_ALREADYTAKEN');
		else :
			print JText::_('ARI_QUIZ_ALREADYTAKEN');
		endif;
	} else {
		if ($canTakeQuiz)
		{
	?>
		<input type="submit" class="button" value="<?php AriQuizWebHelper::displayResValue('Label.Continue'); ?>" />
	<?php
		}
	}
?>
	<input type="hidden" name="tmpl" value="<?php echo JRequest::getString('tmpl', ''); ?>" />
	<input type="hidden" name="task" value="get_ticket" />
	<input type="hidden" name="quizId" value="<?php echo $quiz->QuizId; ?>" />
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
	</form>
</div>