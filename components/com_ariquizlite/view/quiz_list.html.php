<?php
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
	defined('ARI_FRAMEWORK_LOADED') or die('Direct Access to this location is not allowed.');

	$quizList = $processPage->getVar('quizList');
	$arisI18N = $processPage->getVar('arisI18N');
	$option = $processPage->getVar('option');
	$Itemid = $processPage->getVar('Itemid');
	
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
?>

<?php

if (!empty($quizList))
{
?>
<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">

    <?php
	$prevCategoryId = -1;
	foreach ($quizList as $quiz)
	{
		if ($quiz->CategoryId != $prevCategoryId)
		{

?>
<?php
		}
		$database->setQuery("SELECT * "
                . "\n FROM #__ariquiz "
                . "\n WHERE QuizId = " . $quiz->QuizId);
   
      
	   $ciruser = $database->loadObject();
	   
            $datetitle = 'Survey Ends on';
		$quizTitle = $quiz->QuizName;
		if ($tag == 'ar-AA') :
			$quizTitle = $quiz->QuizName_ar;
		$datetitle = 'إنهاء المسح على';
		endif;
		
		//$ExpiryDate =  JHtml::_('date', $ciruser->ExpiryDate, JText::_('DATE_FORMAT_LC5'));
		$ExpiryDate =date("d-m-Y", strtotime($ciruser->ExpiryDate)); 
		$Description = $ciruser->Description;
		if ($tag == 'ar-AA') :
			$Description = $ciruser->Description_ar;
		endif;
?>
	   <tr class="<?php echo "row$k"; ?>">
            <td valign="top" class="yellow-row">
			<div id="form-colm-two">
			<div class="form-colm-left">
			<div class="form-list-title"><a href="index.php?option=<?php echo $option; ?>&task=quiz&quizId=<?php echo $quiz->QuizId; ?>&Itemid=<?php echo $Itemid; ?>"><?php AriQuizWebHelper::displayDbValue($quizTitle); ?></a></div>
            <div class="form-list-decs"> 
			<span class="form-list-decs-head"><?php echo $datetitle; ?>: <?php echo $ExpiryDate; ?></span><br/><?php echo $Description; ?>
            </div>
            </div>
			<div class="form-colm-right">
<div class="sub_title"><?php //echo JText::sprintf('%s', JHtml::_('date', $ciruser->Created, JText::_('DATE_FORMAT_LC5'))); ?></div>
<div class="sub_title"><?php echo 'Published Date:' ?> <?php echo JText::sprintf('%s', JHtml::_('date', $ciruser->Created, JText::_('DATE_FORMAT_LC5'))); ?></div>
			<div>&nbsp;</div>            
        </tr>
<?php		
		$prevCategoryId = $quiz->CategoryId;
	}
?>
    </table>
<?php
}
else
{
	AriQuizWebHelper::displayResValue('Label.NotItemsFound');
}
?>