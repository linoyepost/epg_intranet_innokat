<?php
	defined('ARI_FRAMEWORK_LOADED') or die('Direct Access to this location is not allowed.');
	$language = &JFactory::getLanguage();
	$database = &JFactory::getDBO();
	$tag = $language->get('tag');
	$results = $processPage->getVar('results');
	$option =  $processPage->getVar('option');
	$Itemid = $processPage->getVar('Itemid');
	$catid = JRequest::getCmd('aricategory');
?>

<div class="staff_name_title"><?php AriQuizWebHelper::displayResValue('Title.QuizResultList'); ?></div>
<table width="100%" class="adminlist">
	<tr>
		<th class="sectiontableheader" width="10"><?php AriQuizWebHelper::displayResValue('Label.NumberPos'); ?></th>
		<th class="sectiontableheader"><?php AriQuizWebHelper::displayResValue('Label.Quiz'); ?></th>
		<th class="sectiontableheader" width="70"><?php AriQuizWebHelper::displayResValue('Label.StartDate'); ?></th>
		<th class="sectiontableheader" width="70"><?php AriQuizWebHelper::displayResValue('Label.EndDate'); ?></th>
        <?php if ($catid != 1) : ?>
		<th class="sectiontableheader" width="70"><?php AriQuizWebHelper::displayResValue('Label.Score'); ?></th>
		<th class="sectiontableheader" width="70"><?php AriQuizWebHelper::displayResValue('Label.Passed'); ?></th>
        <?php else : ?>
		<th class="sectiontableheader" width="70"><?php print JText::_('ARI_TOTALUSERS'); ?></th>
		<th class="sectiontableheader" width="100"><?php print JText::_('ARI_EXPORTRESULT'); ?></th>
        <?php endif; ?>
		<th class="sectiontableheader" width="50"><?php AriQuizWebHelper::displayResValue('Label.Details'); ?></th>
	</tr>
	<?php
		if (!empty($results))
		{
			$i = 0;
			$lblView = AriQuizWebHelper::getResValue('Label.View');
			$lblPassed = AriQuizWebHelper::getResValue('Label.Passed');
			$lblNoPassed = AriQuizWebHelper::getResValue('Label.NoPassed');
			foreach ($results as $row)
			{
	

	?>
	 <?php $database->setQuery("SELECT PassedScore FROM #__ariquizstatisticsinfo WHERE `TicketId` = '".$row->TicketId."'");
         //echo ("SELECT PassedScore FROM #__ariquizstatisticsinfo WHERE `TicketId` = '".$row->TicketId."'");      
			 $PassedScore = $database->loadResult(); 
		
		?>
	 <tr class="<?php echo 'sectiontableentry' . ($i % 2); ?>">
    	<td align="left"><?php echo ($i + 1); ?></td>
	 	<td><?php AriQuizWebHelper::displayDbValue($row->QuizName); ?></td>
	 	<td><?php echo JText::sprintf('%s', JHtml::_('date', ArisDate::formatDate($row->StartDate), JText::_('DATE_FORMAT_LC5'))); ?></td>
	 	<td><?php echo JText::sprintf('%s', JHtml::_('date', ArisDate::formatDate($row->EndDate), JText::_('DATE_FORMAT_LC5'))); ?></td>
        <?php if ($catid != 1) : ?>
	 	<td><?php echo $row->UserScore . ' / ' . $row->MaxScore; ?></td>
	 		<td><?php if ($row->UserScore >= $PassedScore)  { echo $lblPassed;}else {echo$lblNoPassed;} ?></td>
        <?php else : ?>
	 	<td align="right"><?php echo $row->TotalUsers; ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	 	<td align="center"><a href="index.php?option=com_ariadmin&task=results&aricategory=<?php print $catid; ?>&quizId=<?php echo $row->QuizId; ?>&Itemid=<?php echo $Itemid; ?>"><?php echo JText::_('ARI_EXPORT'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <?php endif; ?>
	 	<td align="center"><a href="index.php?option=<?php echo $option; ?>&task=quiz_result&aricategory=<?php print $catid; ?>&ticketId=<?php echo $row->TicketId; ?>&Itemid=<?php echo $Itemid; ?>"><?php echo $lblView; ?></a>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	 </tr>
	 <?php
		 		++$i;
			}
		} 
		else
		{
	 ?>
	 <tr>
	 	<td colspan="7" align="left"><?php AriQuizWebHelper::displayResValue('Label.NotItemsFound'); ?></td>
	 </tr>
	 <?php
		}
	 ?>
</table>