<?php

/**
 * @version		$Id: default.php 15 2009-11-02 18:37:15Z chdemko $
 * @package		Joomla16.Tutorials
 * @subpackage	Components
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @author		Christophe Demko
 * @link		http://joomlacode.org/gf/project/helloworld_1_6/
 * @license		License GNU General Public License version 2 or later
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$user = JFactory::getUser();
?>
<script>
window.addEvent('domready', function(e) {
	$('ctForm').submit();
});
</script>
<h1><?php echo $this->msg; ?></h1>
<form id="ctForm" action="http://intranet/web_survey/login/" method="post">

        <input type="hidden" name="UserId" value="<?php print $user->username; ?>" />

        <input type="hidden" name="Email" value="<?php print $user->email; ?>" />

        <input type="hidden" name="lang" value="en" />

        <input type="hidden" name="FullName" value="<?php print $user->name; ?>" />
        <input type="submit" style="display:none;" id="ctformbtn" />

    </form>
