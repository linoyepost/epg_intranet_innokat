<?php
function mainListMedias($option, $view ){
	global $jlistConfig, $Itemid, $mainframe, $page_title;
	$language = &JFactory::getLanguage();
	$tag = $language->get('tag');
	$user = &JFactory::getUser();
	$database = &JFactory::getDBO();
    $document=& JFactory::getDocument();
    $document->setTitle($page_title.' - '.JText::_('COM_EPOSTS_MAIN_MEDIAS_LIST_PAGE_TITLE'));
                            
    $can_upload = false;
    
    $html_form = makeHeader(JText::_('COM_EPOSTS_MAIN_MEDIAS_LIST_PAGE_TITLE'), true, false, false, 0, false, false, true, false, false, 0, 0, 0, 0, 0, 0, '', '');
	//echo $html_form;
	$html_form = '';  
	
    $footer = makeFooter(true, false, false, 0, 0, 0, 0, false, false, false);  
//////////////////////////////////////////////////
   $deptid = (int)JRequest::getString('deptid', 0);
   
   $deptquery = "";
 
   if($user->id ){
$deptquery .= " (media_access = 1 || media_department IN (SELECT department_id FROM #__eposts_department_members WHERE user_id = '".$user->id."')) ";
   }else{
	    $deptquery .= " media_access = 1 ";	 
   }
   
    $where = ' WHERE 1 = 1 && publish = 1 && '.$deptquery.' ';
  

    $search = JRequest::getVar('Searchbox', '');
	$announcements_expirydate = JRequest::getVar('announcements_expirydate', '');
	$deparrtment = JRequest::getVar('deparrtment', '');
	if ($search=="Search by Name"){
	$search ="";
	}
// echo  sprintf('%s',$announcements_expirydate,'DATE_FORMAT_LC5');   
   if (get_magic_quotes_gpc()) {
        $search = stripslashes( $search );
    }    
    if ( $search ) {
        $search =  $database->getEscaped( trim( mb_strtolower( $search )));
        $where .= "AND ( LOWER(media_name) LIKE '%".$search."%'";
        $where .= " OR LOWER(media_name_ar) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(media_description)) LIKE '%".$search."%'";
		$where .= " OR LOWER(fnStripTags(media_description_ar)) LIKE '%".$search."%' )";
    } elseif (( $announcements_expirydate ) &&( $deparrtment )) {
	$where .= "AND media_department = '".$deparrtment."'"; ?>
        $where .= " AND  publishedon LIKE '".$announcements_expirydate."%'";
	
	
} elseif (( $announcements_expirydate )) {
	
        $where .= " AND  publishedon LIKE '".$announcements_expirydate."%'";
	
	  
} elseif (( $deparrtment )) {
	
        $where .= "AND LOWER(media_department) = '".$deparrtment."'";
	
	}  	
    // echo ( "SELECT count(*) FROM #__eposts_dwnmedias".$where);
	
	$database->SetQuery( "SELECT count(*) FROM #__eposts_dwnmedias".$where);
   
	$total = $database->loadResult();   
   $limit = intval( JRequest::getInt( 'limit', 0 ) );
   $session = JFactory::getSession();
   if(!$limit){
	  $limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
   }else{
	   $session->set('limit', $limit);
   }
   $limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
    jimport('joomla.html.pagination'); 
    $pageNav = new JPagination( $total, $limitstart, $limit );
    if ($pageNav->limitstart != $limitstart){
       $session->set('jdlimitstart', $pageNav->limitstart);
        $limitstart = $pageNav->limitstart;
     }
    
   $query = "SELECT * FROM #__eposts_dwnmedias
              $where
              ORDER BY id DESC";
                                                                
    $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
    $rows = $database->loadObjectList();
    // get amount of members
    foreach ($rows as $row){
       $database->setQuery("SELECT departments_name "
                . "\n FROM #__eposts_departments "
                . "\n WHERE id = " . $row->media_department
            );
   
       $departments = $database->loadObject();
            $row->department = $departments->departments_name;
		if ($tag == 'ar-AA') :
				$row->department = $department[departments_name_ar];
			endif;
	   
	   $database->setQuery("SELECT name "
                . "\n FROM #__users "
                . "\n WHERE id = " . $row->publishedby
            );
   
       $ciruser = $database->loadObject();
            $row->publishedbyName = $ciruser->name;
    }    
    
?>
<?php
								JHTML::_('behavior.calendar');
								/*// for plugin
								 JPluginHelper::importPlugin('datepicker');	
								echo plgSystemDatePicker::calendar($row->news_newsdate, 'news_newsdate', 'news_newsdate', '%Y-%m-%d');*/
								 ?>
<form name="uploadForm" id="uploadForm" action="<?php echo $uri;?>" method="post">
	<table class="jd_upload_form main_filter" border="0" cellpadding="0" cellspacing="5" width="50%" >
	  	  <tr>
	  
	   	<td><input type="text" name="Searchbox" value="Search by Name"  class="text_area" alt="Search Criteria" autocomplete="off" onblur="if (this.value == '') {this.value = 'Search by Name';}" onfocus="if (this.value == 'Search by Name') {this.value = '';}"></td>
		<td><input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
		<td>
			<input class="text_area" type="text" name="announcements_expirydate" id="announcements_expirydate" value="" onclick="return showCalendar('announcements_expirydate','%Y-%m-%d');"></td>
			
				<td>
			<select name="deparrtment" class="inputbox" >
				<option value="" >Select departments </option>

				<?php

 

$database->setQuery("SELECT * "
                . "\n FROM #__eposts_departments "               
            );
   
       $listDepartments2 = $database->loadObjectList();

				foreach($listDepartments2 as $xdepartment2) { ?>
		
	<option value="<?php echo $xdepartment2->id; ?>"><?php echo $xdepartment2->departments_name;  ?></option>
			<?php	} ?>
			</select></td>
			<div class="select-office-dropdown"><td> <input class="selectofficebtn" type="submit" name="submit_go" value="<?php echo JText::_('COM_EPOSTS_GO_BTN')?>"/></td>
	    
 
  <?php
		  
		
	$form = '';
		$form .= '<tr>
             <td width="200px">&nbsp;</td>
			<td width="90" valign="middle"></td>
        
			
			';
  		/*	  <td><div class="select-office-dropdown">  
			   // build listbox with Offices
    	   $officess = array();
    	   $officess[] = JHTML::_('select.option', '0', JText::_('COM_EPOSTS_SELECT_OPTION') );
    	   $database->setQuery( "SELECT id AS value, offices_title AS text FROM #__eposts_offices" );
		   if ($tag == 'ar-AA') :
		      $database->setQuery( "SELECT id AS value, offices_title_ar AS text FROM #__eposts_offices" );
		   endif;
    	   $officess = array_merge( $officess, $database->loadObjectList() );
    	   $listbox_officess = JHTML::_('select.genericlist', $officess, 'resources_offices', 'size="1" class="inputbox"', 'value', 'text', $resources_offices );
   		  
		  $form .= $listbox_officess; */  
        $form .= '
               <div class="select-office-dropdown"><td> </td></div>
        </tr>';   
		
        $form .= '
            </table>';	

	echo $form;  ?>
 
     <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
      
       
 <?php  print '<div class="ofctitle">'.$row->department . '</div><br>';?>	   <?php
		if (count($rows)) :
        $k = 0;
        for($i=0, $n=count( $rows ); $i < $n; $i++) {
            $row = &$rows[$i];
			$deptlink = JRoute::_("index.php?option=com_eposts&view=mydepartment&cirid=".$row->media_department."&Itemid=103");
			$title = $row->media_name;
			$descp = $row->media_description;
			if ($tag == 'ar-AA') :
			$title = $row->media_name_ar;
			$descp = $row->media_description_ar;
			endif;
			$title = strip_tags($title);
			$extension = end(explode('.', $row->media_document));
			/*$length = 400;
			$descp = strip_tags($descp);
			if (mb_strlen($descp) > $length) :
				$descp = mb_substr($descp, 0, $length) . '...';
			endif;*/
            ?>
    
	   <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
			<div id="form-colm-two">
			<div class="form-colm-left">
			<div class="form-list-title"><?php echo $title;?></div>
            <div class="form-list-decs"> 
			<span class="form-list-decs-head"><?php echo JText::_('Description: '); ?></span><br/><?php echo $descp; ?>
            </div>
            </div>
			<div class="form-colm-right">
			<div class="sub_title"><?php echo JText::_('COM_EPOSTS_MAIN_PUBLISHEDBY'); ?> <?php echo $row->publishedbyName; ?></div>
			<div class="sub_title"><?php echo JText::_('Date'); ?> <?php echo JText::sprintf('%s', JHtml::_('date', $row->publishedon, JText::_('DATE_FORMAT_LC5'))); ?></div>
            <div class="sub_title"><div class="sub_title"><div class="form_download_icon"><a href="<?php print JRoute::_('index.php?option=com_eposts&view=empdownload&filename=' . $row->media_document); ?>">
   <?php if (($extension=="docx") or ($extension=="dot")or ($extension=="doc")or ($extension=="docm")or ($extension=="dotm")or ($extension=="dotx"))   { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-word.png" border="0" alt="" /></a></div></div>
   <?php }elseif ($extension=="pdf") { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-pdf.png" border="0" alt="" /></a></div></div>
  <?php }elseif (($extension=="xls") or ($extension=="xlt")or ($extension=="xlm")or ($extension=="xlsx")or ($extension=="xltx")or ($extension=="xltm"))   { ?>
   
   <img src="components/com_eposts/assets/images/icons/icon-excel.png" border="0" alt="" /></a></div></div>
   <?php }else { ?>
   
   <img src="components/com_eposts/assets/images/icons/forms-download.png" border="0" alt="" /></a></div></div>
   <?php } ?>
   </div></div>
			</div>
			</div>
			<div>&nbsp;</div>            
        </tr>
        <?php $k = 1 - $k;  } ?>
        <?php else : ?>
        <tr>
            <td align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
        </tr>
        <?php endif; ?>
        <tr class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo $pageNav->getPagesCounter(); ?></td>
          </tr>
    </table>