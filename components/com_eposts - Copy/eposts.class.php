<?php
/**
* @version 1.5
* @package JDownloads
* @copyright (C) 2009 www.eposts.com
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* 
*/

defined( '_JEXEC' ) or die( 'Restricted access-class' );



class jlist_config extends JTable{
	var $id = null;
	var $setting_name = null;
	var $setting_value = null;

	function jlist_config(&$db){
		parent::__construct('#__eposts_config', 'id', $db);
	}

}

class jlist_cats extends JTable{
	var $cat_id = null;
	var $cat_dir = null;
	var $parent_id = null;
	var $cat_title = null;
    var $cat_alias = null;
	var $cat_description = null;
	var $cat_pic = null;
	var $cat_access = null;
    var $cat_department_access = null;
    var $metakey = null;
    var $metadesc = null;
	var $jaccess = null;
    var $jlanguage = null;
    var $ordering = null;
	var $published = null;
	var $checked_out = null;
	var $checked_out_time = null;
    
	function jlist_cats(&$db){
		parent::__construct('#__eposts_cats', 'cat_id', $db);
	}

}

class jlist_files extends JTable{
	var $file_id = null;
	var $file_title = null;
    var $file_alias = null;
	var $description = null;
	var $description_long = null;
	var $file_pic = null;
    var $thumbnail = null;
    var $thumbnail2 = null;
    var $thumbnail3 = null;
    var $price = null;
	var $release = null;
    var $language = null;
    var $system = null;
	var $license = null;
	var $url_license = null;
    var $license_agree = null;
	var $update_active = null;
    var $cat_id = null;
    var $metakey = null;
    var $metadesc = null;
	var $size = null;
	var $date_added = null;
    var $file_date = null;
    var $publish_from = null;
    var $publish_to = null;
    var $use_timeframe = null;
	var $url_download = null;
    var $extern_file = null;
    var $extern_site = null;
    var $mirror_1 = null;
    var $mirror_2 = null;
    var $extern_site_mirror_1 = null;	
    var $extern_site_mirror_2 = null;
    var $url_home = null;
	var $author = null;
	var $url_author = null;
	var $created_by = null;
    var $created_id = null;
	var $created_mail = null;
	var $modified_by = null;
    var $modified_id = null;
  	var $modified_date = null;
    var $submitted_by = null;
    var $set_aup_points = null;    		
	var $downloads = null;
    var $custom_field_1 = null;
	var $custom_field_2 = null;
    var $custom_field_3 = null;
    var $custom_field_4 = null;
    var $custom_field_5 = null;
    var $custom_field_6 = null;
    var $custom_field_7 = null;
    var $custom_field_8 = null;
    var $custom_field_9 = null;
    var $custom_field_10 = null;
    var $custom_field_11 = null;
    var $custom_field_12 = null;
    var $custom_field_13 = null;
    var $custom_field_14 = null;
    var $jaccess = null;
    var $jlanguage = null;
    var $ordering = null;
	var $published = null;
	var $checked_out = null;
	var $checked_out_time = null;

	function jlist_files(&$db){
		parent::__construct('#__eposts_files', 'file_id', $db);
	}

}

class jlist_license extends JTable{
	var $id = null;
	var $license_title = null;
	var $license_text = null;
	var $license_url = null;
    var $jlanguage = null;
	var $checked_out = null;
	var $checked_out_time = null;
	
	function jlist_license(&$db){
		parent::__construct('#__eposts_license', 'id', $db);
	}
}

class jlist_templates extends JTable{
	var $id = null;
	var $template_name = null;
	var $template_typ = null;
	var $template_header_text = null;
    var $template_subheader_text = null;
    var $template_footer_text = null;
    var $template_text = null;
	var $template_active = null;
	var $locked = null;
    var $note = null;
    var $cols = null;
    var $checkbox_off = null;
    var $symbol_off = null;
    var $jlanguage = null;	
    var $checked_out = null;
	var $checked_out_time = null;
	
	function jlist_templates(&$db){
		parent::__construct('#__eposts_templates', 'id', $db);
	}
}



class jlist_bookingrequest extends JTable{
    var $id = null;
    var $bookingdate = null;
    var $from_h = null;
    var $from_m = null;
    var $to_h = null;
    var $to_m = null;
	var $booking_title = null;
    var $booking_description = null;
	var $booking_details_description = null;
    var $resourceid = null;
	var $resources_offices= null;
	var $booking_status = null;
	var $publishedon = null;
	var $publishedby = null;
    var $submittedto = null;
	var $approvedon = null;
	var $approvedby = null;
    
    function jlist_bookingrequest(&$db){
        parent::__construct('#__eposts_bookingrequest', 'id', $db);
    }
}

////////////////////////////////DEVELOPMENT PHASE III /////////////////////

class jlist_announcements extends JTable{
    var $id = null;
    var $announcements_name = null;
	var $announcements_name_ar = null;
    var $announcements_description = null;
	var $announcements_description_ar = null;
	var $announcements_expirydate = null;
	var $announcements_showontop = null;
	var $announcements_access = null;	
	var $announcements_department = null;
	var $publish = null;
	var $publishedon = null;
	var $publishedby = null;
    
    function jlist_announcements(&$db){
        parent::__construct('#__eposts_announcements', 'id', $db);
    }
}

class jlist_news extends JTable{
    var $id = null;
    var $news_name = null;
	var $news_name_ar = null;
    var $news_description = null;
	var $news_description_ar = null;
	var $news_longdescription = null;
	var $news_longdescription_ar = null;
	var $news_newsdate = null;
	var $news_access = null;	
	var $news_department = null;
	var $news_image = null;
	var $publish = null;
	var $publishedon = null;
	var $publishedby = null;
    
    function jlist_news(&$db){
        parent::__construct('#__eposts_news', 'id', $db);
    }
}

class jlist_visions extends JTable{
    var $id = null;
    var $visions_name = null;
	var $visions_name_ar = null;
    var $visions_description = null;
	var $visions_description_ar = null;
	var $visions_longdescription = null;
	var $visions_longdescription_ar = null;
	var $publishedon = null;
	var $publishedby = null;
    
    function jlist_visions(&$db){
        parent::__construct('#__eposts_visions', 'id', $db);
    }
}

class jlist_alerts extends JTable{
    var $id = null;
    var $alerts_name = null;
	var $alerts_name_ar = null;
    var $alerts_description = null;
	var $alerts_description_ar = null;
	var $alerts_longdescription = null;
	var $alerts_longdescription_ar = null;
	var $alerts_startdate = null;
	var $alerts_enddate = null;
	var $publishedon = null;
	var $publishedby = null;
    
    function jlist_alerts(&$db){
        parent::__construct('#__eposts_alerts', 'id', $db);
    }
}


class jlist_quotes extends JTable{
    var $id = null;
    var $quotes_name = null;
	var $quotes_name_ar = null;
	var $quotes_file = null;
    var $quotes_description = null;
	var $quotes_description_ar = null;
	var $publish = null;
	var $publishedon = null;
	var $publishedby = null;
    
    function jlist_quotes(&$db){
        parent::__construct('#__eposts_quotes', 'id', $db);
    }
}


class jlist_policies extends JTable{
    var $id = null;
    var $policies_name = null;
	var $policies_name_ar = null;
	var $policies_plicydate = null;
	var $policies_access = null;	
	var $policies_department = null;
	var $policies_doc = null;
	var $policies_doc_ar = null;
	var $publish = null;
	var $publishedon = null;
	var $publishedby = null;
    
    function jlist_policies(&$db){
        parent::__construct('#__eposts_policies', 'id', $db);
    }
}

class jlist_manuals extends JTable{
    var $id = null;
    var $manuals_name = null;
	var $manuals_name_ar = null;
	var $manuals_manualdate = null;
	var $manuals_access = null;	
	var $manuals_department = null;
	var $manuals_doc = null;
	var $manuals_doc_ar = null;
	var $publish = null;
	var $publishedon = null;
	var $publishedby = null;
    
    function jlist_manuals(&$db){
        parent::__construct('#__eposts_manuals', 'id', $db);
    }
}

class jlist_faqs extends JTable{
    var $id = null;
    var $faqs_name = null;
	var $faqs_name_ar = null;	
    var $faqs_description = null;
	var $faqs_description_ar = null;
	var $publishedon = null;
	var $publishedby = null;
    
    function jlist_faqs(&$db){
        parent::__construct('#__eposts_faqs', 'id', $db);
    }
}

class jlist_benchmarks extends JTable{
    var $id = null;
    var $benchmarks_name = null;
	var $benchmarks_name_ar = null;	
    var $benchmarks_description = null;
	var $benchmarks_description_ar = null;
	var $benchmarks_department = null;
	var $publishedon = null;
	var $publishedby = null;
	var $publish = null;
    
    function jlist_benchmarks(&$db){
        parent::__construct('#__eposts_benchmarks', 'id', $db);
    }
}

class jlist_links extends JTable{
    var $id = null;
    var $links_name = null;
	var $links_name_ar = null;	
    var $links_link = null;
	var $links_link_ar = null;
	var $publishedon = null;
	var $publishedby = null;
    
    function jlist_links(&$db){
        parent::__construct('#__eposts_links', 'id', $db);
    }
}


//////////////////////////END DEVELOPMENT PHASE III //////////////////////

class jlist_tasawaqs extends JTable{
    var $id = null;
    var $tasawaqs_name = null;
	var $tasawaqs_name_ar = null;
    var $tasawaqs_description = null;
	var $tasawaqs_description_ar = null;
    var $tasawaqs_access = null;
	var $tasawaqs_price= null;
	var $tasawaqs_department = null;
	var $publishedon = null;
	var $publishedby = null;
    var $jlanguage = null;
    
    function jlist_tasawaqs(&$db){
        parent::__construct('#__eposts_tasawaqs', 'id', $db);
    }
}
	
class jlist_bookingslots extends JTable{
    var $id = null;
	var $bookingslots_title = null;
    var $bookingslots_title_ar = null;
	var $bookingslots_description = null;
    var $bookingslots_description_ar = null;
	var $publishedon = null;
	var $publishedby = null;
    var $jlanguage = null;
    
    function jlist_bookingslots(&$db){
        parent::__construct('#__eposts_bookingslots', 'id', $db);
    }
}

class jlist_resources extends JTable{
    var $id = null;
	var $resources_title = null;
    var $resources_title_ar = null;
	var $resources_description = null;
    var $resources_description_ar = null;
	var $resources_controller= null;
    var $resources_members = null;
	var $resources_offices = null;
	var $publishedon = null;
	var $publishedby = null;
    var $jlanguage = null;
    
    function jlist_resources(&$db){
        parent::__construct('#__eposts_resources', 'id', $db);
    }
}

class jlist_offices extends JTable{
    var $id = null;
	var $offices_title = null;
    var $offices_title_ar = null;
	var $offices_location = null;
    var $offices_location_ar = null;
	var $offices_phone= null;
    var $offices_description = null;
	var $offices_description_ar = null;
	var $publishedon = null;
	var $publishedby = null;
    var $jlanguage = null;
    
    function jlist_offices(&$db){
        parent::__construct('#__eposts_offices', 'id', $db);
    }
}

class jlist_events extends JTable{
    var $id = null;
    var $events_title = null;
	var $events_title_ar = null;
	var $events_location = null;
	var $events_location_ar = null;
	var $events_organized_by = null;
	var $events_organized_by_ar = null;
    var $events_startdate = null;
	var $events_enddate = null;
    var $events_description = null;
	var $events_description_ar= null;
    var $events_access = null;
	var $events_department = null;
	var $publish = null;
	var $publishedon = null;
	var $publishedby = null;
    var $jlanguage = null;
    
    function jlist_events(&$db){
        parent::__construct('#__eposts_events', 'id', $db);
    }
}
	
class jlist_newhires extends JTable{
    var $id = null;
    var $newhires_name = null;
	var $newhires_name_ar = null;
	var $newhires_designation = null;
	var $newhires_designation_ar = null;
    var $newhires_mobile = null;
	var $newhires_email = null;
    var $newhires_description = null;
	var $newhires_description_ar= null;
    var $newhires_access = null;
	var $newhires_picture = null;
	var $newhires_department = null;
	var $publish = null;
	var $publishedon = null;
	var $publishedby = null;
    var $jlanguage = null;
    
    function jlist_newhires(&$db){
        parent::__construct('#__eposts_newhires', 'id', $db);
    }
}

class jlist_dms extends JTable{
    var $id = null;
    var $dms_name = null;
	var $dms_name_ar = null;
    var $dms_description = null;
	var $dms_description_ar = null;
    var $dms_access = null;
	var $dms_title = null;
	var $dms_path= null;
	var $dms_file_data = null;
	var $dms_department = null;
	var $publishedon = null;
	var $publishedby = null;
	var $publish = null;
    var $jlanguage = null;
    
    function jlist_dms(&$db){
        parent::__construct('#__eposts_dms', 'id', $db);
    }
}

class jlist_circulars extends JTable{
    var $id = null;
    var $circulars_name = null;
	var $circulars_name_ar = null;
    var $circulars_description = null;
	var $circulars_description_ar = null;
    var $circulars_access = null;
	var $circulars_document= null;
	var $circulars_document_ar= null;
    var $circulars_date = null;
	var $circulars_department = null;
	var $publish = null;
	var $publishedon = null;
	var $publishedby = null;
    var $jlanguage = null;
    
    function jlist_circulars(&$db){
        parent::__construct('#__eposts_circulars', 'id', $db);
    }
}

class jlist_dwnmedias extends JTable{
    var $id = null;
    var $media_name = null;
	var $media_name_ar = null;
    var $media_description = null;
	var $media_description_ar = null;
    var $media_access = null;
	var $media_document= null;
	var $media_department = null;
	var $publishedon = null;
	var $publishedby = null;
	var $publish = null;
    var $jlanguage = null;
    
    function jlist_dwnmedias(&$db){
        parent::__construct('#__eposts_dwnmedias', 'id', $db);
    }
}

class jlist_departments extends JTable{
    var $id = null;
    var $departments_name = null;
	var $departments_name_ar = null;
	var $departments_email = null;
    var $departments_description = null;
	var $departments_description_ar = null;
    var $departments_access = null;
    var $departments_members = null;
	var $departments_map = null;
	var $publishedon = null;
	var $publishedby = null;
    var $jlanguage = null;
    
    function jlist_departments(&$db){
        parent::__construct('#__eposts_departments', 'id', $db);
    }
}

class jlist_log extends JTable{
    var $id = null;
    var $log_file_id = null;
    var $log_ip = null;
    var $log_datetime = null;
    var $log_user = null;
    var $log_browser = null;
    var $jlanguage = null;
    
    function jlist_log(&$db){
        parent::__construct('#__eposts_log', 'id', $db);
    }
}

class jlist_rating extends JTable{
    var $file_id = null;
    var $rating_sum = null;
    var $rating_count = null;
    var $lastip = null;
    var $jlanguage = null;
   
    function jlist_rating(&$db){
        parent::__construct('#__eposts_rating', 'file_id', $db);
    }
}

/** SS_ZIP class is designed to work with ZIP archives
@author Yuriy Horobey, smiledsoft.com
@email info@smiledsoft.com
*/
class ss_zip{
	/** contains whole zipfile
	@see ss_zip::archive()
	@see ss_zip::ss_zip()
	*/
	var $zipfile="";
	/** compression level	*/
	var $complevel=6;
	/** entry counter */
	var $cnt=0;
	/** current offset in zipdata segment */
	var $offset=0;
	/** index of current entry
		@see ss_zip::read()
	*/
	var $idx=0;
	/**
	ZipData segment, each element of this array contains local file header plus zipped data
	*/
	var $zipdata=array();
	/**	central directory array	*/
	var $cdir=array();
	/**	constructor
	@param string zipfile if not empty must contain path to valid zip file, ss_zip will try to open and parse it.
	If this parameter is empty, the new empty zip archive is created. This parameter has no meaning in LIGHT verion, please upgrade to PROfessional version.
	@param int complevel compression level, 1-minimal compression, 9-maximal, default is 6
	*/
	function ss_zip($zipfile="",$complevel=6){
		$this->clear();
		if($complevel<1)$complevel=1;
		if($complevel>9)$complevel=9;
		$this->complevel=$complevel;
		$this->open($zipfile);
	}

	/**Resets the objec, clears all the structures
	*/
	function clear(){
		$this->zipfile="";
		$this->complevel=6;
		$this->cnt=0;
		$this->offset=0;
		$this->idx=0;
		$this->zipdata=array();
		$this->cdir=array();
	}
		/**opens zip file.
		<center><hr nashade>*** This functionality is available in PRO version only. ***<br><a href='http://smiledsoft.com/demos/phpzip/' target='_blank'>please upgrade </a><hr nashade></center>
	This function opens file pointed by zipfile parameter and creates all necessary structures
	@param str zipfile path to the file
	@param bool append if true the newlly opened archive will be appended to existing object structure
	*/
	function open($zipfile, $append=false){}


	/**saves to the disc or sends zipfile to the browser.
	@param str zipfile path under which to store the file on the server or file name under which the browser will receive it.
	If you are saving to the server, you are responsible to obtain appropriate write permissions for this operation.
	@param char where indicates where should the file be sent
	<ul>
	<li>'f' -- filesystem </li>
	<li>'b' -- browser</li>
	</ul>
	Please remember that there should not be any other output before you call this function. The only exception is
	that other headers may be sent. See <a href='http://php.net/header' target='_blank'>http://php.net/header</a>
	*/
	function save($zipfile, $where='f'){
		if(!$this->zipfile)$this->archive();
		$zipfile=trim($zipfile);

		if(strtolower(trim($where))=='f'){
			 $this->_write($zipfile,$this->zipfile);
		}else{
			$zipfile = basename($zipfile);
			header("Content-type: application/octet-stream");
			header("Content-disposition: attachment; filename=\"$zipfile\"");
			print $this->archive();
		}
	}

	/** adds data to zip file
	@param str filename path under which the content of data parameter will be stored into the zip archive
	@param str data content to be stored under name given by path parameter
	@see ss_zip::add_file()
	*/
	function add_data($filename,$data=null){

		$filename=trim($filename);
		$filename=str_replace('\\', '/', $filename);
		if($filename[0]=='/') $filename=substr($filename,1);

		if( ($attr=(($datasize = strlen($data))?32:16))==32 ){
			$crc	=	crc32($data);
			$gzdata = gzdeflate($data,$this->complevel);
			$gzsize	=	strlen($gzdata);
			$dir=dirname($filename);
//			if($dir!=".") $this->add_data("$dir/");
		}else{
			$crc	=	0;
			$gzdata = 	"";
			$gzsize	=	0;

		}
		$fnl=strlen($filename);
        $fh = "\x14\x00";    // ver needed to extract
        $fh .= "\x00\x00";    // gen purpose bit flag
        $fh .= "\x08\x00";    // compression method
        $fh .= "\x00\x00\x00\x00"; // last mod time and date
		$fh .=pack("V3v2",
			$crc, //crc
			$gzsize,//c size
			$datasize,//unc size
			$fnl, //fname lenght
			0 //extra field length
		);


		//local file header
		$lfh="PK\x03\x04";
		$lfh .= $fh.$filename;
		$zipdata = $lfh;
		$zipdata .= $gzdata;
		$zipdata .= pack("V3",$crc,$gzsize,$datasize);
		$this->zipdata[]=$zipdata;
		//Central Directory Record
		$cdir="PK\x01\x02";
		$cdir.=pack("va*v3V2",
		0,
		$fh,
    	0, 		// file comment length
    	0,		// disk number start
    	0,		// internal file attributes
    	$attr,	// external file attributes - 'archive/directory' bit set
		$this->offset
		).$filename;

		$this->offset+= 42+$fnl+$gzsize;
		$this->cdir[]=$cdir;
		$this->cnt++;
		$this->idx = $this->cnt-1;
	}
	/** adds a file to the archive
	@param str filename contains valid path to file to be stored in the arcive.
	@param str storedasname the path under which the file will be stored to the archive. If empty, the file will be stored under path given by filename parameter
	@see ss_zip::add_data()
	*/
	function add_file($filename, $storedasname=""){
		$fh= fopen($filename,"r");
		$data=fread($fh,filesize($filename));
		if(!trim($storedasname))$storedasname=$filename;
		return $this->add_data($storedasname, $data);
	}
	/** compile the arcive.
	This function produces ZIP archive and returns it.
	@return str string with zipfile
	*/
	function archive(){
		if(!$this->zipdata) return "";
		$zds=implode('',$this->zipdata);
		$cds=implode('',$this->cdir);
		$zdsl=strlen($zds);
		$cdsl=strlen($cds);
		$this->zipfile=
			$zds
			.$cds
			."PK\x05\x06\x00\x00\x00\x00"
	        .pack('v2V2v'
        	,$this->cnt			// total # of entries "on this disk"
        	,$this->cnt			// total # of entries overall
        	,$cdsl					// size of central dir
        	,$zdsl					// offset to start of central dir
        	,0);							// .zip file comment length
		return $this->zipfile;
	}
	/** changes pointer to current entry.
	Most likely you will always use it to 'rewind' the archive and then using read()
	Checks for bopundaries, so will not allow index to be set to values < 0 ro > last element
	@param int idx the new index to which you want to rewind the archive curent pointer
	@return int idx the index to which the curent pointer was actually set
	@see ss_zip::read()
	*/
	function seek_idx($idx){
		if($idx<0)$idx=0;
		if($idx>=$this->cnt)$idx=$this->cnt-1;
		$this->idx=$idx;
		return $idx;
	}
	
	function read(){}
    	
	function remove($idx){}
    	
	function extract_data($idx){}

    function extract_file($idx,$path="."){}
    
	function _check_idx($idx){
		return $idx>=0 and $idx<$this->cnt;
	}
	function _write($name,$data){
		$fp=fopen($name,"w");
		fwrite($fp,$data);
		fclose($fp);
	}
}

?>