<?php defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<div class="gallery_wrpr">
<?php
$document =& JFactory::getDocument();

if( count($this->categoryChildren) && $this->showMenu == 1)
{
    echo $this->loadTemplate('menu');
}
if( !empty($this->photoList) ) {

	$this->thumbsVars = new stdClass();
	$this->thumbsVars->thumb_array = $this->thumbFiles;
	
	JHTML::_("behavior.mootools");
	$document->addStyleSheet(JURI::base() . 'components/com_igallery/cerabox.css');
	$document->addScript(JURI::base() . 'components/com_igallery/cerabox.min.js');
	$document->addScriptDeclaration("
		document.addEvent('domready', function() {
		
		
			$$('.thumb_container .thumbs_div a').cerabox({
				displayTitle: false
			});
		});
	");
	
	echo $this->loadTemplate('thumbs');
}

?>
</div>


