<?php defined('_JEXEC') or die('Restricted access'); ?>
<div style="float: left;" id="<?php echo $this->ratingsVars->prefix; ?>_alratings<?php echo $this->uniqueid; ?>">
<?php
$alPath = JPATH_SITE.DS.'components'.DS.'com_alratings'.DS.'classes'.DS.'alratings.php';
if( file_exists($alPath) )
{
	require_once($alPath);
	$url  = 'index.php?option=com_igallery&view=category&igid='.$this->category->id.'&image='.$this->photoList[0]->ordering;
	echo ALRatings::show('com_igallery', $this->photoList[0]->id, $this->photoList[0]->filename, $url);
}
else
{
	JError::raiseWarning(404, 'Please install AL Ratings or turn off ratings integration the profile settings.');
}
?>
</div>