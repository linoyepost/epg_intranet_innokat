<?php defined('_JEXEC') or die('Restricted access'); ?>

	<div id="<?php echo $this->desVars->prefix; ?>_des_container<?php echo $this->uniqueid; ?>" class="<?php echo $this->desVars->prefix; ?>_des_container" style="overflow: auto; width: <?php echo $this->desVars->des_container_width; ?>px; height: <?php echo $this->desVars->photo_des_height; ?>px;">

    <?php
    for ($i=0; $i<count($this->photoList); $i++)
    {
    $row = &$this->photoList[$i];
    $style = $i==0 ? '' : 'style="display: none"';
    
    $profileValue = $this->desVars->prefix == 'main' ? 'show_filename' : 'lbox_show_filename';
    $imgFileName = '';
    
    if($this->profile->{$profileValue} == 'full')
    {
    	$imgFileName = $row->filename;
    }
    	
    if($this->profile->{$profileValue} == 'no-id')
    {	
    	preg_match_all('/-[0-9]+/', $row->filename, $matches);
    	$imgFileName = str_replace($matches[0][0], '', $row->filename);
    }
    
    if($this->profile->{$profileValue} == 'no-ext')
    {	
    	$imgFileName = str_replace(array('.jpg','.jpeg','.gif','.png'), '', $row->filename);
    }
    
    if($this->profile->{$profileValue} == 'no-id-no-ext')
    {	
    	preg_match_all('/-[0-9]+/', $row->filename, $matches);
    	$imgFileName = str_replace($matches[0][0], '', $row->filename);
    	$imgFileName = str_replace(array('.jpg','.jpeg','.gif','.png'), '', $imgFileName);
    }
    
    ?>
		<div <?php echo $style; ?> class="des_div"><?php if( strlen($imgFileName) > 1){echo $imgFileName.' ';} ?><?php echo $row->description; ?></div>
	<?php
	}
	?>
	
	</div>

