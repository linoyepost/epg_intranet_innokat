<?php defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php //var_dump($this->dimensions);die; ?>

<div id="lbox_dark<?php echo $this->uniqueid; ?>" class="lbox_dark" style="display: none;" ></div>

<div id="lbox_white<?php echo $this->uniqueid; ?>" class="lbox_white" style="width:<?php echo $this->dimensions['galleryLboxWidth']; ?>px; display: none;" >

<?php
if($this->profile->lbox_close_position == 'above')
{
    ?>
    <img src="<?php echo IG_IMAGE_ASSET_PATH; ?>close.gif" id="closeImage<?php echo $this->uniqueid; ?>" class="closeImage" alt="close" />
    <?php
}

$this->thumbsVars->thumb_scrollbar = $this->profile->lbox_thumb_scrollbar;
$this->thumbsVars->images_per_row = $this->profile->lbox_images_per_row;
$this->thumbsVars->prefix = 'lbox';
$this->thumbsVars->arrows_left_right = $this->profile->lbox_arrows_left_right;
$this->thumbsVars->arrows_up_down = $this->profile->lbox_arrows_up_down;
$this->thumbsVars->thumb_array = $this->lboxThumbFiles;

if($this->profile->lbox_thumb_position == 'above' && $this->profile->lbox_show_thumbs == 1)
{
	$this->thumbsVars->thumb_container_width = $this->profile->lbox_thumb_container_width == 0 ? $this->dimensions['galleryLboxWidth'] : $this->profile->lbox_thumb_container_width;
	$this->thumbsVars->thumb_container_height = $this->profile->lbox_thumb_container_height;
    echo $this->loadTemplate('thumbs');
}

$this->desVars->prefix = 'lbox';

if($this->profile->lbox_photo_des_position == 'above' && $this->profile->lbox_show_descriptions == 1)
{
	$this->desVars->des_container_width = $this->profile->lbox_photo_des_width == 0 ? $this->dimensions['galleryLboxWidth'] : $this->profile->lbox_photo_des_width;
	$this->desVars->photo_des_height = $this->profile->lbox_photo_des_height == 0 ? 50 : $this->profile->lbox_photo_des_height;
	echo $this->loadTemplate('descriptions');
}

if($this->profile->lbox_photo_des_position == 'left' && $this->profile->lbox_show_descriptions == 1)
{   
    $this->desVars->des_container_width = $this->profile->lbox_photo_des_width == 0 ? 200 : $this->profile->lbox_photo_des_width;
	$this->desVars->photo_des_height = $this->profile->lbox_photo_des_height == 0 ? $this->dimensions['largestLboxHeight'] : $this->profile->lbox_photo_des_height;
	echo $this->loadTemplate('descriptions');
}

if($this->profile->lbox_thumb_position == 'left' && $this->profile->lbox_show_thumbs == 1)
{
    $this->thumbsVars->thumb_container_width = $this->profile->lbox_thumb_container_width;
	$this->thumbsVars->thumb_container_height = $this->profile->lbox_thumb_container_height == 0 ? $this->dimensions['largestLboxHeight'] : $this->profile->lbox_thumb_container_height;
	echo $this->loadTemplate('thumbs');
}

?>
		<div id="lbox_image_slideshow_wrapper<?php echo $this->uniqueid; ?>" class="lbox_image_slideshow_wrapper">
		
		<?php
		if($this->profile->lbox_show_slideshow_controls == 1 && $this->profile->lbox_slideshow_position == 'left-right')
		{
		?>
		<img src="<?php echo IG_IMAGE_ASSET_PATH; ?>left-side-arrow.png" id="lbox_slideshow_rewind<?php echo $this->uniqueid; ?>" alt="" style="float: left; padding-top: <?php echo $this->dimensions['largestLboxHeight']/2 - 15; ?>px; padding-bottom:<?php echo $this->dimensions['largestLboxHeight']/2 - 15; ?>px;"/>
		<?php
		}
		?>
		<div id="lbox_large_image<?php echo $this->uniqueid; ?>" class="lbox_large_image" style="width: <?php echo $this->dimensions['largestLboxWidth']; ?>px; height: <?php echo $this->dimensions['largestLboxHeight']; ?>px;" >
		</div>
		
		<?php
		if($this->profile->lbox_show_slideshow_controls == 1 && $this->profile->lbox_slideshow_position == 'left-right')
		{
		?>
		<img src="<?php echo IG_IMAGE_ASSET_PATH; ?>right-side-arrow.png" id="lbox_slideshow_forward<?php echo $this->uniqueid; ?>" alt="" style="float: left; padding-top: <?php echo $this->dimensions['largestLboxHeight']/2 - 15; ?>px; padding-bottom:<?php echo $this->dimensions['largestLboxHeight']/2 - 15; ?>px;"/>
		<?php
		}
		?>
		
		<div class="igallery_clear"></div>
		<?php
		if($this->profile->lbox_show_slideshow_controls == 1 && $this->profile->lbox_slideshow_position == 'below')
		{
			?>
			
			<div id="lbox_slideshow_buttons<?php echo $this->uniqueid; ?>" class="lbox_slideshow_buttons" >
				<img src="<?php echo IG_IMAGE_ASSET_PATH; ?>rewind.jpg" id="lbox_slideshow_rewind<?php echo $this->uniqueid; ?>" alt="" />
				<img src="<?php echo IG_IMAGE_ASSET_PATH; ?>play.jpg" id="lbox_slideshow_play<?php echo $this->uniqueid; ?>" alt="" />
				<img src="<?php echo IG_IMAGE_ASSET_PATH; ?>forward.jpg" id="lbox_slideshow_forward<?php echo $this->uniqueid; ?>" alt="" />
			</div>
		<?php
		}
		
		if($this->profile->lbox_show_numbering == 1)
		{
		   ?>
			<div id="lbox_img_numbering<?php echo $this->uniqueid; ?>" class="lbox_img_numbering">
				<span id="lbox_image_number<?php echo $this->uniqueid; ?>"></span>&#47;<?php echo count($this->photoList); ?>
			</div>
			<?php
		}

		if($this->profile->lbox_download_image != 'none')
    	{
    	   ?>
            <div id="lbox_download_button<?php echo $this->uniqueid; ?>" class="lbox_download_button" >
    	       <a href="#">
    	           <img src="<?php echo IG_IMAGE_ASSET_PATH; ?>btn_download.png" alt="" />
    	       </a>
    	   </div>
    	   <?php
    	}
    	
    	if($this->profile->lbox_share_facebook == 1)
		{
			?>
			<div id="lbox_facebook_share<?php echo $this->uniqueid; ?>" class="lbox_facebook_share">
				<iframe src="http://www.facebook.com/plugins/like.php?send=false&amp;layout=button_count&amp;width=70&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;href=" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:70px; height:21px;" allowTransparency="true"></iframe>
			</div>
			<?php
		}
		
		if($this->profile->lbox_plus_one == 1)
		{
			?>
			<div id="lbox_plus_one_div<?php echo $this->uniqueid; ?>" class="lbox_plus_one_div">
				<div id="lbox_plus_one_button<?php echo $this->uniqueid; ?>" ></div>
			</div>
			
			<?php
		}
		
		if($this->profile->lbox_report_image == 1)
		{
		   ?>
			<div id="lbox_report<?php echo $this->uniqueid; ?>" class="lbox_report" >
		       <a href="#" id="lbox_report_link"><?php echo JText::_( 'REPORT_IMAGE' ) ?></a>
		       <form action="index.php?option=com_igallery&amp;task=image.reportImage&id='<?php echo $this->photoList[0]->id; ?>&catid=<?php echo $this->category->id; ?>" method="post" name="lbox_report_form" id="lbox_report_form" style="display: none;">
					<textarea name="report_textarea" id="lbox_report_textarea<?php echo $this->uniqueid; ?>" rows="4" style="width: 100%;"></textarea>
					<input name="Itemid" type="hidden" value="<?php echo JRequest::getInt('Itemid'); ?>" />
					<input type="submit" value="<?php echo JText::_( 'JSUBMIT' ) ?>" />
				</form>
		    </div>
		<?php
		}
		?>
		
		</div>
		<?php

if($this->profile->lbox_thumb_position == 'right' && $this->profile->lbox_show_thumbs == 1)
{
    $this->thumbsVars->thumb_container_width = $this->profile->lbox_thumb_container_width;
	$this->thumbsVars->thumb_container_height = $this->profile->lbox_thumb_container_height == 0 ? $this->dimensions['largestLboxHeight'] : $this->profile->lbox_thumb_container_height;
	echo $this->loadTemplate('thumbs');
}

if($this->profile->lbox_photo_des_position == 'right' && $this->profile->lbox_show_descriptions == 1)
{
	$this->desVars->des_container_width = $this->profile->lbox_photo_des_width == 0 ? 200 : $this->profile->lbox_photo_des_width;
	$this->desVars->photo_des_height = $this->profile->lbox_photo_des_height == 0 ? $this->dimensions['largestLboxHeight'] : $this->profile->lbox_photo_des_height;
	echo $this->loadTemplate('descriptions');
}

if($this->profile->lbox_photo_des_position == 'below' && $this->profile->lbox_show_descriptions == 1)
{
    $this->desVars->des_container_width = $this->profile->lbox_photo_des_width == 0 ? $this->dimensions['galleryLboxWidth'] : $this->profile->lbox_photo_des_width;
	$this->desVars->photo_des_height = $this->profile->lbox_photo_des_height == 0 ? 50 : $this->profile->lbox_photo_des_height;
    echo $this->loadTemplate('descriptions');
}

if($this->profile->lbox_show_tags == 1)
{
	echo $this->loadTemplate('tags');
}

$this->ratingsVars->prefix = 'lbox';

if($this->profile->lbox_allow_rating == 2)
{
	echo $this->loadTemplate('alratings');
}

if($this->profile->lbox_thumb_position == 'below' && $this->profile->lbox_show_thumbs == 1)
{
    $this->thumbsVars->thumb_container_width = $this->profile->lbox_thumb_container_width == 0 ? $this->dimensions['galleryLboxWidth'] : $this->profile->lbox_thumb_container_width;
	$this->thumbsVars->thumb_container_height = $this->profile->lbox_thumb_container_height;
	echo $this->loadTemplate('thumbs');
}

$this->commentsVars->prefix = 'lbox';
$this->commentsVars->width = $this->dimensions['galleryLboxWidth'];

if($this->profile->lbox_allow_comments == 4)
{
	echo $this->loadTemplate('fbcomments');
}

if($this->profile->lbox_close_position == 'below')
{
    ?>
    <img src="<?php echo IG_IMAGE_ASSET_PATH; ?>close.gif" id="closeImage<?php echo $this->uniqueid; ?>" class="closeImage" alt="close" /><!-- close image -->
    <?php
}
?>
</div>