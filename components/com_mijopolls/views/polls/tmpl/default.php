<?php 
/**
* @version		1.0.0
* @package		MijoPolls
* @subpackage	MijoPolls
* @copyright	2009-2011 Mijosoft LLC, www.mijosoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		GNU/GPL based on AcePolls www.joomace.net
*
* Based on Apoll Component
* @copyright (C) 2009 - 2011 Hristo Genev All rights reserved
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @link http://www.afactory.org
*/

defined('_JEXEC') or die('Restricted access'); 

$language = &JFactory::getLanguage();
$tag = $language->get('tag');
?>

<script language="javascript" type="text/javascript">
	function tableOrdering(order, dir, task) {
	var form = document.adminForm;

	form.filter_order.value 	= order;
	form.filter_order_Dir.value	= dir;
	document.adminForm.submit(task);
}
</script>

<!--<h1><?php echo $this->params->get('page_title'); ?></h1> -->
<form action="<?php echo JRoute::_(JFactory::getURI()->toString()); ?>" method="post" name="adminForm">
	<table width="100%">
		<tr>
			<td nowrap="nowrap" class="main_filter">
				<?php if($this->params->get('show_filter_box')) : ?>
				<input type="text" name="search" value="<?php echo $this->lists['search'];?>" />
				<!--<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button> -->
				<?php endif; ?>
				<?php
			echo JText::_('COM_MIJOPOLLS_DISPLAY_NUM') .'&nbsp;';
			echo $this->pagination->getLimitBox();
		?>
			</td>
		</tr>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminlist">
		<thead>
			<tr>
				<td width="5" height="20" style="" class="sectiontableheader">
					<?php echo JText::_('Num'); ?>
				</td>
				<td class="sectiontableheader">
					<?php echo JText::_('Title'); ?>
				</td>
					<?php if($this->params->get('show_start_date')) { ?>
				<td width="15%" class="sectiontableheader" style="text-align:center;" nowrap="nowrap">
					<?php echo JText::_('COM_MIJOPOLLS_START'); ?>
				</td><?php } ?>						
				<?php if($this->params->get('show_end_date')) { ?>
				<td width="15%" class="sectiontableheader" style="text-align:center;" nowrap="nowrap">
					<?php echo JText::_('COM_MIJOPOLLS_END'); ?>
				</td><?php } ?>					
				<?php if($this->params->get('show_status')) { ?>
				<td width="5%" class="sectiontableheader" style="text-align:center;" nowrap="nowrap">
					<?php echo JText::_('COM_MIJOPOLLS_STATUS'); ?>
				</td><?php } ?>				
				<?php if($this->params->get('show_num_voters')) { ?>
				<td width="5%" class="sectiontableheader" style="text-align:center;" nowrap="nowrap">
					<?php echo JText::_('COM_MIJOPOLLS_VOTES'); ?>
				</td><?php } ?>
					<?php if($this->params->get('show_num_options')) { ?>
				<td width="5%" class="sectiontableheader" style="text-align:center;" nowrap="nowrap">
					<?php echo JText::_('COM_MIJOPOLLS_OPTIONS'); ?>
				</td><?php } ?>
			</tr>
		</thead>

		<tbody>
		<?php
		if (count($this->items)) :
		$k = 0;
		$n = count($this->items);
		for ($i=0; $i < $n; $i++) {
			$row = &$this->items[$i];
			
			//find the Itemid that correspondents to the link if any.
			$component 	=& JComponentHelper::getComponent('com_mijopolls');
			$menus		=& JApplication::getMenu('site', array());
			
			if (MijopollsHelper::is15()) {
				$menu_items	= $menus->getItems('componentid', $component->id);
			}
			else {
				$menu_items	= $menus->getItems('component_id', $component->id);
			}
			
			$itemid		= null;
			
			if (isset($menu_items)) {
				foreach ($menu_items as $item) {
					if ((@$item->query['view'] == 'poll') && (@$item->query['id'] == $row->id)) {
						$itemid = '&Itemid='.$item->id;
						break;
					}			
				}
			}
			
			$link = JRoute::_('index.php?option=com_mijopolls&view=poll&id='.$row->slug.$itemid);
			if ($tag == 'ar-AA') :
				$row->title = $row->title_ar;
			endif;
		
		?>
			<tr class="sectiontableentry<?php echo $k + 1; ?> row<?php echo ($k % 2) ? 1 : 0; ?>">
				<td>
					<?php echo $this->pagination->getRowOffset($i); ?>
				</td>
				<td>
						<a href="<?php echo $link;?>"><?php echo $row->title; ?></a>
				</td>
				<?php if ($this->params->get('show_start_date')) { ?>
				<td>
					<?php print JText::sprintf('%s', JHtml::_('date', $row->publish_up, JText::_('DATE_FORMAT_LC5'))); //JFactory::getDate($row->publish_up)->toFormat($this->params->get('date_format')); ?>
				</td>
				<?php } ?>	
				<?php if($this->params->get('show_end_date')) { ?>
				<td>
					<?php print JText::sprintf('%s', JHtml::_('date', $row->publish_down, JText::_('DATE_FORMAT_LC5'))); //JFactory::getDate($row->publish_down)->toFormat($this->params->get('date_format')); ?>
				</td>
				<?php } ?>				
				<?php if ($this->params->get('show_status')) { ?>
				<td align="center" style="text-transform:capitalize;">
					<?php 
					if ($this->params->get('show_status_as')) { ?>
						<img src="<?php echo JURI::base(); ?>media/mijopolls/images/poll-<?php echo $row->status; ?>.gif" />
					<?php 
					} else {
						echo JText::_('COM_MIJOPOLLS_'.$row->status);
					} ?>
				</td>			
				<?php 
				}
				if ($this->params->get('show_num_voters')) { ?>
				<td align="center">
					<?php echo $row->voters; ?>
				</td>
				<?php } ?>
				<?php if($this->params->get('show_num_options')) { ?>
				<td align="center">
					<?php echo $row->numoptions; ?>
				</td>
				<?php } ?>
			</tr>
			<?php $k = 1 - $k;
		}	?>
        <?php else : ?>
        <tr>
            <td colspan="7" align="center"><?php print JText::_('JGLOBAL_NOTFOUND'); ?></td>
        </tr>
        <?php endif; ?>
		</tbody>
	</table>
	<div>
		<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
	<div class="pagecounter">
		<?php echo $this->pagination->getPagesCounter(); ?>
	</div>

	<input type="hidden" name="option" value="com_mijopolls" />
	<input type="hidden" name="view" value="polls" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="" />
	<?php echo JHTML::_('form.token'); ?>
</form>
<br />