/*------------------------------------------------------------------------
# mod_newscalendar - News Calendar
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2010 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

function updateNewsCalendar(showhijri,curmonth,curyear,mid) 
{	
	var randNm = Math.random();
	var currentURL = window.location;
	var live_site = currentURL.protocol+'//'+currentURL.host+sfolder;
	
	var loading = document.getElementById('monthyear_'+mid);
	
	loading.innerHTML='<img src="'+live_site+'/modules/mod_newscalendar/assets/loading.gif" border="0" align="absmiddle" />';
	
	var ajax = new XMLHttpRequest;
   	ajax.onreadystatechange=function()
  	{
		if (ajax.readyState==4 && ajax.status==200)
		{
			document.getElementById("newscalendar"+mid).innerHTML = ajax.responseText;
		}
  	}	
	ajax.open("GET",live_site+"/modules/mod_newscalendar/assets/ajax.php?showhijri="+showhijri+"&month="+curmonth+"&year="+curyear+"&mid="+mid+"&num="+randNm,true);
	ajax.send();
}