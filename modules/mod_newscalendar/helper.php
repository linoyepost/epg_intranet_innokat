<?php
/*------------------------------------------------------------------------
# mod_newscalendar - News Calendar
# ------------------------------------------------------------------------
# author    Joomla!Vargas
# copyright Copyright (C) 2010 joomla.vargas.co.cr. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.vargas.co.cr
# Technical Support:  Forum - http://joomla.vargas.co.cr/forum
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die;

require_once JPATH_SITE.'/components/com_content/helpers/route.php';

jimport('joomla.application.component.model');

JModel::addIncludePath(JPATH_SITE.'/components/com_content/models', 'ContentModel');

abstract class modNewsCalendarHelper
{ 
    public static function getCal(&$params)
    {
		$cal = new JObject();
		
		$curmonth=(int) JRequest::getVar('month',($params->get("defmonth")?$params->get("defmonth"):date('n')));
		$curyear=(int) JRequest::getVar('year',($params->get("defyear")?$params->get("defyear"):date('Y')));
		 
		$dayofmonths=array(31,(!($curyear%400)?29:(!($curyear%100)?28:(!($curyear%4)?29:28)) ), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		
		$dayofmonth = $dayofmonths[$curmonth-1];
		$day_count = 1;
		$num = 0;

		$weeks = array();
		for($i = 0; $i < 7; $i++)
		{
			$a=floor((14-$curmonth)/12);
			$y=$curyear-$a;
			$m=$curmonth+12*$a-2;
			$dayofweek=($day_count+$y+floor($y/4)-floor($y/100)+floor($y/400)+floor((31*$m)/12)) % 7;
			$dayofweek = $dayofweek - 1 - $params->get("firstday");
			if($dayofweek <= -1) $dayofweek =$dayofweek + 7;


			if($dayofweek == $i)
			{
				$weeks[$num][$i] = $day_count.' 0';
				$day_count++;
			}
			else
			{
				$weeks[$num][$i] = ($dayofmonths[$curmonth!=1?($curmonth-2):(11)]-($dayofweek-1-$i)).' 1';
			}
		}

		while(true)
		{
			$num++;
			for($i = 0; $i < 7; $i++)
			{
				if ($day_count > $dayofmonth) {
					$weeks[$num][$i] = ($day_count-$dayofmonths[$curmonth-1]).' 1';
				} elseif ($day_count <= $dayofmonth) {
					$weeks[$num][$i] = $day_count.' 0';
				}
				$day_count++;
	  
				if($day_count > $dayofmonth && $i==6) break;
			}
			if($day_count > $dayofmonth && $i==6) break;
		}
		
		if (!$params->get('ajaxed')) {
			$ajaxed = 0;	
		} else {
			$ajaxed = 1;	
		}
		
		$monthname = 'MOD_NEWSCALENDAR_MONTHNAME_' . $params->get( "submonthname" ) . '_' . $curmonth;
		$monthname = modNewsCalendarHelper::encode($monthname,$params->get('encode'),$ajaxed);
		
		//$cal->items = modNewsCalendarHelper::getList($params, $curmonth, $curyear ); //chheena
		$cal->items = modNewsCalendarHelper::getList($params, $curmonth, $curyear );
		$cal->hj_dates = modNewsCalendarHelper::getHijriDates($dayofmonth, $curmonth, $curyear, &$params);
		
		$cal->weeks = $weeks;
		$cal->curmonth = $curmonth;
		$cal->curyear = $curyear;
		$cal->monthname = $monthname;
		$cal->dayofmonths = $dayofmonths;
		$cal->ajaxed = $ajaxed;
		
		
		$cal->hj_curyear = $cal->hj_dates[1][2];
		$cal->hj_monthname = $cal->hj_dates[1][1];
		if($cal->hj_curyear != $cal->hj_dates[$dayofmonth][2]){
			  $cal->hj_curyear .= '-'.$cal->hj_dates[$dayofmonth][2];
		}
		if($cal->hj_monthname != $cal->hj_dates[$dayofmonth][1]){
			  $cal->hj_monthname .= '-'.$cal->hj_dates[$dayofmonth][1];
		}
		return $cal;
    }
	
	public static function getHijriDates($dayofmonth, $curmonth, $curyear, &$params){
       $gmonths = array(1=>$jan,2=>$feb,3=>$mar,4=>$apr,5=>$may,6=>$jun,7=>$jul,8=>$aug,9=>$sep,10=>$oct,11=>$nov,12=>$dec);
	  // Get Hijri month names
	  $muh = 'muharram';
	  $saf = 'safar';
	  $rb1 = 'rabi1';
	  $rb2 = 'rabi2';
	  $jm1 = 'jamada1';
	  $jm2 = 'jamada2';
	  $rjb = 'rajab';
	  $shb = 'shaban';
	  $ram = 'ramadan';
	  $shw = 'shawal';
	  $zqd = 'zulqada';
	  $zhj = 'zulhajj';
	  
	  $hmonths = array(1=>$muh,2=>$saf,3=>$rb1,4=>$rb2,5=>$jm1,6=>$jm2,7=>$rjb,8=>$shb,9=>$ram,10=>$shw,11=>$zqd,12=>$zhj);

	  $adjust = $params->get('adjust');
	  /*<param name="adjust" type="list" default="0" label="Hijri Date Adjustment" description="Adjusting Hijri Date based on moon-sighting.">
					  <option value="2">+2</option>
					  <option value="1">+1</option>
					  <option value="0">No</option>
					  <option value="-1">-1</option>
					  <option value="-2">-2</option>
				  </param>*/
	  
	  // Main Logic
	  date_default_timezone_set('UTC');
	  $outPutAr = array();
	  for($dayscount=1;$dayscount<=$dayofmonth; $dayscount++){
		$myDate = mktime(0,0,0,$curmonth,$dayscount,$curyear);
		
		$day = intval(gmdate("w",$myDate));
		$date = intval(gmdate("j",$myDate));
		$month = intval(gmdate("n",$myDate));
		$year = intval(gmdate("Y",$myDate));
		
		// Calculate hijri date
		if($adjust == 0)
			$hijri = modNewsCalendarHelper::jd2hijri(modNewsCalendarHelper::greg2jd($date,$month,$year));
		else
		{
			$newdate = mktime(0,0,0,$month,$date+$adjust,$year);
			$ndate = intval(gmdate("j",$newdate));
			$nmonth = intval(gmdate("n",$newdate));
			$nyear = intval(gmdate("Y",$newdate));
			$hijri = modNewsCalendarHelper::jd2hijri(modNewsCalendarHelper::greg2jd($ndate,$nmonth,$nyear));
		}
		$hdate = $hijri[0];
		$hmonth = $hijri[1];
		$hyear = $hijri[2];
	  
	  
		$outPutAr[$dayscount]  = array($hdate,$hmonths[$hmonth], $hyear);
	  }
	   return $outPutAr;
	}
	
	public static function getList(&$params, $curmonth, $curyear)
	{
		
		//print_r($curmonth); 1
		//print_r($curyear); //2013

		$db = JFactory::getDbo();
	   	/*$db->setQuery("SELECT id, events_title, events_title_ar, events_startdate, events_enddate, DAY(events_startdate) AS stDay, MONTH(events_startdate) AS stMonth, YEAR(events_startdate) as stYear, DAY(events_enddate) AS enDay, MONTH(events_enddate) AS enMonth, YEAR(events_enddate) as enYear "
                . "\n FROM #__eposts_events "
				. "\n WHERE (MONTH(events_startdate) = $curmonth &&  YEAR(events_startdate) = $curyear)"
				. "\n  || (MONTH(events_enddate) = $curmonth &&  YEAR(events_enddate) = $curyear) "
            );*/
		     $db->setQuery("SELECT id, events_title, events_title_ar, events_startdate, events_enddate, DAY(events_startdate) AS stDay, MONTH(events_startdate) AS stMonth, YEAR(events_startdate) as stYear, DAY(events_enddate) AS enDay, MONTH(events_enddate) AS enMonth, YEAR(events_enddate) as enYear "
                . "\n FROM #__eposts_events "
                . "\n WHERE publish = '1' AND events_access = '1'"
            );
       $myEvents = $db->loadAssocList();
       $myEventsOut = array();
	   if($myEvents){
		   for($i=0;$i<count($myEvents);$i++){
			if($myEvents[$i]['stDay'] == $myEvents[$i]['enDay'] && $myEvents[$i]['stMonth'] == $myEvents[$i]['enMonth'] && $myEvents[$i]['stYear'] == $myEvents[$i]['enYear'] && $myEvents[$i]['stMonth'] == $curmonth && $myEvents[$i]['stYear'] == $curyear){
					$myEventsOut[$myEvents[$i]['stDay']][] = $myEvents[$i];  
			}elseif($myEvents[$i]['stMonth'] == $myEvents[$i]['enMonth']&& $myEvents[$i]['stMonth'] == $curmonth && $myEvents[$i]['stYear'] == $myEvents[$i]['enYear'] && $myEvents[$i]['stYear'] == $curyear && $myEvents[$i]['stDay'] < $myEvents[$i]['enDay']){
					  for($d = $myEvents[$i]['stDay']; $d <= $myEvents[$i]['enDay']; $d++){
						 $myEventsOut[$d][] = $myEvents[$i]; 
					 }
			//}else{ 
			  }elseif($myEvents[$i]['stYear'] == $myEvents[$i]['enYear'] && $myEvents[$i]['stYear'] == $curyear){
			  //if($myEvents[$i]['stMonth'] == $curmonth || $myEvents[$i]['enMonth'] == $curmonth){
			  $dayofmonths=array(31,(!($curyear%400)?29:(!($curyear%100)?28:(!($curyear%4)?29:28)) ), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
			  $dayofcurrentmonth = $dayofmonths[$curmonth-1];
			  
			  if($myEvents[$i]['stMonth'] == $curmonth){
				  for($ho = $myEvents[$i]['stDay']; $ho <= $dayofcurrentmonth; $ho++){
						 $myEventsOut[$ho][] = $myEvents[$i]; 
					 }				  
			  }elseif($myEvents[$i]['enMonth'] == $curmonth){
				  for($jo = 1; $jo <= $myEvents[$i]['enDay']; $jo++){
						 $myEventsOut[$jo][] = $myEvents[$i]; 
					 }
			  }elseif($myEvents[$i]['stMonth'] != $curmonth && $myEvents[$i]['stMonth'] != $curmonth && $myEvents[$i]['stMonth'] < $curmonth && $myEvents[$i]['enMonth'] > $curmonth){
				   for($sdo = 1; $sdo <= $dayofcurrentmonth; $sdo++){
						 $myEventsOut[$sdo][] = $myEvents[$i]; 
					 }
			  }//else if phase 2
			  /*$dayofmonths=array(31,(!($curyear%400)?29:(!($curyear%100)?28:(!($curyear%4)?29:28)) ), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
			  if($curmonth-1 < 0){
				 $testCurrentMonth = 12; 
			  }else{
				  $testCurrentMonth = $curmonth-1;
			  }
			  
			  $dayofcurrentmonth = $dayofmonths[$testCurrentMonth];
			  
			  if($myEvents[$i]['stMonth'] == $curmonth){
				  for($ho = $myEvents[$i]['stDay']; $ho <= $dayofcurrentmonth; $ho++){
						 $myEventsOut[$ho][] = $myEvents[$i]; 
					 }				  
			  }elseif($myEvents[$i]['enMonth'] == $curmonth){
				  for($jo = 1; $jo <= $myEvents[$i]['enDay']; $jo++){
						 $myEventsOut[$jo][] = $myEvents[$i]; 
					 }
			  }elseif($myEvents[$i]['stMonth'] != $curmonth && $myEvents[$i]['stMonth'] != $curmonth){
				   for($sdo = 1; $sdo <= $dayofcurrentmonth; $sdo++){
						 $myEventsOut[$sdo][] = $myEvents[$i]; 
					 }
			  }//else if phase 2*/
			  //}
			}//end elseif
			     
		  }// end for events
		   
	   }//end if events
	  // print_r($myEventsOut);

	return $myEventsOut;
	}

	public static function encode($text,$encode,$ajaxed)
    {
		if ($encode!='UTF-8' && $ajaxed) { 
			$text=iconv("UTF-8", $encode, JText::_($text));
		}
		else {
			$text=JText::_($text);
		}
		return $text;
    }
	public static function greg2jd($d, $m, $y) {
		$jd = (1461 * ($y + 4800 + ($m - 14) / 12)) / 4 +
		(367 * ($m - 2 - 12 * (($m - 14) / 12))) / 12 -
		(3 * (($y + 4900 + ($m - 14) / 12) / 100 )) / 4 +
		$d - 32075;
		return $jd;
	}
	
	public static function jd2hijri($jd) {
		$jd = $jd - 1948440 + 10632;
		$n = (int) (($jd - 1) / 10631);
		$jd = $jd - 10631 * $n + 354;
		$j = ((int) ((10985 - $jd) / 5316)) *
		((int) (50 * $jd / 17719)) +
		((int) ($jd / 5670)) *
		((int) (43 * $jd / 15238));
		$jd = $jd - ((int) ((30 - $j) / 15)) *
		((int) ((17719 * $j) / 50)) -
		((int) ($j / 16)) *
		((int) ((15238 * $j) / 43)) + 29;
		$m = (int) (24 * $jd / 709);
		$d = $jd - (int) (709 * $m / 24);
		$y = 30 * $n + $j - 30;
		return array((int)$d, $m, $y);
	}
}
