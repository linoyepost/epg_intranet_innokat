<?php
defined('_JEXEC') or die;

$app		= JFactory::getApplication();
$doc		=& JFactory::getDocument();

JHtml::_('behavior.mootools');
$doc->addStyleSheet(JURI::root(true).'/modules/mod_aweather/style.css');
$doc->addScript(JURI::root(true).'/modules/mod_aweather/acordion.js');


if ($params->get('aweather_layout')) :
function wheathercast($url) {
	$opts = array(
		'http' => array(
			'proxy' => 'tcp://10.1.1.111:8080',
			'request_fulluri' => true,
			'header' => "Proxy-Authorization: Basic $auth"
		)
	);
	$context = stream_context_create($opts);
	$result = file_get_contents($url, false, $context);
	//$result = file_get_contents($url);
	$xml = simplexml_load_string($result);
	$xml->registerXPathNamespace('yweather', 'http://xml.weather.yahoo.com/ns/rss/1.0');
	$location = $xml->channel->xpath('yweather:location');
	//return $xml;
	$location = $xml->channel->xpath('yweather:location');

	if(!empty($location)){
		foreach($xml->channel->item as $item){
			$current = $item->xpath('yweather:condition');
			$weather->forecast = $item->xpath('yweather:forecast');
			$weather->current = $current[0];
		}
	}
	return $weather;
	

}
endif;

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
require JModuleHelper::getLayoutPath('mod_aweather', $params->get('layout', 'default'));
