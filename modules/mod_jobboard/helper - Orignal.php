<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_login
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class modJobBoardHelper
{
	function getList()
	{
			$database = JFactory::getDbo();
			
			require_once "nusoap/lib/nusoap.php";
			$client = new nusoap_client("http://intranet/irecruitmentservice/WS_ApplicantManager.svc?wsdl", true);
			$error = $client->getError();
			if ($error) {
				echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
			}
			$client->soap_defencoding = 'UTF-8';
			$result = $client->call("GetOpenJobs", array("candidateid" => "", "TargetApplicants"=>"Public"));
			if(count($result) > 0){
				
			$output = simplexml_load_string($result['GetOpenJobsResult'], NULL, LIBXML_NOCDATA);
			$limit = 5;	
			$i = 0;	
			foreach ($output->vaccancyinforow as $item) {
				if($i >= $limit) continue;
				echo 'VACCANCYCODE'.((string)$item->VACCANCYCODE);
				echo 'JOBTITLE'.((string)$item->JOBTITLE);
				echo 'COMPANY'.((string)$item->COMPANY);
				echo 'DEPARTMENT'.((string)$item->DEPARTMENT);
				echo 'LOCATION'.((string)$item->LOCATION);
				echo 'CLOSEDATE'.((string)$item->CLOSEDATE);
				$i++;
			}
			
			}else {
			 echo "Sorry currently there is no open vacancy.";
			}
                                        
			
			$post_data = '<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <GetOpenJobs xmlns="http://tempuri.org/">
	  <staffId>TargetApplicants.Internal</staffId>
      <TargetApplicants>TargetApplicants.Internal</TargetApplicants>
    </GetOpenJobs>
  </soap12:Body>
</soap12:Envelope>';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,            'http://intranet/irecwstest/WS_ApplicantManager.svc/' );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($ch, CURLOPT_POST,           1 );
		curl_setopt($ch, CURLOPT_POSTFIELDS,     $post_data ); 
		curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/soap+xml', 'charset=utf-8')); 
		$result = curl_exec ($ch);	
		
		/*//Output
		$result = '<?xml version="1.0" encoding="UTF-8"?> <vaccancyinfo> 
		<vaccancyinforow> 
		<VACCANCYCODE><![CDATA[VC0230113025347]]></VACCANCYCODE> 
		<JOBTITLE><![CDATA[Senior Project Manager - Data Warehousing / ETL, BO]]></JOBTITLE> 
		<COMPANY><![CDATA[Emirates Post]]></COMPANY> 
		<DEPARTMENT><![CDATA[IT]]></DEPARTMENT> 
		<LOCATION><![CDATA[Dubai]]></LOCATION> 
		<CLOSEDATE><![CDATA[2/14/2013 12:00:00 AM]]></CLOSEDATE> 
		<JOBDESCRIPTION><![CDATA[job description one ]]></JOBDESCRIPTION> 
		<JOBGRADE><![CDATA[Grade 14]]></JOBGRADE> 
		<JOBEDUCATION><![CDATA[Bachelors]]></JOBEDUCATION> 
		<JOBAGEFRM><![CDATA[35]]></JOBAGEFRM> 
		<JOBAGETO><![CDATA[40]]></JOBAGETO> 
		<JOBSALARYFRM><![CDATA[20000]]></JOBSALARYFRM> 
		<JOBSALARYTO><![CDATA[30000]]></JOBSALARYTO> 
		<JOBSKILLS><![CDATA[PMP]]></JOBSKILLS> 
		<JOBBENEFITS><![CDATA[Car Allowance- HRA]]></JOBBENEFITS> 
		<JOBPOSITIONS><![CDATA[1]]></JOBPOSITIONS> 
		<JOBTARGET><![CDATA[2]]></JOBTARGET> 
		<JOBEXPERIENCEFROM><![CDATA[10]]></JOBEXPERIENCEFROM> 
		<JOBEXPERIENCETO><![CDATA[15]]></JOBEXPERIENCETO> 
		<JOBNATIONALITY><![CDATA[United Arab Emirates]]></JOBNATIONALITY> 
		</vaccancyinforow> 
		<vaccancyinforow> 
		<VACCANCYCODE><![CDATA[VC0230113025348]]></VACCANCYCODE>
<JOBTITLE><![CDATA[Junior Project Manager - Data Warehousing]]> </JOBTITLE> 
<COMPANY><![CDATA[Emirates Post]]></COMPANY> 
<DEPARTMENT><![CDATA[IT]]></DEPARTMENT> 
<LOCATION><![CDATA[Dubai]]></LOCATION> 
<CLOSEDATE><![CDATA[2/14/2013 12:00:00 AM]]></CLOSEDATE> 
<JOBDESCRIPTION><![CDATA[description of job]]></JOBDESCRIPTION> 
<JOBGRADE><![CDATA[Grade 14]]></JOBGRADE> 
<JOBEDUCATION><![CDATA[Bachelors]]></JOBEDUCATION> 
<JOBAGEFRM><![CDATA[25]]></JOBAGEFRM> 
<JOBAGETO><![CDATA[30]]></JOBAGETO> 
<JOBSALARYFRM><![CDATA[10000]]></JOBSALARYFRM> 
<JOBSALARYTO><![CDATA[20000]]></JOBSALARYTO> 
<JOBSKILLS><![CDATA[PMP]]></JOBSKILLS> 
<JOBBENEFITS><![CDATA[Car Allowance- HRA]]></JOBBENEFITS> 
<JOBPOSITIONS><![CDATA[1]]></JOBPOSITIONS> 
<JOBTARGET><![CDATA[2]]></JOBTARGET> 
<JOBEXPERIENCEFROM><![CDATA[5]]></JOBEXPERIENCEFROM> 
<JOBEXPERIENCETO><![CDATA[10]]></JOBEXPERIENCETO> 
<JOBNATIONALITY><![CDATA[United Arab Emirates]]></JOBNATIONALITY> 
</vaccancyinforow> 
</vaccancyinfo>';*/

		return	modJobBoardHelper::xml2tree2($result);


	}

}
