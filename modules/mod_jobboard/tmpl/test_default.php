<?php
// no direct access
defined('_JEXEC') or die;
$document = &JFactory::getDocument();
function downloadFile($filename) { // $file = include path 
        if(file_exists($filename)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filename));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filename));
            ob_clean();
            flush();
            readfile($filename);
            exit;
        }

    }
$jobboard_js = "
			document.addEvent('domready', function(){
				var slider02 = new anewsSlide({
					box: $('newhire_bx02'),
					items: $$('#newhire_bx02 .newhire_item'),
					autoPlay: 10000,
					interval: 10000,
					size: 464,
					handles: $$('#handles02 span'),
					onWalk: function(currentItem,currentHandle){
						this.handles.removeClass('active');
						currentHandle.addClass('active');
					}
				});
			});
	";
            require_once dirname(__FILE__).'/nusoap/lib/nusoap.php';
			$client = new nusoap_client("http://intranet/irecwstest/WS_ApplicantManager.svc?wsdl", true);
			$error = $client->getError();
			if ($error) {
				echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
			}
			$client->soap_defencoding = 'UTF-8';
			$result = $client->call("GetOpenJobs", array("candidateid" => "", "target"=>"Internal", "targetSpecified"=>"True" ));
?>
		<?php 
		
		 ?>

<div class="moduletable">
  <h3><?php print JText::_('EMIRATES_JOBBOARD'); ?></h3>
  <?php
	if(count($result) > 0){
			$output = simplexml_load_string($result['GetOpenJobsResult'], NULL, LIBXML_NOCDATA);
			//$output = array_reverse($output);
			if(count($output) > 1){
				$document->addScriptDeclaration($jobboard_js);
			}
			$limit = 5;	
			$i = 0;
			$count = 0;
  ?>
  <div id="handles02" class="handles">
  	<?php foreach ($output->vaccancyinforow as $item) : ?>
    <?php
	if($count >= $limit) continue;
	$count++;
    ?>
    <span><?php print $count; ?></span>
    <?php endforeach; ?>
  </div>
  <div class="newhire_cn">
    <div id="newhire_bx02" class="newhire_wrpr">
    <?php	
			foreach ($output->vaccancyinforow as $item) {
				if($i >= $limit) continue;
				$applylink = JRoute::_("index.php?option=com_eposts&view=mainshowjobs&page=home&cirid=".((string)$item->VACCANCYCODE).'&Itemid=303&jobtitle='.((string)$item->JOBTITLE));
				/*echo 'VACCANCYCODE'.((string)$item->VACCANCYCODE);
				echo 'JOBTITLE'.((string)$item->JOBTITLE);
				echo 'COMPANY'.((string)$item->COMPANY);
				echo 'DEPARTMENT'.((string)$item->DEPARTMENT);
				echo 'LOCATION'.((string)$item->LOCATION);
				echo 'CLOSEDATE'.((string)$item->CLOSEDATE);*/
				?>
      <?php
header("content-type: text/html; charset=utf-8");

?>
	  <div class="newhire_item item">
        <div class="newhire_details">
            <p><?php print JText::_('Reference Code: ') . ((string)$item->VACCANCYCODE); ?></p>
			<h4><?php print JText::_('Job Title: '); ?><a href="<?php echo $applylink; ?>"><?php echo utf8_encode(((string)$item->JOBTITLE)); ?></a></h4>
            <!--<p><?php print JText::_('Job Description: ') . utf8_encode(((string)$item->JOBDESCRIPTION)); ?></p> -->
            <p><?php print JText::_('Company: ') . utf8_encode(((string)$item->COMPANY)); ?></p>
            <p><?php print JText::_('Department: ') . utf8_encode(((string)$item->DEPARTMENT)); ?></p>
            <p><?php print JText::_('Grade: ') .utf8_encode(((string)$item->JOBGRADE)); ?></p>
            <p><?php print JText::_('Last Date: ') . date('d-m-Y', strtotime(((string)$item->CLOSEDATE))); ?></p>
            <p><?php print JText::_('Job Code: ') . utf8_encode(((string)$item->JOBCODE)); ?></p>
            <!--<p><?php print JText::_('More Details: ') . ''; ?></p>-->
            <p><?php print JText::_('Apply: '); ?><a href="<?php echo $applylink; ?>">Click Here</a></p> 
       <?php
	  // GetJobAttachment('JB8240712011059');
			
	//	echo $FileName;
	//	echo $FileExtension; 
				
	?>
        
        <?php 
	//	GetJobAttachment((string)$item->JOBCODE);
			
		//	{
		//if (isset($FileName)){
		//echo '<a href="#" onClick="'.downloadFile($FileName).'">Download here</a>';
   		//	}					
		//	}
		
		?>
        
        </div>
        <div class="clear"></div>
      </div>
				<?php
				$i++;
			}
}
?>
    </div>
  </div>
</div>
<div class="moduletable">
  <div class="controlls">
    <div class="container"><a class="readmore" href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistjobs&page=home&Itemid=303'); ?>"><span><?php print JText::_('EMIRATES_VIEWALL'); ?></span></a></div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>