
<?php
// no direct access
defined('_JEXEC') or die;
$document = &JFactory::getDocument();
$jobboard_js = "
			document.addEvent('domready', function(){
				var slider02 = new anewsSlide({
					box: $('newhire_bx02'),
					items: $$('#newhire_bx02 .newhire_item'),
					autoPlay: 10000,
					interval: 10000,
					size: 464,
					handles: $$('#handles02 span'),
					onWalk: function(currentItem,currentHandle){
						this.handles.removeClass('active');
						currentHandle.addClass('active');
					}
				});
			});
	";
            require_once dirname(__FILE__).'/nusoap/lib/nusoap.php';
			//$client = new nusoap_client("http://intranet/irecwstest/WS_ApplicantManager.svc?wsdl", true);
			$client = new nusoap_client("http://intranet/irecruitmentservice/WS_ApplicantManager.svc?wsdl", true);  // Production Webservice
			
			
			$error = $client->getError();
			if ($error) {
				echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
			}
			$client->soap_defencoding = 'UTF-8';
			$client->decode_utf8 = false;
			$client->encode_utf8 = true;
			
			
			if ($_REQUEST['lang']=="ar")
{
$lang='ar';	
}else{
	
$lang='en';	}
				$result = $client->call("GetOpenJobsByLanguage", array("candidateid" => "", "target"=>"Internal", "targetSpecified"=>"True" , "language"=>$lang ));
?>
		<?php 
		
		 ?>

<div class="moduletable">
  <h3><?php print JText::_('EMIRATES_JOBBOARD'); ?></h3>
  <?php
			
	if(count($result) > 0){
			$output = simplexml_load_string($result['GetOpenJobsByLanguageResult'], NULL, LIBXML_NOCDATA);
			//$output = array_reverse($output);
			if(count($output) > 1){
				$document->addScriptDeclaration($jobboard_js);
			}
			$limit = 5;	
			$i = 0;
			$count = 0;
  ?>
  
  <div id="handles02" class="handles">
  	<?php foreach ($output->vaccancyinforow as $item) : ?>
    <?php
	if($count >= $limit) continue;
	$count++;
    ?>
    <span><?php print $count; ?></span>
    <?php endforeach; ?>
  </div>
  <div class="newhire_cn">
    <div id="newhire_bx02" class="newhire_wrpr">
  	<?php    
		if (empty($output)) {
			if ($_REQUEST['lang']=="ar") {
				print "<p>لا توجد حالياً أية وظائف شاغرة.</p>";
			} else {
				print "<p>There are currently no vacancies available.</p>";
			}
		}
	?>
    <?php	
			
			foreach ($output->vaccancyinforow as $item) {
				if($i >= $limit) continue;
				$applylink = JRoute::_("index.php?option=com_eposts&view=mainapplyjobs&page=home&cirid=".((string)$item->VACCANCYCODE).'&Itemid=303&jobtitle='.((string)$item->JOBTITLE).'&jobid='.((string)$item->JOBCODE));
				/*echo 'VACCANCYCODE'.((string)$item->VACCANCYCODE);
				echo 'JOBTITLE'.((string)$item->JOBTITLE);
				echo 'COMPANY'.((string)$item->COMPANY);
				echo 'DEPARTMENT'.((string)$item->DEPARTMENT);
				echo 'LOCATION'.((string)$item->LOCATION);
				echo 'CLOSEDATE'.((string)$item->CLOSEDATE);*/
				?>
      <div class="newhire_item item">
        <div class="newhire_details">
       <?php if ($_REQUEST['lang']=="ar")
{?>
<p><?php print  JText::_('<b> الرمز المرجعي:</b> ') . ((string)$item->VACCANCYCODE); ?>, <?php print JText::_('<b>رمز الوظيفة:</b> ') . ((string)$item->JOBCODE); ?></p>
			<h4 style="COLOR: #FF8F00;"><?php print  JText::_('<b> المسمى الوظيفي:</b> '); ?><a style="COLOR: #FF8F00; font-weight: bold" href="<?php echo $applylink; ?>"><?php echo  ((string)$item->JOBTITLE); ?></a></h4>
			<p><?php print JText::_('<b>الشركة:</b> ') . ((string)$item->COMPANY); ?></p>
            <p><?php print JText::_('<b>لإدارة:</b> ') . ((string)$item->DEPARTMENT); ?></p>
            <p><?php print JText::_('<b>الدرجة الوظيفية:</b> ') .((string)$item->JOBGRADE); ?></p>
            <p><?php print JText::_('<b>موقع:</b> ') . ((string)$item->LOCATION); ?>, <?php print JText::_('&nbsp;<b>الجنسية:</b> ') . ((string)$item->NATIONALITY); ?></p>
            <p><?php print JText::_('<b>آخر يوم للتقديم:</b> ') . date('d-m-Y', strtotime(((string)$item->CLOSEDATE))); ?></p>
            <!--<p><?php //print JText::_('More Details: ') . ''; ?></p>-->
            
        <p>وصف الوظيفة: <a href="modules/mod_jobboard/tmpl/download.php?hello=true&jbcode=<?php echo ((string)$item->JOBCODE); ?>"> عرض</a></p>
        
		 <p><a style="COLOR: #ffffff;  float:right;" class=readmore href="<?php echo $applylink; ?>"><SPAN> التقديم</SPAN></a></p> 
   
           
		 <?php } else{?>
      <p><?php print JText::_('<b>Reference Code:</b> ') . ((string)$item->VACCANCYCODE); ?>, <?php print JText::_('<b>Job Code:</b> ') . ((string)$item->JOBCODE); ?></p>
			<h4 style="COLOR: #FF8F00;"><?php print JText::_('<b>Job Title:</b> '); ?><a style="COLOR: #FF8F00; font-weight: bold" href="<?php echo $applylink; ?>"><?php echo ((string)$item->JOBTITLE); ?></a></h4>
            <p><?php print JText::_('<b>Company:</b> ') . ((string)$item->COMPANY); ?></p>
            <p><?php print JText::_('<b>Department:</b> ') . ((string)$item->DEPARTMENT); ?></p>
            <p><?php print JText::_('<b>Grade:</b> ') .((string)$item->JOBGRADE); ?></p>
            <p><?php print JText::_('<b>Location:</b> ') . ((string)$item->LOCATION); ?>, <?php print JText::_('<b>Nationality:</b> ') . ((string)$item->NATIONALITY); ?></p>
            <p><?php print JText::_('<b>Last Date:</b> ') . date('d-m-Y', strtotime(((string)$item->CLOSEDATE))); ?></p>
			
            <!--<p><?php //print JText::_('More Details: ') . ''; ?></p>-->
            
        <p>Job Description: <a href="/modules/mod_jobboard/tmpl/download.php?hello=true&jbcode=<?php echo ((string)$item->JOBCODE); ?>"> View </a></p>
        
		 <p><a style="COLOR: #ffffff;  float:left;" class=readmore href="<?php echo $applylink; ?>"><SPAN>Apply</SPAN></a></p> <br />
        <?php }?> 
        </div>
        <div class="clear"></div>
      </div>
				<?php
				$i++;
			}
}
?>
    </div>
  </div>
</div>
<div class="moduletable">
  <div class="controlls">
    <div class="container"><a class="readmore" href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistjobs&page=home&Itemid=303'); ?>"><span><?php print JText::_('EMIRATES_VIEWALL'); ?></span></a></div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>