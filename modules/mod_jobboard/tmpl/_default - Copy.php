
<?php
// no direct access
defined('_JEXEC') or die;
$document = &JFactory::getDocument();
$jobboard_js = "
			document.addEvent('domready', function(){
				var slider02 = new anewsSlide({
					box: $('newhire_bx02'),
					items: $$('#newhire_bx02 .newhire_item'),
					autoPlay: 10000,
					interval: 10000,
					size: 464,
					handles: $$('#handles02 span'),
					onWalk: function(currentItem,currentHandle){
						this.handles.removeClass('active');
						currentHandle.addClass('active');
					}
				});
			});
	";
            require_once dirname(__FILE__).'/nusoap/lib/nusoap.php';
			$client = new nusoap_client("http://intranet/irecwstest/WS_ApplicantManager.svc?wsdl", true);
			
			$error = $client->getError();
			if ($error) {
				echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
			}
			$client->soap_defencoding = 'UTF-8';
			if ($_REQUEST['lang']=="ar")
{
$lang='ar';	
}else{
	
$lang='en';	}
				$result = $client->call("GetOpenJobsByLanguage", array("candidateid" => "", "target"=>"Internal", "targetSpecified"=>"True" , "language"=>$lang ));
?>
		<?php 
		
		 ?>

<div class="moduletable">
  <h3><?php print JText::_('EMIRATES_JOBBOARD'); ?></h3>
  <?php
	if(count($result) > 0){
			$output = simplexml_load_string($result['GetOpenJobsByLanguageResult'], NULL, LIBXML_NOCDATA);
			//$output = array_reverse($output);
			if(count($output) > 1){
				$document->addScriptDeclaration($jobboard_js);
			}
			$limit = 5;	
			$i = 0;
			$count = 0;
  ?>
  <div id="handles02" class="handles">
  	<?php foreach ($output->vaccancyinforow as $item) : ?>
    <?php
	if($count >= $limit) continue;
	$count++;
    ?>
    <span><?php print $count; ?></span>
    <?php endforeach; ?>
  </div>
  <div class="newhire_cn">
    <div id="newhire_bx02" class="newhire_wrpr">
    <?php	
			
			foreach ($output->vaccancyinforow as $item) {
				if($i >= $limit) continue;
				$applylink = JRoute::_("index.php?option=com_eposts&view=mainapplyjobs&page=home&cirid=".((string)$item->VACCANCYCODE).'&Itemid=303&jobtitle='.((string)$item->JOBTITLE));
				/*echo 'VACCANCYCODE'.((string)$item->VACCANCYCODE);
				echo 'JOBTITLE'.((string)$item->JOBTITLE);
				echo 'COMPANY'.((string)$item->COMPANY);
				echo 'DEPARTMENT'.((string)$item->DEPARTMENT);
				echo 'LOCATION'.((string)$item->LOCATION);
				echo 'CLOSEDATE'.((string)$item->CLOSEDATE);*/
				?>
      <div class="newhire_item item">
        <div class="newhire_details">

<?php
header("content-type: text/html; charset=utf-8");
//header("content-type: text/html; charset=windows-1256");
?>
     
	 <?php if ($_REQUEST['lang']=="ar")
{?>
   <p><?php print JText::_(' الرمز المرجعي: ') . ((string)$item->VACCANCYCODE); ?></p>
			<h4><?php print JText::_(' المسمى الوظيفي: '); ?><a href="<?php echo $applylink; ?>"><?php echo  utf8_encode(((string)$item->JOBTITLE)); ?></a></h4>
			<!-- <h4><?php print JText::_(' المسمى الوظيفي: '); ?><a href="<?php echo $applylink; ?>"><?php  utf8_encode((string)$item->JOBTITLE)); ?></a></h4> -->
			
			<!--<p><?php print JText::_('الوظيفي وصف : ') . ((string)$item->JOBDESCRIPTION); ?></p> -->
            <p><?php print JText::_('الشركة: ') . ((string)$item->COMPANY); ?></p>
            <p><?php print JText::_('لإدارة: ') . ((string)$item->DEPARTMENT); ?></p>
            <p><?php print JText::_('الدرجة الوظيفية : ') .((string)$item->JOBGRADE); ?></p>
            <p><?php print JText::_('آخر يوم للتقديم: ') . date('d-m-Y', strtotime(((string)$item->CLOSEDATE))); ?></p>
            <p><?php print JText::_('رمز الوظيفة: ') . ((string)$item->JOBCODE); ?></p>
            <!--<p><?php //print JText::_('More Details: ') . ''; ?></p>-->
            
        <p>وصف الوظيفة: <a href="modules/mod_jobboard/tmpl/download.php?hello=true&jbcode=<?php echo ((string)$item->JOBCODE); ?>"> تحميل المرفقات</a></p>
        
		 <p><a style="COLOR: #ffffff;  float:right;" class=readmore href="<?php echo $applylink; ?>"><SPAN> التقديم</SPAN></a></p> 
   
           
		 <?php } else{?>
          <p><?php print JText::_('Reference Code: ') . ((string)$item->VACCANCYCODE); ?></p>
			<h4><?php print JText::_('Job Title: '); ?><a href="<?php echo $applylink; ?>"><?php echo ((string)$item->JOBTITLE); ?></a></h4>
            <!--<p><?php print JText::_('Job Description: ') . ((string)$item->JOBDESCRIPTION); ?></p> -->
            <p><?php print JText::_('Company: ') . ((string)$item->COMPANY); ?></p>
            <p><?php print JText::_('Department: ') . ((string)$item->DEPARTMENT); ?></p>
            <p><?php print JText::_('Grade: ') .((string)$item->JOBGRADE); ?></p>
            <p><?php print JText::_('Last Date: ') . date('d-m-Y', strtotime(((string)$item->CLOSEDATE))); ?></p>
            <p><?php print JText::_('Job Code: ') . ((string)$item->JOBCODE); ?></p>
            <!--<p><?php //print JText::_('More Details: ') . ''; ?></p>-->
            
        <p>Job Description: <a href="modules/mod_jobboard/tmpl/download.php?hello=true&jbcode=<?php echo ((string)$item->JOBCODE); ?>"> Download Attachment</a></p>
        
		 <p><a style="COLOR: #ffffff;  float:left;" class=readmore href="<?php echo $applylink; ?>"><SPAN>Apply</SPAN></a></p> 
        <?php }?> 
        </div>
        <div class="clear"></div>
      </div>
				<?php
				$i++;
			}
}
?>
    </div>
  </div>
</div>
<div class="moduletable">
  <div class="controlls">
    <div class="container"><a class="readmore" href="<?php print JRoute::_('index.php?option=com_eposts&view=mainlistjobs&page=home&Itemid=303'); ?>"><span><?php print JText::_('EMIRATES_VIEWALL'); ?></span></a></div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>