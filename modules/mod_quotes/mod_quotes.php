<?php
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$document = &JFactory::getDocument();

$document->addStyleSheet(JURI::base() . 'components/com_igallery/cerabox.css');
$document->addScript(JURI::base() . 'components/com_igallery/cerabox.min.js');
$list = modQuotesHelper::getList($params);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_quotes', $params->get('layout', 'default'));
