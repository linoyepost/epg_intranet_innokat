<?php
defined('_JEXEC') or die;

require_once JPATH_SITE.'/components/com_content/helpers/route.php';

JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_content/models', 'ContentModel');

abstract class modANewsHelper
{
	public static function getList(&$params)
	{
		$document = &JFactory::getDocument();
		
		// Get the dbo
		$db = JFactory::getDbo();
		$database = &JFactory::getDBO();

		// Get an instance of the generic articles model
		$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));

		// Set application parameters in model
		$app = JFactory::getApplication();
		$appParams = $app->getParams();
		$model->setState('params', $appParams);
		
		if ($params->get('catid') == 'eposts_announcements') :

			$where = ' WHERE (publish = 1 && announcements_access = 1 && announcements_showontop = 1 && announcements_expirydate >= "' . date('Y-m-d H:i:s') . '")';
                            $query = "SELECT * FROM #__eposts_announcements
                                      $where
                                      ORDER BY id DESC
                                      LIMIT 10";
                                                                                        
                             $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
                             $items = $database->loadObjectList();
		else :

			$where = ' WHERE (publish = 1 && news_access = 1)';
                            $query = "SELECT * FROM #__eposts_news
                                      $where
                                      ORDER BY id DESC
                                      LIMIT 5";
                                                                                        
                             $database->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
                             $items = $database->loadObjectList();
		endif;
		return $items;
	}
	
}