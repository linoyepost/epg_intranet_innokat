/*
---

name: SideBySlide

description: A vertical accordion-type effect.

authors: StÃ©phane P. PÃ©ricat (@sppericat)

license: MIT-style license.

requires: [Core]

provides : SideBySlide

...
*/

SideBySlide = {
	'author': 'Stephane P. Pericat',
	'version': '1.0'
}

SideBySlide.Init = new Class({
	Implements: [Options, Events],
	
	options: {
		'wrapper': 'wrapper', //the id of the div wrapping the slides
		'slides': '.slide', //class name of the slides
		'togglers': '.toggler', //class name of the toggler element
		'defaultView': 0, //the id of the slide to open by default
	},
	
	slides: [],
	togglers: [],
	
	space: 0,
	currentActive: null,
	
	initialize: function(options) {
		if(options) this.setOptions(options);
		
		if($$(this.options.slides) && $$(this.options.togglers)) {
			this.slides = $$(this.options.slides);
			this.togglers = $$(this.options.togglers);
			
			this.slides.each(function(el) {
				el.setStyle("display", "none");
			});
			
			this.space = this.getAvailableSpace();
			this.slideIt(this.options.defaultView);
		} else {
			throw "Invalid class name for the slides and / or the togglers";
		}
		
		this.togglers.each(function(el, id) {
			el.addEvent('click', function(evt) {
				evt.stop();
				if(id == this.currentActive) return;
				this.slideIt(id);
			}.bind(this));
		}.bind(this));
	},
	
	//returns the max width that a slide can have
	getAvailableSpace: function() {
		var usedSpace = 0;
		this.togglers.each(function(el) {
			usedSpace = usedSpace + el.getSize().x;
		});
		
		if($(this.options.wrapper)) {
			return $(this.options.wrapper).getSize().x - usedSpace;
		} else {
			throw "no wrapper element found.";
		}
	},
	
	//performs the accordion effect between slides
	slideIt: function(id) {
		if(!this.currentActive) this.currentActive = this.options.defaultView;
		this.slides[this.currentActive].morph({'width': 0, 'opacity': 0});
		this.slides[id].setStyles({'display': 'block', 'width': 0});
		this.slides[id].morph({'width': 240, 'opacity': 1});
		this.currentActive = id;
	}
});